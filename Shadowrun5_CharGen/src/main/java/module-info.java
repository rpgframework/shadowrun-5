/**
 * @author Stefan Prelle
 *
 */
module shadowrun.chargen {
	exports org.prelle.shadowrun5.chargen.cost;
	exports org.prelle.shadowrun5.chargen.proc;
	exports org.prelle.shadowrun5.chargen;
	exports org.prelle.shadowrun5.common;
	exports org.prelle.shadowrun5.charctrl;
	exports org.prelle.shadowrun5.gen;
	exports org.prelle.shadowrun5.gen.event;
	exports org.prelle.shadowrun5.levelling;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun.core;
	requires simple.persist;
}