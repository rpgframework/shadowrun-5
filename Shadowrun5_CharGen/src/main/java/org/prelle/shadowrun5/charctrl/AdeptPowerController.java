/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;

/**
 * @author Stefan
 *
 */
public interface AdeptPowerController extends Controller {

	//-------------------------------------------------------------------
	public List<AdeptPower> getAvailablePowers();

	//-------------------------------------------------------------------
	public float getPowerPointsLeft();

	//-------------------------------------------------------------------
	public float getPowerPointsInvested();

	//-------------------------------------------------------------------
	public boolean canIncreasePowerPoints();

	//-------------------------------------------------------------------
	public boolean canDecreasePowerPoints();

	//-------------------------------------------------------------------
	public boolean increasePowerPoints();

	//-------------------------------------------------------------------
	public boolean decreasePowerPoint();

	//-------------------------------------------------------------------
	public boolean canBeSelected(AdeptPower data);

	//-------------------------------------------------------------------
	public AdeptPowerValue select(AdeptPower data);

	//-------------------------------------------------------------------
	public AdeptPowerValue select(AdeptPower data, Object selection);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean increase(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(AdeptPowerValue ref);

	//-------------------------------------------------------------------
	public boolean isRecommended(AdeptPower val);

}
