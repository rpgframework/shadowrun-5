package org.prelle.shadowrun5.charctrl;

import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.modifications.AttributeModification;

import de.rpgframework.genericrpg.NumericalValueController;

public interface AttributeController extends Controller, NumericalValueController<Attribute,AttributeValue> {

	public enum ValueState {
		BELOW_MINIMUM,
		WITHIN_MODIFIER,
		UNSELECTED,
		SELECTED,
		UNATAINABLE_MAX,
		ABOVE_MAX
	}

	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeft();

	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeftSpecial();

	//-------------------------------------------------------------------
	public int getMinimalValue(Attribute key);

	//-------------------------------------------------------------------
	public ValueState getValueState(Attribute key, int value);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(Attribute key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(Attribute key);

	//-------------------------------------------------------------------
	public int getIncreaseCost(Attribute key);

	//-------------------------------------------------------------------
	public boolean increase(Attribute key);

	//-------------------------------------------------------------------
	public boolean decrease(Attribute key);

	//-------------------------------------------------------------------
	public void apply(AttributeModification mod);

	//-------------------------------------------------------------------
	public void undo(AttributeModification mod);

}