/**
 *
 */
package org.prelle.shadowrun5.charctrl;

import java.util.Collection;

import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface CharacterController {
	
	//-------------------------------------------------------------------
	public WizardPageType[] getWizardPages();

	//-------------------------------------------------------------------
	public CharGenMode getMode();

	//-------------------------------------------------------------------
	public String getName();

	//-------------------------------------------------------------------
	public void setCallback(SR5LetUserChooseListener callback);

	//-------------------------------------------------------------------
	public void runProcessors();

	//-------------------------------------------------------------------
	/**
	 * @return Real modifications applied (in case of choices this is different than the input)
	 */
	public Collection<Modification> apply(String reason, Collection<Modification> mods);

	//--------------------------------------------------------------------
	public void undo(Collection<Modification> mods);

	//-------------------------------------------------------------------
	public ShadowrunCharacter getCharacter();

	//-------------------------------------------------------------------
	public AttributeController getAttributeController();

	//-------------------------------------------------------------------
	public SkillController getSkillController();

	//-------------------------------------------------------------------
	public SpellController getSpellController();

	//-------------------------------------------------------------------
	public SpellController getAlchemyController();

	//-------------------------------------------------------------------
	public RitualController getRitualController();

	//-------------------------------------------------------------------
	public QualityController getQualityController();

	//-------------------------------------------------------------------
	public AdeptPowerController getPowerController();

	//-------------------------------------------------------------------
	public ComplexFormController getComplexFormController();

	//-------------------------------------------------------------------
	public EquipmentController getEquipmentController();

	//-------------------------------------------------------------------
	public ConnectionsController getConnectionController();

	//-------------------------------------------------------------------
	public SINController getSINController();

	//-------------------------------------------------------------------
	public LifestyleController getLifestyleController();

	//-------------------------------------------------------------------
	public SummonableController getSummonableController();

	//-------------------------------------------------------------------
	public ProgramController getProgramController(CarriedItem item);

}
