/**
 *
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;

/**
 * @author prelle
 *
 */
public interface ComplexFormController extends Controller {

	//--------------------------------------------------------------------
	/**
	 * Returns the number of complex forms left to select
	 */
	public int getComplexFormsLeft();

	//-------------------------------------------------------------------
	public List<ComplexForm> getAvailableComplexForms();

	//-------------------------------------------------------------------
	public boolean canBeSelected(ComplexForm data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(ComplexFormValue data);

	//-------------------------------------------------------------------
	public ComplexFormValue select(ComplexForm data);

	//-------------------------------------------------------------------
	public void deselect(ComplexFormValue data);

}
