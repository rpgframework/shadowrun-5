/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import org.prelle.shadowrun5.Connection;

/**
 * @author prelle
 *
 */
public interface ConnectionsController extends Controller {

	//-------------------------------------------------------------------
	public int getPointsLeft();

	//-------------------------------------------------------------------
	public boolean canCreateConnection();
	
	//-------------------------------------------------------------------
	public Connection createConnection();

	//-------------------------------------------------------------------
	public void removeConnection(Connection con);

	//-------------------------------------------------------------------
	public boolean canIncreaseInfluence(Connection con);

	//-------------------------------------------------------------------
	public boolean increaseInfluence(Connection con);
	
	//-------------------------------------------------------------------
	public boolean canDecreaseInfluence(Connection con);
	
	//-------------------------------------------------------------------
	public boolean decreaseInfluence(Connection con);
	
	//-------------------------------------------------------------------
	public boolean canIncreaseLoyalty(Connection con);
	
	//-------------------------------------------------------------------
	public boolean increaseLoyalty(Connection con);
	
	//-------------------------------------------------------------------
	public boolean canDecreaseLoyalty(Connection con);
	
	//-------------------------------------------------------------------
	public boolean decreaseLoyalty(Connection con);

}
