/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public interface Controller {
	
	//-------------------------------------------------------------------
	public ShadowrunCharacter getModel();
	
	//-------------------------------------------------------------------
	/**
	 * Returns a list of steps to do in this controller
	 */
	public List<String> getToDos();
	
//	//-------------------------------------------------------------------
//	public CharacterController getParentController();

}
