/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class DecisionToMake {
	
	private Modification choice;
	private List<Modification> decision;

	//-------------------------------------------------------------------
	public DecisionToMake(Modification choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	public List<Modification> getDecision() {
		return decision;
	}

	//-------------------------------------------------------------------
	public void setDecision(List<Modification> decision) {
		this.decision = decision;
	}

	//-------------------------------------------------------------------
	public Modification getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (decision==null)
			return "UNDECIDED("+choice+")";
		else
			return "DECIDED("+decision+")";
	}

}
