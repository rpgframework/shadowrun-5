/**
 *
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.items.BodytechQuality;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemEnhancement;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.modifications.CarriedItemModification;

/**
 * @author Stefan
 *
 */
public interface EquipmentController extends Controller {
	
	public enum SelectionOptionType {
		AMOUNT,
		BODYTECH_QUALITY,
		RATING,
	}
	
	public class SelectionOption {
		SelectionOptionType type;
		Object value;
		public SelectionOption(SelectionOptionType type, Object val) {
			this.type = type;
			this.value= val;
		}
		public void setValue(Object value) { this.value = value; }
		public SelectionOptionType getType() { return type; }
		public int getAsAmount() { return (Integer)value; }
		public int getAsRating() { return (Integer)value; }
		public BodytechQuality getAsBodytechQuality() { return (BodytechQuality)value; }
	}

	//-------------------------------------------------------------------
	public List<SelectionOptionType> getOptions(ItemTemplate item);

	//-------------------------------------------------------------------
	/**
	 * Calculate cost for buying an item
	 * @param item
	 * @param rating Item rating. Ignored for items without rating
	 * @param quality Bodytech quality. Ignored for non-bodytech
	 * @return Price in nuyen
	 */
	public int getCost(ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean canBuyLevel(CarriedItem item, int newLevel);

	//-------------------------------------------------------------------
	public boolean changeRating(CarriedItem item, int newRating);

	//-------------------------------------------------------------------
	public boolean canBuyQuality(CarriedItem item, BodytechQuality newQuality);

	//-------------------------------------------------------------------
	public boolean changeQuality(CarriedItem item, BodytechQuality newQuality);

	//-------------------------------------------------------------------
	public boolean canBeSelected(ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	public CarriedItem select(ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean deselect(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * This method sells the item for a factor of the value. In case of 
	 * bodytech, the essence hole is growing.
	 * @param ref Item to sell
	 * @param factor 0.5 = 50% 1.0=100%
	 */
	public void sell(CarriedItem ref, float factor);

	//-------------------------------------------------------------------
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed);

	//-------------------------------------------------------------------
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed, ItemHook slot);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embeddable in the given object
	 */
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embeddable in the given object
	 */
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref, ItemHook slot);

	//-------------------------------------------------------------------
	/**
	 * Get all items that are embedded in the given object
	 */
	public List<CarriedItem> getEmbeddedIn(CarriedItem ref);

	//-------------------------------------------------------------------
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options);

	//-------------------------------------------------------------------
	/**
	 * Use with items that can be embedded in multiple slots
	 */
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook slot, SelectionOption...options);

	//-------------------------------------------------------------------
	public ProgramValue embed(CarriedItem container, Program program, SelectionOption...options);

	//-------------------------------------------------------------------
	public boolean remove(CarriedItem container, CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all enhancements available for the given object
	 */
	public List<ItemEnhancement> getAvailableEnhancementsFor(CarriedItem ref);

	//-------------------------------------------------------------------
	/**
	 * Get all enhancements present in the object
	 */
	public List<ItemEnhancement> getEnhancementsIn(CarriedItem ref);

	//-------------------------------------------------------------------
	public boolean modify(CarriedItem container, ItemEnhancement mod);

	//-------------------------------------------------------------------
	public boolean remove(CarriedItem container, ItemEnhancement mod);

	
	//-------------------------------------------------------------------
	public boolean increase(CarriedItem data);

	//-------------------------------------------------------------------
	public boolean decrease(CarriedItem data);

	//-------------------------------------------------------------------
	public void markUpdated(CarriedItem data);

	//-------------------------------------------------------------------
	public int getCountChangeCost(CarriedItem data, int newCount);

	//-------------------------------------------------------------------
	public boolean canChangeCount(CarriedItem data, int newCount);
	

	//-------------------------------------------------------------------
	public void apply(CarriedItemModification mod);

	//-------------------------------------------------------------------
	public void undo(CarriedItemModification mod);	

	//--------------------------------------------------------------------
	public boolean canIncreaseBoughtNuyen();

	//--------------------------------------------------------------------
	public boolean canDecreaseBoughtNuyen();

	//--------------------------------------------------------------------
	public void increaseBoughtNuyen();

	//--------------------------------------------------------------------
	public void decreaseBoughtNuyen();

	//-------------------------------------------------------------------
	public int getBoughtNuyen();
	
}
