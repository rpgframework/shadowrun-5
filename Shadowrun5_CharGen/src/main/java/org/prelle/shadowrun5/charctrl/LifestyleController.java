/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;

/**
 * @author prelle
 *
 */
public interface LifestyleController extends Controller {

	public int getLifestyleCost(LifestyleValue lifestyle);
	
	public void addLifestyle(LifestyleValue toAdd);

	public void removeLifestyle(LifestyleValue lifestyle);
	
	public boolean canIncreaseMonths(LifestyleValue data);
	
	public boolean canDecreaseMonths(LifestyleValue data);
	
	public void increaseMonths(LifestyleValue data);
	
	public void decreaseMonths(LifestyleValue data);

	public void apply(LifestyleCostModification mod);

	public void undo(LifestyleCostModification mod);
	
}
