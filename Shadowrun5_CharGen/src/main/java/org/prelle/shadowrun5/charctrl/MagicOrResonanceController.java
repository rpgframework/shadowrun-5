/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.MagicOrResonanceOption;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public interface MagicOrResonanceController extends Controller {

	//--------------------------------------------------------------------
	public List<MagicOrResonanceOption> getAvailable();
	
	//--------------------------------------------------------------------
	public boolean canBeSelected(MagicOrResonanceOption type);
	
	//--------------------------------------------------------------------
	public void select(MagicOrResonanceOption value);

	//--------------------------------------------------------------------
	public List<Modification> getToApplyFromSelection();
	
}
