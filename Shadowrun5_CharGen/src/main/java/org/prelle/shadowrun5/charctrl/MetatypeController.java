/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

/**
 * @author Stefan
 *
 */
public interface MetatypeController extends GenerationEventListener {

	//--------------------------------------------------------------------
	/**
	 * Returns all metatypes that may be selected.
	 * @return
	 */
	public List<MetaTypeOption> getAvailable();
	
	//--------------------------------------------------------------------
	public int getSpecialAttributes(MetaType type);
	
	//--------------------------------------------------------------------
	public int getKarmaCost(MetaType type);
	
	//--------------------------------------------------------------------
	public boolean canBeSelected(MetaType type);
	
	//--------------------------------------------------------------------
	public void select(MetaType value);
	
}
