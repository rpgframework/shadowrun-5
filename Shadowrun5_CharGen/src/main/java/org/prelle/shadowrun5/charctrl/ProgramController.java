/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.Program.ProgramType;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.CarriedItem;

/**
 * Instantiated per CarriedItem
 * 
 * @author prelle
 *
 */
public interface ProgramController {

	public CarriedItem getMatrixItem();

	public List<Program> getAvailable(ProgramType type);

	public boolean canBeSelected(Program val);

	public ProgramValue select(Program val);

	public boolean canBeDeselected(ProgramValue val);

	public void deselect(ProgramValue val);

	public int getPrice(Program value);

	public Availability getAvailability(Program value);
	
}
