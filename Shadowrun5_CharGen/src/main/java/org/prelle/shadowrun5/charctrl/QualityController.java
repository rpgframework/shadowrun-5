package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.modifications.QualityModification;

public interface QualityController extends Controller {

	//--------------------------------------------------------------------
	public int getPointsInNegativeQualities();

	//-------------------------------------------------------------------
	public List<Quality> getAvailableQualities();

	//-------------------------------------------------------------------
	public boolean canBeSelected(Quality skill);

	//-------------------------------------------------------------------
	public QualityValue select(Quality skill);

	//-------------------------------------------------------------------
	/**
	 * For qualities that require a selection (e.g. exceptional attribute)
	 */
	public QualityValue select(Quality skill, Object choice);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean increase(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(QualityValue ref);

	//-------------------------------------------------------------------
	public boolean isRecommended(Quality val);

	//-------------------------------------------------------------------
	public void setIgnoreKarma(boolean state);

	//-------------------------------------------------------------------
	public <T> List<T> getChoicesFor(Quality val);


	//-------------------------------------------------------------------
	public void apply(QualityModification mod);

	//-------------------------------------------------------------------
	public void undo(QualityModification mod);

}