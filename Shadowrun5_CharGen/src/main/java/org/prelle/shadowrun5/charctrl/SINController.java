/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import org.prelle.shadowrun5.LicenseValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.modifications.SINModification;

/**
 * @author prelle
 *
 */
public interface SINController extends Controller {

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.Quality quality);

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.Quality quality, int count);

	//-------------------------------------------------------------------
	public boolean canDeleteSIN(SIN data);

	//-------------------------------------------------------------------
	public SIN createNewSIN(String name, SIN.Quality quality);

	//-------------------------------------------------------------------
	public SIN[] createNewSIN(String name, SIN.Quality quality, int count);

	//-------------------------------------------------------------------
	public boolean deleteSIN(SIN data);

	//-------------------------------------------------------------------
	public void apply(SINModification mod);

	//-------------------------------------------------------------------
	public void undo(SINModification mod);
	
	

	//-------------------------------------------------------------------
	public boolean canCreateNewLicense(SIN.Quality quality);

	//-------------------------------------------------------------------
	public boolean canCreateNewLicense(SIN.Quality quality, int count);

	//-------------------------------------------------------------------
	public LicenseValue createNewLicense(String name, SIN.Quality quality);

	//-------------------------------------------------------------------
	public LicenseValue[] createNewLicense(String name, SIN.Quality quality, int count);

}
