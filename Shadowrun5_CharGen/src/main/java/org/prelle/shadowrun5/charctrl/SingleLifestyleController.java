/**
 * 
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Lifestyle;
import org.prelle.shadowrun5.LifestyleOption;
import org.prelle.shadowrun5.LifestyleOptionValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.SIN;

/**
 * Con
 * @author prelle
 *
 */
public interface SingleLifestyleController extends Controller {

	public List<Lifestyle> getAvailableLifestyles();

	public boolean canSelectLifestyle(Lifestyle lifestyle);

	public void selectLifestyle(Lifestyle lifestyle);
	

	public List<LifestyleOption> getAvailableOptions();

	public boolean canSelectOption(LifestyleOption option);

	public LifestyleOptionValue selectOption(LifestyleOption option);

	public void deselectOption(LifestyleOptionValue option);


	public void selectSIN(SIN sin);

	public void selectName(String name);

	public void selectDescription(String text);

	public int getLifestyleCost();
	
	public LifestyleValue getResult();
	
}
