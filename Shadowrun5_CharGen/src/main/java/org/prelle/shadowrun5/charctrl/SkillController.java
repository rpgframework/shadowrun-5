package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.NumericalValueController;

public interface SkillController extends Controller, NumericalValueController<Skill, SkillValue> {

	//--------------------------------------------------------------------
	/**
	 * @return -1 if this does not apply, otherwise a 0+
	 */
	public int getPointsLeftSkillGroups();

	//--------------------------------------------------------------------
	/**
	 * @return -1 if this does not apply, otherwise a 0+
	 */
	public int getPointsLeftSkills();

	//-------------------------------------------------------------------
	public int getPointsLeftInKnowledgeAndLanguage();

	//-------------------------------------------------------------------
	public List<SkillGroup> getAvailableSkillGroups();

	//-------------------------------------------------------------------
	public List<SkillGroup> getAvailableSkillGroups(SkillType type);

	//-------------------------------------------------------------------
	public List<Skill> getAvailableSkills();

	//-------------------------------------------------------------------
	public List<Skill> getAvailableSkills(SkillType type);

	//-------------------------------------------------------------------
	public boolean canBeSelected(SkillGroup grp);

	//-------------------------------------------------------------------
	public boolean canBeSelected(Skill skill);

	//-------------------------------------------------------------------
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec);

	//-------------------------------------------------------------------
	public SkillGroupValue select(SkillGroup grp);

	//-------------------------------------------------------------------
	public SkillValue select(Skill skill);

	//-------------------------------------------------------------------
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName);

	//-------------------------------------------------------------------
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec);

	//-------------------------------------------------------------------
	public boolean deselect(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(SkillValue ref, SkillSpecialization spec);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean increase(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean increase(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean decrease(SkillValue ref);

	//-------------------------------------------------------------------
	public boolean isRecommended(SkillGroup val);

	//-------------------------------------------------------------------
	public boolean isRecommended(Skill val);

	//-------------------------------------------------------------------
	public boolean canBreakSkillGroup(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public void breakSkillGroup(SkillGroupValue ref);

	//-------------------------------------------------------------------
	public boolean canSpecializeIn(SkillValue val);

	//-------------------------------------------------------------------
	public void apply(SkillModification mod);

	//-------------------------------------------------------------------
	public void undo(SkillModification mod);

	//-------------------------------------------------------------------
	public void apply(SkillGroupModification mod);

	//-------------------------------------------------------------------
	public void undo(SkillGroupModification mod);

	//-------------------------------------------------------------------
	public void apply(IncompetentSkillGroupModification mod);

	//-------------------------------------------------------------------
	public void undo(IncompetentSkillGroupModification mod);

}