/**
 *
 */
package org.prelle.shadowrun5.charctrl;

import java.util.List;

import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.SummonableValue;

/**
 * @author Stefan
 *
 */
public interface SummonableController extends Controller {

	public List<Summonable> getAvailable();

	public boolean canBeSelected(Summonable value);

	public boolean canBeDeselected(SummonableValue value);

	public SummonableValue select(Summonable value, int services);

	public void deselect(SummonableValue value);

}
