/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.LifestyleController;
import org.prelle.shadowrun5.chargen.cost.LifestyleCostCalculator;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonLifestyleController implements LifestyleController, CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	protected EquipmentController equip;
	protected ShadowrunCharacter model;
	protected CommonSR5CharacterGenerator parent;
	
	protected LifestyleCostCalculator costs;
	
	//-------------------------------------------------------------------
	public CommonLifestyleController(CommonSR5CharacterGenerator parent, EquipmentController equip) {
		this.parent = parent;
		this.model = parent.getCharacter();
		this.equip = equip;
		costs = new LifestyleCostCalculator();
	}

	//-------------------------------------------------------------------
	public LifestyleCostCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (model.getLifestyle().isEmpty())
			ret.add(RES.getString("lifestylegen.todo.missing_primary_lifestyle"));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#addLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void addLifestyle(LifestyleValue ref) {
		// Add generic modifications
//		for (LifestyleCostModification tmp : otherModifications) {
//			ref.addModification(tmp);
//		}
		
		int cost = ShadowrunTools.getLifestyleCost(model, ref);
		
		model.addLifestyle(ref);
//		model.setNuyen(model.getNuyen() - cost);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_ADDED, ref));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, model.getNuyen()));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#removeLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void removeLifestyle(LifestyleValue lifestyle) {
		// Add generic modifications
//		for (LifestyleCostModification tmp : otherModifications) {
//			lifestyle.removeModification(tmp);
//		}
		
		model.removeLifestyle(lifestyle);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_REMOVED, lifestyle));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#canIncreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public boolean canIncreaseMonths(LifestyleValue data) {		
		// Is it affordable?
		if (data.getCostPerMonth()>model.getNuyen())
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#canDecreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public boolean canDecreaseMonths(LifestyleValue data) {
		// Not below 1
		if (data.getPaidMonths()<=1)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#increaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void increaseMonths(LifestyleValue data) {
		logger.debug("increaseMonths");
		if (!canIncreaseMonths(data))
			return;
		
		data.setPaidMonths(data.getPaidMonths()+1);

		int cost = data.getCostPerMonth();
		model.setNuyen(model.getNuyen() - cost);
		logger.info("Lifestyle "+data.getName()+" increased to "+data.getPaidMonths()+" months for "+cost+" nuyen");
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, new int[]{model.getNuyen(), -1}));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#decreaseMonths(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void decreaseMonths(LifestyleValue data) {
		if (!canDecreaseMonths(data))
			return;
		
		data.setPaidMonths(data.getPaidMonths()-1);

		int cost = data.getCostPerMonth();
		model.setNuyen(model.getNuyen() + cost);
		logger.info("Lifestyle "+data.getName()+" decreased to "+data.getPaidMonths()+" months for "+cost+" nuyen");
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, new int[]{model.getNuyen(), -1}));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#apply(org.prelle.shadowrun.modifications.LifestyleCostModification)
	 */
	@Override
	public void apply(LifestyleCostModification mod) {
		logger.warn("IGNORE "+mod);
//		if (otherModifications.contains(mod))
//			return;
//		otherModifications.add(mod);
//		
//		// Add to existing lifestyles
//		for (LifestyleValue tmp : model.getLifestyle()) {
//			tmp.addModification(mod);
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#undo(org.prelle.shadowrun.modifications.LifestyleCostModification)
	 */
	@Override
	public void undo(LifestyleCostModification mod) {
		logger.warn("IGNORE "+mod);
//		if (!otherModifications.remove(mod))
//			return;
//		
//		// Remove from existing lifestyles
//		for (LifestyleValue tmp : model.getLifestyle()) {
//			tmp.removeModification(mod);
//		}
//		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#getLifestyleCost(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public int getLifestyleCost(LifestyleValue lifestyle) {
		int percentToAdd = 0;
		int nuyenToAdd = 0;
		// Check for lifestyle multipliers
		for (LifestyleCostModification mod : costs.getLifestyleModifications()) {
				percentToAdd += mod.getPercent();
				nuyenToAdd   += mod.getFixed();
		}
		/*
		 * Include modifications
		 */
		int total = lifestyle.getCost() + nuyenToAdd + (lifestyle.getCost()*percentToAdd/100);
		return total;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.proc.CharacterProcessor#process(org.prelle.shadowrun.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

//		logger.trace("START: process");
//		try {
//			for (Modification tmp : previous) {
//				if (tmp instanceof AddLifestyleModification) {
//					otherModifications.add((LifestyleCostModification) tmp);
//				}
//			}
//		} finally {
//			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
//		}
		return unprocessed;
	}

}
