/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Sense;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.chargen.cost.PowerCostCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.proc.CharacterProcessor;

/**
 * @author Stefan
 *
 */
public class CommonPowerController implements AdeptPowerController {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	protected CommonSR5CharacterGenerator parent;
	protected ShadowrunCharacter model;
	private PowerCostCalculator costs;
	
	//--------------------------------------------------------------------
	public CommonPowerController(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		costs = new PowerCostCalculator(parent);
	}

	//-------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		float left = getPowerPointsLeft(); 
		if (left!=0 && model.getMagicOrResonanceType().usesPowers()) {
			ret.add(String.format(RES.getString("powergen.todo"), left));
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#getPowerPointsInvested()
	 */
	@Override
	public float getPowerPointsInvested() {
		return costs.getPowerPointsInvested();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#getAvailablePowers()
	 */
	@Override
	public List<AdeptPower> getAvailablePowers() {
		// Build a list 
		List<AdeptPower> ret = new ArrayList<AdeptPower>();
		for (AdeptPower power : ShadowrunCore.getAdeptPowers()) {			
			if (canBeSelected(power))
				ret.add(power);
		}
		
		Collections.sort(ret);
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#getPowerPointsLeft()
	 */
	@Override
	public float getPowerPointsLeft() {
		return costs.getPowerPointsLeft();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canIncreasePowerPoints()
	 */
	@Override
	public boolean canIncreasePowerPoints() {
		MagicOrResonanceType magic = model.getMagicOrResonanceType();
		if (!magic.usesPowers())
			return false;
		
		// Normal adepts don't increase power points
		if (!magic.paysPowers())
			return false;
		
		// Cannot increase higher than magic attribute
		if (model.getBoughtPowerPoints() >= model.getAttribute(Attribute.MAGIC).getModifiedValue())
			return false;
		
		// Needs 5 Karma per power point
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canDecreasePowerPoints()
	 */
	@Override
	public boolean canDecreasePowerPoints() {
		return model.getBoughtPowerPoints()>0 && getPowerPointsLeft()>=1.0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#increasePowerPoints()
	 */
	@Override
	public boolean increasePowerPoints() {
		if (!canIncreasePowerPoints())
			return false;
		
		model.setBoughtPowerPoints(model.getBoughtPowerPoints()+1);
		model.setKarmaFree(model.getKarmaFree()-5);
		model.setKarmaInvested(model.getKarmaInvested()+5);
		logger.info("Increase power points to "+model.getPowerPoints());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[] {model.getKarmaFree(), model.getKarmaInvested()}));
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#decreasePowerPoint()
	 */
	@Override
	public boolean decreasePowerPoint() {
		if (!canDecreasePowerPoints())
			return false;
		
		model.setBoughtPowerPoints(model.getBoughtPowerPoints()-1);
		model.setKarmaFree(model.getKarmaFree()+5);
		model.setKarmaInvested(model.getKarmaInvested()-5);
		logger.info("Decrease power points to "+model.getPowerPoints());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[] {model.getKarmaFree(), model.getKarmaInvested()}));
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canBeSelected(org.prelle.shadowrun5.AdeptPower)
	 */
	@Override
	public boolean canBeSelected(AdeptPower data) {
		MagicOrResonanceType magic = model.getMagicOrResonanceType();
		if (!magic.usesPowers())
			return false;
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Character already has power. Can it be selected
			// multiple times?
			if (data.getSelectFrom()==null) {
				// No, than this can not be selected
				return false;
			}
		}
		// Can the character afford the cost
		if (getPowerPointsLeft()<data.getCostForLevel(1))
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	private boolean canBeSelected(AdeptPower data, Object choice) {
		if (!canBeSelected(data))
			return false;
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Ensure no given power has same choice
			for (AdeptPowerValue tmp : model.getAdeptPowers()) {
				if (tmp.getModifyable()!=data)
					continue;
				if (tmp.getChoice()==choice) {
					return false;
				}
			}
		}
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#select(org.prelle.shadowrun5.AdeptPower)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data) {
		logger.debug("try select "+data);
		if (!canBeSelected(data))
			return null;
		if (data.needsChoice())
			throw new IllegalArgumentException(data.getId()+" needs to be accompanied with a selection");
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_ADDED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWERS_AVAILABLE_CHANGED, getAvailablePowers()));		
		parent.runProcessors();
		
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#select(org.prelle.shadowrun5.AdeptPower, java.lang.Object)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data, Object selection) {
		logger.debug("try select "+data+" with "+selection);
		if (!canBeSelected(data, selection))
			return null;
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		switch (data.getSelectFrom()) {
		case LIMIT:
			if (selection!=Attribute.LIMIT_ASTRAL && selection!=Attribute.LIMIT_MENTAL
			&& selection!=Attribute.LIMIT_PHYSICAL && selection!=Attribute.LIMIT_SOCIAL) {
				logger.warn("Selection "+selection+" is not a limit");
				return null;
			}
			ref.setChoice((Attribute)selection);
			ref.setChoiceReference(((Attribute)selection).name());
			break;
		case PHYSICAL_ATTRIBUTE:
			if (!(selection instanceof Attribute) || !((Attribute)selection).isPhysical()) {
				logger.warn("Selection "+selection+" is not a physical attribute");
				return null;
			}
			ref.setChoice((Attribute)selection);
			ref.setChoiceReference(((Attribute)selection).name());
			break;
		case COMBAT_SKILL:
			if (!(selection instanceof Skill))
				return null;
			Skill skilSel = (Skill)selection;
			if (skilSel.getType()!=SkillType.COMBAT) {
				logger.warn("Can only select combat skills");
				return null;
			}
			ref.setChoice((Skill)selection);
			ref.setChoiceReference(((Skill)selection).getId());
//			ref.setSelectedSkill(skilSel);
			break;
		case SKILL:
			if (!(selection instanceof Skill))
				return null;
			skilSel = (Skill)selection;
			switch (skilSel.getType()) {
			case COMBAT:
			case PHYSICAL:
			case SOCIAL:
			case TECHNICAL:
			case VEHICLE:
//				ref.setSelectedSkill(skilSel);
				ref.setChoice(skilSel);
				ref.setChoiceReference(skilSel.getId());
				break;
			default:
				logger.warn("Can not select "+skilSel.getType()+" for "+data);
				return null;
			}
			break;
		case SENSE:
			if (!(selection instanceof Sense)) {
				logger.warn("Selection "+selection+" is not a sense");
				return null;
			}
			ref.setChoice((Sense)selection);
			ref.setChoiceReference(((Sense)selection).name());
			break;
		default:
			throw new RuntimeException("Not supported yet: "+data.getSelectFrom());
		}
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_ADDED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWERS_AVAILABLE_CHANGED, getAvailablePowers()));		
		parent.runProcessors();
		
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canBeDeselected(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDeselected(AdeptPowerValue ref) {
		if (!model.getAdeptPowers().contains(ref)) {
			return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#deselect(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean deselect(AdeptPowerValue ref) {
		logger.debug("try deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
		
		model.removeAdeptPower(ref);
		logger.info("Deselected adept power "+ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWERS_AVAILABLE_CHANGED, getAvailablePowers()));		
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canBeIncreased(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean canBeIncreased(AdeptPowerValue ref) {
		// Is the power one that can be increased
		if (!ref.getModifyable().hasLevels())
			return false;
		// Is the level below magic attribute
		if (ref.getLevel()>=model.getAttribute(Attribute.MAGIC).getModifiedValue())
			return false;
		
		// Is there enough karma
		float costDiff = ref.getModifyable().getCostForLevel(ref.getLevel()+1) - ref.getModifyable().getCostForLevel(ref.getLevel());
		if (getPowerPointsLeft()<costDiff)
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#canBeDecreased(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDecreased(AdeptPowerValue ref) {
		// TODO Auto-generated method stub
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#increase(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean increase(AdeptPowerValue ref) {
		if (!canBeIncreased(ref))
			return false;
		
		logger.info("Increase power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()+1));
		ref.setLevel(ref.getLevel()+1);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#decrease(org.prelle.shadowrun5.AdeptPowerValue)
	 */
	@Override
	public boolean decrease(AdeptPowerValue ref) {
		if (!canBeDecreased(ref))
			return false;
		
		logger.info("Decrease power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()+1));
		ref.setLevel(ref.getLevel()-1);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AdeptPowerController#isRecommended(org.prelle.shadowrun5.AdeptPower)
	 */
	@Override
	public boolean isRecommended(AdeptPower val) {
		// TODO Auto-generated method stub
		return false;
	}

}
