/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ChoiceType;
import org.prelle.shadowrun5.MentorSpirit;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.Quality.QualityType;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.chargen.cost.CommonQualityCostCalculator;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.QualityModification;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonQualityController implements QualityController {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private CharGenMode mode;
	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;

	private List<Quality> available;
	private boolean ignoreKarma;
	
	private CommonQualityCostCalculator costs;
	
	//-------------------------------------------------------------------
	public CommonQualityController(CommonSR5CharacterGenerator parent, CharGenMode mode) {
		this.parent= parent;
		this.model = parent.getCharacter();
		this.mode  = mode;
		available = new ArrayList<>();
		updateAvailable();
		
		costs = new CommonQualityCostCalculator();
	}

	//-------------------------------------------------------------------
	public CommonQualityCostCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("START updateAvailable");
		available.clear();
		available.addAll(ShadowrunCore.getQualities());
		// Remove those that are not freely selectable
		available = available.stream().filter(qual -> qual.isFreeSelectable() && !qual.isMetagenetic()).collect(Collectors.toList());
		// Remove those the character already has
		for (QualityValue val : model.getQualities()) {
			if (!val.getModifyable().isMultipleSelectable())
				available.remove(val.getModifyable());
			else if (!val.getModifyable().isFreeSelectable())
				available.remove(val.getModifyable());
		}

		Collections.sort(available);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, available));
		logger.debug("STOP updateAvailable");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getPointsInNegativeQualities()
	 */
	@Override
	public int getPointsInNegativeQualities() {
		return costs.getPointsInNegativeQualities();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getAvailableQualities()
	 */
	@Override
	public List<Quality> getAvailableQualities() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeSelected(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public boolean canBeSelected(Quality data) {
		if (data==null)
			throw new NullPointerException("Quality not set");
		if (!data.isFreeSelectable())
			throw new NullPointerException("Quality only selectable by metatype");
		if (data.isMetagenetic())
			throw new NullPointerException("Quality only selectable for changelings");
		// Not already selected
		if (model.getQualities().stream().anyMatch(qval -> (qval.getModifyable()==data && !data.isMultipleSelectable()))) {
			return false;
		}
		// Max 25 points for negative qualities
		if (data.getType()==QualityType.NEGATIVE && (getPointsInNegativeQualities()+data.getCost())>25)
			return false;
		
		// Points left
		if (ignoreKarma)
			return true;
		
		if (data.getType()==QualityType.POSITIVE)
			return model.getKarmaFree()>=(getKarmaCost(data));
		else
			return true;
	}

	//-------------------------------------------------------------------
	private int getKarmaCost(Quality data) {
		return data.getCost() * ((mode==CharGenMode.CREATING)?1:2);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#select(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public QualityValue select(Quality data) {
		logger.debug("select "+data);
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected quality "+data);
		QualityValue sVal = new QualityValue(data, 1);
		model.addQuality(sVal);
		// Record in history
//		QualityModification mod = new QualityModification(data);
//		
//		// Pay karma
//		if (!ignoreKarma && data.getType()==QualityType.POSITIVE) {
//			mod.setExpCost(getKarmaCost(data));
//			model.setKarmaFree(model.getKarmaFree()-getKarmaCost(data));
//			if (mode==CharGenMode.LEVELING) {
//				model.setKarmaInvested(model.getKarmaInvested()+getKarmaCost(data));
//			}
//			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
//		} else if (!ignoreKarma && mode==CharGenMode.CREATING) {
//			mod.setExpCost(getKarmaCost(data));
//			model.setKarmaFree(model.getKarmaFree()+getKarmaCost(data));
//			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
//		}
//		logger.debug("  karma cost was "+mod.getExpCost()+"  new free karma is "+model.getKarmaFree());

		// Apply modifications
//		logger.warn("----------------TODO: apply modificiations of quality----------------------");
//		parent.apply(data.getName(), data.getModifications());
		
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_ADDED, sVal));
		updateAvailable();
		
		
		parent.runProcessors();
		
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#select(org.prelle.shadowrun5.Quality, java.lang.Object)
	 */
	@Override
	public QualityValue select(Quality data, Object choice) {
		logger.debug("select "+data+" with choice "+choice);
		if (!data.needsChoice())
			throw new IllegalArgumentException("Quality "+data+" does not need a choice");
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}
		
		// Change model
		QualityValue sVal = new QualityValue(data, 1);
		model.addQuality(sVal);
		sVal.setChoice(choice);
		switch (data.getSelect()) {
		case SKILLGROUP:
			sVal.setChoiceReference(  ((SkillGroup)choice).getId() );
			break;
		case ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
		case LIMIT:
			sVal.setChoiceReference(  ((Attribute)choice).name() );
			break;
		case COMBAT_SKILL:
		case SKILL:
			sVal.setChoiceReference(  ((Skill)choice).getId() );
			break;
		case MATRIX_ACTION:
			sVal.setChoiceReference(  ((ShadowrunAction)choice).getId() );
			break;
		case MENTOR_SPIRIT:
			sVal.setChoiceReference(  ((MentorSpirit)choice).getId() );
			break;
		default:
			logger.error("Don't know how to set reference for choice type "+choice.getClass());
		}
		
//		// Record in history
//		QualityModification qmod = new QualityModification(data);
//		
//		// Pay karma
//		if (!ignoreKarma && data.getType()==QualityType.POSITIVE) {
//			model.setKarmaFree(model.getKarmaFree()-getKarmaCost(data));
//			model.setKarmaInvested(model.getKarmaInvested()+getKarmaCost(data));
//			qmod.setExpCost(getKarmaCost(data));
//			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
//		}
		
		/*
		 * When an quality needs a choice to be made, all modifications attached 
		 * to the quality needs to be scanned to be modified accordingly
		 */
		ChoiceType type = data.getSelect();
		logger.debug("Mods = "+data.getModifications());
		for (Modification mod : data.getModifications()) {
			if (type==null) {
				sVal.addModification(mod);
			} else {
				logger.debug("process option "+type+" with "+mod.getClass()+" = "+mod);
				switch (type) {
				case ATTRIBUTE:
					if (mod instanceof AttributeModification) {
						AttributeModification newMod = (AttributeModification) ((AttributeModification)mod).clone();
						newMod.setAttribute((Attribute)choice);
						sVal.addModification(newMod);
						sVal.setDescription(newMod.getAttribute().getName());
					} else
						logger.warn("Choice of "+type+" was "+choice+", but I find modification "+mod);
					break;
				case SKILL:
					if (mod instanceof SkillModification) {
						SkillModification newMod = (SkillModification) ((SkillModification)mod).clone();
						newMod.setSkill((Skill)choice);
						sVal.addModification(newMod);
						sVal.setDescription(newMod.getSkill().getName());
						sVal.addModification(newMod);
					} else
						logger.warn("Choice of "+type+" was "+choice+", but I find modification "+mod);
					break;
				case SKILLGROUP:
					if (mod instanceof IncompetentSkillGroupModification) {
						IncompetentSkillGroupModification newMod = new IncompetentSkillGroupModification((SkillGroup) choice, sVal);
						sVal.addModification(newMod);
						sVal.setDescription(newMod.getSkillGroup().getName());
						sVal.addModification(newMod);
					} else
						logger.warn("Choice of "+type+" was "+choice+", but I find modification "+mod);
					break;
				case MATRIX_ACTION:
					sVal.setDescription(((ShadowrunAction)choice).getName());
//					if (mod instanceof IncompetentSkillGroupModification) {
//						IncompetentSkillGroupModification newMod = new IncompetentSkillGroupModification((SkillGroup) choice, sVal);
//						sVal.addModification(newMod);
//						sVal.setDescription(newMod.getSkillGroup().getName());
//						sVal.addModification(newMod);
//					} else
						logger.warn("Choice of "+type+" was "+choice+", but I find modification "+mod);
					break;
				default:
					logger.error("Don't know how to select with choice "+type);
					sVal.addModification(mod);
				}
			}
		}
		logger.info("Selected quality "+data+" with modifications "+sVal.getModifications());

		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_ADDED, sVal));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		updateAvailable();
		
		parent.runProcessors();
		
		return sVal;
	}

//	//-------------------------------------------------------------------
//	private QualityModification findModification(QualityValue ref) {
//		QualityModification ret = null;
//		for (Modification mod : undoList) {
//			if (!(mod instanceof QualityModification))
//				continue;
//			QualityModification tmp = (QualityModification)mod;
//			if (tmp.getModifiedItem()==ref.getModifyable())
//				ret = tmp;
//		}
//		return ret;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeDeselected(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeDeselected(QualityValue ref) {
		// Find modification to undo
		return model.getUserSelectedQualities().contains(ref);
//		QualityModification mod = findModification(ref);
//		if (mod!=null)
//			return true;
//		
//		// Must exist
//		if (!model.getQualities().contains(ref))
//			return false;
//		
//		// Points left
//		if (ignoreKarma)
//			return true;
//
//		// Must have enough karma to remove negative qualities
//		if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
//			if (model.getKarmaFree()<getKarmaCost(ref.getModifyable()) )
//				return false;
//		}
//		
//		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#deselect(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean deselect(QualityValue ref) {
		logger.info("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;

		// Change model
//		ref.setPoints(0);
		model.removeQuality(ref);
		
		// Find modification to undo
//		QualityModification mod = findModification(ref);
//		// Change karma
//		if (mod!=null) {
//			logger.debug("  undo "+mod);
//			int cost = getKarmaCost(ref.getModifyable())*(ref.getPoints()>0?ref.getPoints():1);
//			cost = mod.getExpCost();
//			logger.debug("  cost was "+cost+"   old free karma was "+model.getKarmaFree());
//			if (ref.getModifyable().getType()==QualityType.POSITIVE) {
//				model.setKarmaFree(model.getKarmaFree()+cost);
//				model.setKarmaInvested(model.getKarmaInvested()-cost);
//			} else {
//				model.setKarmaFree(model.getKarmaFree()-cost);
//				model.setKarmaInvested(model.getKarmaInvested()+cost);
//			}
//			undoList.remove(mod);
//		} else {
//			logger.debug("  deselecting must be payed with karma");
//			int cost = getKarmaCost(ref.getModifyable())*(ref.getPoints()>0?ref.getPoints():1);
////			int cost = getKarmaCost( ref.getModifyable() );
//			if (!ignoreKarma) {
//				if (ref.getModifyable().getType()==QualityType.POSITIVE) {
//					model.setKarmaFree(model.getKarmaFree()+cost);
//					model.setKarmaInvested(model.getKarmaInvested()-cost);
//				} else {
//					model.setKarmaFree(model.getKarmaFree()-cost);
//					model.setKarmaInvested(model.getKarmaInvested()+cost);
//					cost = -cost;
//				}
//			}
//			// Record in history
//			mod = new QualityModification(ref.getModifyable());
//			mod.setExpCost(cost);
//			mod.setRemove(true);
//			undoList.add(mod);
//		}
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		updateAvailable();
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeIncreased(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeIncreased(QualityValue ref) {
		// Quality must exist in character
		if (!model.hasQuality(ref.getModifyable().getId()))
			return false;
		Quality data = ref.getModifyable();
		if (data.getMax()<2) {
			return false;
		}
		// Must not be at limit
		if (ref.getPoints()>=ref.getModifyable().getMax())
			return false;

		if (data.getType()==QualityType.POSITIVE) {
			// Enough points
			if (model.getKarmaFree()<getKarmaCost(data)) {
				return false;
			}
		} else {
			if (mode==CharGenMode.CREATING) {
				// May not exceed 25 points from negative qualities
				if ( (getPointsInNegativeQualities()+getKarmaCost(data))>25)
					return false;
			}
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeDecreased(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeDecreased(QualityValue ref) {
		// Quality must exist in character
		if (!model.hasQuality(ref.getModifyable().getId()))
			return false;
		// Must have levels
		if (ref.getModifyable().getMax()==0)
			return false;
		// Must not be 0
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#increase(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean increase(QualityValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()+1);

		Quality data = ref.getModifyable();
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()-getKarmaCost(data));
		else
			model.setKarmaFree(model.getKarmaFree()+getKarmaCost(data));
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#decrease(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean decrease(QualityValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getPoints()==0)
			model.removeQuality(ref);
		
		// Change karma
		Quality data = ref.getModifyable();
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()+getKarmaCost(data));
		else
			model.setKarmaFree(model.getKarmaFree()-getKarmaCost(data));
		
		// Inform listeners
		if (ref.getPoints()==0)
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_REMOVED, ref));
		else
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		
		updateAvailable();
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#isRecommended(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public boolean isRecommended(Quality val) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#setIgnoreKarma(boolean)
	 */
	@Override
	public void setIgnoreKarma(boolean state) {
		this.ignoreKarma = state;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getChoicesFor(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public List<Object> getChoicesFor(Quality val) {
		List<Object> ret = new ArrayList<>();
		if (val.needsChoice()) {
			switch (val.getSelect()) {
			case SKILLGROUP:
				for (SkillGroup grp : ShadowrunCore.getSkillgroups()) {
//					if (!grp.getId().equals("KNOWLEDGE"))
						ret.add(grp);
				}
				break;
			default:
				logger.warn("TODO: don't know how to select "+val.getSelect());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#apply(org.prelle.shadowrun5.modifications.QualityModification)
	 */
	@Override
	public void apply(QualityModification mod) {
		logger.info(" apply "+mod);
		String id = mod.getModifiedItem().getId();
		if (model.hasQuality(id) && mod.isRemove())
			model.removeQuality(model.getQuality(id));
		else if (!model.hasQuality(id) && !mod.isRemove()) {
			model.addQuality(new QualityValue(mod.getModifiedItem(), mod.getValue()));
		}
	}

	@Override
	public void undo(QualityModification mod) {
		logger.warn("TODO: undo "+mod);
		
	}

}
