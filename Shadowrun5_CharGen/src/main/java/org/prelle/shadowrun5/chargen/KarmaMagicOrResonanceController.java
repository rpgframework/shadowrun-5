/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.chargen.cost.KarmaMagicOrResonanceCostCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaMagicOrResonanceController implements MagicOrResonanceController {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	
	private KarmaMagicOrResonanceCostCalculator costs;

	private MagicOrResonanceOption current;
	private List<Modification> applyLater;

	private List<MagicOrResonanceOption> available;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaMagicOrResonanceController(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		available = new ArrayList<>();
		model = parent.getCharacter();
		
		costs = new KarmaMagicOrResonanceCostCalculator();
		
		available = new ArrayList<>();
		for (MagicOrResonanceType type : ShadowrunCore.getMagicOrResonanceTypes()) {
			available.add(new MagicOrResonanceOption(type));
//			switch (type.getId()) {
//			case "mundane": available.add(new MagicOrResonanceOption(type)); break;
//			case "magician": available.add(new MagicOrResonanceOption()); break;
//			}
		}
	}

	//--------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getAvailable()
	 */
	@Override
	public List<MagicOrResonanceOption> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#canBeSelected(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public boolean canBeSelected(MagicOrResonanceOption type) {
		return model.getKarmaFree() >= type.getType().getCost();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#select(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public void select(MagicOrResonanceOption value) {
		if (value==current)
			return;
		if (value==null)
			return;
		if (!canBeSelected(value)) {
			logger.warn("Trying to select option that cannot be selected: "+value);
			return;
		}

		logger.info("Select "+value.getType());
		// Change
		model.setMagicOrResonanceType(value.getType());

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MAGICORRESONANCE_CHANGED, value));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getToApplyFromSelection()
	 */
	@Override
	public List<Modification> getToApplyFromSelection() {
		// TODO Auto-generated method stub
		return null;
	}

}
