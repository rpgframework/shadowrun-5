/**
 *
 */
package org.prelle.shadowrun5.chargen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.SummonableValue;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.chargen.cost.SummonableCostCalculator;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.levelling.NoCostSummonableController;
import org.prelle.shadowrun5.proc.CharacterProcessor;

/**
 * @author Stefan
 *
 */
public class KarmaSummonableController extends NoCostSummonableController implements SummonableController {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private CommonSR5CharacterGenerator parent;
	private SummonableCostCalculator costs;

	//--------------------------------------------------------------------
	/**
	 */
	public KarmaSummonableController(CommonSR5CharacterGenerator parent) {
		super(parent.getCharacter());
		this.parent = parent;
		costs = new SummonableCostCalculator();
	}

	//-------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeSelected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeSelected(Summonable value) {
		return model.getKarmaFree()>=1;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeDeselected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeDeselected(SummonableValue value) {
		return model.getSummonables().contains(value);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#select(org.prelle.shadowrun5.Summonable, int, int)
	 */
	@Override
	public SummonableValue select(Summonable value, int services) {
		SummonableValue val = super.select(value, services);

		if (val!=null) {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLE_ADDED, val));
			parent.runProcessors();
		}

		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#delect(org.prelle.shadowrun5.SummonableValue)
	 */
	@Override
	public void deselect(SummonableValue value) {
		if (!canBeDeselected(value))
			return;

		super.deselect(value);

		parent.runProcessors();
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLE_REMOVED, value));

	}

}
