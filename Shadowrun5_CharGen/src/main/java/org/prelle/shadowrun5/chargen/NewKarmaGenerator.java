/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.Random;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.RewardImpl;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.chargen.cost.CommonQualityCostCalculator;
import org.prelle.shadowrun5.chargen.proc.CalculateFreeConnectionPoints;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AddNuyenModification;
import org.prelle.shadowrun5.proc.ApplyAttributeModifications;
import org.prelle.shadowrun5.proc.ApplySINModifications;
import org.prelle.shadowrun5.proc.CalculateDerivedAttributes;
import org.prelle.shadowrun5.proc.GetModificationsFromMagicOrResonance;
import org.prelle.shadowrun5.proc.GetModificationsFromMetaType;
import org.prelle.shadowrun5.proc.GetModificationsFromQualities;
import org.prelle.shadowrun5.proc.ResetModifications;

import de.rpgframework.genericrpg.Reward;

/**
 * @author prelle
 *
 */
public class NewKarmaGenerator extends CommonSR5CharacterGenerator {
	
	private enum Mode {
		FINISHED,
		CREATION,
		TUNING
	}

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	protected final static ResourceBundle RUNFAST = ShadowrunCharGenConstants.RES; // RunFaster

	private boolean dontProcess;
	private Mode mode;

	//-------------------------------------------------------------------
	/**
	 */
	public NewKarmaGenerator() {
		super();
		mode = Mode.FINISHED;
		i18n = ResourceBundle.getBundle(NewKarmaGenerator.class.getName());
		i18nHelp = i18n;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
				WizardPageType.METATYPE,
				WizardPageType.MAGIC_OR_RESONANCE,
				WizardPageType.TRADITION,
				WizardPageType.QUALITIES,
				WizardPageType.ATTRIBUTES,
				WizardPageType.SKILLS,
				WizardPageType.SPELLS,
				WizardPageType.ALCHEMY,
				WizardPageType.RITUALS,
				WizardPageType.POWERS,
				WizardPageType.COMPLEX_FORMS,
//				WizardPageType.BODYTECH,
//				WizardPageType.GEAR,
//				WizardPageType.VEHICLES,
				WizardPageType.NAME,
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		if (mode==Mode.FINISHED)
			return true;
		
		List<String> todos = getToDos();
//		logger.info("ToDos: "+todos);
		if (todos.isEmpty())
			return true;
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (mode==Mode.TUNING && model.getKarmaFree()>7) {
			ret.add(RES.getString("priogen.todo.maxkarma"));
		}
		if (model==null)
			return ret;
		
		if (model.getMetatype()==null) {
			ret.add(RES.getString("priogen.todo.metatype"));
		}
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}

		if (model.getName()==null) {
			ret.add(RES.getString("priogen.todo.name"));
		}

		logger.debug("ToDos: "+ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return "karma";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return "No i18n resources for "+this.getClass();
		return RUNFAST.getString("chargen.karma.title");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "chargen.karma.page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "chargen.karma.desc";
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		if (dontProcess)
			return;
		model.setKarmaFree(800);
		model.setKarmaInvested(0);
		((PriorityEquipmentController)equip).setMaxConvertibleKarma(225);
		super.runProcessors();
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null ));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#start(org.prelle.shadowrun5.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		// Stop previous
		stop();

		mode = Mode.CREATION;
		this.model = model;
		model.setKarmaFree(800);
		model.setKarmaInvested(0);
		
		logger.info("----------------Start generator-----------------------");

		meta     = new KarmaMetatypeController(this, model);
		magOrRes = new KarmaMagicOrResonanceController(this);
		attrib   = new PriorityAttributeController(this);
		skill    = new PrioritySkillController(this);
		spell    = new PrioritySpellController(this);
		alchemy  = spell;
		ritual   = (PrioritySpellController)spell;
		quality  = new CommonQualityController(this, CharGenMode.CREATING);
		power    = new CommonPowerController(this);
		cforms   = new PriorityComplexFormController(this);
		equip    = new PriorityEquipmentController(this);
		connections = new PriorityConnectionController(this);
		sins     = new CommonSINController(this);
		lifestyles  = new CommonLifestyleController(this, equip);
		summonables = new KarmaSummonableController(this);
		
		processChain.clear();
		processChain.add(new ResetModifications());
		processChain.add( new GetModificationsFromMetaType() );  // Add modifications from metatype
		processChain.add( new GetModificationsFromMagicOrResonance() );  // Add modifications from metatype
		processChain.add( new GetModificationsFromQualities() );  // Add modifications from qualities
		processChain.add( (PrioritySkillController)skill );
		processChain.add( (CommonLifestyleController)lifestyles );
		processChain.add( new ApplyAttributeModifications() );  // Add modifications for attributes
		processChain.add( new ApplySINModifications() );  // Add modifications for SINs
		processChain.add(new CalculateFreeConnectionPoints() );  
		processChain.add(new CalculateDerivedAttributes());

		
		processChain.add( ((KarmaMetatypeController)meta).getCostCalculator() );
		processChain.add( ((KarmaMagicOrResonanceController)magOrRes).getCostCalculator() );
		processChain.add(  ((PriorityAttributeController)attrib).getCostCalculator() );
		processChain.add(new CommonQualityCostCalculator() );
		processChain.add(  ((PrioritySkillController)skill).getCostCalculator() );
		processChain.add(  ((PrioritySpellController)spell).getCostCalculator() );
		processChain.add(  ((PriorityComplexFormController)cforms).getCostCalculator() );
		processChain.add(  ((PriorityEquipmentController)equip).getCostCalculator() );
		processChain.add(  ((CommonSINController)sins).getCostCalculator() );
		processChain.add(  ((CommonLifestyleController)lifestyles).getCostCalculator() );
		processChain.add(  ((PriorityConnectionController)connections).getCostCalculator() );
		processChain.add(  ((CommonPowerController)power).getCostCalculator() );
		processChain.add(  ((KarmaSummonableController)summonables).getCostCalculator() );
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		logger.info("START: Stop generation");

		try {
			if (model==null)
				return;

			/*
			 * Fix attributes
			 * Attribute modifications from metatype are included into points
			 */
			logger.debug("Sum up attribute values before finalizing");
			for (Attribute key : Attribute.values()) {
				if (key.isPrimary() || key.isSpecial()) {
					AttributeValue aVal = model.getAttribute(key);
					aVal.setStart(aVal.getPoints());
					logger.debug("..."+aVal);
				}
			}

			/*
			 * Fix skills
			 */
			logger.debug("Sum up skill values before finalizing");
			for (SkillValue val : model.getSkillValues(false)) {
				int modified = val.getModifiedValue();
				val.clearModifications();
				val.setPoints(modified);
				val.setStart(modified);
			}
			for (SkillGroupValue val : model.getSkillGroupValues()) {
				int modified = val.getModifiedValue();
				val.clearModifications();
				val.setPoints(modified);
				val.setStart(modified);
			}


			// Start-Nuyen
			// Take highest lifestyle into account
			int nuyen = Math.min(5000, model.getNuyen());
			int dice = 0;
			int factor = 0;
			for (LifestyleValue ref : model.getLifestyle()) {
				if (ref.getLifestyle().getDice()>dice) {
					dice = ref.getLifestyle().getDice();
					factor = ref.getLifestyle().getFactor();
				}
			}
			int diceValues = 0;
			Random random = new Random();
			for (int i=0; i<dice; i++) {
				diceValues += (random.nextInt(6)+1);
			}
			logger.debug("  new nuyen = "+nuyen+" + "+diceValues+" *"+factor);
			nuyen += diceValues*factor;
			model.setNuyen(nuyen);

			model.setKarmaInvested(0);

			Reward creationReward = new RewardImpl(model.getKarmaFree(), RES.getString("reward.creation"));
			creationReward.addModification(new AddNuyenModification(nuyen));
			model.addReward(creationReward);

			mode = Mode.FINISHED;
		} finally {
			logger.debug("STOP : stop generation with mode "+mode);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#startTuningMode()
	 */
	@Override
	public void startTuningMode() {
		logger.info("------Change to tuning mode----------------");
		mode = Mode.TUNING;
	}
}
