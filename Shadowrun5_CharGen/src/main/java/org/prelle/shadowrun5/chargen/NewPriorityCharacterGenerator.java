/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.RewardImpl;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.chargen.proc.CalculateFreeConnectionPoints;
import org.prelle.shadowrun5.chargen.proc.LetUserChooseProcessor;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AddNuyenModification;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.proc.ApplyAttributeModifications;
import org.prelle.shadowrun5.proc.ApplyCarriedItemModifications;
import org.prelle.shadowrun5.proc.ApplySINModifications;
import org.prelle.shadowrun5.proc.ApplySkillModifications;
import org.prelle.shadowrun5.proc.CalculateDerivedAttributes;
import org.prelle.shadowrun5.proc.CharacterProcessor;
import org.prelle.shadowrun5.proc.GetModificationsFromMagicOrResonance;
import org.prelle.shadowrun5.proc.GetModificationsFromMetaType;
import org.prelle.shadowrun5.proc.GetModificationsFromQualities;
import org.prelle.shadowrun5.proc.ResetModifications;

import de.rpgframework.ConfigChangeListener;
import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class NewPriorityCharacterGenerator extends CommonSR5CharacterGenerator {
	
	private enum Mode {
		FINISHED,
		CREATION,
		TUNING
	}
	public final static String PROP_OPTIMIZE_BUILD_POINTS = "optimize_build_points";

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	private Map<Priority,PriorityType> priorities;
	private Mode mode;
	
	private boolean dontProcess;
	private LetUserChooseProcessor callbackProc;
	private static ConfigOption<Boolean>    OPTIMIZE_BUILD_POINTS;

	//-------------------------------------------------------------------
	public NewPriorityCharacterGenerator() {
		super();

		priorities = new HashMap<Priority, PriorityType>();
		callbackProc = new LetUserChooseProcessor();
		mode = Mode.FINISHED;
		i18n = ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName());
//		i18nHelp = ShadowrunCore.getI18nHelpResources();
		logger.debug("<init> done");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		OPTIMIZE_BUILD_POINTS= (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_OPTIMIZE_BUILD_POINTS, Type.BOOLEAN, false);
		
		cfgShadowrun.addListener(new ConfigChangeListener() {
			public void configChanged(ConfigContainer source, Collection<ConfigOption<?>> options) {
				for (ConfigOption<?> opt : options) {
					if (opt==OPTIMIZE_BUILD_POINTS) {
						logger.debug("Configuration for Karma optimization changed");
						runProcessors();
					}
				}
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#setCallback(org.prelle.shadowrun5.gen.SR5LetUserChooseListener)
	 */
	@Override
	public void setCallback(SR5LetUserChooseListener callback) {
		super.setCallback(callback);
		callbackProc.setCallback(callback);
	}

	//-------------------------------------------------------------------
	public void setKarmaOptimization(boolean enabled) {
		((PrioritySkillController)skill).getCostCalculator().setKarmaOptimization(enabled);
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		if (dontProcess)
			return;
		model.setKarmaFree(25);
		model.setKarmaInvested(0);
		super.runProcessors();
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null ));
	}

	//-------------------------------------------------------------------
	public Priority getPriority(PriorityType option) {
		for (Priority prio : Priority.values()) {
			if (priorities.get(prio)==option)
				return prio;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		logger.debug("..................."+priorities);
		Priority oldPriority = getPriority(option);
		PriorityType oldOption = priorities.get(prio);
		
		if (oldOption==option && oldPriority==prio) {
			runProcessors();
			return;
		}

		logger.info("Select "+option+" with prio "+prio+"  - and set "+oldOption+" to prio "+oldPriority);
		callbackProc.clearDecisions();

		priorities.put(prio, option);
		
		switch (option) {
		case METATYPE : ((PriorityMetatypeController)meta).setPriority(prio); break;
		case ATTRIBUTE: ((PriorityAttributeController)attrib).setPriority(prio); break;
		case MAGIC    : ((PriorityMagicOrResonanceController)magOrRes).setPriority(prio); break;
		case SKILLS   : ((PrioritySkillController)skill).setPriority(prio); break;
		case RESOURCES: ((PriorityEquipmentController)equip).setPriority(prio); break;
		default:
			logger.error("Don't know what to do with prio");
		}
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, option, prio));

		if (oldPriority!=null) {
			if (oldOption==null) {
				logger.error("There was nothing set with prio "+prio+" before");
			} else {
				priorities.put(oldPriority, oldOption);
				
				switch (oldOption) {
				case METATYPE:	((PriorityMetatypeController)meta).setPriority(oldPriority); break;
				case ATTRIBUTE: ((PriorityAttributeController)attrib).setPriority(oldPriority); break;
				case MAGIC    : ((PriorityMagicOrResonanceController)magOrRes).setPriority(oldPriority); break;
				case SKILLS   : ((PrioritySkillController)skill).setPriority(oldPriority); break;
				case RESOURCES: ((PriorityEquipmentController)equip).setPriority(oldPriority); break;
				default:
					logger.error("Don't know what to do with prio "+oldOption);
				}
			}
		}
		
		runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "chargen.priority.page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "chargen.priority.desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return "No i18n resources for "+this.getClass();
		return i18n.getString("chargen.priority.title");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
				WizardPageType.PRIORITIES,
				WizardPageType.METATYPE,
				WizardPageType.MAGIC_OR_RESONANCE,
				WizardPageType.TRADITION,
				WizardPageType.QUALITIES,
				WizardPageType.ATTRIBUTES,
				WizardPageType.SKILLS,
				WizardPageType.SPELLS,
				WizardPageType.ALCHEMY,
				WizardPageType.RITUALS,
				WizardPageType.POWERS,
				WizardPageType.COMPLEX_FORMS,
//				WizardPageType.BODYTECH,
//				WizardPageType.GEAR,
//				WizardPageType.VEHICLES,
				WizardPageType.NAME,
		};
	}

	//-------------------------------------------------------------------
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (mode==Mode.TUNING && model.getKarmaFree()>7) {
			ret.add(RES.getString("priogen.todo.maxkarma"));
		}
		if (model==null)
			return ret;
		
		if (model.getMetatype()==null) {
			ret.add(RES.getString("priogen.todo.metatype"));
		}
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		ret.addAll(equip.getToDos());
//		ret.addAll(connections.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}

		if (model.getName()==null) {
			ret.add(RES.getString("priogen.todo.name"));
		}

		logger.debug("ToDos: "+ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		if (mode==Mode.FINISHED)
			return true;
		
		List<String> ret = new ArrayList<String>();
		if (mode==Mode.TUNING && model.getKarmaFree()>7) {
			ret.add(RES.getString("priogen.todo.maxkarma"));
		}
		if (model==null)
			return false;
		
		if (model.getMetatype()==null) {
			ret.add(RES.getString("priogen.todo.metatype"));
		}
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		ret.addAll(equip.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}

		if (model.getName()==null) {
			ret.add(RES.getString("priogen.todo.name"));
		}
		
		return ret.isEmpty();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#start(org.prelle.shadowrun5.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		logger.debug("start()");
		// Stop previous
		stop();

		mode = Mode.CREATION;
		this.model = model;
		model.setKarmaFree(25);
		logger.info("----------------Start generator-----------------------\n\n\n");
		
		
		magOrRes = new PriorityMagicOrResonanceController(this, callbackProc);
		meta     = new PriorityMetatypeController(this, model);
		attrib   = new PriorityAttributeController(this);
		skill    = new PrioritySkillController(this);
		((PrioritySkillController)skill).getCostCalculator().setKarmaOptimization(OPTIMIZE_BUILD_POINTS);
		spell    = new PrioritySpellController(this);
		alchemy  = spell;
		ritual   = (PrioritySpellController)spell;
		quality  = new CommonQualityController(this, CharGenMode.CREATING);
		power    = new CommonPowerController(this);
		cforms   = new PriorityComplexFormController(this);
		equip    = new PriorityEquipmentController(this);
		connections = new PriorityConnectionController(this);
		sins     = new CommonSINController(this);
		lifestyles  = new CommonLifestyleController(this, equip);
		summonables = new KarmaSummonableController(this);
		
		processChain.clear();
		processChain.add( new ResetModifications());
		processChain.add( new GetModificationsFromMetaType() );  // Add modifications from metatype
		processChain.add( new GetModificationsFromMagicOrResonance() ); 
		processChain.add( (PriorityMagicOrResonanceController)magOrRes ); // Get special mods from priority option
		processChain.add( new GetModificationsFromQualities() );  // Add modifications from qualities
		processChain.add( callbackProc);
		processChain.add( new ApplyAttributeModifications() );  // Add modifications for attributes
		processChain.add( new ApplySkillModifications() );  // Add modifications for skills
		processChain.add( new ApplyCarriedItemModifications() ); // Add equipment from modifications
		processChain.add( new CalculateFreeConnectionPoints() );  
		processChain.add( (CharacterProcessor) connections );  
		processChain.add( (PrioritySkillController)skill );
		processChain.add( new ApplySINModifications() );  // Add modifications for SINs
		processChain.add( (CommonLifestyleController)lifestyles );
		processChain.add( new CalculateDerivedAttributes());
		processChain.add( (CharacterProcessor) equip);
		// Calculate cost
		processChain.add(  ((PriorityMetatypeController)meta).getCostCalculator() );
		processChain.add(  ((PriorityAttributeController)attrib).getCostCalculator() );
		processChain.add(  ((CommonQualityController)quality).getCostCalculator() );
		processChain.add(  ((PrioritySkillController)skill).getCostCalculator() );
		processChain.add(  ((PrioritySpellController)spell).getCostCalculator() );
		processChain.add(  ((PriorityComplexFormController)cforms).getCostCalculator() );
		processChain.add(  ((PriorityEquipmentController)equip).getCostCalculator() );
		processChain.add(  ((CommonSINController)sins).getCostCalculator() );
		processChain.add(  ((CommonLifestyleController)lifestyles).getCostCalculator() );
		processChain.add(  ((PriorityConnectionController)connections).getCostCalculator() );
		processChain.add(  ((CommonPowerController)power).getCostCalculator() );
		processChain.add(  ((KarmaSummonableController)summonables).getCostCalculator() );

//		GenerationEventDispatcher.addListener((PriorityMagicOrResonanceGenerator)magOrRes);
		GenerationEventDispatcher.addListener(meta);
//		GenerationEventDispatcher.addListener((PriorityAttributeGenerator)attrib);
//		GenerationEventDispatcher.addListener((PrioritySkillGenerator)skill);
//		GenerationEventDispatcher.addListener((PrioritySpellGenerator)spell);
//		GenerationEventDispatcher.addListener((GenerationEventListener)quality);
//		GenerationEventDispatcher.addListener((GenerationEventListener)power);
//		GenerationEventDispatcher.addListener((PriorityComplexFormGenerator)cforms);
//		GenerationEventDispatcher.addListener((GenerationEventListener)equip);
//		GenerationEventDispatcher.addListener((PriorityConnectionGenerator)connections);
//		GenerationEventDispatcher.addListener((CommonSINController)sins);
////		GenerationEventDispatcher.addListener((CommonLifestyleController)lifestyles);
//		GenerationEventDispatcher.addListener((KarmaSummonableController)summonables);
//		GenerationEventDispatcher.addListener(this);


		dontProcess = true;
		setPriority(PriorityType.METATYPE , Priority.A);
		setPriority(PriorityType.ATTRIBUTE, Priority.B);
		setPriority(PriorityType.MAGIC    , Priority.C);
		setPriority(PriorityType.SKILLS   , Priority.D);
		setPriority(PriorityType.RESOURCES, Priority.E);
		dontProcess = false;

//		((MetatypeGenerator)meta).updateAvailable();
		logger.debug("start done");
		
		runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#startTuningMode()
	 */
	@Override
	public void startTuningMode() {
		logger.info("------Change to tuning mode----------------");
		mode = Mode.TUNING;
//		((PrioritySkillController)skill).getCostCalculator().setKarmaOptimization(false);
		
		/*
		 * All points given from priorities are set to start points
		 */
		for (SkillValue sVal : model.getSkillValues(false)) {
			sVal.setStart(sVal.getModifiedValue());
			logger.info("Skill "+sVal.getName()+" started at "+sVal.getStart());
		}
		for (SkillGroupValue sVal : model.getSkillGroupValues()) {
			sVal.setStart(sVal.getModifiedValue());
			logger.info("Skill "+sVal.getModifyable().getName()+" started at "+sVal.getStart());
		}
		
		/*
		 * Switch to paying with karma now
		 */
		((PrioritySkillController)skill).setPriority(null);
		
//		undoList  = new ArrayList<Modification>();
//		GenerationEventDispatcher.removeListener((PriorityMagicOrResonanceGenerator)magOrRes);
//		GenerationEventDispatcher.removeListener(meta);
//		GenerationEventDispatcher.removeListener((PriorityAttributeGenerator)attrib);
//		GenerationEventDispatcher.removeListener((PrioritySkillGenerator)skill);
//		GenerationEventDispatcher.removeListener((PrioritySpellGenerator)spell);
////		GenerationEventDispatcher.removeListener((QualityGenerator)quality);
//		GenerationEventDispatcher.removeListener((GenerationEventListener)power);
//		GenerationEventDispatcher.removeListener((PriorityComplexFormGenerator)cforms);
//
//		attrib = new KarmaAttributeController(model, undoList);
//		skill  = new KarmaSkillController(model, CharGenMode.CREATING, undoList);
//		spell  = new KarmaSpellController(model, CharGenMode.CREATING, undoList, false);
//		alchemy= new KarmaSpellController(model, CharGenMode.CREATING, undoList, true);
//		ritual = new KarmaRitualController(model, CharGenMode.CREATING, undoList);
//		cforms = new KarmaComplexFormController(model, CharGenMode.CREATING, undoList);
////		logger.warn("TODO: switch other controller");
//
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONTROLLER_CHANGED, this));
		
		runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		logger.info("Stop generation");

//		GenerationEventDispatcher.removeListener((GenerationEventListener)equip);
//		GenerationEventDispatcher.removeListener((KarmaSummonableController)summonables);
//		GenerationEventDispatcher.removeListener(this);

		if (model==null)
			return;

		/*
		 * Fix attributes
		 */
		logger.debug("Sum up attribute values before finalizing");
		for (Attribute key : Attribute.values()) {
			if (key.isPrimary() || key.isSpecial()) {
				AttributeValue aVal = model.getAttribute(key);
				switch (key) {
				case ESSENCE:
					aVal.setPoints(aVal.getModifiedValue());
					break;
				case MAGIC: case RESONANCE:
					// Ignore modifications from quality
					int modPoints = 0;
					for (Modification mod : new ArrayList<Modification>(aVal.getModifications())) {
						AttributeModification aMod = (AttributeModification)mod;
						if (mod.getSource() instanceof MagicOrResonanceType) {
							modPoints += aMod.getValue();
							aVal.removeModification(mod);
						} else {
							logger.debug("Dont add "+aMod+" to permanent values");
							continue;
						} 
					}
					aVal.setPoints(modPoints);
					break;
				default:
				}
//				int modified = aVal.getModifiedValue();
//				aVal.clearModifications();
//				aVal.setPoints(modified);
				aVal.setStart(aVal.getPoints());
				logger.debug("..."+aVal);
			}
		}

		/*
		 * Fix skills
		 */
		logger.debug("Sum up skill values before finalizing");
		for (SkillValue val : model.getSkillValues(false)) {
			int modified = val.getModifiedValue();
			val.clearModifications();
			val.setPoints(modified);
			val.setStart(modified);
		}
		for (SkillGroupValue val : model.getSkillGroupValues()) {
			int modified = val.getModifiedValue();
			val.clearModifications();
			val.setPoints(modified);
			val.setStart(modified);
		}

		// Start-Nuyen
		// Take highest lifestyle into account
		int nuyen = Math.min(5000, model.getNuyen());
		int dice = 0;
		int factor = 0;
		for (LifestyleValue ref : model.getLifestyle()) {
			if (ref.getLifestyle().getDice()>dice) {
				dice = ref.getLifestyle().getDice();
				factor = ref.getLifestyle().getFactor();
			}
		}
		int diceValues = 0;
		Random random = new Random();
		for (int i=0; i<dice; i++) {
			diceValues += (random.nextInt(6)+1);
		}
		logger.debug("  new nuyen = "+nuyen+" + "+diceValues+" *"+factor);
		nuyen += diceValues*factor;
		model.setNuyen(nuyen);

		model.setKarmaInvested(0);

		Reward creationReward = new RewardImpl(model.getKarmaFree(), RES.getString("reward.creation"));
		creationReward.addModification(new AddNuyenModification(nuyen));
		model.addReward(creationReward);
		
		mode = Mode.FINISHED;
		
		priorities.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return "priority";
	}

}
