/**
 *
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.chargen.cost.PriorityComplexFormCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PriorityComplexFormController implements ComplexFormController {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;

	private List<ComplexForm> recommended;

	private List<ComplexForm> availableComplexForms;
	private PriorityComplexFormCalculator costs;

	private int points;

	//-------------------------------------------------------------------
	public PriorityComplexFormController(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		availableComplexForms   = new ArrayList<ComplexForm>();
		recommended = new ArrayList<ComplexForm>();
		updateAvailableComplexForms();
		
		costs = new PriorityComplexFormCalculator();
	}

	//-------------------------------------------------------------------
	public PriorityComplexFormCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getParentController()
	 */
//	@Override
//	public CharacterController getParentController() {
//		return parent;
//	}

	//--------------------------------------------------------------------
	private void updateAvailableComplexForms() {
		logger.debug("START updateAvailableComplexForms");
		availableComplexForms.clear();
		availableComplexForms.addAll(ShadowrunCore.getComplexForms());
		// Remove those the character already has
		for (ComplexFormValue val : model.getComplexForms()) {
			logger.trace("Not available anymore "+val);
			availableComplexForms.remove(val.getModifyable());
		}

		Collections.sort(availableComplexForms);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.COMPLEX_FORMS_AVAILABLE_CHANGED, availableComplexForms));
		logger.debug("STOP updateAvailableComplexForms");
	}

	//-------------------------------------------------------------------
	public void setData(int ptSkills) {
		points      = ptSkills;
	}

	//--------------------------------------------------------------------
	private int getPointsInComplexForms() {
		return model.getComplexForms().size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getComplexFormsLeft()!=0) {
			ret.add(String.format(RES.getString("cmplxformgen.todo.normal"), getComplexFormsLeft()));
		}

		return ret;
	}

	//-------------------------------------------------------------------	
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getComplexFormsLeft()
	 */
	@Override
	public int getComplexFormsLeft() {
		return costs.getComplexFormsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getAvailableComplexForms()
	 */
	@Override
	public List<ComplexForm> getAvailableComplexForms() {
		return availableComplexForms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeSelected(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public boolean canBeSelected(ComplexForm data) {
		if (data==null)
			throw new NullPointerException();

		// Is it already selected?
		for (ComplexFormValue selected : model.getComplexForms()) {
			if (selected.getModifyable()==data)
				return false;
		}

		// Are there points left
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeDeselected(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public boolean canBeDeselected(ComplexFormValue data) {
		return model.getComplexForms().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#select(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return null;
		}
		logger.info("select "+data);

		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);
		parent.runProcessors();

		updateAvailableComplexForms();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#deselect(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public void deselect(ComplexFormValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return;
		}
		logger.debug("select "+data);

		model.removeComplexForm(data);
		parent.runProcessors();

		updateAvailableComplexForms();
	}

}
