/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.chargen.cost.ConnectionCostCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityConnectionController implements ConnectionsController, CharacterProcessor {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	protected CommonSR5CharacterGenerator parent;
	protected ShadowrunCharacter model;

	private ConnectionCostCalculator costs;
	
	//-------------------------------------------------------------------
	/**
	 */
	public PriorityConnectionController(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		
		costs = new ConnectionCostCalculator();
	}

	//-------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return costs.getPointsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canCreateConnection()
	 */
	@Override
	public boolean canCreateConnection() {
		if (getPointsLeft()<2)
			return false;
				
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#createConnection()
	 */
	@Override
	public Connection createConnection() {
		if (!canCreateConnection()) {
			logger.warn("Trying to create new connection which is not allowed");
			return null;
		}
		
		Connection ref =  new Connection("?", "?", 1, 1);
		// Add connection
		model.addConnection(ref);		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_ADDED, ref));
		
		// Recalc
		parent.runProcessors();
		
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#removeConnection(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public void removeConnection(Connection con) {
		model.removeConnection(con);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	private boolean allowHighFriends() {
		return costs.getHighPointsLeft()>0;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseInfluence(Connection con) {
		if (con.getInfluence()>=12)
			return false;
		if (con.getLoyalty()>=6)
			return false;
		
		if ( ((con.getInfluence()+con.getLoyalty()>=7) && allowHighFriends() && costs.getHighPointsLeft()<=0))
			return false;
		
		if (parent.getMode()==CharGenMode.CREATING)
			return getPointsLeft()>0;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseInfluence(Connection con) {
		if (!canIncreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseInfluence(Connection con) {
		return con.getInfluence()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseInfluence(Connection con) {
		if (!canDecreaseInfluence(con))
			return false;
		
		if (con.getInfluence()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setInfluence(con.getInfluence()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseLoyalty(Connection con) {
		if (con.getLoyalty()>=6)
			return false;
		
//		if ( (con.getLoyalty()+con.getLoyalty()>=7))
//			return false;
		
		if (parent.getMode()==CharGenMode.CREATING)
			return getPointsLeft()>0;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseLoyalty(Connection con) {
		if (!canIncreaseLoyalty(con))
			return false;
		
		logger.info("Increase loyalty of "+con);
		con.setLoyalty(con.getLoyalty()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseLoyalty(Connection con) {
		return con.getLoyalty()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseLoyalty(Connection con) {
		if (!canDecreaseLoyalty(con))
			return false;
		
		if (con.getLoyalty()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setLoyalty(con.getLoyalty()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (getPointsLeft()>0) {
			ret.add(String.format(RES.getString("priogen.todo.connection"), getPointsLeft()));
		}
		
		// Check connections are in range
		for (Connection con : model.getConnections()) {
			if (con.getInfluence()>6 && !allowHighFriends())
				ret.add(String.format(RES.getString("priogen.todo.connection.highinfluence"), con.getName()));
			else if ( (con.getLoyalty()+con.getInfluence())>7 && !allowHighFriends())
				ret.add(String.format(RES.getString("priogen.todo.connection.hightotal"), con.getName()));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();

//		try {
//			allowHighFriends = false;
//			int highPointsLeft = 0; // Only investable for connections with rating 8+
//			for (Modification mod : previous) {
//				if (mod instanceof SpecialRuleModification) {
//					switch (((SpecialRuleModification)mod).getRule()) {
//					case FRIENDS_IN_HIGH_PLACES:
//						logger.debug("consume "+mod);
//						highPointsLeft += model.getAttribute(Attribute.CHARISMA).getModifiedValue() * 4;
//						allowHighFriends=true;
//						break;
//					default:
//						unprocessed.add(mod);
//					}
//				} else
//					unprocessed.add(mod);
//			}
//			costs.setHighPointsLeft(highPointsLeft);
//			logger.info("Points to spend for connections with influence 8+: "+highPointsLeft);
//
//		} finally {
//			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
//		}
		return previous;
	}

}
