/**
 *
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.chargen.cost.PriorityEquipmentCostCalculator;
import org.prelle.shadowrun5.common.CommonEquipmentController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.modifications.SpecialRuleModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class PriorityEquipmentController extends CommonEquipmentController implements CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR5CharacterGenerator parent;
	private PriorityEquipmentCostCalculator costs;

	private Priority priority;
	/**
	 * Karma points converted to Nuyen
	 */
	private int usedKarma;
	
	private int maxConvertibleKarma;

	//--------------------------------------------------------------------
	public PriorityEquipmentController(CommonSR5CharacterGenerator parent) {
		super(parent.getCharacter(), CharGenMode.CREATING);
		this.parent = parent;
		costs = new PriorityEquipmentCostCalculator();
	}

	//-------------------------------------------------------------------
	public PriorityEquipmentCostCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	public void setMaxConvertibleKarma(int val) {
		this.maxConvertibleKarma = val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		/*
		 * Find all items that are above availability 12 and require the 
		 * quality "restricted_gear"
		 */
		for (CarriedItem item : model.getItems(false)) {
			Availability avail = item.getAvailability();
			if (avail.getValue()>12) {
				ret.add( String.format(RES.getString("equip.todo.restricted_gear"), item.getName(), avail.toString()));
			}
		}
		
		ret.addAll(costs.getToDos());
		return ret;
	}

	//-------------------------------------------------------------------
	public void setPriority(Priority prio) {
		logger.debug("Set priority to "+prio);
		this.priority = prio;
		costs.setPriority(prio);
//		if (prio!=Priority.E) {
//			logger.fatal("STOP HERE++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
//			System.err.println("---------------------------------------------------------");
////			System.exit(0);
//		}
	}

	//--------------------------------------------------------------------
	public boolean canIncreaseBoughtNuyen() {
		return model.getKarmaFree()>0 && usedKarma<maxConvertibleKarma;
	}

	//--------------------------------------------------------------------
	public boolean canDecreaseBoughtNuyen() {
		return usedKarma>0;
	}

	//--------------------------------------------------------------------
	public void increaseBoughtNuyen() {
		logger.debug("Increase");
		if (!canIncreaseBoughtNuyen())
			return;
		
		logger.trace("Generate 2000 nuyen for 1 karma");
		usedKarma++;
		costs.setInvestedKarma(usedKarma);
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	public void decreaseBoughtNuyen() {
		if (!canDecreaseBoughtNuyen())
			return;
		
		logger.info("Return 2000 nuyen for 1 karma");
		usedKarma--;
		costs.setInvestedKarma(usedKarma);
		parent.runProcessors();
	}

//	//--------------------------------------------------------------------
//	public void updateNuyen() {
//		calcFree = baseNuyen;
//		calcFree+= usedKarma*2000;
//
//		// Calculate worth of all gear
//		calcInvested = ShadowrunTools.calculateInvestedNuyen(model);
//
//		logger.debug(calcInvested+" of "+calcFree+"\u00A5 invested");
//		calcFree -= calcInvested;
//		model.setNuyen(calcFree);
//
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, calcFree, calcInvested));
//	}

	//-------------------------------------------------------------------
	public int getBoughtNuyen() {
		return usedKarma;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#select(org.prelle.shadowrun5.items.ItemTemplate, int)
	 */
	@Override
	public CarriedItem select(ItemTemplate item, SelectionOption...options) {
		CarriedItem ret = super.select(item, options);
		parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook hook, SelectionOption...options) {
		CarriedItem ret = super.embed(container, item, hook, options);
		parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options) {
		CarriedItem ret = super.embed(container, item, options);
		parent.runProcessors();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.common.CommonEquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.Program, org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption[])
	 */
	@Override
	public ProgramValue embed(CarriedItem container, Program program, SelectionOption... options) {
		ProgramValue ret = super.embed(container, program, options);
		parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#deselect(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		boolean removed = super.deselect(ref);
		parent.runProcessors();
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#sell(org.prelle.shadowrun5.items.CarriedItem, float)
	 */
	@Override
	public void sell(CarriedItem ref, float factor) {
		super.sell(ref, factor);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#increase(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		if (!super.increase(data))
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#decrease(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		if (!super.decrease(data))
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Filter modification for convertible nuyen
			maxConvertibleKarma = (priority==null)?225:10;
			for (Modification mod : previous) {
				if (mod instanceof SpecialRuleModification) {
					SpecialRuleModification free = (SpecialRuleModification)mod;
					if (free.getRule()==SpecialRuleModification.Rule.KARMA_TO_NUYEN) {
						logger.debug("  add "+free.getLevel()+" allowed karma for conversion");
						maxConvertibleKarma += free.getLevel();
						continue;
					} 
				}
				unprocessed.add(mod);
			}
			costs.setKarmaInvestLimit(maxConvertibleKarma);
			logger.debug("max convertible karma is "+maxConvertibleKarma);

		} finally {
			logger.trace("STOP : process()");
		}
		
		return unprocessed;
	}

}
