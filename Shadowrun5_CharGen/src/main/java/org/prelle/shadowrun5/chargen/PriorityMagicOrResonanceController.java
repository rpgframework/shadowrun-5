/**
 *
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.chargen.cost.PriorityMagicOrResonanceCostCalculator;
import org.prelle.shadowrun5.chargen.proc.LetUserChooseProcessor;
import org.prelle.shadowrun5.common.DerivedAttributeCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.ModificationChoice;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMagicOrResonanceController implements MagicOrResonanceController, CharacterProcessor {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	private LetUserChooseProcessor callback;
	
	private PriorityMagicOrResonanceCostCalculator costs;

	private MagicOrResonanceOption current;
	private List<Modification> applyLater;

	private List<MagicOrResonanceOption> available;
	private Priority priority;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityMagicOrResonanceController(CommonSR5CharacterGenerator parent, LetUserChooseProcessor callback) {
		this.parent = parent;
		this.callback = callback;
		available = new ArrayList<>();
		model = parent.getCharacter();
		DerivedAttributeCalculator calc = new DerivedAttributeCalculator();
		calc.setData(model);
		applyLater = new ArrayList<>();
		
		costs = new PriorityMagicOrResonanceCostCalculator();
	}

	//--------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}
	
	//-------------------------------------------------------------------
	public void setPriority(Priority priority) {
		available.clear();
		this.priority = priority;
		PriorityTableEntry  entry = ShadowrunCore.getPriorityTableEntry(PriorityType.MAGIC, priority);
		logger.debug("  updateAvailable with "+entry);
		if (entry==null)
			return;
		for (PriorityOption option : entry) {
			if (option instanceof MagicOrResonanceOption) {
				available.add((MagicOrResonanceOption)option);
			}
		}
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MAGICORRESONANCE_AVAILABLE_CHANGED, available));
		costs.setPriority(entry);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getAvailable()
	 */
	@Override
	public List<MagicOrResonanceOption> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#canBeSelected(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public boolean canBeSelected(MagicOrResonanceOption type) {
		return available.contains(type);
	}

	//-------------------------------------------------------------------
	private List<Modification> getApplyLater(MagicOrResonanceOption opt) {
		List<Modification> ret = new ArrayList<>();
		for (Modification mod : opt.getModifications()) {
			if (mod instanceof ModificationChoice)
				ret.add(mod);
			else if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getType()!=null && sMod.getSkill()==null)
					ret.add(mod);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private List<Modification> getApplyImmediately(MagicOrResonanceOption opt) {
		List<Modification> ret = new ArrayList<>(opt.getModifications());
		ret.removeAll(getApplyLater(opt));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#select(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public void select(MagicOrResonanceOption value) {
		if (value==current)
			return;

		if (value==null)
			return;
		logger.debug("clear old decisions");
		callback.clearDecisions();
		logger.info("Select "+value.getType());
		// Change
		model.setMagicOrResonanceType(value.getType());
		current = value;
		// Apply later in applyFromSelection()
		parent.apply(value.getType().getName(), getApplyImmediately(value));
		applyLater = getApplyLater(value);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MAGICORRESONANCE_CHANGED, value));
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (model.getMagicOrResonanceType()==null) {
			ret.add(RES.getString("magicgen.todo"));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getToApplyFromSelection()
	 */
	@Override
	public List<Modification> getToApplyFromSelection() {
		logger.debug("------------APPLY LATE "+applyLater);
//		parent.apply(current.getType().getName(), applyLater);
		return applyLater;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Find matching option for type
			for (MagicOrResonanceOption opt : available) {
				if (opt.getType()==model.getMagicOrResonanceType()) {
					logger.info("Apply options from "+model.getMagicOrResonanceType()+": "+opt.getModifications());
					unprocessed.addAll(opt.getModifications());
					
					switch (priority) {
					case A:
						switch (opt.getType().getId()) {
						case "magician"   :unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 10)); break;
						case "mysticadept":unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 10)); break;
						case "technomancer":unprocessed.add(new FreePointsModification(Type.COMPLEX_FORMS, 5)); break;
						}
						break;
					case B:
						switch (opt.getType().getId()) {
						case "magician"   :unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 7)); break;
						case "mysticadept":unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 7)); break;
						case "technomancer":unprocessed.add(new FreePointsModification(Type.COMPLEX_FORMS, 2)); break;
						}
						break;
					case C:
						switch (opt.getType().getId()) {
						case "magician"   :unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 5)); break;
						case "mysticadept":unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, 5)); break;
						case "technomancer":unprocessed.add(new FreePointsModification(Type.COMPLEX_FORMS, 1)); break;
						}
						break;
					default:
					}
					break;
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
