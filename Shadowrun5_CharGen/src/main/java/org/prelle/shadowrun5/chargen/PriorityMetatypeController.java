/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MetatypeController;
import org.prelle.shadowrun5.chargen.cost.PriorityMetatypeCostCalculator;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.proc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public class PriorityMetatypeController implements MetatypeController {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	
	private PriorityMetatypeCostCalculator costs;
	
	private List<MetaTypeOption> availableOptions;
	private List<MetaType> availableTypes;
	
	private PriorityTableEntry tableEntry;

	//-------------------------------------------------------------------
	public PriorityMetatypeController(CommonSR5CharacterGenerator parent, ShadowrunCharacter model) {
		this.parent= parent;
		this.model = model;
		availableOptions  = new ArrayList<MetaTypeOption>();
		availableTypes = new ArrayList<MetaType>();
		
		costs = new PriorityMetatypeCostCalculator();
	}

	//--------------------------------------------------------------------
	public CharacterProcessor getCostCalculator() {
		return costs;
	}

	//--------------------------------------------------------------------
	void updateAvailable() {
		logger.debug("START updateAvailable");
		availableOptions.clear();
		availableTypes.clear();
		logger.debug("  updateAvailable with "+tableEntry);
		if (tableEntry==null)
			return;
		for (PriorityOption tmp : tableEntry) {
			if (tmp instanceof MetaTypeOption) {
				availableOptions.add( ((MetaTypeOption)tmp) );
				availableTypes.add( ((MetaTypeOption)tmp).getType());
			}
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.METATYPES_AVAILABLE_CHANGED, availableOptions, availableTypes));
		logger.debug("STOP  updateAvailable");
	}

	//--------------------------------------------------------------------
	public void setPriority(Priority prio) {
		logger.info("  setPriority("+prio+")");
		tableEntry = ShadowrunCore.getPriorityTableEntry(PriorityType.METATYPE, prio);
		costs.setPriority(tableEntry);
		updateAvailable();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getAvailable()
	 */
	@Override
	public List<MetaTypeOption> getAvailable() {
		return availableOptions;
	}

	//--------------------------------------------------------------------
	private MetaTypeOption getOptionByMetaType(MetaType type) {
		for (MetaTypeOption tmp : availableOptions)
			if (tmp.getType()==type)
				return tmp;
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getSpecialAttributes(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public int getSpecialAttributes(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option!=null)
			return option.getSpecialAttributePoints();
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getKarmaCost(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public int getKarmaCost(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option!=null)
			return option.getAdditionalKarmaKost();
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#canBeSelected(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public boolean canBeSelected(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option==null)
			return false;
		
		logger.debug("canBeSelected("+type+") = "+model.getKarmaFree()+" >= "+option.getAdditionalKarmaKost());
		return model.getKarmaFree()>=option.getAdditionalKarmaKost();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#select(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public void select(MetaType value) {
		logger.info("Select "+value);
		if (!canBeSelected(value)) {
			logger.warn("Cannot select "+value);
			return;
		}

		model.setMetatype(value);
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.METATYPE_CHANGED, value));
		
		parent.runProcessors();
	}

}
