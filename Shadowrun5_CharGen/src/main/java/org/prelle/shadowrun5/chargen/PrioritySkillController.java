/**
 *
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.chargen.cost.PrioritySkillCostCalculator;
import org.prelle.shadowrun5.common.CommonSkillController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class PrioritySkillController extends CommonSkillController implements SkillController, CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private PrioritySkillCostCalculator costs;
	private Priority prio;

	//--------------------------------------------------------------------
	/**
	 */
	public PrioritySkillController(CommonSR5CharacterGenerator parent) {
		super(parent);
		model = parent.getCharacter();
		avSkillGroups  = new ArrayList<SkillGroup>();
		avSkills       = new ArrayList<Skill>();
		recSkillGroups = new ArrayList<SkillGroup>();
		recSkills      = new ArrayList<Skill>();
		
		costs = new PrioritySkillCostCalculator();
		updateAvailable();
	}

	//-------------------------------------------------------------------
	public PrioritySkillCostCalculator getCostCalculator() {
		return costs;
	}

	//-------------------------------------------------------------------
	public void setPriority(Priority prio) {
		this.prio = prio;
		costs.setPriority(prio);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

//	//--------------------------------------------------------------------
//	public int getPointsInKnowledgeAndLanguage() {
//		int sum = 0;
//		for (SkillValue sVal : model.getSkillValues(false)) {
//			Skill skill = sVal.getModifyable();
//			if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
//				if (skill.getId().startsWith("native_language"))
//					continue;
//				sum += sVal.getPoints();
//				sum += sVal.getSkillSpecializations().size();
//			}
//		}
//		return sum;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftInKnowledgeAndLanguage()
	 */
	@Override
	public int getPointsLeftInKnowledgeAndLanguage() {
		return costs.getPointsLeftInKnowledgeAndLanguage();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkillGroups()
	 */
	@Override
	public int getPointsLeftSkillGroups() {
		return costs.getPointsLeftSkillGroups();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkills()
	 */
	@Override
	public int getPointsLeftSkills() {
		return costs.getPointsLeftSkills();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getPointsLeftSkills()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.normal"), getPointsLeftSkills()));
		}
		if (getPointsLeftSkillGroups()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.groups"), getPointsLeftSkillGroups()));
		}
		if (getPointsLeftInKnowledgeAndLanguage()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.knowledge"), getPointsLeftInKnowledgeAndLanguage()));
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public boolean canBeSelected(SkillGroup grp) {
		// Not already selected
		if (model.getSkillGroups().contains(grp))
			return false;
		// Points left
		return getPointsLeftSkillGroups()>0 || costs.getIncreaseCost(model,grp)<=model.getKarmaFree();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public boolean canBeSelected(Skill skill) {
		if (skill.getType()==SkillType.LANGUAGE || skill.getType()==SkillType.KNOWLEDGE)
			return canBeSelectedKnowledgeOrLanguage(skill);
		
		// Not already selected
		if (model.getSkills(false).contains(skill)) {
			return false;
		}
		// Points left
		return getPointsLeftSkills()>0 || costs.getIncreaseCost(model,skill)<=model.getKarmaFree();
	}

	//--------------------------------------------------------------------
	public boolean canBeSelectedKnowledgeOrLanguage(Skill skill) {
		// Only check knowledge and language skills here
		if (skill.getType()!=SkillType.KNOWLEDGE && skill.getType()!=SkillType.LANGUAGE) {
			return false;
		}

		boolean isNative1 = skill.getId().equals("native_language");
		boolean isNative2 = skill.getId().equals("native_language2");

		// Native language may ony be selected once
		if (isNative1) {
			return !(model.getSkills(false).contains(skill));
		}
		// Native language 2 may ony be selected once AND if the needed quality exists
		if (isNative2) {
			logger.warn("CHECK FOR Quality Bilingual");
			return !(model.getSkills(false).contains(skill));
		}

		// Points left
		return (getPointsLeftInKnowledgeAndLanguage()>0 || getPointsLeftSkills()>0);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#selectKnowledgeOrLanguage(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName) {
		logger.warn("TODO: select "+skill.getId()+"/"+customName);

		if (!canBeSelectedKnowledgeOrLanguage(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		// Change model
		SkillValue sVal = new SkillValue(skill, 1);
		sVal.setName(customName);
		logger.info("Selected skill "+sVal);
		model.addSkill(sVal);

		updateAvailable();
		parent.runProcessors();

		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDeselected(SkillGroupValue ref) {
		return model.getSkillGroupValues().contains(ref);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref) {
		return model.getSkillValues(false).contains(ref);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeIncreased(SkillValue ref) {
		// Maximum not reached yet
		if (ref.getModifiedValue()>=6)
			return false;
		
		boolean enoughKarma = model.getKarmaFree() >= ((ref.getModifiedValue()+1)*5); 
		if (ref.getModifiedValue()>=6 && !enoughKarma)
			return false;

		// Enough points
		SkillType type = ref.getModifyable().getType();
		if (type!=SkillType.KNOWLEDGE && type!=SkillType.LANGUAGE) {
			if (getPointsLeftSkills()<=0 && costs.getIncreaseCost(model,ref.getModifyable())>model.getKarmaFree()) {
					return false;
			}
		} else {
			// Is language or knowledge
			return getPointsLeftInKnowledgeAndLanguage()>0 || costs.getIncreaseCost(model,ref.getModifyable())<=model.getKarmaFree();
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeIncreased(SkillGroupValue ref) {
		// Enough points
		boolean enoughKarma = model.getKarmaFree() >= ((ref.getModifiedValue()+1)*5); 
		if (getPointsLeftSkillGroups()<=0 && !enoughKarma)
			return false;
		// Maximum not reached yet
		if (ref.getModifiedValue()>=6)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDecreased(SkillGroupValue ref) {
		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeDecreased(SkillValue ref) {
		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec) {
		// Not already selected
		if (ref.hasSpecialization(spec))
			return false;
		// Ref belongs to character
		if (!model.getSkillValues(false).contains(ref))
			return false;
		// At generation only one specialization per skill
		if (ref.getSkillSpecializations().size()>0)
			return false;
		// Points left
		return getPointsLeftSkills()>0 || model.getKarmaFree()>0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec) {
		// Not already selected
		if (!ref.hasSpecialization(spec))
			return false;
		// Ref belongs to character
		if (!model.getSkillValues(false).contains(ref))
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * Do enough skillpoints exist to break group
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBreakSkillGroup(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBreakSkillGroup(SkillGroupValue ref) {
		if (prio!=null)
			return false;
		if (ref.getModifier()>0)
			return false;

//		int pointsNecessary = 0;
//		for (@SuppressWarnings("unused") Skill skill : ShadowrunCore.getSkills(ref.getModifyable()))
//			pointsNecessary += ref.getPoints();
//
//		return getPointsLeftSkills()>=pointsNecessary;
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canSpecializeIn(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canSpecializeIn(SkillValue val) {
		return !val.getModifyable().getSpecializations().isEmpty() && model.getKarmaFree()>6;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean increase(SkillGroupValue ref) {
		boolean ret = super.increase(ref);
		if (ret)
			parent.runProcessors();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.common.CommonSkillController#decrease(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean decrease(SkillGroupValue ref) {
		boolean ret = super.decrease(ref);
		if (ret)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		boolean ret = super.increase(ref);
		if (ret)
			parent.runProcessors();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.common.CommonSkillController#decrease(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		boolean ret = super.decrease(ref);
		if (ret)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public SkillValue select(Skill skill) {
		SkillValue ret = super.select(skill);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public SkillGroupValue select(SkillGroup grp) {
		SkillGroupValue ret = super.select(grp);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.common.CommonSkillController#select(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec) {
		SkillSpecializationValue ret = super.select(ref, spec);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			for (Modification tmp : previous) {
				if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					SkillValue sval = model.getSkillValueExisting(mod.getSkill());
					if (sval==null) {
						sval = new SkillValue(mod.getSkill(), 0);
						model.addSkill(sval);
					}
					sval.addModification(mod);
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}
		
}
