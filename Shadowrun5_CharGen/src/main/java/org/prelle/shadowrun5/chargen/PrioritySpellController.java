/**
 * 
 */
package org.prelle.shadowrun5.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Ritual;
import org.prelle.shadowrun5.RitualValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.Tradition;
import org.prelle.shadowrun5.Spell.Category;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.chargen.cost.PrioritySpellCostCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PrioritySpellController implements SpellController, RitualController {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	private PrioritySpellCostCalculator costs;
	
//	private List<Spell> recommended;
	
	private List<Spell> availableSpells;
	private List<Spell> availableAlchemy;
	private List<Ritual> availableRituals;

	//-------------------------------------------------------------------
	public PrioritySpellController(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		availableSpells   = new ArrayList<Spell>();
		availableAlchemy  = new ArrayList<Spell>();
		availableRituals  = new ArrayList<Ritual>();
//		recommended = new ArrayList<Spell>();
		costs = new PrioritySpellCostCalculator();
		updateAvailableSpells();
		updateAvailableRituals();
	}

	//-------------------------------------------------------------------
	public PrioritySpellCostCalculator getCostCalculator() {
		return costs;
	}

	//--------------------------------------------------------------------
	private void updateAvailableSpells() {
		logger.debug("START updateAvailableSpells");
		availableSpells.clear();
		availableSpells.addAll(ShadowrunCore.getSpells());
		availableAlchemy.clear();
		availableAlchemy.addAll(ShadowrunCore.getSpells());
		// Remove those the character already has
		for (SpellValue val : model.getSpells()) {
			logger.trace("Not available anymore "+val);
			if (val.isAlchemistic())
				availableAlchemy.remove(val.getModifyable());
			else
				availableSpells.remove(val.getModifyable());
		}
		
		Collections.sort(availableSpells);
		Collections.sort(availableAlchemy);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.SPELLS_AVAILABLE_CHANGED, availableSpells, availableAlchemy));
		logger.debug("STOP updateAvailableSpells");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getSpellsLeft()!=0 && model.getMagicOrResonanceType().usesSpells()) { 
			ret.add(String.format(RES.getString("spellgen.todo.normal"), getSpellsLeft()));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private int getTotalSpellsAndRituals() {
		return model.getRituals().size()+model.getSpells().size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return costs.getSpellsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return alchemistic?availableAlchemy:availableSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun5.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Category category, boolean alchemistic) {
		List<Spell> ret = new ArrayList<>();
		for (Spell tmp : alchemistic?availableAlchemy:availableSpells)
			if (tmp.getCategory()==category)
				ret.add(tmp);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeSelected(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell data, boolean alchemistic) {
		if (getTotalSpellsAndRituals()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()*2)
			return false;
		return getAvailableSpells(alchemistic).contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeDeselected(org.prelle.shadowrun5.SpellValue)
	 */
	@Override
	public boolean canBeDeselected(SpellValue data) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#select(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public SpellValue select(Spell data, boolean alchemistic) {
		if (!canBeSelected(data, alchemistic)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		SpellValue ref = new SpellValue(data);
		ref.setAlchemistic(alchemistic);
		model.addSpell(ref);
		logger.debug("send event "+data);
		parent.runProcessors();
		
		updateAvailableSpells();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#deselect(org.prelle.shadowrun5.SpellValue)
	 */
	@Override
	public void deselect(SpellValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a spell that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeSpell(data);
		parent.runProcessors();
		
		updateAvailableSpells();
	}

	//--------------------------------------------------------------------
	private int getPointsInRituals() {
		return model.getRituals().size();
	}

	//--------------------------------------------------------------------
	private void updateAvailableRituals() {
		logger.debug("START updateAvailableRituals");
		availableRituals.clear();
		availableRituals.addAll(ShadowrunCore.getRituals());
		// Remove those the character already has
		for (RitualValue val : model.getRituals()) {
			logger.trace("Not available anymore "+val);
			availableRituals.remove(val.getModifyable());
		}
		
		Collections.sort(availableRituals);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.RITUALS_AVAILABLE_CHANGED, availableRituals));
		logger.debug("STOP updateAvailableRituals");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getRitualsLeft()
	 */
	@Override
	public int getRitualsLeft() {
		return getSpellsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getAvailableRituals()
	 */
	@Override
	public List<Ritual> getAvailableRituals() {
		return availableRituals;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeSelected(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public boolean canBeSelected(Ritual data) {
		if (getTotalSpellsAndRituals()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()*2)
			return false;
		return getAvailableRituals().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#select(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public RitualValue select(Ritual data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a ritual that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		RitualValue ref = new RitualValue(data);
		model.addRitual(ref);
		parent.runProcessors();
		
		updateAvailableRituals();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeDeselected(org.prelle.shadowrun5.RitualValue)
	 */
	@Override
	public boolean canBeDeselected(RitualValue data) {
		return model.hasRitual(data.getModifyable().getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#deselect(org.prelle.shadowrun5.RitualValue)
	 */
	@Override
	public void deselect(RitualValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a ritual that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeRitual(data);
		parent.runProcessors();
		
		updateAvailableRituals();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun5.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.TRADITION_CHANGED, data));
	}

}
