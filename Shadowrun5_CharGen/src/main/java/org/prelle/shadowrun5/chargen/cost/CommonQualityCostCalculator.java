/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Quality.QualityType;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonQualityCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private int pointsNegative;

	//-------------------------------------------------------------------
	public CommonQualityCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {
			pointsNegative = 0;
			// Only count invested karma, if quality was not given automatically
			for (QualityValue val : model.getUserSelectedQualities()) {
//				logger.debug("* "+model.getKarmaFree()+"     "+val);
				Quality data = val.getModifyable();
				int cost = data.getCost();
				if (data.getMax()>1)
					cost *= val.getPoints();
				
				if (data.getType()==QualityType.NEGATIVE) {
					logger.info("  grant "+data.getCost()+" karma for negative quality "+data.getId());
					model.setKarmaFree( model.getKarmaFree() + cost);
					pointsNegative += cost;
				} else {
					logger.info("  pay "+data.getCost()+" karma for positive quality "+data.getId());
					model.setKarmaFree( model.getKarmaFree() - cost);
				}
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	public int getPointsInNegativeQualities() {
		return pointsNegative;
	}

}
