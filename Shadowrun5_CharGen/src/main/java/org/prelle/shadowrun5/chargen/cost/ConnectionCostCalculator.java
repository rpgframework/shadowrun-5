/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ConnectionCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private int pointsLeft;
	private int highPointsLeft;

	//-------------------------------------------------------------------
	public ConnectionCostCalculator() {
	}

	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return pointsLeft + highPointsLeft;
	}

	//-------------------------------------------------------------------
	public int getHighPointsLeft() {
		return highPointsLeft;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();

		try {
			pointsLeft = 0;
			highPointsLeft = 0; // Only investable for connections with rating 8+
			for (Modification mod : previous) {
				if (mod instanceof FreePointsModification) {
					FreePointsModification free = (FreePointsModification)mod;
					if (free.getType()==Type.CONNECTIONS) {
//						logger.debug("  add "+free.getCount()+" special points");
						pointsLeft += free.getCount();
						continue;
					} 
					else if (free.getType()==Type.HIGH_CONNECTIONS) {
//						logger.debug("  add "+free.getCount()+" special points");
						highPointsLeft += free.getCount();
						continue;
					} 
				}
				unprocessed.add(mod);
			}
			logger.debug("Points for connections to spend: "+pointsLeft+"   ("+highPointsLeft+" for high places)");
			
			
			// Pay connections
			List<Connection> list = new ArrayList<>(model.getConnections());
			for (Connection con : list) {
				int value = con.getInfluence() + con.getLoyalty();
				if (con.getInfluence()>=8) {
					if (value<=highPointsLeft) {
						highPointsLeft -= value;
						logger.debug("Pay "+value+" 'Friends in High Places' points for "+con);
						value = 0;
					} else {
						logger.debug("Pay "+highPointsLeft+" 'Friends in High Places' points for "+con);
						logger.warn("Cannot afford 'High' connection: "+con+" - reduce it to normal");
						con.setInfluence(6);
						con.setLoyalty(1);
						value = con.getInfluence() + con.getLoyalty();
						logger.debug("Pay "+value+" free points for "+con);
						pointsLeft-=value;
					}
				} else {

					if (value<=pointsLeft) {
						logger.debug("Pay "+value+" free points for "+con);
						pointsLeft-=value;
					} else {
						if (pointsLeft>0) {
							value -= pointsLeft;
							logger.debug("Pay "+pointsLeft+" free points for "+con);
							pointsLeft = 0;
						}
						// Pay rest with karma
						logger.debug("Pay "+value+" karma for "+con);
						model.setKarmaFree(model.getKarmaFree() - value);
					}
				}
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
