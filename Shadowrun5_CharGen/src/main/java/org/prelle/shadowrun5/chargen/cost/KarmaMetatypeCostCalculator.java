/**
 *
 */
package org.prelle.shadowrun5.chargen.cost;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaMetatypeCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	private Properties costs;

	//-------------------------------------------------------------------
	public KarmaMetatypeCostCalculator() {
		// Load karma costs for metatypes
		costs = new Properties();
		try {
			InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("org/prelle/rpgframework/shadowrun5/data/runfaster/data/pointbuy-metacost.properties");
			costs.load( in );
		} catch (IOException e) {
			logger.fatal("Failed loading karma costs for metatypes");
			System.exit(0);
		}
	}

	//-------------------------------------------------------------------
	public int getCostFor(MetaType meta) {
		return Integer.parseInt(costs.getProperty(meta.getId()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");

		try {
			if (model.getMetatype()!=null) {
				int cost = getCostFor(model.getMetatype());
				logger.info("Pay "+cost+" karma for metatype "+model.getMetatype().getId());

				model.setKarmaFree( model.getKarmaFree() - cost);
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
