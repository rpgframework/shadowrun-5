/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Lifestyle;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.modifications.AddLifestyleModification;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class LifestyleCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	private List<LifestyleCostModification> otherModifications;
	
	//-------------------------------------------------------------------
	/**
	 */
	public LifestyleCostCalculator() {
		otherModifications = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public List<LifestyleCostModification> getLifestyleModifications() {
		return new ArrayList<>(otherModifications);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();
		otherModifications.clear();
		
		List<Lifestyle> paidByTrustFund = new ArrayList<>();
		try {		
			int percentToAdd = 0;
			int nuyenToAdd = 0;
			// Check for lifestyle multipliers
			for (Modification tmp : previous) {
				if (tmp instanceof LifestyleCostModification) {
					LifestyleCostModification mod = (LifestyleCostModification)tmp;
					otherModifications.add(mod);
					mod.getPercent();
					logger.debug("process "+mod);
					percentToAdd += mod.getPercent();
					nuyenToAdd   += mod.getFixed();
					continue;
				} else if (tmp instanceof AddLifestyleModification) {
					paidByTrustFund.add( ((AddLifestyleModification)tmp).getType() );
				}
				unprocessed.add(tmp);
			}
			int toPay = 0;
			// Walk through lifestyles
			for (LifestyleValue tmp : model.getLifestyle()) {
				logger.info("  Pay "+tmp.getCost() +" for "+tmp);
				if (paidByTrustFund.contains(tmp.getLifestyle())) {
					paidByTrustFund.remove(tmp.getLifestyle());
				} else
					toPay += tmp.getCost();
			}
			
			/*
			 * Include modifications
			 */
			int total = toPay + nuyenToAdd + (toPay*percentToAdd/100);
			logger.info("Pay "+total +" (modified) for all lifestyles");
			
			model.setNuyen( model.getNuyen() - total);

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
