/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PowerCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	protected CommonSR5CharacterGenerator parent;
	private float pointsLeft;
	private float pointsInvested;

	//-------------------------------------------------------------------
	public PowerCostCalculator(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
	}
	
	//-------------------------------------------------------------------
	public float getPowerPointsInvested() {
		return pointsInvested;
	}
	
	//-------------------------------------------------------------------
	public float getPowerPointsLeft() {
		return pointsLeft;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {
			if (model.getMagicOrResonanceType()==null)
				return unprocessed;
			pointsLeft = 0;
			pointsInvested = 0;
			
			int magic = model.getAttribute(Attribute.MAGIC).getModifiedValue();
			
			// Determine maximum value for powers - depending if character
			// is adept or mystical adept
			if (model.getMagicOrResonanceType().paysPowers())
				pointsLeft = model.getBoughtPowerPoints();
			else if (model.getMagicOrResonanceType().usesPowers())
				pointsLeft = magic;

			/*
			 * Pay bought power points when in generation mode
			 */
			if (parent.getMode()==CharGenMode.CREATING && model.getMagicOrResonanceType().paysPowers()) {
				int karma = model.getBoughtPowerPoints()*5;
				logger.info("Pay "+karma+" karma for "+model.getBoughtPowerPoints()+" bought power points");
				model.setKarmaFree( model.getKarmaFree() - karma);
			}
			
			/*
			 * Walk through powers
			 */
			for (AdeptPowerValue tmp : model.getAdeptPowers()) {
				AdeptPower power = tmp.getModifyable();
				float cost = (float)power.getCost();
				if (power.hasLevels())
				 cost = power.getCostForLevel(tmp.getLevel());
				
				logger.debug(String.format("Pay %.3f power points for %s", cost, tmp.getName()));
				pointsLeft -= cost;
				pointsInvested += cost;
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
