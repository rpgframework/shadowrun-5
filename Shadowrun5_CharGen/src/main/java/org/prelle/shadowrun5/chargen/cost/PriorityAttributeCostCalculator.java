/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityAttributeCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	private int pointsAllowed;
	private int pointsSpecial;
	private Priority priority;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityAttributeCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public void setPriority(Priority priority) {
		this.priority = priority;
		logger.info("setPriority("+priority+")");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		logger.trace("START: process");
		int karmaBefore = model.getKarmaFree();
		try {
			// Determine points allowed
			if (priority!=null) {
				switch (priority) {
				case A: pointsAllowed = 24; break;
				case B: pointsAllowed = 20; break;
				case C: pointsAllowed = 16; break;
				case D: pointsAllowed = 14; break;
				case E: pointsAllowed = 12; break;
				}
			} else
				pointsAllowed = 0;
			
			// Filter modification for free special points
			pointsSpecial = 0;
			for (Modification mod : previous) {
				if (mod instanceof FreePointsModification) {
					FreePointsModification free = (FreePointsModification)mod;
					if (free.getType()==Type.SPECIAL_POINTS) {
						logger.debug("  add "+free.getCount()+" special points");
						pointsSpecial += free.getCount();
						continue;
					} 
				}
				unprocessed.add(mod);
			}
			logger.debug("Points for special attributes to spend: "+pointsSpecial);

			/*
			 * Sort all attribute values, so that the attributes with the highest
			 * values are first. By doing so priority points are spent for high
			 * attributes (which would be expensive with karma) first.
			 */
			List<AttributeValue> allValues = new ArrayList<>();
			for (Attribute key : Attribute.primaryAndSpecialValues()) {
				allValues.add(model.getAttribute(key));
			}
			Collections.sort(allValues, new Comparator<AttributeValue>() {
				public int compare(AttributeValue o1, AttributeValue o2) {
					return -((Integer)o1.getModifiedValue()).compareTo(o2.getModifiedValue());
				}
			});
//			logger.debug("  sorted: "+allValues);

			
			for (AttributeValue val : allValues) {
			boolean isSpecial = val.getAttribute().isSpecial();
			
			int payKarmaFrom = 0;
			int payKarmaTo   = 0;
			int payFreeTo    = 0;
			if (val.getStart()==0) {
				// Generation mode
				if (isSpecial) {
					if (val.getPoints()<=pointsSpecial) {
						payFreeTo = val.getPoints();
					} else {
						payFreeTo = pointsSpecial;
						payKarmaFrom = pointsSpecial+val.getModifier();
						payKarmaTo   = val.getPoints()+val.getModifier();
					}
				} else {
					// primary attribute
					if (val.getPoints()<=pointsAllowed) {
						payFreeTo = val.getPoints();
					} else {
						payFreeTo = pointsAllowed;
						payKarmaFrom = pointsAllowed+val.getModifier();
						payKarmaTo   = val.getPoints()+val.getModifier();
					}
				}
			} else {
				// In priority tuning mode or levelling
				payKarmaFrom = val.getStart();
				payKarmaTo   = val.getPoints();
			}
			if (payFreeTo>0) {
				if (isSpecial) {
					pointsSpecial -= payFreeTo;
				} else {
					pointsAllowed -= payFreeTo;
				}
				logger.info("invest "+payFreeTo+" priority points to set attribute "+val.getAttribute()+" to "+payFreeTo);
			}
			for (int i=payKarmaFrom; i<payKarmaTo; i++) {
				int cost = (i+1)*5;
				logger.info("invest "+cost+" karma to increase attribute "+val.getAttribute()+" to "+(i+1));
				model.setKarmaFree( model.getKarmaFree() - cost );
			}
//			logger.debug("...after "+val.getName()+" Left points for  Skills="+pointsSkills+", SkillGroups="+pointsSkillGroups+", Knowledge="+pointsLangAndKnow);

//			// Spent points
//			for (AttributeValue val : allValues) {
//				switch (val.getAttribute()) {
//				case MAGIC: case RESONANCE: case EDGE:
//					if (val.getPoints()<=pointsSpecial) {
//						val.setStart(val.getPoints());
//						pointsSpecial -= val.getPoints();
//					} else {
//						// TODO
//						logger.warn("Attribute is raised above what would be possible");
//					}
//					break;
//				default:
//					if (val.getPoints()<=pointsAllowed) {
//						val.setStart(val.getPoints());
//						pointsAllowed -= val.getPoints();
//					} else {
//						// TODO
//						logger.warn("Attribute "+val+" is raised above what would be possible - free "+pointsAllowed);
//						System.exit(0);
//					}
//				}
//			}
			}
			logger.info("  free normal: "+pointsAllowed+"    free special: "+pointsSpecial);
			logger.info(" invested "+(karmaBefore - model.getKarmaFree())+" karma for attributes");
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return pointsAllowed;
	}

	//-------------------------------------------------------------------
	public int getPointsLeftSpecial() {
		return pointsSpecial;
	}

}
