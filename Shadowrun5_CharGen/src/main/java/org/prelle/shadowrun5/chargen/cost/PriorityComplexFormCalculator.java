/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityComplexFormCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private int pointsCForms;

	//-------------------------------------------------------------------
	public PriorityComplexFormCalculator() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			
			// Filter modification for free special points
			pointsCForms = 0;
			for (Modification mod : previous) {
				if (mod instanceof FreePointsModification) {
					FreePointsModification free = (FreePointsModification)mod;
					if (free.getType()==Type.COMPLEX_FORMS) {
						logger.info("  add "+free.getCount()+" points for complex forms");
						pointsCForms += free.getCount();
						continue;
					} 
				}
				unprocessed.add(mod);
			}
			logger.info("Points for complex forms to spend: "+pointsCForms);
			

			// Spent complex forms
			for (ComplexFormValue val : model.getComplexForms()) {
				if (pointsCForms>0) {
					pointsCForms--;
				} else {
					logger.info("Pay 4 karma for complex form: "+val);
					model.setKarmaFree( model.getKarmaFree() -4 );
				}
			}

		} finally {
			logger.trace("STOP : process()");
		}
		
		return unprocessed;
	}

	//-------------------------------------------------------------------
	public int getComplexFormsLeft() {
		return pointsCForms;
	}

}
