/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMagicOrResonanceCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private PriorityTableEntry tableEntry;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityMagicOrResonanceCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public void setPriority(PriorityTableEntry entry) {
		logger.debug("Set priority to "+entry);
		this.tableEntry = entry;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("START: process");
		try {
			MagicOrResonanceType selected = model.getMagicOrResonanceType();
			if (selected==null) 
				return unprocessed;
			boolean found = false;
			for (PriorityOption opt : tableEntry) {
				MagicOrResonanceOption metaOpt = (MagicOrResonanceOption)opt;
				if (selected==metaOpt.getType()) {
					found = true;
					logger.warn("Handle "+selected);
//					unprocessed.add(new FreePointsModification(Type.SPECIAL_POINTS, metaOpt.getSpecialAttributePoints()));
//					logger.info("  pay "+metaOpt.getAdditionalKarmaKost()+" Karma for "+metaOpt.getType()+" and receive "+metaOpt.getSpecialAttributePoints()+" points for special attributes");
//					model.setKarmaFree(model.getKarmaFree() - metaOpt.getAdditionalKarmaKost());
//					found = true;
				}
			}
			
			if (!found) {
				throw new IllegalStateException("Invalid metatype "+selected+" selected");
			}
		} finally {
			logger.debug("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
