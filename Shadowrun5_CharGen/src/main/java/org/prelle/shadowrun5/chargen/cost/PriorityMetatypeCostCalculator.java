/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMetatypeCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private PriorityTableEntry tableEntry;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityMetatypeCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public void setPriority(PriorityTableEntry entry) {
		logger.info("Set priority to "+entry);
		this.tableEntry = entry;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			MetaType selected = model.getMetatype();
			if (selected==null) 
				return unprocessed;
			boolean found = false;
			for (PriorityOption opt : tableEntry) {
				MetaTypeOption metaOpt = (MetaTypeOption)opt;
				if (selected==metaOpt.getType()) {
					found = true;
					unprocessed.add(new FreePointsModification(Type.SPECIAL_POINTS, metaOpt.getSpecialAttributePoints()));
					logger.info("  pay "+metaOpt.getAdditionalKarmaKost()+" Karma for "+metaOpt.getType()+" and receive "+metaOpt.getSpecialAttributePoints()+" points for special attributes");
					model.setKarmaFree(model.getKarmaFree() - metaOpt.getAdditionalKarmaKost());
					found = true;
					break;
				}
			}
			
			if (!found) {
				throw new IllegalStateException("Invalid metatype "+selected+" selected");
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
