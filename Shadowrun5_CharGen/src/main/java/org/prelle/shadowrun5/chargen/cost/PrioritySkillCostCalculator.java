/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.modifications.SpecialRuleModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioritySkillCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	/**
	 * Sort skill values so that higher values are paid by free points
	 * given by priorities
	 */
	private Boolean karmaOptimization;
	private ConfigOption<Boolean> cfgKarmaOptimization;
	
	private Priority priority;
	
	private int pointsSkills;
	private int pointsSkillGroups;
	private int pointsLangAndKnow;

	//-------------------------------------------------------------------
	/**
	 */
	public PrioritySkillCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public void setKarmaOptimization(boolean val) {
		logger.debug("Set Karma Optimization to "+val);
		this.karmaOptimization = val;
	}

	//-------------------------------------------------------------------
	public void setKarmaOptimization(ConfigOption<Boolean> b) {
		cfgKarmaOptimization = b;
	}

	//-------------------------------------------------------------------
	private boolean getKarmaOptimization() {
		if (karmaOptimization!=null)
			return karmaOptimization; 
		if (cfgKarmaOptimization!=null)
			return (boolean) cfgKarmaOptimization.getValue(); 
		return false;
	}

	//-------------------------------------------------------------------
	public void setPriority(Priority priority) {
		this.priority = priority;
		logger.info("setPriority("+priority+")");
	}

	//-------------------------------------------------------------------
	public int getPointsLeftInKnowledgeAndLanguage() {
		return pointsLangAndKnow;
	}

	//-------------------------------------------------------------------
	public int getPointsLeftSkillGroups() {
		return pointsSkillGroups;
	}

	//-------------------------------------------------------------------
	public int getPointsLeftSkills() {
		return pointsSkills;
	}

	//-------------------------------------------------------------------
	public int getIncreaseCost(ShadowrunCharacter model, Skill key) {
		SkillValue sVal = model.getSkillValue(key);
		int newVal = (sVal==null)?1:(sVal.getModifiedValue()+1);

		if (key.getType()==SkillType.KNOWLEDGE || key.getType()==SkillType.LANGUAGE)
			return newVal;
		return newVal*2;
	}

	//-------------------------------------------------------------------
	public int getIncreaseCost(ShadowrunCharacter model, SkillGroup key) {
		SkillGroupValue sVal = model.getSkillGroupValue(key);
		int newVal = (sVal==null)?1:(sVal.getModifiedValue()+1);
		return newVal*5;
	}


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process Modifications
			List<SpecialRuleModification.Rule> specialRules = new ArrayList<>();
			for (Modification tmp : previous) {
				if (tmp instanceof SpecialRuleModification) {
					switch (((SpecialRuleModification)tmp).getRule()) {
					case SKILL_ACADEMIC_COST_REDUCED:
					case SKILL_STREETLORE_COST_REDUCED:
					case SKILL_LANGUAGE_COST_REDUCED:
						specialRules.add( ((SpecialRuleModification)tmp).getRule() );
						break;
					default:
						unprocessed.add(tmp);
					}
				} else
					unprocessed.add(tmp);
			}

			/*
			 * Calculate free points for Skills, SkillGroups and Knowledge Skills
			 * depending on priority
			 */
			pointsSkills = 0;
			pointsSkillGroups = 0;
			if (priority!=null) {
				switch (priority) {
				case A:
					pointsSkills = 46;
					pointsSkillGroups = 10;
					break;
				case B:
					pointsSkills = 36;
					pointsSkillGroups = 5;
					break;
				case C:
					pointsSkills = 28;
					pointsSkillGroups = 2;
					break;
				case D:
					pointsSkills = 22;
					pointsSkillGroups = 0;
					break;
				case E:
					pointsSkills = 18;
					pointsSkillGroups = 0;
					break;
				}
				logger.debug("Priority "+priority+" grants "+pointsSkillGroups+" points for skillgroups and "+pointsSkills+" points for skills");
			}

			/*
			 * Calculate free language and knowledge points
			 */
			pointsLangAndKnow = (
					model.getAttribute(Attribute.LOGIC).getModifiedValue()+
					model.getAttribute(Attribute.INTUITION).getModifiedValue()
					)*2;

			logger.debug("  Free points for Knowledge="+pointsLangAndKnow);
			
			List<SkillValue> allSkillValues = new ArrayList<>(model.getSkillValues(false));
			if (getKarmaOptimization()) {
				logger.info("OPTIMIZE - order skills by value");
				/*
				 * Sort all skill values, so that the skills with the highest
				 * values are first. By doing so priority points are spent for high
				 * skill (which would be expensive with karma) first.
				 */
				Collections.sort(allSkillValues, new Comparator<SkillValue>() {
					public int compare(SkillValue o1, SkillValue o2) {
						Integer payWithKarma1 = ((Integer)o1.getModifiedValue()) - o1.getStart();
						Integer payWithKarma2 = ((Integer)o2.getModifiedValue()) - o2.getStart();
						int karmaSort = payWithKarma1.compareTo(payWithKarma2);
//						logger.info("---Pay "+payWithKarma1+" for "+o1.getName()+" and "+payWithKarma2+" for "+o2.getName()+" = "+karmaSort);
//						if (karmaSort==0) {
//							Integer payWithKarma1 = ((Integer)o1.getModifiedValue()) - o1.getStart();
//							Integer payWithKarma2 = ((Integer)o2.getModifiedValue()) - o2.getStart();
//							int karmaSort = -payWithKarma1.compareTo(payWithKarma2);
//						}
						return -karmaSort;
					}
				});
				logger.trace("  sorted: "+allSkillValues);
			} else {
				logger.info("DO NOT OPTIMIZE");
			}

			// Spent skill points
			for (SkillValue val : allSkillValues) {
				boolean isKnowledge = val.getModifyable().getType()==SkillType.LANGUAGE || val.getModifyable().getType()==SkillType.KNOWLEDGE;
				if (isKnowledge && "native_language".equals(val.getModifyable().getId()))
					continue;
				boolean applyAcademic = val.getModifyable().getId().equals("academic_knowledge") && specialRules.contains(SpecialRuleModification.Rule.SKILL_ACADEMIC_COST_REDUCED);
				boolean applyLinguist = val.getModifyable().getId().equals("language") && specialRules.contains(SpecialRuleModification.Rule.SKILL_LANGUAGE_COST_REDUCED);
				boolean applyHardKnock = val.getModifyable().getId().equals("street_knowledge") && specialRules.contains(SpecialRuleModification.Rule.SKILL_STREETLORE_COST_REDUCED);
				boolean applyTechnical = val.getModifyable().getId().equals("professional_knowledge") && specialRules.contains(SpecialRuleModification.Rule.KNOWLEDGE_2_FOR_1_GEN);
				
				int payKarmaFrom = 0;
				int payKarmaTo   = 0;
				int payFreeTo    = 0;
				
				/*
				 * Determine how many points can be paid with free building points
				 * If not in tuning mode (val.getStart()==0) or if Karma optimization
				 * is active, all points can be paid with free building points
				 */
				boolean tuningMode = val.getStart()!=0;
				int maxFree = (!tuningMode)?val.getPoints():val.getStart();
				// Now reduce this value until a point is reached, where it can be
				// afforded
				int affordableFree = maxFree;
				if (!tuningMode) {
					for (int i=maxFree; i>0; i--) {
						affordableFree = i;
						int freeCost = affordableFree;
						if (isKnowledge && (applyAcademic || applyLinguist || applyHardKnock || applyTechnical)) { 
							freeCost = (int)Math.round(freeCost*0.5);
							logger.debug("  Apply factor 0.5 to cost of "+val+" due to special cost rules: "+specialRules);
						}
						if (isKnowledge && freeCost<=pointsLangAndKnow) break;
						if (!isKnowledge && freeCost<=pointsSkills) break;
						// Not affordable yet
						--affordableFree;
					} 
				}
				// affordableFree is somewhere between 0 and maxFree
				payFreeTo = affordableFree;
				payKarmaFrom = affordableFree;
				payKarmaTo   = val.getPoints();
				
				// Now it is time to spend the points
				if (!tuningMode && payFreeTo>0) {
					if (isKnowledge) {
						int pay = (int) ((applyAcademic||applyLinguist||applyHardKnock||applyTechnical)?Math.round(payFreeTo*0.5):payFreeTo);
						pointsLangAndKnow -= pay;
						logger.debug("invest "+pay+" free points to set skill "+val.getName()+" to "+payFreeTo+" (remain="+pointsLangAndKnow+")");
					} else {
						pointsSkills -= payFreeTo;
						logger.debug("invest "+payFreeTo+" free points to set skill "+val.getName()+" to "+payFreeTo+" (remain="+pointsSkills+")");
					}
				}
				for (int i=payKarmaFrom; i<payKarmaTo; i++) {
					int cost = (i+1)*(isKnowledge?1:2);
					logger.debug("invest "+cost+" karma to increase skill "+val.getName()+" to "+(i+1));
					model.setKarmaFree( model.getKarmaFree() - cost );
				}
				
				/*
				 * Specializations
				 */
				int specs = val.getSkillSpecializations().size();
				while (specs>0) {
					if (pointsSkills>0) {
						logger.debug("invest 1 free points to specialize skill "+val.getName()+"  (remain="+pointsLangAndKnow+")");
						pointsSkills--;
					} else {
						logger.debug("invest 7 karma to specialize skill "+val.getName()+"  (remain="+pointsLangAndKnow+")");
						model.setKarmaFree( model.getKarmaFree() -7 );
					}
					specs--;
				}
				
//				logger.debug("...after "+val.getName()+" Left points for  Skills="+pointsSkills+", SkillGroups="+pointsSkillGroups+", Knowledge="+pointsLangAndKnow);
			}

			/*
			 * Sort all skill values, so that the skills with the highest
			 * values are first. By doing so priority points are spent for high
			 * skill (which would be expensive with karma) first.
			 */
			List<SkillGroupValue> allGrpValues = new ArrayList<>(model.getSkillGroupValues());
			Collections.sort(allGrpValues, new Comparator<SkillGroupValue>() {
				public int compare(SkillGroupValue o1, SkillGroupValue o2) {
					return -((Integer)o1.getModifiedValue()).compareTo(o2.getModifiedValue());
				}
			});
			logger.trace("  sorted groups: "+allGrpValues);

			// Spent skill points
			for (SkillGroupValue val : allGrpValues) {
				
				int payKarmaFrom = 0;
				int payKarmaTo   = 0;
				int payFreeTo    = 0;
				if (val.getStart()==0) {
					// Generation mode
					// Action skill				
					if (val.getPoints()<=pointsSkillGroups) {
						payFreeTo = val.getPoints();
					} else {
						payFreeTo = pointsSkillGroups;
						payKarmaFrom = pointsSkillGroups;
						payKarmaTo   = val.getPoints();
					}
				} else {
					// In priority tuning mode or levelling
					payKarmaFrom = val.getStart();
					payKarmaTo   = val.getPoints();
				}
				if (payFreeTo>0) {
					pointsSkillGroups -= payFreeTo;
					logger.debug("invest "+payFreeTo+" priority points to set skillgroup "+val.getModifyable().getName()+" to "+payFreeTo);
				}
				for (int i=payKarmaFrom; i<payKarmaTo; i++) {
					int cost = (i+1)*5;
					logger.debug("invest "+cost+" karma to increase skillgroup "+val.getModifyable().getName()+" to "+(i+1));
					model.setKarmaFree( model.getKarmaFree() - cost );
				}
			}
			logger.debug("Left points for  Skills="+pointsSkills+", SkillGroups="+pointsSkillGroups+", Knowledge="+pointsLangAndKnow);

		} finally {
			logger.trace("STOP : process()");
		}
		
		return unprocessed;
	}

}
