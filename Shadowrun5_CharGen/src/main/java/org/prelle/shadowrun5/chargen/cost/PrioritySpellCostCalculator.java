/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioritySpellCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");
	
	private int pointsSpellsAndRituals;

	//-------------------------------------------------------------------
	/**
	 */
	public PrioritySpellCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			
			// Filter modification for free special points
			pointsSpellsAndRituals = 0;
			for (Modification mod : previous) {
				if (mod instanceof FreePointsModification) {
					FreePointsModification free = (FreePointsModification)mod;
					if (free.getType()==Type.SPELLS_RITUALS) {
//						logger.debug("  add "+free.getCount()+" special points");
						pointsSpellsAndRituals += free.getCount();
						continue;
					} 
				}
				unprocessed.add(mod);
			}
			logger.debug("Points for spells/rituals to spend: "+pointsSpellsAndRituals);
			

			// Spent free spells
			for (SpellValue val : model.getSpells()) {
				if (pointsSpellsAndRituals>0) {
					pointsSpellsAndRituals--;
				} else {
					logger.info("Pay 5 karma for "+val);
					model.setKarmaFree( model.getKarmaFree() -5);
				}
			}

		} finally {
			logger.trace("STOP : process()");
		}
		
		return unprocessed;
	}

	//-------------------------------------------------------------------
	public int getSpellsLeft() {
		return pointsSpellsAndRituals;
	}

}
