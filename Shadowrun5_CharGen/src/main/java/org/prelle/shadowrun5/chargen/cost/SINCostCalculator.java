/**
 * 
 */
package org.prelle.shadowrun5.chargen.cost;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.LicenseValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SINCostCalculator implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	//-------------------------------------------------------------------
	/**
	 */
	public SINCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {

			// Walk through all SINs - ignore real SINs / criminals
			for (SIN sin : model.getSINs()) {
				if (sin.getQuality()==Quality.REAL_SIN)
					continue;
				if (sin.isCriminal())
					continue;
				logger.debug("Pay "+sin.getQualityValue()*2500 +" for "+sin);
				model.setNuyen( model.getNuyen() - sin.getQualityValue()*2500);
			}

			// Walk through all Licenses 
			for (LicenseValue lic : model.getLicenses()) {
				logger.debug("Pay "+lic.getRating().getValue()*200 +" for "+lic);
				model.setNuyen( model.getNuyen() - lic.getRating().getValue()*200);
			}

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
