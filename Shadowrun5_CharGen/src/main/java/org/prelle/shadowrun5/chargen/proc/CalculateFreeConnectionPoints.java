/**
 * 
 */
package org.prelle.shadowrun5.chargen.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.modifications.SpecialRuleModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateFreeConnectionPoints implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public CalculateFreeConnectionPoints() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			for (Modification mod : previous) {
				if (mod instanceof SpecialRuleModification) {
					switch (((SpecialRuleModification)mod).getRule()) {
					case FRIENDS_IN_HIGH_PLACES:
						unprocessed.add(new FreePointsModification(Type.HIGH_CONNECTIONS, model.getAttribute(Attribute.CHARISMA).getModifiedValue()*4));
						break;
					default:
						unprocessed.add(mod);
					}
				} else
					unprocessed.add(mod);
			}

			// Inject free connections
			unprocessed.add(new FreePointsModification(Type.CONNECTIONS, model.getAttribute(Attribute.CHARISMA).getModifiedValue()*3));
			logger.debug("Grant "+(model.getAttribute(Attribute.CHARISMA).getModifiedValue()*3)+" free points for connections");
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
