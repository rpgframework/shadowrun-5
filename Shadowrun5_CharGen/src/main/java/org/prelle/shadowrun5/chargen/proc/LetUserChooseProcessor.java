/**
 * 
 */
package org.prelle.shadowrun5.chargen.proc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.modifications.ModificationChoice;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class LetUserChooseProcessor implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private SR5LetUserChooseListener callback;
	private Map<Modification, Collection<Modification>> decisions; 

	//-------------------------------------------------------------------
	/**
	 */
	public LetUserChooseProcessor() {
		decisions = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public void setCallback(SR5LetUserChooseListener callback) {
		this.callback = callback;
	}

	//-------------------------------------------------------------------
	public void clearDecisions() {
		decisions.clear();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			for (Modification tmp : previous) {
				if (tmp instanceof ModificationChoice) {
					logger.warn("+++++ TODO: choose "+tmp);
					ModificationChoice choice = (ModificationChoice)tmp;
					// Is there already a decision for this choice?
					if (decisions.containsKey(choice)) {
						logger.info("  choice "+choice+" already decided to "+decisions.get(choice));
						unprocessed.addAll(decisions.get(choice));
					} else {
						// Build a reason string
						String reason = String.valueOf(tmp.getSource());
						if (tmp.getSource() instanceof MagicOrResonanceType)
							reason = ((MagicOrResonanceType)tmp.getSource()).getName();
						else
							logger.warn("Don't know how to convert "+tmp.getSource()+" into a human readable string");
//						logger.debug("     reason of "+tmp.getSource().getClass()+" = "+reason);
						if (callback!=null) {
							Collection<Modification> decision = callback.letUserChoose(reason, choice);
							if (decision!=null) {
								unprocessed.addAll(decision);
								decisions.put(choice, decision);
								logger.debug("User chose "+decision);
							} else {
								logger.debug("User did not choose "+choice);
								unprocessed.add(choice);
							}
								
						}
					}
				} else if (tmp instanceof SkillModification && ((SkillModification)tmp).getSkill()==null) {
					SkillModification mod = (SkillModification)tmp;
					logger.warn("+++++TODO: choose "+tmp);
					List<Modification> mods = new ArrayList<>();
					for (Skill skill : ShadowrunCore.getSkills(mod.getType())) {
						SkillModification toAdd = new SkillModification(skill, mod.getValue());
						toAdd.setSource(mod.getSource());
						mods.add(toAdd);
					}
					ModificationChoice choice = new ModificationChoice(mods,1); 
					// Is there already a decision for this choice?
					if (decisions.containsKey(mod)) {
						logger.debug("  choice "+choice+" already decided to "+decisions.get(mod));
						unprocessed.addAll(decisions.get(mod));
					} else {
						// Build a reason string
						String reason = String.valueOf(tmp.getSource());
						if (tmp.getSource() instanceof MagicOrResonanceType)
							reason = ((MagicOrResonanceType)tmp.getSource()).getName();
						else
							logger.warn("Don't know how to convert "+tmp.getSource()+" into a human readable string");
//						logger.debug("     reason of "+tmp.getSource().getClass()+" = "+reason);
						if (callback!=null) {
							Collection<Modification> decision = callback.letUserChoose(reason, choice);
							if (decision!=null) {
								unprocessed.addAll(decision);
								decisions.put(mod, decision);
								logger.debug("User chose "+decision);
							} else {
								logger.debug("User did not choose "+choice);
								unprocessed.add(choice);
							}
								
						}
					}
//					System.exit(0);
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
