/**
 *
 */
package org.prelle.shadowrun5.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.items.AccessoryData;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.BodytechQuality;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemEnhancement;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.requirements.AnyRequirement;
import org.prelle.shadowrun5.requirements.ItemHookRequirement;
import org.prelle.shadowrun5.requirements.ItemRequirement;
import org.prelle.shadowrun5.requirements.ItemSubTypeRequirement;
import org.prelle.shadowrun5.requirements.ItemTypeRequirement;
import org.prelle.shadowrun5.requirements.Requirement;
import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.CarriedItemModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public abstract class CommonEquipmentController implements EquipmentController {

	protected static final Logger logger = LogManager.getLogger("shadowrun5.gen");
	
	protected ShadowrunCharacter model;
	private CharGenMode mode;
	
	private Map<Modification, CarriedItem> itemsByMods;

	//--------------------------------------------------------------------
	public CommonEquipmentController(ShadowrunCharacter model, CharGenMode mode) {
		this.model = model;
		this.mode  = mode;
		itemsByMods = new HashMap<>();
		calculateEssenceCost();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	protected void calculateEssenceCost() {
		logger.debug("calculateEssenceCost");
		float sum = 0.0f;
		for (CarriedItem item : model.getItems(false)) { 
			if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getItem().getType())) {
				float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
				
				logger.debug("* "+item.getName()+" = "+essence);
				sum += essence;
			}
		}
			
		float normalLow = 6.0f - sum;
		if (model.getUnusedEssence()==0 || normalLow<model.getUnusedEssence()) {
			logger.debug("Unused essence decreased to "+normalLow);
			model.setUnusedEssence(normalLow);
		}
		logger.debug("sum="+sum+"  normalLow="+normalLow+"  unused="+model.getUnusedEssence());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ESSENCE_CHANGED, sum, model.getUnusedEssence()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getCost(org.prelle.shadowrun5.items.ItemTemplate, int, org.prelle.shadowrun5.items.BodytechQuality)
	 */
	@Override
	public int getCost(ItemTemplate item, SelectionOption...options) {
		float cost = item.getPrice();
//		logger.debug("getCost("+Arrays.toString(options)+")");
		
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case AMOUNT:
				cost *= option.getAsAmount();
				break;
			case BODYTECH_QUALITY:
				if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getType())) {
					switch (option.getAsBodytechQuality()) {
					case ALPHA: cost *= 1.2; break;
					case BETA : cost *= 1.5; break;
					case DELTA: cost *= 2.5; break;
					case USED : cost *= 0.75; break;
					default:
					}
				}
				break;
			case RATING:
				if (item.hasRating() && option.getAsRating()>1 && Arrays.asList(item.getMultiplyWithRate()).contains(Multiply.PRICE)) {
					cost *= option.getAsRating();			
				}
				break;
			}
		}
		
		return Math.round(cost);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canBeSelected(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public boolean canBeSelected(ItemTemplate item, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException();
		// Calculate price and check it
		int price = getCost(item, options);
		
		if (price>model.getNuyen())
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	private CarriedItem buildItem(ItemTemplate item, SelectionOption...options) {
		CarriedItem ref = new CarriedItem(item);
		if (item.hasRating())
			ref.setRating(1);
		if (item.getType()==ItemType.CYBERWARE || item.getType()==ItemType.BIOWARE || item.getType()==ItemType.NANOWARE)
			ref.setQuality(BodytechQuality.STANDARD);
		
		// Modify according to options
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case AMOUNT:
				ref.setCount(option.getAsAmount());
				break;
			case BODYTECH_QUALITY:
				ref.setQuality(option.getAsBodytechQuality());
				break;
			case RATING:
				if (item.hasRating() && option.getAsRating()>1) {
					ref.setRating(option.getAsRating());
				}
				break;
			}
		}

		return ref;
	}

	//-------------------------------------------------------------------
	private ProgramValue buildProgram(Program item, SelectionOption...options) {
		ProgramValue ref = new ProgramValue(item);
		if (item.hasRating())
			ref.setRating(1);
		
		// Modify according to options
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case RATING:
				if (item.hasRating() && option.getAsRating()>1) {
					ref.setRating(option.getAsRating());
				}
				break;
			default:
				logger.warn("Ignore option "+option);
			}
		}

		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#select(org.prelle.shadowrun5.items.ItemTemplate, int)
	 */
	@Override
	public CarriedItem select(ItemTemplate item, SelectionOption...options) {
		logger.debug("START-------select "+item.getId()+"-------------");
		if (!canBeSelected(item, options)) {
			logger.warn("Cannot select "+item);
			return null;
		}

		CarriedItem ref = buildItem(item, options);
		logger.info("Select "+ref);
		model.addItem(ref);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_ADDED, ref));
		if (mode==CharGenMode.LEVELING) {
			model.setNuyen(model.getNuyen() - item.getPrice());
			logger.debug("Substract "+item.getPrice()+" Nuyen - new account is "+model.getNuyen());
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, ref));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, ref));
		}
		
		/*
		 * For bodytech
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getItem().getType())) {
			calculateEssenceCost();
		}

		/*
		 * Apply modifications from item
		 */
		ShadowrunTools.equip(model, ref);
		
		
		logger.debug("STOP -------select "+item.getId()+"-------------");
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#deselect(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		/*
		 * Undo modifications from item
		 */
		ShadowrunTools.unequip(model, ref);

		boolean removed = model.removeItem(ref);
		logger.info("removed = "+removed);
		// If not found directly, check accessory slots
		if (!removed) {
			logger.debug("Item to remove not found directly - check accessories");
			for (CarriedItem tmp : model.getItems(false)) {
				for (CarriedItem access : tmp.getAccessories()) {
					if (access==ref) {
						logger.debug("Remove item "+ref.getName()+"  EMBEDDED IN "+tmp.getName());
						removed = tmp.removeAccessory(ref);
						break;
					}
				}
			}
		}
		if (!removed) {
			logger.warn("Item to remove not found");
		}
		
		/*
		 * For bodytech
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getItem().getType())) {
			float essence = (Float)ref.getAsObject(ItemAttribute.ESSENCECOST);
			model.setUnusedEssence(Math.min(6.0f, model.getUnusedEssence()+essence));
			calculateEssenceCost();
		}

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_REMOVED, ref));
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#sell(org.prelle.shadowrun5.items.CarriedItem, float)
	 */
	@Override
	public void sell(CarriedItem ref, float factor) {
		boolean removed = model.removeItem(ref);
		// If not found directly, check accessory slots
		if (!removed) {
			logger.debug("Item to remove not found directly - check accessories");
			for (CarriedItem tmp : model.getItems(false)) {
				for (CarriedItem access : tmp.getAccessories()) {
					if (access==ref) {
						logger.debug("Remove item "+ref.getName()+"  EMBEDDED IN "+tmp.getName());
						tmp.removeAccessory(ref);
						removed = true;
						break;
					}
				}
			}
		}
		
		/*
		 * For bodytech
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getItem().getType())) {
			calculateEssenceCost();
		}

		/*
		 * Undo modifications from item
		 */
		ShadowrunTools.unequip(model, ref);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_REMOVED, ref));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getEmbeddableIn(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		for (ItemTemplate item : ShadowrunCore.getItems(ItemType.ACCESSORY)) {
			if (!ref.hasEmbedded(item))
				ret.add(item);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed) {
		if (toEmbed==null)
			throw new NullPointerException("Item is null");
		
		// Ensure that all slot requirements are met
		for (Requirement req : toEmbed.getItem().getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement hookReq = (ItemHookRequirement)req;
				AvailableSlot avail = container.getSlot(hookReq.getSlot());
				if (avail==null) {
					// Required slot not available
					return false;
				}
			} else
				logger.warn("Did not check requirement "+req);
		}
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canBeEmbedded(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemTemplate, org.prelle.shadowrun5.items.AccessorySlot)
	 */
	@Override
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed, ItemHook slot) {

		AvailableSlot avail = container.getSlot(slot);
		if (avail==null) {
			// Required slot not available
			return false;
		}

		int neededCap = toEmbed.getCapacity(slot);
		logger.debug("Needed capacity for "+toEmbed.getName()+" is "+neededCap);
		if (slot.hasCapacity() && avail.getFreeCapacity()<neededCap) {
			logger.debug("Cannot embed - no free capacity  ("+avail.getFreeCapacity()+" < "+neededCap+")");
			return false;
		}
//			cap = avail.getCapacity();
//		if (embeddedItems.size()>cap)
//			throw new IllegalStateException("Cannot add any more items. Already have "+embeddedItems.size());

		return true;
	}

	//--------------------------------------------------------------------
	private boolean isRequirementMet(CarriedItem ref, ItemHook slot, AvailableSlot hook, Requirement req, AccessoryData access) {
		if (access==null)
			throw new NullPointerException("AccessoryData");
		if (req instanceof ItemHookRequirement) {
			ItemHookRequirement iReq = (ItemHookRequirement)req;
//			logger.debug("  - slot="+slot+"   iReq.getSlot="+iReq.getSlot());
			if (iReq.getSlot()!=slot) {
				return false;
			}
			if (slot.hasCapacity()) {
				int free = hook.getFreeCapacity();
				if (free<access.getCapacitySize()) {
					// Too big
					return false;
				}
			}
		} else if (req instanceof ItemTypeRequirement) {
			ItemTypeRequirement tReq = (ItemTypeRequirement)req;
			if (ref.getItem().getType()!=tReq.getType()) {
				// Wrong item type
				logger.debug("  wrong item type - exp. "+tReq.getType());
				return false;
			}
		} else if (req instanceof ItemSubTypeRequirement) {
			ItemSubTypeRequirement tReq = (ItemSubTypeRequirement)req;
			boolean found = false;
			for (ItemSubType tmp : tReq.getType()) {
				if (ref.getItem().getSubtype()==tmp) {
					found = true;
					break;
				}
			}
			if (!found) {
				// Wrong item subtype
//				logger.debug("  wrong item subtype - exp. "+tReq.getType()+" but item is "+ref.getItem().getSubtype());
				return false;
			}
		} else if (req instanceof ItemRequirement) {
			ItemRequirement iReq = (ItemRequirement)req;
			if (!iReq.getItemID().equals(ref.getItem().getId())) {
//				logger.debug("  wrong item - only usable with "+iReq.getItemID());
				return false;
			}
		} else {
			logger.warn("Unprocessed requirement type: "+req);
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getEmbeddableIn(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref, ItemHook slot) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();

		AvailableSlot hook = ref.getSlot(slot);
		// Ensure item has that slot available
		if (hook==null) {
			logger.warn("Embeddable items for non-existing hook "+slot+" have been requested for "+ref);
			return ret;
		}

		itemloop:
			for (ItemTemplate item : ShadowrunCore.getItems(ItemType.ACCESSORY)) {
				logger.trace("Check for embedding "+item);
//				Accessory access = (Accessory)item;
//				if (item.getId().contains("silencer")) {
//					logger.debug("Foo");
//				}
				AccessoryData access = item.getAccessoryData();
				boolean slotMatches = false;
				for (Requirement req : item.getRequirements()) {
					if (req instanceof ItemHookRequirement) {
						ItemHookRequirement iReq = (ItemHookRequirement)req;
						if (iReq.getSlot()==slot) {
							slotMatches = true;
							if (iReq.getCapacity()>hook.getFreeCapacity()) {
								// Not enough capacity left
								continue itemloop;
							}
//						} else if (slot.hasCapacity()) {
//							int free = hook.getFreeCapacity();
//							logger.debug("for item "+item+" the accessory is "+access);
//							if (access==null) {
//								// Not an item with capacity
//								continue itemloop;
//							}
//							if (free<access.getCapacitySize()) {
//								// Too big
//								continue itemloop;
//							}
						}
					} else if (req instanceof AnyRequirement) {
						AnyRequirement aReq = (AnyRequirement)req;
						logger.info("check AnyRequirement for "+access);
//						boolean anyMisMatches = false;
//						for (Requirement tmpReq : aReq.getOptionList()) {
//							if (!isRequirementMet(ref, slot, hook, tmpReq, access))
//								anyMisMatches = true;
//						}
//						if (anyMisMatches)
//							continue itemloop;
						boolean anyMatch = false;
						for (Requirement tmpReq : aReq.getOptionList()) {
							logger.debug("* check "+ref+" and slot "+slot+" for hook "+hook+" and "+tmpReq+" = "+isRequirementMet(ref, slot, hook, tmpReq, access));
							if (isRequirementMet(ref, slot, hook, tmpReq, access))
								anyMatch = true;
						}
						if (!anyMatch)
							continue itemloop;
						slotMatches = true;
					} else {
						if (!isRequirementMet(ref, slot, hook, req, access))
							continue itemloop;
//						logger.debug("  requirement "+req+" is met");
					}
				}

				if (!slotMatches) {
//					logger.debug("  Request was for slot "+slot+", but item does not provide that");
					// Wrong slot
					continue itemloop;
				}
				logger.debug("--- Add "+item+" ---");
				ret.add(item);
			}

		// Remove all those already inserted in that slot
		for (CarriedItem already : hook.getUserEmbeddedItems()) {
			if (ret.contains(already.getItem())) {
				ret.remove(already.getItem());
//				logger.debug("  Remove option "+already.getItem()+" since it already exists in slot");
			}
		}


		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getEmbeddedIn(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public List<CarriedItem> getEmbeddedIn(CarriedItem ref) {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException("Item is null");

		CarriedItem accessory = buildItem(item, options);
		
		if (!canBeEmbedded(container, accessory)) {
			logger.warn("Cannot embed "+item+" to "+container);
			return null;
		}
		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		List<AvailableSlot> embedIn = new ArrayList<>();
		// Ensure that all slot requirements are met
		for (Requirement req : item.getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement hookReq = (ItemHookRequirement)req;
				AvailableSlot avail = container.getSlot(hookReq.getSlot());
				if (avail==null) {
					logger.error("  Container "+container+" hasn't the necessary slot "+hookReq.getSlot());
					return null;
				}
				embedIn.add(avail);
			}
		}

		
		logger.debug("  Accessory is worth "+accessory.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		for (AvailableSlot slot : embedIn) {
			logger.info("Embed "+accessory+" in "+slot.getSlot());
			container.addAccessory(slot.getSlot(), accessory);
		}

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, container));
		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		logger.debug("STOP -------embed "+item.getId()+" into "+container+"-------------");
		return accessory;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook hook, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException("Item is null");
		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
		CarriedItem accessory = buildItem(item, options);

		if (!canBeEmbedded(container, accessory, hook)) {
			logger.warn("Cannot embed "+item+" to "+container);
			return null;
		}
		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		logger.info("Add accessory "+accessory+"  in "+container.getName()+" in slot "+hook);
		container.addAccessory(hook, accessory);
//		AvailableSlot slot = container.getSlot(hook);
//		slot.addEmbeddedItem(accessory);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, container));
		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		logger.debug("STOP -------embed "+item.getId()+" into "+container+"-------------");
		return accessory;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#embed(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.Program, org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption[])
	 */
	@Override
	public ProgramValue embed(CarriedItem container, Program item, SelectionOption... options) {
		if (item==null)
			throw new NullPointerException("Item is null");
		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
		ProgramValue accessory = buildProgram(item, options);

		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		container.addProgram(accessory);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, container));
		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		logger.debug("STOP -------embed "+item.getId()+" into "+container+"-------------");
		return accessory;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#remove(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean remove(CarriedItem container, CarriedItem ref) {
		// TODO Auto-generated method stub
		logger.warn("TODO: remove");
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getAvailableEnhancementsFor(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public List<ItemEnhancement> getAvailableEnhancementsFor(CarriedItem ref) {
		// TODO Auto-generated method stub
		logger.warn("TODO: getAvailableEnhancementsFor");
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getEnhancementsIn(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public List<ItemEnhancement> getEnhancementsIn(CarriedItem ref) {
		// TODO Auto-generated method stub
		logger.warn("TODO: getAvailableEnhancementsFor");
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#modify(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemEnhancement)
	 */
	@Override
	public boolean modify(CarriedItem container, ItemEnhancement mod) {
		// TODO Auto-generated method stub
		logger.warn("TODO: modify");
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#remove(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.ItemEnhancement)
	 */
	@Override
	public boolean remove(CarriedItem container, ItemEnhancement mod) {
		// TODO Auto-generated method stub
		logger.warn("TODO: remove");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#increase(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
		if (model.getNuyen()<instanceCost)
			return false;
		data.setCount(data.getCount()+1);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#decrease(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		if (data.getCount()<=1)
			return false;
		data.setCount(data.getCount()-1);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getCountChangeCost(org.prelle.shadowrun5.items.CarriedItem, int)
	 */
	@Override
	public int getCountChangeCost(CarriedItem data, int newCount) {
		int totalCost  = data.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		int singleCost = totalCost / data.getCount();
		int newTotal   = singleCost * newCount;
		return newTotal - totalCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canChangeCount(org.prelle.shadowrun5.items.CarriedItem, int)
	 */
	@Override
	public boolean canChangeCount(CarriedItem data, int newCount) {
		if (newCount<1) return false;
		if (newCount>100) return false;
		
		int newCost = getCountChangeCost(data, newCount);
		return (newCost<0 || (newCost<=model.getNuyen()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#markUpdated(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public void markUpdated(CarriedItem data) {
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, data));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canBuyLevel(org.prelle.shadowrun5.items.CarriedItem, int)
	 */
	@Override
	public boolean canBuyLevel(CarriedItem item, int newLevel) {
		int currentLevel= item.getRating();
		int currentCost = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		item.setRating(newLevel);
		int newCost = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		Availability newAvail= item.getAvailability();
		item.setRating(currentLevel);
		
		if (newAvail.getValue()>12)
			return false;
		
		int diff = newCost - currentCost;
		if (diff<=0)
			return true;
		
		return diff <= model.getNuyen();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#changeRating(org.prelle.shadowrun5.items.CarriedItem, int)
	 */
	@Override
	public boolean changeRating(CarriedItem item, int newRating) {
		if (!canBuyLevel(item, newRating))
			return false;
		
		int currentLevel= item.getRating();
		int currentCost = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		item.setRating(newRating);
		int newCost = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		
		int diff = newCost - currentCost;
		
		logger.info("Changed rating of "+item+" from "+currentLevel+" to "+newRating+" and paid \u00A5 "+diff);
		model.setNuyen(model.getNuyen()-diff);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, model.getNuyen()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, item));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canBuyQuality(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.BodytechQuality)
	 */
	@Override
	public boolean canBuyQuality(CarriedItem item, BodytechQuality newQuality) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#changeQuality(org.prelle.shadowrun5.items.CarriedItem, org.prelle.shadowrun5.items.BodytechQuality)
	 */
	@Override
	public boolean changeQuality(CarriedItem item, BodytechQuality newQuality) {
		if (!canBuyQuality(item, newQuality))
			return false;
		logger.debug("Change bodytech quality of "+item+" to "+newQuality);
		
		item.setQuality(newQuality);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, item));
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getOptions(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public List<SelectionOptionType> getOptions(ItemTemplate item) {
		List<SelectionOptionType> list = new ArrayList<>();
		
		if (item.hasRating())
			list.add(SelectionOptionType.RATING);
		
		if (item.getType()==ItemType.CYBERWARE || (item.getType()==ItemType.BIOWARE && item.getSubtype()==ItemSubType.BIOWARE_STANDARD))
			list.add(SelectionOptionType.BODYTECH_QUALITY);
		
		return list;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#apply(org.prelle.shadowrun5.modifications.CarriedItemModification)
	 */
	@Override
	public void apply(CarriedItemModification mod) {
		logger.debug("apply "+mod);
		CarriedItem ref = buildItem(mod.getItem());
		model.addItem(ref);
		itemsByMods.put(mod, ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#undo(org.prelle.shadowrun5.modifications.CarriedItemModification)
	 */
	@Override
	public void undo(CarriedItemModification mod) {
		logger.debug("undo "+mod);
		CarriedItem item = itemsByMods.get(mod);
		if (item!=null) {
			model.removeItem(item);
		}
	}

}
