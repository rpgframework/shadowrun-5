/**
 * 
 */
package org.prelle.shadowrun5.common;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.MagicEquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;

/**
 * @author prelle
 *
 */
public class CommonMagicEquipmentController implements MagicEquipmentController {

	private final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/shadowrun/chargen");
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	protected EquipmentController equip;
	protected ShadowrunCharacter model;

	//-------------------------------------------------------------------
	public CommonMagicEquipmentController(ShadowrunCharacter model, EquipmentController equip) {
		if (model==null)
			throw new NullPointerException("Model is null");
		this.model = model;
		this.equip = equip;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (model.getLifestyle().isEmpty())
			ret.add(RES.getString("lifestylegen.todo.missing_primary_lifestyle"));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicEquipmentController#getBindingCost(org.prelle.shadowrun5.items.ItemTemplate, int)
	 */
	@Override
	public int getBindingCost(ItemTemplate focus, int power) {
		ItemSubType sub = focus.getSubtype();
		switch (sub) {
		case FOCI_ENCHANTING: return power*3;
		case FOCI_METAMAGIC: return power*3;
		case FOCI_POWER: return power*6;
		case FOCI_QI:    return power*2;
		case FOCI_SPELL: return power*2;
		case FOCI_SPIRIT: return power*2;
		case FOCI_WEAPON: return power*3;
		default:
			return 0;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicEquipmentController#canBindFocus(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean canBindFocus(CarriedItem focus) {
		if (!focus.isType(ItemType.MAGICAL))
			return false;
		// Is it a focus?
		ItemSubType sub = focus.getItem().getSubtype();
		switch (sub) {
		case FOCI_ENCHANTING:
		case FOCI_METAMAGIC:
		case FOCI_POWER:
		case FOCI_QI:
		case FOCI_SPELL:
		case FOCI_SPIRIT:
		case FOCI_WEAPON:
			break;
		default:
			return false;
		}
		
		if (focus.isBound())
			return false;
		
		// Enough karma for binding
		if (model.getKarmaFree()<getBindingCost(focus.getItem(), focus.getRating()))
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicEquipmentController#canBindFocus(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean canUndoBindFocus(CarriedItem focus) {
		if (!focus.isType(ItemType.MAGICAL))
			return false;
		// Is it a focus?
		ItemSubType sub = focus.getItem().getSubtype();
		switch (sub) {
		case FOCI_ENCHANTING:
		case FOCI_METAMAGIC:
		case FOCI_POWER:
		case FOCI_QI:
		case FOCI_SPELL:
		case FOCI_SPIRIT:
		case FOCI_WEAPON:
			break;
		default:
			return false;
		}
		
		return focus.isBound();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicEquipmentController#bindFocus(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean bindFocus(CarriedItem focus) {
		if (!canBindFocus(focus)) 
			return false;
		
		int cost = getBindingCost(focus.getItem(), focus.getRating());
		logger.info("Bind focus "+focus.getName()+" with power "+focus.getRating()+" for "+cost+" karma");
		focus.setBound(true);
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, focus));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicEquipmentController#undoBindFocus(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean undoBindFocus(CarriedItem focus) {
		if (!canUndoBindFocus(focus)) 
			return false;
		
		int cost = getBindingCost(focus.getItem(), focus.getRating());
		logger.info("Unbind focus "+focus.getName()+" with power "+focus.getRating()+" for "+cost+" karma");
		focus.setBound(false);
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, focus));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		
		return true;
	}

}
