/**
 * 
 */
package org.prelle.shadowrun5.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.LicenseValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.charctrl.SINController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.SINModification;

/**
 * @author prelle
 *
 */
public class CommonSINController implements SINController, GenerationEventListener {

	private final static int PRICE_SIN_LEVEL = 2500;
	private final static int PRICE_LICENSE_LEVEL = 200;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	protected ShadowrunCharacter model;

	//-------------------------------------------------------------------
	public CommonSINController(ShadowrunCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun5.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun5.SIN.Quality, int)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality, int count) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL * count;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#canDeleteSIN(org.prelle.shadowrun5.SIN)
	 */
	@Override
	public boolean canDeleteSIN(SIN data) {
		if (data.getQuality()==Quality.REAL_SIN)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun5.SIN.Quality)
	 */
	@Override
	public SIN createNewSIN(String name, Quality quality) {
		if (!canCreateNewSIN(quality))
			return null;
		
		SIN sin = new SIN(quality);
		sin.setName(name);
		model.addSIN(sin);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SIN_ADDED, sin));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun5.SIN.Quality, int)
	 */
	@Override
	public SIN[] createNewSIN(String name, Quality quality, int count) {
		if (!canCreateNewSIN(quality, count))
			return null;
		
		SIN[] ret = new SIN[count];
		for (int i=1; i<=count; i++) {
			SIN sin = new SIN(quality);
			sin.setName(name+" "+i);
			model.addSIN(sin);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SIN_ADDED, sin));
			ret[i-1]=sin;
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#deleteSIN(org.prelle.shadowrun5.SIN)
	 */
	@Override
	public boolean deleteSIN(SIN data) {
		if (!canDeleteSIN(data)) {
			logger.warn("Cannot delete SIN: "+data);
			return false;
		}
		
		model.removeSIN(data);
		logger.warn("TODO: delete licenses and lifestyles related to the deleted SIN");
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SIN_REMOVED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#apply(org.prelle.shadowrun5.modifications.SINModification)
	 */
	@Override
	public void apply(SINModification mod) {
		SIN sin = new SIN(mod.getQuality());
		sin.setName(model.getRealName());
		sin.setCriminal(mod.isCriminal());
		model.addSIN(sin);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#undo(org.prelle.shadowrun5.modifications.SINModification)
	 */
	@Override
	public void undo(SINModification mod) {
		for (SIN sin : model.getSINs()) {
			if (sin.getQuality()==mod.getQuality()) {
				model.removeSIN(sin);
				return;
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case BASE_DATA_CHANGED:
			logger.debug("RCV "+event);
			/*
			 * If real SIN exist, assign character name
			 */
			for (SIN sin : model.getSINs()) {
				if (sin.getQuality()==Quality.REAL_SIN)
					sin.setName(model.getRealName());
			}
			break;
		default:
		}
	}

	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun5.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewLicense(Quality quality) {
		int cost = quality.getValue()*PRICE_LICENSE_LEVEL;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#canCreateNewLicense(org.prelle.shadowrun5.SIN.Quality, int)
	 */
	@Override
	public boolean canCreateNewLicense(Quality quality, int count) {
		int cost = quality.getValue()*PRICE_LICENSE_LEVEL * count;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#createNewLicense(java.lang.String, org.prelle.shadowrun5.SIN.Quality)
	 */
	@Override
	public LicenseValue createNewLicense(String name, Quality quality) {
		if (!canCreateNewLicense(quality))
			return null;
		
		LicenseValue sin = new LicenseValue();
		sin.setRating(quality);
		sin.setName(name);
		model.addLicense(sin);
		logger.info("Added license "+sin);
		
		// Buy
		model.setNuyen(model.getNuyen() - quality.getValue()*PRICE_LICENSE_LEVEL);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LICENSE_ADDED, sin));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED,null));
		
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SINController#createNewLicense(java.lang.String, org.prelle.shadowrun5.SIN.Quality, int)
	 */
	@Override
	public LicenseValue[] createNewLicense(String name, Quality quality, int count) {
		if (!canCreateNewSIN(quality, count))
			return null;
		
		LicenseValue[] ret = new LicenseValue[count];
		for (int i=1; i<=count; i++) {
			LicenseValue sin = new LicenseValue();
			sin.setRating(quality);
			sin.setName(name);
			model.addLicense(sin);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LICENSE_ADDED, sin));
			ret[i-1]=sin;
			
			// Buy
			model.setNuyen(model.getNuyen() - quality.getValue()*PRICE_LICENSE_LEVEL);
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED,null));
		
		return ret;
	}

}
