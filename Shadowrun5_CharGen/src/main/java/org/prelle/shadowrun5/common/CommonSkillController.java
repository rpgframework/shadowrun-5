package org.prelle.shadowrun5.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;

public abstract class CommonSkillController implements SkillController {

	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	protected ShadowrunCharacter model;
	protected CharacterController parent;
	protected List<SkillGroup> recSkillGroups;
	protected List<Skill> recSkills;
	protected List<SkillGroup> avSkillGroups;
	protected List<Skill> avSkills;
	protected List<SkillGroup> incSkillGroups;

	//--------------------------------------------------------------------
	public CommonSkillController(CharacterController parent) {
		this.parent = parent;
		avSkillGroups = new ArrayList<>();
		avSkills      = new ArrayList<>();
		recSkillGroups = new ArrayList<>();
		recSkills      = new ArrayList<>();
		incSkillGroups = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		logger.trace("START updateAvailable");
		avSkillGroups.clear();
		if (getPointsLeftSkillGroups()>0 || model.getKarmaFree()>=5) {
			avSkillGroups.addAll(ShadowrunCore.getSkillgroups());
			// Remove those the character already has
			for (SkillGroupValue val : model.getSkillGroupValues()) {
				avSkillGroups.remove(val.getModifyable());
			}
			// Remove those groups character is incompetent in
			for (SkillGroup val : incSkillGroups) {
				avSkillGroups.remove(val);
			}
			// Remove those groups the character has already a single skill from
			for (SkillGroup val : new ArrayList<>(avSkillGroups)) {
				for (Skill skill : ShadowrunCore.getSkills(val)) {
					if (model.getSkillValue(skill)!=null) {
						avSkillGroups.remove(val);
					}
				}
			}
		}
		

		avSkills.clear();
		avSkills.addAll(ShadowrunCore.getSkills());
		// Remove those the character already has
		for (SkillValue val : model.getSkillValues(true)) {
			// Keep knowledges and lamguages
			if (val.getModifyable().getType()==SkillType.KNOWLEDGE)
				continue;
			if (val.getModifyable().getType()==SkillType.LANGUAGE && !val.getModifyable().getId().contains("native_language"))
				continue;
			logger.trace("Not available anymore "+val);
			avSkills.remove(val.getModifyable());
		}
		// Remove those skills of groups character is incompetent in
		for (SkillGroup val : incSkillGroups) {
			for (Skill skl : ShadowrunCore.getSkills(val)) {
				logger.trace("Not available anymore "+skl);
				avSkills.remove(skl);
			}
		}
		// Remove second native language skill, if character hasn't the "Bilingual' quality
//		if (model.get)
		
		// for aspected magicians remove other magical skill groups
		if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().getId().equals("aspectedmagician")) {
			// Detect already selected skillgroup
			SkillGroup magicSkillGroup = null;
			for (SkillGroupValue grp : model.getSkillGroupValues()) {
				if (grp.getModifyable().getType()==SkillType.MAGIC) { 
					magicSkillGroup = grp.getModifyable();
					break;
				}
			}
			logger.debug("  this aspected magician already decided to use skill group "+magicSkillGroup);
			
			for (SkillGroup val : ShadowrunCore.getSkillgroups()) {
				if (val.getType()!=SkillType.MAGIC) 
					continue;
				if (magicSkillGroup!=null && val!=magicSkillGroup) {			
					logger.debug("  .. cannot select group "+val);
					avSkillGroups.remove(val);
					// Remove its skills
					for (Skill skill : ShadowrunCore.getSkills(val)) {
						logger.debug("  .. cannot select groups skill "+skill);
						avSkills.remove(skill);
					}
				}
			}
		}

		Collections.sort(avSkills);
		Collections.sort(avSkillGroups);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, avSkills, avSkillGroups));
		logger.trace("STOP updateAvailable");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#apply(org.prelle.shadowrun5.modifications.SkillModification)
	 */
	@Override
	public void apply(SkillModification mod) {
		boolean added = false;
		SkillValue val = model.getSkillValue(mod.getSkill());
		if (val==null) {
			val = new SkillValue(mod.getSkill(), 0);
			logger.debug("Add skill "+val);
			model.addSkill(val);
		
			added = true;
			updateAvailable();
		}
		
		// Fire event
		model.getSkillValue(mod.getSkill()).addModification(mod);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(added?GenerationEventType.SKILL_ADDED:GenerationEventType.SKILL_CHANGED, val));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#undo(org.prelle.shadowrun5.modifications.SkillModification)
	 */
	@Override
	public void undo(SkillModification mod) {
		SkillValue val = model.getSkillValue(mod.getSkill());
		if (val==null) {
			logger.error("Trying to remove modification for non-existing skill");
			return;
		}
		
		if (!val.getModifications().contains(mod)) {
			logger.error("Trying to remove modification currently not present in skill value "+mod);
		}
		val.removeModification(mod);
		
		if (val.getPoints()==0 && val.getModifications().isEmpty()) {
			logger.debug("Remove skill "+val);
			model.removeSkill(val);
			
			// Fire event
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, val));
			updateAvailable();
		} else {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, val));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#apply(org.prelle.shadowrun5.modifications.SkillGroupModification)
	 */
	@Override
	public void apply(SkillGroupModification mod) {
		boolean added = false;
		SkillGroupValue val = model.getSkillGroupValue(mod.getSkillGroup());
		if (val==null) {
			val = new SkillGroupValue(mod.getSkillGroup(), 0);
			logger.debug("Add skillgroup "+val);
			model.addSkillGroup(val);
		
			added = true;
			updateAvailable();
		}
		
		// Fire event
		model.getSkillGroupValue(mod.getSkillGroup()).addModification(mod);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(added?GenerationEventType.SKILLGROUP_ADDED:GenerationEventType.SKILLGROUP_CHANGED, val));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#undo(org.prelle.shadowrun5.modifications.SkillModification)
	 */
	@Override
	public void undo(SkillGroupModification mod) {
		SkillGroupValue val = model.getSkillGroupValue(mod.getSkillGroup());
		if (val==null) {
			logger.error("Trying to remove modification for non-existing skillgrouo");
			return;
		}
		
		if (!val.getModifications().contains(mod)) {
			logger.error("Trying to remove modification currently not present in skill value "+mod);
		}
		val.removeModification(mod);
		
		if (val.getPoints()==0 && val.getModifications().isEmpty()) {
			logger.debug("Remove skill "+val);
			model.removeSkillGroup(val);
			
			// Fire event
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_REMOVED, val));
			updateAvailable();
		} else {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_CHANGED, val));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#apply(org.prelle.shadowrun5.modifications.SkillGroupModification)
	 */
	@Override
	public void apply(IncompetentSkillGroupModification mod) {
		incSkillGroups.add(mod.getSkillGroup());
		updateAvailable();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#undo(org.prelle.shadowrun5.modifications.SkillModification)
	 */
	@Override
	public void undo(IncompetentSkillGroupModification mod) {
		incSkillGroups.remove(mod.getSkillGroup());
		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getAvailableSkillGroups()
	 */
	@Override
	public List<SkillGroup> getAvailableSkillGroups() {
		return avSkillGroups;
	}

	//-------------------------------------------------------------------
	public List<SkillGroup> getAvailableSkillGroups(SkillType type) {
		return avSkillGroups.stream().filter(p -> p.getType()==type ).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills() {
		return avSkills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills(SkillType type) {
		return avSkills.stream().filter(p -> p.getType()==type ).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public SkillGroupValue select(SkillGroup grp) {
		logger.debug("select "+grp);
		
		if (!canBeSelected(grp) || grp==null) {
			logger.warn("Trying to select skillgroup "+grp+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected skillgroup "+grp);
		SkillGroupValue sVal = new SkillGroupValue(grp, 1);
		model.addSkillGroup(sVal);
	
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLGROUPS, null, getPointsLeftSkillGroups()));
		updateAvailable();
		
		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public SkillValue select(Skill skill) {
		logger.debug("select "+skill);
		if (skill==null)
			throw new NullPointerException("Parameter is null");
		
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected skill "+skill);
		SkillValue sVal = new SkillValue(skill, 1);
		model.addSkill(sVal);
	
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		updateAvailable();
		
		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean increase(SkillGroupValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref))
			return false;
	
		// Change model
		ref.setPoints(ref.getPoints()+1);
		logger.info("Skillgroup "+ref.getModifyable().getId()+" raised to "+ref.getPoints());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLGROUPS, null, getPointsLeftSkillGroups()));
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref)) {
			logger.warn("Trying to increase a skill which cannot be increased: "+ref);
			return false;
		}
	
		// Change model
		ref.setPoints(ref.getPoints()+1);
		logger.info("Increase skill "+ref.getModifyable().getId()+" to "+ref.getPoints());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#decrease(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean decrease(SkillGroupValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;
	
		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getPoints()==0)
			model.removeSkillGroup(ref);
		
		// Inform listeners
		if (ref.getPoints()==0)
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_REMOVED, ref));
		else
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLGROUPS, null, getPointsLeftSkillGroups()));
		
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#decrease(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;
	
		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getModifiedValue()==0)
			model.removeSkill(ref);
		
		// Inform listeners
		if (ref.getModifiedValue()==0)
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, ref));
		else
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec) {
		if (!canBeSelected(ref, spec))
			return null;
		
		logger.info("Select specialization "+spec+" of "+ref);
		SkillSpecializationValue ret = new SkillSpecializationValue(spec);
		ref.getSkillSpecializations().add(ret);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
	
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean deselect(SkillGroupValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
		
		// Change model
		ref.setPoints(0);
		model.removeSkillGroup(ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean deselect(SkillValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
	
		// Change model
		ref.setPoints(0);
		model.removeSkill(ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean deselect(SkillValue ref, SkillSpecialization spec) {
		if (!canBeDeselected(ref, spec))
			return false;

		logger.info("Deselect specialization "+spec+" from "+ref);
		SkillSpecializationValue val = ref.getSpecialization(spec);
		if (val==null) {
			logger.error("Could not find specialization "+spec+" in "+ref.getSkillSpecializations());
			return false;
		}
		ref.removeSpecialization(val);

		logger.debug("Result = "+ref.getSkillSpecializations());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#isRecommended(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public boolean isRecommended(SkillGroup val) {
		return recSkillGroups.contains(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#isRecommended(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public boolean isRecommended(Skill val) {
		return recSkills.contains(val);
	}

	//--------------------------------------------------------------------
	/**
	 * Break a skillgroup into individual skills with the same value
	 * @see org.prelle.shadowrun5.charctrl.SkillController#breakSkillGroup(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public void breakSkillGroup(SkillGroupValue ref) {
		logger.info("breakSkillGroup "+ref.getModifyable());
		if (!canBreakSkillGroup(ref)) {
			logger.warn("Cannot perform breaking skill group into seperate skills");
			return;
		}
		
		// Change model
		model.removeSkillGroup(ref);
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILLGROUP_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLGROUPS, null, getPointsLeftSkillGroups()));
	
		for (Skill skill : ShadowrunCore.getSkills(ref.getModifyable())) {
			SkillValue sVal = new SkillValue(skill, ref.getPoints());
			sVal.setStart(ref.getStart());
			model.addSkill(sVal);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_ADDED, sVal));
		}
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		
		updateAvailable();
		parent.runProcessors();
	}

}