/**
 * 
 */
package org.prelle.shadowrun5.common;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author Stefan
 *
 */
public class DerivedAttributeCalculator implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	private ShadowrunCharacter model;
	
	//--------------------------------------------------------------------
	public DerivedAttributeCalculator() {
		GenerationEventDispatcher.addListener(this);
	}
	
	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		calculate();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()!=GenerationEventType.ATTRIBUTE_CHANGED)
			return;
		if (!Arrays.asList(Attribute.primaryValues()).contains(event.getKey()))
			return;
		if (logger.isTraceEnabled())
			logger.trace("RCV "+event);
		
		calculate();
	}

	//--------------------------------------------------------------------
	public void calculate() {
		if (model==null)
			return;
		
		AttributeValue val = null;
		
		/*
		 * Physical initiative
		 */
		int ini = 
				model.getAttribute(Attribute.REACTION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_PHYSICAL);
		if (ini!=val.getPoints()) {
			val.setPoints(ini);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, Attribute.INITIATIVE_PHYSICAL, val));
		}
		
		/*
		 * astral initiative
		 */
		ini = 
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_ASTRAL);
		if (ini!=val.getPoints()) {
			val.setPoints(ini);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * matrix initiative
		 */
		ini = 
				model.getAttribute(Attribute.REACTION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_MATRIX);
		if (ini!=val.getPoints()) {
			val.setPoints(ini);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * physical limit
		 */
		int sum = (
				model.getAttribute(Attribute.STRENGTH).getModifiedValue()*2+
				model.getAttribute(Attribute.BODY).getModifiedValue()+
				model.getAttribute(Attribute.REACTION).getModifiedValue()
				);
		int limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_PHYSICAL);
		if (limit!=val.getPoints()) {
			val.setPoints(limit);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * mental limit
		 */
		sum = (
				model.getAttribute(Attribute.LOGIC).getModifiedValue()*2+
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				model.getAttribute(Attribute.WILLPOWER).getModifiedValue()
				);
		limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_MENTAL);
		if (limit!=val.getPoints()) {
			val.setPoints(limit);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * social limit
		 */
		sum = (
				model.getAttribute(Attribute.CHARISMA).getModifiedValue()*2+
				model.getAttribute(Attribute.WILLPOWER).getModifiedValue()+
				model.getAttribute(Attribute.ESSENCE).getModifiedValue()
				);
		limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_SOCIAL);
		if (limit!=val.getPoints()) {
			val.setPoints(limit);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * astral limit
		 */
		sum = (
		limit = Math.max(model.getAttribute(Attribute.LIMIT_MENTAL).getModifiedValue(), model.getAttribute(Attribute.LIMIT_SOCIAL).getModifiedValue()));
		val = model.getAttribute(Attribute.LIMIT_ASTRAL);
		if (limit!=val.getPoints()) {
			val.setPoints(limit);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * Composure
		 */
		sum = model.getAttribute(Attribute.CHARISMA).getModifiedValue() + model.getAttribute(Attribute.WILLPOWER).getModifiedValue();
		val = model.getAttribute(Attribute.COMPOSURE);
		if (sum!=val.getPoints()) {
			val.setPoints(sum);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * Judge intentions
		 */
		sum = model.getAttribute(Attribute.CHARISMA).getModifiedValue() + model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.JUDGE_INTENTIONS);
		if (sum!=val.getPoints()) {
			val.setPoints(sum);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * lifting/carrying
		 */
		sum = model.getAttribute(Attribute.BODY).getModifiedValue() + model.getAttribute(Attribute.STRENGTH).getModifiedValue();
		val = model.getAttribute(Attribute.LIFT_CARRY);
		if (sum!=val.getPoints()) {
			val.setPoints(sum);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
		
		/*
		 * lifting/carrying
		 */
		sum = model.getAttribute(Attribute.LOGIC).getModifiedValue() + model.getAttribute(Attribute.WILLPOWER).getModifiedValue();
		val = model.getAttribute(Attribute.MEMORY);
		if (sum!=val.getPoints()) {
			val.setPoints(sum);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val));
		}
	}

}
