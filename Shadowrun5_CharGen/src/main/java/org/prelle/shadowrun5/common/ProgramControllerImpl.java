/**
 * 
 */
package org.prelle.shadowrun5.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Program.ProgramType;
import org.prelle.shadowrun5.charctrl.ProgramController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemTemplate.Legality;

/**
 * @author prelle
 *
 */
public class ProgramControllerImpl implements ProgramController {

	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private ShadowrunCharacter model;
	private CarriedItem item;

	//-------------------------------------------------------------------
	/**
	 */
	public ProgramControllerImpl(ShadowrunCharacter model, CarriedItem item) {
		this.item = item;
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#getAvailable(org.prelle.shadowrun5.Program.ProgramType)
	 */
	@Override
	public List<Program> getAvailable(ProgramType type) {
		logger.debug("getAvailable("+type+")");
		List<Program> avail = new ArrayList<Program>();
		outer:
		for (Program prog : ShadowrunCore.getPrograms()) {
			if (prog.getType()!=type)
				continue;
			// Is it already selected?
			for (ProgramValue val : item.getPrograms()) {
				if (val.getModifyable()==prog)
					continue outer;
			}
			avail.add(prog);
		}
		return avail;
	}

	//-------------------------------------------------------------------
	@Override
	public int getPrice(Program value) {
		switch (value.getType()) {
		case STANDARD: return 80;
		case HACKING : return 250;
		case AGENT   : return 6000; // eigentlich Stufe *X
		}
		return 0;
	}

	//-------------------------------------------------------------------
	@Override
	public Availability getAvailability(Program value) {
		switch (value.getType()) {
		case STANDARD: return new Availability(0, false);
		case HACKING : return new Availability(6, Legality.RESTRICTED, false);
		case AGENT   : return new Availability(18, false); // eigentlich Stufe *3
		}
		return new Availability(0, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#canBeSelected(org.prelle.shadowrun5.Program)
	 */
	@Override
	public boolean canBeSelected(Program prog) {
		for (ProgramValue val : item.getPrograms()) {
			if (val.getModifyable()==prog)
				return false;
		}
		
		return model.getNuyen()>getPrice(prog);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#select(org.prelle.shadowrun5.Program)
	 */
	@Override
	public ProgramValue select(Program val) {
		if (!canBeSelected(val))
			return null;
		
		logger.info("Select program "+val.getId()+" for "+item);
		ProgramValue prog = new ProgramValue(val);
		item.addProgram(prog);
		model.setNuyen(model.getNuyen() - getPrice(val));
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, item));
		return prog;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#canBeDeselected(org.prelle.shadowrun5.ProgramValue)
	 */
	@Override
	public boolean canBeDeselected(ProgramValue prog) {
		return item.getPrograms().contains(prog);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#deselect(org.prelle.shadowrun5.ProgramValue)
	 */
	@Override
	public void deselect(ProgramValue val) {
		if (!canBeDeselected(val))
			return;
		
		logger.info("Deselect program "+val.getName()+" from "+item);
		item.removeProgram(val);
		model.setNuyen(model.getNuyen() + getPrice(val.getModifyable()));
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, item));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, item));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ProgramController#getMatrixItem()
	 */
	@Override
	public CarriedItem getMatrixItem() {
		return item;
	}

}
