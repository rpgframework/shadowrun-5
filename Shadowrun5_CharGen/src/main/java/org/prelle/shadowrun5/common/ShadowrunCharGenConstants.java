/**
 * 
 */
package org.prelle.shadowrun5.common;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface ShadowrunCharGenConstants {

	public final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/shadowrun/chargen/i18n/chargen");

}
