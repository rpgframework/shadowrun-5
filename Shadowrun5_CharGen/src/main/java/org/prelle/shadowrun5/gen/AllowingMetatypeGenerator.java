/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MetatypeController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;

/**
 * @author Stefan
 *
 */
public class AllowingMetatypeGenerator implements MetatypeController {

	private ShadowrunCharacter model;
	private List<MetaTypeOption> available;
	
	//--------------------------------------------------------------------
	/**
	 */
	public AllowingMetatypeGenerator(ShadowrunCharacter model) {
		this.model = model;
		available = new ArrayList<MetaTypeOption>();
		for (MetaType type : ShadowrunCore.getMetaTypes())
			available.add(new MetaTypeOption(type));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getAvailable()
	 */
	@Override
	public List<MetaTypeOption> getAvailable() {
		return available;
	}
	
	//--------------------------------------------------------------------
	public int getSpecialAttributes(MetaType type) {return 0; }
	
	//--------------------------------------------------------------------
	public int getKarmaCost(MetaType type) { return 0; }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#canBeSelected(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public boolean canBeSelected(MetaType type) {
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#select(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public void select(MetaType value) {
		model.setMetatype(value);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
	}

}
