/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.List;

import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.charctrl.MetatypeController;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.HardcopyPluginData;

/**
 * @author Stefan
 *
 */
public interface CharacterGenerator extends CharacterController, HardcopyPluginData {

	//-------------------------------------------------------------------
	public void attachConfigurationTree(ConfigContainer addBelow);
	
	//-------------------------------------------------------------------
	public void start(ShadowrunCharacter model);
	
	//-------------------------------------------------------------------
	public void startTuningMode();
	
	//-------------------------------------------------------------------
	public void stop();
	
	//-------------------------------------------------------------------
	public WizardPageType[] getWizardPages();
	
	//-------------------------------------------------------------------
	public boolean hasEnoughData();

	//-------------------------------------------------------------------
	public List<String> getToDos();

	//-------------------------------------------------------------------
	public void setConcept(CharacterConcept concept);

	//-------------------------------------------------------------------
	public CharacterConcept getConcept();

	//-------------------------------------------------------------------
	public MetatypeController getMetatypeController();

	//-------------------------------------------------------------------
	public MagicOrResonanceController getMagicOrResonanceController();
	
}
