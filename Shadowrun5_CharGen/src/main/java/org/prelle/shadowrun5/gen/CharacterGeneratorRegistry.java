
/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;

/**
 * @author prelle
 *
 */
public class CharacterGeneratorRegistry {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private static List<CharacterGenerator> generators;
	private static CharacterGenerator selected;
	private static ConfigContainer addBelow;
	
	//-------------------------------------------------------------------
	static {
		generators = new ArrayList<CharacterGenerator>();
	}
	
	//-------------------------------------------------------------------
	public static void attachConfigurationTree(ConfigContainer value) {
		addBelow = value;
	}

	//-------------------------------------------------------------------
	public static void register(CharacterGenerator charGen) {
		logger.info("register "+charGen+"********************************************************");
		if (!generators.contains(charGen)) {
			generators.add(charGen);
			if (addBelow!=null)
				charGen.attachConfigurationTree(addBelow);
		}
//		throw new RuntimeException("Rtrace");
	}

	//-------------------------------------------------------------------
	public static List<CharacterGenerator> getGenerators() {
		return new ArrayList<>(generators);
	}

	//-------------------------------------------------------------------
	public static void select(CharacterGenerator charGen, ShadowrunCharacter model) {
		CharacterGeneratorRegistry.selected = charGen;
		if (charGen!=null) {
			logger.info(charGen.getClass()+" as generator selected");
			logger.debug("Call "+charGen.getClass()+".start");
			charGen.start(model);
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
		}
	}

	//--------------------------------------------------------------------
	public static CharacterGenerator getSelected() {
		return selected;
	}

}
