/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.charctrl.DecisionToMake;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.LifestyleController;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.charctrl.MetatypeController;
import org.prelle.shadowrun5.charctrl.ProgramController;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.charctrl.SINController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.common.ProgramControllerImpl;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.CarriedItemModification;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.modifications.ModificationChoice;
import org.prelle.shadowrun5.modifications.QualityModification;
import org.prelle.shadowrun5.modifications.SINModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.ConfigContainer;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class CommonSR5CharacterGenerator extends BasePluginData implements CharacterGenerator, GenerationEventListener {

	protected final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private static final Logger logger = LogManager.getLogger("shadowrun.gen");


	protected ShadowrunCharacter model;
	protected CharGenMode mode;
	protected SR5LetUserChooseListener callback;

	protected CharacterConcept concept;
	protected MetatypeController meta;
	protected MagicOrResonanceController magOrRes;
	protected AttributeController attrib;
	protected SkillController    skill;
	protected SpellController    spell;
	protected SpellController    alchemy;
	protected RitualController   ritual;
	protected QualityController  quality;
	protected AdeptPowerController power;
	protected ComplexFormController cforms;
	protected EquipmentController   equip;
	protected ConnectionsController connections;
	protected SINController       sins;
	protected LifestyleController lifestyles;
	protected SummonableController summonables;

	protected List<CharacterProcessor> processChain;
	protected Map<Modification, DecisionToMake> decisions;

	protected List<Modification> unitTestModifications;

	//-------------------------------------------------------------------
	protected CommonSR5CharacterGenerator() {
		mode = CharGenMode.CREATING;
		processChain = new ArrayList<>();
		decisions    = new HashMap<>();
		unitTestModifications = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		unitTestModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		unitTestModifications.remove(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		// To override in child classes
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		logger.info("START: runProcessors: "+processChain.size()+"-------------------------------------------------------");
		List<Modification> unprocessed = new ArrayList<>(unitTestModifications);
		for (CharacterProcessor processor : processChain) {
			unprocessed = processor.process(model, unprocessed);
			logger.debug("------karma is "+model.getKarmaFree()+" after "+processor.getClass().getSimpleName()+"     "+unprocessed);
		}
		logger.info("Remaining mods  = "+unprocessed);
		logger.info("Remaining karma = "+model.getKarmaFree());
		logger.info("ToDos = "+getToDos());
		logger.info("STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
	}

	//-------------------------------------------------------------------


	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return mode;
	}

	//-------------------------------------------------------------------
	private List<Modification> expandTyped(SkillModification mod) {
		List<Modification> ret = new ArrayList<Modification>();

		for (Skill skill : ShadowrunCore.getSkills(mod.getType())) {
			ret.add(new SkillModification(skill, mod.getValue()));
		}

		logger.warn("Expand "+mod+" to "+ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#apply(java.util.Collection)
	 */
	@Override
	public Collection<Modification> apply(String reason, Collection<Modification> mods) {
		logger.debug("Apply "+mods);
		for (Modification mod : mods) {
			if (mod instanceof AttributeModification) {
				attrib.apply((AttributeModification)mod);
			} else if (mod instanceof SINModification) {
				sins.apply( (SINModification)mod );
			} else if (mod instanceof ModificationChoice) {
				if (callback!=null) {
					Collection<Modification> realChoice = callback.letUserChoose(reason, (ModificationChoice)mod);
					apply(reason, realChoice);
					return realChoice;
				} else {
					logger.error("Cannot ask users choice for "+reason+", because callback is not set");
					throw new IllegalStateException("Cannot ask users choice for "+reason+", because callback is not set");
				}
			} else if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getType()!=null && sMod.getSkill()==null) {
					ModificationChoice choice = new ModificationChoice(expandTyped(sMod),1);
					if (callback!=null) {
						apply(reason, callback.letUserChoose(reason, choice));
					} else {
						logger.error("Cannot ask users choice for "+reason+", because callback is not set");
						throw new IllegalStateException("Cannot ask users choice for "+reason+", because callback is not set");
					}
				} else {
					skill.apply(sMod);
				}
			} else if (mod instanceof SkillGroupModification) {
				skill.apply((SkillGroupModification)mod );
			} else if (mod instanceof LifestyleCostModification) {
				lifestyles.apply((LifestyleCostModification)mod );
			} else if (mod instanceof IncompetentSkillGroupModification) {
				skill.apply( (IncompetentSkillGroupModification)mod );
			} else if (mod instanceof QualityModification) {
				quality.apply( (QualityModification)mod );
			} else if (mod instanceof CarriedItemModification) {
				equip.apply( (CarriedItemModification)mod );
			} else {
				logger.warn("Don't know how to apply "+mod);
				System.err.println("Don't know how to apply "+mod.getClass());
				System.exit(0);
			}
		}
		return mods;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#undo(java.util.Collection)
	 */
	@Override
	public void undo(Collection<Modification> mods) {
		logger.debug("Undo "+mods);
		for (Modification mod : mods) {
			if (mod instanceof AttributeModification) {
				attrib.undo((AttributeModification)mod);
			} else if (mod instanceof SINModification) {
				sins.undo( (SINModification)mod );
			} else if (mod instanceof LifestyleCostModification) {
				lifestyles.undo((LifestyleCostModification)mod );
			} else if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getType()!=null && sMod.getSkill()==null)
					continue;
				skill.undo(sMod);
			} else if (mod instanceof SkillGroupModification) {
				skill.undo((SkillGroupModification)mod );
			} else if (mod instanceof ModificationChoice) {
			} else if (mod instanceof IncompetentSkillGroupModification) {
				skill.undo( (IncompetentSkillGroupModification)mod );
			} else if (mod instanceof QualityModification) {
				quality.undo( (QualityModification)mod );
			} else {
				logger.warn("Don't know how to undo "+mod.getClass());
				System.err.println("Don't know how to undo "+mod.getClass());
				System.exit(0);
			}
		}
	}

	//	//--------------------------------------------------------------------
	//	/**
	//	 * @see org.prelle.shadowrun.gen.CharacterGenerator#start(org.prelle.shadowrun.ShadowrunCharacter)
	//	 */
	//	@Override
	//	public void start(ShadowrunCharacter model) {
	//		this.model = model;
	//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getHelpText()
	 */
	@Override
	public String getHelpText() {
		if (i18nHelp==null)
			return null;
		String key = getHelpI18NKey();

		try {
			return i18nHelp.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18nHelp.getBaseBundleName());
			return "Missing property '"+key+"' in "+i18nHelp.getBaseBundleName();
		}
		//		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#startTuningMode()
	 */
	@Override
	public void startTuningMode() {
		logger.warn("Is there really nothing to do for switching in tuning mode");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		if (meta!=null)
			GenerationEventDispatcher.removeListener(meta);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getMagicOrResonanceController()
	 */
	@Override
	public MagicOrResonanceController getMagicOrResonanceController() {
		return magOrRes;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getMetatypeController()
	 */
	@Override
	public MetatypeController getMetatypeController() {
		return meta;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attrib;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getSpellController()
	 */
	@Override
	public SpellController getSpellController() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getAlchemyController()
	 */
	@Override
	public SpellController getAlchemyController() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getRitualController()
	 */
	@Override
	public RitualController getRitualController() {
		return ritual;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getQualityController()
	 */
	@Override
	public QualityController getQualityController() {
		return quality;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getPowerController()
	 */
	@Override
	public AdeptPowerController getPowerController() {
		return power;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getComplexFormController()
	 */
	@Override
	public ComplexFormController getComplexFormController() {
		return cforms;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getEquipmentController()
	 */
	@Override
	public EquipmentController getEquipmentController() {
		return equip;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getConnectionController()
	 */
	@Override
	public ConnectionsController getConnectionController() {
		return connections;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSINController()
	 */
	@Override
	public SINController getSINController() {
		return sins;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getLifestyleController()
	 */
	@Override
	public LifestyleController getLifestyleController() {
		return lifestyles;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSummonableController()
	 */
	@Override
	public SummonableController getSummonableController() {
		return summonables;
	}

	//--------------------------------------------------------------------
	public CharacterConcept getConcept() {
		return concept;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#setConcept(org.prelle.shadowrun5.CharacterConcept)
	 */
	@Override
	public void setConcept(CharacterConcept concept) {
		if (this.concept==concept)
			// Nothing changed
			return;

		if (this.concept!=null) {
			// Remove old concept
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.CHARACTERCONCEPT_REMOVED, this.concept));
		}

		this.concept = concept;
		if (concept!=null) {
			logger.info("Select concept "+concept);
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.CHARACTERCONCEPT_ADDED, concept));
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent ev) {
		switch (ev.getType()) {
		case QUALITY_ADDED:
			QualityValue qalAdd = (QualityValue)ev.getKey();
			logger.debug("Add modifications: "+qalAdd.getModifications());
			apply(qalAdd.getName(), qalAdd.getModifications());
			break;
		case QUALITY_REMOVED:
			QualityValue qalRem = (QualityValue)ev.getKey();
			undo(qalRem.getModifications());
			break;
		case ATTRIBUTE_CHANGED:
		case MAGICORRESONANCE_AVAILABLE_CHANGED:
		case MAGICORRESONANCE_CHANGED:
		case POINTS_LEFT_ATTRIBUTES:
		case POINTS_LEFT_QUALITIES:
		case POINTS_LEFT_POWERS:
		case POINTS_LEFT_SKILLGROUPS:
		case POINTS_LEFT_SKILLS:
		case POINTS_LEFT_SPELLS_RITUALS:
		case PRIORITY_CHANGED:
		case SKILLGROUP_CHANGED:
		case SKILL_CHANGED:
			break;
		default:
//			logger.info("Ignore "+ev);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#setCallback(org.prelle.shadowrun5.gen.SR5LetUserChooseListener)
	 */
	@Override
	public void setCallback(SR5LetUserChooseListener callback) {
		this.callback = callback;
	}

	//-------------------------------------------------------------------
	public ProgramController getProgramController(CarriedItem item) {
		return new ProgramControllerImpl(getCharacter(), item);
	}
}
