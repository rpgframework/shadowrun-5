/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.MentorSpirit;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.Recommendation;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.Quality.QualityType;
import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.QualityModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class DeleteMeQualityGenerator implements QualityController, GenerationEventListener {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	
	private List<Quality> recQualities;
	
	private List<Quality> avQualities;
	
	private int pointsQualities;

	//--------------------------------------------------------------------
	/**
	 */
	public DeleteMeQualityGenerator(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		avQualities       = new ArrayList<Quality>();
		recQualities      = new ArrayList<Quality>();
		updateAvailable();
	}
	
//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun.charctrl.Controller#getParentController()
//	 */
//	@Override
//	public CharacterController getParentController() {
//		return parent;
//	}

	//-------------------------------------------------------------------
	public void setData(int ptQualities) {
		pointsQualities      = ptQualities;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getPointsInNegativeQualities()
	 */
	public int getPointsInNegativeQualities() {
		int sum = 0;
		for (QualityValue sVal : model.getQualities()) {
			if (sVal.getModifyable().getType()==QualityType.NEGATIVE)
				sum += sVal.getModifyable().getCost() * sVal.getPoints();
		}
		return sum;
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("START updateAvailable");
		avQualities.clear();
		avQualities.addAll(ShadowrunCore.getQualities());
		// Remove those the character already has
		for (QualityValue val : model.getQualities()) {
			if (val.getModifyable().isMultipleSelectable())
				continue;
			logger.trace("Not available anymore "+val);
			avQualities.remove(val.getModifyable());
		}
		// Remove second native language skill, if character hasn't the "Bilingual' quality
//		if (model.get)
		
		Collections.sort(avQualities);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, avQualities));
		logger.debug("STOP updateAvailable");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getAvailableQualities()
	 */
	@Override
	public List<Quality> getAvailableQualities() {
		return avQualities;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeSelected(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public boolean canBeSelected(Quality data) {
		if (data==null)
			throw new NullPointerException("Quality not set");
		// Not already selected
		if (model.getQualities().stream().anyMatch(qval -> (qval.getModifyable()==data && !data.isMultipleSelectable()))) {
			return false;
		}
		// Points left
		if (data.getType()==QualityType.POSITIVE)
			return model.getKarmaFree()>=data.getCost();
		else
			return (getPointsInNegativeQualities()+data.getCost())<=25;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#select(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public QualityValue select(Quality data) {
		logger.debug("select "+data);
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected quality "+data);
		QualityValue sVal = new QualityValue(data, 1);
		model.addQuality(sVal);
		int cost = data.getCost();
		if (parent.getMode()==CharGenMode.LEVELING)
			cost *= 2;
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()-cost);
		else
			model.setKarmaFree(model.getKarmaFree()+cost);

		// Apply modifications
		parent.apply(data.getName(), data.getModifications());
		
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		updateAvailable();
		
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#select(org.prelle.shadowrun5.Quality, java.lang.Object)
	 */
	@Override
	public QualityValue select(Quality data, Object choice) {
		logger.debug("select '"+data.getId()+"' with choice "+choice);
		if (!data.needsChoice())
			throw new IllegalArgumentException("Quality "+data+" does not need a choice");
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}
		
		// Change model
		QualityValue sVal = new QualityValue(data, 1);
		sVal.setChoice(choice);
		switch (data.getSelect()) {
		case ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
		case LIMIT:
			sVal.setChoiceReference( ((Attribute)choice).name() );
			break;
		case SKILL:
		case COMBAT_SKILL:
			sVal.setChoiceReference( ((Skill)choice).getId());
			break;
		case MATRIX_ACTION:
			sVal.setChoiceReference( ((ShadowrunAction)choice).getId());
			break;
		case MENTOR_SPIRIT:
			sVal.setChoiceReference( ((MentorSpirit)choice).getId());
			break;
		case SKILLGROUP:
			sVal.setChoiceReference( ((SkillGroup)choice).getId());
			break;
		default:
			logger.error("Don't know how to set choice reference for "+data.getSelect()+" / "+choice.getClass());
		}
		
		
		model.addQuality(sVal);
		int cost = data.getCost();
		if (parent.getMode()==CharGenMode.LEVELING)
			cost *= 2;
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()-cost);
		else
			model.setKarmaFree(model.getKarmaFree()+cost);
		
		/*
		 * When an quality needs a choice to be made, all modifications attached 
		 * to the quality needs to be scanned to be modified accordingly
		 */
		logger.debug("Unresolved modifications = "+data.getModifications());
		
		List<Modification> toAdd = ShadowrunTools.resolveModifications(data, choice);
		logger.debug("Resolved modifications = "+toAdd);
		for (Modification mod : toAdd) {
			mod.setSource(sVal);
			sVal.addModification(mod);
			logger.debug("  apply modification "+mod);
			ShadowrunTools.applyModification(model, mod);
			if (mod instanceof AttributeModification) {
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, ((AttributeModification)mod).getAttribute(), model.getAttribute(((AttributeModification)mod).getAttribute())));
			} else
				logger.warn("Don't know if I should generate a GenerationEvent for "+mod.getClass());
		}
		logger.info("Selected quality "+data+" with modifications "+sVal.getModifications());

		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		updateAvailable();
		
		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeDeselected(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeDeselected(QualityValue ref) {
		return model.getQualities().contains(ref);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeIncreased(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeIncreased(QualityValue ref) {
		Quality data = ref.getModifyable();
		logger.trace("canBeIncreased("+ref+") with max="+data.getMax()+"   current="+ref.getPoints());
		if (data.getMax()<2) {
			return false;
		}
		
		// Maximum not reached yet
		if (ref.getPoints()>=data.getMax()) {
			return false;
		}
	
		if (data.getType()==QualityType.POSITIVE) {
			// Enough points
			if (model.getKarmaFree()<data.getCost()) {
				return false;
			}
		} else {
			// May not exceed 25 points from negative qualities
			if ( (getPointsInNegativeQualities()+data.getCost())>25)
				return false;
		}
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#canBeDecreased(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean canBeDecreased(QualityValue ref) {
		Quality data = ref.getModifyable();
		if (data.getMax()<2)
			return false;

		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#increase(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean increase(QualityValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()+1);

		Quality data = ref.getModifyable();
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()-data.getCost());
		else
			model.setKarmaFree(model.getKarmaFree()+data.getCost());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#decrease(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean decrease(QualityValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getPoints()==0)
			model.removeQuality(ref);
		
		// Change karma
		Quality data = ref.getModifyable();
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()+data.getCost());
		else
			model.setKarmaFree(model.getKarmaFree()-data.getCost());
		
		// Inform listeners
		if (ref.getPoints()==0)
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_REMOVED, ref));
		else
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_CHANGED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTERCONCEPT_ADDED:
			logger.debug("RCV "+event);
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getQuality()!=null) {
					recQualities.add(rec.getQuality());
					logger.debug("  Quality "+rec.getQuality().getId()+" is recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, getAvailableQualities()));
			break;
		case CHARACTERCONCEPT_REMOVED:
			logger.debug("RCV "+event);
			logger.warn("TODO: ensure that when multiple recommendations are made for same skill, only one is removed");
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getQuality()!=null) {
					recQualities.remove(rec.getQuality());
					logger.debug("  Quality "+rec.getQuality().getId()+" is recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, getAvailableQualities()));
			break;
		default:
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#deselect(org.prelle.shadowrun5.QualityValue)
	 */
	@Override
	public boolean deselect(QualityValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;

		// Change model
		ref.setPoints(0);
		model.removeQuality(ref);
		
		// Change karma
		Quality data = ref.getModifyable();
		if (data.getType()==QualityType.POSITIVE)
			model.setKarmaFree(model.getKarmaFree()+data.getCost());
		else
			model.setKarmaFree(model.getKarmaFree()-data.getCost());

		// Apply modifications
		parent.undo(data.getModifications());
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.QUALITY_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_QUALITIES, model.getKarmaFree(), getPointsInNegativeQualities()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new Integer[] {model.getKarmaFree(), model.getKarmaInvested()}));
		updateAvailable();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#isRecommended(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public boolean isRecommended(Quality val) {
		return recQualities.contains(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#setIgnoreKarma(boolean)
	 */
	@Override
	public void setIgnoreKarma(boolean state) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.QualityController#getChoicesFor(org.prelle.shadowrun5.Quality)
	 */
	@Override
	public List<Object> getChoicesFor(Quality val) {
		List<Object> ret = new ArrayList<>();
		if (val.needsChoice()) {
			switch (val.getSelect()) {
			case SKILLGROUP:
				for (SkillGroup grp : ShadowrunCore.getSkillgroups()) {
//					if (!grp.getId().equals("KNOWLEDGE"))
						ret.add(grp);
				}
				break;
			default:
				logger.warn("TODO: don't know how to select "+val.getSelect());
			}
		}
		return ret;
	}

	@Override
	public void apply(QualityModification mod) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void undo(QualityModification mod) {
		// TODO Auto-generated method stub
		
	}

}
