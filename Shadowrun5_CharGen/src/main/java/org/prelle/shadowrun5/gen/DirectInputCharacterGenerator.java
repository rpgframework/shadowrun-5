/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;

/**
 * @author prelle
 *
 */
public class DirectInputCharacterGenerator extends CommonSR5CharacterGenerator {
	
	private final static Logger logger = LogManager.getLogger("shadowrun");

	//-------------------------------------------------------------------
	public DirectInputCharacterGenerator() {
		super();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "chargen.input.page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "chargen.input.desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return "No i18n resources for "+this.getClass();
		return i18n.getString("chargen.input.title");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		// TODO Auto-generated method stub
		return new WizardPageType[]{
				WizardPageType.METATYPE,
				WizardPageType.ATTRIBUTES,
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		// TODO Auto-generated method stub
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#start(org.prelle.shadowrun5.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) { 
		this.model = model;
		meta = new AllowingMetatypeGenerator(model);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		super.stop();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return "input";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		// TODO Auto-generated method stub
		return new ArrayList<String>();
	}

}
