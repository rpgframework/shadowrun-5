/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.chargen.FreePointsModification;
import org.prelle.shadowrun5.chargen.FreePointsModification.Type;
import org.prelle.shadowrun5.common.CommonEquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class EquipmentGenerator extends CommonEquipmentController implements GenerationEventListener, CharacterProcessor {

	/**
	 * Nuyen granted by priority table
	 */
	private int baseNuyen;
	/**
	 * Karma points converted to Nuyen
	 */
	private int usedKarma;

	private int calcInvested;
	private int calcFree;

	//--------------------------------------------------------------------
	public EquipmentGenerator(CommonSR5CharacterGenerator parent) {
		super(parent.getCharacter(), CharGenMode.CREATING);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	public boolean canIncreaseBoughtNuyen() {
		return model.getKarmaFree()>0 && usedKarma<20;
	}

	//--------------------------------------------------------------------
	public boolean canDecreaseBoughtNuyen() {
		return usedKarma>0;
	}

	//--------------------------------------------------------------------
	public void increaseBoughtNuyen() {
		logger.debug("Increase");
		if (!canIncreaseBoughtNuyen())
			return;
		
		logger.info("Generate 2000 nuyen for 1 karma");
		usedKarma++;
		model.setKarmaFree(model.getKarmaFree()-1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		updateNuyen();
	}

	//--------------------------------------------------------------------
	public void decreaseBoughtNuyen() {
		if (!canDecreaseBoughtNuyen())
			return;
		
		logger.info("Return 2000 nuyen for 1 karma");
		usedKarma--;
		model.setKarmaFree(model.getKarmaFree()+1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		updateNuyen();
	}

	//--------------------------------------------------------------------
	public void updateNuyen() {
		calcFree = baseNuyen;
		calcFree+= usedKarma*2000;

		// Calculate worth of all gear
		calcInvested = ShadowrunTools.calculateInvestedNuyen(model);

		logger.debug(calcInvested+" of "+calcFree+"\u00A5 invested");
		calcFree -= calcInvested;
		model.setNuyen(calcFree);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, calcFree, calcInvested));
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			logger.debug("RCV "+event);
			PriorityType type = (PriorityType)event.getKey();
			Priority prio = (Priority)event.getValue();
			if (type==PriorityType.RESOURCES) {
				logger.debug("  recalculate available resources");
				switch (prio) {
				case A: baseNuyen=450000; break;
				case B: baseNuyen=275000; break;
				case C: baseNuyen=140000; break;
				case D: baseNuyen= 50000; break;
				case E: baseNuyen=  6000; break;
				}
				updateNuyen();
			}
			break;
		case EQUIPMENT_ADDED:
		case EQUIPMENT_CHANGED:
		case EQUIPMENT_REMOVED:
			logger.debug("RCV "+event);
			updateNuyen();
			break;
		case NUYEN_RECALC_NECESSARY:
			logger.debug("RCV "+event);
			updateNuyen();
			break;
		default:
		}

	}

	//-------------------------------------------------------------------
	public int getBoughtNuyen() {
		return usedKarma;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#deselect(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		boolean removed = super.deselect(ref);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, ref));
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#sell(org.prelle.shadowrun5.items.CarriedItem, float)
	 */
	@Override
	public void sell(CarriedItem ref, float factor) {
		super.sell(ref, factor);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, ref));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#increase(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		if (!super.increase(data))
			return false;
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#decrease(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		if (!super.decrease(data))
			return false;
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			
			// Inject free connections
			unprocessed.add(new FreePointsModification(Type.CONNECTIONS, model.getAttribute(Attribute.CHARISMA).getModifiedValue()*3));
			logger.debug("Grant "+(model.getAttribute(Attribute.CHARISMA).getModifiedValue()*3)+" free points for connections");
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
