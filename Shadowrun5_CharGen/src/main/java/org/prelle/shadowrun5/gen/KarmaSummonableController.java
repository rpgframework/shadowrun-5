/**
 *
 */
package org.prelle.shadowrun5.gen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.SummonableValue;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.levelling.NoCostSummonableController;

/**
 * @author Stefan
 *
 */
public class KarmaSummonableController extends NoCostSummonableController implements SummonableController, GenerationEventListener {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	//--------------------------------------------------------------------
	/**
	 */
	public KarmaSummonableController(ShadowrunCharacter model) {
		super(model);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeSelected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeSelected(Summonable value) {
		return model.getKarmaFree()>=1;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeDeselected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeDeselected(SummonableValue value) {
		return model.getSummonables().contains(value);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#select(org.prelle.shadowrun5.Summonable, int, int)
	 */
	@Override
	public SummonableValue select(Summonable value, int services) {
		SummonableValue val = super.select(value, services);

		if (val!=null) {
			// Pay
			model.setKarmaFree( model.getKarmaFree() - services );
			logger.debug("Pay "+services+" karma");

			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(),model.getKarmaInvested()}));
		}

		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#delect(org.prelle.shadowrun5.SummonableValue)
	 */
	@Override
	public void deselect(SummonableValue value) {
		if (!canBeDeselected(value))
			return;

		super.deselect(value);

		model.setKarmaFree( model.getKarmaFree() + value.getRating());
		logger.debug("Grant "+value.getRating()+" karma");
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLE_REMOVED, value));

	}

}
