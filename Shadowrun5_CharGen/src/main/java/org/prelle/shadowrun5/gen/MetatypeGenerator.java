/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MetatypeController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author Stefan
 *
 */
public class MetatypeGenerator implements MetatypeController, GenerationEventListener {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");
	
	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;
	private List<MetaTypeOption> availableOptions;
	private List<MetaType> availableTypes;
	
	private PriorityTableEntry tableEntry;
	
	private MetaTypeOption current;
	
	//--------------------------------------------------------------------
	public MetatypeGenerator(CommonSR5CharacterGenerator parent, ShadowrunCharacter model) {
		this.parent= parent;
		this.model = model;
		availableOptions  = new ArrayList<MetaTypeOption>();
		availableTypes = new ArrayList<MetaType>();
	}

	//--------------------------------------------------------------------
	void updateAvailable() {
		logger.debug("START updateAvailable");
		availableOptions.clear();
		availableTypes.clear();
		logger.debug("  updateAvailable with "+tableEntry);
		if (tableEntry==null)
			return;
		for (PriorityOption tmp : tableEntry) {
			if (tmp instanceof MetaTypeOption) {
				availableOptions.add( ((MetaTypeOption)tmp) );
				availableTypes.add( ((MetaTypeOption)tmp).getType());
			}
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.METATYPES_AVAILABLE_CHANGED, availableOptions, availableTypes));
		logger.debug("STOP  updateAvailable");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.PRIORITY_CHANGED) {
			logger.debug("RCV "+event);
			PriorityType type = (PriorityType)event.getKey();
			Priority prio = (Priority)event.getValue();
			if (type==PriorityType.METATYPE) {
				logger.debug("  recalculate available metatypes("+type+", "+prio+")");
				tableEntry = ShadowrunCore.getPriorityTableEntry(type, prio);
				logger.debug("  tableEntry = "+tableEntry);
				updateAvailable();
			}
		}
		
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getAvailable()
	 */
	@Override
	public List<MetaTypeOption> getAvailable() {
		return availableOptions;
	}

	//--------------------------------------------------------------------
	private MetaTypeOption getOptionByMetaType(MetaType type) {
		for (MetaTypeOption tmp : availableOptions)
			if (tmp.getType()==type)
				return tmp;
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#canBeSelected(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public boolean canBeSelected(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option==null)
			return false;
		
		logger.debug("canBeSelected("+type+") = "+model.getKarmaFree()+" >= "+option.getAdditionalKarmaKost());
		return model.getKarmaFree()>=option.getAdditionalKarmaKost();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getSpecialAttributes(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public int getSpecialAttributes(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option!=null)
			return option.getSpecialAttributePoints();
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#getKarmaCost(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public int getKarmaCost(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option!=null)
			return option.getAdditionalKarmaKost();
		return 0;
	}

	//--------------------------------------------------------------------
	private void undoCurrent()  {
		if (current==null)
			return;
		
		// Reimburse invested karma
		if (current.getAdditionalKarmaKost()!=0) {
			model.setKarmaFree(model.getKarmaFree() + current.getAdditionalKarmaKost());
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));			
		}
		
		model.setMetatype(null);
		parent.undo(current.getType().getModifications());
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MetatypeController#select(org.prelle.shadowrun5.MetaType)
	 */
	@Override
	public void select(MetaType value) {
		logger.info("Select "+value);
		if (!canBeSelected(value)) {
			logger.warn("Cannot select "+value);
			return;
		}
		
		/*
		 * Undo previous selection
		 */
		undoCurrent();
		
		/*
		 * Apply
		 */
		MetaTypeOption option = getOptionByMetaType(value);
		model.setMetatype(value);
		
		/*
		 * Inform listener
		 */
		logger.info("-----additional karma kost = "+option.getAdditionalKarmaKost());
		if (option.getAdditionalKarmaKost()!=0) {
			model.setKarmaFree(model.getKarmaFree() - option.getAdditionalKarmaKost());
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));			
		}
		
		parent.apply(value.getName(), value.getModifications());
		current = option;
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.METATYPE_CHANGED, value, option.getSpecialAttributePoints()));			
		
	}

}
