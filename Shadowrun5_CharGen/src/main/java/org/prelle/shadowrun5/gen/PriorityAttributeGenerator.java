/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.common.DerivedAttributeCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AttributeModification;

/**
 * @author prelle
 *
 */
public class PriorityAttributeGenerator implements AttributeController, GenerationEventListener {

	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;

	private int pointsAllowed;
	private int pointsSpecial;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityAttributeGenerator(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		DerivedAttributeCalculator calc = new DerivedAttributeCalculator();
		calc.setData(model);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun.charctrl.Controller#getParentController()
//	 */
//	@Override
	public CharacterController getParentController() {
		return parent;
	}

	//-------------------------------------------------------------------
	private int getPointsInvested() {
		int invest = 0;
		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue aVal = model.getAttribute(key);
			invest += aVal.getBought();
		}
		return invest;
	}

	//-------------------------------------------------------------------
	private int getPointsInvestedSpecial() {
		int invest = 0;
		for (Attribute key : Attribute.specialAttributes()) {
			AttributeValue aVal = model.getAttribute(key);
			invest += aVal.getBought();
		}
		return invest;
	}

	//-------------------------------------------------------------------
	private void checkLimits() {
		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue aVal = model.getAttribute(key);
			boolean changed = false;
			int diff = aVal.getMaximum() - aVal.getModifiedValue();
			if (diff<0) {
				logger.debug("  "+key+" "+aVal.getModifiedValue()+" is higher than allowed maximum of "+aVal.getMaximum()+" - decrease it");
				aVal.setPoints(aVal.getPoints() + diff);
				changed = true;
			}

			if (changed) {
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, aVal));
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, null, getPointsLeft()));
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return pointsAllowed - getPointsInvested();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getPointsLeftSpecial()
	 */
	@Override
	public int getPointsLeftSpecial() {
		return pointsSpecial - getPointsInvestedSpecial();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getMinimalValue(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public int getMinimalValue(Attribute key) {
		return model.getAttribute(key).getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#mightReach(org.prelle.shadowrun5.Attribute, int)
	 */
	@Override
	public ValueState getValueState(Attribute key, int value) {
		if (value<1) return ValueState.BELOW_MINIMUM;
		AttributeValue val = model.getAttribute(key);
		if (value<=val.getModifier()) return ValueState.WITHIN_MODIFIER;

		if (value>val.getMaximum()) return ValueState.ABOVE_MAX;

		if (value<=val.getModifiedValue()) return ValueState.SELECTED;

		// Not selected yet.
		if (value==val.getMaximum()) {
			// If another attribute is already maxed, this attribute may not
			List<Attribute> maxed = getMaximizedAttributes();
			if (!maxed.isEmpty())
				return ValueState.UNATAINABLE_MAX;
		}
		return ValueState.UNSELECTED;
	}

	//-------------------------------------------------------------------
	public int getMaximumValue(Attribute key) {
		return model.getAttribute(key).getMaximum();
	}

	//-------------------------------------------------------------------
	private List<Attribute> getMaximizedAttributes() {
		List<Attribute> maxed = new ArrayList<Attribute>();
		for (Attribute key : Attribute.primaryValues()) {
			if (model.getAttribute(key).getModifiedValue() == model.getAttribute(key).getMaximum())
				maxed.add(key);
		}
		return maxed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeDecreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute key) {
		AttributeValue val = model.getAttribute(key);
		return val.getModifiedValue()>getMinimalValue(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		if (key.isPrimary()) {
			if (getPointsLeft()<1)
				return false;
		} else {
			if (getPointsLeftSpecial()<1)
				return false;
		}

		Collection<Attribute> alreadyMaxed = getMaximizedAttributes();

		AttributeValue val = model.getAttribute(key);
		// Only allow to max an attribute, if there isn't one already
		if ((val.getModifiedValue()+1)==getMaximumValue(key) && key.isPrimary()) {
			logger.debug("Increasing "+key+" would reach maximum of "+getMaximumValue(key)+".  Is already one maxed = "+alreadyMaxed);
			return alreadyMaxed.isEmpty();
		}
		return val.getModifiedValue()<getMaximumValue(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getIncreaseCost(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#increase(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean increase(Attribute key) {
		if (!canBeIncreased(key))
			return false;
		AttributeValue val = model.getAttribute(key);
		val.setPoints(val.getPoints()+1);
		logger.info("Increase "+key+" to "+val.getPoints());

		ShadowrunTools.calculateDerived(model);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, null, getPointsLeft()));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#decrease(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean decrease(Attribute key) {
		if (!canBeDecreased(key))
			return false;
		logger.info("Decrease "+key);
		AttributeValue val = model.getAttribute(key);
		val.setPoints(val.getPoints()-1);

		ShadowrunTools.calculateDerived(model);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, null, getPointsLeft()));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#apply(org.prelle.shadowrun5.modifications.AttributeModification)
	 */
	@Override
	public void apply(AttributeModification mod) {
		logger.debug("apply "+mod);
		if (mod.getAttribute()==null)
			throw new NullPointerException("Attribute in modification is NULL");
		model.getAttribute(mod.getAttribute()).addModification(mod);
		checkLimits();
		ShadowrunTools.calculateDerived(model);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.ATTRIBUTE_CHANGED,
				mod.getAttribute(),
				model.getAttribute(mod.getAttribute())));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#undo(org.prelle.shadowrun5.modifications.AttributeModification)
	 */
	@Override
	public void undo(AttributeModification mod) {
		model.getAttribute(mod.getAttribute()).removeModification(mod);
		checkLimits();
		ShadowrunTools.calculateDerived(model);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.ATTRIBUTE_CHANGED,
				mod.getAttribute(),
				model.getAttribute(mod.getAttribute())));
	}

	//-------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			PriorityType prioType = (PriorityType)event.getKey();
			Priority priority = (Priority)event.getValue();
			if (prioType!=PriorityType.ATTRIBUTE)
				return;
			switch (priority) {
			case A: pointsAllowed = 24; break;
			case B: pointsAllowed = 20; break;
			case C: pointsAllowed = 16; break;
			case D: pointsAllowed = 14; break;
			case E: pointsAllowed = 12; break;
			}
			logger.debug("Priority "+priority+" selected. Allowed are "+pointsAllowed+" points to distribute");
			break;
		case METATYPE_CHANGED:
			logger.debug("RCV "+event);
			pointsSpecial = (int)event.getValue();
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, getPointsLeftSpecial(), getPointsLeft()));
			break;
		case MAGICORRESONANCE_CHANGED:
			logger.debug("RCV "+event);
			MagicOrResonanceOption mor = (MagicOrResonanceOption)event.getKey();
			MagicOrResonanceType morType = mor.getType();
			if (!morType.usesMagic() && model.getAttribute(Attribute.MAGIC).getPoints()>0) {
				model.getAttribute(Attribute.MAGIC).setPoints(0);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, Attribute.MAGIC,	model.getAttribute(Attribute.MAGIC)));
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, getPointsLeftSpecial(), getPointsLeft()));
			}
			if (!morType.usesResonance() && model.getAttribute(Attribute.RESONANCE).getPoints()>0) {
				model.getAttribute(Attribute.RESONANCE).setPoints(0);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, Attribute.RESONANCE,	model.getAttribute(Attribute.RESONANCE)));
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, getPointsLeftSpecial(), getPointsLeft()));
			}
			break;
		default:
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getPointsLeft()>0) {
			ret.add(String.format(RES.getString("attribgen.todo.normal"), getPointsLeft()));
		}
		if (getPointsLeftSpecial()>0) {
			boolean couldBeIncreased = false;
			if (canBeDecreased(Attribute.EDGE)) couldBeIncreased = true;
			if (canBeDecreased(Attribute.MAGIC) && model.getMagicOrResonanceType().usesMagic()) couldBeIncreased = true;
			if (canBeDecreased(Attribute.RESONANCE) && model.getMagicOrResonanceType().usesResonance()) couldBeIncreased = true;
			if (couldBeIncreased)
				ret.add(String.format(RES.getString("attribgen.todo.special"), getPointsLeftSpecial()));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		return canBeIncreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		return canBeDecreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean increase(AttributeValue value) {
		return increase(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean decrease(AttributeValue value) {
		return decrease(value.getModifyable());
	}

}
