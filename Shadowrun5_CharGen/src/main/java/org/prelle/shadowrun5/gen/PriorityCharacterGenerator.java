/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.RewardImpl;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.common.CommonLifestyleController;
import org.prelle.shadowrun5.common.CommonSINController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.levelling.KarmaAttributeController;
import org.prelle.shadowrun5.levelling.KarmaComplexFormController;
import org.prelle.shadowrun5.levelling.KarmaRitualController;
import org.prelle.shadowrun5.levelling.KarmaSkillController;
import org.prelle.shadowrun5.levelling.KarmaSpellController;
import org.prelle.shadowrun5.modifications.AddNuyenModification;

import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityCharacterGenerator extends CommonSR5CharacterGenerator {
	
	private enum Mode {
		FINISHED,
		CREATION,
		TUNING
	}

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	private Map<Priority,PriorityType> priorities;
	private List<Modification> undoList;
	private Mode mode;

	//-------------------------------------------------------------------
	public PriorityCharacterGenerator() {
		super();

		priorities = new HashMap<Priority, PriorityType>();
		mode = Mode.FINISHED;
//		priorities = new PriorityType[]{PriorityType.METATYPE, PriorityType.ATTRIBUTE, PriorityType.MAGIC, PriorityType.SKILLS, PriorityType.RESOURCES};
	}

	//-------------------------------------------------------------------
	public Priority getPriority(PriorityType option) {
		for (Priority prio : Priority.values()) {
			if (priorities.get(prio)==option)
				return prio;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		Priority oldPriority = getPriority(option);
		PriorityType oldOption = priorities.get(prio);
		
		if (oldOption==option && oldPriority==prio)
			return;

		logger.info("Select "+option+" with prio "+prio+"  - and set "+oldOption+" to prio "+oldPriority);

		priorities.put(prio, option);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, option, prio));

		if (oldPriority!=null) {
			if (oldOption==null) {
				logger.error("There was nothing set with prio "+prio+" before");
			} else {
				priorities.put(oldPriority, oldOption);

				GenerationEventDispatcher.fireEvent(new GenerationEvent(
						GenerationEventType.PRIORITY_CHANGED,
						oldOption,
						oldPriority));
			}
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "chargen.priority.page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "chargen.priority.desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return "No i18n resources for "+this.getClass();
		return i18n.getString("chargen.priority.title");
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
				WizardPageType.PRIORITIES,
				WizardPageType.METATYPE,
				WizardPageType.MAGIC_OR_RESONANCE,
				WizardPageType.TRADITION,
				WizardPageType.QUALITIES,
				WizardPageType.ATTRIBUTES,
				WizardPageType.SKILLS,
				WizardPageType.SPELLS,
				WizardPageType.ALCHEMY,
				WizardPageType.RITUALS,
				WizardPageType.POWERS,
				WizardPageType.COMPLEX_FORMS,
//				WizardPageType.BODYTECH,
//				WizardPageType.GEAR,
//				WizardPageType.VEHICLES,
				WizardPageType.NAME,
		};
	}

	//-------------------------------------------------------------------
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (mode==Mode.TUNING && model.getKarmaFree()>7) {
			ret.add(RES.getString("priogen.todo.maxkarma"));
		}
		if (model==null)
			return ret;
		
		if (model.getMetatype()==null) {
			ret.add(RES.getString("priogen.todo.metatype"));
		}
		ret.addAll(attrib.getToDos());
		ret.addAll(magOrRes.getToDos());
		ret.addAll(skill.getToDos());
		if (model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesSpells()) {
				ret.addAll(spell.getToDos());
			}
			if (model.getMagicOrResonanceType().usesPowers()) {
				ret.addAll(power.getToDos());
			}
			if (model.getMagicOrResonanceType().usesResonance()) {
				ret.addAll(cforms.getToDos());
			}
		}

		if (model.getName()==null) {
			ret.add(RES.getString("priogen.todo.name"));
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		List<String> todos = getToDos();
		logger.debug("ToDos: "+todos);
		if (todos.isEmpty())
			return true;
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#start(org.prelle.shadowrun5.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		// Stop previous
		stop();

		mode = Mode.CREATION;
		this.model = model;
		model.setKarmaFree(25);
		logger.info("----------------Start generator-----------------------");

		magOrRes = new PriorityMagicOrResonanceGenerator(this);
		meta     = new MetatypeGenerator(this, model);
		attrib   = new PriorityAttributeGenerator(this);
		skill    = new PrioritySkillGenerator(this);
		spell    = new PrioritySpellGenerator(this);
		alchemy  = spell;
		ritual   = (PrioritySpellGenerator)spell;
		quality  = new PriorityQualityGenerator(getCharacter());
		power    = new AdeptPowerGeneratorAndLeveller(this, CharGenMode.CREATING, new ArrayList<>());
		cforms   = new PriorityComplexFormGenerator(this);
		equip    = new EquipmentGenerator(this);
		connections = new PriorityConnectionGenerator_DeleteMe(this);
		sins     = new CommonSINController(model);
		lifestyles  = new CommonLifestyleController(model, equip);
		summonables = new KarmaSummonableController(model);

		GenerationEventDispatcher.addListener((PriorityMagicOrResonanceGenerator)magOrRes);
		GenerationEventDispatcher.addListener(meta);
		GenerationEventDispatcher.addListener((PriorityAttributeGenerator)attrib);
		GenerationEventDispatcher.addListener((PrioritySkillGenerator)skill);
		GenerationEventDispatcher.addListener((PrioritySpellGenerator)spell);
		GenerationEventDispatcher.addListener((GenerationEventListener)quality);
		GenerationEventDispatcher.addListener((GenerationEventListener)power);
		GenerationEventDispatcher.addListener((PriorityComplexFormGenerator)cforms);
		GenerationEventDispatcher.addListener((GenerationEventListener)equip);
		GenerationEventDispatcher.addListener((PriorityConnectionGenerator_DeleteMe)connections);
		GenerationEventDispatcher.addListener((CommonSINController)sins);
//		GenerationEventDispatcher.addListener((CommonLifestyleController)lifestyles);
		GenerationEventDispatcher.addListener((KarmaSummonableController)summonables);
		GenerationEventDispatcher.addListener(this);

		setPriority(PriorityType.METATYPE , Priority.A);
		setPriority(PriorityType.ATTRIBUTE, Priority.B);
		setPriority(PriorityType.MAGIC    , Priority.C);
		setPriority(PriorityType.SKILLS   , Priority.D);
		setPriority(PriorityType.RESOURCES, Priority.E);

		((MetatypeGenerator)meta).updateAvailable();
		logger.debug("start done");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#startTuningMode()
	 */
	@Override
	public void startTuningMode() {
		logger.info("------Change to tuning mode----------------");
		mode = Mode.TUNING;
		undoList  = new ArrayList<Modification>();
		GenerationEventDispatcher.removeListener((PriorityMagicOrResonanceGenerator)magOrRes);
		GenerationEventDispatcher.removeListener(meta);
		GenerationEventDispatcher.removeListener((PriorityAttributeGenerator)attrib);
		GenerationEventDispatcher.removeListener((PrioritySkillGenerator)skill);
		GenerationEventDispatcher.removeListener((PrioritySpellGenerator)spell);
//		GenerationEventDispatcher.removeListener((QualityGenerator)quality);
		GenerationEventDispatcher.removeListener((GenerationEventListener)power);
		GenerationEventDispatcher.removeListener((PriorityComplexFormGenerator)cforms);

		attrib = new KarmaAttributeController(model, undoList);
		skill  = new KarmaSkillController(this, CharGenMode.CREATING, undoList);
		spell  = new KarmaSpellController(model, CharGenMode.CREATING, undoList, false);
		alchemy= new KarmaSpellController(model, CharGenMode.CREATING, undoList, true);
		ritual = new KarmaRitualController(model, CharGenMode.CREATING, undoList);
		cforms = new KarmaComplexFormController(model, CharGenMode.CREATING, undoList);
//		logger.warn("TODO: switch other controller");

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONTROLLER_CHANGED, this));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		logger.debug("Remove generation listeners");

//		GenerationEventDispatcher.removeListener((KarmaAttributeController)attrib);
//		GenerationEventDispatcher.removeListener((KarmaSkillGenerator)skill);
//		GenerationEventDispatcher.removeListener((KarmaSpellGenerator)spell);
//		GenerationEventDispatcher.removeListener((QualityGenerator)quality);
//		GenerationEventDispatcher.removeListener((GenerationEventListener)power);
//		GenerationEventDispatcher.removeListener((PriorityComplexFormGenerator)cforms);
//		GenerationEventDispatcher.removeListener((GenerationEventListener)equip);
//		GenerationEventDispatcher.removeListener((PriorityConnectionGenerator)connections);
//		GenerationEventDispatcher.removeListener((CommonSINController)sins);

		GenerationEventDispatcher.removeListener((GenerationEventListener)equip);
		GenerationEventDispatcher.removeListener((KarmaSummonableController)summonables);
		GenerationEventDispatcher.removeListener(this);

		if (model==null)
			return;

		/*
		 * Fix attributes
		 */
		logger.debug("Sum up attribute values before finalizing");
		for (Attribute key : Attribute.values()) {
			if (key.isPrimary() || key.isSpecial()) {
				AttributeValue aVal = model.getAttribute(key);
				int modified = aVal.getModifiedValue();
				aVal.clearModifications();
				aVal.setPoints(modified);
				aVal.setStart(modified);
				logger.debug("..."+aVal);
			}
		}

		/*
		 * Fix skills
		 */
		logger.debug("Sum up skill values before finalizing");
		for (SkillValue val : model.getSkillValues(false)) {
			int modified = val.getModifiedValue();
			val.clearModifications();
			val.setPoints(modified);
		}
		for (SkillGroupValue val : model.getSkillGroupValues()) {
			int modified = val.getModifiedValue();
			val.clearModifications();
			val.setPoints(modified);
		}

		// Start-Nuyen
		// Take highest lifestyle into account
		int nuyen = Math.min(5000, model.getNuyen());
		int dice = 0;
		int factor = 0;
		for (LifestyleValue ref : model.getLifestyle()) {
			if (ref.getLifestyle().getDice()>dice) {
				dice = ref.getLifestyle().getDice();
				factor = ref.getLifestyle().getFactor();
			}
		}
		int diceValues = 0;
		Random random = new Random();
		for (int i=0; i<dice; i++) {
			diceValues += (random.nextInt(6)+1);
		}
		logger.debug("  new nuyen = "+nuyen+" + "+diceValues+" *"+factor);
		nuyen += diceValues*factor;
		model.setNuyen(nuyen);

		model.setKarmaInvested(0);

		Reward creationReward = new RewardImpl(model.getKarmaFree(), RES.getString("reward.creation"));
		creationReward.addModification(new AddNuyenModification(nuyen));
		model.addReward(creationReward);
		
		mode = Mode.FINISHED;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return "priority";
	}

}
