/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PriorityComplexFormGenerator implements ComplexFormController, GenerationEventListener {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;

	private List<ComplexForm> recommended;

	private List<ComplexForm> availableComplexForms;

	private int points;

	//-------------------------------------------------------------------
	public PriorityComplexFormGenerator(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		availableComplexForms   = new ArrayList<ComplexForm>();
		recommended = new ArrayList<ComplexForm>();
		updateAvailableComplexForms();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getParentController()
	 */
//	@Override
//	public CharacterController getParentController() {
//		return parent;
//	}

	//--------------------------------------------------------------------
	private void updateAvailableComplexForms() {
		logger.debug("START updateAvailableComplexForms");
		availableComplexForms.clear();
		availableComplexForms.addAll(ShadowrunCore.getComplexForms());
		// Remove those the character already has
		for (ComplexFormValue val : model.getComplexForms()) {
			logger.trace("Not available anymore "+val);
			availableComplexForms.remove(val.getModifyable());
		}

		Collections.sort(availableComplexForms);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.COMPLEX_FORMS_AVAILABLE_CHANGED, availableComplexForms));
		logger.debug("STOP updateAvailableComplexForms");
	}

	//-------------------------------------------------------------------
	public void setData(int ptSkills) {
		points      = ptSkills;
	}

	//--------------------------------------------------------------------
	private int getPointsInComplexForms() {
		return model.getComplexForms().size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getComplexFormsLeft()!=0) {
			ret.add(String.format(RES.getString("cmplxformgen.todo.normal"), getComplexFormsLeft()));
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			PriorityType option = (PriorityType)event.getKey();
			Priority prio = (Priority)event.getValue();
			if (option!=PriorityType.MAGIC)
				return;
			logger.info("RCV "+event);
			switch (prio) {
			case A:
				points = 5;
				break;
			case B:
				points = 2;
				break;
			case C:
				points = 1;
				break;
			case D:
				points = 0;
				break;
			case E:
				points = 0;
				break;
			}
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_COMPLEX_FORMS, null, getComplexFormsLeft()));
			break;
		case CHARACTERCONCEPT_ADDED:
			logger.debug("RCV "+event);
//			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
//				if (rec.getSkill()!=null) {
//					recSkills.add(rec.getSkill());
//					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
//				}
//				if (rec.getSkillgroup()!=null) {
//					recSkillGroups.add(rec.getSkillgroup());
//					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is recommended");
//				}
//			}
//			GenerationEventDispatcher.fireEvent(
//					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		case CHARACTERCONCEPT_REMOVED:
			logger.debug("RCV "+event);
//			logger.warn("TODO: ensure that when multiple recommendations are made for same skill, only one is removed");
//			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
//				if (rec.getSkill()!=null) {
//					recSkills.remove(rec.getSkill());
//					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
//				}
//				if (rec.getSkillgroup()!=null) {
//					recSkillGroups.remove(rec.getSkillgroup());
//					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is not recommended");
//				}
//			}
//			GenerationEventDispatcher.fireEvent(
//					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getComplexFormsLeft()
	 */
	@Override
	public int getComplexFormsLeft() {
		return points - getPointsInComplexForms();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getAvailableComplexForms()
	 */
	@Override
	public List<ComplexForm> getAvailableComplexForms() {
		return availableComplexForms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeSelected(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public boolean canBeSelected(ComplexForm data) {
		if (data==null)
			throw new NullPointerException();

		// Is it already selected?
		for (ComplexFormValue selected : model.getComplexForms()) {
			if (selected.getModifyable()==data)
				return false;
		}

		// Are there points left
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeDeselected(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public boolean canBeDeselected(ComplexFormValue data) {
		return model.getComplexForms().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#select(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return null;
		}
		logger.debug("select "+data);

		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.COMPLEX_FORM_ADDED, ref));
		updateAvailableComplexForms();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#deselect(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public void deselect(ComplexFormValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return;
		}
		logger.debug("select "+data);

		model.removeComplexForm(data);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.COMPLEX_FORM_REMOVED, data));
		updateAvailableComplexForms();
	}

}
