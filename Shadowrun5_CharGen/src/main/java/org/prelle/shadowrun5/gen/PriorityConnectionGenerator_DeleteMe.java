/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PriorityConnectionGenerator_DeleteMe implements ConnectionsController, GenerationEventListener {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	protected ShadowrunCharacter model;

	private int connectionKarma;
	private int invest;
	
	
	//-------------------------------------------------------------------
	/**
	 */
	public PriorityConnectionGenerator_DeleteMe(CommonSR5CharacterGenerator parent) {
		model = parent.getCharacter();
		// Calculate
		refreshConnectionPoints();
	}
	
	//-------------------------------------------------------------------
	private void refreshConnectionPoints() {
		invest = 0;
		for (Connection conn : model.getConnections()) {
			invest += conn.getInfluence();
			invest += conn.getLoyalty();
		}
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_CONNECTIONS, getPointsLeft()));
	}
	
	//-------------------------------------------------------------------
	public int getHighPointsLeft() {
		return 0;
	}
	
	//-------------------------------------------------------------------
	public int getPointsLeft() {
		return connectionKarma - invest;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canCreateConnection()
	 */
	@Override
	public boolean canCreateConnection() {
		if (getPointsLeft()<2)
			return false;
				
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#createConnection()
	 */
	@Override
	public Connection createConnection() {
		if (!canCreateConnection()) {
			logger.warn("Trying to create new connection which is not allowed");
			return null;
		}
		
		Connection ref =  new Connection("?", "?", 1, 1);
		// Add connection
		model.addConnection(ref);		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_ADDED, ref));
		
		// Recalc
		refreshConnectionPoints();
		
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#removeConnection(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public void removeConnection(Connection con) {
		model.removeConnection(con);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));
		refreshConnectionPoints();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseInfluence(Connection con) {
		if (con.getInfluence()>=12)
			return false;
		
		if ( (con.getInfluence()+con.getLoyalty()>=7))
			return false;
		
		return invest<connectionKarma;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseInfluence(Connection con) {
		if (!canIncreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		refreshConnectionPoints();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseInfluence(Connection con) {
		return con.getInfluence()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseInfluence(Connection con) {
		if (!canDecreaseInfluence(con))
			return false;
		
		if (con.getInfluence()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setInfluence(con.getInfluence()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		refreshConnectionPoints();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseLoyalty(Connection con) {
		if (con.getLoyalty()>=6)
			return false;
		
		if ( (con.getLoyalty()+con.getLoyalty()>=7))
			return false;
		
		return invest<connectionKarma;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseLoyalty(Connection con) {
		if (!canIncreaseLoyalty(con))
			return false;
		
		logger.info("Increase loyalty of "+con);
		con.setLoyalty(con.getLoyalty()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		refreshConnectionPoints();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseLoyalty(Connection con) {
		return con.getLoyalty()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseLoyalty(Connection con) {
		if (!canDecreaseLoyalty(con))
			return false;
		
		if (con.getLoyalty()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setLoyalty(con.getLoyalty()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		refreshConnectionPoints();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case ATTRIBUTE_CHANGED:
			if ( ((Attribute)event.getKey())==Attribute.CHARISMA) {
				logger.debug("RCV "+event);
				connectionKarma = model.getAttribute(Attribute.CHARISMA).getModifiedValue()*3;
				refreshConnectionPoints();
			}
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (invest!=connectionKarma) {
			ret.add(String.format(RES.getString("priogen.todo.connection"), getPointsLeft()));
		}
		return ret;
	}

}
