/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.common.DerivedAttributeCalculator;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.ModificationChoice;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMagicOrResonanceGenerator implements MagicOrResonanceController, GenerationEventListener {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private CommonSR5CharacterGenerator parent;
	private ShadowrunCharacter model;

	private MagicOrResonanceOption current;
	private List<Modification> applyLater;

	private List<MagicOrResonanceOption> available;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityMagicOrResonanceGenerator(CommonSR5CharacterGenerator parent) {
		this.parent = parent;
		available = new ArrayList<>();
		model = parent.getCharacter();
		DerivedAttributeCalculator calc = new DerivedAttributeCalculator();
		calc.setData(model);
		applyLater = new ArrayList<>();
	}
	
//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun.charctrl.Controller#getParentController()
//	 */
//	@Override
//	public CharacterController getParentController() {
//		return parent;
//	}

	//-------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			PriorityType prioType = (PriorityType)event.getKey();
			Priority priority = (Priority)event.getValue();
			if (prioType!=PriorityType.MAGIC)
				return;
			logger.debug("RCV "+event);

			available.clear();
			PriorityTableEntry  entry = ShadowrunCore.getPriorityTableEntry(prioType, priority);
			for (PriorityOption option : entry) {
				if (option instanceof MagicOrResonanceOption)
					available.add((MagicOrResonanceOption)option);
			}
			// Inform about changed options
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MAGICORRESONANCE_AVAILABLE_CHANGED, available));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getAvailable()
	 */
	@Override
	public List<MagicOrResonanceOption> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#canBeSelected(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public boolean canBeSelected(MagicOrResonanceOption type) {
		return available.contains(type);
	}

	//-------------------------------------------------------------------
	private List<Modification> getApplyLater(MagicOrResonanceOption opt) {
		List<Modification> ret = new ArrayList<>();
		for (Modification mod : opt.getModifications()) {
			if (mod instanceof ModificationChoice)
				ret.add(mod);
			else if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getType()!=null && sMod.getSkill()==null)
					ret.add(mod);
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private List<Modification> getApplyImmediately(MagicOrResonanceOption opt) {
		List<Modification> ret = new ArrayList<>(opt.getModifications());
		ret.removeAll(getApplyLater(opt));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#select(org.prelle.shadowrun5.MagicOrResonanceOption)
	 */
	@Override
	public void select(MagicOrResonanceOption value) {
		if (value==current)
			return;

		// Undo old selection
		if (current!=null) {
			logger.error("------------UNDO "+current.getModifications());
			parent.undo(current.getModifications());
		}

		if (value==null)
			return;
		logger.info("Select "+value.getType());
		// Change
		model.setMagicOrResonanceType(value.getType());
		current = value;
		// Apply later in applyFromSelection()
		parent.apply(value.getType().getName(), getApplyImmediately(value));
		applyLater = getApplyLater(value);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MAGICORRESONANCE_CHANGED, value));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (model.getMagicOrResonanceType()==null) {
			ret.add(RES.getString("magicgen.todo"));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.MagicOrResonanceController#getToApplyFromSelection()
	 */
	@Override
	public List<Modification> getToApplyFromSelection() {
		logger.debug("------------APPLY LATE "+applyLater);
//		parent.apply(current.getType().getName(), applyLater);
		return applyLater;
	}

}
