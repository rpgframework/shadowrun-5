/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.Recommendation;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.common.CommonQualityController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PriorityQualityGenerator extends CommonQualityController implements GenerationEventListener {
	
	private List<Quality> recQualities;

	//-------------------------------------------------------------------
	public PriorityQualityGenerator(ShadowrunCharacter model) {
		super(model, CharGenMode.CREATING, new ArrayList<>());
		recQualities      = new ArrayList<Quality>();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTERCONCEPT_ADDED:
			logger.debug("RCV "+event);
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getQuality()!=null) {
					recQualities.add(rec.getQuality());
					logger.debug("  Quality "+rec.getQuality().getId()+" is recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, getAvailableQualities()));
			break;
		case CHARACTERCONCEPT_REMOVED:
			logger.debug("RCV "+event);
			logger.warn("TODO: ensure that when multiple recommendations are made for same skill, only one is removed");
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getQuality()!=null) {
					recQualities.remove(rec.getQuality());
					logger.debug("  Quality "+rec.getQuality().getId()+" is recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.QUALITY_AVAILABLE_CHANGED, getAvailableQualities()));
			break;
		default:
		}
	}

}
