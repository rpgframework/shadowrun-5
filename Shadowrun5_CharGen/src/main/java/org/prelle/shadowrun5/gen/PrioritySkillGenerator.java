/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.Recommendation;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.common.CommonSkillController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author Stefan
 *
 */
public class PrioritySkillGenerator extends CommonSkillController implements SkillController, GenerationEventListener {

	private final static ResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/shadowrun/chargen/i18n/chargen");

	private int pointsSkillGroups;
	private int pointsSkills;
	private int pointsLangAndKnow;

	//--------------------------------------------------------------------
	/**
	 */
	public PrioritySkillGenerator(CommonSR5CharacterGenerator parent) {
		super(parent);
		model = parent.getCharacter();
		avSkillGroups  = new ArrayList<SkillGroup>();
		avSkills       = new ArrayList<Skill>();
		recSkillGroups = new ArrayList<SkillGroup>();
		recSkills      = new ArrayList<Skill>();
		updateAvailable();
	}

	//-------------------------------------------------------------------
	public void setData(int ptGroups, int ptSkills) {
		pointsSkillGroups = ptGroups;
		pointsSkills      = ptSkills;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	private int getPointsInSkillGroups() {
		int sum = 0;
		for (SkillGroupValue sVal : model.getSkillGroupValues()) {
			sum += sVal.getPoints();
		}
		return sum;
	}

	//--------------------------------------------------------------------
	private int getPointsInSkills() {
		int sum = 0;
		// Calculate points invested in knowledge skills that exceed free points
		int know = getPointsInKnowledgeAndLanguage();
		if (know>pointsLangAndKnow)
			sum = know - pointsLangAndKnow;

		// Now add rest that is not knowledge
		for (SkillValue sVal : model.getSkillValues(false)) {
			if (sVal.getModifyable().getType()==SkillType.KNOWLEDGE)
				continue;
			if (sVal.getModifyable().getType()==SkillType.LANGUAGE)
				continue;
			sum += sVal.getPoints();
			sum += sVal.getSkillSpecializations().size();
		}
		return sum;
	}

	//--------------------------------------------------------------------
	public int getPointsInKnowledgeAndLanguage() {
		int sum = 0;
		for (SkillValue sVal : model.getSkillValues(false)) {
			Skill skill = sVal.getModifyable();
			if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
				if (skill.getId().startsWith("native_language"))
					continue;
				sum += sVal.getPoints();
				sum += sVal.getSkillSpecializations().size();
			}
		}
		return sum;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftInKnowledgeAndLanguage()
	 */
	@Override
	public int getPointsLeftInKnowledgeAndLanguage() {
		return Math.max(0, pointsLangAndKnow - getPointsInKnowledgeAndLanguage());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkillGroups()
	 */
	@Override
	public int getPointsLeftSkillGroups() {
		return pointsSkillGroups - getPointsInSkillGroups();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkills()
	 */
	@Override
	public int getPointsLeftSkills() {
		return pointsSkills - getPointsInSkills();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getPointsLeftSkills()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.normal"), getPointsLeftSkills()));
		}
		if (getPointsLeftSkillGroups()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.groups"), getPointsLeftSkillGroups()));
		}
		if (getPointsLeftInKnowledgeAndLanguage()!=0) {
			ret.add(String.format(RES.getString("skillgen.todo.knowledge"), getPointsLeftInKnowledgeAndLanguage()));
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public boolean canBeSelected(SkillGroup grp) {
		// Not already selected
		if (model.getSkillGroups().contains(grp))
			return false;
		// Points left
		return getPointsLeftSkillGroups()>0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public boolean canBeSelected(Skill skill) {
		// Not already selected
		if (model.getSkills(false).contains(skill)) {
			return false;
		}
		// Points left
		return getPointsLeftSkills()>0;
	}

	//--------------------------------------------------------------------
	public boolean canBeSelectedKnowledgeOrLanguage(Skill skill) {
		// Only check knowledge and language skills here
		if (skill.getType()!=SkillType.KNOWLEDGE && skill.getType()!=SkillType.LANGUAGE) {
			return false;
		}

		boolean isNative1 = skill.getId().equals("native_language");
		boolean isNative2 = skill.getId().equals("native_language2");

		// Native language may ony be selected once
		if (isNative1) {
			return !(model.getSkills(false).contains(skill));
		}
		// Native language 2 may ony be selected once AND if the needed quality exists
		if (isNative2) {
			logger.warn("CHECK FOR Quality Bilingual");
			return !(model.getSkills(false).contains(skill));
		}

		// Points left
		int invested = getPointsInKnowledgeAndLanguage();
		if (invested<pointsLangAndKnow)
			return true;

		return getPointsLeftSkills()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#selectKnowledgeOrLanguage(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName) {
		logger.warn("TODO: select "+skill.getId()+"/"+customName);

		if (!canBeSelectedKnowledgeOrLanguage(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		// Change model
		SkillValue sVal = new SkillValue(skill, 1);
		sVal.setName(customName);
		logger.info("Selected skill "+sVal);
		model.addSkill(sVal);

		// Fire event
		int free = Math.max(0, pointsLangAndKnow - getPointsInKnowledgeAndLanguage());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_KNOWLEDGE_SKILLS, null, free));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		updateAvailable();

		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDeselected(SkillGroupValue ref) {
		return model.getSkillGroupValues().contains(ref);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref) {
		return model.getSkillValues(false).contains(ref);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeIncreased(SkillValue ref) {
		// Maximum not reached yet
		if (ref.getModifiedValue()>=6)
			return false;

		// Enough points
		SkillType type = ref.getModifyable().getType();
		if (type!=SkillType.KNOWLEDGE && type!=SkillType.LANGUAGE) {
			if (getPointsLeftSkills()<=0) {
					return false;
			}
		} else {
			// Is language or knowledge
			logger.debug("LEFT: "+getPointsInKnowledgeAndLanguage()+" // "+getPointsLeftSkills());
			if (getPointsInKnowledgeAndLanguage()<=0 && getPointsLeftSkills()<=0)
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeIncreased(SkillGroupValue ref) {
		// Enough points
		if (getPointsLeftSkillGroups()<=0)
			return false;
		// Maximum not reached yet
		if (ref.getModifiedValue()>=6)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDecreased(SkillGroupValue ref) {
		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeDecreased(SkillValue ref) {
		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			PriorityType option = (PriorityType)event.getKey();
			Priority prio = (Priority)event.getValue();
			if (option!=PriorityType.SKILLS)
				return;
			logger.info("RCV "+event);
			switch (prio) {
			case A:
				pointsSkills = 46;
				pointsSkillGroups = 10;
				break;
			case B:
				pointsSkills = 36;
				pointsSkillGroups = 5;
				break;
			case C:
				pointsSkills = 28;
				pointsSkillGroups = 2;
				break;
			case D:
				pointsSkills = 22;
				pointsSkillGroups = 0;
				break;
			case E:
				pointsSkills = 18;
				pointsSkillGroups = 0;
				break;
			}
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLGROUPS, null, getPointsLeftSkillGroups()));
			updateAvailable();
			break;
		case CHARACTERCONCEPT_ADDED:
			logger.debug("RCV "+event);
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getSkill()!=null) {
					recSkills.add(rec.getSkill());
					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
				}
				if (rec.getSkillgroup()!=null) {
					recSkillGroups.add(rec.getSkillgroup());
					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		case CHARACTERCONCEPT_REMOVED:
			logger.debug("RCV "+event);
			logger.warn("TODO: ensure that when multiple recommendations are made for same skill, only one is removed");
			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
				if (rec.getSkill()!=null) {
					recSkills.remove(rec.getSkill());
					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
				}
				if (rec.getSkillgroup()!=null) {
					recSkillGroups.remove(rec.getSkillgroup());
					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is not recommended");
				}
			}
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		case ATTRIBUTE_CHANGED:
//			logger.debug("RCV "+event);
			// Calculate free points for knowledge and languages
			int calcFree = (
					model.getAttribute(Attribute.LOGIC).getModifiedValue()+
					model.getAttribute(Attribute.INTUITION).getModifiedValue()
					)*2;
			if (calcFree!=pointsLangAndKnow) {
				logger.info("Free points for knowledge and languages changed to "+calcFree);
				pointsLangAndKnow = calcFree;
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_KNOWLEDGE_SKILLS, null, pointsLangAndKnow));
				if (getPointsInKnowledgeAndLanguage()>pointsLangAndKnow) {
					logger.warn("Invested points in knowledge exceed free");
					updateAvailable();
					GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsInSkills()));
				}
			}
			break;
		default:
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec) {
		// Not already selected
		if (ref.hasSpecialization(spec))
			return false;
		// Ref belongs to character
		if (!model.getSkillValues(false).contains(ref))
			return false;
		// At generation only one specialization per skill
		if (ref.getSkillSpecializations().size()>0)
			return false;
		// Points left
		return getPointsLeftSkills()>0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec) {
		// Not already selected
		if (!ref.hasSpecialization(spec))
			return false;
		// Ref belongs to character
		if (!model.getSkillValues(false).contains(ref))
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unused")
	/**
	 * Do enough skillpoints exist to break group
	 * @see org.prelle.shadowrun.charctrl.SkillController#canBreakSkillGroup(org.prelle.shadowrun.SkillGroupValue)
	 */
	@Override
	public boolean canBreakSkillGroup(SkillGroupValue ref) {
		if (ref.getModifier()>0)
			return false;

		int pointsNecessary = 0;
		for (Skill skill : ShadowrunCore.getSkills(ref.getModifyable()))
			pointsNecessary += ref.getPoints();

		return getPointsLeftSkills()>=pointsNecessary;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canSpecializeIn(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canSpecializeIn(SkillValue val) {
		return !val.getModifyable().getSpecializations().isEmpty() && model.getKarmaFree()>0;
	}

}
