/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.Ritual;
import org.prelle.shadowrun5.RitualValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.Tradition;
import org.prelle.shadowrun5.Spell.Category;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class PrioritySpellGenerator implements SpellController, RitualController, GenerationEventListener {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private ShadowrunCharacter model;
	
//	private List<Spell> recommended;
	
	private List<Spell> availableSpells;
	private List<Spell> availableAlchemy;
	private List<Ritual> availableRituals;
	
	private int points;

	//-------------------------------------------------------------------
	public PrioritySpellGenerator(CommonSR5CharacterGenerator parent) {
		model = parent.getCharacter();
		availableSpells   = new ArrayList<Spell>();
		availableAlchemy  = new ArrayList<Spell>();
		availableRituals  = new ArrayList<Ritual>();
//		recommended = new ArrayList<Spell>();
		updateAvailableSpells();
		updateAvailableRituals();
	}

	//--------------------------------------------------------------------
	private void updateAvailableSpells() {
		logger.debug("START updateAvailableSpells");
		availableSpells.clear();
		availableSpells.addAll(ShadowrunCore.getSpells());
		availableAlchemy.clear();
		availableAlchemy.addAll(ShadowrunCore.getSpells());
		// Remove those the character already has
		for (SpellValue val : model.getSpells()) {
			logger.trace("Not available anymore "+val);
			if (val.isAlchemistic())
				availableAlchemy.remove(val.getModifyable());
			else
				availableSpells.remove(val.getModifyable());
		}
		
		Collections.sort(availableSpells);
		Collections.sort(availableAlchemy);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.SPELLS_AVAILABLE_CHANGED, availableSpells, availableAlchemy));
		logger.debug("STOP updateAvailableSpells");
	}

	//-------------------------------------------------------------------
	public void setData(int ptSkills) {
		points      = ptSkills;
	}

	//--------------------------------------------------------------------
	private int getPointsInSpells() {
		return model.getSpells().size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (getSpellsLeft()!=0 && model.getMagicOrResonanceType().usesSpells()) { 
			ret.add(String.format(RES.getString("spellgen.todo.normal"), getSpellsLeft()));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			PriorityType option = (PriorityType)event.getKey();
			Priority prio = (Priority)event.getValue();
			if (option!=PriorityType.MAGIC)
				return;
			logger.info("RCV "+event);
			switch (prio) {
			case A:
				points = 10;
				break;
			case B:
				points = 7;
				break;
			case C:
				points = 5;
				break;
			case D:
				points = 0;
				break;
			case E:
				points = 0;
				break;
			}
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SPELLS_RITUALS, null, getSpellsLeft()));
			break;
		case CHARACTERCONCEPT_ADDED:
			logger.debug("RCV "+event);
//			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
//				if (rec.getSkill()!=null) {
//					recSkills.add(rec.getSkill());
//					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
//				}
//				if (rec.getSkillgroup()!=null) {
//					recSkillGroups.add(rec.getSkillgroup());
//					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is recommended");
//				}
//			}
//			GenerationEventDispatcher.fireEvent(
//					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		case CHARACTERCONCEPT_REMOVED:
			logger.debug("RCV "+event);
//			logger.warn("TODO: ensure that when multiple recommendations are made for same skill, only one is removed");
//			for (Recommendation rec : ((CharacterConcept)event.getKey()).getRecommendations()) {
//				if (rec.getSkill()!=null) {
//					recSkills.remove(rec.getSkill());
//					logger.debug("  Skill "+rec.getSkill().getId()+" is recommended");
//				}
//				if (rec.getSkillgroup()!=null) {
//					recSkillGroups.remove(rec.getSkillgroup());
//					logger.debug("  Skillgroup "+rec.getSkillgroup().getId()+" is not recommended");
//				}
//			}
//			GenerationEventDispatcher.fireEvent(
//					new GenerationEvent(GenerationEventType.SKILLS_AVAILABLE_CHANGED, getAvailableSkills(), getAvailableSkillGroups()));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return points - getPointsInSpells() -getPointsInRituals();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return alchemistic?availableAlchemy:availableSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun5.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Category category, boolean alchemistic) {
		List<Spell> ret = new ArrayList<>();
		for (Spell tmp : alchemistic?availableAlchemy:availableSpells)
			if (tmp.getCategory()==category)
				ret.add(tmp);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeSelected(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell data, boolean alchemistic) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeDeselected(org.prelle.shadowrun5.SpellValue)
	 */
	@Override
	public boolean canBeDeselected(SpellValue data) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#select(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public SpellValue select(Spell data, boolean alchemistic) {
		if (!canBeSelected(data, alchemistic)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		SpellValue ref = new SpellValue(data);
		ref.setAlchemistic(alchemistic);
		model.addSpell(ref);
		logger.debug("send event "+data);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.POINTS_LEFT_SPELLS_RITUALS, null, getSpellsLeft()));
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null, model));
		
		updateAvailableSpells();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#deselect(org.prelle.shadowrun5.SpellValue)
	 */
	@Override
	public void deselect(SpellValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a spell that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeSpell(data);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.POINTS_LEFT_SPELLS_RITUALS, null, getRitualsLeft()));
		
		updateAvailableSpells();
	}

	//--------------------------------------------------------------------
	private int getPointsInRituals() {
		return model.getRituals().size();
	}

	//--------------------------------------------------------------------
	private void updateAvailableRituals() {
		logger.debug("START updateAvailableRituals");
		availableRituals.clear();
		availableRituals.addAll(ShadowrunCore.getRituals());
		// Remove those the character already has
		for (RitualValue val : model.getRituals()) {
			logger.trace("Not available anymore "+val);
			availableRituals.remove(val.getModifyable());
		}
		
		Collections.sort(availableRituals);
		
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.RITUALS_AVAILABLE_CHANGED, availableRituals));
		logger.debug("STOP updateAvailableRituals");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getRitualsLeft()
	 */
	@Override
	public int getRitualsLeft() {
		return getSpellsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getAvailableRituals()
	 */
	@Override
	public List<Ritual> getAvailableRituals() {
		return availableRituals;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeSelected(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public boolean canBeSelected(Ritual data) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#select(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public RitualValue select(Ritual data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a ritual that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		RitualValue ref = new RitualValue(data);
		model.addRitual(ref);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.POINTS_LEFT_SPELLS_RITUALS, null, getRitualsLeft()));
		
		updateAvailableRituals();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeDeselected(org.prelle.shadowrun5.RitualValue)
	 */
	@Override
	public boolean canBeDeselected(RitualValue data) {
		return model.hasRitual(data.getModifyable().getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#deselect(org.prelle.shadowrun5.RitualValue)
	 */
	@Override
	public void deselect(RitualValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a ritual that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeRitual(data);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.POINTS_LEFT_SPELLS_RITUALS, null, getRitualsLeft()));
		
		updateAvailableRituals();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun5.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.TRADITION_CHANGED, data));
	}

}
