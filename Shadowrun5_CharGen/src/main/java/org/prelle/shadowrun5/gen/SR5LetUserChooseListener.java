/**
 *
 */
package org.prelle.shadowrun5.gen;

import java.util.Collection;

import org.prelle.shadowrun5.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface SR5LetUserChooseListener {

	//-------------------------------------------------------------------
	public Collection<Modification> letUserChoose(String choiceReason, ModificationChoice choice);


}
