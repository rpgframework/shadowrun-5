/**
 * 
 */
package org.prelle.shadowrun5.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Lifestyle;
import org.prelle.shadowrun5.LifestyleOption;
import org.prelle.shadowrun5.LifestyleOptionValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.SingleLifestyleController;
import org.prelle.shadowrun5.common.ShadowrunCharGenConstants;

/**
 * @author prelle
 *
 */
public class SingleLifestyleGenerator implements SingleLifestyleController {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	protected ShadowrunCharacter model;

	private LifestyleValue data;
	
	//-------------------------------------------------------------------
	public SingleLifestyleGenerator(ShadowrunCharacter model) {
		if (model==null)
			throw new NullPointerException("Model is null");
		this.model = model;
		
		data = new LifestyleValue();
		data.setLifestyle(ShadowrunCore.getLifestyles().get(0));
	}
	
	//-------------------------------------------------------------------
	public SingleLifestyleGenerator(ShadowrunCharacter model, LifestyleValue life) {
		if (model==null)
			throw new NullPointerException("Model is null");
		this.model = model;
		
		data = life;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
		if (model.getLifestyle().isEmpty())
			ret.add(RES.getString("lifestylegen.todo.missing_primary_lifestyle"));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.LifestyleController#getAvailableLifestyles()
	 */
	@Override
	public List<Lifestyle> getAvailableLifestyles() {
		return ShadowrunCore.getLifestyles();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#canSelectLifestyle(org.prelle.shadowrun5.Lifestyle)
	 */
	@Override
	public boolean canSelectLifestyle(Lifestyle lifestyle) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#selectLifestyle(org.prelle.shadowrun5.Lifestyle)
	 */
	@Override
	public void selectLifestyle(Lifestyle lifestyle) {
		data.setLifestyle(lifestyle);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.LifestyleController#getAvailableOptions(org.prelle.shadowrun5.Lifestyle)
	 */
	@Override
	public List<LifestyleOption> getAvailableOptions() {
		return ShadowrunCore.getLifestyleOptions();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#canSelectOption(org.prelle.shadowrun5.LifestyleOption)
	 */
	@Override
	public boolean canSelectOption(LifestyleOption option) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#selectOption(org.prelle.shadowrun5.LifestyleOption)
	 */
	@Override
	public LifestyleOptionValue selectOption(LifestyleOption option) {
		LifestyleOptionValue val = new LifestyleOptionValue(option);
		data.addOption(val);
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#deselectOption(org.prelle.shadowrun5.LifestyleOptionValue)
	 */
	@Override
	public void deselectOption(LifestyleOptionValue option) {
		data.removeOption(option);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#selectSIN(org.prelle.shadowrun5.SIN)
	 */
	@Override
	public void selectSIN(SIN sin) {
		data.setSIN(sin.getUniqueId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#selectName(java.lang.String)
	 */
	@Override
	public void selectName(String name) {
		data.setName(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#selectDescription(java.lang.String)
	 */
	@Override
	public void selectDescription(String text) {
		data.setDescription(text);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.LifestyleController#getLifestyleCost(org.prelle.shadowrun5.Lifestyle, java.util.List)
	 */
	@Override
	public int getLifestyleCost() {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : data.getOptions())
			options.add(opt.getOption());
		
		return ShadowrunTools.getLifestyleCost(model, data.getLifestyle(), options, new ArrayList<>());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SingleLifestyleController#getResult()
	 */
	@Override
	public LifestyleValue getResult() {
		return data;
	}

}
