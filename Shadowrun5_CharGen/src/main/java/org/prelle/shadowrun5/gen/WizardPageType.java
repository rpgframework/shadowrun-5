/**
 *
 */
package org.prelle.shadowrun5.gen;

/**
 * @author Stefan
 *
 */
public enum WizardPageType {

	PRIORITIES,
	METATYPE,
	MAGIC_OR_RESONANCE,
	QUALITIES,
	ATTRIBUTES,
	SKILLS,
	TRADITION,
	SPELLS,
	ALCHEMY,
	RITUALS,
	POWERS,
	COMPLEX_FORMS,
	BODYTECH,
	GEAR,
	VEHICLES,

	NAME,
}
