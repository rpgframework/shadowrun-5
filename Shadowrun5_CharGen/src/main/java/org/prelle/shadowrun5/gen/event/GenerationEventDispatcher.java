/**
 * 
 */
package org.prelle.shadowrun5.gen.event;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.core.BabylonEvent;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author prelle
 *
 */
public class GenerationEventDispatcher {
	
	private static Logger logger = LogManager.getLogger("shadowrun.chargen");
	
	private static Collection<GenerationEventListener> listener;

	//--------------------------------------------------------------------
	static {
		listener = new ArrayList<GenerationEventListener>();
	}

	//--------------------------------------------------------------------
	public static void addListener(GenerationEventListener callback) {
		if (!listener.contains(callback))
			listener.add(callback);
	}

	//--------------------------------------------------------------------
	public static void removeListener(GenerationEventListener callback) {
		listener.remove(callback);
	}

	//--------------------------------------------------------------------
	public static void clear() {
		listener.clear();
	}

	//--------------------------------------------------------------------
	public static void fireEvent(GenerationEvent event) {
		logger.warn("fire "+event.getType()+" "+event.getKey());
		for (GenerationEventListener callback : new ArrayList<>(listener)) {
//			if (event.getType()==GenerationEventType.NUYEN_RECALC_NECESSARY)
//				logger.warn("Deliver "+event+" to "+callback.getClass());
			try {
				callback.handleGenerationEvent(event);
			} catch (Exception e) {
				logger.error("Error delivering generation event",e);
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));				
				BabylonEventBus.fireEvent(new BabylonEvent(
						callback, 
						BabylonEventType.UI_MESSAGE, 
						new Object[]{2,out.toString()}));
			}
		}
	}

}
