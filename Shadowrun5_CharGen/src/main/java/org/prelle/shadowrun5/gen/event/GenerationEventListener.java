/**
 * 
 */
package org.prelle.shadowrun5.gen.event;

/**
 * @author prelle
 *
 */
public interface GenerationEventListener {

	//-------------------------------------------------------------------
	public void handleGenerationEvent(GenerationEvent event);
	
}
