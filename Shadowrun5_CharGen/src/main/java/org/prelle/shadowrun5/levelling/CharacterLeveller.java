/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.LifestyleController;
import org.prelle.shadowrun5.charctrl.ProgramController;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.charctrl.SINController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.common.CommonLifestyleController;
import org.prelle.shadowrun5.common.CommonQualityController;
import org.prelle.shadowrun5.common.CommonSINController;
import org.prelle.shadowrun5.common.ProgramControllerImpl;
import org.prelle.shadowrun5.gen.AdeptPowerGeneratorAndLeveller;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.CarriedItemModification;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.modifications.QualityModification;
import org.prelle.shadowrun5.modifications.SINModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharacterLeveller implements CharacterController {

	private final static Logger logger = LogManager.getLogger("shadowrun.level");

	private ShadowrunCharacter model;
	private SR5LetUserChooseListener callback;
	private List<Modification> undoList;

	private AttributeController attrib;
	private QualityController qualities;
	private SkillController skills;
	private SpellController spells;
	private SpellController alchemy;
	private RitualController rituals;
	private AdeptPowerController powers;
	private ComplexFormController cforms;
	private EquipmentController equip;
	private ConnectionsController connections;
	private SINController sins;
	private LifestyleController lifestyles;
	private SummonableController summonables;

	//-------------------------------------------------------------------
	public CharacterLeveller(ShadowrunCharacter model) {
		this.model = model;
		undoList  = new ArrayList<Modification>();

		attrib = new KarmaAttributeController(model, undoList);
		qualities = new CommonQualityController(model, CharGenMode.LEVELING, undoList);
		skills = new KarmaSkillController(this, CharGenMode.LEVELING, undoList);
		spells = new KarmaSpellController(model, CharGenMode.LEVELING, undoList, false);
		alchemy = new KarmaSpellController(model, CharGenMode.LEVELING, undoList, true);
		rituals= new KarmaRitualController(model, CharGenMode.LEVELING, undoList);
		powers = new AdeptPowerGeneratorAndLeveller(this, CharGenMode.LEVELING, undoList);
		cforms = new KarmaComplexFormController(model, CharGenMode.LEVELING, undoList);
		equip  = new EquipmentLeveller(model);
		connections = new ConnectionLeveller(model);
		sins   = new CommonSINController(model);
		lifestyles = new CommonLifestyleController(model, equip);
		summonables = new NoCostSummonableController(model);
		
		for (Modification mod : ShadowrunTools.getCharacterModifications(model)) {
			if (mod instanceof AttributeModification) {
				// Already applied on load
				continue;
			} else
			if (mod instanceof LifestyleCostModification) {
				apply("From "+mod.getSource(), Arrays.asList(mod));
				continue;
			} else
			if (mod instanceof IncompetentSkillGroupModification) {
				apply("From "+mod.getSource(), Arrays.asList(mod));
				continue;
			} else
			if (mod instanceof CarriedItemModification) {
				continue;
			} else
			if (mod instanceof QualityModification) {
				apply("From "+mod.getSource(), Arrays.asList(mod));
				continue;
			}
			
			logger.error("TODO: apply "+mod.getClass());
			System.exit(0);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
				WizardPageType.PRIORITIES,
				WizardPageType.METATYPE,
				WizardPageType.MAGIC_OR_RESONANCE,
				WizardPageType.TRADITION,
				WizardPageType.QUALITIES,
				WizardPageType.ATTRIBUTES,
				WizardPageType.SKILLS,
				WizardPageType.SPELLS,
				WizardPageType.ALCHEMY,
				WizardPageType.RITUALS,
				WizardPageType.POWERS,
				WizardPageType.COMPLEX_FORMS,
//				WizardPageType.BODYTECH,
//				WizardPageType.GEAR,
//				WizardPageType.VEHICLES,
				WizardPageType.NAME,
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#setCallback(org.prelle.shadowrun5.gen.SR5LetUserChooseListener)
	 */
	@Override
	public void setCallback(SR5LetUserChooseListener callback) {
		this.callback = callback;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getName()
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return CharGenMode.LEVELING;
	}
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#apply(java.util.Collection)
	 */
	@Override
	public Collection<Modification> apply(String reason, Collection<Modification> mods) {
		logger.debug("Apply "+mods);
		for (Modification mod : mods) {
			if (mod instanceof AttributeModification) {
				attrib.apply((AttributeModification)mod);
			} else if (mod instanceof SINModification) {
				sins.apply( (SINModification)mod );
			} else if (mod instanceof LifestyleCostModification) {
				lifestyles.apply( (LifestyleCostModification)mod );
			} else if (mod instanceof IncompetentSkillGroupModification) {
				skills.apply( (IncompetentSkillGroupModification)mod );
			} else
				logger.warn("Don't know how to apply "+mod.getClass()+": "+mod);
		}
		return mods;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#undo(java.util.Collection)
	 */
	@Override
	public void undo(Collection<Modification> mods) {
		logger.debug("Undo "+mods);
		for (Modification mod : mods) {
			if (mod instanceof AttributeModification) {
				attrib.undo((AttributeModification)mod);
			} else if (mod instanceof SINModification) {
				sins.undo( (SINModification)mod );
			} else if (mod instanceof LifestyleCostModification) {
				lifestyles.undo( (LifestyleCostModification)mod );
			} else if (mod instanceof IncompetentSkillGroupModification) {
				skills.undo( (IncompetentSkillGroupModification)mod );
			} else
				logger.warn("Don't know how to undo "+mod.getClass());
		}
	}

	//--------------------------------------------------------------------
	public void updateHistory() {
		logger.info("Update character history");
		Date now = new Date(System.currentTimeMillis());
		for (Modification changed : undoList) {
			changed.setDate(now);
			model.addToHistory(changed);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attrib;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getPowerController()
	 */
	@Override
	public AdeptPowerController getPowerController() {
		return powers;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getEquipmentController()
	 */
	@Override
	public EquipmentController getEquipmentController() {
		return equip;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getComplexFormController()
	 */
	@Override
	public ComplexFormController getComplexFormController() {
		return cforms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getConnectionController()
	 */
	@Override
	public ConnectionsController getConnectionController() {
		return connections;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSINController()
	 */
	@Override
	public SINController getSINController() {
		return sins;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getLifestyleController()
	 */
	@Override
	public LifestyleController getLifestyleController() {
		return lifestyles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSpellController()
	 */
	@Override
	public SpellController getSpellController() {
		return spells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getAlchemyController()
	 */
	@Override
	public SpellController getAlchemyController() {
		return alchemy;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getRitualController()
	 */
	@Override
	public RitualController getRitualController() {
		return rituals;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getQualityController()
	 */
	@Override
	public QualityController getQualityController() {
		return qualities;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.CharacterController#getSummonableController()
	 */
	@Override
	public SummonableController getSummonableController() {
		return summonables;
	}

	//-------------------------------------------------------------------
	public ProgramController getProgramController(CarriedItem item) {
		return new ProgramControllerImpl(getCharacter(), item);
	}

	@Override
	public void runProcessors() {
		// TODO Auto-generated method stub
		
	}

}
