/**
 * 
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author prelle
 *
 */
public class ConnectionLeveller implements ConnectionsController {

	protected static final Logger logger = LogManager.getLogger("shadowrun.lvl");
	
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	/**
	 */
	public ConnectionLeveller(ShadowrunCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canCreateConnection()
	 */
	@Override
	public boolean canCreateConnection() {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#createConnection()
	 */
	@Override
	public Connection createConnection() {
		logger.info("Add new connection");
		Connection data = new Connection(null, null, 1, 1);
		model.addConnection(data);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_ADDED, data));
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#removeConnection(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public void removeConnection(Connection con) {
		logger.info("Remove connection");
		model.removeConnection(con);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseInfluence(Connection con) {
		return con.getInfluence()<12;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseInfluence(Connection con) {
		if (!canIncreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()+1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseInfluence(Connection con) {
		return con.getInfluence()>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseInfluence(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseInfluence(Connection con) {
		if (!canDecreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()-1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canIncreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canIncreaseLoyalty(Connection con) {
		return con.getLoyalty()<6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#increaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean increaseLoyalty(Connection con) {
		if (!canIncreaseLoyalty(con))
			return false;
		
		con.setLoyalty(con.getLoyalty()+1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#canDecreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean canDecreaseLoyalty(Connection con) {
		return con.getLoyalty()>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#decreaseLoyalty(org.prelle.shadowrun5.Connection)
	 */
	@Override
	public boolean decreaseLoyalty(Connection con) {
		if (!canDecreaseLoyalty(con))
			return false;
		
		con.setLoyalty(con.getLoyalty()-1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		List<String> ret = new ArrayList<String>();
//		if (invest!=)
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ConnectionsController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return 0;
	}

}
