package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.common.CommonEquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;

public class EquipmentLeveller extends CommonEquipmentController {

	//-------------------------------------------------------------------
	public EquipmentLeveller(ShadowrunCharacter model) {
		super(model, CharGenMode.LEVELING);
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#increase(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
		if (!super.increase(data))
			return false;
		
		model.setNuyen(model.getNuyen() - instanceCost);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#decrease(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
		if (!super.decrease(data))
			return false;
		
		model.setNuyen(model.getNuyen() + instanceCost);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, data));
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#deselect(org.prelle.shadowrun5.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		logger.info("Deselect "+ref+"  (it exists "+ref.getCount()+" times)");
		boolean removed = super.deselect(ref);
		logger.info("Removing returned "+removed);
		
		model.setNuyen(model.getNuyen() + ref.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, ref));
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#sell(org.prelle.shadowrun5.items.CarriedItem, float)
	 */
	@Override
	public void sell(CarriedItem ref, float factor) {
		logger.info("Sell "+ref+" with factor "+factor+"  (it exists "+ref.getCount()+" times)");
		float grant = ref.getAsValue(ItemAttribute.PRICE).getModifiedValue() * factor;

		if (ref.getCount()>1) {
			// Multiple instances - sell only one instance
			grant = grant / ref.getCount();
			model.setNuyen(model.getNuyen() + Math.round(grant));
			ref.setCount(ref.getCount()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, ref));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, ref));
			return;
		}
		
		// Only one instance
		super.sell(ref, factor);
		
		model.setNuyen(model.getNuyen() + Math.round(grant));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, ref));
		
//		/*
//		 * In case of bodytech, enlarge essence hole
//		 */
//		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getItem().getType())) {
//			float essence = (Float)ref.getAsObject(ItemAttribute.ESSENCECOST);
//			logger.warn("TODO: calculate essence hole");
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canIncreaseBoughtNuyen()
	 */
	@Override
	public boolean canIncreaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#canDecreaseBoughtNuyen()
	 */
	@Override
	public boolean canDecreaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#increaseBoughtNuyen()
	 */
	@Override
	public void increaseBoughtNuyen() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#decreaseBoughtNuyen()
	 */
	@Override
	public void decreaseBoughtNuyen() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.EquipmentController#getBoughtNuyen()
	 */
	@Override
	public int getBoughtNuyen() {
		return 0;
	}

}
