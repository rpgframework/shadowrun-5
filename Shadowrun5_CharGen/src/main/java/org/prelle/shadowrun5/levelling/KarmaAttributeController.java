/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.common.DerivedAttributeCalculator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationValueType;
import org.prelle.shadowrun5.modifications.AttributeModification.ModificationType;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaAttributeController implements AttributeController {

	private static Logger logger = LogManager.getLogger("shadowrun.level");

	private ShadowrunCharacter model;
	private List<Modification> undoList;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaAttributeController(ShadowrunCharacter model, List<Modification> undoList) {
		this.model = model;
		this.undoList = undoList;
		DerivedAttributeCalculator calc = new DerivedAttributeCalculator();
		calc.setData(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return model.getKarmaFree();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getPointsLeftSpecial()
	 */
	@Override
	public int getPointsLeftSpecial() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getMinimalValue(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public int getMinimalValue(Attribute key) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getValueState(org.prelle.shadowrun5.Attribute, int)
	 */
	@Override
	public ValueState getValueState(Attribute key, int value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	private AttributeModification getHighestModification(Attribute key) {
		AttributeModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof AttributeModification))
				continue;
			AttributeModification amod = (AttributeModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeDecreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute key) {
		AttributeModification toUndo = getHighestModification(key);
		return toUndo!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		// Enough karma?
		if (model.getKarmaFree()<getIncreaseCost(key))
			return false;

		AttributeValue val = model.getAttribute(key);
		// Calculate the level (Add points with race bonus)
		// and the maximum
		int level = val.getPoints();
		int max   = Integer.MAX_VALUE;
		for (Modification tmp : val.getModifications()) {
			AttributeModification mod = (AttributeModification)tmp;
			if (mod.getType()==ModificationValueType.MAX) {
				max = Math.min(max, mod.getValue());
			} else
			if (mod.getType()==ModificationValueType.CURRENT) {
				level += mod.getValue();
			}
		}
		// If maximum already reached, deny increasing
		if (level>=max)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#getIncreaseCost(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		return (model.getAttribute(key).getModifiedValue()+1)*5;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#increase(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean increase(Attribute key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;

		int cost = getIncreaseCost(key);

		// Change
		model.getAttribute(key).setPoints( model.getAttribute(key).getPoints() +1);
		logger.info("Increased "+key+" to "+model.getAttribute(key).getModifiedValue()+" paying "+cost+" karma");

		ShadowrunTools.calculateDerived(model);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, model.getAttribute(key)));

		// Make undoable
		AttributeModification mod = new AttributeModification(key, model.getAttribute(key).getPoints());
		mod.setModificationType(ModificationType.ABSOLUTE);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		model.setKarmaFree(model.getKarmaFree()-cost);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(),model.getKarmaInvested()}));

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#decrease(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean decrease(Attribute key) {
		logger.debug("decrease "+key);
		if (!canBeDecreased(key))
			return false;

		AttributeModification toUndo = getHighestModification(key);
		undoList.remove(toUndo);
		logger.debug("UndoList now "+undoList);

		// Change
		model.getAttribute(key).setPoints( model.getAttribute(key).getPoints() -1);
		logger.info("Decreased "+key+" to "+model.getAttribute(key).getModifiedValue()+" granting "+toUndo.getExpCost()+" karma");

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, model.getAttribute(key)));

		// Pay
		model.setKarmaInvested(model.getKarmaInvested()-toUndo.getExpCost());
		model.setKarmaFree(model.getKarmaFree()+toUndo.getExpCost());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, model.getKarmaFree()));

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#apply(org.prelle.shadowrun5.modifications.AttributeModification)
	 */
	@Override
	public void apply(AttributeModification mod) {
		logger.info("apply "+mod);
		if (mod instanceof AttributeModification) {
			AttributeModification aMod = (AttributeModification)mod;
			AttributeValue aVal = model.getAttribute(aMod.getAttribute());
			aVal.addModification(mod);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, aMod.getAttribute(), aVal));
		} else
			logger.error("Got a non-AttributeModification to apply: "+mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#undo(org.prelle.shadowrun5.modifications.AttributeModification)
	 */
	@Override
	public void undo(AttributeModification mod) {
		logger.info("undo "+mod);
		if (mod instanceof AttributeModification) {
			AttributeModification aMod = (AttributeModification)mod;
			AttributeValue aVal = model.getAttribute(aMod.getAttribute());
			aVal.removeModification(mod);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, aMod.getAttribute(), aVal));
		} else
			logger.error("Got a non-AttributeModification to apply: "+mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		return canBeIncreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		return canBeDecreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean increase(AttributeValue value) {
		return increase(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean decrease(AttributeValue value) {
		return decrease(value.getModifyable());
	}

}
