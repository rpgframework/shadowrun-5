/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.ComplexFormModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaComplexFormController implements ComplexFormController {

	private static Logger logger = LogManager.getLogger("shadowrun.level");

	protected ShadowrunCharacter model;
	private CharGenMode mode;
	private List<Modification> undoList;

	private List<ComplexForm> availableComplexForms;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaComplexFormController(ShadowrunCharacter model, CharGenMode mode, List<Modification> undoList) {
		this.model = model;
		this.mode  = mode;
		this.undoList = undoList;
		availableComplexForms   = new ArrayList<ComplexForm>();
		updateAvailableComplexForms();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getComplexFormsLeft()
	 */
	@Override
	public int getComplexFormsLeft() {
		return 0;
	}

	//--------------------------------------------------------------------
	private void updateAvailableComplexForms() {
		logger.debug("START updateAvailableComplexForms");
		availableComplexForms.clear();
		availableComplexForms.addAll(ShadowrunCore.getComplexForms());
		// Remove those the character already has
		for (ComplexFormValue val : model.getComplexForms()) {
			logger.trace("Not available anymore "+val);
			availableComplexForms.remove(val.getModifyable());
		}

		Collections.sort(availableComplexForms);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.COMPLEX_FORMS_AVAILABLE_CHANGED, availableComplexForms));
		logger.debug("STOP updateAvailableComplexForms");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#getAvailableComplexForms()
	 */
	@Override
	public List<ComplexForm> getAvailableComplexForms() {
		return availableComplexForms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeSelected(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public boolean canBeSelected(ComplexForm data) {
		if (data==null)
			throw new NullPointerException();
		// Is it already selected
		for (ComplexFormValue already : model.getComplexForms()) {
			if (already.getModifyable()==data)
				return false;
		}

		// Logic restricts amount of forms
		if (model.getComplexForms().size()>=model.getAttribute(Attribute.LOGIC).getModifiedValue())
			return false;

		return model.getKarmaFree()>=4;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#canBeDeselected(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public boolean canBeDeselected(ComplexFormValue cform) {
		return model.getComplexForms().contains(cform);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#select(org.prelle.shadowrun5.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select complex form '"+data.getId()+"' which is not allowed");
		}

		int cost = 4;
		logger.debug("select "+data+" for "+cost+" karma");

		// Change model
		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);

		// Make undoable
		ComplexFormModification mod = new ComplexFormModification(data, this, null);
		mod.setExpCost(cost);
		undoList.add(mod);

		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.COMPLEX_FORM_ADDED, ref));

		// Pay karma
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		model.setKarmaFree(model.getKarmaFree() -cost);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		updateAvailableComplexForms();
		return ref;
	}

	//-------------------------------------------------------------------
	private ComplexFormModification getModification(ComplexForm key) {
		ComplexFormModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof ComplexFormModification))
				continue;
			ComplexFormModification amod = (ComplexFormModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.ComplexFormController#deselect(org.prelle.shadowrun5.ComplexFormValue)
	 */
	@Override
	public void deselect(ComplexFormValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return;

		model.removeComplexForm(ref);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.COMPLEX_FORM_REMOVED, ref));

		int cost = 5;
		// Something to undo?
		ComplexFormModification mod = getModification(ref.getModifyable());
		if (mod!=null) {
			cost = mod.getExpCost();
			undoList.remove(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null));

		updateAvailableComplexForms();
	}

}
