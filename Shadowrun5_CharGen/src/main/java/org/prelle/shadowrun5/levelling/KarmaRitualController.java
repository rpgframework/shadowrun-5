/**
 * 
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Ritual;
import org.prelle.shadowrun5.RitualValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.RitualModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaRitualController implements RitualController {
	
	private static Logger logger = LogManager.getLogger("shadowrun.level");

	private ShadowrunCharacter model;

	private CharGenMode mode;
	private List<Modification> undoList;
	protected List<Ritual> recRituals;
	protected List<Ritual> avRituals;
	
	//-------------------------------------------------------------------
	/**
	 */
	public KarmaRitualController(ShadowrunCharacter model, CharGenMode mode, List<Modification> undoList) {
		this.model = model;
		this.mode  = mode;
		this.undoList = undoList;
		avRituals = new ArrayList<>();
		recRituals = new ArrayList<>();
		updateAvailable();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		logger.debug("START updateAvailable");
		
		avRituals.clear();
		avRituals.addAll(ShadowrunCore.getRituals());
		// Remove those the character already has
		for (SkillValue val : model.getSkillValues(true)) {
			logger.trace("Not available anymore "+val);
			avRituals.remove(val.getModifyable());
		}
		
		Collections.sort(avRituals);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.RITUALS_AVAILABLE_CHANGED, avRituals));
		logger.debug("STOP updateAvailable");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getRitualsLeft()
	 */
	@Override
	public int getRitualsLeft() {
		return -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#getAvailableRituals()
	 */
	@Override
	public List<Ritual> getAvailableRituals() {
		return avRituals;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeSelected(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public boolean canBeSelected(Ritual spell) {
		// Not already selected
		if (model.getRituals().contains(spell)) 
			return false;
		
		if (mode==CharGenMode.CREATING && model.getRituals().size()>= (model.getAttribute(Attribute.MAGIC).getBought()*2))
			return false;
		
		// Enough karma?
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#canBeSelected(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public boolean canBeDeselected(RitualValue spell) {
		return model.getRituals().contains(spell);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.RitualController#select(org.prelle.shadowrun5.Ritual)
	 */
	@Override
	public RitualValue select(Ritual spell) {
		if (!canBeSelected(spell)) {
			logger.warn("Trying to select spell "+spell+" which cannot be selected");
			return null;
		}
		
		int cost = 5;
		logger.debug("select "+spell+" for "+cost+" karma");

		
		if (!canBeSelected(spell)) {
			logger.warn("Trying to select skill "+spell+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected spell "+spell);
		RitualValue sVal = new RitualValue(spell);
		model.addRitual(sVal);
		
		// Make undoable
		RitualModification mod = new RitualModification(spell);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);
	
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.RITUAL_ADDED, sVal));

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		
		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null));
		
		updateAvailable();
		return sVal;
	}

	//-------------------------------------------------------------------
	private RitualModification getModification(Ritual key) {
		RitualModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof RitualModification))
				continue;
			RitualModification amod = (RitualModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public void deselect(RitualValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return;
	
		model.removeRitual(ref);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.RITUAL_REMOVED, ref));

		int cost = 5;
		// Something to undo?
		RitualModification mod = getModification(ref.getModifyable());
		if (mod!=null) {
			cost = mod.getExpCost();
			undoList.remove(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null));
		
		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

}
