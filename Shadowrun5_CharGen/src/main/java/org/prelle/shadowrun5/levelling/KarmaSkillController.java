/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.common.CommonSkillController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.modifications.SkillSpecializationModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaSkillController extends CommonSkillController implements SkillController {

	private static Logger logger = LogManager.getLogger("shadowrun.level");

	private CharGenMode mode;
	private List<Modification> undoList;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaSkillController(CharacterController parent, CharGenMode mode, List<Modification> undoList) {
		super(parent);
		this.model = parent.getCharacter();
		this.mode  = mode;
		this.undoList = undoList;
		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkills()
	 */
	@Override
	public int getPointsLeftSkills() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftSkillGroups()
	 */
	@Override
	public int getPointsLeftSkillGroups() {
		// Return 1 to have skill groups appear in available
		return 1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeDecreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeDecreased(SkillValue val) {
		SkillModification toUndo = getHighestModification(val.getModifyable());
		return toUndo!=null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean canBeIncreased(SkillValue val) {
		// Enough karma?
		if (model.getKarmaFree()<getIncreaseCost(val.getModifyable()))
			return false;

		switch (mode) {
		case CREATING:
			if (val.getModifiedValue()>=6)
				return false;
			break;
		case LEVELING:
			if (val.getModifiedValue()>=12)
				return false;
			break;
		}

		// Skills for which groups character is incompetent, cannbot be increased
		for (QualityValue qual : model.getQualities()) {
			if (qual.getModifyable().getId().equals("incompetent")) {
				logger.warn("Check for incomponence: ref="+qual.getChoiceReference()+" // choice="+qual.getChoice());
				SkillGroup grp = (SkillGroup)qual.getChoice();
				if (val.getModifyable().getGroup()==grp)
					return false;
			}
		}
		
		
		return true;
	}

	//-------------------------------------------------------------------
	private int getIncreaseCost(Skill key) {
		SkillValue sVal = model.getSkillValue(key);
		int newVal = (sVal==null)?1:(sVal.getModifiedValue()+1);

		int cost = newVal*2;
		if (key.getType()==SkillType.KNOWLEDGE || key.getType()==SkillType.LANGUAGE) {
			cost = newVal;
		}
		
		// Quality "Jack of all traces" (Run Faster)
		if (model.hasQuality("jack_of_all_trades_master_of_none")) {
			if (sVal.getPoints()<=5)
				cost-=1;
			else
				cost+=2;
		}
		// Quality "Linguist" (Run Faster)
		if (model.hasQuality("linguist") && key.getId().equals("language") && model.getSkillValue(key).getModifiedValue()>=3) {
			cost--;
		}
		// Quality "College Education" (Run Faster)
		if (model.hasQuality("college_education_rf") && key.getId().equals("acedemic_knowledge") && model.getSkillValue(key).getModifiedValue()>=3) {
			cost--;
		}
		
		
		
		if (cost<=0) cost=1;
		
		return cost;
	}

	//-------------------------------------------------------------------
	private int getIncreaseCost(SkillGroup key) {
		SkillGroupValue sVal = model.getSkillGroupValue(key);
		int newVal = (sVal==null)?1:(sVal.getModifiedValue()+1);
		return newVal*5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public SkillValue select(Skill skill) {
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		int cost = getIncreaseCost(skill);
		logger.debug("select "+skill+" for "+cost+" karma");

		SkillValue val = super.select(skill);
		if (val==null)
			return null;

		// Make undoable
		SkillModification mod = new SkillModification(skill, 1);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canSpecializeIn(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canSpecializeIn(SkillValue val) {
		return !val.getModifyable().getSpecializations().isEmpty() && model.getKarmaFree()>=7;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean deselect(SkillValue ref, SkillSpecialization spec) {
		if (!canBeDeselected(ref, spec)) {
			logger.warn("Trying to deselect "+spec+" with cannot be done");
			return false;
		}
		
		logger.info("Deselect specialization "+spec+" from "+ref);
		super.deselect(ref, spec);
		SkillSpecializationModification toUndo = getSpecializationModification(ref.getModifyable(), spec);
		undoList.remove(toUndo);
		logger.debug("UndoList now "+undoList);

		// Pay karma
		int cost = toUndo.getExpCost(); // getIncreaseCost(ref.getModifyable());
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null, getPointsLeftSkills()));
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec) {
		if (!canBeSelected(ref, spec)) {
			logger.warn("Trying to select specialization "+spec+" which cannot be selected");
			return null;
		}

		int cost = 7;

		logger.debug("  select specialization "+spec+" in skill "+ref);
		SkillSpecializationValue val = super.select(ref, spec);

		// Make undoable
		SkillSpecializationModification mod = new SkillSpecializationModification(ref.getModifyable(), spec);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#select(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public SkillGroupValue select(SkillGroup grp) {
		if (!canBeSelected(grp)) {
			logger.warn("Trying to select skillgroup "+grp+" which cannot be selected");
			return null;
		}

		int cost = getIncreaseCost(grp);
		logger.debug("select "+grp+" for "+cost+" karma");

		SkillGroupValue val = super.select(grp);
		if (val==null)
			return null;

		// Make undoable
		SkillGroupModification mod = new SkillGroupModification(grp, 1);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		if (!canBeIncreased(ref)) {
			logger.warn("Trying to increase skill "+ref+" which cannot be increased");
			return false;
		}

		int cost = getIncreaseCost(ref.getModifyable());
		logger.debug("try increase "+ref+" for "+cost+" karma");
		if (!super.increase(ref))
			return false;

		// Make undoable
		SkillModification mod = new SkillModification(ref.getModifyable(), ref.getPoints());
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#increase(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean increase(SkillGroupValue ref) {
		if (!canBeIncreased(ref)) {
			logger.warn("Trying to increase skill "+ref+" which cannot be increased");
			return false;
		}

		int cost = getIncreaseCost(ref.getModifyable());
		logger.debug("try increase "+ref+" for "+cost+" karma");
		if (!super.increase(ref))
			return false;

		// Make undoable
		SkillGroupModification mod = new SkillGroupModification(ref.getModifyable(), ref.getPoints());
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		return true;
	}

	//-------------------------------------------------------------------
	private SkillModification getHighestModification(Skill key) {
		SkillModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof SkillModification))
				continue;
			SkillModification amod = (SkillModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private SkillSpecializationModification getSpecializationModification(Skill key, SkillSpecialization spec) {
		for (Modification mod : undoList) {
			logger.debug("  check "+mod+" / "+mod.getClass());
			if (!(mod instanceof SkillSpecializationModification))
				continue;
			SkillSpecializationModification amod = (SkillSpecializationModification)mod;
			logger.debug("  compare "+amod.getSkillSpecialization()+" with "+spec);
			if (amod.getSkillSpecialization()==spec)
				return amod;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private SkillGroupModification getHighestModification(SkillGroup key) {
		SkillGroupModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof SkillGroupModification))
				continue;
			SkillGroupModification amod = (SkillGroupModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.AttributeController#decrease(org.prelle.shadowrun5.Attribute)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		logger.debug("try decrease "+ref);
		if (!super.decrease(ref))
			return false;

		SkillModification toUndo = getHighestModification(ref.getModifyable());
		undoList.remove(toUndo);
		logger.debug("UndoList now "+undoList);

		// Pay karma
		int cost = toUndo.getExpCost(); // getIncreaseCost(ref.getModifyable());
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null, getPointsLeftSkills()));

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.common.CommonSkillController#decrease(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean decrease(SkillGroupValue ref) {
		logger.debug("try decrease "+ref);
		if (!super.decrease(ref))
			return false;

		SkillGroupModification toUndo = getHighestModification(ref.getModifyable());
		undoList.remove(toUndo);
		logger.debug("UndoList now "+undoList);

		// Pay karma
		int cost = toUndo.getExpCost(); // getIncreaseCost(ref.getModifyable());
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null, getPointsLeftSkills()));

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillGroup)
	 */
	@Override
	public boolean canBeSelected(SkillGroup grp) {
		// Not already selected
		boolean found = model.getSkillGroupValues().stream().anyMatch(val -> val.getModifyable()==grp);
		if (found)
			return false;
		// Enough karma?
		return model.getKarmaFree()>=getIncreaseCost(grp);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.Skill)
	 */
	@Override
	public boolean canBeSelected(Skill skill) {
		if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE)
			return model.getKarmaFree()>=getIncreaseCost(skill);
		
		// Not already selected
		boolean found = model.getSkillValues(false).stream().anyMatch(val -> val.getModifyable()==skill);
		if (found)
			return false;
		// Enough karma?
		return model.getKarmaFree()>=getIncreaseCost(skill);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeSelected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec) {
		// Skill should exist in Character
		if (model.getSkillValue(ref.getModifyable())==null)
			return false;
		// Characters SkillValue should match given skill value
		if (model.getSkillValue(ref.getModifyable())!=ref)
			return false;
		
		// Value should be at least 1
		if (ref.getPoints()<1)
			return false;
		
		// Specialization should not exist yet
		if (ref.hasSpecialization(spec))
			return false;
		
		// Requires 7 karma
		return model.getKarmaFree()>=7;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#selectKnowledgeOrLanguage(org.prelle.shadowrun5.Skill, java.lang.String)
	 */
	@Override
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName) {
		logger.debug("try select "+skill);
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		int cost = getIncreaseCost(skill);
		logger.debug("select "+skill+" for "+cost+" karma");

		// Change model
		logger.info("Selected skill "+skill);
		SkillValue sVal = new SkillValue(skill, 1);
		sVal.setName(customName);
		model.addSkill(sVal);
	
		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SKILL_ADDED, sVal));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_SKILLS, null, getPointsLeftSkills()));
		updateAvailable();

		// Make undoable
		SkillModification mod = new SkillModification(skill, 1);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));

		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDeselected(SkillGroupValue ref) {
		SkillGroupModification toUndo = getHighestModification(ref.getModifyable());
		if (toUndo==null)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref) {
		SkillModification toUndo = getHighestModification(ref.getModifyable());
		if (toUndo==null)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun5.SkillValue, org.prelle.shadowrun5.SkillSpecialization)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec) {
		SkillSpecializationModification toUndo = getSpecializationModification(ref.getModifyable(), spec);
		logger.debug("canBeDeselected = "+toUndo);
		if (toUndo==null)
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeIncreased(SkillGroupValue val) {
		// Enough karma?
		if (model.getKarmaFree()<getIncreaseCost(val.getModifyable()))
			return false;

		switch (mode) {
		case CREATING:
			if (val.getModifiedValue()>=6)
				return false;
			break;
		case LEVELING:
			if (val.getModifiedValue()>=12)
				return false;
			break;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBeDecreased(SkillGroupValue val) {
		SkillGroupModification toUndo = getHighestModification(val.getModifyable());
		return toUndo!=null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#canBreakSkillGroup(org.prelle.shadowrun5.SkillGroupValue)
	 */
	@Override
	public boolean canBreakSkillGroup(SkillGroupValue ref) {
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#getPointsLeftInKnowledgeAndLanguage()
	 */
	@Override
	public int getPointsLeftInKnowledgeAndLanguage() {
		return 0;
	}

}
