/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.Tradition;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.SpellModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaSpellController implements SpellController {

	private static Logger logger = LogManager.getLogger("shadowrun.level");

	private ShadowrunCharacter model;
	private boolean alchemistic;
	private CharGenMode mode;
	private List<Modification> undoList;
	protected List<Spell> recSpells;
	protected List<Spell> avSpells;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaSpellController(ShadowrunCharacter model, CharGenMode mode, List<Modification> undoList, boolean alchemistic) {
		this.model = model;
		this.mode  = mode;
		this.undoList = undoList;
		this.alchemistic = alchemistic;
		avSpells = new ArrayList<>();
		recSpells = new ArrayList<>();
		updateAvailable();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		logger.debug("START updateAvailable");

		avSpells.clear();
		avSpells.addAll(ShadowrunCore.getSpells());
		// Remove those the character already has
		for (SpellValue val : model.getSpells(alchemistic)) {
			logger.trace("Not available anymore "+val);
			avSpells.remove(val.getModifyable());
		}

		Collections.sort(avSpells);

		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.SPELLS_AVAILABLE_CHANGED, avSpells));
		logger.debug("STOP updateAvailable");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return avSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun5.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Spell.Category category, boolean alchemistic) {
		return getAvailableSpells(alchemistic).stream().filter(test -> test.getCategory()==category).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeSelected(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell spell, boolean alchemistic) {
		// Not already selected
		for (SpellValue tmp : model.getSpells(alchemistic)) {
			if (tmp.getModifyable()==spell)
				return false;
		}

		if (mode==CharGenMode.CREATING && model.getSpells().size()>= (model.getAttribute(Attribute.MAGIC).getBought()*2))
			return false;

		// Enough karma?
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#canBeSelected(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public boolean canBeDeselected(SpellValue spell) {
		return model.getSpells().contains(spell);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#select(org.prelle.shadowrun5.Spell)
	 */
	@Override
	public SpellValue select(Spell spell, boolean alchemistic) {
		if (!canBeSelected(spell,alchemistic)) {
			logger.warn("Trying to select spell "+spell+" which cannot be selected");
			return null;
		}

		int cost = 5;
		logger.debug("select "+spell+" for "+cost+" karma");

		// Change model
		logger.info("Selected spell "+spell);
		SpellValue sVal = new SpellValue(spell);
		sVal.setAlchemistic(alchemistic);
		model.addSpell(sVal);

		// Make undoable
		SpellModification mod = new SpellModification(spell);
		mod.setExpCost(cost);
		mod.setSource(this);
		undoList.add(mod);

		// Fire event
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SPELL_ADDED, sVal));

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null));

		updateAvailable();
		return sVal;
	}

	//-------------------------------------------------------------------
	private SpellModification getModification(Spell key) {
		SpellModification ret = null;
		for (Modification mod : undoList) {
			if (!(mod instanceof SpellModification))
				continue;
			SpellModification amod = (SpellModification)mod;
			if (mod.getSource()!=this)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SkillController#deselect(org.prelle.shadowrun5.SkillValue)
	 */
	@Override
	public void deselect(SpellValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return;

		model.removeSpell(ref);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SPELL_REMOVED, ref));

		int cost = 5;
		// Something to undo?
		SpellModification mod = getModification(ref.getModifyable());
		if (mod!=null) {
			cost = mod.getExpCost();
			undoList.remove(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null));

		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun5.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.TRADITION_CHANGED, data));
	}

}
