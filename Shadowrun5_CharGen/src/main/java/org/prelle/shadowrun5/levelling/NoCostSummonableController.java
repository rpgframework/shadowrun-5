/**
 *
 */
package org.prelle.shadowrun5.levelling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.SummonableValue;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;

/**
 * @author Stefan
 *
 */
public class NoCostSummonableController implements SummonableController, GenerationEventListener {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen");

	protected ShadowrunCharacter model;
	private List<Summonable> available;

	//--------------------------------------------------------------------
	/**
	 */
	public NoCostSummonableController(ShadowrunCharacter model) {
		this.model = model;

		updateAvailable();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getModel()
	 */
	@Override
	public ShadowrunCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.Controller#getToDos()
	 */
	@Override
	public List<String> getToDos() {
		return new ArrayList<String>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.MAGICORRESONANCE_CHANGED) {
			logger.debug("RCV "+event);
			updateAvailable();
		}
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		available = new ArrayList<>();
		if (model!=null && model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesResonance()) {
				available.addAll(ShadowrunCore.getSprites());
			}
			if (model.getMagicOrResonanceType().usesSpells()) {
				available.addAll(ShadowrunCore.getSpirits());
			}
		}

		Collections.sort(available);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLES_AVAILABLE_CHANGED, available));
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#getSummonables()
	 */
	@Override
	public List<Summonable> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeSelected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeSelected(Summonable value) {
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#canBeDeselected(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public boolean canBeDeselected(SummonableValue value) {
		return model.getSummonables().contains(value);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#select(org.prelle.shadowrun5.Summonable)
	 */
	@Override
	public SummonableValue select(Summonable value, int services) {
		if (!canBeSelected(value))
			return null;

		int rating = model.getAttribute(Attribute.MAGIC).getModifiedValue();
		logger.info("Select summonable "+value.getId()+" with rating "+rating);
		SummonableValue val = new SummonableValue(value, rating, services);
		model.addSummonable(val);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLE_ADDED, val));

		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.charctrl.SummonableController#delect(org.prelle.shadowrun5.SummonableValue)
	 */
	@Override
	public void deselect(SummonableValue value) {
		if (!canBeDeselected(value))
			return;

		model.removeSummonable(value);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SUMMONABLE_REMOVED, value));
	}

}
