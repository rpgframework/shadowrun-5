package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemAttributeValue;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemTemplate.Legality;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class CarriedItemTest {

	private static ItemTemplate ARES_PREDATOR;
	private static ItemTemplate GAS_VENT;
	private static ItemTemplate IMAGING_SCOPE;
	private static ItemTemplate LOW_LIGHT_VISION;

	private ShadowrunCharacter model;
	private Serializer serializer;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
//		System.exit(0);

		ARES_PREDATOR = ShadowrunCore.getItem("ares_predator_v");
		GAS_VENT = ShadowrunCore.getItem("gas-vent_system");
		IMAGING_SCOPE = ShadowrunCore.getItem("imaging_scope");
		LOW_LIGHT_VISION = ShadowrunCore.getItem("low_light_vision");

	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testTemplates() {
		assertEquals(5, ARES_PREDATOR.getWeaponData().getAccuracy());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleAres() {
		CarriedItem item = new CarriedItem(ARES_PREDATOR);
		assertNotNull(item);

		ItemAttributeValue accVal = item.getAsValue(ItemAttribute.ACCURACY);
		assertNotNull(accVal);
		assertEquals(5, accVal.getPoints());
		assertEquals(7, accVal.getModifiedValue());
		// Recoil
		ItemAttributeValue rcVal = item.getAsValue(ItemAttribute.RECOIL_COMPENSATION);
		assertNotNull(rcVal);
		assertEquals(0, rcVal.getPoints());
		assertEquals(0, rcVal.getModifiedValue());
		// Price
		assertEquals(725, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		// Availability
		Availability avail1 = item.getAvailability();
		Availability avail2 = (Availability) item.getAsObject(ItemAttribute.AVAILABILITY);
		assertEquals(avail1, avail2);
		assertEquals(5, avail2.getValue());
		assertEquals(Legality.RESTRICTED, avail2.getLegality());

		// Check slots
		assertNotNull( item.getSlot(ItemHook.TOP   ) );
		assertNotNull( item.getSlot(ItemHook.BARREL) );
	}

	//-------------------------------------------------------------------
	@Test
	public void testGasVent() {
		System.out.println("-------testGasVent()------------------------");
		CarriedItem item = new CarriedItem(GAS_VENT);
		item.setRating(2);

		// Price
		assertEquals(400, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		// Availability
		Availability avail1 = item.getAvailability();
		Availability avail2 = (Availability) item.getAsObject(ItemAttribute.AVAILABILITY);
		assertEquals(avail1, avail2);
		assertEquals(6, avail2.getValue());
		assertEquals(Legality.RESTRICTED, avail2.getLegality());
	}

	//-------------------------------------------------------------------
	@Test
	public void testImagingScope() {
		System.out.println("-------testImagingScope()------------------------");
		CarriedItem item = new CarriedItem(IMAGING_SCOPE);
		CarriedItem vision = new CarriedItem(LOW_LIGHT_VISION);
		item.addAccessory(ItemHook.OPTICAL, vision);

		// Price
//		assertEquals(300, item.getAsValue(ItemAttribute.PRICE).getPoints());
		assertEquals(800, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		// Availability
		Availability avail1 = item.getAvailability();
		Availability avail2 = (Availability) item.getAsObject(ItemAttribute.AVAILABILITY);
		assertEquals(avail1, avail2);
		assertEquals(Legality.LEGAL, avail2.getLegality());
		assertEquals("Availability additions not supported", 6, avail2.getValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testAresWithAccessories() {
		System.out.println("-------testAresWithAccessories()------------------------");
		CarriedItem gasVent = new CarriedItem(GAS_VENT);
		gasVent.setRating(3);
		CarriedItem item = new CarriedItem(ARES_PREDATOR);
		item.addAccessory(ItemHook.BARREL, gasVent);
		assertNotNull(item);

		ItemAttributeValue accVal = item.getAsValue(ItemAttribute.ACCURACY);
		assertNotNull(accVal);
		assertEquals(5, accVal.getPoints());
		assertEquals(7, accVal.getModifiedValue());
		// Recoil
		ItemAttributeValue rcVal = item.getAsValue(ItemAttribute.RECOIL_COMPENSATION);
		assertNotNull(rcVal);
		assertEquals(0, rcVal.getPoints());
		assertEquals(3, rcVal.getModifiedValue());
		// Price
		assertEquals(1325, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		// Availability
		Availability avail1 = item.getAvailability();
		Availability avail2 = (Availability) item.getAsObject(ItemAttribute.AVAILABILITY);
		assertEquals(avail1, avail2);
		assertEquals(9, avail2.getValue());
		assertEquals(Legality.RESTRICTED, avail2.getLegality());

//		try {
//			StringWriter out = new StringWriter();
//			serializer.write(item, out);
//			System.out.println(out.toString());
//
////			assertEquals(DATA, out.toString());
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail(e.toString());
//		}
	}

	//-------------------------------------------------------------------
	@Test
	public void testAresWithTwoAccessories() {
		System.out.println("-------testAresWithTwoAccessories()------------------------");
		CarriedItem gasVent = new CarriedItem(GAS_VENT);
		gasVent.setRating(3);
		CarriedItem item = new CarriedItem(ARES_PREDATOR);
		item.addAccessory(ItemHook.BARREL, gasVent);
		assertNotNull(item);

		ItemAttributeValue accVal = item.getAsValue(ItemAttribute.ACCURACY);
		assertNotNull(accVal);
		assertEquals(5, accVal.getPoints());
		assertEquals(7, accVal.getModifiedValue());
		// Recoil
		ItemAttributeValue rcVal = item.getAsValue(ItemAttribute.RECOIL_COMPENSATION);
		assertNotNull(rcVal);
		assertEquals(0, rcVal.getPoints());
		assertEquals(3, rcVal.getModifiedValue());
		// Price
		assertEquals(1325, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		// Availability
		Availability avail1 = item.getAvailability();
		Availability avail2 = (Availability) item.getAsObject(ItemAttribute.AVAILABILITY);
		assertEquals(avail1, avail2);
		assertEquals(9, avail2.getValue());
		assertEquals(Legality.RESTRICTED, avail2.getLegality());

		try {
			StringWriter out = new StringWriter();
			serializer.write(item, out);
			System.out.println(out.toString());

//			assertEquals(DATA, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
