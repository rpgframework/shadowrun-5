package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.gen.PriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;

/**
 * @author prelle
 *
 */
public class EquipmentControllerTest {

	private PriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new PriorityCharacterGenerator();
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	@Test
	public void checkAresLF() {
		ItemTemplate tmpImaging = ShadowrunCore.getItem("imaging_scope");
		ItemTemplate tmpSilencer= ShadowrunCore.getItem("ares_lf_silencer");
		ItemTemplate tmpLowLight= ShadowrunCore.getItem("low_light_vision");

		EquipmentController ctrl = charGen.getEquipmentController();
		CarriedItem weapon = ctrl.select(ShadowrunCore.getItem("ares_light_fire_75"));

		System.out.println("Dump Ares Light Fire 75");
		System.out.println("Weapon: "+weapon.getName());
		for (AvailableSlot slot : weapon.getSlots()) {
			for (CarriedItem accessory : slot.getAllEmbeddedItems()) {
				System.out.println("- "+accessory.getName()+" \t("+slot.getSlot().getName()+")");
				// WiFi advantages
				for (String wifi : accessory.getWiFiAdvantageStrings()) {
					System.out.println("    WIFI: "+wifi);
				}
			}
		}

		// What is already included
		Collection<CarriedItem> access = weapon.getAccessories();
		assertNotNull(access);
		boolean found = false;
		for (CarriedItem val : access) {
			if (val.getItem()==tmpSilencer)
				found=true;
		}
		assertTrue("Included silencer not found", found);

		// INTERNAL and BARREL and UNDER are not available
//		assertEquals(ctrl.getEmbeddableIn(weapon, ItemHook.INTERNAL).toString(), 0, ctrl.getEmbeddableIn(weapon, ItemHook.INTERNAL).size());
		assertEquals(ctrl.getEmbeddableIn(weapon, ItemHook.BARREL).toString(), 2, ctrl.getEmbeddableIn(weapon, ItemHook.BARREL).size());  // Ehemals 4 ??
		assertEquals(ctrl.getEmbeddableIn(weapon, ItemHook.UNDER).toString(), 0, ctrl.getEmbeddableIn(weapon, ItemHook.UNDER).size());
		// There should be options for TOP slot
		List<ItemTemplate> opt = ctrl.getEmbeddableIn(weapon, ItemHook.TOP);
		assertFalse(opt.isEmpty());
		assertTrue(opt.contains(tmpImaging));

		// Add imaging
		assertTrue(ctrl.canBeEmbedded(weapon, ctrl.select(tmpImaging), ItemHook.TOP));
		CarriedItem imaging = ctrl.embed(weapon, tmpImaging);
		System.out.println("Embedding = "+imaging);
		found = false;
		for (CarriedItem val : weapon.getAccessories()) {
			if (val.getItem()==tmpImaging)
				found=true;
		}
		assertTrue("Embedded imaging scope not found", found);
		

		System.out.println("Dump Ares Light Fire 75");
		System.out.println("Weapon: "+weapon.getName());
		for (AvailableSlot slot : weapon.getSlots()) {
			if (slot.getAllEmbeddedItems().isEmpty()) {
				System.out.println("- Slot "+slot.getSlot()+": empty");
			}
			for (CarriedItem accessory : slot.getAllEmbeddedItems()) {
				System.out.println("- "+accessory.getName()+" \t("+slot.getSlot().getName()+")");
				// WiFi advantages
				for (String wifi : accessory.getWiFiAdvantageStrings()) {
					System.out.println("    WIFI: "+wifi);
				}
			}
		}
		
		assertTrue("Missing slot from imaging scope", imaging.hasHook(ItemHook.OPTICAL));
		assertEquals(ctrl.getEmbeddableIn(imaging, ItemHook.OPTICAL).toString(), 7, ctrl.getEmbeddableIn(imaging, ItemHook.OPTICAL).size());
		assertTrue(ctrl.canBeEmbedded(imaging, ctrl.select(tmpLowLight), ItemHook.OPTICAL));
		assertNotNull(ctrl.embed(imaging, tmpLowLight));

		System.out.println("Dump Ares Light Fire 75 with optical");
		System.out.println("Weapon: "+weapon.dump());
		
		StringWriter out = new StringWriter();
		Persister serial = new Persister();
		try {
			serial.write(weapon, out);
			System.out.println(out.toString());
		} catch (SerializationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
