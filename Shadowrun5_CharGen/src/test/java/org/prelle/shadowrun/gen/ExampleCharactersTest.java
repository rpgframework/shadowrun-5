package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun5.data.Shadowrun5DataPlugin;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.LicenseValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.SummonableValue;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.ShadowrunCharacter.Gender;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.SingleLifestyleController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.chargen.PriorityConnectionController;
import org.prelle.shadowrun5.chargen.PriorityEquipmentController;
import org.prelle.shadowrun5.chargen.PriorityMagicOrResonanceController;
import org.prelle.shadowrun5.gen.EquipmentGenerator;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.gen.SingleLifestyleGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ExampleCharactersTest implements SR5LetUserChooseListener {

	private NewPriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun5DataPlugin plugin = new Shadowrun5DataPlugin();
		plugin.init(null);
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new NewPriorityCharacterGenerator();
		charGen.setCallback(this);
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	@Test
	public void generateRob() {
		assertEquals(25, model.getKarmaFree());

		// S.66 Qualities
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("sinner_national"))!=null);
		assertEquals(30, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("exceptional_attribute"), Attribute.STRENGTH)!=null);
		assertEquals(16, model.getKarmaFree());

		// S.68 Metatype
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		charGen.getMetatypeController().select(
				ShadowrunCore.getMetaType("troll"));

		// S.68 Attributes
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.A);
		AttributeController attrib = charGen.getAttributeController();
		// Body +4
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.BODY));
		// Agility +3
		for (int i=0; i<3; i++)
			assertTrue(attrib.increase(Attribute.AGILITY));
		// Reaction +2
		for (int i=0; i<2; i++)
			assertTrue(attrib.increase(Attribute.REACTION));
		// Reaction +6
		for (int i=0; i<6; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.STRENGTH));
		// Willpower +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.WILLPOWER));
		// Logic +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.LOGIC));
		// Intuition +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.INTUITION));
		// Charisma +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.CHARISMA));

		assertEquals(9, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.REACTION).getModifiedValue());
		assertEquals(11, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.LOGIC).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.CHARISMA).getModifiedValue());

		// S.71 Magic and resonance
		charGen.setPriority(PriorityType.MAGIC, Priority.E);
		MagicOrResonanceType mType = ShadowrunCore.getMagicOrResonanceType("mundane");
		for (MagicOrResonanceOption opt : charGen.getMagicOrResonanceController().getAvailable()) {
			if (opt.getType()==mType)
				charGen.getMagicOrResonanceController().select(opt);
		}
		assertNotNull(model.getMagicOrResonanceType());

		// S.76 Skills
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		SkillController skills = charGen.getSkillController();
		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("ATHLETICS"));
		assertNotNull("Could not select skillgroup", sgVal);
		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("pilot_ground_craft"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("computer"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("first_aid"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("longarms"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("blades"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("pistols"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("automatics"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("heavyweapons"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("unarmed"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("throwing"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(12,skills.getPointsLeftInKnowledgeAndLanguage());

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("professional_knowledge"), "Militärvorschriften der UCAS");
		assertNotNull(sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Runnertreffpunkte");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Schieber");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("professional_knowledge"), "Sioux-Nation");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("professional_knowledge"), "Stadr Bismarck");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Straßenkliniken");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());


		/*
		 * S. 81 Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		assertEquals(50000, model.getNuyen());
		assertEquals(16, model.getKarmaFree());
		for (int i=0; i<10; i++) {
			if (ctrl instanceof EquipmentGenerator)
				((EquipmentGenerator)ctrl).increaseBoughtNuyen();
			if (ctrl instanceof PriorityEquipmentController)
				((PriorityEquipmentController)ctrl).increaseBoughtNuyen();
		}
		assertEquals(70000, model.getNuyen());
		// SHopping
		ctrl.select(ShadowrunCore.getItem("fn_har"));
		SIN sin = charGen.getSINController().createNewSIN("Fake 1", Quality.SUPERFICIALLY_PLAUSIBLE);
		assertEquals(58500, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("colt_cobra_tz120"));
		LicenseValue license = charGen.getSINController().createNewLicense("Waffenschein", SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
		license.setSIN(sin.getUniqueId());
		assertEquals(57040, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("ares_predator_v"));
		ctrl.select(ShadowrunCore.getItem("credstick_silver"));
		assertEquals(56295, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("katana"));
		ctrl.select(ShadowrunCore.getItem("renraku_sensei"));
		assertEquals(54295, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("knucks"));
		ctrl.select(ShadowrunCore.getItem("high_explosive"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
		assertEquals(53895, model.getNuyen());

		CarriedItem jacket = ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		assertEquals(1000, jacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(jacket, ShadowrunCore.getItem("nonconductivity"), new SelectionOption(SelectionOptionType.AMOUNT, 4));
		assertEquals(2000, jacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.select(ShadowrunCore.getItem("explosive_rounds"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		assertEquals(51495, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("reaktionsverbesserung"), new SelectionOption(SelectionOptionType.RATING, 2));
		ctrl.select(ShadowrunCore.getItem("hollow_points"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		assertEquals(25355, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("regular_ammo"), new SelectionOption(SelectionOptionType.AMOUNT, 30));
		ctrl.select(ShadowrunCore.getItem("spare_clip"), new SelectionOption(SelectionOptionType.AMOUNT, 9));
		assertEquals(24710, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("flash-bang"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		assertEquals(24210, model.getNuyen());
		LifestyleValue life = new LifestyleValue(ShadowrunCore.getLifestyle("low"));
		life.setPrimary(true);
		life.setPaidMonths(3);
		charGen.getLifestyleController().addLifestyle(life);
		assertEquals(12000, charGen.getLifestyleController().getLifestyleCost(life));
		assertEquals(12210, model.getNuyen());

		CarriedItem contacts = ctrl.select(ShadowrunCore.getItem("contacts"), new SelectionOption(SelectionOptionType.RATING, 2));
		assertEquals(400, contacts.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(contacts, ShadowrunCore.getItem("sichtverbesserung"), new SelectionOption(SelectionOptionType.RATING, 2));
		assertEquals(1400, contacts.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.select(ShadowrunCore.getItem("kompositknochenKunststoff"));
		assertEquals(2810, model.getNuyen());

		// Now start tuning mode
		charGen.startTuningMode();
		skills = charGen.getSkillController();



		// S.97 more Qualities
		assertEquals(6, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("quick_healer"))!=null);
		assertEquals(3, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("will_to_live"))!=null);
		assertEquals(0, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("bad_rep"))!=null);
		assertEquals(7, model.getKarmaFree());
		QualityValue qVal = charGen.getQualityController().select(ShadowrunCore.getQuality("gremlins"));
		assertTrue(qVal!=null);
		assertTrue("Failed increasing "+qVal,charGen.getQualityController().increase(qVal));
		assertEquals(15, model.getKarmaFree());


//		assertEquals(16, model.getKarmaFree());

//		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("combat_axe"));
//		model.addItem(item);
//		item = new CarriedItem(ShadowrunCore.getItem("ares_predator_v"));
//		model.addItem(item);
//
//
//		ItemTemplate tmpImaging = ShadowrunCore.getItem("imaging_scope");
//		ItemTemplate tmpSilencer= ShadowrunCore.getItem("ares_lf_silencer");
//		ItemTemplate tmpLowLight= ShadowrunCore.getItem("low_light_vision");
//
//		CarriedItem aresLF75 = ctrl.select(ShadowrunCore.getItem("ares_light_fire_75"));
//
//		System.out.println("Dump Ares Light Fire 75");
//		System.out.println("Weapon: "+aresLF75.getName());
//		for (AvailableSlot slot : aresLF75.getSlots()) {
//			for (CarriedItem accessory : slot.getEmbeddedItems()) {
//				System.out.println("- "+accessory.getName()+" \t("+slot.getSlot().getName()+")");
//				// WiFi advantages
//				for (String wifi : accessory.getWiFiAdvantageStrings()) {
//					System.out.println("    WIFI: "+wifi);
//				}
//			}
//		}
//
//		// What is already included
//		Collection<CarriedItem> access = aresLF75.getAccessories();
//		assertNotNull(access);
//		boolean found = false;
//		for (CarriedItem val : access) {
//			if (val.getItem()==tmpSilencer)
//				found=true;
//		}
//		assertTrue("Included silencer not found", found);
//		// INTERNAL and BARREL and UNDER are not available
//		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.INTERNAL).toString(), 5, ctrl.getEmbeddableIn(aresLF75, ItemHook.INTERNAL).size());
////		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.BARREL).toString(), 7, ctrl.getEmbeddableIn(aresLF75, ItemHook.BARREL).size());
//		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.UNDER).toString(), 0, ctrl.getEmbeddableIn(aresLF75, ItemHook.UNDER).size());
//		// There should be options for TOP slot
//		List<ItemTemplate> opt = ctrl.getEmbeddableIn(aresLF75, ItemHook.TOP);
//		assertFalse(opt.isEmpty());
//		assertTrue(opt.contains(tmpImaging));
//
//		// Add imaging
//		assertTrue(ctrl.canBeEmbedded(aresLF75, tmpImaging, ItemHook.TOP));
//		CarriedItem imaging = ctrl.embed(aresLF75, tmpImaging);
//		System.out.println("Embedding = "+imaging);
//		found = false;
//		for (CarriedItem val : aresLF75.getAccessories()) {
//			if (val.getItem()==tmpImaging)
//				found=true;
//		}
//		assertTrue("Embedded imaging scope not found", found);

		// S.99 remaining karma
		charGen.setKarmaOptimization(false);
		assertEquals(15, model.getKarmaFree());
		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("perception")))); // Perception at 2 for 4 Karma
//		assertEquals(11, model.getKarmaFree());
		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("heavyweapons")))); // Heavy weapons at 2 for 4 Karma
//		assertEquals(7, model.getKarmaFree());
		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("first_aid")))); // First aid at 3 for 6 Karma
		assertEquals(1, model.getKarmaFree());


		// S.99 Connections
		Connection schieber = charGen.getConnectionController().createConnection();
		schieber.setName("Schieber");
		charGen.getConnectionController().increaseInfluence(schieber);
		charGen.getConnectionController().increaseLoyalty(schieber);
		Connection doc = charGen.getConnectionController().createConnection();
		doc.setName("Straßendoc");
		charGen.getConnectionController().increaseInfluence(doc);
		charGen.getConnectionController().increaseInfluence(doc);
		charGen.getConnectionController().increaseLoyalty(doc);
		assertEquals(0, charGen.getConnectionController().getToDos().size());

		model.setRealName("Thyson Hughes");
		model.setName("The Hammer");
		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(230);
		model.setWeight(160);

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateKyra() {
		System.out.println("----------------------generateKyra---------------------");
		assertEquals(25, model.getKarmaFree());

		// S.66 Qualities
		QualityValue qVal2 = charGen.getQualityController().select(ShadowrunCore.getQuality("mentor_spirit"));
		qVal2.setDescription("Meer");
		assertEquals(20, model.getKarmaFree());
		QualityValue qVal = charGen.getQualityController().select(ShadowrunCore.getQuality("focused_concentration"));
		assertEquals(16, model.getKarmaFree());
		assertTrue(qVal!=null);
		assertTrue(charGen.getQualityController().increase(qVal));
		assertEquals(12, model.getKarmaFree());

		// S.68 Metatype
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		assertEquals(12, model.getKarmaFree());
		charGen.getMetatypeController().select(
				ShadowrunCore.getMetaType("elf"));
		assertEquals(12, model.getKarmaFree());

		// S.68 Attributes
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(12, model.getKarmaFree());
		// Body +2
		for (int i=0; i<2; i++)
			assertTrue(attrib.increase(Attribute.BODY));
		// Agility +4
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.AGILITY));
		// Reaction +2
		for (int i=0; i<2; i++)
			assertTrue(attrib.increase(Attribute.REACTION));
		// Strength +1
		for (int i=0; i<1; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.STRENGTH));
		// Willpower +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.WILLPOWER));
		// Logic +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.LOGIC));
		// Intuition +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.INTUITION));
		// Charisma +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.CHARISMA));

		assertEquals(3, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.REACTION).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.LOGIC).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.CHARISMA).getModifiedValue());
		assertEquals(12, model.getKarmaFree());

		// S.71 Magic and resonance
		charGen.setPriority(PriorityType.MAGIC, Priority.A);
		MagicOrResonanceType mType = ShadowrunCore.getMagicOrResonanceType("mysticadept");
		for (MagicOrResonanceOption opt : charGen.getMagicOrResonanceController().getAvailable()) {
			if (opt.getType()==mType) {
				charGen.getMagicOrResonanceController().select(opt);
			}
		}
		assertNotNull(model.getMagicOrResonanceType());
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("influence"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("stunball"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("lightningbolt"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("blast"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("mind_probe"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("heal"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("clairaudience"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("clout"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("imprinvisibility"),false));
		assertNotNull(charGen.getSpellController().select(ShadowrunCore.getSpell("analyze_truth"),false));
		assertEquals(0, charGen.getSpellController().getSpellsLeft());

		assertTrue(charGen.getPowerController().increasePowerPoints()); // 1 PP
		assertTrue(charGen.getPowerController().increasePowerPoints()); // 2 PP
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("enhanced_accuracy"), ShadowrunCore.getSkill("automatics")));
		assertEquals(1.75, charGen.getPowerController().getPowerPointsLeft(), 0);
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("improved_potential"), Attribute.LIMIT_SOCIAL));
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("danger_sense")));
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("voice_control")));
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("improved_ability"), ShadowrunCore.getSkill("pistols")));
		assertEquals(0, charGen.getPowerController().getPowerPointsLeft(), 0);

		// S.76 Skills
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		SkillController skills = charGen.getSkillController();
		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("CONJURING"));
		assertNotNull("Could not select skillgroup", sgVal);
		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("pilot_ground_craft"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("computer"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("etiquette"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("pistols"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("locksmith"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("automatics"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("con"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("negotiating"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("unarmed"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(14,skills.getPointsLeftInKnowledgeAndLanguage());

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Dealer für Straßendrogen");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Identifizieren von Straßengangs");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Organisiertes Verbrechen");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Polizeitaktiken");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Schieber");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Straßengangpolitik");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("native_language"), "Englisch");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Sperethiel");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		// S.97 more Qualities
		assertEquals(2, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("addiction_mild"))!=null);
		assertEquals(6, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("distinctive_style"))!=null);
		assertEquals(11, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("code_of_honor"))!=null);
		assertEquals(26, model.getKarmaFree());

		/*
		 * S. 81 Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		assertEquals(6000, model.getNuyen());
		assertEquals(26, model.getKarmaFree());
		for (int i=0; i<10; i++)
			ctrl.increaseBoughtNuyen();
		assertEquals(26000, model.getNuyen());
		assertEquals(16, model.getKarmaFree());
		// Shopping
		ctrl.select(ShadowrunCore.getItem("browning_ultra_power"));
		LicenseValue license1 = charGen.getSINController().createNewLicense("Magieausübung", SIN.Quality.GOOD_MATCH);
		assertEquals(24760, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("uzi_iv"));
		ctrl.select(ShadowrunCore.getItem("steyr_tmp"));
		assertEquals(23960, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		ctrl.select(ShadowrunCore.getItem("regular_ammo"), new SelectionOption(SelectionOptionType.AMOUNT, 20));
		assertEquals(22560, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("renraku_sensei"));
		ctrl.select(ShadowrunCore.getItem("autmatischerDietrich"), new SelectionOption(SelectionOptionType.RATING, 3));
		assertEquals(20060, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("yamahaGrowler"));
		LicenseValue license2 = charGen.getSINController().createNewLicense("Waffenschein", SIN.Quality.GOOD_MATCH);
		assertEquals(14460, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("bliss"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		ctrl.select(ShadowrunCore.getItem("mini-schweissgeraet"));
		assertEquals(14135, model.getNuyen());

		SingleLifestyleController lsCtrl = new SingleLifestyleGenerator(model);
		lsCtrl.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		LifestyleValue ls1 = lsCtrl.getResult();
		ls1.setPaidMonths(2);
		assertEquals(2000, ls1.getCostPerMonth());
		charGen.getLifestyleController().addLifestyle(ls1);
		ctrl.select(ShadowrunCore.getItem("gel_rounds"), new SelectionOption(SelectionOptionType.AMOUNT, 7));
		assertEquals(9960, model.getNuyen());

		assertEquals(16, model.getKarmaFree());
		SIN sin = charGen.getSINController().createNewSIN("Fake 1", Quality.GOOD_MATCH);
		ctrl.select(ShadowrunCore.getItem("credstick_silver"));
		assertEquals(2440, model.getNuyen());
		assertEquals(16, model.getKarmaFree());

		LicenseValue license3 = charGen.getSINController().createNewLicense("Führerschein", SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
		assertEquals(1640, model.getNuyen());

		license1.setSIN(sin.getUniqueId());
		license2.setSIN(sin.getUniqueId());
		license3.setSIN(sin.getUniqueId());
		ls1.setSIN(sin.getUniqueId());
		assertEquals(16, model.getKarmaFree());

		/*
		 * Tuning mode
		 */
		charGen.startTuningMode();
		skills = charGen.getSkillController();

		assertEquals(16, model.getKarmaFree());

		sVal = model.getSkillValue(ShadowrunCore.getSkill("etiquette"));
		assertNotNull("Could not select skill", sVal);
		assertEquals(16, model.getKarmaFree());
		assertTrue(skills.increase(sVal)); // Increase to 3
//		assertEquals(102, model.getKarmaFree());
		assertEquals(10, model.getKarmaFree());

		// Spirits
		charGen.getSummonableController().select(ShadowrunCore.getSpirit("spirit_of_water"), 4);
		SummonableValue spirit = charGen.getSummonableController().select(ShadowrunCore.getSpirit("spirit_of_beasts"), 4);
		assertEquals(4, spirit.getServices());
		assertEquals(6, spirit.getRating());
		// Due to optimizations in Genesis, there is more karma left
		assertEquals(2, model.getKarmaFree());

		model.setRealName("?");
		model.setName("Silver");
		model.setGender(Gender.FEMALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(170);
		model.setWeight(60);

		// Finalize
		charGen.stop();

		try {
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateJames() {
		assertEquals(25, model.getKarmaFree());

		// S.66 Qualities
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("natural_hardening"))!=null);
		assertEquals(15, model.getKarmaFree());

		// S.68 Metatype
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.getMetatypeController().select(
				ShadowrunCore.getMetaType("human"));
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(3, attrib.getPointsLeftSpecial());
		assertTrue(attrib.increase(Attribute.EDGE));

		// S.68 Attributes
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.C);
		// Body +2
		for (int i=0; i<2; i++)
			assertTrue(attrib.increase(Attribute.BODY));
		// Agility +1
		for (int i=0; i<1; i++)
			assertTrue(attrib.increase(Attribute.AGILITY));
		// Reaction +1
		for (int i=0; i<1; i++)
			assertTrue(attrib.increase(Attribute.REACTION));
		// Strength +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.STRENGTH));
		// Willpower +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.WILLPOWER));
		// Logic +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.LOGIC));
		// Intuition +3
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.INTUITION));
		// Charisma +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.CHARISMA));

		assertEquals(3, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.REACTION).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.LOGIC).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.CHARISMA).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.EDGE).getModifiedValue());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(2, attrib.getPointsLeftSpecial());

		// S.71 Magic and resonance
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		MagicOrResonanceType mType = ShadowrunCore.getMagicOrResonanceType("technomancer");
		for (MagicOrResonanceOption opt : charGen.getMagicOrResonanceController().getAvailable()) {
			if (opt.getType()==mType) {
				charGen.getMagicOrResonanceController().select(opt);
				List<Modification> applyLate = ((PriorityMagicOrResonanceController)charGen.getMagicOrResonanceController()).getToApplyFromSelection();
				System.out.println("Apply late "+applyLate);
				charGen.apply("Reso", applyLate);
				// Should have selected the first two options
				SkillValue sVal = model.getSkillValue(ShadowrunCore.getSkill("compiling"));
				assertNotNull("Should have selected skill 'compiling'", sVal);
				assertEquals(4, sVal.getModifiedValue());
				sVal = model.getSkillValue(ShadowrunCore.getSkill("decompiling"));
				assertNotNull("Should have selected skill 'decompiling'", sVal);
				assertEquals(4, sVal.getModifiedValue());
			}
		}
		assertNotNull(model.getMagicOrResonanceType());
		assertEquals(2, attrib.getPointsLeftSpecial());
		assertTrue(attrib.increase(Attribute.RESONANCE));
		assertTrue(attrib.increase(Attribute.RESONANCE));
		assertEquals(0, attrib.getPointsLeftSpecial());

		ComplexFormController cforms = charGen.getComplexFormController();
		assertEquals(2, cforms.getComplexFormsLeft());
		assertNotNull(cforms.select(ShadowrunCore.getComplexForm("cleaner")));
		assertNotNull(cforms.select(ShadowrunCore.getComplexForm("editor")));
		assertEquals(0, cforms.getComplexFormsLeft());

		// S.76 Skills
		charGen.setPriority(PriorityType.SKILLS, Priority.E);
		SkillController skills = charGen.getSkillController();

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("computer"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("electronic_warfare"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("forgery"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("hacking"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("hardware"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("pistols"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("automatics"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(16, skills.getPointsLeftInKnowledgeAndLanguage());

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("interest"), "Betriebsysteme");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "BTL-Dealer");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Konzerne");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("interest"), "Matrixsicherheit");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Mr. Johnsons");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Neo-Anarchisten");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Ortsansässige Decker");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Ortsansässige Technomancer");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("street_knowledge"), "Schieber");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("native_language"), "Englisch");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Japanisch");
		assertNotNull("Could not select skill", sVal);

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());


		/*
		 * S. 80 Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		assertEquals(450000, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("ares_crusader_ii"));
		ctrl.select(ShadowrunCore.getItem("aresRoadmaster"));
		assertEquals(397170, model.getNuyen());
		CarriedItem diet = ctrl.select(ShadowrunCore.getItem("autmatischerDietrich"), new SelectionOption(SelectionOptionType.RATING,6));
		assertEquals(3000, diet.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		CarriedItem jammer1 = ctrl.select(ShadowrunCore.getItem("jammer_area"), new SelectionOption(SelectionOptionType.RATING,4));
		assertEquals(800, jammer1.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, null));
		assertEquals(393370, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("ceska_black_scorpion"));
		ctrl.select(ShadowrunCore.getItem("chameleon_suit"));
		assertEquals(391400, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("defiance_ex_shocker"));
		assertEquals(391150, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("docwagon-vertrag,PlatinProJahr"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
		assertEquals(241150, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("dreamchip"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		ctrl.select(ShadowrunCore.getItem("spare_clip"), new SelectionOption(SelectionOptionType.AMOUNT, 6));
		assertEquals(241020, model.getNuyen());
		// Gefälschte Lizenzen und Waffenscheine
		for (int i=1; i<=10; i++) {
			SIN sin = charGen.getSINController().createNewSIN("Fake Name "+i, SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
			LicenseValue license = charGen.getSINController().createNewLicense("Waffenschein", SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
			license.setSIN(sin.getUniqueId());
		}
		assertEquals(133020, model.getNuyen());
		CarriedItem commlink = ctrl.select(ShadowrunCore.getItem("transys_avalon"));
		assertEquals(128020, model.getNuyen());
		assertEquals(128020, model.getNuyen());
		ctrl.embed(commlink, ShadowrunCore.getItem("sim_module"));
		assertEquals(5100, commlink.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(127920, model.getNuyen());
		CarriedItem magnet = ctrl.select(ShadowrunCore.getItem("magnetkarten-kopierer"));
		ctrl.changeRating(magnet, 6);
		assertEquals(124320, model.getNuyen());
		CarriedItem magschloss = ctrl.select(ShadowrunCore.getItem("magschlossknacker"));
		ctrl.changeRating(magschloss, 4);
		assertEquals(116320, model.getNuyen());
		CarriedItem medkit = ctrl.select(ShadowrunCore.getItem("medkit"), new SelectionOption(SelectionOptionType.AMOUNT, 2), new SelectionOption(SelectionOptionType.RATING, 6));
		ctrl.changeRating(medkit, 6);
		assertEquals(113320, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("medkit-nachfuellpack"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		ctrl.select(ShadowrunCore.getItem("moodchip"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		assertEquals(112570, model.getNuyen());

		CarriedItem ohr = ctrl.select(ShadowrunCore.getItem("ohrstoepsel"), new SelectionOption(SelectionOptionType.RATING, 3));
		assertEquals(150, ohr.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(ohr, ShadowrunCore.getItem("audioverbesserung_acc"), new SelectionOption(SelectionOptionType.RATING, 2));
		assertEquals(1150, ohr.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(ohr, ShadowrunCore.getItem("selektivergeraeuschfilter_acc"));
		assertEquals(1400, ohr.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		CarriedItem armorJacket = ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		ctrl.embed(armorJacket, ShadowrunCore.getItem("fire_resistance"), new SelectionOption(SelectionOptionType.RATING, 4));
		assertEquals(2000, armorJacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(109170, model.getNuyen());

		CarriedItem cloth = ctrl.select(ShadowrunCore.getItem("armor_clothing"));
		assertEquals(450, cloth.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		CarriedItem silence = ctrl.select(ShadowrunCore.getItem("silencer"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
		assertEquals(1500, silence.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(107220, model.getNuyen());

		CarriedItem  handsensor = ctrl.select(ShadowrunCore.getItem("tragbaresGehaeuse"), new SelectionOption(SelectionOptionType.RATING, 2));
		assertEquals(200, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		CarriedItem sensor1 = ctrl.embed(handsensor, ShadowrunCore.getItem("singlesensor"), new SelectionOption(SelectionOptionType.RATING, 3));
		assertEquals(300, sensor1.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(500, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(handsensor, ShadowrunCore.getItem("singlesensor"), new SelectionOption(SelectionOptionType.RATING, 3));
		assertEquals(800, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		model.addItem(ctrl.select(ShadowrunCore.getItem("hk_227")));
		assertEquals(105690, model.getNuyen());

		CarriedItem  flashbang = ctrl.select(ShadowrunCore.getItem("flash-bang"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		assertEquals(500, flashbang.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.select(ShadowrunCore.getItem("apds"), new SelectionOption(SelectionOptionType.AMOUNT, 30));
		assertEquals(101590, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("explosive_rounds"), new SelectionOption(SelectionOptionType.AMOUNT, 50));
		ctrl.select(ShadowrunCore.getItem("stick_n_shock"), new SelectionOption(SelectionOptionType.AMOUNT, 8));
		assertEquals(96950, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("regular_ammo"), new SelectionOption(SelectionOptionType.AMOUNT, 100));
		ctrl.select(ShadowrunCore.getItem("taser_dart"), new SelectionOption(SelectionOptionType.AMOUNT, 10));
		assertEquals(94450, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("sequencer"), new SelectionOption(SelectionOptionType.RATING, 4));
		assertEquals(93450, model.getNuyen());
		CarriedItem  sichtg = ctrl.select(ShadowrunCore.getItem("goggles"), new SelectionOption(SelectionOptionType.RATING, 6));
		assertEquals(93150, model.getNuyen());
		ctrl.embed(sichtg, ShadowrunCore.getItem("bildverbindung"));
		assertEquals(93125, model.getNuyen());
		ctrl.embed(sichtg, ShadowrunCore.getItem("blitzkompensation"));
		assertEquals(92875, model.getNuyen());
		ctrl.embed(sichtg, ShadowrunCore.getItem("low_light_vision"));
		assertEquals(92375, model.getNuyen());
		ctrl.embed(sichtg, ShadowrunCore.getItem("smartlink"));
		assertEquals(90375, model.getNuyen());
		assertEquals(3075, sichtg.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		ctrl.select(ShadowrunCore.getItem("concealable_holster"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		ctrl.select(ShadowrunCore.getItem("trodes"));
		assertEquals(90005, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("bug_scanner"), new SelectionOption(SelectionOptionType.RATING, 6));
		ctrl.select(ShadowrunCore.getItem("white_noise_generator"), new SelectionOption(SelectionOptionType.RATING, 6));
		assertEquals(89105, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("jammer_direct"), new SelectionOption(SelectionOptionType.RATING, 6));
		assertEquals(87905, model.getNuyen());
		// Unterschicht Lebensstil


		/*
		 * Lifestyles
		 */
		SingleLifestyleController lsCtrl = new SingleLifestyleGenerator(model);
		lsCtrl.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsCtrl.selectName("Versteck");
		LifestyleValue ls1 = lsCtrl.getResult();
		ls1.setPaidMonths(3);
		charGen.getLifestyleController().addLifestyle(ls1);
		assertEquals(81905, model.getNuyen());

		lsCtrl = new SingleLifestyleGenerator(model);
		lsCtrl.selectLifestyle(ShadowrunCore.getLifestyle("middle"));
		lsCtrl.selectName("Wohnung");
		LifestyleValue ls2 = lsCtrl.getResult();
		ls2.setPaidMonths(12);
		charGen.getLifestyleController().addLifestyle(ls2);
		assertEquals(21905, model.getNuyen());




		// S.97 more Qualities
		assertEquals(15, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("analytical_mind"))!=null);
		assertEquals(10, model.getKarmaFree());
		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("resistance_to1"))!=null);
		assertEquals( 6, model.getKarmaFree());
		QualityValue addic = charGen.getQualityController().select(ShadowrunCore.getQuality("addiction_moderate"));
		assertTrue(addic!=null);
		addic.setDescription("BTLs");
		QualityValue depen = charGen.getQualityController().select(ShadowrunCore.getQuality("dependents_regular"));
		assertTrue(depen!=null);
		depen.setDescription("Lebt mit Freundin");
		QualityValue prej = charGen.getQualityController().select(ShadowrunCore.getQuality("prejudiced_common_biased"));
		assertTrue(prej!=null);
		prej.setDescription("Konzernlohnsklaven");

		assertEquals(26, model.getKarmaFree());

		charGen.startTuningMode();
		skills = charGen.getSkillController();
		cforms = charGen.getComplexFormController();

		/*
		 * S.99 remaining karma
		 */
		assertEquals(26, model.getKarmaFree());
		sVal = skills.select(ShadowrunCore.getSkill("software"));
		assertNotNull("Could not select skill", sVal);
		assertEquals(24, model.getKarmaFree());
		assertTrue(skills.increase(sVal));
		assertEquals(20, model.getKarmaFree());
		sVal = skills.select(ShadowrunCore.getSkill("cybercombat"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertEquals(14, model.getKarmaFree());

		assertTrue( skills.increase(model.getSkillValue(ShadowrunCore.getSkill("electronic_warfare"))) );
		assertEquals(10, model.getKarmaFree());

		// Fine tune complex forms
		assertNotNull(cforms.select(ShadowrunCore.getComplexForm("resonance_spike")));
		assertEquals(6, model.getKarmaFree());

		// TODO: Sprites
//		assertEquals(0, model.getKarmaFree());

		// Connections
		ConnectionsController conns = charGen.getConnectionController();
		Connection hehler = conns.createConnection();
		assertNotNull("Connection not created", hehler);
		hehler.setName("Hehler");
		Connection leutnant = conns.createConnection();
		leutnant.setName("Leutnant der Neo-Anarchisten");
		conns.increaseInfluence(leutnant);
		conns.increaseLoyalty(leutnant);
		Connection schieber = conns.createConnection();
		schieber.setName("Schieber");
		conns.increaseInfluence(schieber);
		assertEquals(0, conns.getPointsLeft());

		// TODO: Letzte Berechnungen S.102
		model.setRealName("Andrew Mason");
		model.setName("Mega Pulse");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(170);
		model.setWeight(60);

		// Finalize
		charGen.stop();
		assertTrue(model.getNuyen()>=5400);
		assertTrue(model.getNuyen()<=7400);

		try {
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateAdept() {
		assertEquals(25, model.getKarmaFree());


		// S.66 Qualities
		charGen.getQualityController().select(ShadowrunCore.getQuality("natural_athlete"));
		charGen.getQualityController().select(ShadowrunCore.getQuality("guts"));
		assertEquals(8, model.getKarmaFree());

		// S.68 Metatype
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.getMetatypeController().select(
				ShadowrunCore.getMetaType("elf"));

		// S.68 Attributes
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.E);
		AttributeController attrib = charGen.getAttributeController();
		// Body +1
		for (int i=0; i<1; i++)
			assertTrue(attrib.increase(Attribute.BODY));
		// Agility +4
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.AGILITY));
		// Reaction +2
		for (int i=0; i<2; i++)
			assertTrue(attrib.increase(Attribute.REACTION));
		// Strength +1
		for (int i=0; i<1; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.STRENGTH));
		// Willpower +1
		for (int i=0; i<1; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.WILLPOWER));
		// Logic +1
		for (int i=0; i<1; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.LOGIC));
		// Intuition +2
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.INTUITION));
		// Charisma +0
//		for (int i=0; i<3; i++)
//			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.CHARISMA));

		assertEquals(2, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.REACTION).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.LOGIC).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.CHARISMA).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.EDGE).getModifiedValue());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
//		assertTrue("Attribute not increased",attrib.increase(Attribute.EDGE));

		// S.71 Magic and resonance
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		assertEquals(0, attrib.getPointsLeftSpecial());
		MagicOrResonanceType mType = ShadowrunCore.getMagicOrResonanceType("adept");
		for (MagicOrResonanceOption opt : charGen.getMagicOrResonanceController().getAvailable()) {
			if (opt.getType()==mType) {
				System.out.println("########Select "+opt);
				charGen.getMagicOrResonanceController().select(opt);
//				((PriorityMagicOrResonanceGenerator)charGen.getMagicOrResonanceController()).getToApplyFromSelection();
				// Should have selected the first two options
//				SkillValue sVal = model.getSkillValue(ShadowrunCore.getSkill("compiling"));
//				assertNotNull("Should have selected skill 'compiling'", sVal);
//				assertEquals(4, sVal.getModifiedValue());
//				sVal = model.getSkillValue(ShadowrunCore.getSkill("decompiling"));
//				assertNotNull("Should have selected skill 'decompiling'", sVal);
//				assertEquals(4, sVal.getModifiedValue());
			}
		}
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertNotNull(model.getMagicOrResonanceType());
//		assertTrue(attrib.increase(Attribute.MAGIC));
//		assertTrue(attrib.increase(Attribute.MAGIC));
		assertEquals(0, attrib.getPointsLeftSpecial());

		AdeptPowerController powers = charGen.getPowerController();
		assertEquals(6.0f, powers.getPowerPointsLeft(), 0);
		AdeptPowerValue pow1 = powers.select(ShadowrunCore.getAdeptPower("attribute_boost"), Attribute.AGILITY);
		assertEquals(5.75f, powers.getPowerPointsLeft(), 0);
		assertTrue(powers.canBeIncreased(pow1));  assertTrue(powers.increase(pow1));
		assertTrue(powers.canBeIncreased(pow1));  assertTrue(powers.increase(pow1));
		assertTrue(powers.canBeIncreased(pow1));  assertTrue(powers.increase(pow1));
		assertEquals(5.0f, powers.getPowerPointsLeft(), 0);
		powers.select(ShadowrunCore.getAdeptPower("enhanced_accuracy"), ShadowrunCore.getSkill("pistols"));
//		pow2.setSelectedSkill(ShadowrunCore.getSkill("pistols"));
		assertEquals(4.75f, powers.getPowerPointsLeft(), 0);
		powers.select(ShadowrunCore.getAdeptPower("enhanced_accuracy"), ShadowrunCore.getSkill("automatics"));
//		pow3.setSelectedSkill(ShadowrunCore.getSkill("automatics"));
		assertEquals(4.5f, powers.getPowerPointsLeft(), 0);
		AdeptPowerValue pow4 = powers.select(ShadowrunCore.getAdeptPower("improved_reflexes"));
		assertTrue(powers.canBeIncreased(pow4));  assertTrue(powers.increase(pow4));
		assertEquals(2.0f, powers.getPowerPointsLeft(), 0);
		powers.select(ShadowrunCore.getAdeptPower("combat_sense"));
		powers.select(ShadowrunCore.getAdeptPower("spell_resistance"));
		AdeptPowerValue pow5 = powers.select(ShadowrunCore.getAdeptPower("mystic_armor"));
		assertTrue(powers.canBeIncreased(pow5));  assertTrue(powers.increase(pow5));
		assertEquals(0, powers.getPowerPointsLeft(), 0);

		// S.76 Skills
		charGen.setPriority(PriorityType.SKILLS, Priority.B);
		SkillController skills = charGen.getSkillController();
		assertEquals(5, skills.getPointsLeftSkillGroups());
		assertEquals(36, skills.getPointsLeftSkills());

		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("FIREARMS"));
		assertNotNull("Could not select skillgroup", sgVal);
		assertTrue(skills.increase(sgVal));
		assertTrue(skills.increase(sgVal));
		assertTrue(skills.increase(sgVal));
		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("gymnastics"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("pilot_ground_craft"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("first_aid"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("blades"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("running"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("sneaking"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		skills.select(sVal, ShadowrunCore.getSkill("sneaking").getSpecialization("terrain_urban"));

		sVal = skills.select(ShadowrunCore.getSkill("swimming"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("tracking"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("con"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(10, skills.getPointsLeftInKnowledgeAndLanguage());

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("interest"), "Ägyptische Keramik");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("interest"), "Waffenhersteller");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("native_language"), "Lakota");
		assertNotNull("Could not select skill", sVal);

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Englisch");
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal));
		assertTrue(skills.increase(sVal));
//		assertTrue(skills.increase(sVal));
//		assertTrue(skills.increase(sVal));

		sVal = skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Sperethiel");
		assertNotNull("Could not select skill", sVal);
//		assertTrue(skills.increase(sVal));
//		assertTrue(skills.increase(sVal));
//		assertTrue(skills.increase(sVal));

		assertEquals(0, skills.getPointsLeftSkillGroups());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());


//		/*
//		 * S. 80 Equipment
//		 */
//		EquipmentController ctrl = charGen.getEquipmentController();
//		assertEquals(450000, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("ares_crusader_ii"));
//		ctrl.select(ShadowrunCore.getItem("aresRoadmaster"));
//		assertEquals(397170, model.getNuyen());
//		CarriedItem diet = ctrl.select(ShadowrunCore.getItem("autmatischerDietrich"));
//		diet.setRating(6);
//		assertEquals(3000, diet.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		CarriedItem jammer1 = ctrl.select(ShadowrunCore.getItem("jammer_area"));
//		jammer1.setRating(4);
//		assertEquals(800, jammer1.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, null));
//		assertEquals(393370, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("ceska_black_scorpion"));
//		ctrl.select(ShadowrunCore.getItem("chameleon_suit"));
//		assertEquals(391400, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("defiance_ex_shocker"));
//		assertEquals(391150, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("docwagon-vertrag,PlatinProJahr"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
//		assertEquals(241150, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("dreamchip"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		ctrl.select(ShadowrunCore.getItem("spare_clip"), new SelectionOption(SelectionOptionType.AMOUNT, 6));
//		assertEquals(241020, model.getNuyen());
//		// Gefälschte Lizenzen und Waffenscheine
//		for (int i=1; i<=10; i++) {
//			SIN sin = charGen.getSINController().createNewSIN("Fake Name "+i, SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
//			LicenseValue license = charGen.getSINController().createNewLicense("Waffenschein", SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
//			license.setSIN(sin.getUniqueId());
//		}
//		assertEquals(133020, model.getNuyen());
//		CarriedItem commlink = ctrl.select(ShadowrunCore.getItem("transys_avalon"));
//		assertTrue(ctrl.canBeEmbedded(commlink, ShadowrunCore.getItem("sim_module")));
//		ctrl.embed(commlink, ShadowrunCore.getItem("sim_module"));
//		assertEquals(5100, commlink.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(127920, model.getNuyen());
//		CarriedItem magnet = ctrl.select(ShadowrunCore.getItem("magnetkarten-kopierer"));
//		ctrl.changeRating(magnet, 6);
//		assertEquals(124320, model.getNuyen());
//		CarriedItem magschloss = ctrl.select(ShadowrunCore.getItem("magschlossknacker"));
//		ctrl.changeRating(magschloss, 4);
//		assertEquals(116320, model.getNuyen());
//		CarriedItem medkit = ctrl.select(ShadowrunCore.getItem("medkit"), new SelectionOption(SelectionOptionType.AMOUNT, 2), new SelectionOption(SelectionOptionType.RATING, 6));
//		ctrl.changeRating(medkit, 6);
//		assertEquals(113320, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("medkit-nachfuellpack"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		ctrl.select(ShadowrunCore.getItem("moodchip"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		assertEquals(112570, model.getNuyen());
//
//		CarriedItem ohr = ctrl.select(ShadowrunCore.getItem("ohrstoepsel"), new SelectionOption(SelectionOptionType.RATING, 3));
//		assertEquals(150, ohr.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		ctrl.embed(ohr, ShadowrunCore.getItem("audioverbesserung_acc"), new SelectionOption(SelectionOptionType.RATING, 2));
//		ctrl.embed(ohr, ShadowrunCore.getItem("selektivergeraeuschfilter_acc"));
//		assertEquals(1400, ohr.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//
//		CarriedItem armorJacket = ctrl.select(ShadowrunCore.getItem("armor_jacket"));
//		ctrl.embed(armorJacket, ShadowrunCore.getItem("fire_resistance"), new SelectionOption(SelectionOptionType.RATING, 4));
//		assertEquals(2000, armorJacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(109170, model.getNuyen());
//
//		CarriedItem cloth = ctrl.select(ShadowrunCore.getItem("armor_clothing"));
//		assertEquals(450, cloth.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		CarriedItem silence = ctrl.select(ShadowrunCore.getItem("silencer"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
//		assertEquals(1500, silence.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(107220, model.getNuyen());
//
//		CarriedItem  handsensor = ctrl.select(ShadowrunCore.getItem("tragbaresGehaeuse"), new SelectionOption(SelectionOptionType.RATING, 2));
//		assertEquals(200, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		CarriedItem sensor1 = ctrl.embed(handsensor, ShadowrunCore.getItem("singlesensor"), new SelectionOption(SelectionOptionType.RATING, 3));
//		assertEquals(300, sensor1.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(500, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		ctrl.embed(handsensor, ShadowrunCore.getItem("singlesensor"), new SelectionOption(SelectionOptionType.RATING, 3));
//		assertEquals(800, handsensor.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		model.addItem(ctrl.select(ShadowrunCore.getItem("hk_227")));
//		assertEquals(105690, model.getNuyen());
//
//		CarriedItem  flashbang = ctrl.select(ShadowrunCore.getItem("flash-bang"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		assertEquals(500, flashbang.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		ctrl.select(ShadowrunCore.getItem("apds"), new SelectionOption(SelectionOptionType.AMOUNT, 30));
//		assertEquals(101590, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("explosive_rounds"), new SelectionOption(SelectionOptionType.AMOUNT, 50));
//		ctrl.select(ShadowrunCore.getItem("stick_n_shock"), new SelectionOption(SelectionOptionType.AMOUNT, 8));
//		assertEquals(96950, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("regular_ammo"), new SelectionOption(SelectionOptionType.AMOUNT, 100));
//		ctrl.select(ShadowrunCore.getItem("taser_dart"), new SelectionOption(SelectionOptionType.AMOUNT, 10));
//		assertEquals(94450, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("sequencer"), new SelectionOption(SelectionOptionType.RATING, 4));
//		assertEquals(93450, model.getNuyen());
//		CarriedItem  sichtg = ctrl.select(ShadowrunCore.getItem("goggles"), new SelectionOption(SelectionOptionType.RATING, 6));
//		assertEquals(93150, model.getNuyen());
//		ctrl.embed(sichtg, ShadowrunCore.getItem("bildverbindung_acc"));
//		assertEquals(93125, model.getNuyen());
//		ctrl.embed(sichtg, ShadowrunCore.getItem("blitzkompensation_acc"));
//		assertEquals(92875, model.getNuyen());
//		ctrl.embed(sichtg, ShadowrunCore.getItem("low_light_vision"));
//		ctrl.embed(sichtg, ShadowrunCore.getItem("smartlink"));
//		assertEquals(3075, sichtg.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		assertEquals(90375, model.getNuyen());
//
//		ctrl.select(ShadowrunCore.getItem("concealable_holster"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
//		ctrl.select(ShadowrunCore.getItem("trodes"));
//		assertEquals(90005, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("bug_scanner"), new SelectionOption(SelectionOptionType.RATING, 6));
//		ctrl.select(ShadowrunCore.getItem("white_noise_generator"), new SelectionOption(SelectionOptionType.RATING, 6));
//		assertEquals(89105, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("jammer_direct"), new SelectionOption(SelectionOptionType.RATING, 6));
//		assertEquals(87905, model.getNuyen());
//		// Unterschicht Lebensstil


		/*
		 * Lifestyles
		 */
		SingleLifestyleController lsCtrl = new SingleLifestyleGenerator(model);
		lsCtrl.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsCtrl.selectName("Versteck");
		LifestyleValue ls1 = lsCtrl.getResult();
		ls1.setPaidMonths(1);
		charGen.getLifestyleController().addLifestyle(ls1);
//		assertEquals(81905, model.getNuyen());




//		// S.97 more Qualities
//		assertEquals(15, model.getKarmaFree());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("analytical_mind"))!=null);
//		assertEquals(10, model.getKarmaFree());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("resistance_to1"))!=null);
//		assertEquals( 6, model.getKarmaFree());
//		QualityValue addic = charGen.getQualityController().select(ShadowrunCore.getQuality("addiction_moderate"));
//		assertTrue(addic!=null);
//		addic.setDescription("BTLs");
//		QualityValue depen = charGen.getQualityController().select(ShadowrunCore.getQuality("dependents_regular"));
//		assertTrue(depen!=null);
//		depen.setDescription("Lebt mit Freundin");
//		QualityValue prej = charGen.getQualityController().select(ShadowrunCore.getQuality("prejudiced_common_biased"));
//		assertTrue(prej!=null);
//		prej.setDescription("Konzernlohnsklaven");
//
//		assertEquals(26, model.getKarmaFree());

		charGen.startTuningMode();
		skills = charGen.getSkillController();
		powers = charGen.getPowerController();

//		/*
//		 * S.99 remaining karma
//		 */
//		sVal = skills.select(ShadowrunCore.getSkill("software"));
//		assertNotNull("Could not select skill", sVal);
//		assertTrue(skills.increase(sVal));
//		assertEquals(20, model.getKarmaFree());
//		sVal = skills.select(ShadowrunCore.getSkill("cybercombat"));
//		assertNotNull("Could not select skill", sVal);
//		assertTrue(skills.increase(sVal));
//		assertEquals(14, model.getKarmaFree());
//
//		assertTrue( skills.increase(model.getSkillValue(ShadowrunCore.getSkill("electronic_warfare"))) );
//		assertEquals(10, model.getKarmaFree());
//
//		// Fine tune complex forms
////		assertNotNull(cforms.select(ShadowrunCore.getComplexForm("resonance_spike")));
//		assertEquals(6, model.getKarmaFree());

		// TODO: Sprites
//		assertEquals(0, model.getKarmaFree());

		// Connections
		PriorityConnectionController conns = (PriorityConnectionController) charGen.getConnectionController();
		Connection hehler = conns.createConnection();
		hehler.setName("Schwarzmarkthändler");
		conns.increaseInfluence(hehler);
		conns.increaseLoyalty(hehler);
		conns.increaseInfluence(hehler);
		conns.increaseLoyalty(hehler);

		Connection schiess = conns.createConnection();
		schiess.setName("Schießstandbesitzer");
		conns.increaseInfluence(schiess);
		conns.increaseInfluence(schiess);
		conns.increaseLoyalty(schiess);
		assertEquals(0, conns.getPointsLeft());

		// TODO: Letzte Berechnungen S.102
		model.setRealName("Archetyp S.123");
		model.setName("Fernkampf Adept");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(170);
		model.setWeight(60);

		// Finalize
		charGen.stop();
		System.out.println("Nuyen = "+model.getNuyen());
		assertTrue(model.getNuyen()+"", model.getNuyen()>=5300);
		assertTrue(model.getNuyen()+"", model.getNuyen()<=7400);

		try {
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.SR5LetUserChooseListener#letUserChoose(java.lang.String, org.prelle.shadowrun5.modifications.ModificationChoice)
	 */
	@Override
	public Collection<Modification> letUserChoose(String choiceReason, ModificationChoice choice) {
		// TODO Auto-generated method stub
		System.out.println("Because of '"+choiceReason+"' choose "+choice);

		if (choice.getNumberOfChoices()>0) {
			System.out.println("Select "+choice.subList(0, choice.getNumberOfChoices()));
			return choice.subList(0, choice.getNumberOfChoices());
		}

		System.exit(0);
		return null;
	}

}
