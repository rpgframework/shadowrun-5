/**
 * 
 */
package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.PriorityAttributeGenerator;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationValueType;

/**
 * @author prelle
 *
 */
public class PrioAttributeGeneratorTest {

	private PriorityAttributeGenerator realCharGen;
	private AttributeController charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		realCharGen = new PriorityAttributeGenerator(new CommonSR5CharacterGenerator() {
			public void start(ShadowrunCharacter model) {}
			public boolean hasEnoughData() {return false;}
			public WizardPageType[] getWizardPages() {return null;}
			public String getName() {return null;}
			public String getPageI18NKey() {return null;}
			public String getHelpI18NKey() {return null;}
			public ShadowrunCharacter getCharacter() { return PrioAttributeGeneratorTest.this.model; }
			public String getId() { return null;}
			@Override
			public List<String> getToDos() {
				// TODO Auto-generated method stub
				return new ArrayList<String>();
			}

		});
		charGen = realCharGen;
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		System.out.println("-------------testIdle----------------");
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.A));
		assertEquals(24, charGen.getPointsLeft());
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.B));
		assertEquals(20, charGen.getPointsLeft());
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.C));
		assertEquals(16, charGen.getPointsLeft());
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.D));
		assertEquals(14, charGen.getPointsLeft());
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.E));
		assertEquals(12, charGen.getPointsLeft());
	}

	//-------------------------------------------------------------------
	@Test
	public void test1() {
		System.out.println("-------------test1----------------");
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.A));

		AttributeModification minMod = new AttributeModification(ModificationValueType.CURRENT, Attribute.AGILITY, 1);
		AttributeModification maxMod = new AttributeModification(ModificationValueType.MAX, Attribute.AGILITY, 6);
		assertEquals(0, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(24, charGen.getPointsLeft());
		realCharGen.apply(minMod);
		realCharGen.apply(maxMod);
		// Other attributes must also have unreached maximums, otherwise AGILITY could not be maxed
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.STRENGTH, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.WILLPOWER, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.LOGIC, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.INTUITION, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.CHARISMA, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.REACTION, 6));
		assertEquals(1, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(24, charGen.getPointsLeft());
		assertTrue(charGen.canBeIncreased(Attribute.AGILITY));
		assertTrue(charGen.increase(Attribute.AGILITY)); // 2
		assertTrue(charGen.increase(Attribute.AGILITY)); // 3
		assertTrue(charGen.increase(Attribute.AGILITY)); // 4
		assertTrue(charGen.increase(Attribute.AGILITY)); // 5
		assertTrue(charGen.increase(Attribute.AGILITY)); // 6
		assertEquals(""+model.getAttribute(Attribute.AGILITY).getModifiedValue(), 6, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(19, charGen.getPointsLeft());
		assertFalse(charGen.increase(Attribute.AGILITY));
		assertEquals(6, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(19, charGen.getPointsLeft());
	}

	//-------------------------------------------------------------------
	/**
	 * Raise an attribute to the max limit and decrease the maximum afterwards
	 */
	@Test
	public void testDecreaseMax() {
		System.out.println("-------------testDecreaseMax----------------");
		realCharGen.handleGenerationEvent(new GenerationEvent(GenerationEventType.PRIORITY_CHANGED, PriorityType.ATTRIBUTE, Priority.A));

		// Other attributes must also have unreached maximums, otherwise AGILITY could not be maxed
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.STRENGTH, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.WILLPOWER, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.LOGIC, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.INTUITION, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.CHARISMA, 6));
		realCharGen.apply(new AttributeModification(ModificationValueType.MAX, Attribute.REACTION, 6));

		AttributeModification minMod = new AttributeModification(ModificationValueType.CURRENT, Attribute.AGILITY, 1);
		AttributeModification maxMod1 = new AttributeModification(ModificationValueType.MAX, Attribute.AGILITY, 4);
		AttributeModification maxMod2 = new AttributeModification(ModificationValueType.MAX, Attribute.AGILITY, 1);
		assertEquals(0, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(24, charGen.getPointsLeft());
		realCharGen.apply(minMod);
		realCharGen.apply(maxMod1);
		realCharGen.apply(maxMod2);
		// Raise to limit
		while (charGen.canBeIncreased(Attribute.AGILITY))
			assertTrue(charGen.increase(Attribute.AGILITY));
		assertEquals(5, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(20, charGen.getPointsLeft());
		
		// Now remove one limit
		realCharGen.undo(maxMod2);
		assertEquals(4, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(21, charGen.getPointsLeft());
	}

}
