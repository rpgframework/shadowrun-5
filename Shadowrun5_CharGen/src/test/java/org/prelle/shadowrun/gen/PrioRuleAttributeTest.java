/**
 * 
 */
package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.Connection;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.modifications.SpecialRuleModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioRuleAttributeTest {

	private NewPriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new NewPriorityCharacterGenerator();
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	@Test
	public void testBoughtNuyen() {
		System.out.println("-------------testBoughtNuyen----------------");
		
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		EquipmentController ctrl = charGen.getEquipmentController();
		
		assertEquals(   25, model.getKarmaFree());
		assertEquals( 6000, model.getNuyen());
		assertEquals(    0, ctrl.getBoughtNuyen());
		
		// Convert 10 karma
		for (int i=0; i<10; i++) {
			assertTrue("Cannot raise invested karma above "+i, ctrl.canIncreaseBoughtNuyen());
			ctrl.increaseBoughtNuyen();
			assertEquals( i+1, ctrl.getBoughtNuyen());
		}
		assertFalse("Should not be able to increase invested karma above 10 ", ctrl.canIncreaseBoughtNuyen());
		
		// Add "Born rich"
		Modification bornRich = new SpecialRuleModification(SpecialRuleModification.Rule.KARMA_TO_NUYEN, 10);
		charGen.addUnitTestModification(bornRich);
		assertTrue("Should now be able to increase invested karma above 10 ", ctrl.canIncreaseBoughtNuyen());
		// Convert 10 additional karma
		for (int i=0; i<10; i++) {
			assertTrue("Cannot raise invested karma above "+(10+i), ctrl.canIncreaseBoughtNuyen());
			ctrl.increaseBoughtNuyen();
			assertEquals( (10+i)+1, ctrl.getBoughtNuyen());
		}
		assertFalse("Should not be able to increase invested karma above 20 ", ctrl.canIncreaseBoughtNuyen());
		int oldToDoSize = ctrl.getToDos().size();
		
		// Remove "Born rich" again
		charGen.removeUnitTestModification(bornRich);
		assertEquals(oldToDoSize+1, ctrl.getToDos().size(), 0);
		
		// Re-Convert 10 additional karma
		for (int i=20; i>10; i--) {
			assertTrue("Cannot lower invested karma below "+i, ctrl.canDecreaseBoughtNuyen());
			ctrl.decreaseBoughtNuyen();
			assertEquals( i-1, ctrl.getBoughtNuyen());
		}
		// ToDo should be gone
		assertEquals(oldToDoSize, ctrl.getToDos().size(), 0);
	}

	//-------------------------------------------------------------------
	@Test
	public void testAcademicKnowledgeHalfPrice() {
		System.out.println("-------------testAcademicKnowledgeHalfPrice----------------");
		
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("human"));
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.getAttributeController().increase(Attribute.LOGIC);
		charGen.getAttributeController().increase(Attribute.LOGIC);
		charGen.getAttributeController().increase(Attribute.LOGIC);
		SkillController ctrl = charGen.getSkillController();
		assertEquals(   25, model.getKarmaFree());
		assertEquals(10, ctrl.getPointsLeftInKnowledgeAndLanguage());
		
		SkillValue academic = ctrl.select(ShadowrunCore.getSkill("academic_knowledge"));
		assertNotNull(academic);
		SkillValue interest = ctrl.select(ShadowrunCore.getSkill("interest"));
		assertNotNull(interest);
		assertEquals(8, ctrl.getPointsLeftInKnowledgeAndLanguage());
		// Increase academic knowledge to 6
		ctrl.increase(academic);
		ctrl.increase(academic);
		ctrl.increase(academic);
		ctrl.increase(academic);
		ctrl.increase(academic);
		assertEquals(3, ctrl.getPointsLeftInKnowledgeAndLanguage());
		// Ensure not raisable above 6
		assertFalse(ctrl.canBeIncreased(academic));
		assertFalse(ctrl.increase(academic));
		
		// Decrease again to 5
		assertTrue(ctrl.canBeDecreased(academic));
		assertTrue(ctrl.decrease(academic));
		assertEquals(4, ctrl.getPointsLeftInKnowledgeAndLanguage());
		
		// Grant modification "College education"
		Modification college = new SpecialRuleModification(SpecialRuleModification.Rule.SKILL_ACADEMIC_COST_REDUCED,0);
		charGen.addUnitTestModification(college);
		assertEquals("Half price for academic cost not applied",6, ctrl.getPointsLeftInKnowledgeAndLanguage());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFriendsInHighPlaces() {
		System.out.println("-------------testFriendsInHighPlaces----------------");
		
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("human"));
		charGen.getAttributeController().increase(Attribute.CHARISMA);
		charGen.getAttributeController().increase(Attribute.CHARISMA); // CH 3
		charGen.getAttributeController().increase(Attribute.CHARISMA); // CH 4
		
		ConnectionsController ctrl = charGen.getConnectionController();
		assertEquals(12, ctrl.getPointsLeft());
		
		Connection lowCon  = ctrl.createConnection();  lowCon.setName("low");
		Connection highCon = ctrl.createConnection(); highCon.setName("high");
		assertEquals(8, ctrl.getPointsLeft());

		// Add a connection
		for (int i=1; i<=5; i++) {
			assertTrue("Cannot increase influence to "+(highCon.getInfluence()+1),ctrl.canIncreaseInfluence(highCon));
			assertTrue(ctrl.increaseInfluence(highCon));
		}
		assertEquals(2, lowCon.getInfluence()+lowCon.getLoyalty());
		assertEquals(7, highCon.getInfluence()+highCon.getLoyalty());
		assertEquals(3, ctrl.getPointsLeft());
		
		// Grant modification "College education"
		Modification friends = new SpecialRuleModification(SpecialRuleModification.Rule.FRIENDS_IN_HIGH_PLACES,0);
		charGen.addUnitTestModification(friends); // Add 16 points
		assertEquals(19, ctrl.getPointsLeft());
		
		// Add a connection
		assertEquals(6, highCon.getInfluence());
		for (int i=0; i<2; i++) {
			assertTrue("Cannot increase influence to "+(highCon.getInfluence()+1),ctrl.canIncreaseInfluence(highCon));
			assertTrue("Failed to increase influence to "+(highCon.getInfluence()+1), ctrl.increaseInfluence(highCon));
		}
		ctrl.increaseLoyalty(highCon);
		ctrl.increaseLoyalty(highCon);
		assertEquals(15, ctrl.getPointsLeft());
		
		int oldToDoSize = ctrl.getToDos().size();
		
		// Remove modification
		charGen.removeUnitTestModification(friends);
		assertEquals(3, ctrl.getPointsLeft());
		assertEquals(25, model.getKarmaFree());
		// Additional ToDo should be present
		assertEquals(oldToDoSize, ctrl.getToDos().size());
	}

}
