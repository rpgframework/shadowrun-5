/**
 * 
 */
package org.prelle.shadowrun.gen;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.gen.PriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;

/**
 * @author prelle
 *
 */
public class PriorityGeneratorTest {

	private PriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}
	
	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new PriorityCharacterGenerator();
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		System.out.println("-------------testIdle----------------");
		assertEquals(Priority.A, charGen.getPriority(PriorityType.METATYPE));
		assertEquals(Priority.B, charGen.getPriority(PriorityType.ATTRIBUTE));
		assertEquals(Priority.C, charGen.getPriority(PriorityType.MAGIC));
		assertEquals(Priority.D, charGen.getPriority(PriorityType.SKILLS));
		assertEquals(Priority.E, charGen.getPriority(PriorityType.RESOURCES));
		assertEquals(20, charGen.getAttributeController().getPointsLeft());
	}

	//-------------------------------------------------------------------
	@Test
	public void testPriority() {
		System.out.println("-------------testPriority----------------");
		
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		assertEquals(Priority.B, charGen.getPriority(PriorityType.METATYPE));
		assertEquals(Priority.A, charGen.getPriority(PriorityType.ATTRIBUTE));
	}

	//-------------------------------------------------------------------
	@Test
	public void testMetatype() {
		System.out.println("-------------testMetatype----------------");
		
		MetaType meta = ShadowrunCore.getMetaType("elf");
		// Metatypes = B, Attributes = A
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		charGen.getMetatypeController().select(meta);
		
		// Verify attribute ranges
		assertEquals(1, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.BODY).getMaximum());
		assertEquals(2, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(7, model.getAttribute(Attribute.AGILITY).getMaximum());
	}

}
