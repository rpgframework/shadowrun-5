/**
 * 
 */
package org.prelle.shadowrun.gen.newprio;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.chargen.PrioritySkillController;
import org.prelle.shadowrun5.chargen.cost.PrioritySkillCostCalculator;

/**
 * @author prelle
 *
 */
public class NewPrioGenTest {

	private NewPriorityCharacterGenerator charGen;
	
	//-------------------------------------------------------------------
	static {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		charGen = new NewPriorityCharacterGenerator();
		charGen.start(new ShadowrunCharacter());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testTroll() {
		ShadowrunCharacter model = charGen.getCharacter();
		
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("troll"));
		assertNotNull(model.getMetatype());
		assertEquals("troll", model.getMetatype().getId());
		assertNotNull(model.getQuality("thermographic_vision"));
		
		// Change metatype
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("ork"));
		assertNotNull(model.getMetatype());
		assertEquals("ork", model.getMetatype().getId());
		try {
			System.out.println("Got = "+model.getQuality("thermographic_vision"));
			fail("Expected NoSuchElementExcetion accessing not existing quality");
		} catch (NoSuchElementException e) {
		}
		
	}

	//--------------------------------------------------------------------
	@Test
	public void testSkillGeneration() {
		charGen.setPriority(PriorityType.SKILLS, Priority.A);
		SkillController ctrl = charGen.getSkillController();
		
		SkillValue val = ctrl.select(ShadowrunCore.getSkill("automatics"));
		assertNotNull(val);
		assertEquals(1, val.getModifiedValue());
		ctrl.increase(val); // 2
		ctrl.increase(val); // 3
		ctrl.increase(val); // 4
		ctrl.increase(val); // 5
		assertTrue(ctrl.canBeIncreased(val));
		ctrl.increase(val); // 6
		assertEquals(6, val.getModifiedValue());
		assertFalse(ctrl.canBeIncreased(val));
		assertFalse(ctrl.increase(val));
		assertEquals(6, val.getModifiedValue());
	}

	//--------------------------------------------------------------------
	@Test
	public void testBreakGroup() {
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		PrioritySkillController ctrl = (PrioritySkillController) charGen.getSkillController();
		PrioritySkillCostCalculator cost = ctrl.getCostCalculator();
		ShadowrunCharacter model = charGen.getCharacter();
		
		SkillGroupValue val = new SkillGroupValue(ShadowrunCore.getSkillGroup("ATHLETICS"), 2); 
		model.addSkillGroup(val);
		charGen.runProcessors();
		assertEquals(28, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(25, model.getKarmaFree());
		
		assertFalse("Should not be able to break skill group",ctrl.canBreakSkillGroup(val));
		
		// Start tuning mode
		charGen.startTuningMode();
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(25, model.getKarmaFree());
		
		assertTrue("Should be able to break skill group",ctrl.canBreakSkillGroup(val));
		ctrl.breakSkillGroup(val);
		
		assertFalse(model.getSkillGroupValues().contains(val));
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(25, model.getKarmaFree());

		SkillValue gym = model.getSkillValue(ShadowrunCore.getSkill("gymnastics"));
		SkillValue run = model.getSkillValue(ShadowrunCore.getSkill("running"));
		SkillValue swim= model.getSkillValue(ShadowrunCore.getSkill("swimming"));
		assertNotNull(gym );
		assertNotNull(run );
		assertNotNull(swim);
		
		assertTrue(ctrl.canBeIncreased(gym));
		assertTrue(ctrl.increase(gym));
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(19, model.getKarmaFree());
	}
	
}
