/**
 * 
 */
package org.prelle.shadowrun.gen.newprio;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.chargen.cost.PrioritySkillCostCalculator;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.modifications.SpecialRuleModification;
import org.prelle.shadowrun5.modifications.SpecialRuleModification.Rule;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillCostTest {

	private PrioritySkillCostCalculator cost;
	private List<Modification> previous;
	private ConfigOption<Boolean> OPTIMIZE;
	
	//-------------------------------------------------------------------
	static {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		cost = new PrioritySkillCostCalculator();
		previous = new ArrayList<>();
		cost.setKarmaOptimization(false);
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testSkillSimplePrioOnly1() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("swimming"), 2);
		model.addSkill(val);
		cost.process(model, previous);
		
		assertEquals(16, cost.getPointsLeftSkills());
		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testSkillSimplePrioAnd1Karma() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("swimming"), 16);
		model.addSkill(val);
		val = new SkillValue(ShadowrunCore.getSkill("software"), 3); // 2 are free - from 2 to 3 is payed with 6 karma
		model.addSkill(val);
		cost.process(model, previous);
		
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(4, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testSkillSimplePrioOnlyAfterTune() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		
		cost.setPriority(Priority.E);
		cost.setKarmaOptimization(false);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("swimming"), 2);
		val.setStart(2); // Shouldn't cost anything
		model.addSkill(val);
		cost.process(model, previous);
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(10, model.getKarmaFree());
		
		// increase in tuning mode
		val.setPoints(3);
		
		model.setKarmaFree(10);
		cost.process(model, previous);
		assertEquals(18, cost.getPointsLeftSkills()); // Priority points don't matter in tuning mode
		assertEquals(4, model.getKarmaFree());
		
//		/*
//		 * New turn on Karma optimization
//		 */
//		model.setKarmaFree(10);
//		cost.setKarmaOptimization(true);
//		cost.process(model, previous);
//		assertEquals(15, cost.getPointsLeftSkills()); // Priority points don't matter in tuning mode
//		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testKnowledgeSimplePrioOnly1() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		model.getAttribute(Attribute.LOGIC).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(2); // (1+2)*2 = 6 Points for knowledge
		cost.process(model, previous);
		assertEquals(6, cost.getPointsLeftInKnowledgeAndLanguage());
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("street_knowledge"), 2);
		model.addSkill(val);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(4, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testKnowledgeSimplePrioAnd1Karma() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		model.getAttribute(Attribute.LOGIC).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(1); // (1+1)*2 = 4 Points for knowledge
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("street_knowledge"), 5); // Requires 5*1
		model.addSkill(val);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(5, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testKnowledgeSimplePrioOnlyAfterTune() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		cost.setKarmaOptimization(false);
		model.setKarmaFree(10);
		model.getAttribute(Attribute.LOGIC).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(1); // (1+1)*2 = 4 Points for knowledge
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("street_knowledge"), 2);
		val.setStart(2); // Shouldn't cost anything
		model.addSkill(val);
		SkillValue val2 = new SkillValue(ShadowrunCore.getSkill("academic_knowledge"), 2);
		val2.setStart(2); // Shouldn't cost anything
		model.addSkill(val2);
		cost.process(model, previous);
		assertEquals(18, cost.getPointsLeftSkills()); // Priority points don't matter in tuning mode
		assertEquals(4, cost.getPointsLeftInKnowledgeAndLanguage());// Priority points don't matter in tuning mode
		assertEquals(10, model.getKarmaFree());
		
		// increase in tuning mode
		val.setPoints(3); // Will cost 3 karma, since 2 free points will be invested here
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills()); // Priority points don't matter in tuning mode
		assertEquals(4, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals( 7, model.getKarmaFree());
		
		// Now increase LOGIC and by this the free points
		// There should still be spent karma, since "academic_knowledge" will only be paid up to 2
		// with free points
		model.getAttribute(Attribute.LOGIC).setPoints(2);
		model.setKarmaFree(10);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills()); 
		assertEquals(6, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(7, model.getKarmaFree());

//		// Now optimize
//		model.setKarmaFree(10);
//		cost.setKarmaOptimization(true);
//		cost.process(model, previous);
//		assertEquals(18, cost.getPointsLeftSkills()); 
//		assertEquals(1, cost.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testGroupSimplePrioOnly1() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(10);
		
		cost.setPriority(Priority.C);
		SkillGroupValue val = new SkillGroupValue(ShadowrunCore.getSkillGroup("ATHLETICS"), 2);
		model.addSkillGroup(val);
		cost.process(model, previous);
		
		assertEquals(28, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testGroupSimplePrioAnd1Karma() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(20);
		
		cost.setPriority(Priority.C);
		SkillGroupValue val = new SkillGroupValue(ShadowrunCore.getSkillGroup("ATHLETICS"), 3); // 2 are free - from 2 to 3 is payed with 15 karma
		model.addSkillGroup(val);
		cost.process(model, previous);
		
		assertEquals(28, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(5, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testBreakGroup() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setKarmaFree(20);
		
		cost.setPriority(Priority.C);
		SkillGroupValue val = new SkillGroupValue(ShadowrunCore.getSkillGroup("ATHLETICS"), 2); 
		model.addSkillGroup(val);
		cost.process(model, previous);
		assertEquals(28, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(20, model.getKarmaFree());
		
		// Start tuning mode
		val.setStart(2);
		cost.setPriority(null);
		cost.process(model, previous);
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(20, model.getKarmaFree());
		
		val.setPoints(3);
		cost.process(model, previous);
		assertEquals(0, cost.getPointsLeftSkills());
		assertEquals(0, cost.getPointsLeftSkillGroups());
		assertEquals(5, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 */
	@Test
	public void testKnowledgeInTuning() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		cost.setKarmaOptimization(false);
		model.setKarmaFree(10);
		model.getAttribute(Attribute.LOGIC).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(2); // (1+2)*2 = 6 Points for knowledge
		cost.process(model, previous);
		assertEquals(6, cost.getPointsLeftInKnowledgeAndLanguage());
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("street_knowledge"), 3);
		val.setStart(2);
		model.addSkill(val);
		SkillValue val2 = new SkillValue(ShadowrunCore.getSkill("academic_knowledge"), 4);
		model.addSkill(val2);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(2, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(7, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	@Test
	public void testKnowledgeWithQuality() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		cost.setKarmaOptimization(false);
		previous.add(new SpecialRuleModification(Rule.SKILL_STREETLORE_COST_REDUCED));
		model.setKarmaFree(10);
		model.getAttribute(Attribute.LOGIC).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(2); // (1+2)*2 = 6 Points for knowledge
		cost.process(model, previous);
		assertEquals(6, cost.getPointsLeftInKnowledgeAndLanguage());
		
		cost.setPriority(Priority.E);
		SkillValue val = new SkillValue(ShadowrunCore.getSkill("street_knowledge"), 5);
		// Skill should cost 3 (5/2)
		model.addSkill(val);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(3, cost.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(10, model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 * Skill 1: +3 by free points, +1 by modification, +2 by karma  = 8+10  = 18 karma
	 * Skill 2: +1 by free point2, +3 by modification, +3 by karma  = 4+6+8 = 18 karma
	 */
	@Test
	public void testWithModifications() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		cost.setKarmaOptimization(false);
		model.setKarmaFree(40);
		
		cost.setPriority(Priority.E); // 18 Free points
		SkillValue val1 = new SkillValue(ShadowrunCore.getSkill("swimming"), 5);
		val1.setStart(3); 
		val1.addModification(new SkillModification(val1.getModifyable(), 1));
		model.addSkill(val1);
		SkillValue val2 = new SkillValue(ShadowrunCore.getSkill("automatics"), 4);
		val2.setStart(1); 
		val2.addModification(new SkillModification(val2.getModifyable(), 3));
		model.addSkill(val2);
		cost.process(model, previous);
		
		assertEquals(18, cost.getPointsLeftSkills());
		assertEquals(4, model.getKarmaFree());
		
//		// Now optimize
//		cost.setKarmaOptimization(true);
//		model.setKarmaFree(40);
//		cost.process(model, previous);
//		assertEquals(9, cost.getPointsLeftSkills());
//		assertEquals(40, model.getKarmaFree());
	}
	
}
