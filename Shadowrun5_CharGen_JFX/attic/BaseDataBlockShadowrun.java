/**
 * 
 */
package org.prelle.shadowrun.jfx;

import java.util.PropertyResourceBundle;

import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.CharacterGenerator;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public class BaseDataBlockShadowrun extends VBox implements GenerationEventListener {
	
	enum View {
		TILES,
		DOCUMENT,
	}
	
	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private ViewMode mode;
	private ScreenManager manager;
	private CharacterController control;
	private ShadowrunCharacter model;

	private Label lblExpFree;
	private Label lblExpInv;
	private Label lblNuyen;
	private Label ctrlMetatype;
	private Label ctrlConcept;
	private Label ctrlMagicOrReso;
	private Label ctrlGender;
	private Label lblSize;
	private Label lblWeight;
	private Label lblHair;
	private Label lblEyes;
//	private Button btnAddExp;
	private Button btnEditBase;
	
	private Button btnMeta;
	private Button btnConc;
	private Button btnMagic;
	private Button btnGend;
	
	private ToggleGroup toggleView;
	private RadioButton btnTiles, btnDocument;
	private ObjectProperty<View> viewProperty;

	//-------------------------------------------------------------------
	/**
	 */
	public BaseDataBlockShadowrun(ViewMode mode, CharacterController control) {
		this.mode = mode;
		this.control = control;
		
		initComponents();
		initLayout();
		initInteractivity();
		
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblExpFree = new Label("0");
		lblExpInv  = new Label("0");
		lblNuyen   = new Label("0 \u00A5");
			ctrlMetatype = new Label();
			ctrlConcept  = new Label();
			ctrlMagicOrReso = new Label();
			ctrlGender   = new Label();
			((Label)ctrlMetatype).setWrapText(true);
		lblSize      = new Label();
		lblWeight    = new Label();
		lblHair      = new Label();
		lblEyes      = new Label();
		
		
		lblExpFree.getStyleClass().add("text-header");
		lblExpInv .getStyleClass().add("text-header");
		lblNuyen  .getStyleClass().add("text-subheader");
		ctrlMetatype.getStyleClass().add("text-subheader");
		ctrlMagicOrReso.getStyleClass().add("text-subheader");
		ctrlConcept.getStyleClass().add("text-subheader");
		ctrlGender.getStyleClass().add("text-subheader");
		FontIcon iconAddExp = new FontIcon();
		FontIcon iconEditBase = new FontIcon("\uE17E\uE104");
		iconAddExp.addFontSymbol("\uE17E\uE109");
//		iconEditBase.addFontSymbol("\uE17E\uE104");
//		btnAddExp   = new Button(null, iconAddExp);
		btnEditBase = new Button(null, iconEditBase);
//		btnAddExp  .setTooltip(new Tooltip(uiResources.getString("button.addExp.tooltip")));
		btnEditBase.setTooltip(new Tooltip(UI.getString("button.editBase.tooltip")));
//		btnAddExp  .setStyle("-fx-padding: 8px");
		btnEditBase.setStyle("-fx-padding: 8px; -fx-border-width:0px");
		
		btnMeta = new Button(null, new FontIcon("\uE095"));
		btnConc = new Button(null, new FontIcon("\uE095"));
		btnMagic= new Button(null, new FontIcon("\uE095"));
		btnGend = new Button(null, new FontIcon("\uE095"));
		btnMeta.setStyle("-fx-font-size: 150%; -fx-border-width: 0px");
		btnConc.setStyle("-fx-font-size: large; -fx-border-width:0px");
		btnMagic.setStyle("-fx-font-size: 150%; -fx-border-width: 0px");
		btnGend.setStyle("-fx-font-size: large; -fx-border-width:0px");
		btnMagic.setVisible(false);
		
		toggleView = new ToggleGroup();
		btnTiles    = new RadioButton(UI.getString("button.view.tiles"));
		btnTiles.setToggleGroup(toggleView);
		btnDocument = new RadioButton(UI.getString("button.view.document"));
		btnDocument.setToggleGroup(toggleView);
		toggleView.selectToggle(btnTiles);
		viewProperty = new SimpleObjectProperty<View>(View.TILES);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setStyle("-fx-padding: 0em 0.5em 0.5em, 0.5em");
		setAlignment(Pos.TOP_CENTER);
		setPrefWidth(300);
		setMinWidth(300);
		setMinHeight(300);
		
		// Level
		Label heaExp       = new Label(UI.getString("label.experience"));
		Label heaNuyen     = new Label(UI.getString("label.money"));
		Label heaMetatype  = new Label(UI.getString("label.metatype"));
		Label heaConcept   = new Label(UI.getString("label.concept"));
		Label heaMagic     = new Label(UI.getString("label.magicOrResonance"));
		Label heaGender    = new Label(UI.getString("label.gender"));
		Label heaSize      = new Label(UI.getString("label.size"));
		Label heaWeight    = new Label(UI.getString("label.weight"));
		Label heaHair      = new Label(UI.getString("label.hair"));
		Label heaEyes      = new Label(UI.getString("label.eyes"));

		heaExp.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaNuyen.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaMetatype.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaConcept.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaMagic.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaGender.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaSize.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaWeight.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaHair.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaEyes.setStyle("-fx-text-fill: textcolor-highlight-primary");
		
		// Block experience
		Label expDelim = new Label("/");
		expDelim.getStyleClass().add("text-header");
		HBox lineExp = new HBox(5);
		lineExp.getChildren().addAll(lblExpFree,expDelim, lblExpInv);
		lineExp.setMaxWidth(Double.MAX_VALUE);
		lineExp.setAlignment(Pos.CENTER);
		VBox blkExp = new VBox();
		blkExp.setMaxWidth(Double.MAX_VALUE);
		blkExp.getChildren().addAll(heaExp, lineExp);
		
		// Block money
		VBox blkMoney = new VBox();
		blkMoney.setMaxWidth(Double.MAX_VALUE);
		blkMoney.getChildren().addAll(heaNuyen, lblNuyen);
		lblNuyen.setMaxWidth(Double.MAX_VALUE);
		blkMoney.setMaxWidth(Double.MAX_VALUE);
		lblNuyen.setAlignment(Pos.CENTER);
		lblNuyen.setTextAlignment(TextAlignment.CENTER);
		
		// Block Character Concept
		HBox lineConcept = new HBox(5);		
		lineConcept.getChildren().add(ctrlConcept);
		if (mode==ViewMode.GENERATION)
			lineConcept.getChildren().add(btnConc);
		lineConcept.setAlignment(Pos.CENTER_RIGHT);
		lineConcept.setMaxWidth(Double.MAX_VALUE);
		ctrlConcept.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(ctrlConcept, Priority.ALWAYS);
		VBox blkConc = new VBox();
		blkConc.getChildren().addAll(heaConcept, lineConcept);
		
		// Block metatype
		HBox lineMetatype = new HBox(5);		
		lineMetatype.getChildren().add(ctrlMetatype);
		if (mode==ViewMode.GENERATION)
			lineMetatype.getChildren().add(btnMeta);
		lineMetatype.setMaxWidth(Double.MAX_VALUE);
		ctrlMetatype.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(ctrlMetatype, Priority.ALWAYS);
		VBox blkMeta = new VBox();
		blkMeta.getChildren().addAll( heaMetatype, lineMetatype);
		
		// Block Magic Or Resonance
		HBox lineMagicOrReso = new HBox(5);		
		lineMagicOrReso.getChildren().add(ctrlMagicOrReso);
		if (mode==ViewMode.GENERATION)
			lineMagicOrReso.getChildren().add(btnMagic);
		lineMagicOrReso.setMaxWidth(Double.MAX_VALUE);
		ctrlMagicOrReso.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(ctrlMagicOrReso, Priority.ALWAYS);
		VBox blkMagic = new VBox();
		blkMagic.getChildren().addAll( heaMagic, lineMagicOrReso);
		
		// Block gender
		HBox lineGender = new HBox(5);		
		lineGender.getChildren().add(ctrlGender);
		if (mode==ViewMode.GENERATION)
			lineGender.getChildren().add(btnGend);
		lineGender.setMaxWidth(Double.MAX_VALUE);
		lineGender.setAlignment(Pos.CENTER_RIGHT);
		ctrlGender.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(ctrlGender, Priority.ALWAYS);
		VBox blkGender = new VBox();
		blkGender.getChildren().addAll(heaGender, lineGender);

		
		// Align text
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(5);
		grid.add(blkExp  , 0, 0, 2,1);
		grid.add(blkMoney, 0, 1, 2,1);
		grid.add(blkMeta , 0, 2, 2,1);
		grid.add(blkGender, 0, 4, 2,1);
		grid.add(blkMagic, 0, 5, 2,1);
		
		grid.add(heaSize, 0, 6);
		grid.add(lblSize, 1, 6);
		grid.add(heaWeight, 0, 7);
		grid.add(lblWeight, 1, 7);
		grid.add(heaHair, 0, 8);
		grid.add(lblHair, 1, 8);
		grid.add(heaEyes, 0, 9);
		grid.add(lblEyes, 1, 9);
		
		GridPane.setConstraints(lineMetatype, 0, 2, 2, 1, HPos.CENTER, VPos.CENTER, Priority.ALWAYS, Priority.NEVER);
		
		grid.getColumnConstraints().add(new ColumnConstraints(80, 120, 140));
		grid.getColumnConstraints().add(new ColumnConstraints(160, 180, 220));
		getChildren().add(grid);
		
		// Spacing
		Region spacing = new Region();
		spacing.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(spacing, Priority.ALWAYS);
		getChildren().add(spacing);
		
		// View
		VBox bxView = new VBox(5);
		bxView.getChildren().addAll(btnTiles, btnDocument);
//		getChildren().add(bxView);
		
		// Icons
		HBox icons = new HBox(20);
		icons.setAlignment(Pos.CENTER_RIGHT);
		icons.setMaxWidth(Double.MAX_VALUE);
		icons.getChildren().addAll(btnEditBase);
		getChildren().add(icons);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEditBase.setOnMouseClicked(event -> openAppearance() );
		
		btnMeta.setOnAction(event -> {
			SinglePageWizard<WizardPageMetaType> wizard = new SinglePageWizard<WizardPageMetaType>(model, (CharacterGenerator)control, WizardPageMetaType.class);
			manager.show(wizard);
		});
//		btnCult.setOnAction(event -> {
//			SinglePageWizard<WizardPageCulture> wizard = new SinglePageWizard<WizardPageCulture>(model, (SpliMoCharacterGenerator)control, WizardPageCulture.class, true);
//			manager.show(wizard);
//		});
//		btnBack.setOnAction(event -> {
//			SinglePageWizard<WizardPageBackground> wizard = new SinglePageWizard<WizardPageBackground>(model, (SpliMoCharacterGenerator)control, WizardPageBackground.class, true);
//			manager.show(wizard);
//		});
//		btnEduc.setOnAction(event -> {
//			SinglePageWizard<WizardPageEducation> wizard = new SinglePageWizard<WizardPageEducation>(model, (SpliMoCharacterGenerator)control, WizardPageEducation.class, true);
//			manager.show(wizard);
//		});
//		
//		
//		if (mode==ViewMode.GENERATION) {
//			((TextField)lblCulture   ).setOnKeyTyped(event -> {
//				Culture own = model.getOwnCulture();
//				if (own==null) {
//					own = new Culture();
//					own.setKey("custom");
//					model.setOwnCulture(own);
//				}
//				own.setName(((TextField)lblCulture   ).getText());
//			});
//			((TextField)lblBackground).setOnKeyTyped(event -> {
//				Background own = model.getOwnBackground();
//				if (own==null) {
//					own = new Background();
//					own.setKey("custom");
//					model.setOwnBackground(own);
//				}
//				own.setName(((TextField)lblBackground   ).getText());
//			});
//			((TextField)lblEducation).setOnKeyTyped(event -> {
//				Education own = model.getOwnEducation();
//				if (own==null) {
//					own = new Education();
//					own.setKey("custom");
//					model.setOwnEducation(own);
//				}
//				own.setName(((TextField)lblEducation   ).getText());
//			});
//		}
		
		toggleView.selectedToggleProperty().addListener( (ov,o,n) -> {
			if (n==btnDocument)
				viewProperty.set(View.DOCUMENT);
			if (n==btnTiles)
				viewProperty.set(View.TILES);
		});
		
		viewProperty.addListener( (ov,o,n) -> {
			if (n==View.DOCUMENT && toggleView.getSelectedToggle()!=btnDocument)
				toggleView.selectToggle(btnDocument);
			if (n==View.TILES && toggleView.getSelectedToggle()!=btnTiles)
				toggleView.selectToggle(btnTiles);
		});
	}

	//-------------------------------------------------------------------
	public ObjectProperty<View> viewProperty() {
		return viewProperty;
	}

	//-------------------------------------------------------------------
	public void setManager(ScreenManager manager) {
		if (manager==null)
			throw new NullPointerException("Manager may not be null");
		this.manager = manager;
	}

	//-------------------------------------------------------------------
	private void openAppearance() {
		ShadowrunAppearanceScreen toShow = new ShadowrunAppearanceScreen(control, mode);
		toShow.setData(model);
		manager.show(toShow);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		lblExpFree.setText(String.valueOf(model.getKarmaFree()));
		lblExpInv.setText(String.valueOf(model.getKarmaInvested()));
		lblNuyen.setText(model.getNuyen()+" \u00A5");

		if (model.getMagicOrResonanceType()!=null)
			ctrlMagicOrReso.setText(model.getMagicOrResonanceType().getName());
		else
			ctrlMagicOrReso.setText("-");

		if (model.getConcept()!=null)
			ctrlConcept.setText(model.getConcept().getName());
		else
			ctrlConcept.setText("-");

		if (model.getMetatype()!=null)
			ctrlMetatype.setText(model.getMetatype().getName());
		else
			ctrlMetatype.setText("-");

			((Label)ctrlGender).setText(model.getGender().toString());
		
//		if (mode==ViewMode.GENERATION) {
//			if (model.getArchetype()!=null) ((TextField)lblArchetype).setText(model.getArchetype().getName());
//			if (model.getMotivation()!=null) ((TextField)lblMotivation).setText(model.getMotivation().getName());
//		} else {
//			if (model.getArchetype()!=null) ((Label)lblArchetype).setText(model.getArchetype().getName());
//			if (model.getMotivation()!=null) ((Label)lblMotivation).setText(model.getMotivation().getName());
//		}
		lblSize.setText(model.getSize()+" "+UI.getString("label.size.unit"));
		lblWeight.setText(model.getWeight()+" "+UI.getString("label.weight.unit"));
		lblHair.setText(model.getHairColor());
		lblEyes.setText(model.getEyeColor());
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter charac) {
		this.model = charac;
		refresh();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
		case BASE_DATA_CHANGED:
		case METATYPE_CHANGED:
		case EXPERIENCE_CHANGED:
		case NUYEN_CHANGED:
			refresh();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	public void setCharacterGenerator(CharacterController control) {
		this.control = control;
	}

}
