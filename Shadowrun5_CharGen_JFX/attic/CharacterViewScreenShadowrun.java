package org.prelle.shadowrun.jfx;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.PropertyResourceBundle;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManager;
import org.prelle.rpgframework.jfx.CardTile;
import org.prelle.rpgframework.jfx.CharacterViewScreen;
import org.prelle.rpgframework.jfx.CharacterViewScreenSkin;
import org.prelle.shadowrun.Attribute;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.Skill.SkillType;
import org.prelle.shadowrun.charctrl.CharGenMode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.CharacterGenerator;
import org.prelle.shadowrun.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.attributes.AttributeCard;
import org.prelle.shadowrun.jfx.attributes.AttributeScreen;
import org.prelle.shadowrun.jfx.cforms.ComplexFormCard;
import org.prelle.shadowrun.jfx.cforms.ComplexFormScreen;
import org.prelle.shadowrun.jfx.connections.ConnectionCard;
import org.prelle.shadowrun.jfx.connections.ConnectionScreen;
import org.prelle.shadowrun.jfx.develop.DevelopmentCard;
import org.prelle.shadowrun.jfx.develop.DevelopmentScreenSR5;
import org.prelle.shadowrun.jfx.items.BodytechCard;
import org.prelle.shadowrun.jfx.items.BodytechScreen;
import org.prelle.shadowrun.jfx.items.GearCard;
import org.prelle.shadowrun.jfx.items.GearScreen;
import org.prelle.shadowrun.jfx.items.MagicalEquipmentCard;
import org.prelle.shadowrun.jfx.items.MagicalEquipmentScreen;
import org.prelle.shadowrun.jfx.items.VehicleCard;
import org.prelle.shadowrun.jfx.items.VehicleScreen;
import org.prelle.shadowrun.jfx.items.WeaponCard;
import org.prelle.shadowrun.jfx.items.WeaponScreen;
import org.prelle.shadowrun.jfx.matrix.MatrixCard;
import org.prelle.shadowrun.jfx.matrix.MatrixScreen;
import org.prelle.shadowrun.jfx.notes.NotesCard;
import org.prelle.shadowrun.jfx.notes.NotesScreen;
import org.prelle.shadowrun.jfx.powers.PowerCard;
import org.prelle.shadowrun.jfx.powers.PowerScreen;
import org.prelle.shadowrun.jfx.qualities.QualityCard;
import org.prelle.shadowrun.jfx.qualities.QualityScreen;
import org.prelle.shadowrun.jfx.sins.LifestyleCard;
import org.prelle.shadowrun.jfx.sins.LifestyleScreen;
import org.prelle.shadowrun.jfx.sins.SINCard;
import org.prelle.shadowrun.jfx.sins.SINScreen;
import org.prelle.shadowrun.jfx.skills.SkillCard;
import org.prelle.shadowrun.jfx.skills.SkillScreen;
import org.prelle.shadowrun.jfx.spells.RitualCard;
import org.prelle.shadowrun.jfx.spells.RitualScreen;
import org.prelle.shadowrun.jfx.spells.SpellCard;
import org.prelle.shadowrun.jfx.spells.SpellScreen;
import org.prelle.shadowrun.jfx.summonables.SummonableCard;
import org.prelle.shadowrun.jfx.summonables.SummonableScreen;
import org.prelle.shadowrun.levelling.CharacterLeveller;

import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterHandle.Format;
import de.rpgframework.character.CharacterHandle.Type;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class CharacterViewScreenShadowrun extends CharacterViewScreen implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private final static String CSS_COMMON = "css/shadowrun-common.css";
	private final static String CSS = "css/shadowrun-dark.css";

	private static Preferences CONFIG = Preferences.userRoot().node("/org/rpgframework/genesis/shadowrun");

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;

	private BaseDataBlockShadowrun baseBlock;
	/*
	 * Attribute shortview
	 */
	private AttributeCard attrPrimary;
	private AttributeCard attrSecondary;
	private AttributeCard attrSpecial;
	private QualityCard qualities;
	/* Skills */
	private SkillCard skillNormal;
	private SpellCard spells;
	private SpellCard alchemy;
	private RitualCard rituals;
	private PowerCard powers;
	private BodytechCard bodytech;
	private WeaponCard weapons;
	private GearCard gear;
	private MagicalEquipmentCard magical;
	private ComplexFormCard cplxforms;
	private VehicleCard vehicles;
	private ConnectionCard connections;
	private SINCard sins;
	private LifestyleCard lifestyles;
	private SummonableCard summonables;
	private DevelopmentCard development;
	private MatrixCard matrix;
	private NotesCard notes;

	private CardTile tileAttr;
	private CardTile tileSkills;
	private CardTile tileQualities;
	private CardTile tilePowers;
	private CardTile tileWeapons;
	private CardTile tileSpells;
	private CardTile tileAlchemy;
	private CardTile tileRituals;
	private CardTile tileBodytech;
	private CardTile tileGear;
	private CardTile tileMagical;
	private CardTile tileCplxForms;
	private CardTile tileVehicles;
	private CardTile tileConnections;
	private CardTile tileSINs;
	private CardTile tileLifestyles;
	private CardTile tileSummonables;
	private CardTile tileMatrix;
	private CardTile tileDevelopment;
	private CardTile tileNotes;

	private transient CharGenWizardNG    wizard;

	//-------------------------------------------------------------------
	public CharacterViewScreenShadowrun(CharacterController control, ViewMode mode, CharacterHandle handle) {
		this.setId("genesis/shadowrun");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		setSkin(new CharacterViewScreenSkin(this));
		initComponents();

		//		getStyleClass().add("page");
		setTitle("Unnamed chummer");

		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.fluent.NavigableContentNode#getStyleSheets()
	 */
	@Override
	public String[] getStyleSheets() {
		return new String[] {getClass().getResource(CSS_COMMON).toExternalForm(), getClass().getResource(CSS).toExternalForm()};
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		getNavigButtons().addAll(CloseType.APPLY, CloseType.CANCEL);

		initBaseBlockLayout();
		initAttributeBlocks();
		initQualityBlock();
		initSkillBlock();
		initSpellBlock();
		initAlchemyBlock();
		initRitualBlock();
		initPowerBlock();
		initBodytechBlock();
		initGearBlock();
		initMagicalBlock();
		initComplexFormsBlock();
		initWeaponBlock();
		initVehicleBlock();
		initConnectionBlock();
		initSINBlock();
		initLifestyleBlock();
		initSummonableBlock();
		initMatrixBlock();
		initDevelopmentBlock();
		initNotesCard();
	}

	//-------------------------------------------------------------------
	private void initBaseBlockLayout() {
		baseBlock = new BaseDataBlockShadowrun(mode, control);
		HBox.setMargin(baseBlock, new Insets(40,0,20,0));
		VBox.setVgrow(baseBlock, Priority.ALWAYS);
		setSide(baseBlock);
	}

	//-------------------------------------------------------------------
	private void initAttributeBlocks() {
		attrPrimary   = new AttributeCard(Attribute.primaryValues());
		attrSpecial   = new AttributeCard(Attribute.specialAttributes());
		attrSecondary = new AttributeCard(Attribute.secondaryValues());

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		Region spacing2 = new Region();
		spacing2.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(attrPrimary, spacing2, attrSpecial, spacing,attrSecondary);

		tileAttr = new CardTile("Attributes", UI.getString("card.attributes"));
		tileAttr.setBackside(content);
		tileAttr.setOnAction(event -> openAttributes());
		getItems().add(tileAttr);
	}

	//-------------------------------------------------------------------
	private void initQualityBlock() {
		qualities = new QualityCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(qualities);

		tileQualities = new CardTile("Qualities", UI.getString("card.qualities"));
		tileQualities.setBackside(content);
		tileQualities.setOnAction(event -> openQualities());
		getItems().add(tileQualities);
	}

	//-------------------------------------------------------------------
	private void initSkillBlock() {
		skillNormal = new SkillCard(SkillType.COMBAT);

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(skillNormal);

		tileSkills = new CardTile("Skills", UI.getString("card.skills"));
		tileSkills.setBackside(content);
		tileSkills.setOnAction(event -> openSkills());
		getItems().add(tileSkills);
	}

	//-------------------------------------------------------------------
	private void initSpellBlock() {
		spells = new SpellCard(false);

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(spells);

		tileSpells = new CardTile("Spells", UI.getString("card.spells"));
		tileSpells.setBackside(content);
		tileSpells.setOnAction(event -> openSpells(false));
		getItems().add(tileSpells);
	}

	//-------------------------------------------------------------------
	private void initAlchemyBlock() {
		alchemy = new SpellCard(true);

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(alchemy);

		tileAlchemy = new CardTile("Alchemy", UI.getString("card.alchemy"));
		tileAlchemy.setBackside(content);
		tileAlchemy.setOnAction(event -> openSpells(true));
		getItems().add(tileAlchemy);
	}

	//-------------------------------------------------------------------
	private void initRitualBlock() {
		rituals = new RitualCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(rituals);

		tileRituals = new CardTile("Rituals", UI.getString("card.rituals"));
		tileRituals.setBackside(content);
		tileRituals.setOnAction(event -> openRituals());
		getItems().add(tileRituals);
	}

	//-------------------------------------------------------------------
	private void initPowerBlock() {
		powers = new PowerCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(powers);

		tilePowers = new CardTile("Powers", UI.getString("card.powers"));
		tilePowers.setBackside(content);
		tilePowers.setOnAction(event -> openPowers());
		getItems().add(tilePowers);
	}

	//-------------------------------------------------------------------
	private void initBodytechBlock() {
		bodytech = new BodytechCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(bodytech);

		tileBodytech = new CardTile("Bodytech", UI.getString("card.bodytech"));
		tileBodytech.setBackside(content);
		tileBodytech.setOnAction(event -> openBodytech());
		getItems().add(tileBodytech);
	}

	//-------------------------------------------------------------------
	private void initWeaponBlock() {
		weapons = new WeaponCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(weapons);

		tileWeapons = new CardTile("Weapons", UI.getString("card.weapons"));
		tileWeapons.setBackside(content);
		tileWeapons.setOnAction(event -> openWeapons());
		getItems().add(tileWeapons);
	}

	//-------------------------------------------------------------------
	private void initGearBlock() {
		gear = new GearCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(gear);

		tileGear = new CardTile("Gear", UI.getString("card.gear"));
		tileGear.setBackside(content);
		tileGear.setOnAction(event -> openGear());
		getItems().add(tileGear);
	}

	//-------------------------------------------------------------------
	private void initMagicalBlock() {
		magical = new MagicalEquipmentCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(magical);

		tileMagical = new CardTile("Magical", UI.getString("card.magical"));
		tileMagical.setBackside(content);
		tileMagical.setOnAction(event -> openMagical());
		getItems().add(tileMagical);
	}

	//-------------------------------------------------------------------
	private void initComplexFormsBlock() {
		cplxforms = new ComplexFormCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(cplxforms);

		tileCplxForms = new CardTile("ComplexForms", UI.getString("card.complexforms"));
		tileCplxForms.setBackside(content);
		tileCplxForms.setOnAction(event -> openComplexForms());
		getItems().add(tileCplxForms);
	}

	//-------------------------------------------------------------------
	private void initVehicleBlock() {
		vehicles = new VehicleCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(vehicles);

		tileVehicles = new CardTile("Vehicles", UI.getString("card.vehicles"));
		tileVehicles.setBackside(content);
		tileVehicles.setOnAction(event -> openVehicles());
		getItems().add(tileVehicles);
	}

	//-------------------------------------------------------------------
	private void initConnectionBlock() {
		connections = new ConnectionCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(connections);

		tileConnections = new CardTile("Connections", UI.getString("card.connections"));
		tileConnections.setBackside(content);
		tileConnections.setOnAction(event -> openConnections());
		getItems().add(tileConnections);
	}

	//-------------------------------------------------------------------
	private void initSINBlock() {
		sins = new SINCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(sins);

		tileSINs = new CardTile("Sins", UI.getString("card.sins"));
		tileSINs.setBackside(content);
		tileSINs.setOnAction(event -> openSINs());
		getItems().add(tileSINs);
	}

	//-------------------------------------------------------------------
	private void initLifestyleBlock() {
		lifestyles = new LifestyleCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(lifestyles);

		tileLifestyles = new CardTile("lifestyle", UI.getString("card.lifestyles"));
		tileLifestyles.setBackside(content);
		tileLifestyles.setOnAction(event -> openLifestyles());
		getItems().add(tileLifestyles);
	}

	//-------------------------------------------------------------------
	private void initSummonableBlock() {
		summonables = new SummonableCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(summonables);

		tileSummonables = new CardTile("summonables", UI.getString("card.summonables"));
		tileSummonables.setBackside(content);
		tileSummonables.setOnAction(event -> openSummonables());
		getItems().add(tileSummonables);
	}

	//-------------------------------------------------------------------
	private void initMatrixBlock() {
		matrix = new MatrixCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(matrix);

		tileMatrix = new CardTile("matrix", UI.getString("card.matrix"));
		tileMatrix.setBackside(content);
		tileMatrix.setOnAction(event -> openMatrix());
		getItems().add(tileMatrix);
	}

	//-------------------------------------------------------------------
	private void initDevelopmentBlock() {
		development = new DevelopmentCard();

		Region spacing = new Region();
		spacing.setPrefHeight(20);
		VBox content = new VBox();
		content.setPrefWidth(300);
		content.getChildren().addAll(development);

		tileDevelopment = new CardTile("Development", UI.getString("card.development"));
		tileDevelopment.setBackside(content);
		tileDevelopment.setOnAction(event -> openDevelopment());
		getItems().add(tileDevelopment);
	}

	//-------------------------------------------------------------------
	private void initNotesCard() {
		notes   = new NotesCard();

		tileNotes = new CardTile("Notes", UI.getString("card.notes"));
		tileNotes.setBackside(notes);
		tileNotes.setOnAction(event -> openNotes());
		getItems().add(tileNotes);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#setScreenManager(org.prelle.javafx.ScreenManager)
	 */
	@Override
	public void setScreenManager(ScreenManager manager) {
		super.setScreenManager(manager);
		logger.debug("Update stylesheet");
		//		manager.getScene().getStylesheets().add(CSS);

		baseBlock.setManager(manager);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#close(org.prelle.javafx.ManagedScreen.CloseType)
	 */
	@Override
	public boolean close(CloseType type) {
		logger.debug("clean up before closing("+type+") in mode "+mode);
		logger.trace("Update stylesheet: "+manager.getScene().getStylesheets());
		//		manager.getScene().getStylesheets().remove(CSS);

		if (type==CloseType.APPLY) {
			if (control instanceof CharacterGenerator) {
				// Before finalizing character creation, ensure that it is okay
				if (!((CharacterGenerator)control).hasEnoughData()) {
					StringBuffer message = new StringBuffer( UI.getString("error.charcreation.todos") );
					((CharacterGenerator)control).getToDos().stream().forEach(txt -> message.append("\n"+txt));
					logger.warn(message);
					Label mess = new Label(message.toString());
					mess.setWrapText(true);
					manager.showAlertAndCall(AlertType.NOTIFICATION, UI.getString("dialog.cannotfinishcreation"), mess);
					return false;
				}
				if (model.getNuyen()>5000) {
					logger.info("ask user to confirm finishing character generation although money >5000 nuyen is lost");
					Label mess = new Label(String.format(UI.getString("error.charcreation.nuyen"), model.getNuyen()-5000 ));
					mess.setWrapText(true);
					CloseType response = manager.showAlertAndCall(AlertType.CONFIRMATION, UI.getString("dialog.to_many_nuyen.title"), mess);
					getCloseType();
					if (response!=CloseType.YES)
						return false;
				}

				((CharacterGenerator)control).stop();
			}

			GenerationEventDispatcher.clear();
			saveCharacter(model);
			logger.debug("Saved character "+handle);

			return true;
		} else {
			logger.warn("TODO: reload ShadowrunCharacter");
		}
		return true;
	}

	//-------------------------------------------------------------------
	private void saveCharacter(ShadowrunCharacter charac) {
		CharacterProvider provider = RPGFrameworkLoader.getInstance().getCharacterAndRules().getCharacterService();
		/*
		 * Write all made modifications to character
		 */
		if (control instanceof CharacterLeveller) {
			logger.info("Add modifications to character log");
			((CharacterLeveller)control).updateHistory();
		}

		try {

			/*
			 * If there is no handle, this character has not yet been
			 * created on disk.
			 */
			if (handle==null) {
				logger.debug("Create character handle for "+model.getName());
				handle = provider.createCharacter(charac.getName(), RoleplayingSystem.SHADOWRUN);
				assert handle.getName()!=null;
			}

			/*
			 * Update the character definition on disk.
			 * If it just been created, this may be redundant.
			 */
			handle.setCharacter(charac);
			handle.setLastModified(new Date(System.currentTimeMillis()));

			/*
			 * 1. Call plugin to encode character
			 * 2. Use character service to save character
			 */
			logger.debug("encode character "+model.getName());
			CommandResult result = CommandBus.fireCommand(this, CommandType.ENCODE,
					handle.getRuleIdentifier(),
					model
					);
			if (!result.wasProcessed()) {
				logger.error("Cannot save character, since encoding failed");
				manager.showAlertAndCall(
						AlertType.ERROR,
						"Das hätte nicht passieren dürfen",
						"Es hat sich kein Plugin gefunden, welches das Kodieren von Charakteren dieses Systems erlaubt."
						);
			} else {
				byte[] encoded = (byte[]) result.getReturnValue();
				try {
					logger.info("Save character "+model.getName());
					RPGFrameworkLoader.getInstance().getCharacterAndRules().getCharacterService().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, null, encoded);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			/*
			 * The character is stored safely. No write the image
			 * if one exists
			 */
			Attachment imgAttachment = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
			if (charac.getImage()!=null) {
				if (imgAttachment!=null) {
					// Old image attachment exists - update it
					logger.debug("Update image attachment");
					imgAttachment.setData(charac.getImage());
					provider.modifyAttachment(handle, imgAttachment);
				} else {
					// No previous image attachment - create it
					logger.debug("Create image attachment");
					provider.addAttachment(handle, Type.CHARACTER, Format.IMAGE, null, charac.getImage());
				}
			} else {
				if (imgAttachment!=null) {
					// Old image attachment exists - delete it
					logger.warn("Delete image attachment");
					provider.removeAttachment(handle, imgAttachment);
				}
			}

		} catch (IOException e) {
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, UI.getString("error.savingCharacter"), out.toString());
		}

	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case BASE_DATA_CHANGED:
			logger.debug("RCV "+event);
			setTitle(model.getName());
			if (model.getImage()!=null) {
				setImage(new Image(new ByteArrayInputStream(model.getImage())));
			} else {
				setImage(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"images/guest-256.png")));
			}
			break;
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			control = (CharacterGenerator) event.getKey();
			baseBlock.setCharacterGenerator(control);
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model, CharacterHandle handle) {
		logger.debug("-----------------Show character "+model+"-------------------");
		this.model = model;
		this.handle= handle;

		setTitle(model.getName());

		/*
		 * Base block
		 */
		if (model.getImage()!=null) {
			logger.debug("hasImage");
			setImage(new Image(new ByteArrayInputStream(model.getImage())));
			//			setPortrait(new Image(new ByteArrayInputStream(model.getImage())));
		} else {
			setImage(new Image(getClass().getResourceAsStream("images/guest-256.png")));
		}
		baseBlock.setData(model);
		attrPrimary.setData(model);
		attrSecondary.setData(model);
		attrSpecial.setData(model);
		qualities.setData(model);
		skillNormal.setData(model);
		spells.setData(model);
		alchemy.setData(model);
		rituals.setData(model);
		powers.setData(model);
		weapons.setData(model);
		gear.setData(model);
		magical.setData(model);
		cplxforms.setData(model);
		bodytech.setData(model);
		vehicles.setData(model);
		sins.setData(model);
		lifestyles.setData(model);
		connections.setData(model);
		summonables.setData(model);
		matrix.setData(model);
		development.setData(model);
		notes.setData(model);

		if (model.getMagicOrResonanceType()!=null && !model.getMagicOrResonanceType().usesPowers()) {
			logger.debug("Do not show Power tile, since character does not use them");
			getItems().remove(tilePowers);
		} else if (!getItems().contains(tilePowers)){
			getItems().add(tilePowers);
		}

		if (model.getMagicOrResonanceType()!=null && !model.getMagicOrResonanceType().usesSpells()) {
			logger.debug("Do not show Spell/Ritual/Foki tile, since character does not use them");
			getItems().removeAll(tileMagical, tileRituals, tileSpells, tileAlchemy);
		} else if (!getItems().contains(tileMagical)){
			getItems().addAll(tileMagical, tileRituals, tileSpells, tileAlchemy);
		}

		if (model.getMagicOrResonanceType()!=null && !model.getMagicOrResonanceType().usesResonance()) {
			logger.debug("Do not show ComplexForms tile, since character does not use them");
			getItems().remove(tileCplxForms);
		} else if (!getItems().contains(tileCplxForms)){
			getItems().add(tileCplxForms);
		}

		if (!model.getMagicOrResonanceType().usesMagic() && !model.getMagicOrResonanceType().usesResonance()) {
			logger.debug("Do not show Summonables tile, since character does not use them");
			getItems().remove(tileSummonables);
		} else if (!getItems().contains(tileSummonables)){
			getItems().add(tileSummonables);
		}

		updateAttentionFlags();
	}

	//--------------------------------------------------------------------
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	public void startGeneration(ShadowrunCharacter model) {
		logger.debug("startGeneration "+model);
		mode = ViewMode.GENERATION;
		this.model = model;
		setData(model, null);
		tileDevelopment.setVisible(false);
		new CharacterGeneratorRegistry();
		//		control = CharacterGeneratorRegistry.getGenerators().get(0);

		wizard = new CharGenWizardNG(model);
		CloseType close = (CloseType)manager.showAndWait(wizard);
		logger.info("Closed with "+close);

		updateAttentionFlags();

		if (close==CloseType.FINISH) {
			logger.info("Wizard finished");
			try {
				byte[] data = ShadowrunCore.save(model);
				handle = RPGFrameworkLoader.getInstance().getCharacterService().createCharacter(model.getName(), RoleplayingSystem.SHADOWRUN);
				RPGFrameworkLoader.getInstance().getCharacterService().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, model.getName()+".xml", data);
				manager.showAlertAndCall(AlertType.NOTIFICATION, UI.getString("alert.start_tuning.title"),
						String.format(UI.getString("alert.start_tuning.message"), handle.getPath().toString()));
			} catch (IOException e) {
				logger.error("Failed writing newly created character to disk",e);
				manager.showAlertAndCall(AlertType.ERROR, UI.getString("error.saving_character.title"),
						String.format(UI.getString("error.saving_character.message"), e.toString()));
			}
//			this.control = new CoriolisCharacterLeveller(model);
			setData(model, handle);
//			refresh();
		} else {
			logger.warn("Wizard "+close);
			wizard.close(close);
			logger.warn("Close current page ");
			getScreenManager().closeCurrent(close);
			logger.warn("Call ScreenManager.cancel() ");
			getScreenManager().cancel();

		}
	}

	//-------------------------------------------------------------------
	private void updateAttentionFlags() {
		logger.debug("updateAttentionFlags");
		if (control==null)
			return;
		tileAttr.setAttentionFlag(control.getAttributeController().getToDos().size()>0);
		tileBodytech.setAttentionFlag(control.getEquipmentController().getToDos().size()>0);
		tileConnections.setAttentionFlag(control.getConnectionController().getToDos().size()>0);
		tileCplxForms.setAttentionFlag(control.getComplexFormController().getToDos().size()>0);
		tileGear.setAttentionFlag(control.getEquipmentController().getToDos().size()>0);
		tileLifestyles.setAttentionFlag(control.getLifestyleController().getToDos().size()>0);
		tilePowers.setAttentionFlag(control.getPowerController().getToDos().size()>0 );
		tileRituals.setAttentionFlag(control.getSpellController().getToDos().size()>0);
		tileQualities.setAttentionFlag(control.getQualityController().getToDos().size()>0);
		tileSINs.setAttentionFlag(control.getSINController().getToDos().size()>0);
		tileSkills.setAttentionFlag(control.getSkillController().getToDos().size()>0);
		tileSpells.setAttentionFlag(control.getSpellController().getToDos().size()>0);

		tileAttr.setAttentionText(control.getAttributeController().getToDos());
		tileBodytech.setAttentionText(control.getEquipmentController().getToDos());
		tileConnections.setAttentionText(control.getConnectionController().getToDos());
		tileCplxForms.setAttentionText(control.getComplexFormController().getToDos());
		tileGear.setAttentionText(control.getEquipmentController().getToDos());
		tileLifestyles.setAttentionText(control.getLifestyleController().getToDos());
		tileRituals.setAttentionText(control.getSpellController().getToDos());
		tilePowers.setAttentionText(control.getPowerController().getToDos());
		tileQualities.setAttentionText(control.getQualityController().getToDos());
		tileSINs.setAttentionText(control.getSINController().getToDos());
		tileSkills.setAttentionText(control.getSkillController().getToDos());
		tileSpells.setAttentionText(control.getSpellController().getToDos());
	}

	//-------------------------------------------------------------------
	private void saveCharacter() {
		logger.debug("START: saveCharacter");

		if (mode==ViewMode.MODIFICATION) {
			/*
			 * Write all made modifications to character
			 */
			logger.debug("Add modifications to character log");
			((CharacterLeveller)control).updateHistory();
		}

		try {
			/*
			 * 1. Call plugin to encode character
			 * 2. Use character service to save character
			 */
			logger.debug("encode character "+model.getName());
			CommandResult result = CommandBus.fireCommand(this, CommandType.ENCODE,
					handle.getRuleIdentifier(),
					model
					);
			if (!result.wasProcessed()) {
				logger.error("Cannot save character, since encoding failed");
				manager.showAlertAndCall(
						AlertType.ERROR,
						"Das hätte nicht passieren dürfen",
						"Es hat sich kein Plugin gefunden, welches das Kodieren von Charakteren dieses Systems erlaubt."
						);
			} else {
				byte[] encoded = (byte[]) result.getReturnValue();
				try {
					logger.info("Save character "+model.getName());
					RPGFrameworkLoader.getInstance().getCharacterAndRules().getCharacterService().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, null, encoded);
					logger.info("Saved character "+model.getName()+" successfully");
				} catch (IOException e) {
					logger.error("Failed saving character",e);
					BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Failed saving character.\n"+e);
				}
			}

			/*
			 * Update portrait
			 */
			logger.debug("Update portrait");
			CharacterProvider charServ = RPGFrameworkLoader.getInstance().getCharacterAndRules().getCharacterService();
			try {
				if (model.getImage()!=null && handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Update character image");
						attach.setData(model.getImage());
						charServ.modifyAttachment(handle, attach);
					} else {
						charServ.addAttachment(handle, Type.CHARACTER, Format.IMAGE, null, model.getImage());
					}
				} else if (handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Delete old character image");
						charServ.removeAttachment(handle, attach);
					}
				}
			} catch (IOException e) {
				logger.error("Failed modifying portrait attachment",e);
			}
		} finally {
			logger.debug("STOP : saveCharacter");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#backSelected()
	 */
	@Override
	public CloseType backSelected() {
		logger.debug("backSelected()");

		logger.debug("View mode is "+mode);
		if (mode==ViewMode.GENERATION) {
			CharacterGenerator generator = (CharacterGenerator)control;
			if (!generator.hasEnoughData()) {
				logger.warn("Generator has not enough data");
				CloseType dialog = manager.showAlertAndCall(AlertType.ERROR,
						UI.getString("error.chargen.missingdata.title"),
						UI.getString("error.chargen.missingdata.text"));
				return null;
			}
			logger.debug("call generate()");
			((CharacterGenerator)control).stop();
			logger.debug("generate() finished");
		}

		CloseType choice = manager.showAlertAndCall(AlertType.CONFIRMATION,
				UI.getString("check.chargen.editing.abort.title"),
				UI.getString("check.chargen.editing.abort.text")
				);
		logger.debug("Choice was "+choice);
		if (choice==CloseType.YES) {
			logger.info("User confirmed changes to character");
			saveCharacter();
			return CloseType.APPLY;
		} else if (choice==CloseType.NO) {
			logger.info("User rejected changes to character");
			return CloseType.CANCEL;
		}

		logger.error("Unexpected type of response: "+choice);
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#childClosed(org.prelle.javafx.ManagedScreen, org.prelle.javafx.CloseType)
	 */
	public void childClosed(ManagedScreen child, CloseType type) {
		logger.debug("START: childClosed("+child+", "+type+")");
		try {
			if (child==wizard) {
				if (type==CloseType.FINISH) {
					logger.info("generation wizard finished");

					// Update Levelling GUI
					logger.debug("Update GUI");
					setData(model, handle);
					if (control instanceof CharacterGenerator)
						((CharacterGenerator)control).startTuningMode();

					updateAttentionFlags();
				} else if (type==CloseType.CANCEL) {
					logger.debug("Generation cancelled");
					if (control instanceof CharacterGenerator)
						((CharacterGenerator)control).stop();
					GenerationEventDispatcher.clear();
					wizard  = null;
					manager.close(this, CloseType.CANCEL);
				}
			} else if (child.getClass()==ShadowrunAppearanceScreen.class) {
				if (handle!=null && !model.getName().equals(handle.getName())) {
					logger.info("Rename "+handle.getName()+" to "+model.getName());
					try {
						RPGFrameworkLoader.getInstance().getCharacterAndRules().getCharacterService().renameCharacter(handle, model.getName());
						BabylonEventBus.fireEvent(BabylonEventType.CHAR_RENAMED, handle);
					} catch (IOException e) {
						logger.error("Failed renaming",e);
					}
				}
			} else {
				updateAttentionFlags();
				logger.warn("childClosed("+child+", "+type+") not processed");
			}
		} finally {
			logger.debug("STOP : childClosed("+child+", "+type+")");
		}
	}

	//-------------------------------------------------------------------
	private void openAttributes() {
		logger.debug("openAttributes");
		try {
			AttributeScreen toShow = new AttributeScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening AttributeScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening AttributeScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openQualities() {
		logger.debug("openQualities in mode "+mode);
		try {
			QualityScreen toShow = new QualityScreen(control, (mode==ViewMode.GENERATION)?CharGenMode.CREATING:CharGenMode.LEVELING);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening QualityScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening QualityScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openSkills() {
		logger.debug("openSkills");
		try {
			SkillScreen toShow = new SkillScreen(control);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening SkillScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening SkillScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openSpells(boolean alchemistic) {
		logger.debug("openSpells("+alchemistic+")");
		try {
			SpellScreen toShow = new SpellScreen(control,alchemistic);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening SpellScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening SpellScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openRituals() {
		logger.debug("openRituals");
		try {
			RitualScreen toShow = new RitualScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening RitualScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening SpellScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openPowers() {
		logger.debug("openPowers");
		try {
			PowerScreen toShow = new PowerScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening PowerScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening PowerScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openBodytech() {
		logger.debug("openBodytech");
		try {
			BodytechScreen toShow = new BodytechScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening BodytechScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening BodytechScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openWeapons() {
		logger.debug("openWeapons");
		try {
			WeaponScreen toShow = new WeaponScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening WeaponScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening WeaponScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openGear() {
		logger.debug("openGear");
		try {
			GearScreen toShow = new GearScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening GearScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening GearScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openMagical() {
		logger.debug("openMagical");
		try {
			MagicalEquipmentScreen toShow = new MagicalEquipmentScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening MagicalEquipmentScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening MagicalEquipmentScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openComplexForms() {
		logger.debug("openComplexForms");
		try {
			ComplexFormScreen toShow = new ComplexFormScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening ComplexFormScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening ComplexFormScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openVehicles() {
		logger.debug("openVehicles");
		try {
			VehicleScreen toShow = new VehicleScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening VehicleScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening VehicleScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openConnections() {
		logger.debug("openConnections");
		try {
			ConnectionScreen toShow = new ConnectionScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening ConnectionScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening ConnectionScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openSINs() {
		logger.debug("openSINs");
		try {
			SINScreen toShow = new SINScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening SINScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening SINScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openLifestyles() {
		logger.debug("openLifestyles");
		try {
			LifestyleScreen toShow = new LifestyleScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening LifestyleScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening LifestyleScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openSummonables() {
		logger.debug("openSummonables");
		try {
			SummonableScreen toShow = new SummonableScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening SummonableScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening SummonableScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openMatrix() {
		logger.debug("openMatrix");
		try {
			MatrixScreen toShow = new MatrixScreen(control);
			toShow.setData(model);
			manager.show(toShow);
		} catch (ExceptionInInitializerError eiie) {
			Throwable e = eiie.getCause();
			logger.error("Failed opening MatrixScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		} catch (Throwable e) {
			logger.error("Failed opening MatrixScreen",e);
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			manager.showAlertAndCall(AlertType.ERROR, e.getClass().getSimpleName(), out.toString());
		}
	}

	//-------------------------------------------------------------------
	private void openDevelopment() {
		logger.debug("openDevelopment");
		DevelopmentScreenSR5 toShow = new DevelopmentScreenSR5();
		toShow.setData(model);
		manager.show(toShow);
	}

	//-------------------------------------------------------------------
	private void openNotes() {
		logger.info("open Notes dialog");
		NotesScreen toShow = new NotesScreen(control);
		toShow.setData(model);
		manager.show(toShow);
	}

}
