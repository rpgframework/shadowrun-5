/**
 * 
 */
package org.prelle.shadowrun.jfx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.skin.ManagedScreenStructuredSkin;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCharacter.Gender;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventType;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ShadowrunAppearanceScreen extends ManagedScreen {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	private final static String INVALID = "invalid-data";

	private ShadowrunCharacter model;

	private TextField tfName;
	private TextField tfRealName;
	private ChoiceBox<Gender> cbGender;
	private TextField tfSize;
	private TextField tfWeight;
	private TextField tfHair;
	private TextField tfEyes;
	
	private ImageView ivPortrait;
	private Button    btnOpen;
	private Button    btnDel;
	
	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunAppearanceScreen(CharacterController control, ViewMode mode) {
		initComponents();
		initLayout();
		initInteractivity();
		setSkin(new ManagedScreenStructuredSkin(this));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		getNavigButtons().add(CloseType.BACK);
		setTitle(UI.getString("label.appearance"));
		
		tfName = new TextField();		
		tfRealName = new TextField();	
		cbGender = new ChoiceBox<>();
		cbGender.getItems().addAll(Gender.values());
		cbGender.setConverter(new StringConverter<ShadowrunCharacter.Gender>() {
			public String toString(Gender val) {return val.toString();}
			public Gender fromString(String val) {return null;}
		});
		tfSize = new TextField();
		tfWeight = new TextField();
		tfHair = new TextField();
		tfEyes = new TextField();
		
		ivPortrait = new ImageView();
		ivPortrait.setFitHeight(400);
		ivPortrait.setFitWidth(400);
		btnOpen = new Button(null, new FontIcon("\uE227\uE187",32));
		btnDel  = new Button(null, new FontIcon("\uE227\uE107",32));
		btnOpen.setStyle("-fx-padding: 8px; -fx-border-width:0px");
		btnDel .setStyle("-fx-padding: 8px; -fx-border-width:0px");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaName      = new Label(UI.getString("label.streetname"));
		Label heaRealName  = new Label(UI.getString("label.realname"));
		Label heaGender    = new Label(UI.getString("label.gender"));
		Label heaSize      = new Label(UI.getString("label.size"));
		Label heaWeight    = new Label(UI.getString("label.weight"));
		Label heaHair      = new Label(UI.getString("label.hair"));
		Label heaEyes      = new Label(UI.getString("label.eyes"));
		
		HBox sizeLine = new HBox(5);
		sizeLine.getChildren().addAll(tfSize, new Label(UI.getString("label.size.unit")));
		HBox wghtLine = new HBox(5);
		wghtLine.getChildren().addAll(tfWeight, new Label(UI.getString("label.weight.unit")));
		sizeLine.setAlignment(Pos.CENTER_LEFT);
		wghtLine.setAlignment(Pos.CENTER_LEFT);
		
		GridPane grid = new GridPane();
		grid.add(heaName  , 0, 0);
		grid.add( tfName  , 1, 0);
		grid.add(heaRealName, 0, 1);
		grid.add( tfRealName, 1, 1);
		grid.add(heaGender, 0, 2);
		grid.add( cbGender, 1, 2);
		grid.add(heaSize  , 0, 3);
		grid.add( sizeLine, 1, 3);
		grid.add(heaWeight, 0, 4);
		grid.add( wghtLine, 1, 4);
		grid.add(heaHair  , 0, 5);
		grid.add( tfHair  , 1, 5);
		grid.add(heaEyes  , 0, 6);
		grid.add( tfEyes  , 1, 6);

		grid.setHgap(20);
		grid.setVgap(20);
		heaName.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaRealName.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaSize.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaWeight.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaHair.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaEyes.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaGender.setStyle("-fx-text-fill: textcolor-highlight-primary");
		((FontIcon)btnOpen.getGraphic()).getLabel().setStyle("-fx-fill: textcolor-highlight-primary");
		((FontIcon)btnDel.getGraphic()).getLabel().setStyle("-fx-fill: textcolor-highlight-primary");
		
		TilePane buttons = new TilePane(Orientation.HORIZONTAL, 10, 10);
		buttons.getChildren().addAll(btnOpen, btnDel);
		buttons.setAlignment(Pos.CENTER);
		VBox bxPortrait = new VBox(20);
		bxPortrait.getChildren().addAll(ivPortrait, buttons);
		
		HBox content = new HBox(40);
		content.getChildren().addAll(grid, bxPortrait);
		grid.getStyleClass().add("content");
		grid.setStyle("-fx-padding: 1em;");
		HBox.setMargin(grid, new Insets(0,0,20,0));
		
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbGender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> model.setGender(n));
//		if (control instanceof SpliMoCharacterGenerator) {
//			SpliMoCharacterGenerator charGen = (SpliMoCharacterGenerator)control;
//			tfName.textProperty().addListener( (ov,o,n) -> {
//				charGen.setName(tfName.getText());
//				setTitle(model.getName()+" / "+uiResources.getString("label.appearance"));
//				});
//		}
		
		btnDel.setOnAction(event -> {
			model.setImage(null);
			ivPortrait.setImage(null);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null));
		});
		btnOpen.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FileChooser chooser = new FileChooser();
				chooser.setInitialDirectory(new File(System.getProperty("user.home")));
				chooser.getExtensionFilters().addAll(
		                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
		                new FileChooser.ExtensionFilter("PNG", "*.png")
		            );
				File selection = chooser.showOpenDialog(new Stage());
				if (selection!=null) {
					try {
						byte[] imgBytes = Files.readAllBytes(selection.toPath());
						ivPortrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
						model.setImage(imgBytes);
						GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null));
					} catch (IOException e) {
						logger.warn("Failed loading image from "+selection+": "+e);
					}
				}
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#close(org.prelle.javafx.ManagedScreen.CloseType)
	 */
	@Override
	public boolean close(CloseType type) {
		logger.info("close("+type+")");
		model.setName(tfName.getText());
		model.setRealName(tfRealName.getText());
		logger.info("Set real name to "+model.getRealName());
		try {
			model.setSize(Integer.parseInt(tfSize.getText()));
			tfSize.getStyleClass().remove(INVALID);
		} catch (NumberFormatException e) {
			tfSize.getStyleClass().add(INVALID);
			return false;
		}
		try {
			model.setWeight(Integer.parseInt(tfWeight.getText()));
			tfWeight.getStyleClass().remove(INVALID);
		} catch (NumberFormatException e) {
			tfWeight.getStyleClass().add(INVALID);
			return false;
		}
		
		model.setHairColor(tfHair.getText());
		model.setEyeColor(tfEyes.getText());
		
		logger.info("fire event");
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#childClosed(org.prelle.javafx.ManagedScreen, org.prelle.javafx.ManagedScreen.CloseType)
	 */
	@Override
	public void childClosed(ManagedScreen child, CloseType type) {
		logger.info("childClosed("+type+")");
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		setTitle(model.getName()+" / "+UI.getString("label.appearance"));
		
		tfName.setText(model.getName());
		tfRealName.setText(model.getRealName());
		cbGender.setValue(model.getGender());
//		if (model.getRace()!=null)
//			cbRace.getSelectionModel().select(model.getRace());
//		
//		// Culture
//		if (model.getCulture()!=null) {
//			tfCulture.setText(model.getCulture().getName());
//			cbCulture.getSelectionModel().select(model.getCulture());
//		}
		
		tfSize.setText(String.valueOf(model.getSize()));
		tfWeight.setText(String.valueOf(model.getWeight()));
		tfHair.setText(model.getHairColor());
		tfEyes.setText(model.getEyeColor());
		
		if (model.getImage()!=null) {
			logger.debug("Found image with "+model.getImage().length+" bytes");
			ivPortrait.setImage(new Image(new ByteArrayInputStream(model.getImage())));
		} else {
			ivPortrait.setImage(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/guest-256.png")));
		}
	}

}
