/**
 *
 */
package org.prelle.shadowrun.jfx;

import java.lang.reflect.Constructor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.CharacterGenerator;

/**
 * @author prelle
 *
 */
public class SinglePageWizard<T extends WizardPage> extends Wizard {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private T page;

	//-------------------------------------------------------------------
	/**
	 * @param nodes
	 */
	public SinglePageWizard(ShadowrunCharacter model, CharacterGenerator charGen, Class<T> cls) {
		getNavigButtons().clear();
		getNavigButtons().addAll(
				CloseType.CANCEL,
				CloseType.OK
				);

		try {
			Constructor<T> cons = cls.getConstructor(Wizard.class, CharacterGenerator.class);
			page = cons.newInstance(this, charGen);

			getPages().addAll(page);

			setOnAction(CloseType.OK, event -> {
//				page.pageLeft(CloseType.OK);
				this.impl_getBehaviour().finish();
			});

		} catch (Exception e) {
			logger.fatal("Failed creating wizard page",e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Wizard#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the page
	 */
	public T getPage() {
		return page;
	}

}
