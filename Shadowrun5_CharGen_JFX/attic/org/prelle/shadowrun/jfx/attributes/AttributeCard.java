/**
 * 
 */
package org.prelle.shadowrun.jfx.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.Attribute;
import org.prelle.shadowrun.AttributeValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.gen.event.GenerationEventType;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributeCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Attribute[] attributes;
	private Label headAttr, headValue;
	private Map<Attribute, Label> finalValue;

	//-------------------------------------------------------------------
	/**
	 */
	public AttributeCard(Attribute[] attribs) {
		attributes = attribs;
		finalValue = new HashMap<Attribute, Label>();
		
		getStyleClass().addAll("dark-background","table");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headValue  = new Label(UI.getString("label.value"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headValue.getStyleClass().add("table-heading");
		
		headValue.setAlignment(Pos.CENTER);
		
		for (final Attribute attr : attributes) {
			Label finVal= new Label();
			finalValue  .put(attr, finVal);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);
		this.add(headValue , 1,0);
		
		int y=0;
		for (final Attribute attr : attributes) {
			y++;
			Label longName  = new Label(attr.getName());
			Label finVal    = finalValue.get(attr);
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			longName.getStyleClass().addAll(lineStyle);
			finVal.getStyleClass().add(lineStyle);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setAlignment(Pos.CENTER);
			
			this.add(longName , 0, y);
			this.add(  finalValue.get(attr), 1, y);
			
			
		}
		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		constraints.add(maxGrow);
		constraints.add(centerAlign);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()!=GenerationEventType.ATTRIBUTE_CHANGED)
			return;
		
		updateContent();
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		for (Attribute attr : attributes) {
			Label final_l = finalValue.get(attr);
			AttributeValue data = model.getAttribute(attr);
			final_l.setText(String.valueOf(data.getModifiedValue()));
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
