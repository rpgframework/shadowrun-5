/**
 *
 */
package org.prelle.shadowrun.jfx.attributes;

import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.NodeWithTitleSkeleton;
import org.prelle.shadowrun.Attribute;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.charctrl.AttributeController;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.event.EventTarget;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public class AttributeScreen extends NodeWithTitleSkeleton implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	private PropertyResourceBundle DESCRIPTIONS;

	private CharacterController control;

	private AttributePanePrimary primary;
	private AttributePaneSecondary secondary;
	private Label focusAttributeName;
	private Label focusAttributeDescription;
	private Attribute focussed;
	private TilePane flow;

	private PointsPane points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public AttributeScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();
		DESCRIPTIONS = ShadowrunCore.getI18nHelpResources();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.attributes"));
		flow = new TilePane(Orientation.VERTICAL);

		focusAttributeName = new Label();
		focusAttributeName.getStyleClass().add("section-head");
		focusAttributeName.setWrapText(true);

		focusAttributeDescription = new Label();
		focusAttributeDescription.setWrapText(true);
		focusAttributeDescription.getStyleClass().add("text-body");
		focusAttributeDescription.setMaxHeight(Double.MAX_VALUE);
		focusAttributeDescription.setTextAlignment(TextAlignment.JUSTIFY);
		focusAttributeDescription.setAlignment(Pos.TOP_LEFT);

		primary = new AttributePanePrimary(control.getAttributeController(), control.getMode());
		secondary = new AttributePaneSecondary(control.getAttributeController(), control.getMode());

		/*
		 * Exp & Co.
		 */
		points = new PointsPane(control, AttributeController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox focusAttr = new VBox();
		focusAttr.setSpacing(20);
		focusAttr.getChildren().addAll(focusAttributeName, focusAttributeDescription);
		focusAttr.getStyleClass().add("content");

		flow.setPrefTileWidth(400);
		flow.setHgap(20);
		flow.setVgap(20);
		flow.getChildren().addAll(primary, secondary, focusAttr);

//		ScrollPane scroll = new ScrollPane(flow);
//		scroll.setFitToHeight(true);
//		scroll.setMaxHeight(Double.MAX_VALUE);
//		scroll.setMaxWidth(Double.MAX_VALUE);

		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, flow);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(flow, new Insets(0,0,20,0));
		HBox.setHgrow(flow, Priority.ALWAYS);
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		primary.setOnMouseEntered(event -> mouseFocusChanged(event));
		primary.setOnMouseMoved(event -> mouseFocusChanged(event));
		secondary.setOnMouseEntered(event -> mouseFocusChanged(event));
		secondary.setOnMouseMoved(event -> mouseFocusChanged(event));
	}

	//-------------------------------------------------------------------
	private void mouseFocusChanged(MouseEvent event) {
		EventTarget target = event.getTarget();
		if ((target instanceof Label) && (((Label)target).getUserData() instanceof Attribute)) {
			Attribute newAttr = (Attribute) ((Label)target).getUserData();
			if (newAttr==focussed)
				return;
//			logger.debug("Focus changes to "+newAttr);
			focusAttributeName.setText(newAttr.getName());
			String key = "attribute."+newAttr.name().toLowerCase()+".desc";
			if (DESCRIPTIONS.containsKey(key))
				focusAttributeDescription.setText(DESCRIPTIONS.getString(key));
			else {
				logger.error("Missing key '"+key+"' in core_help.properties");
				focusAttributeDescription.setText("Missing key "+key);
			}
			focussed = newAttr;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case CHARACTER_CHANGED:
		case POINTS_LEFT_ATTRIBUTES:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.attributes"));
		primary.setData(model);
		secondary.setData(model);
		focusAttributeName.setText(Attribute.CHARISMA.getName());
		String key = "attribute."+Attribute.CHARISMA.name().toLowerCase()+".desc";
		try {
			focusAttributeDescription.setText(DESCRIPTIONS.getString(key));
		} catch (MissingResourceException e) {
			logger.error("Missing property "+key+" in "+DESCRIPTIONS.getBaseBundleName());
		}

		points.setData(model);
		points.refresh();
	}

}
