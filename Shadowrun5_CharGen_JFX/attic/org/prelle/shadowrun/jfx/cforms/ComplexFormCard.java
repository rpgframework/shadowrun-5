/**
 *
 */
package org.prelle.shadowrun.jfx.cforms;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun.ComplexFormValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ComplexFormCard extends VBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headAttr;

	//-------------------------------------------------------------------
	/**
	 */
	public ComplexFormCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");

		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		headAttr.getStyleClass().add("table-heading");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getChildren().add(headAttr);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case COMPLEX_FORM_ADDED:
		case COMPLEX_FORM_CHANGED:
		case COMPLEX_FORM_REMOVED:
			updateContent();
			break;
		default:
		}

	}

	//-------------------------------------------------------------------
	private void updateContent() {
		if (model==null) {
			logger.error("No model set");
			return;
		}
		this.getChildren().retainAll(headAttr);

		int y=0;
		for (ComplexFormValue item : model.getComplexForms()) {
			y++;

			Label lblName  = new Label(item.getName());

			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

			getChildren().add(lblName);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
