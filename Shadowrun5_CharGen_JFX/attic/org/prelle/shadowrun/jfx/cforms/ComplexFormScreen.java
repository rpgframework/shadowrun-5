/**
 *
 */
package org.prelle.shadowrun.jfx.cforms;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.ComplexFormController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class ComplexFormScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private ComplexFormSelectionPane primary;

	private PointsPane points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public ComplexFormScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.complexforms"));

		primary = new ComplexFormSelectionPane(control.getComplexFormController(), this);

		/*
		 * Exp & Co.
		 */
		points = new PointsPane(control, ComplexFormController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, primary);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(primary, new Insets(0,0,20,0));
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
		case EXPERIENCE_CHANGED:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.complexforms"));
		primary.setData(model);
		points.setData(model);
		points.refresh();
	}

}
