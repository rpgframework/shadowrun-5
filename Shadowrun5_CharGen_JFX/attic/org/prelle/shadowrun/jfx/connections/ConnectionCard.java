/**
 * 
 */
package org.prelle.shadowrun.jfx.connections;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.Connection;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class ConnectionCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headName, headInfl, headLoyl;

	//-------------------------------------------------------------------
	/**
	 */
	public ConnectionCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headName  = new Label(UI.getString("label.name"));
		headInfl  = new Label(UI.getString("label.influence.short"));
		headLoyl  = new Label(UI.getString("label.loyalty.short"));
		headName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headInfl.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headLoyl.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headName.getStyleClass().add("table-heading");
		headInfl.getStyleClass().add("table-heading");
		headLoyl.getStyleClass().add("table-heading");
		
		headInfl.setAlignment(Pos.CENTER_RIGHT);
		headLoyl.setAlignment(Pos.CENTER_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headName  , 0,0);
		this.add(headInfl , 1,0);
		this.add(headLoyl  , 2,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		ColumnConstraints centerAlign2 = new ColumnConstraints();
		centerAlign2.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
		constraints.add(centerAlign2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONNECTION_ADDED:
		case CONNECTION_CHANGED:
		case CONNECTION_REMOVED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headName, headInfl, headLoyl);
		
		int y=0;
		for (Connection con : model.getConnections()) {
			y++;

			Label lblName  = new Label(con.getType());
			Label lblInfl  = new Label(String.valueOf(con.getInfluence()));
			Label lblLoyl  = new Label(String.valueOf(con.getLoyalty()));
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblInfl.getStyleClass().add(lineStyle);
			lblLoyl.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblInfl, 1, y);
			this.add(lblLoyl, 2, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
