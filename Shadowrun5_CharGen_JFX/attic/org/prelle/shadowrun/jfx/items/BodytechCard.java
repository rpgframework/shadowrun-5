/**
 * 
 */
package org.prelle.shadowrun.jfx.items;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.CarriedItem;
import org.prelle.shadowrun.items.ItemType;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class BodytechCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headAttr, headValue;

	//-------------------------------------------------------------------
	/**
	 */
	public BodytechCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headValue  = new Label(UI.getString("label.value"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headValue.getStyleClass().add("table-heading");
		
		headValue.setAlignment(Pos.CENTER_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);
		this.add(headValue , 1,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POWER_ADDED:
		case POWER_CHANGED:
		case POWER_REMOVED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headAttr, headValue);
		
		ItemType[] filter = new ItemType[]{
				ItemType.BIOWARE,
				ItemType.CYBERWARE,
				ItemType.NANOWARE
		};
		int y=0;
		for (CarriedItem item : model.getItems(false, filter)) {
			y++;

			Label lblName  = new Label(item.getName());
			Label lblVal   = new Label("?");
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblVal.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblVal , 1, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
