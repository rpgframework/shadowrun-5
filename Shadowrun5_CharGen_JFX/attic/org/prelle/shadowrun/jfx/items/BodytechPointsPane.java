/**
 * 
 */
package org.prelle.shadowrun.jfx.items;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import org.prelle.shadowrun.charctrl.CharGenMode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.EquipmentController;
import org.prelle.shadowrun.gen.EquipmentGenerator;

/**
 * @author prelle
 *
 */
public class BodytechPointsPane extends EquipmentPointsPane {
	
	private EquipmentController equipCtrl;
	private Label lblEssRemain;
	private Label lblEssInvest;
	private Label lblEssHole;

	//-------------------------------------------------------------------
	public BodytechPointsPane(CharacterController ctrl, Class<?> type) {
		super(ctrl, type);
	
		if (ctrl.getMode()==CharGenMode.CREATING) {
			equipCtrl = ctrl.getEquipmentController();
		}
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		super.initComponents();
		
		lblEssRemain = new Label("-");
		lblEssInvest = new Label("-");
		lblEssHole   = new Label("-");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		super.initLayout();
		
			Label heaEssRemain = new Label(UI.getString("label.essence.remain"));
			Label heaEssInvest = new Label(UI.getString("label.essence.invest"));
			Label heaEssHole   = new Label(UI.getString("label.essence.hole"));

			GridPane grid = new GridPane();
			grid.setStyle("-fx-hgap: 0.5em");
			grid.add(heaEssRemain, 0, 0);
			grid.add(lblEssRemain, 1, 0);
			grid.add(heaEssInvest, 0, 1);
			grid.add(lblEssInvest, 1, 1);
			grid.add(heaEssHole  , 0, 2);
			grid.add(lblEssHole  , 1, 2);
			grid.setAlignment(Pos.CENTER);

			getChildren().addAll(grid);
			VBox.setMargin(grid, new Insets(20,0,10,0));
			this.setAlignment(Pos.TOP_CENTER);
	}

	//-------------------------------------------------------------------
	public void refresh(float essInvest, float essence) {
		super.refresh();
		
		float hole = 6.0f - essInvest - essence;
		logger.debug("hole = 6.0 - "+essInvest+" - "+essence+" = "+hole);
		lblEssRemain.setText(String.format("%.2f", essence));
		lblEssInvest.setText(String.format("%.2f", essInvest));
		lblEssHole  .setText(String.format("%.2f", hole));
	}

	//-------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		
		refresh(0, model.getUnusedEssence());
	}

}
