/**
 *
 */
package org.prelle.shadowrun.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.EquipmentController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.ItemType;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class MagicalEquipmentScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private CommonEquipmentSelectionPane primary;

	private PointsPane points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public MagicalEquipmentScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.magical_equipment"));

		primary = new CommonEquipmentSelectionPane(control.getEquipmentController(), this, ItemType.MAGICAL);

		/*
		 * Exp & Co.
		 */
		points = new EquipmentPointsPane(control, EquipmentController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, primary);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(primary, new Insets(0,0,20,0));
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case NUYEN_CHANGED:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
		GenerationEventDispatcher.removeListener(primary);
		primary.removeListener();
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.magical_equipment"));
		primary.setData(model);
		points.setData(model);
		points.refresh();
	}

}
