/**
 *
 */
package org.prelle.shadowrun.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.ItemType;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.fluent.SR5FirstLine;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class VehicleScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private CommonEquipmentSelectionPane primary;

	private SR5FirstLine firstLine;
	private FreePointsNode freePoints;

	//-------------------------------------------------------------------
	/**
	 */
	public VehicleScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.vehicles_drones"));

		primary = new CommonEquipmentSelectionPane(control.getEquipmentController(), this, ItemType.vehicleTypes());

		/*
		 * Exp & Co.
		 */
		firstLine  = new SR5FirstLine();
		freePoints = new FreePointsNode();
		freePoints.setStyle("-fx-max-height: 6em; -fx-max-width: 6em;  -fx-min-width: 5em");
		freePoints.setPoints(control.getCharacter().getNuyen());
		freePoints.setName(UI.getString("label.nuyen"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox flow = new HBox();
		flow.getChildren().addAll(freePoints, primary);
		flow.setStyle("-fx-spacing: 1em");

		VBox content = new VBox();
		content.setSpacing(20);
		content.getChildren().addAll(firstLine, flow);
		VBox.setVgrow(flow, Priority.ALWAYS);
		VBox.setMargin(flow  , new Insets(0,0,20,0));
		setContent(content);
		content.getStyleClass().add("gear-screen");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case NUYEN_CHANGED:
			logger.debug("rcv "+event.getType());
			freePoints.setPoints(control.getCharacter().getNuyen());
			firstLine.setData(control.getCharacter());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
		GenerationEventDispatcher.removeListener(primary);
		primary.removeListener();
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.vehicles_drones"));
		primary.setData(model);
		freePoints.setPoints(control.getCharacter().getNuyen());
		firstLine.setData(control.getCharacter());
	}

}
