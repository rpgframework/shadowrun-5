/**
 *
 */
package org.prelle.shadowrun.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.EquipmentController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.CarriedItem;
import org.prelle.shadowrun.items.ItemTemplate;
import org.prelle.shadowrun.items.ItemType;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WeaponScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private  ListView<CarriedItem> primary;

	private PointsPane points;
	private HBox content;

	private MenuItem btnAdd;
	private Button btnEdit;
	private Button btnDelete;

	//-------------------------------------------------------------------
	/**
	 */
	public WeaponScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		btnDelete.setVisible(false);
		btnEdit.setVisible(false);

	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.gear"));

		primary = new ListView<CarriedItem>();
		primary.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {
			public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
				return new BigCarriedItemListCell(control.getEquipmentController(), primary, WeaponScreen.this);
			}
		});

		FontIcon icoAd   = new FontIcon("\uE17E\uE109");
		FontIcon icoEdit = new FontIcon("\uE17E\uE104");
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		icoAd.getStyleClass().add("action-area-icon");
		icoEdit.getStyleClass().add("action-area-icon");
		icoDelete.getStyleClass().add("action-area-icon");
		btnAdd    = new MenuItem(null, icoAd);
		btnEdit   = new Button(null, icoEdit);
		btnDelete = new Button(null, icoDelete);
//		btnAdd.setTooltip(new Tooltip(UI.getString("tooltip.weapons.add")));
		btnDelete.setTooltip(new Tooltip(UI.getString("tooltip.weapons.delete")));
		btnEdit.setTooltip(new Tooltip(UI.getString("tooltip.weapons.edit")));
		getStaticButtons().addAll(btnAdd);
		getContextButtons().addAll(btnEdit, btnDelete);


		/*
		 * Exp & Co.
		 */
		points = new EquipmentPointsPane(control, EquipmentController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		primary.setStyle("-fx-min-width: 55em; -fx-pref-width: 65em");
		primary.setMaxHeight(Double.MAX_VALUE);

		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, primary);
		HBox.setMargin(points , new Insets(0,0,20,0));
		HBox.setMargin(primary, new Insets(0,0,20,0));
//		HBox.setHgrow(primary, Priority.SOMETIMES);
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAdd.setOnAction( event -> openAddDialog());
		btnEdit.setOnAction( event -> openEditDialog());
		btnDelete.setOnAction( event -> openDeleteDialog());

		primary.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDelete.setVisible(n!=null);
			btnEdit.setVisible(n!=null);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case NUYEN_CHANGED:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		case EQUIPMENT_ADDED:
		case EQUIPMENT_CHANGED:
		case EQUIPMENT_REMOVED:
			logger.debug("rcv "+event.getType());
			primary.getItems().clear();
			primary.getItems().addAll(control.getCharacter().getItemsRecursive(false, ItemType.WEAPON));
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("screen.weapons.title"));

		primary.getItems().clear();
		primary.getItems().addAll(model.getItemsRecursive(false, ItemType.WEAPON));
		points.setData(model);
		points.refresh();
	}

	//-------------------------------------------------------------------
	private void openAddDialog() {
		logger.debug("User clicked add button");
		EquipmentSelector pane = new EquipmentSelector(control.getEquipmentController(), ItemType.WEAPON);

		CloseType result = getManager().showAlertAndCall(AlertType.QUESTION, "Choose", pane);
		if (result==CloseType.OK) {
			ItemTemplate item = pane.getTemplate();
			CarriedItem citem = control.getEquipmentController().select(item);
			if (citem==null) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, String.format(UI.getString("screen.weapons.error.cannot_add"), item.getName()));
				return;
			}
			if (pane.getRating()>0)
				citem.setRating(pane.getRating());
			// Is added to list by EQUIPMENT_ADDED event
		}
	}

	//-------------------------------------------------------------------
	private void openDeleteDialog() {
		logger.debug("User clicked delete button");

		CarriedItem item = primary.getSelectionModel().getSelectedItem();
		String content = String.format(UI.getString("screen.weapons.confirmDelete"), item.getName());

		CloseType result = manager.showAlertAndCall(AlertType.CONFIRMATION, UI.getString("label.confirmation"), content);
		if (result==CloseType.YES) {
			control.getEquipmentController().deselect(item);
			// Is removed from list by EQUIPMENT_REMOVED event
		}
	}

	//-------------------------------------------------------------------
	private void openEditDialog() {
		logger.debug("User clicked edit button");
		logger.warn("TODO: Edit");

		String mess = UI.getString("screen.weapons.error.not_possible_yet");
		String title= UI.getString("label.error.title");

		EditCarriedItemPane pane = new EditCarriedItemPane(control.getEquipmentController(), this);
		pane.setData(primary.getSelectionModel().getSelectedItem());


		manager.showAlertAndCall(AlertType.NOTIFICATION, title, pane);
	}

}
