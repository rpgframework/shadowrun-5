/**
 * 
 */
package org.prelle.shadowrun.jfx.matrix;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.ProgramValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunTools;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.CarriedItem;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class MatrixCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headAttr;

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		ColumnConstraints centerAlign2 = new ColumnConstraints();
		centerAlign2.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
		constraints.add(centerAlign2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EQUIPMENT_CHANGED:
			CarriedItem item = (CarriedItem) event.getKey();
//			if (item.getItem().getType()==)
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headAttr);
		
		int y=0;
		if (model==null)
			return;
		for (CarriedItem item : ShadowrunTools.getMatrixItems(model)) {
			y++;

			Label lblName  = new Label(item.getName());
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblName.setStyle("-fx-font-weight: bold");
			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y, 2,1);
			
			for (ProgramValue prog : item.getPrograms()) {
				y++;
				lineStyle = ((y%2)==0)?"even":"odd";
				lblName  = new Label("- "+prog.getName());
				lblName.getStyleClass().addAll(lineStyle);
				lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
				this.add(lblName, 0, y, 2,1);
			}
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
