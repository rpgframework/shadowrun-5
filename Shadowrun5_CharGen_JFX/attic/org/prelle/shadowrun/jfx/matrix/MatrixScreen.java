/**
 *
 */
package org.prelle.shadowrun.jfx.matrix;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun.Program;
import org.prelle.shadowrun.Program.ProgramType;
import org.prelle.shadowrun.ProgramValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.ShadowrunTools;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.ProgramController;
import org.prelle.shadowrun.charctrl.SkillController;
import org.prelle.shadowrun.common.ProgramControllerImpl;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.items.CarriedItem;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class MatrixScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private VBox devicePane;
	private ChoiceBox<CarriedItem> cbMatrixDevices;
	private ThreeColumnPane pane;
	private ChoiceBox<ProgramType> cbType;
	private ListView<Program> lvAvailable;
	private ListView<ProgramValue> lvSelected;
	private Label descHeading;
	private Label descRef;
//	private Label descFeat;
//	private Label descType, descRange, descDur, descDrain;
	private Label description;

	private PointsPane points;
	private HBox content;
	private ProgramController itemControl;

	//-------------------------------------------------------------------
	/**
	 */
	public MatrixScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("screen.matrix.title"));

		devicePane = new VBox(15);
		cbMatrixDevices = new ChoiceBox<>();
		cbMatrixDevices.setConverter(new StringConverter<CarriedItem>() {
			public String toString(CarriedItem object) { return object.getName(); }
			public CarriedItem fromString(String string) { return null; }
		});

		cbType = new ChoiceBox<ProgramType>();
		cbType.getItems().addAll(ProgramType.STANDARD, ProgramType.HACKING);
		cbType.setValue(ProgramType.STANDARD);
		cbType.setConverter(new StringConverter<Program.ProgramType>() {
			public String toString(ProgramType val) {return val.getName();}
			public ProgramType fromString(String string) {return null;}
		});
		lvAvailable = new ListView<Program>();
		lvAvailable.setCellFactory(new Callback<ListView<Program>, ListCell<Program>>() {
			public ListCell<Program> call(ListView<Program> param) {
				return new ProgramListCell(MatrixScreen.this);
			}
		});
		lvAvailable.setStyle("-fx-pref-width: 25em");
		lvSelected = new ListView<ProgramValue>();
		lvSelected.setCellFactory(new Callback<ListView<ProgramValue>, ListCell<ProgramValue>>() {
			public ListCell<ProgramValue> call(ListView<ProgramValue> param) {
				return new ProgramValueListCell();
			}
		});
		lvSelected.setStyle("-fx-pref-width: 30em");

		pane = new ThreeColumnPane();
		pane.setHeadersVisible(true);
		pane.setColumn1Header(UI.getString("label.available"));
		pane.setColumn2Header(UI.getString("label.selected"));
		pane.setColumn3Header(UI.getString("label.description"));

		Label phAvailable = new Label(UI.getString("matrixpane.placeholder.available"));
		Label phSelected  = new Label(UI.getString("matrixpane.placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);

		/* Deescription */
		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
//		descFeat    = new Label();
//		descType    = new Label();
//		descRange   = new Label();
//		descDur     = new Label();
//		descDrain   = new Label();

		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");


		/*
		 * Exp & Co.
		 */
		points = new PointsPane(control, SkillController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbMatrixDevices = new Label(UI.getString("matrixpane.select_device"));
		devicePane.getChildren().addAll(lbMatrixDevices, cbMatrixDevices);

		VBox column1 = new VBox(10);
		column1.getChildren().addAll(cbType, lvAvailable);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);

		VBox column3 = new VBox(10);
		column3.getChildren().addAll(descHeading, descRef, description);
		VBox.setVgrow(description, Priority.ALWAYS);
		pane.setColumn1Node(column1);
		pane.setColumn2Node(lvSelected);
		pane.setColumn3Node(column3);

		VBox realContent = new VBox(20);
		realContent.getChildren().addAll(devicePane, pane);
		VBox.setVgrow(pane, Priority.ALWAYS);
		pane.setMaxHeight(Double.MAX_VALUE);

		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, realContent);
		HBox.setMargin(points, new Insets(0,0,20,0));
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refreshLists());
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n.getModifyable()));
		lvAvailable.setOnDragDropped(event -> dragDroppedAvailable(event));
		lvSelected.setOnDragDropped(event -> dragDroppedSelected(event));
		lvAvailable.setOnDragOver(event -> dragOver(event));
		lvSelected.setOnDragOver(event -> dragOver(event));
		cbMatrixDevices.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> selectedDevice(n));
	}

	//--------------------------------------------------------------------
	private void describe(Program data) {
		if (data==null)
			return;
		descHeading.setText(data.getName().toUpperCase());
		descRef.setText(data.getProductName()+" "+data.getPage());
//		descFeat.setText(SpellListCell.makeFeatureString(data));
//		descType.setText(data.getType().getName());
//		descDur .setText(data.getDuration().getName());
//		if (data.getRange()==null) {
//			System.err.println("No range for "+data);
//		} else {
//		descRange.setText(data.getRange().getName());
//		}
//		if (data.getDrain()<0)
//			descDrain.setText(CORE.getString("label.force")+" "+data.getDrain());
//		else if (data.getDrain()>0)
//			descDrain.setText(CORE.getString("label.force")+" +"+data.getDrain());
//		else
//			descDrain.setText(CORE.getString("label.force"));

		description.setText(data.getHelpText());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case NUYEN_CHANGED:
		case EQUIPMENT_ADDED:
		case EQUIPMENT_REMOVED:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh "+cbType.getValue());
//		devicePane.getChildren().clear();

		cbMatrixDevices.getItems().clear();
		cbMatrixDevices.getItems().addAll(ShadowrunTools.getMatrixItems(control.getCharacter()));
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("screen.matrix.title"));
//		pane.setData(model);
		refresh();
		points.setData(model);
		points.refresh();

		if (!ShadowrunTools.getMatrixItems(model).isEmpty()) {
			cbMatrixDevices.getSelectionModel().select(0);
		}
	}

	//-------------------------------------------------------------------
	private void refreshLists() {
		lvAvailable.getItems().clear();
		if (itemControl!=null)
			lvAvailable.getItems().addAll(itemControl.getAvailable(cbType.getValue()));

		lvSelected.getItems().clear();
		if (itemControl!=null)
			lvSelected.getItems().addAll(itemControl.getMatrixItem().getPrograms());
	}

	//-------------------------------------------------------------------
	private void selectedDevice(CarriedItem item) {
		itemControl = (item!=null)?(new ProgramControllerImpl(control.getCharacter(), item)):null;
		logger.debug("Selected device "+item);
		refreshLists();
	}

	//-------------------------------------------------------------------
	public ProgramController getProgramController() {
		return itemControl;
	}

	//-------------------------------------------------------------------
	private void dragDroppedAvailable(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("program")) {
					Program data = ShadowrunCore.getProgram(tail);
					if (data==null) {
						logger.warn("Cannot find program for dropped id '"+tail+"'");
					} else {
						for (ProgramValue val : itemControl.getMatrixItem().getPrograms()) {
							if (val.getModifyable()==data) {
								itemControl.deselect(val);
								refreshLists();

							}
						}
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("program")) {
					Program data = ShadowrunCore.getProgram(tail);
					if (data==null) {
						logger.warn("Cannot find program for dropped id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();

						Platform.runLater(new Runnable() {
							public void run() {
								itemControl.select(data);
								refreshLists();
							}
						});
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

}
