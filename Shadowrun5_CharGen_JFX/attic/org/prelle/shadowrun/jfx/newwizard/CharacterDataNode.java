/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

/**
 * @author prelle
 *
 */
public interface CharacterDataNode {

	public void refresh();
	
}
