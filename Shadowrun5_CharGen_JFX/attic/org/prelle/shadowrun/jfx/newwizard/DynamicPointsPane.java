/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import java.util.Arrays;
import java.util.List;

import org.prelle.shadowrun.charctrl.CharacterController;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class DynamicPointsPane extends VBox implements CharacterDataNode {
	
	public enum Item {
		SPECIAL_ATTRIBUTES,
		ATTRIBUTES,
		SKILLGROUPS,
		SKILLS,
		KNOWLEDGE_LANG,
		SPELLS,
		POWER_POINTS,
		NUYEN,
	}
	
	private CharacterController charGen;
	private Label valKarma;
	private Label valSpecial;
	private Label valAttr;
	private Label valSkillGrp;
	private Label valSkill;
	private Label valKnowl;

	//-------------------------------------------------------------------
	public DynamicPointsPane(CharacterController charGen, Item...items) {
		List<Item> enabled = Arrays.asList(items);
		this.charGen = charGen;
		valKarma    = new Label();
		valSpecial  = new Label();
		valAttr     = new Label();
		valSkillGrp = new Label();
		valSkill    = new Label();
		valKnowl    = new Label();
		
		getChildren().add(valKarma);
		if (enabled.contains(Item.SPECIAL_ATTRIBUTES))
			getChildren().add(valSpecial);
		if (enabled.contains(Item.ATTRIBUTES))
			getChildren().add(valAttr);
		if (enabled.contains(Item.SKILLGROUPS))
			getChildren().add(valSkillGrp);
		if (enabled.contains(Item.SKILLS))
			getChildren().add(valSkill);
		if (enabled.contains(Item.KNOWLEDGE_LANG))
			getChildren().add(valKnowl);
		
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		valKarma.setText(charGen.getCharacter().getKarmaFree()+"\nKarma zu verteilen");
		valSpecial.setText(charGen.getAttributeController().getPointsLeftSpecial()+"\nPunkte für Spezialattribute");
		valAttr .setText(charGen.getAttributeController().getPointsLeft()+"\nPunkte für Attribute");
		valSkillGrp.setText(charGen.getSkillController().getPointsLeftSkillGroups()+"\nPunkte für Fertigkeitsgruppen");
		valSkill   .setText(charGen.getSkillController().getPointsLeftSkills()+"\nPunkte für Fertigkeiten");
		valKnowl   .setText(charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage()+"\nPunkte für Sprach- und Wissensfertigkeiten");
	}

}
