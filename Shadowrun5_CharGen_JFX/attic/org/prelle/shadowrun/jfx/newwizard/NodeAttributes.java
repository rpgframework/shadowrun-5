/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import org.prelle.shadowrun.charctrl.CharGenMode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.attributes.AttributePanePrimary2;
import org.prelle.shadowrun.jfx.attributes.AttributePaneSecondary;

import javafx.scene.layout.HBox;

/**
 * @author Stefan
 *
 */
public class NodeAttributes extends HBox implements CharacterDataNode {

	private CharacterController charGen;

	private AttributePanePrimary2 primary;
	private AttributePaneSecondary derived;

	//--------------------------------------------------------------------
	public NodeAttributes(CharacterController charGen) {
		super(20);
		this.charGen = charGen;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		primary = new AttributePanePrimary2(charGen.getAttributeController());
		primary.setData(charGen.getCharacter());
		derived = new AttributePaneSecondary(charGen.getAttributeController(), CharGenMode.CREATING);
		derived.setData(charGen.getCharacter());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		
		getChildren().addAll(primary, derived);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		primary.getStyleClass().add("text-small-secondary");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		primary.updateContent();
		derived.updateContent();
	}

}
