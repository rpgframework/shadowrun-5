/**
 *
 */
package org.prelle.shadowrun.jfx.newwizard;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun.Attribute;
import org.prelle.shadowrun.MagicOrResonanceOption;
import org.prelle.shadowrun.MagicOrResonanceType;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun.gen.CharacterGenerator;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.attributes.AttributeField;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class NodeMagicOrResonance extends HBox implements CharacterDataNode {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private ShadowrunCharacter model;
	private CharacterGenerator charGen;
	private MagicOrResonanceController magicGen;
	private SelectionCallback parent;

	private ListView<MagicOrResonanceOption> morOptList;

	private AttributeField fldEdge;
	private AttributeField fldMagic;
	private AttributeField fldResonance;
	private VBox tileEdge;
	private VBox tileMagic;
	private VBox tileResonance;

	private MagicOrResonanceOption selected;
	private List<MagicOrResonanceOption> lastAvailable;

	private boolean ignoreEvents;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public NodeMagicOrResonance(CharacterGenerator charGen, SelectionCallback parent) {
		super(5);
		this.charGen = charGen;
		this.parent  = parent;
		this.magicGen = charGen.getMagicOrResonanceController();
		if (this.magicGen==null)
			throw new NullPointerException("MagicOrResonance controller not set");
		model = charGen.getCharacter();

		initComponents();
		initLayout();
		initInteractivity();

		morOptList.getSelectionModel().select(0);
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		morOptList = new ListView<MagicOrResonanceOption>();
		morOptList.setCellFactory(new Callback<ListView<MagicOrResonanceOption>, ListCell<MagicOrResonanceOption>>() {
			@Override
			public ListCell<MagicOrResonanceOption> call(ListView<MagicOrResonanceOption> arg0) {
				return new MagicOrResonanceCell();
			}
		});
		lastAvailable = new ArrayList<MagicOrResonanceOption>(magicGen.getAvailable());
		morOptList.getItems().addAll(magicGen.getAvailable());

		/*
		 * Edge
		 */
		fldEdge = new AttributeField();
		fldMagic = new AttributeField();
		fldResonance = new AttributeField();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaEdge = new Label(Attribute.EDGE.getName());
		heaEdge.getStyleClass().add("text-subheader");
		tileEdge = new VBox();
//		tileEdge.getStyleClass().add("bordered");
		tileEdge.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileEdge.getChildren().addAll(heaEdge, fldEdge);

		Label heaMagic = new Label(Attribute.MAGIC.getName());
		heaMagic.getStyleClass().add("text-subheader");
		tileMagic = new VBox();
//		tileMagic.getStyleClass().add("bordered");
		tileMagic.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileMagic.getChildren().addAll(heaMagic, fldMagic);

		Label heaResonance = new Label(Attribute.RESONANCE.getName());
		heaResonance.getStyleClass().add("text-subheader");
		tileResonance = new VBox();
//		tileResonance.getStyleClass().add("bordered");
		tileResonance.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileResonance.getChildren().addAll(heaResonance, fldResonance);

		VBox valueCol = new VBox();
		valueCol.setStyle("-fx-spacing: 2em;");
		valueCol.setMaxHeight(Double.MAX_VALUE);
		valueCol.getChildren().addAll(tileEdge, tileMagic, tileResonance);



		getChildren().addAll(morOptList, valueCol);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		morOptList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));

		fldEdge.setOnDecAction(event -> charGen.getAttributeController().decrease(Attribute.EDGE));
		fldEdge.setOnIncAction(event -> charGen.getAttributeController().increase(Attribute.EDGE));
		fldMagic.setOnDecAction(event -> charGen.getAttributeController().decrease(Attribute.MAGIC));
		fldMagic.setOnIncAction(event -> charGen.getAttributeController().increase(Attribute.MAGIC));
		fldResonance.setOnDecAction(event -> charGen.getAttributeController().decrease(Attribute.RESONANCE));
		fldResonance.setOnIncAction(event -> charGen.getAttributeController().increase(Attribute.RESONANCE));
	}

	//-------------------------------------------------------------------
	private void update(MagicOrResonanceOption option) {
		if (option==null) {
			tileMagic.setDisable(true);
			tileResonance.setDisable(true);
		} else {
			MagicOrResonanceType type = option.getType();

			tileMagic.setDisable(!type.usesMagic());
			tileResonance.setDisable(!type.usesResonance());
		}

		if (parent!=null && option!=null)
			parent.showDescription(this, option.getType());
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MagicOrResonanceOption> property, MagicOrResonanceOption oldRace,
			MagicOrResonanceOption newOpt) {
		logger.debug("Currently display metatype "+newOpt);

		magicGen.select(newOpt);
		update(newOpt);
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.jfx.newwizard.CharacterDataNode#refresh()
	 */
	@Override
	public void refresh() {
		if (ignoreEvents)
			return;

		ignoreEvents = true;
		List<MagicOrResonanceOption> newAvailable = charGen.getMagicOrResonanceController().getAvailable();
		if (!newAvailable.equals(lastAvailable)) {
			// List of available options changed
			morOptList.getItems().clear();
			morOptList.getItems().addAll(newAvailable);
			lastAvailable = new ArrayList<>(newAvailable);
		}
		// Get MagicOrResonanceOption by type
		MagicOrResonanceOption newSelected = null;
		for (MagicOrResonanceOption opt : morOptList.getItems()) {
			if (opt.getType()==charGen.getCharacter().getMagicOrResonanceType()) {
				newSelected = opt;
				break;
			}
		}
		if (selected!=newSelected) {
			morOptList.getSelectionModel().select(newSelected);
			selected = newSelected;
		}


		fldEdge.setText(String.valueOf(model.getAttribute(Attribute.EDGE).getModifiedValue()));
		fldMagic.setText(String.valueOf(model.getAttribute(Attribute.MAGIC).getModifiedValue()));
		fldResonance.setText(String.valueOf(model.getAttribute(Attribute.RESONANCE).getModifiedValue()));
		fldEdge.setDisableDec(!charGen.getAttributeController().canBeDecreased(Attribute.EDGE));
		fldEdge.setDisableInc(!charGen.getAttributeController().canBeIncreased(Attribute.EDGE));
		fldMagic.setDisableDec(!charGen.getAttributeController().canBeDecreased(Attribute.MAGIC));
		fldMagic.setDisableInc(!charGen.getAttributeController().canBeIncreased(Attribute.MAGIC));
		fldResonance.setDisableDec(!charGen.getAttributeController().canBeDecreased(Attribute.RESONANCE));
		fldResonance.setDisableInc(!charGen.getAttributeController().canBeIncreased(Attribute.RESONANCE));
		ignoreEvents = false;
	}

}

class MagicOrResonanceCell extends ListCell<MagicOrResonanceOption> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private VBox box;
	private Label lblHeading;
	private Hyperlink lblHardcopy;

	//--------------------------------------------------------------------
	public MagicOrResonanceCell() {
		lblHeading = new Label();
		lblHardcopy = new Hyperlink();
		box = new VBox(5);
		box.getChildren().addAll(lblHeading, lblHardcopy);

		lblHardcopy.setOnAction(event -> {
			MagicOrResonanceOption hardcopy = (MagicOrResonanceOption) ((Hyperlink)event.getSource()).getUserData();
			logger.warn("TODO: open "+hardcopy);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MagicOrResonanceOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(box);
			lblHeading.setText(item.getType().getName());
			lblHardcopy.setText(item.getType().getProductName()+" "+item.getType().getPage());
			lblHardcopy.setUserData(item);
		}
	}
}