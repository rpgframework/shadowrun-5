/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun.MetaType;
import org.prelle.shadowrun.MetaTypeOption;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.MetatypeController;
import org.prelle.shadowrun.gen.CharacterGenerator;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.ShadowrunJFXUtils;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class NodeMetaType extends HBox {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private MetatypeController charGen;
	private SelectionCallback parent;
	private ShadowrunCharacter model;

	private static Map<MetaType,Image> imageByRace;

	private ImageView iView;
	private ListView<MetaTypeOption> metaList;
	private VBox sideBySide;
	private Region statsAttributes;
	private Region statsPowers;

	//-------------------------------------------------------------------
	static {
		imageByRace = new HashMap<MetaType, Image>();
	}

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public NodeMetaType(CharacterGenerator charGen, SelectionCallback parent) {
		this.charGen = charGen.getMetatypeController();
		this.parent = parent;
		if (this.charGen==null)
			throw new NullPointerException("Metatype controller not set");
		model = charGen.getCharacter();
		
		initComponents();
		initLayout();
		initInteractivity();
		
		metaList.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		iView = new ImageView();
		
		metaList = new ListView<MetaTypeOption>();
		metaList.setCellFactory(new Callback<ListView<MetaTypeOption>, ListCell<MetaTypeOption>>() {
			@Override
			public ListCell<MetaTypeOption> call(ListView<MetaTypeOption> arg0) {
				return new MetatypeListCell();
			}
		});

		metaList.getItems().addAll(charGen.getAvailable());
		
		sideBySide = new VBox(20);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		
		getChildren().addAll(iView, metaList, sideBySide);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		metaList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));		
	}

	//-------------------------------------------------------------------
	private void update(MetaTypeOption option) {
		MetaType newRace = option.getType();
		if (newRace.getVariantOf()!=null)
			newRace = newRace.getVariantOf();
		Image img = imageByRace.get(newRace);
		if (img==null) {
			String fname = "images/shadowrun/race_"+newRace.getKey()+".png";
			logger.trace("Load "+fname);
			InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
				imageByRace.put(newRace, img);
			} else
				logger.warn("Missing image at "+fname);
		}
		iView.setImage(img);
		newRace = option.getType();
		
		try {
			// Remove old entries
			sideBySide.getChildren().clear();
			// Create new panes
			statsAttributes = ShadowrunJFXUtils.getAttributeModPane(newRace);
			statsPowers     = ShadowrunJFXUtils.getOtherModPane(newRace);
			// Style them
			statsAttributes.getStyleClass().addAll("text-body","stats-block");
			statsPowers.getStyleClass().addAll("text-body","stats-block");
			// Layout them
			statsAttributes.setMinWidth(140);
			statsPowers.setMinWidth(240);
			// Add them
			sideBySide.getChildren().add(statsAttributes);
			sideBySide.getChildren().add(statsPowers);
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MetaTypeOption> property, MetaTypeOption oldRace,
			MetaTypeOption newRace) {
		logger.debug("Currently display metatype "+newRace);

		if (newRace!=null) {
			charGen.select(newRace.getType());
			update(newRace);
			parent.showDescription(this, newRace.getType());
		}
	}

}

class MetatypeListCell extends ListCell<MetaTypeOption> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);
	
	private HBox layout;
	private Label lblKarma;
	private ImageView ivMetatype;
	private Label lblName;
	
	//--------------------------------------------------------------------
	public MetatypeListCell() {
		lblKarma = new Label();
		lblKarma.getStyleClass().add("text-subheader");
		lblName  = new Label();
		lblName.getStyleClass().add("text-small-subheader");
		ivMetatype = new ImageView();
		ivMetatype.setFitHeight(64);
		ivMetatype.setFitWidth(64);
		
		layout = new HBox(5);
		layout.getChildren().addAll(ivMetatype, lblName, lblKarma);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetaTypeOption item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setGraphic(null);
			ivMetatype.setImage(null);
		} else {
			setGraphic(layout);
			lblName.setText(item.getType().getName());
			lblKarma.setText(""+item.getAdditionalKarmaKost());
			
			String metaID = (item.getType().getVariantOf()!=null)?item.getType().getVariantOf().getId():item.getType().getId();
			String fName = "images/metaicon_"+metaID+"_rike.png";
			InputStream in = ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/"+fName);
			if (in==null) {
				logger.warn("Missing "+fName);
			} else {
				Image img = new Image(in);
				ivMetatype.setImage(img);
			}
		}
	}
}