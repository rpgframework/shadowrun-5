/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.BasePluginData;
import org.prelle.shadowrun.charctrl.CharGenMode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.qualities.QualitySelectionPane;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class NodeQualities extends HBox implements CharacterDataNode {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController charGen;
	private SelectionCallback parent;
	private ScreenManagerProvider provider;

	private QualitySelectionPane qualityPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	//--------------------------------------------------------------------
	public NodeQualities(CharacterController charGen, SelectionCallback parent, ScreenManagerProvider mgmr) {
		super(20);
		this.charGen = charGen;
		this.parent  = parent;
		this.provider = mgmr;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		qualityPane = new QualitySelectionPane(charGen.getQualityController(), provider, CharGenMode.CREATING, true);
		qualityPane.setData(charGen.getCharacter());
		qualityPane.setSelectionCallback(new SelectionCallback() {
			public void showDescription(Node node, BasePluginData value) {
				parent.showDescription(NodeQualities.this, value);
			}
		});
		
		imgRec = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectQualities.instruct"));
		instruction.setWrapText(true);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);
		
		VBox layout = new VBox(5);
		layout.getChildren().addAll( qualityPane);
		getChildren().add(layout);
		VBox.setVgrow(layout, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.jfx.newwizard.CharacterDataNode#refresh()
	 */
	@Override
	public void refresh() {
		qualityPane.refresh();
	}

}
