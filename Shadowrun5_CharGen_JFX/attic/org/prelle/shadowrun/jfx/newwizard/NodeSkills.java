/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.BasePluginData;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.skills.SkillSelectionPane;

import javafx.scene.Node;
import javafx.scene.layout.HBox;

/**
 * @author Stefan
 *
 */
public class NodeSkills extends HBox implements CharacterDataNode {

	private CharacterController charGen;
	private SelectionCallback parent;
	private ScreenManagerProvider provider;

	private SkillSelectionPane primary;

	//--------------------------------------------------------------------
	public NodeSkills(CharacterController charGen, SelectionCallback parent, ScreenManagerProvider provider) {
		super(20);
		this.charGen = charGen;
		this.parent  = parent;
		this.provider= provider;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		primary = new SkillSelectionPane(charGen.getSkillController(), provider, new SelectionCallback() {
			public void showDescription(Node node, BasePluginData value) {
				parent.showDescription(NodeSkills.this, value);
			}
		});
		primary.setData(charGen.getCharacter());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		
		getChildren().addAll(primary);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		primary.getStyleClass().add("text-small-secondary");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		primary.refresh();
	}

}
