/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import org.prelle.shadowrun.BasePluginData;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.spells.TraditionSelectionPane;

import javafx.scene.Node;
import javafx.scene.layout.HBox;

/**
 * @author Stefan
 *
 */
public class NodeTradition extends HBox implements CharacterDataNode {

	private CharacterController charGen;
	private SelectionCallback parent;

	private TraditionSelectionPane primary;

	//--------------------------------------------------------------------
	public NodeTradition(CharacterController charGen, SelectionCallback parent) {
		super(20);
		this.charGen = charGen;
		this.parent  = parent;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		primary = new TraditionSelectionPane(new SelectionCallback() {
			public void showDescription(Node node, BasePluginData value) {
				parent.showDescription(NodeTradition.this, value);
			}
		});
		primary.setData(charGen.getCharacter());
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		
		getChildren().addAll(primary);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		primary.getStyleClass().add("text-small-secondary");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		primary.refresh();
	}

}
