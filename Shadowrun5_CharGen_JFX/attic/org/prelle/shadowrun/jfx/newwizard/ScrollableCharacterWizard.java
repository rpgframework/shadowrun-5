/**
 *
 */
package org.prelle.shadowrun.jfx.newwizard;

import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.shadowrun.BasePluginData;
import org.prelle.shadowrun.MagicOrResonanceType;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun.gen.CharacterGenerator;
import org.prelle.shadowrun.gen.WizardPageType;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.PriorityTable;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;

/**
 * @author prelle
 *
 */
public class ScrollableCharacterWizard extends ManagedScreen implements GenerationEventListener, SelectionCallback {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = SR5Constants.RES;

	private CharacterController charGen;

	private GridPane grid;
	private VBox bxDesc;

	private Map<WizardPageType, Node> nodesByType;

	//-------------------------------------------------------------------
	public ScrollableCharacterWizard(CharacterController charGen) {
		this.charGen = charGen;
		nodesByType  = new HashMap<>();
		setSkin(new ScrollableWizardSkin(this));

		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		// TODO Auto-generated method stub
		for (WizardPageType type : WizardPageType.values()) {
			switch (type) {
			case PRIORITIES:
			}
		}

		bxDesc = new VBox(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		grid = new GridPane();
		grid.setStyle("-fx-vgap: 1em; -fx-hgap: 1em;");
		grid.setStyle("-fx-min-width: 30em;");
		ColumnConstraints col1 = new ColumnConstraints(100, 150, 200);
		grid.getColumnConstraints().add(col1);
		int y=0;
		for (WizardPageType type : WizardPageType.values()) {
			Label heading = new Label();
			try {
				heading.setText(RES.getString("chargen.heading."+type.name().toLowerCase()));
			} catch (MissingResourceException e) {
				logger.error("Missing resource '"+e.getKey()+"' in "+RES.getBaseBundleName());
				heading.setText("chargen.heading."+type.name().toLowerCase());
			}
			heading.getStyleClass().add("text-subheader");
			heading.setUserData(type);
			grid.add(heading, 1, y);
			GridPane.setMargin(heading, new Insets(20, 0, 0, 0));

//			VBox node = new VBox(5);
//			node.getChildren().add(heading);
			switch (type) {
			case PRIORITIES:
				PriorityTable pTable = new PriorityTable((NewPriorityCharacterGenerator) charGen);
				nodesByType.put(type, pTable);
//				pTable.setStyle("-fx-min-height: 40em");
				grid.add(pTable, 0, y+1, 2,1);
//				GridPane.setFillHeight(pTable, true);
				break;
			case METATYPE:
				NodeMetaType nMeta = new NodeMetaType((CharacterGenerator)charGen, this);
				grid.add(nMeta, 1, y+1);
				break;
			case MAGIC_OR_RESONANCE:
				grid.add(new DynamicPointsPane(charGen, DynamicPointsPane.Item.SPECIAL_ATTRIBUTES), 0,y+1);
				NodeMagicOrResonance nMagic = new NodeMagicOrResonance((CharacterGenerator)charGen, this);
				grid.add(nMagic, 1, y+1);
				break;
			case ATTRIBUTES:
				grid.add(new DynamicPointsPane(charGen, DynamicPointsPane.Item.ATTRIBUTES), 0,y+1);
				NodeAttributes nAttrib = new NodeAttributes(charGen);
				grid.add(nAttrib, 1, y+1);
				break;
			case QUALITIES:
				grid.add(new DynamicPointsPane(charGen), 0,y+1);
				NodeQualities nQuali = new NodeQualities(charGen, this, this);
				grid.add(nQuali, 1, y+1);
				break;
			case SKILLS:
				grid.add(new DynamicPointsPane(charGen, DynamicPointsPane.Item.SKILLGROUPS, DynamicPointsPane.Item.SKILLS, DynamicPointsPane.Item.KNOWLEDGE_LANG), 0,y+1);
				NodeSkills nSkill = new NodeSkills(charGen, this, this);
				grid.add(nSkill, 1, y+1);
				break;
			case TRADITION:
				grid.add(new DynamicPointsPane(charGen), 0,y+1);
				NodeTradition nTrad = new NodeTradition(charGen, this);
				grid.add(nTrad, 1, y+1);
				break;
			default:
			}
			y+=2;
		}

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setFitToWidth(true);

		// Layout all
		HBox layout = new HBox(20);
		layout.getChildren().addAll(scroll, bxDesc);
		bxDesc.setStyle("-fx-min-width: 10em; -fx-pref-width: 15em; -fx-max-width: 20em");
		scroll.setStyle("-fx-min-width: 50em");
		HBox.setHgrow(grid, Priority.ALWAYS);
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	private void setVisible(WizardPageType type, boolean visible) {
		if (nodesByType.containsKey(type)) {
			nodesByType.get(type).setVisible(visible);
			nodesByType.get(type).setClip(visible?null:(new Rectangle(1, 1)));
		}

		for (Node tmp : grid.getChildren()) {
			if (tmp.getUserData()==type) {
				tmp.setVisible(visible);
				tmp.setClip(visible?null:(new Rectangle(1, 1)));
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		// TODO Auto-generated method stub
		logger.debug("RCV "+event);
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			// Refresh all displayed nodes
			for (Node node : grid.getChildren()) {
				if (node instanceof CharacterDataNode) {
					((CharacterDataNode)node).refresh();
				}
			}

			// Toggle magic and resonance nodes
			MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
			if (type!=null) {
				setVisible(WizardPageType.SPELLS, type.usesSpells());
				setVisible(WizardPageType.ALCHEMY, type.usesSpells());
				setVisible(WizardPageType.RITUALS, type.usesSpells());
				setVisible(WizardPageType.TRADITION, type.usesSpells());
				setVisible(WizardPageType.POWERS, type.usesPowers());
				setVisible(WizardPageType.COMPLEX_FORMS, type.usesResonance());
			}
			break;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.jfx.newwizard.SelectionCallback#showDescription(javafx.scene.Node, org.prelle.shadowrun.BasePluginData)
	 */
	@Override
	public void showDescription(Node node, BasePluginData value) {
		// TODO Auto-generated method stub
		bxDesc.getChildren().clear();

		if (value==null)
			return;
		Label lbName = new Label(value.getName());
		lbName.getStyleClass().add("text-small-subheader");
		lbName.setWrapText(true);

		Label lbPage = new Label(value.getProductNameShort()+" "+value.getPage());
		lbPage.getStyleClass().add("text-small-secondary");
		lbPage.setWrapText(true);

		Label lbDesc = new Label(value.getHelpText());
		lbDesc.getStyleClass().add("text-body");
		lbDesc.setWrapText(true);
		lbDesc.setStyle("-fx-min-width: 10em; -fx-pref-width: 20em; -fx-max-width: 25em");

		bxDesc.getChildren().addAll(lbName, lbPage, lbDesc);
	}

}

