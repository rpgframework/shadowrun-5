/**
 * 
 */
package org.prelle.shadowrun.jfx.newwizard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Skin;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 * @author prelle
 *
 */
public class ScrollableWizardSkin implements Skin<ManagedScreen> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private ManagedScreen control;

	private Node  content;
	
	private HBox ltrLayout;
	private Region leftSpacing;
	private Region rightSpacing;

	//-------------------------------------------------------------------
	/**
	 */
	public ScrollableWizardSkin(ManagedScreen control) {
		this.control = control;
		control.getStyleClass().add("dialog");
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		logger.debug("Init components");

		ltrLayout = new HBox();
		leftSpacing  = new Region();
		rightSpacing = new Region();
		
		control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		updateContent(control.getContent());
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		ltrLayout.setSpacing(40);
		
		/*
		 * Layout dialog line with TTB with spacing on both sides
		 */
		leftSpacing.setMaxWidth(Double.MAX_VALUE);
		rightSpacing.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(leftSpacing, Priority.SOMETIMES);
		HBox.setHgrow(rightSpacing, Priority.SOMETIMES);
		ltrLayout.getChildren().addAll(leftSpacing, rightSpacing);
		if (control.getContent()!=null)
			updateContent(control.getContent());
//		ltrLayout.setStyle("-fx-background-color: pink");
		
		
		/*
		 * Tell control to take all the possible space 
		 */
		control.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
//		control.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		getChildren().add(ltrLayout);
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
		ltrLayout.getStyleClass().add("dialog");

		leftSpacing.getStyleClass().add("dialog-outside");
		rightSpacing.getStyleClass().add("dialog-outside");
		
//		ltrLayout.getStyleClass().add("dialog-line");
//		layout.getStyleClass().add("dialog");
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		control.contentProperty().addListener((ov,o,n) -> updateContent(n));
	}
	
	//-------------------------------------------------------------------
	void updateContent(Node value) {
		if (content!=null) {
			// Remove existing heading
			ltrLayout.getChildren().remove(content);
			content = null;
		} 

		if (value!=null) {
			content = value;
			content.getStyleClass().add("dialog-line");
//			content.getStyleClass().addAll("dialog-content","text-body");
			((Region)content).setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			HBox.setHgrow(content, Priority.SOMETIMES);
			ltrLayout.getChildren().add(1, content);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getSkinnable()
	 */
	@Override
	public ManagedScreen getSkinnable() {
		return control;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#getNode()
	 */
	@Override
	public Node getNode() {
		return ltrLayout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Skin#dispose()
	 */
	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
    
	//-------------------------------------------------------------------
    /**
     * Returns the children of the skin.
     */
    public final ObservableList<Node> getChildren() {
        return control.getChildren();
    }

}
