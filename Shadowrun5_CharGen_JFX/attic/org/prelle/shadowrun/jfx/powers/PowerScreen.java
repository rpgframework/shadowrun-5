/**
 *
 */
package org.prelle.shadowrun.jfx.powers;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.AdeptPowerController;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.PointsPane;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class PowerScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private PowerSelectionPane primary;

	private PointsPane points;
	private HBox content;

	private Button decPP, incPP;
	private Label lblPP;

	//-------------------------------------------------------------------
	/**
	 */
	public PowerScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.powers"));

		primary = new PowerSelectionPane(control.getPowerController(), this);

		/*
		 * Exp & Co.
		 */
		decPP = new Button("\uE0C6");
		incPP = new Button("\uE0C5");
		lblPP = new Label("?");
		decPP.setStyle("-fx-font-family: 'Segoe UI Symbol'; -fx-border-width: 0px; -fx-text-fill: textcolor-highlight-primary");
		incPP.setStyle("-fx-font-family: 'Segoe UI Symbol'; -fx-border-width: 0px; -fx-text-fill: textcolor-highlight-primary");
		lblPP.setStyle("-fx-font-size: 150%");
		HBox extra = new HBox(3, decPP, lblPP, incPP);

		points = new PointsPane(control, AdeptPowerController.class);
		points.setExtra(extra);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, primary);
		HBox.setMargin(points, new Insets(0,0,20,0));
		setContent(content);


	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		decPP.setOnAction(ev -> control.getPowerController().decreasePowerPoint());
		incPP.setOnAction(ev -> control.getPowerController().increasePowerPoints());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case POINTS_LEFT_POWERS:
			logger.debug("rcv "+event.getType());
			points.refresh();
			lblPP.setText(String.valueOf(control.getCharacter().getBoughtPowerPoints()));
			decPP.setDisable(!control.getPowerController().canDecreasePowerPoints());
			incPP.setDisable(!control.getPowerController().canIncreasePowerPoints());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.powers"));
		primary.setData(model);
		points.setData(model);
		points.refresh();
		lblPP.setText(String.valueOf(control.getCharacter().getBoughtPowerPoints()));
		decPP.setDisable(!control.getPowerController().canDecreasePowerPoints());
		incPP.setDisable(!control.getPowerController().canIncreasePowerPoints());
	}

}
