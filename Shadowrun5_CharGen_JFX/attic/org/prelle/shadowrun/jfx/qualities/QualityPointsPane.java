/**
 * 
 */
package org.prelle.shadowrun.jfx.qualities;

import javafx.geometry.Insets;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.VBox;

import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.PointsPane;

/**
 * @author prelle
 *
 */
public class QualityPointsPane extends PointsPane {

	private CheckBox cbGratis;
	
	//-------------------------------------------------------------------
	public QualityPointsPane(CharacterController ctrl, Class<?> type) {
		super(ctrl, type);
		
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		super.initComponents();
		
		cbGratis = new CheckBox(UI.getString("qualityscreen.nokarma"));
		cbGratis.setWrapText(true);
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		super.initLayout();
		
//		getChildren().add(cbGratis);
		VBox.setMargin(cbGratis, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbGratis.selectedProperty().addListener( (ov,o,n) -> ctrl.getQualityController().setIgnoreKarma(n));
	}

}
