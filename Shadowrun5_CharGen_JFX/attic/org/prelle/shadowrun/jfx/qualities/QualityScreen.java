/**
 *
 */
package org.prelle.shadowrun.jfx.qualities;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.Attribute;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.charctrl.CharGenMode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.QualityController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class QualityScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	private PropertyResourceBundle DESCRIPTIONS;

	private CharacterController control;
	private CharGenMode mode;

	private QualitySelectionPane primary;
	private Attribute focussed;

	private QualityPointsPane points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public QualityScreen(CharacterController control, CharGenMode mode) {
		this.control = control;
		this.mode    = mode;
		if (control==null)
			throw new NullPointerException();
		if (mode==null)
			throw new NullPointerException("CharGenMode");
		DESCRIPTIONS = ShadowrunCore.getI18nHelpResources();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.powers"));

		primary = new QualitySelectionPane(control.getQualityController(), this, mode);

		/*
		 * Exp & Co.
		 */
		points = new QualityPointsPane(control, QualityController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, primary);
		HBox.setMargin(points, new Insets(0,0,20,0));
		setContent(content);


	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case CHARACTER_CHANGED:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.qualities"));
		primary.setData(model);
		points.setData(model);
		points.refresh();
	}

}
