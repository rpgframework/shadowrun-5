/**
 * 
 */
package org.prelle.shadowrun.jfx.sins;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.LifestyleValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class LifestyleCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headName, headPaid;

	//-------------------------------------------------------------------
	/**
	 */
	public LifestyleCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headName  = new Label(UI.getString("label.name"));
		headPaid  = new Label(UI.getString("label.lifestyle.paid"));
		headName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPaid.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headName.getStyleClass().add("table-heading");
		headPaid.getStyleClass().add("table-heading");
		
		headPaid.setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headName , 0,0);
		this.add(headPaid , 1,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case LIFESTYLE_ADDED:
		case LIFESTYLE_CHANGED:
		case LIFESTYLE_REMOVED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headName, headPaid);
		
		int y=0;
		for (LifestyleValue con : model.getLifestyle()) {
			y++;

			Label lblName  = new Label(con.getName());
			Label lblPaid  = new Label(String.valueOf(con.getPaidMonths()));
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblPaid.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblPaid, 1, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
