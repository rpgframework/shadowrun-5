/**
 *
 */
package org.prelle.shadowrun.jfx.sins;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.LifestyleOptionValue;
import org.prelle.shadowrun.LifestyleValue;
import org.prelle.shadowrun.SIN;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunTools;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.LifestyleController;
import org.prelle.shadowrun.charctrl.SingleLifestyleController;
import org.prelle.shadowrun.gen.SingleLifestyleGenerator;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.gen.event.GenerationEventType;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.ValueField;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class LifestyleScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private LifestyleController control;
	private ShadowrunCharacter model;

	private ListView<LifestyleValue> list;
	private MenuItem btnAdd;
	private Button btnEdit;
	private Button btnDelete;

	private VBox points;
	private HBox content;
	private Label lblNuyen;

	//-------------------------------------------------------------------
	/**
	 */
	public LifestyleScreen(CharacterController control) {
		this.control = control.getLifestyleController();
		if (this.control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		btnEdit.setVisible(false);
		btnDelete.setVisible(false);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.sins"));

		lblNuyen = new Label();
		lblNuyen.getStyleClass().add("text-header");
		Label heaNuyen = new Label(UI.getString("label.nuyen"));
		VBox bxNuyen = new VBox();
		bxNuyen.setMaxWidth(Double.MAX_VALUE);
		bxNuyen.setAlignment(Pos.CENTER);
		bxNuyen.getChildren().addAll(lblNuyen, heaNuyen);

		points = new VBox();
		points.getStyleClass().add("section-bar");
		Label desc = new Label(UI.getString("screen.lifestyles.desc"));
		desc.setWrapText(true);
		points.getChildren().addAll(bxNuyen, desc);
		points.setStyle("-fx-pref-width: 15em");


		list = new ListView<>();
		list.setOrientation(Orientation.HORIZONTAL);
		list.setCellFactory(new Callback<ListView<LifestyleValue>, ListCell<LifestyleValue>>() {
			public ListCell<LifestyleValue> call(ListView<LifestyleValue> param) {
				return new VerticalLifestyleValueListCell(control);
			}
		});


		FontIcon icoAdd    = new FontIcon("\uE17E\uE109");
		FontIcon icoEdit   = new FontIcon("\uE17E\uE104");
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		icoAdd.getStyleClass().add("action-area-icon");
		icoEdit.getStyleClass().add("action-area-icon");
		icoDelete.getStyleClass().add("action-area-icon");
		btnAdd   = new MenuItem(null, icoAdd);
//		btnAdd.setTooltip(new Tooltip(UI.getString("screen.lifestyles.tooltip.add")));
		btnEdit  = new Button(null, icoEdit);
		btnEdit.setTooltip(new Tooltip(UI.getString("screen.lifestyles.tooltip.edit")));
		btnDelete= new Button(null, icoDelete);
		btnDelete.setTooltip(new Tooltip(UI.getString("screen.lifestyles.tooltip.delete")));
		getStaticButtons().addAll(btnAdd);
		getContextButtons().addAll(btnEdit, btnDelete);

//		/*
//		 * Exp & Co.
//		 */
//		points = new PointsPane(control);
//		if (control instanceof Generator)
//			points.setGenerator((Generator) control);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		list.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, list);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(list, new Insets(0,20,20,0));
		HBox.setHgrow(list, Priority.ALWAYS);
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> select(n));

		btnDelete.setOnAction(event -> deleteClicked(list.getSelectionModel().getSelectedItem()));
		btnEdit.setOnAction(event -> editClicked(list.getSelectionModel().getSelectedItem()));
		btnAdd.setOnAction(event -> addClicked());
	}

	//-------------------------------------------------------------------
	private void refresh() {
		list.getItems().clear();
		if (model!=null)
			list.getItems().addAll(model.getLifestyle());

		lblNuyen.setText(String.valueOf(model.getNuyen()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case LIFESTYLE_ADDED:
		case LIFESTYLE_CHANGED:
		case LIFESTYLE_REMOVED:
			logger.debug("RCV "+event);
			logger.debug("Lifestyles now "+model.getLifestyle());
			refresh();
			break;
		case NUYEN_CHANGED:
			logger.debug("TODO RCV "+event.getType());
//			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#close(org.prelle.javafx.ManagedScreen.CloseType)
	 */
	@Override
	public boolean close(CloseType type) {
		GenerationEventDispatcher.removeListener(this);
		return true;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.javafx.ManagedScreen#childClosed(org.prelle.javafx.ManagedScreen, org.prelle.javafx.ManagedScreen.CloseType)
//	 */
//	@Override
//	public void childClosed(ManagedScreen child, CloseType type) {
//		// TODO Auto-generated method stub
//
//	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.lifestyles"));
		this.model = model;

		refresh();
//		points.setData(model);
//		if (control.getConnectionController() instanceof Generator) {
//			points.setGenerator((Generator)control.getConnectionController());
//			points.refresh();
//		}
	}

	//-------------------------------------------------------------------
	private void select(LifestyleValue data) {
		logger.debug("Select "+data);
		btnEdit.setVisible(data!=null);
		btnDelete.setVisible(data!=null);
	}

	//-------------------------------------------------------------------
	private void editClicked(LifestyleValue selectedItem) {
		logger.debug("editClicked("+selectedItem+")");

		SingleLifestyleController perItemCtrl = new SingleLifestyleGenerator(model, selectedItem);

		NavigButtonControl buttonControl = new NavigButtonControl() {};
		NewLifestylePane pane = new NewLifestylePane(perItemCtrl, buttonControl, control.getModel());
		pane.setData(selectedItem);
		CloseType result = getManager().showAlertAndCall(AlertType.QUESTION, UI.getString("screen.lifestyles.editlifestyle"), pane, buttonControl);
		if (result==CloseType.OK) {
			selectedItem.setName(pane.getSelectedName());
			selectedItem.setDescription(pane.getSelectedDescription());
			if (pane.getSelectedSIN()!=null)
				selectedItem.setSIN(pane.getSelectedSIN().getUniqueId());
			refresh();
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.LIFESTYLE_CHANGED, selectedItem));
		}
	}

	//-------------------------------------------------------------------
	private void deleteClicked(LifestyleValue selectedItem) {
		CloseType result = getManager().showAlertAndCall(
				AlertType.CONFIRMATION,
				UI.getString("screen.lifestyles.confirmdelete.heading"),
				String.format(UI.getString("screen.lifestyles.confirmdelete.content"), selectedItem.getName())
				);
		if (result==CloseType.YES) {
			control.removeLifestyle(selectedItem);
		}
	}

	//-------------------------------------------------------------------
	private void addClicked() {
		logger.debug("addClicked");

		SingleLifestyleController perItemCtrl = new SingleLifestyleGenerator(model);

		NavigButtonControl buttonControl = new NavigButtonControl() {};
		NewLifestylePane pane = new NewLifestylePane(perItemCtrl, buttonControl, control.getModel());
		CloseType result = getManager().showAlertAndCall(AlertType.QUESTION, UI.getString("screen.lifestyles.addlifestyle"), pane, buttonControl);
		if (result==CloseType.OK) {
			control.addLifestyle(perItemCtrl.getResult());
//			LifestyleValue lifestyle = control.addLifestyle(perItemCtrl.getResult());
//			lifestyle.setPaidMonths(1);
//			lifestyle.setName(pane.getSelectedName());
//			lifestyle.setDescription(pane.getSelectedDescription());
			refresh();
		}
	}

}

class VerticalLifestyleValueListCell extends ListCell<LifestyleValue> {

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private LifestyleController control;

	private Label lblName;
	private Label lblType;
	private Label lblOpts;
	private Label lblCost;
	private Label lblSIN;
	private ValueField vfMonths;
	private StackPane layout;

	private LifestyleValue data;

	//-------------------------------------------------------------------
	public VerticalLifestyleValueListCell(LifestyleController ctrl) {
		this.control = ctrl;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("text-subheader");
		lblType = new Label();
		lblOpts = new Label();
		lblCost = new Label();
		lblCost.getStyleClass().add("text-subheader");
		lblSIN = new Label();

		vfMonths = new ValueField();

		lblName.setMaxWidth(Double.MAX_VALUE);
		lblName.setAlignment(Pos.CENTER);
		lblType.setMaxWidth(Double.MAX_VALUE);
		lblType.setAlignment(Pos.CENTER);
		lblOpts.setWrapText(true);
		lblOpts.setMaxWidth(Double.MAX_VALUE);
		lblOpts.setAlignment(Pos.CENTER);
		lblCost.setMaxWidth(Double.MAX_VALUE);
		lblCost.setAlignment(Pos.CENTER);
		lblSIN.setMaxWidth(Double.MAX_VALUE);
		lblSIN.setAlignment(Pos.CENTER);
		vfMonths.setMaxWidth(Double.MAX_VALUE);
		vfMonths.setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaOpts   = new Label(UI.getString("lifestylecell.options"));
		Label heaSIN    = new Label(UI.getString("lifestylecell.sin"));
		Label heaCost   = new Label(UI.getString("lifestylecell.cost"));
		Label heaMonths = new Label(UI.getString("lifestylecell.paidmonth"));
		heaOpts.setMaxWidth(Double.MAX_VALUE);
		heaOpts.setAlignment(Pos.CENTER);
		heaSIN.setMaxWidth(Double.MAX_VALUE);
		heaSIN.setAlignment(Pos.CENTER);
		heaCost.setMaxWidth(Double.MAX_VALUE);
		heaCost.setAlignment(Pos.CENTER);
		heaMonths.setMaxWidth(Double.MAX_VALUE);
		heaMonths.setAlignment(Pos.CENTER);
		heaOpts.getStyleClass().add("text-small-subheader");
		heaSIN.getStyleClass().add("text-small-subheader");
		heaCost.getStyleClass().add("text-small-subheader");
		heaMonths.getStyleClass().add("text-small-subheader");

		Region spacing = new Region();
		spacing.setMaxHeight(Double.MAX_VALUE);
		spacing.setMinHeight(0);
		spacing.setPrefHeight(0);
		spacing.setStyle("-fx-background-color: lime");
		VBox.setVgrow(spacing, Priority.SOMETIMES);

		VBox layoutL = new VBox();
		layoutL.getChildren().addAll(lblName, lblType, heaOpts, lblOpts, heaSIN, lblSIN, heaCost, lblCost);
		VBox layoutU = new VBox();
		layoutU.setAlignment(Pos.BOTTOM_CENTER);
		layoutU.setMaxHeight(Double.MAX_VALUE);
		layoutU.getChildren().addAll(heaMonths, vfMonths);
		layout = new StackPane();
		layout.setMaxHeight(Double.MAX_VALUE);
		layout.getChildren().addAll(layoutL, layoutU);
		StackPane.setAlignment(layoutL, Pos.TOP_CENTER);
		StackPane.setAlignment(layoutU, Pos.BOTTOM_CENTER);

		VBox.setMargin(heaOpts, new Insets(20, 0, 0, 0));
		VBox.setMargin(heaSIN , new Insets(20, 0, 0, 0));
		VBox.setMargin(heaCost, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		vfMonths.inc.setOnAction(event -> {control.increaseMonths(data); refreshMonths(); });
		vfMonths.dec.setOnAction(event -> control.decreaseMonths(data));
	}

	//-------------------------------------------------------------------
	private void refreshMonths() {
		vfMonths.setValue(" "+data.getPaidMonths()+" ");
		vfMonths.inc.setDisable(!control.canIncreaseMonths(data));
		vfMonths.dec.setDisable(!control.canDecreaseMonths(data));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(LifestyleValue item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(item.getName());
			lblType.setText(item.getLifestyle().getName());
			// Options
			List<String> optNames = new ArrayList<String>();
			for (LifestyleOptionValue opt : item.getOptions())
				optNames.add(opt.getOption().getName());
			lblOpts.setText(String.join("\n", optNames));

			lblCost.setText(ShadowrunTools.getLifestyleCost(control.getModel(), item)+" \u00A5");
			if (item.getSIN()!=null) {
				SIN sin = control.getModel().getSIN(item.getSIN());
				if (sin!=null)
					lblSIN.setText(sin.getName());
				else
					lblSIN.setText("?Error?");
			} else
				lblSIN.setText("-");

			refreshMonths();

			layout.requestLayout();
			setGraphic(layout);
			this.layoutChildren();
//			if (item.getQuality()==Quality.REAL_SIN)
//				lblQuality.setText(item.getQuality().toString());
//			else
//				lblQuality.setText(item.getQualityValue()+" - "+item.getQuality().toString());
//			lblCriminal.setVisible(item.isCriminal());
//
//			// Lifestyles
//			bxLifestyles.getChildren().clear();
//			List<LifestyleValue> lsData = control.getModel().getLifestyles(item);
//			if (lsData.isEmpty()) {
//				bxLifestyles.getChildren().add(new Label(UI.getString("screen.sins.noLifestyles")));
//
//			} else {
//				for (LifestyleValue tmp : lsData) {
//					bxLifestyles.getChildren().add(new Label(tmp.getName()));
//				}
//			}
//
//			// Licenses
//			bxLicences.getChildren().clear();
//			List<LicenseValue> licData = control.getModel().getLicenses(item);
//			if (licData.isEmpty()) {
//				bxLicences.getChildren().add(new Label(UI.getString("screen.sins.noLicenses")));
//			} else {
//				for (LicenseValue tmp : licData) {
//					bxLicences.getChildren().add(new Label(tmp.getName()+" ("+tmp.getRating()+")"));
//				}
//			}
//
//			icoLockState.getLabel().setText( control.canDeleteSIN(item)?"\uE1F7":"\uE1F6");
		}
	}
}
