/**
 *
 */
package org.prelle.shadowrun.jfx.sins;

import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.LicenseValue;
import org.prelle.shadowrun.LifestyleValue;
import org.prelle.shadowrun.SIN;
import org.prelle.shadowrun.SIN.Quality;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.SINController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.gen.event.GenerationEventType;
import org.prelle.shadowrun.items.ItemTemplate;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SINScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SINController control;

	private ListView<SIN> list;
	private MenuItem btnAdd;
	private Button btnEdit;
	private Button btnDelete;

	private VBox points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public SINScreen(CharacterController control) {
		this.control = control.getSINController();
		if (this.control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		btnEdit.setVisible(false);
		btnDelete.setVisible(false);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.sins"));

		points = new VBox();
		points.getStyleClass().add("section-bar");
		Label desc = new Label(UI.getString("screen.sins.desc"));
		desc.setWrapText(true);
		points.getChildren().add(desc);
		points.setStyle("-fx-pref-width: 15em");


		list = new ListView<>();
		list.setOrientation(Orientation.HORIZONTAL);
		list.setCellFactory(new Callback<ListView<SIN>, ListCell<SIN>>() {
			public ListCell<SIN> call(ListView<SIN> param) {
				return new VerticalSINListCell(control, SINScreen.this);
			}
		});


		FontIcon icoAdd    = new FontIcon("\uE17E\uE109");
		FontIcon icoEdit   = new FontIcon("\uE17E\uE104");
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		icoAdd.getStyleClass().add("action-area-icon");
		icoEdit.getStyleClass().add("action-area-icon");
		icoDelete.getStyleClass().add("action-area-icon");
		btnAdd   = new MenuItem(null, icoAdd);
//		btnAdd.setTooltip(new Tooltip(UI.getString("screen.sins.tooltip.add")));
		btnEdit  = new Button(null, icoEdit);
		btnEdit.setTooltip(new Tooltip(UI.getString("screen.sins.tooltip.edit")));
		btnDelete= new Button(null, icoDelete);
		btnDelete.setTooltip(new Tooltip(UI.getString("screen.sins.tooltip.delete")));
		getStaticButtons().addAll(btnAdd);
		getContextButtons().addAll(btnEdit, btnDelete);

//		/*
//		 * Exp & Co.
//		 */
//		points = new PointsPane(control);
//		if (control instanceof Generator)
//			points.setGenerator((Generator) control);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		list.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, list);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(list, new Insets(0,20,20,0));
		HBox.setHgrow(list, Priority.ALWAYS);
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> select(n));

		btnDelete.setOnAction(event -> deleteClicked(list.getSelectionModel().getSelectedItem()));
		btnEdit.setOnAction(event -> editClicked(list.getSelectionModel().getSelectedItem()));
		btnAdd.setOnAction(event -> addClicked());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SIN_ADDED:

//		case EXPERIENCE_CHANGED:
//			logger.debug("rcv "+event.getType());
//			points.refresh();
//			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.javafx.ManagedScreen#childClosed(org.prelle.javafx.ManagedScreen, org.prelle.javafx.ManagedScreen.CloseType)
//	 */
//	@Override
//	public void childClosed(ManagedScreen child, CloseType type) {
//		// TODO Auto-generated method stub
//
//	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+UI.getString("label.sins"));
		list.getItems().clear();
		list.getItems().addAll(model.getSINs());

//		points.setData(model);
//		if (control.getConnectionController() instanceof Generator) {
//			points.setGenerator((Generator)control.getConnectionController());
//			points.refresh();
//		}
	}

	//-------------------------------------------------------------------
	private void select(SIN data) {
		logger.debug("Select "+data);
		btnEdit.setVisible(data!=null);
		btnDelete.setVisible(data!=null);

		btnDelete.setDisable(!control.canDeleteSIN(data));
	}

	//-------------------------------------------------------------------
	private void editClicked(SIN selectedItem) {
		TextArea taDesc = new TextArea(selectedItem.getDescription());
		CloseType result = getManager().showAlertAndCall(
				AlertType.QUESTION,
				String.format(UI.getString("screen.sins.edit.heading"), selectedItem.getName()),
				taDesc
				);
		if (result==CloseType.OK) {
			selectedItem.setDescription(taDesc.getText());
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.SIN_CHANGED, selectedItem));
		}
	}

	//-------------------------------------------------------------------
	private void deleteClicked(SIN selectedItem) {
		CloseType result = getManager().showAlertAndCall(
				AlertType.CONFIRMATION,
				UI.getString("screen.sins.confirmdelete.heading"),
				String.format(UI.getString("screen.sins.confirmdelete.content"), selectedItem.getName())
				);
		if (result==CloseType.YES) {
			if (control.deleteSIN(selectedItem)) {
				list.getItems().remove(selectedItem);
			}
		}
	}

	//-------------------------------------------------------------------
	private void addClicked() {
		logger.debug("addClicked");

		NavigButtonControl buttonControl = new NavigButtonControl() {};
		NewSINPane pane = new NewSINPane(control, buttonControl, control.getModel());
		CloseType result = getManager().showAlertAndCall(AlertType.QUESTION, UI.getString("screen.sins.addsin"), pane, buttonControl);
		if (result==CloseType.OK) {
			Object[] input = pane.getData();
			String  name = (String)input[0];
			Quality qual = (SIN.Quality)input[1];
			String  desc = (String)input[2];
			SIN sin = control.createNewSIN(name, qual);
			if (sin==null) {
				logger.error("Failed to add SIN");
				return;
			}
			if (desc!=null && desc.length()>0)
				sin.setDescription(desc);
			list.getItems().add(sin);
		}
	}

}

class VerticalSINListCell extends ListCell<SIN> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SINController control;
	private ScreenManagerProvider provider;

	private Label lblName;
	private Label lblQuality;
	private Label lblCriminal;
	private Button btnAddLicense;
	private VBox bxLicences;
	private VBox bxLifestyles;
	private FontIcon icoLockState;
	private VBox layout;

	private SIN data;

	//-------------------------------------------------------------------
	public VerticalSINListCell(SINController ctrl, ScreenManagerProvider provider) {
		this.control = ctrl;
		this.provider = provider;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("text-subheader");
		lblQuality = new Label();
		lblCriminal = new Label(UI.getString("screen.sins.criminal"));
		lblCriminal.setStyle("-fx-text-fill: -fx-focus-color");

		btnAddLicense = new Button(null, new FontIcon("\uE17E\uE109"));
		btnAddLicense.setStyle("-fx-border-width: 0px");
		btnAddLicense.setMaxWidth(Double.MAX_VALUE);
		btnAddLicense.setAlignment(Pos.TOP_CENTER);

		bxLicences = new VBox();
		bxLifestyles = new VBox();

		icoLockState = new FontIcon("\uE1F6");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaLifestyles = new Label(UI.getString("label.lifestyles"));
		heaLifestyles.getStyleClass().add("text-small-subheader");
		Label heaLicenses = new Label(UI.getString("label.licenses"));
		heaLicenses.getStyleClass().add("text-small-subheader");

		Region spacing = new Region();
		spacing.setMaxHeight(Double.MAX_VALUE);

		layout = new VBox();
		layout.getChildren().addAll(lblName, lblQuality, lblCriminal, heaLifestyles, bxLifestyles, heaLicenses, bxLicences, spacing, icoLockState);
		VBox.setVgrow(spacing, Priority.ALWAYS);

		VBox.setMargin(heaLifestyles, new Insets(20, 0, 0, 0));
		VBox.setMargin(heaLicenses, new Insets(20, 0, 0, 0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAddLicense.setOnAction(event -> {
			logger.debug("btnAddLicense clicked");
			openFakeLicenseDialog();
		});
	}

	//-------------------------------------------------------------------
	private void openFakeLicenseDialog() {
		ItemTemplate fake = ShadowrunCore.getItem("fake_license");

		Label heaName = new Label(UI.getString("label.type"));
		Label heaQual = new Label(UI.getString("label.quality"));
		heaName.getStyleClass().add("text-small-subheader");
		heaQual.getStyleClass().add("text-small-subheader");

		TextField tfName = new TextField();
		tfName.setStyle("-fx-pref-width: 25em");
		tfName.setPromptText(UI.getString("label.placeholder.fake_license"));

		ChoiceBox<Quality> cbQual = new ChoiceBox<>();
		cbQual.setStyle("-fx-pref-width: 20em");
		for (int i=1; i<=fake.getMaximumRating(); i++) {
			if (i==1 || control.canCreateNewLicense(Quality.getByIntValue(i)))
				cbQual.getItems().add(Quality.getByIntValue(i));
		}
		cbQual.setConverter(new StringConverter<Quality>() {
			public String toString(Quality val) {
				return UI.getString("label.rating")+" "+val.getValue()+" ("+val.toString()+") "+(fake.getPrice()*val.getValue())+" \u00A5";
			}
			public Quality fromString(String val) { return null; }
		});
		cbQual.setValue(Quality.ANYONE);


		GridPane grid = new GridPane();
		grid.setStyle("-fx-hgap: 1em; -fx-vgap: 1em");
		grid.add(heaName, 0, 0);
		grid.add( tfName, 1, 0);
		grid.add(heaQual, 0, 1);
		grid.add( cbQual, 1, 1);

		CloseType result = provider.getScreenManager().showAlertAndCall(
				AlertType.QUESTION,
				UI.getString("dialog.enterlicense.title"),
				grid);
		if (result==CloseType.OK) {
			if (control.canCreateNewLicense(cbQual.getValue())) {
				LicenseValue val = control.createNewLicense(tfName.getText(), cbQual.getValue());
				val.setSIN(data.getUniqueId());
				val.setName(tfName.getText());
				refresh(data);
			} else
				logger.warn("Player chose to add a license that was not selectable by backend");
		}
	}

	//-------------------------------------------------------------------
	private void refresh(SIN item) {
		lblName.setText(item.getName());
		if (item.getQuality()==Quality.REAL_SIN)
			lblQuality.setText(item.getQuality().toString());
		else
			lblQuality.setText(item.getQualityValue()+" - "+item.getQuality().toString());
		lblCriminal.setVisible(item.isCriminal());

		// Lifestyles
		bxLifestyles.getChildren().clear();
		List<LifestyleValue> lsData = control.getModel().getLifestyles(item);
		if (lsData.isEmpty()) {
			bxLifestyles.getChildren().add(new Label(UI.getString("screen.sins.noLifestyles")));

		} else {
			for (LifestyleValue tmp : lsData) {
				bxLifestyles.getChildren().add(new Label(tmp.getName()));
			}
		}

		// Licenses
		bxLicences.getChildren().clear();
		List<LicenseValue> licData = control.getModel().getLicenses(item);
		if (licData.isEmpty()) {
			bxLicences.getChildren().add(new Label(UI.getString("screen.sins.noLicenses")));
		} else {
			for (LicenseValue tmp : licData) {
				bxLicences.getChildren().add(new Label(tmp.getName()+" ("+UI.getString("label.rating")+" "+tmp.getRating().getValue()+")"));
			}
		}
		bxLicences.getChildren().add(btnAddLicense);
		VBox.setMargin(btnAddLicense, new Insets(20,0,0,0));

		icoLockState.getLabel().setText( control.canDeleteSIN(item)?"\uE1F7":"\uE1F6");
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SIN item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			refresh(item);
		}
	}
}
