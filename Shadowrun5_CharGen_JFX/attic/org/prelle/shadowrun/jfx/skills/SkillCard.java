/**
 * 
 */
package org.prelle.shadowrun.jfx.skills;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.Skill;
import org.prelle.shadowrun.Skill.SkillType;
import org.prelle.shadowrun.SkillGroup;
import org.prelle.shadowrun.SkillGroupValue;
import org.prelle.shadowrun.SkillValue;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class SkillCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headAttr, headValue, headPool;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillCard(SkillType type) {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headValue  = new Label(UI.getString("label.value"));
		headPool   = new Label(UI.getString("label.pool"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPool.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headValue.getStyleClass().add("table-heading");
		headPool.getStyleClass().add("table-heading");
		
		headValue.setAlignment(Pos.CENTER_RIGHT);
		headPool.setAlignment(Pos.CENTER_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);
		this.add(headValue , 1,0);
		this.add(headPool  , 2,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		ColumnConstraints centerAlign2 = new ColumnConstraints();
		centerAlign2.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
		constraints.add(centerAlign2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SKILL_CHANGED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headAttr, headValue, headPool);
		
		int y=0;
		for (SkillGroupValue sVal : model.getSkillGroupValues()) {
			y++;

			SkillGroup skill = sVal.getModifyable();
			Label lblName  = new Label(skill.getName());
			Label lblVal   = new Label(sVal.getModifiedValue()+"");
			Label lblPool  = new Label("-");
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblVal.getStyleClass().add(lineStyle);
			lblPool.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblVal , 1, y);
			this.add(lblPool, 2, y);
		}
		
		// Skills
		for (SkillValue sVal : model.getSkillValues(false)) {
			y++;

			Skill skill = sVal.getModifyable();
			Label lblName  = new Label(sVal.getName());
			Label lblVal   = new Label(sVal.getModifiedValue()+"");
			Label lblPool  = new Label(
					(model.getAttribute(skill.getAttribute1()).getModifiedValue()+
					sVal.getModifiedValue())+"");
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblVal.getStyleClass().add(lineStyle);
			lblPool.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblVal , 1, y);
			this.add(lblPool, 2, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
