/**
 *
 */
package org.prelle.shadowrun.jfx.skills;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;
import org.prelle.shadowrun.jfx.fluent.SR5FirstLine;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SkillScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;

	private SkillSelectionPane pane;

	private SR5FirstLine firstLine;
	private FreePointsNode freePoints;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillScreen(CharacterController control) {
		this.control = control;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("label.skills"));

		pane = new SkillSelectionPane(control.getSkillController(), this);

		/*
		 * Exp & Co.
		 */
		firstLine  = new SR5FirstLine();
		freePoints = new FreePointsNode();
		freePoints.setStyle("-fx-max-height: 3em; -fx-max-width: 3em");
		freePoints.setPoints(control.getCharacter().getKarmaFree());
		freePoints.setName(UI.getString("label.ep.karma"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox flow = new HBox();
		flow.getChildren().addAll(freePoints, pane);
		flow.setStyle("-fx-spacing: 1em");

		VBox content = new VBox();
		content.setSpacing(20);
		content.getChildren().addAll(firstLine, flow);
		VBox.setVgrow(flow, Priority.ALWAYS);
		VBox.setMargin(flow  , new Insets(0,0,20,0));
		setContent(content);
		content.getStyleClass().add("skill-screen");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case POINTS_LEFT_SKILLS:
		case POINTS_LEFT_SKILLGROUPS:
			logger.debug("rcv "+event.getType());
			freePoints.setPoints(control.getCharacter().getKarmaFree());
			firstLine.setData(control.getCharacter());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		setTitle(control.getCharacter().getName()+" / "+UI.getString("label.skills"));
		pane.setData(control.getCharacter());
		freePoints.setPoints(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());
	}

}
