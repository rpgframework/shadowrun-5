/**
 * 
 */
package org.prelle.shadowrun.jfx.spells;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.Ritual;
import org.prelle.shadowrun.RitualValue;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class RitualCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headAttr;

	//-------------------------------------------------------------------
	/**
	 */
	public RitualCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SPELL_ADDED:
		case SPELL_REMOVED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headAttr);
		
		int y=0;
		for (RitualValue sVal : model.getRituals()) {
			y++;

			Ritual skill = sVal.getModifyable();
			Label lblName  = new Label(skill.getName());
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
