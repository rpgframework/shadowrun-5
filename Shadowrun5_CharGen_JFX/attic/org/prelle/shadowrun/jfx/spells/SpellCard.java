/**
 * 
 */
package org.prelle.shadowrun.jfx.spells;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.ShadowrunTools;
import org.prelle.shadowrun.Spell;
import org.prelle.shadowrun.SpellValue;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class SpellCard extends GridPane implements GenerationEventListener {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private boolean alchemistic;
	private Label headAttr, headValue;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellCard(boolean alchemistic) {
		this.alchemistic = alchemistic;
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");
		
		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headAttr   = new Label(UI.getString("label.name"));
		headValue  = new Label(UI.getString("label.drain"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headValue.getStyleClass().add("table-heading");
		
		headValue.setAlignment(Pos.CENTER_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headAttr  , 0,0);
		this.add(headValue , 1,0);

		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		
		constraints.add(maxGrow);
		constraints.add(centerAlign);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SPELL_ADDED:
		case SPELL_REMOVED:
			updateContent();
			break;
		default:
		}
		
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headAttr, headValue);
		
		int y=0;
		for (SpellValue sVal : model.getSpells(alchemistic)) {
			y++;

			Spell spell = sVal.getModifyable();
			Label lblName  = new Label(spell.getName());
			Label lblVal   = new Label(ShadowrunTools.getDrainString(spell));
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblVal.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(lblName, 0, y);
			this.add(lblVal , 1, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
