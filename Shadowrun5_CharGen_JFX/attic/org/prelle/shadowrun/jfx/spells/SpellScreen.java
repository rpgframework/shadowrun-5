/**
 *
 */
package org.prelle.shadowrun.jfx.spells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.charctrl.SkillController;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class SpellScreen extends ManagedScreen implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;
	private boolean alchemistic;

	private SpellSelectionPane pane;

	private TraditonPointsPane points;
	private HBox content;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellScreen(CharacterController control, boolean alchemistic) {
		this.control = control;
		this.alchemistic = alchemistic;
		if (control==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle((alchemistic?UI.getString("label.alchemisticspells"):UI.getString("label.spells")));

		pane = new SpellSelectionPane(alchemistic?control.getAlchemyController():control.getSpellController(), this, alchemistic);

		/*
		 * Exp & Co.
		 */
		points = new TraditonPointsPane(control, SkillController.class);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(points, pane);
		HBox.setMargin(points, new Insets(0,0,20,0));
		HBox.setMargin(pane  , new Insets(0,0,20,0));
		setContent(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
		case POINTS_LEFT_SPELLS_RITUALS:
			logger.debug("rcv "+event.getType());
			points.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#onClose()
	 */
	@Override
	public void onClose() {
		GenerationEventDispatcher.removeListener(this);
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		setTitle(model.getName()+" / "+(alchemistic?UI.getString("label.alchemisticspells"):UI.getString("label.spells")));
		pane.setData(model);
		points.setData(model);
		points.refresh();
	}

}
