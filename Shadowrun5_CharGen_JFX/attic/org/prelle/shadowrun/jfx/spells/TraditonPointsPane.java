/**
 * 
 */
package org.prelle.shadowrun.jfx.spells;

import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

import org.prelle.shadowrun.ShadowrunCore;
import org.prelle.shadowrun.Tradition;
import org.prelle.shadowrun.charctrl.CharacterController;
import org.prelle.shadowrun.jfx.PointsPane;

/**
 * @author prelle
 *
 */
public class TraditonPointsPane extends PointsPane {

	private ChoiceBox<Tradition> cbTradition;

	//-------------------------------------------------------------------
	public TraditonPointsPane(CharacterController ctrl, Class<?> type) {
		super(ctrl, type);
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		super.initComponents();
		
		cbTradition = new ChoiceBox<>();
		cbTradition.setConverter(new StringConverter<Tradition>() {
			public String toString(Tradition data) { return data.getName(); }
			public Tradition fromString(String string) { return null; }
		});
		cbTradition.getItems().addAll(ShadowrunCore.getTraditions());
		
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		super.initLayout();
		Label lblTradition = new Label(UI.getString("label.tradition"));
		
		VBox bxTradition = new VBox(5);
		bxTradition.setStyle("-fx-max-width: 15em");
		bxTradition.setAlignment(Pos.CENTER);
		bxTradition.getChildren().addAll(lblTradition, cbTradition);
		
		getChildren().add(bxTradition);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbTradition.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> ctrl.getSpellController().changeMagicTradition(n));
	}

	//-------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		
		if (model.getTradition()!=null) {
			cbTradition.getSelectionModel().select(model.getTradition());
		} else {
			cbTradition.getSelectionModel().clearSelection();
		}
	}

}
