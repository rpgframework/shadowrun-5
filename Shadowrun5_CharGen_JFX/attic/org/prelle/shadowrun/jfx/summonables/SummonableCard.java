/**
 *
 */
package org.prelle.shadowrun.jfx.summonables;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun.ShadowrunCharacter;
import org.prelle.shadowrun.Summonable;
import org.prelle.shadowrun.SummonableValue;
import org.prelle.shadowrun.gen.event.GenerationEvent;
import org.prelle.shadowrun.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun.gen.event.GenerationEventListener;
import org.prelle.shadowrun.jfx.SR5Constants;

import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class SummonableCard extends GridPane implements GenerationEventListener {

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter     model;

	private Label headName, headRati, headServ;

	//-------------------------------------------------------------------
	/**
	 */
	public SummonableCard() {
		getStyleClass().addAll("table","chardata-tile");
		setStyle("-fx-padding: 0.4em");

		initComponents();
		initLayout();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		headName   = new Label(UI.getString("label.name"));
		headRati  = new Label(UI.getString("label.rating"));
		headServ   = new Label(UI.getString("label.services"));
		headName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headRati.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headServ.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		headName.getStyleClass().add("table-heading");
		headRati.getStyleClass().add("table-heading");
		headServ.getStyleClass().add("table-heading");

		headRati.setAlignment(Pos.CENTER_RIGHT);
		headServ.setAlignment(Pos.CENTER_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.add(headName  , 0,0);
		this.add(headRati , 1,0);
		this.add(headServ  , 2,0);


		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints maxGrow = new ColumnConstraints();
		maxGrow.setMaxWidth(Double.MAX_VALUE);
		maxGrow.setHgrow(Priority.ALWAYS);
		ColumnConstraints centerAlign = new ColumnConstraints();
		centerAlign.setHalignment(HPos.CENTER);
		ColumnConstraints centerAlign2 = new ColumnConstraints();
		centerAlign2.setHalignment(HPos.CENTER);

		constraints.add(maxGrow);
		constraints.add(centerAlign);
		constraints.add(centerAlign2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SUMMONABLE_ADDED:
		case SUMMONABLE_CHANGED:
		case SUMMONABLE_REMOVED:
			updateContent();
			break;
		default:
		}

	}

	//-------------------------------------------------------------------
	private void updateContent() {
		this.getChildren().retainAll(headName, headRati, headServ);

		int y=0;
		for (SummonableValue sVal : model.getSummonables()) {
			y++;

			Summonable skill = sVal.getSummonable();
			Label lblName  = new Label(skill.getName());
			Label lblRati  = new Label(sVal.getRating()+"");
			Label lblServ  = new Label(String.valueOf(sVal.getServices()));

			String lineStyle = ((y%2)==0)?"even":"odd";
			lblName.getStyleClass().addAll(lineStyle);
			lblRati.getStyleClass().add(lineStyle);
			lblServ.getStyleClass().add(lineStyle);

			lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

			this.add(lblName, 0, y);
			this.add(lblRati, 1, y);
			this.add(lblServ, 2, y);
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
	}

}
