/**
 * @author Stefan Prelle
 *
 */
module shadowrun.chargen.jfx {
	exports org.prelle.shadowrun5.jfx.summonables;
	exports org.prelle.shadowrun5.jfx.newwizard;
	exports org.prelle.shadowrun5.jfx;
	exports org.prelle.shadowrun5.jfx.fluent;
	exports org.prelle.shadowrun5.jfx.items;
	exports org.prelle.shadowrun5.jfx.qualities;
	exports org.prelle.shadowrun5.jfx.skills;
	exports org.prelle.shadowrun5.jfx.develop;
	exports org.prelle.shadowrun5.jfx.connections;
	exports org.prelle.shadowrun5.jfx.matrix;
	exports org.prelle.shadowrun5.jfx.sins;
	exports org.prelle.shadowrun5.jfx.powers;
	exports org.prelle.shadowrun5.jfx.spells;
	exports org.prelle.shadowrun5.jfx.notes;
	exports org.prelle.shadowrun5.jfx.attributes;
	exports org.prelle.shadowrun5.jfx.items.input;
	exports org.prelle.shadowrun5.jfx.cforms;

	provides de.rpgframework.character.RulePlugin with org.prelle.shadowrun5.jfx.SR5GeneratorRulePlugin;

	requires java.prefs;
	requires javafx.base;
	requires javafx.controls;
	requires javafx.extensions;
	requires javafx.graphics;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.javafx;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun.chargen;
	requires shadowrun.core;
	requires simple.persist;
	requires shadowrun.data;
}