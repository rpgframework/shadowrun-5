package org.prelle.shadowrun5.chargen.jfx.dialogs;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.jfx.sins.LifestyleOptionListCell;
import org.prelle.shadowrun5.jfx.sins.LifestyleOptionValueListCell;
import org.prelle.shadowrun5.Lifestyle;
import org.prelle.shadowrun5.LifestyleOption;
import org.prelle.shadowrun5.LifestyleOptionValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.LifestyleController;
import org.prelle.shadowrun5.charctrl.SingleLifestyleController;
import org.prelle.shadowrun5.gen.SingleLifestyleGenerator;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EditLifestyleValueDialog extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EditLifestyleValueDialog.class.getName());

	private CharacterController control;
	private SingleLifestyleController sinControl;
	private NavigButtonControl btnControl;
	private LifestyleValue data;

	private Label    lblNuyenFree;
	private TextField tfName;
	private ChoiceBox<Lifestyle> cbLifestyle;
	private ChoiceBox<SIN> cbSINs;
	private Label    lblBaseCost;
	private Label    lblTotalCost;
	private ListView<LifestyleOption> lvAvailable;
	private ListView<LifestyleOptionValue> lvSelected;
	private Label lblName;
	private Label lblProduct;
	private Label lblDesc;
	private TextArea taDesc;

	//-------------------------------------------------------------------
	public EditLifestyleValueDialog(CharacterController control, LifestyleValue value, boolean isEdit) {
		super(RES.getString("dialog.title"), null, CloseType.APPLY);
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		}
		this.control = control;
		this.sinControl = (isEdit)?(new SingleLifestyleGenerator(control.getCharacter(), value)):(new SingleLifestyleGenerator(control.getCharacter()));
		this.data    = value;
		btnControl = new NavigButtonControl();

		initComponents();
		initLayout();
		lblNuyenFree.setText(control.getCharacter().getNuyen()+" ");

		initInteractivity();
		if (isEdit)
			refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbLifestyle = new ChoiceBox<>();
		cbLifestyle.getItems().addAll(sinControl.getAvailableLifestyles());
		cbLifestyle.setConverter(new StringConverter<Lifestyle>() {
			public String toString(Lifestyle data) { return data.getName(); }
			public Lifestyle fromString(String string) { return null; }
		});
		cbSINs = new ChoiceBox<>();
		cbSINs.getItems().addAll(control.getCharacter().getSINs());
		cbSINs.setConverter(new StringConverter<SIN>() {
			public String toString(SIN data) { return data.getName(); }
			public SIN fromString(String string) { return null; }
		});
		taDesc = new TextArea();
		lblBaseCost= new Label();
		lblTotalCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.getStyleClass().add("text-header");

		lvAvailable = new ListView<LifestyleOption>();
		lvAvailable.setCellFactory(listview -> new LifestyleOptionListCell());
		lvSelected  = new ListView<LifestyleOptionValue>();
		lvSelected.setCellFactory(listview -> new LifestyleOptionValueListCell());
		lblName     = new Label("");
		lblName.getStyleClass().add("text-small-subheader");
		lblProduct  = new Label();
		lblDesc     = new Label("");
		lblDesc.setWrapText(true);
		lblDesc.setStyle("-fx-pref-width: 25em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblName2 = new Label(RES.getString("label.name"));
		Label lblQual = new Label(RES.getString("label.quality"));
		Label lblDesc2 = new Label(RES.getString("label.description"));
		Label heaBaseCost = new Label(RES.getString("label.cost.base"));
		Label heaTotalCost = new Label(RES.getString("label.cost.total"));
		Label heaSIN = new Label(RES.getString("lifestyles.new.sin"));

		HBox qualLine = new HBox(cbLifestyle, heaBaseCost, lblBaseCost, heaTotalCost, lblTotalCost);
		qualLine.setStyle("-fx-spacing: 1em");

		HBox sinLine = new HBox(cbSINs, heaSIN);
		sinLine.setStyle("-fx-spacing: 1em");

		VBox bxDesc = new VBox(20);
		bxDesc.getChildren().addAll(lblName, lblProduct, lblDesc);

		ThreeColumnPane threeCol = new ThreeColumnPane();
		threeCol.setColumn1Node(lvAvailable);
		threeCol.setColumn2Node(lvSelected);
		threeCol.setColumn3Node(bxDesc);

		VBox layout = new VBox();
		layout.getChildren().addAll(lblName2, tfName, lblQual, qualLine, sinLine, threeCol, lblDesc2, taDesc);
		VBox.setMargin(lblQual, new Insets(20,0,0,0));
		VBox.setMargin(lblDesc, new Insets(20,0,0,0));
		VBox.setMargin(threeCol, new Insets(20,0,0,0));
		VBox.setMargin(sinLine, new Insets(20,0,0,0));

		Label heaNuyen      = new Label(RES.getString("label.nuyen"));
		VBox points = new VBox();
		points.setAlignment(Pos.TOP_CENTER);
//		points.getStyleClass().add("section-bar");
		points.setStyle("-fx-pref-width: 12em");
		points.getChildren().addAll(heaNuyen, lblNuyenFree);

		HBox outerLayout = new HBox(points, layout);
		outerLayout.setStyle("-fx-spacing: 2em");
		
		setContent(outerLayout);
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : lvSelected.getItems())
			options.add(opt.getOption());

		int cost = ShadowrunTools.getLifestyleCost(control.getCharacter(), sinControl.getResult());
		btnControl.setDisabled(CloseType.OK, cost>control.getCharacter().getNuyen());
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbLifestyle.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				sinControl.selectLifestyle(n);
				updateOKButton();
				updateCost();
			}
		});
		cbSINs.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				sinControl.selectSIN(n);
				updateOKButton();
				updateCost();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> {sinControl.selectName(n); updateOKButton();});
		taDesc.textProperty().addListener( (ov,o,n) -> {sinControl.selectDescription(n); updateOKButton();});

		lvAvailable.setOnDragDropped(event -> dragDroppedSelected(event));
		lvAvailable.setOnDragOver   (event -> dragOverSelected(event));
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelp(n));
		lvSelected.setOnDragDropped (event -> dragDroppedAvailable(event));
		lvSelected.setOnDragOver    (event -> dragOverAvailable(event));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelp(n.getOption()));
	}

	//-------------------------------------------------------------------
	private void showHelp(LifestyleOption option) {
		if (option==null) {
			lblName.setText(null);
			lblProduct.setText(null);
			lblDesc.setText(null);
		} else {
			lblName.setText(option.getName());
			lblProduct.setText(option.getProductName()+" "+option.getPage());
			lblDesc.setText(option.getHelpText());
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		cbLifestyle.setValue(data.getLifestyle());
		cbLifestyle.setDisable(true);

		if (data.getSIN()!=null) {
			cbSINs.setValue(control.getCharacter().getSIN(data.getSIN()));
			cbSINs.setDisable(true);
		}

		lvAvailable.getItems().removeAll(data.getOptions());
		lvAvailable.setDisable(true);
		lvSelected.getItems().addAll(data.getOptions());
		lvSelected.setDisable(true);
		tfName.setText(data.getName());
		taDesc.setText(data.getDescription());
		btnControl.setDisabled(CloseType.OK, false);
		
		
		
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(sinControl.getAvailableOptions());
		lvSelected.getItems().clear();

		lblNuyenFree.setText(control.getCharacter().getNuyen()+" \u00A5");
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	private void updateCost() {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : lvSelected.getItems())
			options.add(opt.getOption());

		int cost = ShadowrunTools.getLifestyleCost(control.getCharacter(), sinControl.getResult());
		lblBaseCost.setText(cbLifestyle.getValue().getCost()+" \u00A5");
		lblTotalCost.setText(cost+" \u00A5");
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragDroppedAvailable(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("lifestyleoption:")) {
        		String id = enhanceID.substring("lifestyleoption:".length());
        		LifestyleOption master = ShadowrunCore.getLifestyleOption(id);
         		if (master!=null) {
         			LifestyleOptionValue added = sinControl.selectOption(master);
         			if (added!=null) {
         				lvAvailable.getItems().remove(master);
         				if (!lvSelected.getItems().contains(master))
         					lvSelected.getItems().add(added);
         				updateCost();
         			}
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("lifestyleoption:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Deselect
	 */
	private void dragDroppedSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("lifestyleoption:")) {
        		String id = enhanceID.substring("lifestyleoption:".length());
        		LifestyleOption master = ShadowrunCore.getLifestyleOption(id);
         		if (master!=null) {
         			lvSelected.getItems().remove(master);
         			if (!lvAvailable.getItems().contains(master))
         				lvAvailable.getItems().add(master);
    				updateCost();
         		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * Deselect
	 */
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("lifestyleoption:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbLifestyle.getValue(), taDesc.getText()};
	}

	//-------------------------------------------------------------------
	public Lifestyle getSelectedLifeStyle() {
		return cbLifestyle.getValue();
	}

	//-------------------------------------------------------------------
	public List<LifestyleOptionValue> getSelectedLifeStyleOptions() {
		return lvSelected.getItems();
	}

	//-------------------------------------------------------------------
	public String getSelectedName() {
		if (tfName.getText()!=null && !tfName.getText().isEmpty())
			return tfName.getText();
		return null;
	}

	//-------------------------------------------------------------------
	public String getSelectedDescription() {
		if (taDesc.getText()!=null && !taDesc.getText().isEmpty())
			return taDesc.getText();
		return null;
	}

	//-------------------------------------------------------------------
	public SIN getSelectedSIN() {
		return cbSINs.getValue();
	}
	
}
