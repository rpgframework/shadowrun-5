package org.prelle.shadowrun5.chargen.jfx.dialogs;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.SINController;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class EditSINDialog extends ManagedDialog {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EditSINDialog.class.getName());

	private CharacterController control;
	private SINController sinControl;
	private NavigButtonControl btnControl;
	private SIN data;

	private Label    lblNuyenFree;
	private TextField tfName;
	private ChoiceBox<SIN.Quality> cbQuality;
	private Label    lblCost;
	private TextArea taDesc;

	//-------------------------------------------------------------------
	public EditSINDialog(CharacterController control, SIN value, boolean isEdit) {
		super(RES.getString("dialog.title"), null, CloseType.APPLY, CloseType.CANCEL);
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		}
		this.control = control;
		this.sinControl = control.getSINController();
		this.data    = value;
		btnControl = new NavigButtonControl();

		initComponents();
		initLayout();
		lblNuyenFree.setText(control.getCharacter().getNuyen()+" ");

		initInteractivity();
		cbQuality.setValue(SIN.Quality.ANYONE);
		
		cbQuality.setDisable(isEdit);
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbQuality = new ChoiceBox<>();
		cbQuality.getItems().addAll(Quality.getSelectableValues());
		taDesc = new TextArea();
		lblCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.getStyleClass().add("text-header");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblName = new Label(RES.getString("label.name"));
		Label lblQual = new Label(RES.getString("label.quality"));
		Label lblDesc = new Label(RES.getString("label.description"));
		lblName.getStyleClass().add("base");
		lblDesc.getStyleClass().add("base");
		lblQual.getStyleClass().add("base");

		HBox qualLine = new HBox(cbQuality, lblCost);
		qualLine.setStyle("-fx-spacing: 1em");

		VBox layout = new VBox();
		layout.getChildren().addAll(lblName, tfName, lblQual, qualLine, lblDesc, taDesc);
		VBox.setMargin(lblQual, new Insets(20,0,0,0));
		VBox.setMargin(lblDesc, new Insets(20,0,0,0));

		Label heaNuyen      = new Label(RES.getString("label.nuyen"));
		VBox points = new VBox();
		points.setAlignment(Pos.TOP_CENTER);
		points.getStyleClass().add("section-bar");
		points.setStyle("-fx-pref-width: 12em");
		points.getChildren().addAll(heaNuyen, lblNuyenFree);

		HBox outerLayout = new HBox(points, layout);
		outerLayout.setStyle("-fx-spacing: 2em");
		
		setContent(outerLayout);
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		boolean enoughText = tfName.getText()!=null && tfName.getText().length()>0;
		boolean canSelect  = sinControl.canCreateNewSIN(cbQuality.getValue());
		btnControl.setDisabled(CloseType.APPLY, !(enoughText && canSelect));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbQuality.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				lblCost.setText("\u00A5 "+(2500*n.getValue()));
				updateOKButton();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> {data.setName(n); updateOKButton();});
		taDesc.textProperty().addListener( (ov,o,n) -> data.setDescription(n));
	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbQuality.getValue(), taDesc.getText()};
	}

	//-------------------------------------------------------------------
	private void refresh() {
		tfName.setText(data.getName());
		taDesc.setText(data.getDescription());
		cbQuality.setValue(data.getQuality());
		cbQuality.setDisable(data.getQuality()==Quality.REAL_SIN);
		lblNuyenFree.setText(control.getCharacter().getNuyen()+" ");
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}
	
}
