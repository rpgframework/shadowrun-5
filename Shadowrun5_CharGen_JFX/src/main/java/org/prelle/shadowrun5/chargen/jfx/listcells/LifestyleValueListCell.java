package org.prelle.shadowrun5.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.NumericalValueField;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.LifestyleController;
import org.prelle.shadowrun5.chargen.jfx.dialogs.EditLifestyleValueDialog;
import org.prelle.shadowrun5.chargen.jfx.sections.SINSection;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class LifestyleValueListCell extends ListCell<LifestyleValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SINSection.class.getName());
	
	private CharacterController control;
	private LifestyleController sinCtrl;
	private ScreenManagerProvider provider;
	
	private Button btnEdit;
	private Label  lbName;
	private Label  lbDesc;
	private Button btnDec, btnInc;
	private Label  lbPaid;
	
	private HBox layout;

	//-------------------------------------------------------------------
	public LifestyleValueListCell(CharacterController control, ScreenManagerProvider provider) {
		this.control = control;
		this.sinCtrl = control.getLifestyleController();
		this.provider= provider;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbDesc = new Label();
		lbDesc.setWrapText(true);
		lbPaid = new Label();
		lbPaid.setStyle("-fx-font-size: 200%");
		btnEdit = new Button(null, new SymbolIcon("edit"));
		btnDec = new Button(null, new SymbolIcon("CalculatorSubstract"));
		btnInc = new Button(null, new SymbolIcon("CalculatorAddition"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox colNameDesc = new VBox(lbName, lbDesc);
		colNameDesc.setStyle("-fx-spacing: 0.2em");
		colNameDesc.setMaxWidth(Double.MAX_VALUE);
		
		TilePane bxValue = new TilePane(Orientation.HORIZONTAL, btnDec, lbPaid, btnInc);
		bxValue.setPrefColumns(3);

		this.layout = new HBox(bxValue, colNameDesc, btnEdit);
		this.layout.setAlignment(Pos.CENTER_LEFT);
		this.layout.setStyle("-fx-spacing: 2em");
		HBox.setHgrow(colNameDesc, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(ev -> {
			EditLifestyleValueDialog dialog = new EditLifestyleValueDialog(control, this.getItem(), true);
			provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
			getListView().refresh();
		});
		
		btnDec.setOnAction(ev -> {
			sinCtrl.decreaseMonths(this.getItem());
			getListView().refresh();
		});
		btnInc.setOnAction(ev -> {
			sinCtrl.increaseMonths(this.getItem());
			getListView().refresh();
		});
	}

	//-------------------------------------------------------------------
	public void updateItem(LifestyleValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			
			lbName.setText(item.getName());
			
			String line2 = String.format("%s, %d Nuyen", item.getLifestyle().getName(), item.getCostPerMonth());
			lbDesc.setText(line2);
			lbPaid.setText(String.valueOf(item.getPaidMonths()));
			btnDec.setDisable(!sinCtrl.canDecreaseMonths(item));
			btnInc.setDisable(!sinCtrl.canIncreaseMonths(item));
//			if (item.getQuality()==Quality.REAL_SIN) {
//				lbQual.setText(RES.getString("label.quality.real"));
//			} else
//				lbQual.setText(String.valueOf(item.getLifestyle().get));
		}
	}

}
