/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.pages;

import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class SR5BodytechFirstLine extends SR5FirstLine {

	private Label lblEssRemain;
	private Label lblEssInvest;
	private Label lblEssHole;

	//-------------------------------------------------------------------
	public SR5BodytechFirstLine() {
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		super.initComponents();
		lblEssRemain = new Label("-");
		lblEssInvest = new Label("-");
		lblEssHole   = new Label("-");
		lblEssRemain.getStyleClass().add("base");
		lblEssInvest.getStyleClass().add("base");
		lblEssHole.getStyleClass().add("base");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		super.initLayout();
		Label heaEssRemain = new Label(SR5Constants.RES.getString("label.essence.remain"));
		Label heaEssInvest = new Label(SR5Constants.RES.getString("label.essence.invest"));
		Label heaEssHole   = new Label(SR5Constants.RES.getString("label.essence.hole"));

		getChildren().addAll(heaEssRemain, lblEssRemain, heaEssInvest, lblEssInvest, heaEssHole, lblEssHole);
		HBox.setMargin(heaEssRemain, new Insets(0,0,0,20));
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		super.setData(model);

		float essence = model.getUnusedEssence();
		float essInvest = model.getEssenceInvested();
		float hole = 6.0f - essInvest - essence;
		lblEssRemain.setText(String.format("%.2f", essence));
		lblEssInvest.setText(String.format("%.2f", essInvest));
		lblEssHole  .setText(String.format("%.2f", hole));
	}

}
