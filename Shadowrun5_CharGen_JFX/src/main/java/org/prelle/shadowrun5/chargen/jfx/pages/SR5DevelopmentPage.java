/**
 * 
 */
package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DevelopmentPage;
import org.prelle.shadowrun5.HistoryElementImpl;
import org.prelle.shadowrun5.RewardImpl;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.develop.RewardBox;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.ProductServiceLoader;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SR5DevelopmentPage extends DevelopmentPage {

	private static Logger logger = LogManager.getLogger("shadowrun.jfx");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ScreenManagerProvider provider;
	private CharacterController control;
	private ShadowrunCharacter model;

	private SR5FirstLine expLine;
	
	//-------------------------------------------------------------------
	/**
	 */
	public SR5DevelopmentPage(CharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(UI, RoleplayingSystem.SHADOWRUN);
		this.setId("shadowrun-development");
		this.setTitle(control.getCharacter().getName());
		this.provider = provider;
		this.control = control;
		history.setTitle("Development".toUpperCase());
		model = control.getCharacter();
		logger.info("<init>()");
		
		expLine = new SR5FirstLine();
		getCommandBar().setContent(expLine);
		setConverter(new StringConverter<Modification>() {
			public String toString(Modification arg0) {  return ShadowrunTools.getModificationString(arg0);}
			public Modification fromString(String arg0) {return null;}
		});
		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		logger.info("refresh");
		history.setData(ShadowrunTools.convertToHistoryElementList(model, super.shallBeAggregated()));
		expLine.setData(control.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(control.getCharacter().getKarmaFree());
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
//		setTitle(model.getName()+" / "+UI.getString("label.development"));
		
//		history.getItems().clear();
//		history.getItems().addAll(SplitterTools.convertToHistoryElementList(model));
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openAdd()
	 */
	@Override
	public HistoryElement openAdd() {
		RewardBox content = new RewardBox();
		logger.warn("TODO: openAdd");
		
		ManagedDialog dialog = new ManagedDialog(UI.getString("dialog.reward.title"), content, CloseType.OK, CloseType.CANCEL);
		
//		ManagedScreen screen = new ManagedScreen() {
//			@Override
//			public boolean close(CloseType closeType) {
//				logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
//				if (closeType==CloseType.OK) {
//					return dialog.hasEnoughData();
//				}
//				return true;
//			}
//		};
//		ManagedScreenDialogSkin skin = new ManagedScreenDialogSkin(screen);
//		screen.setTitle(UI.getString("dialog.reward.title"));
//		screen.setContent(dialog);
//		screen.getNavigButtons().addAll(CloseType.OK, CloseType.CANCEL);
//		screen.setSkin(skin);
//		screen.setOnAction(CloseType.OK, event -> manager.close(screen, CloseType.OK));
//		screen.setOnAction(CloseType.CANCEL, event -> manager.close(screen, CloseType.CANCEL));
//		skin.setDisabled(CloseType.OK, true);
//		dialog.enoughDataProperty().addListener( (ov,o,n) -> {
//			skin.setDisabled(CloseType.OK, !n);
//		});
		CloseType closed = (CloseType)provider.getScreenManager().showAndWait(dialog);
		
		if (closed==CloseType.OK) {
			RewardImpl reward = content.getDataAsReward();
			logger.debug("Add reward "+reward);
			ShadowrunTools.reward(model, reward);
			HistoryElementImpl elem = new HistoryElementImpl();
			elem.setName(reward.getTitle());
			elem.addGained(reward);
			if (reward.getId()!=null)
				elem.setAdventure(ProductServiceLoader.getInstance().getAdventure(RoleplayingSystem.SPLITTERMOND, reward.getId()));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, null));
			return elem;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openEdit(de.rpgframework.genericrpg.HistoryElement)
	 */
	@Override
	public boolean openEdit(HistoryElement elem) {
		logger.warn("TODO: openEdit");
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getScreenManager().showAlertAndCall(
					AlertType.ERROR, 
					UI.getString("error.not-possible"), 
					UI.getString("error.only-single-rewards-editable"));
			return false;
		}
		
		Reward toEdit = elem.getGained().get(0);
		RewardBox dialog = new RewardBox(toEdit);
		
//		ManagedScreen screen = new ManagedScreen() {
//			@Override
//			public boolean close(CloseType closeType) {
//				logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
//				if (closeType==CloseType.OK) {
//					return dialog.hasEnoughData();
//				}
//				return true;
//			}
//		};
//		ManagedScreenDialogSkin skin = new ManagedScreenDialogSkin(screen);
//		screen.setTitle(UI.getString("dialog.reward.title"));
//		screen.setContent(dialog);
//		screen.getNavigButtons().addAll(CloseType.OK, CloseType.CANCEL);
//		screen.setSkin(skin);
//		screen.setOnAction(CloseType.OK, event -> manager.close(screen, CloseType.OK));
//		screen.setOnAction(CloseType.CANCEL, event -> manager.close(screen, CloseType.CANCEL));
//		CloseType closed = (CloseType)manager.showAndWait(screen);
//		
//		if (closed==CloseType.OK) {
//			RewardImpl reward = dialog.getDataAsReward();
//			logger.debug("Copy edited data: ORIG = "+toEdit);
//			toEdit.setTitle(reward.getTitle());
//			toEdit.setId(reward.getId());
//			toEdit.setDate(reward.getDate());
//			// Was single reward. Changes in reward title, change element title
//			((HistoryElementImpl)elem).setName(reward.getTitle());
//			ProductService sessServ = RPGFrameworkLoader.getInstance().getProductService();
//			if (reward.getId()!=null)  {
//				Adventure adv = sessServ.getAdventure(RoleplayingSystem.SPLITTERMOND, reward.getId());
//				if (adv==null) {
//					logger.warn("Reference to an unknown adventure: "+reward.getId());
//				} else
//					((HistoryElementImpl)elem).setAdventure(adv);
//
//			}
////			((HistoryElementImpl)elem).set(reward.getTitle());
//			logger.debug("Copy edited data: NEW  = "+toEdit);
//			logger.debug("Element now   = "+elem);
//			return true;
//		}
		return false;
	}

}