package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class SR5EquipmentPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR5Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR5EquipmentPage.class.getName());

	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SR5FirstLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private GearSection  cyberware;
	private GearSection  bioware;
	private GearSection  weapons;
	private GearSection  armor;
	private GearSection  otherGear;
	
	private Section secAugmentation;
	private Section secArmorAndOther;

	//-------------------------------------------------------------------
	public SR5EquipmentPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("shadowrun-overview");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		this.provider = provider;

		firstLine = new SR5FirstLine();
		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new SR5FirstLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initAugmentation() {
		cyberware = new GearSection(UI.getString("section.cyberware"), control, provider, ItemType.CYBERWARE, ItemType.NANOWARE);
		bioware   = new GearSection(UI.getString("section.bioware"  ), control, provider, ItemType.BIOLOGY, ItemType.GENETICS);

		secAugmentation = new DoubleSection(cyberware, bioware);
		getSectionList().add(secAugmentation);

		// Interactivity
		cyberware.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		bioware  .showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initWeapons() {
		weapons = new GearSection(UI.getString("section.weapons"), control, provider, ItemType.WEAPON);

		getSectionList().add(weapons);

		// Interactivity
		weapons.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initArmorAndOther() {
		armor = new GearSection(UI.getString("section.armor"), control, provider, ItemType.ARMOR);
		otherGear = new GearSection(UI.getString("section.other"), control, provider, ItemType.CHEMICALS, ItemType.SURVIVAL, ItemType.BIOLOGY);

		secArmorAndOther = new DoubleSection(armor, otherGear);
		getSectionList().add(secArmorAndOther);

		// Interactivity
		armor.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		otherGear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initAugmentation();
		initWeapons();
		initArmorAndOther();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());

		cyberware.refresh();
		bioware.refresh();
		weapons.refresh();
		armor.refresh();
		otherGear.refresh();
	}

}
