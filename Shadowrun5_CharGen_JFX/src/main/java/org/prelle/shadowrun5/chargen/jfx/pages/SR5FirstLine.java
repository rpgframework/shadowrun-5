/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.pages;

import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class SR5FirstLine extends HBox {

	private Label lbExpTotal;
	private Label lbExpInvested;
	private Label lbNuyen;
	private Label lbEssence;
	private Label lbEssenceHole;

	//-------------------------------------------------------------------
	public SR5FirstLine() {
		super(5);
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lbExpTotal    = new Label("?");
		lbExpInvested = new Label("?");
		lbNuyen       = new Label("?");
		lbEssenceHole = new Label("?");
		lbEssence     = new Label("?");
		lbExpTotal.getStyleClass().add("base");
		lbExpInvested.getStyleClass().add("base");
		lbNuyen.getStyleClass().add("base");
		lbEssenceHole.getStyleClass().add("base");
		lbEssence.getStyleClass().add("base");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Label hdExpTotal    = new Label(SR5Constants.RES.getString("label.ep.total")+": ");
		Label hdExpInvested = new Label(SR5Constants.RES.getString("label.ep.used")+": ");
		Label hdNuyen       = new Label(SR5Constants.RES.getString("label.nuyen")+": ");
		Label hdEssenceHole = new Label(SR5Constants.RES.getString("label.essencehole")+": ");
		Label hdEssence     = new Label(SR5Constants.RES.getString("label.essence")+": ");

		getChildren().addAll(hdExpTotal, lbExpTotal, hdExpInvested, lbExpInvested, hdNuyen, lbNuyen);
		getChildren().addAll(hdEssence, lbEssence, hdEssenceHole, lbEssenceHole);
		HBox.setMargin(hdNuyen, new Insets(0,0,0,20));
		HBox.setMargin(hdEssence, new Insets(0,0,0,20));
//		getStyleClass().add("character-document-view-firstline");
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		lbExpTotal.setText((model.getKarmaInvested()+model.getKarmaFree())+"");
		lbExpInvested.setText(model.getKarmaInvested()+"");
		lbNuyen.setText(model.getNuyen()+"");

		lbEssence.setText(String.format("%.2f",6.0 - model.getUnusedEssence()));
		lbEssenceHole.setText(String.format("%.2f",6.0 - model.getUnusedEssence() - model.getEssenceInvested()));
	}

}
