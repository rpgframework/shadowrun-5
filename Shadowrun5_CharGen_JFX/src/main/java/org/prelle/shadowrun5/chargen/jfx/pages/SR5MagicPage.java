package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun5.chargen.jfx.sections.PowersSection;
import org.prelle.shadowrun5.chargen.jfx.sections.RitualSection;
import org.prelle.shadowrun5.chargen.jfx.sections.SpellSection;
import org.prelle.shadowrun5.chargen.jfx.sections.TraditionSection;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class SR5MagicPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR5Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR5MagicPage.class.getName());

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SR5FirstLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private TraditionSection tradition;
	private SpellSection spellsNormal;
	private SpellSection spellsAlchemistic;
	private PowersSection  powers;
	private RitualSection rituals;
	private GearSection  gear;
	
	private Section secSpells;
	private Section secRitualsAndGear;

	//-------------------------------------------------------------------
	public SR5MagicPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("shadowrun-overview");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		this.provider = provider;
		model = control.getCharacter();

		firstLine = new SR5FirstLine();
		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new SR5FirstLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initTradition() {
		tradition = new TraditionSection(UI.getString("section.tradition"), control, provider);

		getSectionList().add(tradition);

		// Interactivity
		tradition.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initSpells() {
		spellsNormal = new SpellSection(UI.getString("section.spells.normal"), control, false, provider);
		spellsAlchemistic = new SpellSection(UI.getString("section.spells.alchemistic"), control, true, provider);

		secSpells = new DoubleSection(spellsNormal, spellsAlchemistic);
		getSectionList().add(secSpells);

		// Interactivity
		spellsNormal.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		spellsAlchemistic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initPowers() {
		powers = new PowersSection(UI.getString("section.powers"), control, provider);
		getSectionList().add(powers);

		// Interactivity
		powers.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initRitualsAndGear() {
		rituals = new RitualSection(UI.getString("section.rituals"), control, provider);
		gear    = new GearSection(UI.getString("section.gear"), control, provider, ItemType.MAGICAL);

		secRitualsAndGear = new DoubleSection(rituals, gear);
		getSectionList().add(secRitualsAndGear);

		// Interactivity
		rituals.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		gear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initTradition();
		initSpells();
		initPowers();
		initRitualsAndGear();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());

		spellsNormal.refresh();
		spellsAlchemistic.refresh();
		rituals.refresh();
		gear.refresh();
	}

}
