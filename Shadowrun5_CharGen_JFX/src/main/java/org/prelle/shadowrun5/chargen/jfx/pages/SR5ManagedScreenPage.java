package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import de.rpgframework.character.CharacterHandle;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class SR5ManagedScreenPage extends CharacterDocumentView {

	protected static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);
	
	protected static PropertyResourceBundle UI = SR5Constants.RES;

	protected CharacterController charGen;
	protected CharacterHandle handle;

	protected MenuItem cmdPrint;
	protected MenuItem cmdDelete;
	
	protected SR5FirstLine expLine;

	//-------------------------------------------------------------------
	public SR5ManagedScreenPage(CharacterController charGen, CharacterHandle handle) {
		super();
		this.charGen = charGen;
		initPrivateComponents();
	}

	//-------------------------------------------------------------------
	private void initPrivateComponents() {
		/*
		 * Exp & Co.
		 */
		setPointsNameProperty(UI.getString("label.karma"));
		setPointsFree(charGen.getCharacter().getKarmaFree());
		expLine = new SR5FirstLine();
		expLine.setData(charGen.getCharacter());
		
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		setHandle(handle);
		
		getCommandBar().setContent(expLine);
	}

	//-------------------------------------------------------------------
	protected void setHandle(CharacterHandle value) {
		if (this.handle==value)
			return;
		if (this.handle!=null) {
			getCommandBar().getPrimaryCommands().removeAll(cmdPrint, cmdDelete);			
		}
		
		this.handle = value;
		if (handle!=null) {
			getCommandBar().getPrimaryCommands().addAll(cmdPrint, cmdDelete);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		expLine.setData(charGen.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(charGen.getCharacter().getKarmaFree());
	}

}
