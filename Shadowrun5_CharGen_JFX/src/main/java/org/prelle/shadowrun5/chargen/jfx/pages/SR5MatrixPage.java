package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class SR5MatrixPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR5Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR5MatrixPage.class.getName());

	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SR5FirstLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private GearSection  electronics;
	
	private Section secAugmentation;
	private Section secArmorAndOther;

	//-------------------------------------------------------------------
	public SR5MatrixPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("shadowrun-overview");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		this.provider = provider;

		firstLine = new SR5FirstLine();
		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new SR5FirstLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initElectronics() {
		electronics = new GearSection(UI.getString("section.electronics"), control, provider, ItemType.ELECTRONICS);

		getSectionList().add(electronics);

		// Interactivity
		electronics.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initElectronics();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());

		electronics.refresh();
	}

}
