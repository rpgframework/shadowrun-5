package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.AttributeSection;
import org.prelle.shadowrun5.chargen.jfx.sections.BasicDataSection;
import org.prelle.shadowrun5.chargen.jfx.sections.PortraitSection;
import org.prelle.shadowrun5.chargen.jfx.sections.QualitySection;
import org.prelle.shadowrun5.chargen.jfx.sections.SkillSection;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SR5OverviewPage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR5Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR5OverviewPage.class.getName());

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SR5FirstLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private BasicDataSection basic;
	private PortraitSection     portrait;
	private AttributeSection    attributes;
	private QualitySection      qualities;
	private SkillSection   skills;

	private Section secBasic;
	private Section secAttr;
	private Section secSkills;

	//-------------------------------------------------------------------
	public SR5OverviewPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("shadowrun-overview");
		this.setTitle(control.getCharacter().getName());
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		this.provider = provider;
		model = control.getCharacter();

		firstLine = new SR5FirstLine();
		initComponents();
		initLayout();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new SR5FirstLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
		
	}

	//-------------------------------------------------------------------
	private void initBasicData() {
		basic = new BasicDataSection(UI.getString("section.basedata"), control, mode, provider);
		portrait = new PortraitSection(UI.getString("section.portrait"), control, provider);

		secBasic = new DoubleSection(basic, portrait);
		getSectionList().add(secBasic);

		// Interactivity
		basic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initAttributes() {
		attributes = new AttributeSection(UI.getString("section.attributes"), control, mode, provider);
		qualities = new QualitySection(UI.getString("section.qualities"), control, provider);

		secAttr = new DoubleSection(attributes, qualities);
		getSectionList().add(secAttr);

		// Interactivity
//		attributes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> updateHelp(n));
		qualities.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initSkills() {
		skills = new SkillSection(UI.getString("section.skills"), control, provider);

//		secSkills= new SingleSection(UI.getString("section.skills").toUpperCase(), skills, provider);
		getSectionList().add(skills);

		// Interactivity
		skills.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));


		initBasicData();
		initAttributes();
		initSkills();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//-------------------------------------------------------------------
	private void updateHelp(AttributeValue data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getModifyable().getName());
//			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
//			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());
		setTitle(control.getCharacter().getName());

		basic.refresh();
		
		if (attributes!=null) attributes.refresh();
		if ( qualities!=null)  qualities.refresh();
		if     (skills!=null)     skills.refresh();
	}

}
