package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.CharacterDocumentView;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Resource;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.ComplexFormSection;
import org.prelle.shadowrun5.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun5.chargen.jfx.sections.PowersSection;
import org.prelle.shadowrun5.chargen.jfx.sections.RitualSection;
import org.prelle.shadowrun5.chargen.jfx.sections.SpellSection;
import org.prelle.shadowrun5.chargen.jfx.sections.TraditionSection;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class SR5ResonancePage extends CharacterDocumentView {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR5Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR5ResonancePage.class.getName());

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SR5FirstLine firstLine;
	private MenuItem cmdPrint;
	private MenuItem cmdDelete;

	private ComplexFormSection complexForms;
	
	private Section secSpells;

	//-------------------------------------------------------------------
	public SR5ResonancePage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		this.setId("shadowrun-overview");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		this.provider = provider;
		model = control.getCharacter();

		firstLine = new SR5FirstLine();
		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new SR5FirstLine();
		getCommandBar().setContent(firstLine);
		cmdPrint  = new MenuItem(Resource.get(UI, "command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(Resource.get(UI, "command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initSpells() {
		complexForms = new ComplexFormSection(Resource.get(UI, "section.complexforms"), control, provider);

		getSectionList().add(complexForms);

		// Interactivity
		complexForms.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initSpells();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, control.getCharacter()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		setPointsFree(control.getCharacter().getKarmaFree());
		firstLine.setData(control.getCharacter());

		complexForms.refresh();
		
		MagicOrResonanceType magic = control.getCharacter().getMagicOrResonanceType();
		if (magic!=null) {
			if (magic.usesResonance()) {
				complexForms.setVisible(true);
			} else if (magic.usesPowers()) {
				complexForms.setVisible(false);
			}
		}
	}

}
