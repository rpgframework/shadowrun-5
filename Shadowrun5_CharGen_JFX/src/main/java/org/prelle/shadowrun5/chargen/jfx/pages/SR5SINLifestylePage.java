package org.prelle.shadowrun5.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.sections.LifestyleSection;
import org.prelle.shadowrun5.chargen.jfx.sections.SINSection;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;

/**
 * @author Stefan Prelle
 *
 */
public class SR5SINLifestylePage extends SR5ManagedScreenPage {

	private ScreenManagerProvider provider;

	private SINSection sins;
	private LifestyleSection lifestyles;
	
	private Section secLine1;
	
	//-------------------------------------------------------------------
	/**
	 * @param charGen
	 * @param handle
	 */
	public SR5SINLifestylePage(CharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("shadowrun-life");
		this.setTitle(control.getCharacter().getName());
		this.provider = provider;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		sins = new SINSection(UI.getString("section.sins"), charGen, provider);
		lifestyles = new LifestyleSection(UI.getString("section.lifestyles"), charGen, provider);

		secLine1 = new DoubleSection(sins, lifestyles);
		getSectionList().add(secLine1);

		// Interactivity
//		sins.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getSIN()); else updateHelp(null); });
		lifestyles.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getLifestyle()); else updateHelp(null); });
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		initLine1();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getCharacter());});
//		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.DELETE_REQUESTED, handle, charGen.getModel()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		secLine1.getToDoList().clear();
		for (String tmp : charGen.getSINController().getToDos()) {
			secLine1.getToDoList().add(new ToDoElement(Severity.STOPPER, tmp));
		}
	}

}
