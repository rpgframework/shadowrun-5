package org.prelle.shadowrun5.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.Resource;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class ComplexFormDescriptionPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormSelectionPane.class.getName());
	
	private Label descHeading;
	private Label descRef;
	private Label descDur, descFade;
	private Label description;

	//-------------------------------------------------------------------
	public ComplexFormDescriptionPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descDur     = new Label();
		descFade   = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblDur   = new Label(Resource.get(UI, "label.duration")+":");
		Label lblDrain = new Label(Resource.get(UI, "label.fading")+":");
		lblDur.setStyle("-fx-font-weight: bold");
		lblDrain.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblDur   , 0, 0);
		descGrid.add(descDur   , 1, 0);
		descGrid.add( lblDrain , 0, 1);
		descGrid.add(descFade  , 1, 1);
		descGrid.setHgap(5);
		
		setSpacing(10);
		getChildren().addAll(descHeading, descRef, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	public void setData(ComplexForm data) {
		if (data==null) {
			descHeading.setText(null);
			descRef.setText(null);
			descDur .setText(null);
			descFade.setText(null);
			description.setText(null);
		} else {
			descHeading.setText(data.getName().toUpperCase());
			descRef.setText(data.getProductName()+" "+data.getPage());
			descDur .setText(data.getDuration().getName());
			 if (data.getFading()>0)
				descFade.setText(String.valueOf(data.getFading()));
			else 
				descFade.setText("-");

			description.setText(data.getHelpText());
		}
	}

}
