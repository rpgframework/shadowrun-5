/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.Resource;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.jfx.cforms.ComplexFormListCell;
import org.prelle.shadowrun5.jfx.cforms.ComplexFormValueListCell;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class ComplexFormSelectionPane extends HBox {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormSelectionPane.class.getName());

	private ComplexFormController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<ComplexForm> lvAvailable;
	private ListView<ComplexFormValue> lvSelected;
	private Button btnDelKnow;

	private ObjectProperty<ComplexForm> showHelpFor = new SimpleObjectProperty<ComplexForm>();

	//-------------------------------------------------------------------
	/**
	 */
	public ComplexFormSelectionPane(CharacterController charGen, ScreenManagerProvider provider) {
		this.ctrl = charGen.getComplexFormController();
		this.model = charGen.getCharacter();
		this.provider = provider;
		if (ctrl==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lvAvailable = new ListView<ComplexForm>();
		lvAvailable.setCellFactory(new Callback<ListView<ComplexForm>, ListCell<ComplexForm>>() {
			public ListCell<ComplexForm> call(ListView<ComplexForm> param) {
				ComplexFormListCell cell =  new ComplexFormListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		lvSelected  = new ListView<ComplexFormValue>();
		lvSelected.setCellFactory(new Callback<ListView<ComplexFormValue>, ListCell<ComplexFormValue>>() {
			public ListCell<ComplexFormValue> call(ListView<ComplexFormValue> param) {
				ComplexFormValueListCell cell =  new ComplexFormValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});

		Label phAvailable = new Label(UI.getString("placeholder.available"));
		Label phSelected  = new Label(UI.getString("placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdAvailable = new Label(Resource.get(UI, "label.available"));
		Label hdSelected  = new Label(Resource.get(UI, "label.selected"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdSelected.setStyle("-fx-font-size: 130%");

		lvAvailable.setStyle("-fx-pref-width: 20em;");
		lvSelected .setStyle("-fx-pref-width: 21em");


		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);

		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		Region grow2 = new Region();
		grow2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow2, Priority.ALWAYS);
		HBox lineKnow = new HBox(5, grow2, btnDelKnow);
		column2.getChildren().addAll(hdSelected, lineKnow, lvSelected);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected .setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);

		HBox.setHgrow(lvAvailable, Priority.SOMETIMES);
		HBox.setHgrow(lvSelected.getParent(), Priority.SOMETIMES);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set((n!=null)?n.getModifyable():null));
		lvAvailable.setOnDragDropped(event -> droppedOnAvailable(event));
		lvAvailable.setOnDragOver   (event -> dragOverAvailable(event));
		lvSelected.setOnDragDropped(event -> dragFromCFormDropped(event));
		lvSelected.setOnDragOver(event -> dragFromCFormOver(event));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		logger.info("refresg  "+model.getComplexForms());
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableComplexForms());

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getComplexForms());
	}

	//-------------------------------------------------------------------
	private void dragFromCFormDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("complexform")) {
					ComplexForm data = ShadowrunCore.getComplexForm(tail);
					if (data==null) {
						logger.warn("Cannot find complex form for dropped id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						// Special handling for knowledge and language skills
//						if (data.getType()==SkillType.KNOWLEDGE || data.getType()==SkillType.LANGUAGE) {
//							logger.debug(data.getName()+" must be entered by user");
//							String name = askForSkillName();
//							if (name!=null) {
//								ctrl.selectKnowledgeOrLanguage(data, name);
//							}
//						} else {
							ctrl.select(data);
//						}
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromCFormOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	private ComplexFormValue getAdeptPowerValueByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.equals("power")) {
				StringTokenizer tok = new StringTokenizer(tail,":");
				tail = tok.nextToken();
//				String uuid = (tok.hasMoreTokens())?tok.nextToken():null;
				ComplexFormValue pVal = null;
				for (ComplexFormValue tmp : model.getComplexForms()) {
					if (tmp.getModifyable().getId().equals(tail)) {
						pVal = tmp;
						break;
					}
//						continue;
//					if (uuid==null || tmp.getUniqueId().toString().equals(uuid)) {
//						pVal = tmp;
//						break;
//					}
				}
				return pVal;
			} 
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void droppedOnAvailable(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			logger.debug("Dropped "+db.getString());
			ComplexFormValue data = getAdeptPowerValueByEvent(event);
			if (data==null) {
				logger.warn("Cannot find complex form for dropped power");						
//			} else if (!data.isFreeSelectable()) {
//				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
//				if (model.hasAdeptPower(data.getId())) {
//					AdeptPowerValue ref = model.getAdeptPower(data.getId());
				logger.info("Deselect "+data);
					ctrl.deselect(data);
//				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			ComplexFormValue qual = getAdeptPowerValueByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<ComplexForm> showHelpForProperty() {
		return showHelpFor;
	}

}
