/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.sections;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.ViewMode;
import org.prelle.shadowrun5.jfx.fluent.AttributeTable;
import org.prelle.shadowrun5.jfx.fluent.DerivedAttributeTable;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class AttributeSection extends SingleSection {

	private CharacterController control;
	private ViewMode mode;
	private ShadowrunCharacter model;

	private AttributeTable table;
	private DerivedAttributeTable derived;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public AttributeSection(String title, CharacterController ctrl, ViewMode mode, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		this.mode = mode;
		model = ctrl.getCharacter();

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		table = new AttributeTable(control, mode);
		derived = new DerivedAttributeTable(control);
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		HBox layout = new HBox(table, derived);
		layout.setStyle("-fx-spacing: 1em");
		
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getAttribute());});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		table.refresh();
		derived.refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
