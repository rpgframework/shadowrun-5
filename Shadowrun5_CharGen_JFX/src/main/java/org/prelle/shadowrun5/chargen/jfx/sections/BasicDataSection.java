/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.WindowMode;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunCharacter.Gender;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class BasicDataSection extends SingleSection implements ResponsiveControl {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;
	private ViewMode mode;
	private ShadowrunCharacter model;

	private TextField tfStreetName;
	private TextField tfName;
	private Node      cbMetatype;
	private Node      cbMagORRes;
	private TextField tfAge;
	private ChoiceBox<Gender> cbGender;
	private TextField tfSize;
	private TextField tfWeight;
	private TextField tfStCred;
	private TextField tfNotor;
	private TextField tfPublAw;

	private Label hdName;
	private Label hdStreetName;
	private Label hdMetatype;
	private Label hdMagORRes;
	private Label hdAge;
	private Label hdGender;
	private Label hdSize;
	private Label hdWeight;
	private Label hdStCred;
	private Label hdNotor;
	private Label hdPublAw;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public BasicDataSection(String title, CharacterController ctrl, ViewMode mode, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		this.mode = mode;
		model = ctrl.getCharacter();

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfStreetName = new TextField();
		tfName = new TextField();
		if (mode==ViewMode.GENERATION) {
			cbMetatype = new ChoiceBox<MetaType>();
			((ChoiceBox<MetaType>)cbMetatype).getItems().addAll(ShadowrunCore.getMetaTypes());
			cbMagORRes = new ChoiceBox<MagicOrResonanceType>();
			((ChoiceBox<MagicOrResonanceType>)cbMagORRes).getItems().addAll(ShadowrunCore.getMagicOrResonanceTypes());
		} else {
			cbMetatype = new Label();
			cbMagORRes = new Label();
		}
		tfAge    = new TextField();
		tfAge.setPrefColumnCount(3);
		cbGender = new ChoiceBox<Gender>();
		cbGender.getItems().addAll(Gender.values());
		tfSize   = new TextField();
		tfSize.setPrefColumnCount(3);
		tfWeight = new TextField();
		tfWeight.setPrefColumnCount(3);
		tfStCred = new TextField();
		tfStCred.setPrefColumnCount(3);
		tfNotor = new TextField();
		tfNotor.setPrefColumnCount(2);
		tfPublAw = new TextField();
		tfPublAw.setPrefColumnCount(2);

		hdStreetName = new Label(UI.getString("label.streetname"));
		hdStreetName.getStyleClass().add("base");
		hdName = new Label(UI.getString("label.name"));
		hdName.getStyleClass().add("base");
		hdMetatype = new Label(UI.getString("label.metatype"));
		hdMetatype.getStyleClass().add("base");
		hdMagORRes = new Label(UI.getString("label.magicOrResonance"));
		hdMagORRes.getStyleClass().add("base");
		hdAge       = new Label(UI.getString("label.age"));
		hdGender    = new Label(UI.getString("label.gender"));
		hdSize      = new Label(UI.getString("label.size"));
		hdWeight    = new Label(UI.getString("label.weight"));
		hdStCred    = new Label(Attribute.STREET_CRED.getName());
		hdNotor     = new Label(Attribute.NOTORIETY.getName());
		hdPublAw    = new Label(Attribute.PUBLIC_AWARENESS.getName());
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		HBox.setHgrow(tfName, Priority.ALWAYS);
		HBox.setHgrow(tfStreetName, Priority.ALWAYS);

		GridPane grid = new GridPane();
		grid.add(hdStreetName  , 0, 0);
		grid.add(tfStreetName  , 1, 0, 3,1);
		grid.add(hdName        , 4, 0, 2,1);
		grid.add(tfName        , 6, 0, 2,1);
		grid.add(hdMetatype    , 0, 1);
		grid.add(cbMetatype    , 1, 1, 3,1);
		grid.add(hdMagORRes    , 4, 1, 2,1);
		grid.add(cbMagORRes    , 6, 1, 2,1);
		grid.add(hdAge         , 0, 2);
		grid.add(tfAge         , 1, 2);
		grid.add(hdGender      , 2, 2);
		grid.add(cbGender      , 3, 2);
		grid.add(hdSize        , 4, 2);
		grid.add(tfSize        , 5, 2);
		grid.add(hdWeight      , 6, 2);
		grid.add(tfWeight      , 7, 2);
		grid.add(hdStCred      , 0, 3);
		grid.add(tfStCred      , 1, 3);
		grid.add(hdNotor       , 2, 3, 2,1);
		grid.add(tfNotor       , 4, 3);
		grid.add(hdPublAw      , 5, 3, 2,1);
		grid.add(tfPublAw      , 7, 3);
		grid.setStyle("-fx-vgap: 0.5em; -fx-padding: 0.3em; -fx-hgap: 0.5em");

		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initLayoutMinimal() {
		VBox grid = new VBox(5);
		grid.getChildren().addAll(hdStreetName, tfStreetName);
		grid.getChildren().addAll(hdName, tfName);
		grid.setStyle("-fx-background-color: lightgrey; -fx-spacing: 0.5em; -fx-padding: 0.3em");
		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {
			control.getCharacter().setRealName(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
			});
		tfStreetName.textProperty().addListener( (ov,o,n) -> {
			control.getCharacter().setName(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
			});
		tfAge.textProperty().addListener( (ov,o,n) -> {
			control.getCharacter().setAge(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
			});
		tfSize.textProperty().addListener( (ov,o,n) -> {
			try {
				control.getCharacter().setSize(Integer.parseInt(n));
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
			} catch (Exception e) {
			}
			});
		tfWeight.textProperty().addListener( (ov,o,n) -> {
			try {
				control.getCharacter().setWeight(Integer.parseInt(n));
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
			} catch (Exception e) {
			}
			});
		cbGender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			control.getCharacter().setGender(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, control.getCharacter()));
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @param value
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.debug("Set mode to "+value);
		switch (value) {
		case COMPACT:
		case EXPANDED:
			initLayoutNormal();
			break;
		case MINIMAL:
			initLayoutMinimal();
			break;
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		tfStreetName.setText(model.getName());
		tfName.setText(model.getRealName());
		tfAge.setText(model.getAge());
		cbGender.getSelectionModel().select(model.getGender());
		tfWeight.setText(String.valueOf(model.getWeight()));
		tfSize.setText(String.valueOf(model.getSize()));

		if (mode==ViewMode.GENERATION) {
			((ChoiceBox<MetaType>)cbMetatype).getSelectionModel().select(model.getMetatype());
			((ChoiceBox<MagicOrResonanceType>)cbMagORRes).getSelectionModel().select(model.getMagicOrResonanceType());
		} else {
			((Label)cbMetatype).setText(model.getMetatype().getName());
			((Label)cbMagORRes).setText(model.getMagicOrResonanceType().getName());
		}
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
