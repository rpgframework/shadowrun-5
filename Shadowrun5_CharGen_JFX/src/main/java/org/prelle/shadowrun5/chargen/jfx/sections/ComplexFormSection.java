package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.Resource;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.cforms.ComplexFormValueListCell;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class ComplexFormSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(ComplexFormSection.class.getName());

	private CharacterController ctrl;
	private ListView<ComplexFormValue> listN;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public ComplexFormSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  new ListView<ComplexFormValue>());
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.ctrl = ctrl;
		listN = (ListView<ComplexFormValue>)getContent();
		initComponents();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		listN.setCellFactory(new Callback<ListView<ComplexFormValue>, ListCell<ComplexFormValue>>() {
			public ListCell<ComplexFormValue> call(ListView<ComplexFormValue> param) {
				ComplexFormValueListCell cell =  new ComplexFormValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getComplexFormController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		
		listN.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening complexform selection dialog");
		
		ComplexFormSelector pane = new ComplexFormSelector(ctrl.getComplexFormController());
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "selector.title"), pane, CloseType.OK);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			ComplexForm selected = pane.getSelected();
			logger.debug("Selected complexform: "+selected);
			if (selected!=null) {
				ctrl.getComplexFormController().select(selected);
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		ComplexFormValue selected = listN.getSelectionModel().getSelectedItem();
		logger.debug("ComplexForm to deselect: "+selected);
		ctrl.getComplexFormController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		for (String mess : ctrl.getComplexFormController().getToDos()) {
			todos.add(new ToDoElement(Severity.STOPPER, mess));
		}
		
		listN.getItems().clear();
		listN.getItems().addAll(ctrl.getCharacter().getComplexForms());
	}

}
