package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.fluent.SelectorWithHelp;
import org.prelle.shadowrun5.jfx.items.BigCarriedItemListCell;
import org.prelle.shadowrun5.jfx.items.CommonEquipmentSelectionPane;
import org.prelle.shadowrun5.jfx.items.ItemTemplateSelector;
import org.prelle.shadowrun5.jfx.spells.SpellSelectionPane;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class GearSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(GearSection.class.getName());

	private CharacterController ctrl;
	private ItemType[] allowedItemTypes;
	protected ListView<CarriedItem> lvSelected;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public GearSection(String title, CharacterController ctrl, ScreenManagerProvider provider, ItemType... allowedItemTypes) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;
		this.allowedItemTypes = allowedItemTypes;
		initComponents();
		initCellFactories();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lvSelected  = new ListView<>();
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		getDeleteButton().setDisable(true);

		Label phSelected = new Label(UI.getString("equipmentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	protected  void initCellFactories() {
		lvSelected.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {
			public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
				BigCarriedItemListCell cell = new BigCarriedItemListCell(ctrl.getEquipmentController(), lvSelected, getManagerProvider());
//				cell.setStyle("-fx-max-width: 28em; -fx-pref-width: 26em");
				return cell;
			}
		});
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvSelected.setStyle("-fx-min-width: 22em; -fx-pref-width: 29em");
		this.setMaxHeight(Double.MAX_VALUE);

		setContent(lvSelected);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());

//		selector.selectedItemProperty().addListener( (ov,o,n) -> templateSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			getDeleteButton().setDisable(n==null);
			if (n!=null) {
				showHelpFor.set(n.getItem());
			}
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(ctrl.getCharacter().getItems(true, allowedItemTypes));
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening gear selection dialog");
		
		ItemTemplateSelector selector = new ItemTemplateSelector(allowedItemTypes, ctrl.getEquipmentController());
		SelectorWithHelp<ItemTemplate> pane = new SelectorWithHelp<ItemTemplate>(selector);
		ManagedDialog dialog = new ManagedDialog(UI.getString("selectiondialog.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.debug("Closed with "+close);
		if (close==CloseType.OK) {
			ItemTemplate selected = selector.getSelectedItem();
			logger.debug("Selected gear: "+selected);
			if (selected!=null) {
				CarriedItem item = ctrl.getEquipmentController().select(selected);
				if (item==null) {
					getManagerProvider().getScreenManager().showAlertAndCall(AlertType.ERROR, "header", "Cannot select "+selected);
				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		CarriedItem selected = lvSelected.getSelectionModel().getSelectedItem();
		logger.debug("CarriedItem to deselect: "+selected);
		boolean removed = ctrl.getEquipmentController().deselect(selected);
		logger.info("Item removed = "+removed);
		refresh();
	}

}
