package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.dialogs.EditLifestyleValueDialog;
import org.prelle.shadowrun5.chargen.jfx.listcells.LifestyleValueListCell;

import javafx.scene.control.Label;

/**
 * @author Stefan Prelle
 *
 */
public class LifestyleSection extends GenericListSection<LifestyleValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(LifestyleSection.class.getName());

	//-------------------------------------------------------------------
	public LifestyleSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title.toUpperCase(), ctrl, provider);
		list.setCellFactory( lv -> new LifestyleValueListCell(ctrl, provider));
		initPlaceholder();
		refresh();
		list.setStyle("-fx-min-width: 22em; -fx-pref-width: 36em");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(RES.getString("lifestylevaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getLifestyle());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.debug("opening lifestyle selection dialog");
		
		LifestyleValue toAdd = new LifestyleValue();
		toAdd.setLifestyle(ShadowrunCore.getLifestyle("low"));
		EditLifestyleValueDialog dialog = new EditLifestyleValueDialog(control, toAdd, false);
		CloseType result = (CloseType) provider.getScreenManager().showAndWait(dialog);
		logger.debug("Adding Lifestyle dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			logger.info("Adding Lifestyle: "+toAdd);
			control.getLifestyleController().addLifestyle(toAdd);
		}
		
//		SingleLifestyleController perItemCtrl = new SingleLifestyleGenerator(model);
//		NewLifestylePane pane = new NewLifestylePane(perItemCtrl, buttonControl, control.getCharacter());
//		ManagedDialog dialog = new ManagedDialog(RES.getString("selectiondialog.title"), pane, CloseType.OK, CloseType.CANCEL);
//		
//		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
//		logger.debug("Closed with "+close);
//		if (close==CloseType.OK) {
//			LifestyleValue selected = perItemCtrl.getResult();
//			logger.debug("Selected lifestyle: "+selected);
//			if (selected!=null) {
//				control.getLifestyleController().addLifestyle(selected);
////				CarriedItem item = ctrl.getEquipmentController().select(selected);
//				refresh();
//			}
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		LifestyleValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("CarriedItem to deselect: "+selected);
		control.getLifestyleController().removeLifestyle(selected);
		refresh();
	}

}
