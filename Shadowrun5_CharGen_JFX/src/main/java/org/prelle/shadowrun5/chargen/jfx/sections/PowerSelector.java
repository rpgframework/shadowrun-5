package org.prelle.shadowrun5.chargen.jfx.sections;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;

import javafx.scene.Node;
import javafx.scene.control.ListView;

/**
 * @author Stefan Prelle
 *
 */
public class PowerSelector extends ListView<AdeptPower> implements IListSelector<AdeptPower> {
	
	//-------------------------------------------------------------------
	public PowerSelector(AdeptPowerController ctrl) {
		getItems().addAll(ctrl.getAvailablePowers());
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

}
