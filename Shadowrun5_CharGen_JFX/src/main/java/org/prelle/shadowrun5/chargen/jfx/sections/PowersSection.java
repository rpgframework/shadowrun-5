package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ShadowrunJFXUtils;
import org.prelle.shadowrun5.jfx.fluent.SelectorWithHelp;
import org.prelle.shadowrun5.jfx.powers.PowerSelectionPane;
import org.prelle.shadowrun5.jfx.powers.PowerValueListCell;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class PowersSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = SR5Constants.RES;

	private CharacterController ctrl;
	
	private Label ptsToSpend;
	private ListView<AdeptPowerValue> list;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public PowersSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  null);
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.ctrl = ctrl;
		list = new ListView<AdeptPowerValue>();
		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		ptsToSpend = new Label();
		ptsToSpend.getStyleClass().add("base");
		ptsToSpend.setStyle("-fx-font-size: 133%");
		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		list.setCellFactory(new Callback<ListView<AdeptPowerValue>, ListCell<AdeptPowerValue>>() {
			public ListCell<AdeptPowerValue> call(ListView<AdeptPowerValue> param) {
				PowerValueListCell cell =  new PowerValueListCell(ctrl.getPowerController(), list, getManagerProvider());
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getPowerController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbToSpend = new Label(RES.getString("adeptpowerssection.tospend"));
		HBox line = new HBox(20, lbToSpend, ptsToSpend);
		line.setAlignment(Pos.CENTER_LEFT);
		
		VBox layout = new VBox(20, line, list);
		layout.setStyle("-fx-padding: 0.4em");
		setContent(layout);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening power selection dialog");
		
		PowerSelector selector = new PowerSelector(ctrl.getPowerController());
		selector.setPlaceholder(new Label(RES.getString("adeptpowerssection.selector.placeholder")));
		SelectorWithHelp<AdeptPower> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(RES.getString("adeptpowerssection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			AdeptPower data = selector.getSelectionModel().selectedItemProperty().get();
			logger.debug("Selected power: "+data);
			if (data!=null) {
				if (data.needsChoice()) {
					Platform.runLater(new Runnable() {
						public void run() {
							logger.debug("To select "+data+", a further choice is necessary");
							Object selection = ShadowrunJFXUtils.choose(getManagerProvider(), ctrl.getCharacter(), data.getSelectFrom());
							ctrl.getPowerController().select(data, selection);
						}});
				} else {
					ctrl.getPowerController().select(data);
				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		AdeptPowerValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Spell to deselect: "+selected);
		ctrl.getPowerController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		for (String mess : ctrl.getPowerController().getToDos()) {
			todos.add(new ToDoElement(Severity.STOPPER, mess));
		}
		
		ptsToSpend.setText( String.valueOf( ctrl.getPowerController().getPowerPointsLeft()));
		list.getItems().clear();
		list.getItems().addAll(ctrl.getCharacter().getAdeptPowers());
	}

}
