/**
 *
 */
package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.Quality.QualityType;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.qualities.QualityValueListCell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class QualitySection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterController control;
	private ShadowrunCharacter model;

	private ListView<QualityValue> list;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public QualitySection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		model = ctrl.getCharacter();

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<QualityValue>();
		list.setStyle("-fx-min-height: 8em; -fx-pref-height: 20em"); 
		list.setCellFactory(cell -> new QualityValueListCell(control.getQualityController(), list, getManagerProvider()));
		
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		initLayoutMinimal();
//		VBox col1 = new VBox(5, hdPositive, bxPositive);
//		VBox col2 = new VBox(5, hdNegative, bxNegative);
//		HBox columns = new HBox(col1, col2);
//		columns.setStyle("-fx-spacing: 1em");
//
//		getChildren().clear();
//		getChildren().add(columns);
	}

	//-------------------------------------------------------------------
	private void initLayoutMinimal() {
		setContent(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) showHelpFor.set(n.getModifyable());
			else
				showHelpFor.set(null);
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");

		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getQualities());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
