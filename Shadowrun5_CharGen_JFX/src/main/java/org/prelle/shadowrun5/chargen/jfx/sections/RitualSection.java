package org.prelle.shadowrun5.chargen.jfx.sections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.RitualValue;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.spells.RitualValueListCell;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class RitualSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);
	
	private CharacterController ctrl;

	private ListView<RitualValue> list;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public RitualSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		getDeleteButton().setDisable(true);
		
		list = new ListView<RitualValue>();
		list.setMaxWidth(Double.MAX_VALUE);
		list.setStyle("-fx-min-width: 20em; -fx-pref-width: 30em"); 
		list.setCellFactory(new Callback<ListView<RitualValue>, ListCell<RitualValue>>() {
			public ListCell<RitualValue> call(ListView<RitualValue> param) {
				RitualValueListCell cell =  new RitualValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getRitualController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> logger.warn("TODO: ADD"));
		getDeleteButton().setOnAction(ev -> logger.warn("TODO: DEL"));
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			getDeleteButton().setDisable(n==null); 
			if (n!=null) showHelpFor.set(n.getModifyable()); 
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(ctrl.getCharacter().getRituals());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
