package org.prelle.shadowrun5.chargen.jfx.sections;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ResponsiveControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.rpgframework.jfx.NumericalValueTableCell;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;

/**
 * @author Stefan Prelle
 *
 */
public class SkillSection extends SingleSection implements ResponsiveControl {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private CharacterController ctrl;

	private SkillTable table1;
	private SkillTable table2;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public SkillSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;

		table1 = new SkillTable(ctrl);
		table2 = new SkillTable(ctrl);

		setDeleteButton( new Button(null, new SymbolIcon("add")) );
		setAddButton( new Button(null, new SymbolIcon("delete")) );

		layoutSideBySide();
		initInteractivity();

		table1.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getModifyable()); });
		table2.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getModifyable()); });
	}

	//-------------------------------------------------------------------
	private void layoutSideBySide() {
		table1.getItems().clear();
		table2.getItems().clear();

		List<SkillValue> toShow = ctrl.getCharacter().getSkillValues(true);
		int half = (toShow.size()+1)/2;
		int i=0;
		for (SkillValue val : toShow) {
			if (i<half)
				table1.getItems().add(val);
			else
				table2.getItems().add(val);
			i++;
		}

		HBox layout = new HBox(table1, table2);
		layout.setStyle("-fx-spacing: 1em");
		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void layoutOneColumn() {
		table1.getItems().clear();
		table2.getItems().clear();
		List<SkillValue> toShow = ctrl.getCharacter().getSkillValues(true);
		table1.getItems().addAll(toShow);

		setContent(table1);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> logger.warn("TODO: ADD"));
		getDeleteButton().setOnAction(ev -> logger.warn("TODO: DEL"));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
//		logger.info("Mode "+value);
		switch (value) {
		case MINIMAL:
			layoutOneColumn();
			break;
		default:
			layoutSideBySide();
		}
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		table1.getItems().clear();
		table2.getItems().clear();

		List<SkillValue> toShow = ctrl.getCharacter().getSkillValues(true);
		int half = (toShow.size()+1)/2;
		int i=0;
		for (SkillValue val : toShow) {
			if (i<half)
				table1.getItems().add(val);
			else
				table2.getItems().add(val);
			i++;
		}
	}

}

class SkillTable extends TableView<SkillValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController ctrl;

	private TableColumn<SkillValue, Boolean> recCol;
	private TableColumn<SkillValue, String> nameCol;
	private TableColumn<SkillValue, String> attCol;
	private TableColumn<SkillValue, SkillValue> valCol;
	private TableColumn<SkillValue, Number> modCol;
	private TableColumn<SkillValue, Number> ratCol;

	//--------------------------------------------------------------------
	public SkillTable(CharacterController ctrl) {
		if (ctrl==null)
			throw new NullPointerException();
		this.ctrl = ctrl;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initColumns() {
		recCol = new TableColumn<>();
		nameCol = new TableColumn<>(RES.getString("column.name"));
		valCol  = new TableColumn<>(RES.getString("column.value"));
		attCol  = new TableColumn<>(RES.getString("column.attribute"));
		modCol  = new TableColumn<>(RES.getString("column.mod"));
		ratCol  = new TableColumn<>(RES.getString("column.rat"));

		recCol .setId("attrtable-rec");
		nameCol.setId("attrtable-name");
		attCol .setId("attrtable-attr");
		valCol .setId("attrtable-val");
		modCol .setId("attrtable-mod");
		ratCol .setId("attrtable-sum");

		attCol.setStyle("-fx-alignment: center");
		valCol.setStyle("-fx-alignment: center");
		modCol.setStyle("-fx-alignment: center");
		ratCol.setStyle("-fx-alignment: center");

		recCol.setPrefWidth(40);
		nameCol.setMinWidth(120);
		nameCol.setPrefWidth(140);
		attCol.setMinWidth(40);
		valCol.setPrefWidth(110);
		modCol.setMinWidth(30);
		ratCol.setMinWidth(30);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initLayout() {
		getColumns().addAll(recCol, nameCol, attCol, valCol, modCol, ratCol);
	}

	//--------------------------------------------------------------------
	private void initValueFactories() {
//		recCol.setCellValueFactory(cdf -> new SimpleBooleanProperty(ctrl.getSkillController().isConceptSkill(cdf.getValue().getModifyable())));
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		attCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getAttribute1().getShortName()));
		valCol .setCellValueFactory(cdf -> new SimpleObjectProperty<SkillValue>(cdf.getValue()));
		modCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getModifier()));
		ratCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getModifiedValue()));
	}

	//--------------------------------------------------------------------
	private void initCellFactories() {
		valCol.setCellFactory( (col) -> new NumericalValueTableCell<Skill,SkillValue,SkillValue>(ctrl.getSkillController()));
		recCol.setCellFactory(col -> new TableCell<SkillValue,Boolean>(){
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					if (item) {
						Label lb = new Label("\uE735");
						lb.setStyle("-fx-text-fill: recommendation; -fx-font-family: 'Segoe MDL2 Assets';");
						setGraphic(lb);
					} else
						setGraphic(null);
				}
			}
		});
	}

}