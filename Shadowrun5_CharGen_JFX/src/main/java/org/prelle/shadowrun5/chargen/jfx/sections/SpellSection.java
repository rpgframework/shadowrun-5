package org.prelle.shadowrun5.chargen.jfx.sections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.spells.SpellSelectionPane;
import org.prelle.shadowrun5.jfx.spells.SpellValueListCell;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class SpellSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private CharacterController ctrl;
	private boolean alchemistic;
	private ListView<SpellValue> listN;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public SpellSection(String title, CharacterController ctrl, boolean alchemistic, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  new ListView<SpellValue>());
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.alchemistic = alchemistic;
		this.ctrl = ctrl;
		listN = (ListView<SpellValue>)getContent();
		initComponents();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		listN.setCellFactory(new Callback<ListView<SpellValue>, ListCell<SpellValue>>() {
			public ListCell<SpellValue> call(ListView<SpellValue> param) {
				SpellValueListCell cell =  new SpellValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getSpellController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		
		listN.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening spell selection dialog");
		
		SpellSelectionPane pane = new SpellSelectionPane(ctrl.getSpellController(), getManagerProvider(), alchemistic);
		pane.getColumn2Node().setVisible(false);
		pane.getColumn2Node().setManaged(false);
		pane.getColumn1Node().setStyle("-fx-pref-width: 35em; -fx-min-width: 20em;");
		pane.getColumn3Node().setStyle("-fx-pref-width: 30em; -fx-min-width: 20em;");
		pane.setHeadersVisible(false);
		ManagedDialog dialog = new ManagedDialog("Titel", pane, CloseType.OK);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			@SuppressWarnings("unchecked")
			Spell selected = pane.getSelectedAvailable();
			logger.debug("Selected spell: "+selected);
			if (selected!=null) {
				ctrl.getSpellController().select(selected, alchemistic);
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		SpellValue selected = listN.getSelectionModel().getSelectedItem();
		logger.debug("Spell to deselect: "+selected);
		ctrl.getSpellController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		for (String mess : ctrl.getSpellController().getToDos()) {
			todos.add(new ToDoElement(Severity.STOPPER, mess));
		}
		
		listN.getItems().clear();
		listN.getItems().addAll(ctrl.getCharacter().getSpells(alchemistic));
	}

}
