/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.SR5LetUserChooseListener;
import org.prelle.shadowrun5.gen.WizardPageType;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharGenWizardNG extends Wizard implements GenerationEventListener, SR5LetUserChooseListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private CharacterGenerator charGen;
	private ShadowrunCharacter generated;

	//-------------------------------------------------------------------
	/**
	 * @param nodes
	 */
	public CharGenWizardNG(ShadowrunCharacter model) {
		getPages().addAll(
				new WizardPageGenerator(this, model)
//				new WizardPageCharacterConcept(this,charGen)
				);

		GenerationEventDispatcher.addListener(this);

		this.setConfirmCancelCallback( wizardParam -> {
			CloseType answer = getScreenManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					SR5Constants.RES.getString("wizard.cancelconfirm.header"),
					SR5Constants.RES.getString("wizard.cancelconfirm.content"));
			return answer==CloseType.OK || answer==CloseType.YES;
		});

	}

	//-------------------------------------------------------------------
	public WizardPage createPage(WizardPageType type, CharacterGenerator charGen) {
		if (charGen==null)
			throw new NullPointerException("CharacterGenerator not set");

		switch (type) {
		case PRIORITIES: return new WizardPagePriorities(this,charGen);
		case METATYPE  : return new WizardPageMetaType(this,charGen);
		case ATTRIBUTES: return new WizardPageAttributes(this,charGen);
		case SKILLS    : return new WizardPageSkills(this,charGen);
		case ALCHEMY   : return new WizardPageAlchemy(this,charGen);
		case MAGIC_OR_RESONANCE: return new WizardPageMagicOrResonance(this,charGen);
		case TRADITION : return new WizardPageTradition(this,charGen);
		case SPELLS    : return new WizardPageSpells(this,charGen);
		case RITUALS   : return new WizardPageRituals(this,charGen);
		case QUALITIES : return new WizardPageQualities(this,charGen);
		case POWERS    : return new WizardPagePowers(this,charGen);
		case COMPLEX_FORMS: return new WizardPageComplexForms(this,charGen);
		case BODYTECH  : return new WizardPageBodytech(this,charGen);
		case GEAR      : return new WizardPageGear(this,charGen);
		case NAME      : return new WizardPageName(this,charGen);
		default:
			logger.error("Don't know how to create page for "+type);
			throw new IllegalArgumentException("Don't know how to create page for "+type);
		}
//		return null;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.splimo.chargen.gen.jfx.Wizard#finish()
//	 */
//	@Override
//	public void finish() {
//		logger.debug("finish clicked");
//		// Call nextPage of last page to apply data
//		pages.get(pages.size()-1).nextPage();
//
//		logger.info("---------------Finishing with CharGenWizard");
////		generated = charGen.generate();
//		logger.debug("Generated = "+generated);
//		close();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#close(org.prelle.javafx.CloseType)
	 */
//	@Override
//	public boolean close(CloseType closeType) {
//		logger.info("wizard closed with "+closeType);
//		while (getPages().size()>1) {
//			WizardPage removed = getPages().remove(1);
//			if (removed instanceof GenerationEventListener)
//				GenerationEventDispatcher.removeListener((GenerationEventListener) removed);
//		}
//		pages.clear();
//		charGen.stop();
//		return true;
//	}

	//-------------------------------------------------------------------
	public ShadowrunCharacter getGenerated() {
		return generated;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.CONSTRUCTIONKIT_CHANGED) {
			logger.debug("RCV "+event.getType());
			charGen = (CharacterGenerator) event.getKey();
			logger.info("New construction kit = "+charGen);
			if (charGen!=null)
				charGen.setCallback(this);
			// Clear all pages above 1 (2 if character concepts are used)
			logger.debug("  remove all pages from listener");
			while (getPages().size()>1) {
				WizardPage removed = getPages().remove(1);
				if (removed instanceof GenerationEventListener)
					GenerationEventDispatcher.removeListener((GenerationEventListener) removed);
			}
			// Add new pages
			logger.debug("  create pages and add to listener");
			for (WizardPageType type : charGen.getWizardPages()) {
				WizardPage page = createPage(type, charGen);
				if (page!=null) {
					logger.info("  Add page for "+type);
					getPages().add(page);
					if (page instanceof GenerationEventListener)
						GenerationEventDispatcher.addListener((GenerationEventListener) page);
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Wizard#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		if (charGen!=null)
			return charGen.hasEnoughData();
		logger.warn("Cannot finish - no chargen set");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.SR5LetUserChooseListener#letUserChoose(java.lang.String, org.prelle.shadowrun.modifications.ModificationChoice)
	 */
	@Override
	public Collection<Modification> letUserChoose(String choiceReason, ModificationChoice choice) {
		logger.info("Let user choose "+choice);

		if (choice.getNumberOfChoices()>0)
			return showUserChoiceDialog(choiceReason, choice);
		else
			logger.error("Don't know how to deal with choices: "+choice);

		return new ArrayList<Modification>();
	}

	//-------------------------------------------------------------------
	private Collection<Modification> showUserChoiceDialog(String choiceReason, ModificationChoice choice) {
		List<Modification> ret = new ArrayList<Modification>();

			UserChoiceDialog dialog = new UserChoiceDialog(choiceReason, choice);
			dialog.setOnAction(CloseType.OK, event -> {
				getScreenManager().close(dialog, CloseType.OK);
			});
			getScreenManager().showAndWait(dialog);

			logger.warn("CHECK THIS");
//			if (dialog.getCloseType()==CloseType.OK)
//				return Arrays.asList(dialog.getChoice());
		return ret;
	}

}
