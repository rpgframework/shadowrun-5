/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.ConnectionsController;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.charctrl.SpellController;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class PointsPane extends VBox {

	protected final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	protected static PropertyResourceBundle UI = SR5Constants.RES;

	private Label heaGPLeft;
	private Label lblGPLeft;
	private Label heaExpLeft;
	private Label lblExpLeft;
	private Label lblNuyen;
	private Class<?> typeOfController;
	private Node  extra;

	protected ShadowrunCharacter model;
	protected CharacterController ctrl;

	//-------------------------------------------------------------------
	public PointsPane(CharacterController ctrl, Class<?> type) {
		this.ctrl = ctrl;
		this.typeOfController = type;
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lblGPLeft      = new Label("?");
		lblExpLeft     = new Label("?");
		lblNuyen       = new Label("?");

		lblGPLeft     .getStyleClass().add("text-header");
		lblExpLeft    .getStyleClass().add("text-header");
		lblNuyen      .getStyleClass().add("text-header");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		setStyle("-fx-vgap: 2em; -fx-max-width: 15em");
		setAlignment(Pos.TOP_CENTER);
		getStyleClass().add("section-bar");
//		setPrefWidth(300);
		setMinWidth(200);
		setMinHeight(300);

		heaGPLeft     = new Label(UI.getString("label.gp.free"));
		heaExpLeft    = new Label(UI.getString("label.ep.free"));
		Label heaNuyen      = new Label(UI.getString("label.nuyen"));

		GridPane grid = new GridPane();
		grid.setMaxWidth(Double.MAX_VALUE);
		grid.setHgap(10);
		grid.setVgap(10);
		if (ctrl.getMode()==CharGenMode.CREATING) {
			grid.add(lblGPLeft     , 0, 0);
			grid.add(heaGPLeft     , 0, 1);
		}
		grid.add(lblExpLeft    , 0, 2);
		grid.add(heaExpLeft    , 0, 3);
		grid.add(lblNuyen      , 0, 4);
		grid.add(heaNuyen      , 0, 5);

		GridPane.setFillWidth(heaGPLeft, true);
		GridPane.setFillWidth(lblGPLeft, true);
		GridPane.setFillWidth(heaExpLeft, true);
		GridPane.setFillWidth(lblExpLeft, true);
		GridPane.setFillWidth(heaNuyen, true);
		GridPane.setFillWidth(lblNuyen, true);
		GridPane.setHalignment(lblExpLeft, HPos.CENTER);
		GridPane.setHalignment(heaExpLeft, HPos.CENTER);
		GridPane.setHalignment(lblGPLeft, HPos.CENTER);
		GridPane.setHalignment(heaGPLeft, HPos.CENTER);
		GridPane.setHalignment(lblNuyen, HPos.CENTER);
		GridPane.setHalignment(heaNuyen, HPos.CENTER);
		GridPane.setMargin(heaExpLeft, new Insets(-10,0,0,0));
		GridPane.setMargin(heaNuyen, new Insets(-10,0,0,0));

		getChildren().add(grid);


//		if (ctrl.getMode()!=CharGenMode.CREATING) {
//			lblGPLeft.setVisible(false);
//			heaGPLeft.setVisible(false);
//			grid.getChildren().remove(lblGPLeft);
//			grid.getChildren().remove(heaGPLeft);
//		}

	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		lblExpLeft.setText(String.valueOf(model.getKarmaFree()));
		lblNuyen.setText(String.valueOf(model.getNuyen()));

		if (typeOfController==AttributeController.class) {
			lblGPLeft.setText(String.valueOf(ctrl.getAttributeController().getPointsLeft()));
		} else if (typeOfController==SkillController.class) {
			lblGPLeft.setText(ctrl.getSkillController().getPointsLeftSkills()+"/"+ctrl.getSkillController().getPointsLeftSkillGroups());
			heaGPLeft.setText(UI.getString("label.skillandgroups.free"));
		} else if (typeOfController==SpellController.class) {
			lblGPLeft.setText(String.valueOf(ctrl.getSpellController().getSpellsLeft()));
			heaGPLeft.setText(UI.getString("label.spells.free"));
		} else if (typeOfController==AdeptPowerController.class) {
			lblExpLeft.setText(String.valueOf(ctrl.getPowerController().getPowerPointsLeft()));
			heaExpLeft.setText(UI.getString("label.powerpoints.free"));
		} else if (typeOfController==QualityController.class) {
			lblExpLeft.setText(String.valueOf(model.getKarmaFree()));
			heaExpLeft.setText(UI.getString("label.ep.free"));
			heaGPLeft.setText("");
			lblGPLeft.setVisible(false);
			heaGPLeft.setVisible(false);
		} else if (typeOfController==EquipmentController.class) {
			lblGPLeft.setVisible(false);
			heaGPLeft.setVisible(false);
		} else if (typeOfController==ConnectionsController.class) {
			lblGPLeft.setText(String.valueOf( ctrl.getConnectionController().getPointsLeft() ));
		} 
		else
			logger.error("Don't know how to set points for "+typeOfController);
	}

	//-------------------------------------------------------------------
	public void setExtra(Node extra) {
		if (this.extra!=null)
			getChildren().remove(this.extra);
		this.extra = extra;
		logger.debug("Add "+extra);
		getChildren().add(extra);
	}

}
