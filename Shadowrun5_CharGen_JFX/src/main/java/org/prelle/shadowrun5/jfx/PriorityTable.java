package org.prelle.shadowrun5.jfx;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityOption;
import org.prelle.shadowrun5.PriorityTableEntry;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.Resource;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class PriorityTable extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName());
	
	private ToggleButton[] btnMeta;
	private ToggleButton[] btnAttrib;
	private ToggleButton[] btnMagic;
	private ToggleButton[] btnSkill;
	private ToggleButton[] btnResrc;
	
	private ToggleGroup tgMeta;
	private ToggleGroup tgAttrib;
	private ToggleGroup tgMagic;
	private ToggleGroup tgSkill;
	private ToggleGroup tgResrc;
	
	private NewPriorityCharacterGenerator charGen;
	
	//-------------------------------------------------------------------
	public PriorityTable(NewPriorityCharacterGenerator charGen) {
		this.charGen = charGen;
		
		initCompontents();
		initLayout();
		
		update();
		initInteractivity();
		
		GenerationEventDispatcher.addListener(this);
	}
	
	//-------------------------------------------------------------------
	private void initCompontents() {
		getStyleClass().add("priority-table");
		
		btnMeta = new ToggleButton[5];
		for (org.prelle.shadowrun5.Priority prio : org.prelle.shadowrun5.Priority.values()) {
			btnMeta[prio.ordinal()] = new ToggleButton(RES.getString("priotable.btn.meta"+prio.ordinal()));
			btnMeta[prio.ordinal()].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMeta[prio.ordinal()].setUserData(prio);
			StringBuffer buf = new StringBuffer();
			Iterator<PriorityOption> it = ShadowrunCore.getPriorityTableEntry(PriorityType.METATYPE, prio).iterator();
			while (it.hasNext()) {
				buf.append(String.valueOf(it.next()));
				if (it.hasNext())
					buf.append("\n");
			}
			btnMeta[prio.ordinal()].setTooltip(new Tooltip(buf.toString()));
		}
		
		tgMeta = new ToggleGroup();
		tgMeta.getToggles().addAll(btnMeta);
		tgMeta.setUserData(PriorityType.METATYPE);

		btnAttrib = new ToggleButton[5];
		for (org.prelle.shadowrun5.Priority prio : org.prelle.shadowrun5.Priority.values()) {
			int i = prio.ordinal();
			btnAttrib[i] = new ToggleButton(RES.getString("priotable.btn.attrib"+i));
			btnAttrib[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnAttrib[i].setUserData(prio);
		}
		tgAttrib = new ToggleGroup();
		tgAttrib.getToggles().addAll(btnAttrib);
		tgAttrib.setUserData(PriorityType.ATTRIBUTE);

		btnMagic = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnMagic[i] = new ToggleButton(null, getMagicString(prio));
			btnMagic[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMagic[i].setUserData(prio);
			btnMagic[i].getStyleClass().addAll("priobutton","magic");
			switch (i) {
			case 0: btnMagic[i].setPrefHeight(100); break;
			case 1: btnMagic[i].setPrefHeight(100); break;
			case 2: btnMagic[i].setPrefHeight(100); break;
			case 3: btnMagic[i].setPrefHeight(80); break;
			case 4: btnMagic[i].setPrefHeight(40); break;
			}
		}
		tgMagic = new ToggleGroup();
		tgMagic.getToggles().addAll(btnMagic);
		tgMagic.setUserData(PriorityType.MAGIC);

		btnSkill = new ToggleButton[5];
		for (org.prelle.shadowrun5.Priority prio : org.prelle.shadowrun5.Priority.values()) {
			int i = prio.ordinal();
			btnSkill[i] = new ToggleButton(RES.getString("priotable.btn.skill"+i));
			btnSkill[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnSkill[i].setUserData(prio);
		}
		tgSkill = new ToggleGroup();
		tgSkill.getToggles().addAll(btnSkill);
		tgSkill.setUserData(PriorityType.SKILLS);

		btnResrc = new ToggleButton[5];
		for (org.prelle.shadowrun5.Priority prio : org.prelle.shadowrun5.Priority.values()) {
			int i = prio.ordinal();
			btnResrc[i] = new ToggleButton(RES.getString("priotable.btn.resrc"+i));
			btnResrc[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnResrc[i].setUserData(prio);
		}
		tgResrc = new ToggleGroup();
		tgResrc.getToggles().addAll(btnResrc);
		tgResrc.setUserData(PriorityType.RESOURCES);
	}
	
	//-------------------------------------------------------------------
	private VBox getMagicString(Priority prio) {
		logger.info("------------"+prio+"--------------");
		PriorityTableEntry entry = ShadowrunCore.getPriorityTableEntry(PriorityType.MAGIC, prio);
		VBox ret = new VBox();
		
		int oldPoints = -1;
		StringBuffer buf = null;
		MagicOrResonanceType lastType = null;
		List<PriorityOption> entries = new ArrayList<PriorityOption>(entry);
		Collections.sort(entries, new Comparator<PriorityOption>() {

			@Override
			public int compare(PriorityOption o1, PriorityOption o2) {
				MagicOrResonanceOption t1 = ((MagicOrResonanceOption)o1);
				MagicOrResonanceOption t2 = ((MagicOrResonanceOption)o2);
				return ((Integer)t1.getValue()).compareTo(t2.getValue());
			}});
		for ( PriorityOption opt : entries ) {
			logger.info("*******"+opt);
			MagicOrResonanceType type = ((MagicOrResonanceOption)opt).getType();
			int points = ((MagicOrResonanceOption)opt).getValue();
			logger.info("  type="+type+"  points="+points);
			if (points!=oldPoints) {
				logger.info(" change from "+oldPoints+" to "+points+"   Buffer is "+buf);
				// Print old
				if (buf!=null) {
					Label part1 = new Label(buf.toString()+":");
					part1.setStyle("-fx-font-size: 70%; -fx-font-weight: bold");
					Label part2 = new Label(Resource.get(RES, "priotable.btn.magic."+prio.name().toLowerCase()+"."+oldPoints));
					part2.setWrapText(true);
					part2.setStyle("-fx-font-size: 70%; -fx-pref-width: 40em"); 
					HBox line = new HBox(5, part1, part2);
					ret.getChildren().add(line);
					buf = new StringBuffer(type.getName());
				} else {
					buf = new StringBuffer(type.getName());
				}
			} else {
				// Same points
				if (buf!=null) {
					
				}
				buf.append(", "+type.getName());
			}
			oldPoints = points;
			lastType = type;
		};
		logger.info(" after loop buffer is "+buf+" and points were "+oldPoints+"   and lastType="+lastType);
		Label part1 = new Label(buf.toString()+":");
		part1.setStyle("-fx-font-size: 70%; -fx-font-weight: bold");
		Label part2 = new Label(Resource.get(RES,"priotable.btn.magic."+prio.name().toLowerCase()+"."+oldPoints));
		part2.setWrapText(true);
		part2.setStyle("-fx-font-size: 70%; -fx-pref-width: 40em"); 
		HBox line = new HBox(5, part1, part2);
		if (lastType.getId().equals("mundane"))
			line = new HBox(5, new Label(lastType.getName()));
		ret.getChildren().add(line);
		
		return ret;
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setGridLinesVisible(true);
		
		/*
		 * Headings
		 */
		// Heading row
		Label lblPrio  = new Label(RES.getString("priotable.label.prio"));
		Label lblMeta  = new Label(RES.getString("priotable.label.meta"));
		Label lblAttrib= new Label(RES.getString("priotable.label.attrib"));
		Label lblMagic = new Label(RES.getString("priotable.label.magic"));
		Label lblSkill = new Label(RES.getString("priotable.label.skill"));
		Label lblResrc = new Label(RES.getString("priotable.label.resrc"));
		lblPrio.getStyleClass().add("table-heading");
		lblMeta.getStyleClass().add("table-heading");
		lblAttrib.getStyleClass().add("table-heading");
		lblMagic.getStyleClass().add("table-heading");
		lblSkill.getStyleClass().add("table-heading");
		lblResrc.getStyleClass().add("table-heading");
		lblPrio.setMaxWidth(Double.MAX_VALUE);
		lblMeta.setMaxWidth(Double.MAX_VALUE);
		lblAttrib.setMaxWidth(Double.MAX_VALUE);
		lblMagic.setMaxWidth(Double.MAX_VALUE);
		lblSkill.setMaxWidth(Double.MAX_VALUE);
		lblResrc.setMaxWidth(Double.MAX_VALUE);
		GridPane.setHgrow(lblPrio, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMeta, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblAttrib, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMagic, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblSkill, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblResrc, javafx.scene.layout.Priority.ALWAYS);
		add(lblPrio  , 0,0);
		add(lblMeta  , 1,0);
		add(lblAttrib, 2,0);
		add(lblMagic , 3,0);
		add(lblSkill , 4,0);
		add(lblResrc , 5,0);
		
		// Heading column
		for (org.prelle.shadowrun5.Priority prio : org.prelle.shadowrun5.Priority.values()) {
			Label label = new Label(prio.name());
			label.getStyleClass().addAll("text-subheader","row-heading");
			add(label, 0, prio.ordinal()+1);
		}
//		for (int i=0; i<5; i++) {
//			Label label = new Label(String.valueOf((char)('A'+i)));
//			label.getStyleClass().addAll("text-subheader","row-heading");
//			add(label, 0,i+1);
//		}
		
		/*
		 * Content
		 */
		for (int i=0; i<5; i++) add(btnMeta  [i], 1,i+1);
		for (int i=0; i<5; i++) add(btnAttrib[i], 2,i+1);
		for (int i=0; i<5; i++) add(btnMagic [i], 3,i+1);
		for (int i=0; i<5; i++) add(btnSkill [i], 4,i+1);
		for (int i=0; i<5; i++) add(btnResrc [i], 5,i+1);
		
		// Column layout
		ColumnConstraints center = new ColumnConstraints();
		center.setHalignment(HPos.CENTER);
		ColumnConstraints magicOr = new ColumnConstraints();
		magicOr.setHgrow(javafx.scene.layout.Priority.ALWAYS);
		getColumnConstraints().add(center);
		getColumnConstraints().add(new ColumnConstraints());
		getColumnConstraints().add(new ColumnConstraints());
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tgMeta.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.METATYPE, (org.prelle.shadowrun5.Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgAttrib.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.ATTRIBUTE, (org.prelle.shadowrun5.Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgMagic.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.MAGIC, (org.prelle.shadowrun5.Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgSkill.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.SKILLS, (org.prelle.shadowrun5.Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgResrc.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.RESOURCES, (org.prelle.shadowrun5.Priority) ((ToggleButton)n).getUserData());
			update();
			});
	}
	
	//-------------------------------------------------------------------
	private void update() {
		if (charGen.getPriority(PriorityType.METATYPE)!=null)
			tgMeta.selectToggle(btnMeta[charGen.getPriority(PriorityType.METATYPE).ordinal()]);
		if (charGen.getPriority(PriorityType.ATTRIBUTE)!=null)
			tgAttrib.selectToggle(btnAttrib[charGen.getPriority(PriorityType.ATTRIBUTE).ordinal()]);
		if (charGen.getPriority(PriorityType.MAGIC)!=null)
			tgMagic.selectToggle(btnMagic[charGen.getPriority(PriorityType.MAGIC).ordinal()]);
		if (charGen.getPriority(PriorityType.SKILLS)!=null)
			tgSkill.selectToggle(btnSkill[charGen.getPriority(PriorityType.SKILLS).ordinal()]);
		if (charGen.getPriority(PriorityType.RESOURCES)!=null)
			tgResrc.selectToggle(btnResrc[charGen.getPriority(PriorityType.RESOURCES).ordinal()]);
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			logger.debug("RCV "+event.getType());
			PriorityType option = (PriorityType)event.getKey();
			int priority = ((org.prelle.shadowrun5.Priority)event.getValue()).ordinal();
			switch (option) {
			case METATYPE : tgMeta  .selectToggle(btnMeta[priority]); break;
			case ATTRIBUTE: tgAttrib.selectToggle(btnAttrib[priority]); break;
			case MAGIC    : tgMagic .selectToggle(btnMagic[priority]); break;
			case SKILLS   : tgSkill .selectToggle(btnSkill[priority]); break;
			case RESOURCES: tgResrc .selectToggle(btnResrc[priority]); break;
			}
		}
	}
	
}
