/**
 * 
 */
package org.prelle.shadowrun5.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface SR5Constants {
	
	public final static String PREFIX = "org/prelle/shadowrun/jfx";
	public final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(PREFIX+"/i18n/shadowrun/chargenui");
	public final static PropertyResourceBundle HELP = (PropertyResourceBundle) ResourceBundle.getBundle(PREFIX+"/i18n/shadowrun/chargenui-help");
	
	public final static String BASE_LOGGER_NAME = "shadowrun.jfx";

}
