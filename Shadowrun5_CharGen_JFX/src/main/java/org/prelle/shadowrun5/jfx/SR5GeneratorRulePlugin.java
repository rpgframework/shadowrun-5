/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun5.jfx.fluent.CharacterViewScreenSR5Fluent;
import org.prelle.shadowrun5.jfx.items.input.ShadowrunDataInputScreen;
import org.prelle.shadowrun5.levelling.CharacterLeveller;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class SR5GeneratorRulePlugin implements RulePlugin<ShadowrunCharacter>, CommandBusListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	private static PropertyResourceBundle HELP =  SR5Constants.HELP;

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.CHARACTER_CREATION);
		FEATURES.add(RulePluginFeatures.DATA_INPUT);
	}

	private ConfigContainer cfgShadowrun;

	//-------------------------------------------------------------------
	/**
	 */
	public SR5GeneratorRulePlugin() {
//		DirectInputCharacterGenerator input = new DirectInputCharacterGenerator();
//		input.setResourceBundle(UI);
//		input.setHelpResourceBundle(HELP);
//		input.setPlugin(this);
//		CharacterGeneratorRegistry.register(input);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CHARGEN";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 5 Character Generator";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		List<RulePluginFeatures> ret = new ArrayList<>(FEATURES);
//		if (developerMode!=null && !(Boolean)developerMode.getValue()) {
//			ret.remove(RulePluginFeatures.DATA_INPUT);
//			ret.clear();
//		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		cfgShadowrun = (ConfigContainer) addBelow.getChild("shadowrun");
		CharacterGeneratorRegistry.attachConfigurationTree(cfgShadowrun);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			logger.debug("Check "+Arrays.toString(values));
			if (values[0]!=RoleplayingSystem.SHADOWRUN) return false;
			if (!(values[1] instanceof ShadowrunCharacter)) return false;
			if (!(values[2] instanceof CharacterHandle)) return false;
			if (!(values[4] instanceof ScreenManager)) return false;
			return true;
		case SHOW_CHARACTER_CREATION_GUI:
			if (values[0]!=RoleplayingSystem.SHADOWRUN) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
		case SHOW_DATA_INPUT_GUI:
			if (values[0]!=RoleplayingSystem.SHADOWRUN) return false;
			if (!(values[2] instanceof ScreenManager)) return false;
			return true;
		default:
			return false;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		logger.debug("Handle "+type);
		if (!willProcessCommand(src, type, values))
			return new CommandResult(type, false, null, false);

		logger.debug("Handle "+type);
		ScreenManager manager;
		CharacterController control;
		ShadowrunCharacter model;
		switch (type) {
		case SHOW_CHARACTER_MODIFICATION_GUI:
			logger.debug("start character modification");
			model = (ShadowrunCharacter)values[1];
			control = new CharacterLeveller(model);
			CharacterHandle handle = (CharacterHandle)values[2];
			manager = (ScreenManager)values[4];

			CharacterViewScreenSR5Fluent screen;
			try {
				screen = new CharacterViewScreenSR5Fluent(control, ViewMode.MODIFICATION, handle);
//				screen.setData(model, handle);
				manager.navigateTo(screen);
			} catch (Exception e) {
				logger.error("Failed opening character screen",e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2, e.toString());
			}

			return new CommandResult(type, true);
		case SHOW_CHARACTER_CREATION_GUI:
			logger.debug("start character creation");
			model = new ShadowrunCharacter();
			manager = (ScreenManager)values[2];
			control = new NewPriorityCharacterGenerator();
			((CharacterGenerator)control).start(new ShadowrunCharacter());

			screen = new CharacterViewScreenSR5Fluent(control, ViewMode.GENERATION, null);
			manager.navigateTo(screen);

			logger.debug("You could give a warning here");
			manager.showAlertAndCall(AlertType.NOTIFICATION, "Achtung!", "Zwar werden 'Schattenläufer' und 'Straßengrimoire' unterstützt, aber Metagenetik, Initiation sowie Erschaffung außer Prioriäten- und Karmasystem noch nicht - das kommt erst in späteren Versionen.\n\n"+
			"Wer Fehler in bestehenden Funktionen findet, kann uns dies melden. Siehe hierzu: http://www.rpgframework.de/index.php/de/support/fehler-melden/\n"+
			"Wer Java-Entwickler ist und mitmachen möchte, kann uns an  genesis@rpgframework.de  eine Mail schreiben.\n"
			);

			screen.startGeneration(null);

			CommandResult result = new CommandResult(type, true);
			result.setReturnValue(model);
			return result;
		case SHOW_DATA_INPUT_GUI:
			logger.debug("start data input");
			manager = (ScreenManager)values[2];

			ShadowrunDataInputScreen screen2 = new ShadowrunDataInputScreen();
//			manager.navigateTo(screen2);
//			manager.show(screen2);
			result = new CommandResult(type, true);
//			result.setReturnValue(model);
			return result;
		default:
			return new CommandResult(type, false);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		NewPriorityCharacterGenerator charGen = new NewPriorityCharacterGenerator();
//		charGen.setResourceBundle(UI);
//		charGen.setHelpResourceBundle(HELP);
		charGen.setPlugin(this);
		CharacterGeneratorRegistry.register(charGen);

//		NewKarmaGenerator charGen2 = new NewKarmaGenerator();
////		charGen2.setResourceBundle(UI);
////		charGen2.setHelpResourceBundle(HELP);
//		charGen2.setPlugin(this);
//		CharacterGeneratorRegistry.register(charGen2);

		CommandBus.registerBusCommandListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/i18n/shadowrun/chargenui.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
