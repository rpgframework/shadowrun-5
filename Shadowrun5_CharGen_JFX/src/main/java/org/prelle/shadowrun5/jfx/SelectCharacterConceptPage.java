/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.ShadowrunCore;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SelectCharacterConceptPage extends HBox {

	private static PropertyResourceBundle GUI = SR5Constants.RES;

	private ImageView iViewLogo;
	private ImageView iViewRecom;
	private Map<String, CharacterConceptBox> concepts;
	private Button btnNext, btnCancel;

	//-------------------------------------------------------------------
	/**
	 */
	public SelectCharacterConceptPage() {
		super(10);
		initCompontents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		Image image = new Image(getClass().getResourceAsStream("images/Shadowrun_hochkant.png"));

		iViewLogo = new ImageView(image);

		concepts = new HashMap<String, CharacterConceptBox>();
		concepts.put("face"         , new CharacterConceptBox(HPos.LEFT , ShadowrunCore.getCharacterConcept("face")));
		concepts.put("spellcaster"  , new CharacterConceptBox(HPos.RIGHT, ShadowrunCore.getCharacterConcept("spellcaster")));
		concepts.put("decker"       , new CharacterConceptBox(HPos.LEFT , ShadowrunCore.getCharacterConcept("decker")));
		concepts.put("technomancer" , new CharacterConceptBox(HPos.RIGHT, ShadowrunCore.getCharacterConcept("technomancer")));
		concepts.put("rigger"       , new CharacterConceptBox(HPos.LEFT , ShadowrunCore.getCharacterConcept("rigger")));
		concepts.put("streetsamurai", new CharacterConceptBox(HPos.RIGHT, ShadowrunCore.getCharacterConcept("streetsamurai")));

		btnNext  = new Button(GUI.getString("button.next"));
		btnNext.setMaxWidth(Double.MAX_VALUE);
		btnCancel= new Button(GUI.getString("button.cancel"));
		btnCancel.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Image recom = new Image(getClass().getResourceAsStream("images/recommendation.png"));
		iViewRecom= new ImageView(recom);

		TilePane btnBox = new TilePane();
		btnBox.setAlignment(Pos.BOTTOM_RIGHT);
		btnBox.setHgap(10);
		btnBox.getChildren().addAll(btnNext, btnCancel);

		Label instruct = new Label(GUI.getString("wizard.concept.instruct"), iViewRecom);
		instruct.setWrapText(true);


		TilePane content = new TilePane();
		content.setPrefColumns(2);
		content.getChildren().add(concepts.get("face"));
		content.getChildren().add(concepts.get("spellcaster"));
		content.getChildren().add(concepts.get("decker"));
		content.getChildren().add(concepts.get("technomancer"));
		content.getChildren().add(concepts.get("rigger"));
		content.getChildren().add(concepts.get("streetsamurai"));
		content.setMaxWidth(Double.MAX_VALUE);

		ScrollPane scroll = new ScrollPane();
		scroll.setContent(content);
		scroll.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		scroll.setMaxWidth(USE_COMPUTED_SIZE);
		scroll.setFitToWidth(true);

		VBox content2 = new VBox(10);
		content2.getChildren().addAll(instruct, scroll,  btnBox);
		VBox.setVgrow(scroll, Priority.ALWAYS);
		VBox.setVgrow(content, Priority.SOMETIMES);
		VBox.setMargin(btnBox, new Insets(0,15,15,0));
		VBox.setMargin(instruct, new Insets(15,0,0,0));


		getChildren().addAll(iViewLogo,content2);
		HBox.setHgrow(content2, Priority.SOMETIMES);

	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

}

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class CharacterConceptBox extends HBox {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static ResourceBundle CORE = ShadowrunCore.getI18nResources();

	private CharacterConcept concept;
	private ImageView image;
	private Label     title;
	private TextArea  text;
	private CheckBox  check;

	//-------------------------------------------------------------------
	public CharacterConceptBox(HPos alignment, CharacterConcept concept) {
		this.concept = concept;

		if (alignment==HPos.CENTER)
			throw new IllegalArgumentException("Alignment may either be LEFT or RIGHT");

		initCompontents();
		initLayout(alignment);
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		/*
		 * Image
		 */
		image = new ImageView();

		InputStream ins = getClass().getResourceAsStream("images/concept_"+concept.getId()+".png");
		if (ins!=null) {
			image.setImage(new Image(ins));
		} else {
			logger.error("No image found for character concept "+concept);
		}

		/*
		 * Rest
		 */
		title = new Label(concept.getName());
		text  = new TextArea(CORE.getString("concept."+concept.getId()+".text"));
		check = new CheckBox();

		text.setWrapText(true);
		text.setEditable(false);
	}

	//-------------------------------------------------------------------
	private void initLayout(HPos alignment) {
		VBox content = new VBox();
		content.getChildren().addAll(title, text);

		title.getStyleClass().add("title3");

		// Let checkbox appear at bottom end
		check.setMaxHeight(Double.MAX_VALUE);
		title.setPrefHeight(53);
		title.setAlignment(Pos.BOTTOM_CENTER);
//		text.setPrefWidth(500);
		text.setMaxWidth(Double.MAX_VALUE);
		text.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(text, Priority.ALWAYS);

		switch (alignment) {
		case LEFT:
			getChildren().addAll(image, content, check);

			title.getStyleClass().add("choice-left-title");
			text.getStyleClass().add("choice-left-text-area");

			check.setAlignment(Pos.BOTTOM_LEFT);

			break;
		case RIGHT:
			getChildren().addAll(check, content, image);

			title.getStyleClass().add("choice-right-title");
			text.getStyleClass().add("choice-right-text-area");

			check.setAlignment(Pos.BOTTOM_RIGHT);
			break;
		default:
		}

		HBox.setHgrow(content, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

}