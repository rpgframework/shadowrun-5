/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.util.ResourceBundle;

import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class SelectPrioritiesPage extends HBox {

	private static ResourceBundle RES = SR5Constants.RES;

	private ImageView iView;
	private PriorityTable table;
	private Button btnNext, btnCancel;
	private NewPriorityCharacterGenerator charGen;

	//-------------------------------------------------------------------
	/**
	 */
	public SelectPrioritiesPage(NewPriorityCharacterGenerator charGen) {
		super(10);
		this.charGen = charGen;
		initCompontents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		Image image = new Image(getClass().getResourceAsStream("images/Shadowrun_hochkant.png"));

		iView = new ImageView(image);
		table = new PriorityTable(charGen);
		btnNext  = new Button(RES.getString("button.next"));
		btnNext.setMaxWidth(Double.MAX_VALUE);
		btnCancel= new Button(RES.getString("button.cancel"));
		btnCancel.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		TilePane btnBox = new TilePane();
		btnBox.setAlignment(Pos.BOTTOM_RIGHT);
		btnBox.setHgap(10);
		btnBox.getChildren().addAll(btnNext, btnCancel);

		Region spacing = new Region();

		VBox content = new VBox(10);
		content.getChildren().addAll(table, spacing, btnBox);
		VBox.setVgrow(spacing, Priority.SOMETIMES);
		VBox.setMargin(btnBox, new Insets(0,15,15,0));

		getChildren().addAll(iView,content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

}
