package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.shadowrun5.Shadowrun5Rules;
import org.prelle.rpgframework.shadowrun5.data.Shadowrun5DataPlugin;
import org.prelle.shadowrun5.ShadowrunCharacter;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun5BasePlugin implements RulePlugin<ShadowrunCharacter>, CommandBusListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	private Shadowrun5Rules core;
	private SR5GeneratorRulePlugin charGen;
	private Shadowrun5DataPlugin data;

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.PERSISTENCE);
		FEATURES.add(RulePluginFeatures.CHARACTER_CREATION);
		FEATURES.add(RulePluginFeatures.DATA);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public Shadowrun5BasePlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CORE";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 5 Character Generator";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return new ArrayList<>(FEATURES);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		if (core==null) {
			core = new Shadowrun5Rules();
			data = new Shadowrun5DataPlugin();
			charGen = new SR5GeneratorRulePlugin();
		}
		core.attachConfigurationTree(addBelow);
		data.attachConfigurationTree(addBelow);
		charGen.attachConfigurationTree(addBelow);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		logger.info("Will process "+type);
		switch (type) {
		case ENCODE:
		case DECODE:
			return core.willProcessCommand(src, type, values);
		case SHOW_CHARACTER_MODIFICATION_GUI:
		case SHOW_CHARACTER_CREATION_GUI:
		case SHOW_DATA_INPUT_GUI:
			return charGen.willProcessCommand(src, type, values);
		default:
			return false;
		}

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		if (!willProcessCommand(src, type, values))
			return new CommandResult(type, false, null, false);

		switch (type) {
		case ENCODE:
		case DECODE:
			return core.handleCommand(src, type, values);
		case SHOW_CHARACTER_MODIFICATION_GUI:
		case SHOW_CHARACTER_CREATION_GUI:
		case SHOW_DATA_INPUT_GUI:
			return charGen.handleCommand(src, type, values);
		default:
			return new CommandResult(type, false);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		logger.debug("Initialize");
		if (core==null) {
			core = new Shadowrun5Rules();
			data = new Shadowrun5DataPlugin();
			charGen = new SR5GeneratorRulePlugin();
		}
		
		core.init(callback);
		data.init(callback);
		charGen.init(callback);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return charGen.getAboutHTML();
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
