/**
 * 
 */
package org.prelle.shadowrun5.jfx;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.actions.ShadowrunAction.Category;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ChoiceType;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.Sense;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationValueType;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
@SuppressWarnings("restriction")
public class ShadowrunJFXUtils {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	//-------------------------------------------------------------------
	public static Region getAttributeModPane(MetaType type) {
		GridPane ret = new GridPane();
		ret.setHgap(10);
		ret.setStyle("-fx-border-width: 1px; -fx-border-color: textcolor-secondary; -fx-padding: 0.3em;");
		Map<Attribute, Integer> linesByAttr = new HashMap<>();
		
		int nextLine = 0;
		for (Modification tmp : type.getModifications()) {
			if (!(tmp instanceof AttributeModification)) 
				continue;
			AttributeModification mod = (AttributeModification) tmp;
			Integer line = linesByAttr.get(mod.getAttribute());
			if (line==null) {
				line = nextLine;
				linesByAttr.put(mod.getAttribute(), line);
				nextLine++;
				ret.add(new Label(mod.getAttribute().getShortName()), 0, line);
			}
			
			switch (mod.getType()) {
			case CURRENT:
				ret.add(new Label("+"+mod.getValue()), 1, line);
				break;
			case MAX:
				ret.add(new Label("/ max. "+mod.getValue()), 2, line);
				break;
			default:
			}
		}
		
		return ret;
	}

	//-------------------------------------------------------------------
	public static Region getOtherModPane(MetaType type) {
		VBox ret = new VBox();
		ret.setStyle("-fx-border-width: 1px; -fx-border-color: textcolor-secondary; -fx-padding: 0.3em;");
		
		for (Modification tmp : type.getModifications()) {
			if (tmp instanceof AttributeModification) 
				continue;
			ret.getChildren().add(new Label(ShadowrunTools.getModificationString(tmp)));
		}
		
		return ret;
	}

	//-------------------------------------------------------------------
	public static String getModificationTooltip(Modifyable mods) {
		StringBuffer buf = new StringBuffer();
		for (Iterator<Modification> it=mods.getModifications().iterator(); it.hasNext();) {
			Modification mod = it.next();
			if (mod instanceof AttributeModification) {
				if ( ((AttributeModification)mod).getType()!=ModificationValueType.CURRENT)
					continue;
				buf.append( ((AttributeModification)mod).getValue());
			} else if (mod instanceof SkillModification) {
					buf.append( ((SkillModification)mod).getValue());
			} else {
				logger.warn("Unsupported modification "+mod.getClass());
				buf.append(mod.toString());
			}
			buf.append(" (");
			if (mod.getSource()==null) {
				buf.append("?");
			} else if (mod.getSource().getClass()==CarriedItem.class) {
				buf.append(  ((CarriedItem)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==String.class) {
//				if (mod.getSource()==SplitterTools.LEVEL2)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level2"));
//				else if (mod.getSource()==SplitterTools.LEVEL3)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level3"));
//				else if (mod.getSource()==SplitterTools.LEVEL4)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level4"));
//				else if (((String)mod.getSource()).startsWith(EquipmentTools.TOTAL_HANDICAP))
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.handicap"));
//				else
					logger.warn("Unknown modification source string '"+mod.getSource()+"/"+mod.getClass()+"'");
			} else if (mod.getSource().getClass()==Attribute.class) {
				buf.append(  ((Attribute)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==MetaType.class) {
				buf.append( ((MetaType)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==AdeptPowerValue.class) {
				buf.append( ((AdeptPowerValue)mod.getSource()).getName() );
//			} else if (mod.getSource().getClass()==Power.class) {
//				buf.append(  ((Power)mod.getSource()).getName() );
//			} else if (mod.getSource().getClass()==PowerReference.class) {
//				buf.append(  ((PowerReference)mod.getSource()).getPower().getName()+" "+((PowerReference)mod.getSource()).getCount() );
//			} else if (mod.getSource().getClass()==MastershipReference.class) {
//				buf.append(  ((MastershipReference)mod.getSource()).getMastership().getName());
//			} else if (mod.getSource().getClass()==Mastership.class) {
//				buf.append(  ((Mastership)mod.getSource()).getName());
			} else {
				logger.warn("Unsupported modification source "+mod.getSource().getClass());
				buf.append(String.valueOf(mod.getSource()));
			}
			buf.append(")");
			
			if (it.hasNext())
				buf.append("\n");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public static Object choose(ScreenManagerProvider provider, ShadowrunCharacter model, ChoiceType selectFrom) {
		logger.debug("choose "+selectFrom);
		
		ChoiceBox<Object> options = new ChoiceBox<>();
		switch (selectFrom) {
		case ATTRIBUTE:
			for (Attribute attr : Attribute.primaryValues())
				options.getItems().add(attr);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case COMBAT_SKILL:
			options.getItems().addAll(ShadowrunCore.getSkills(SkillType.COMBAT));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Skill)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case LIMIT:
			options.getItems().addAll(Attribute.LIMIT_SOCIAL);
			options.getItems().addAll(Attribute.LIMIT_MENTAL);
			options.getItems().addAll(Attribute.LIMIT_PHYSICAL);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case MATRIX_ACTION:
			options.getItems().addAll(ShadowrunCore.getActions(Category.MATRIX));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((ShadowrunAction)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case PHYSICAL_ATTRIBUTE:
			for (Attribute attr : Attribute.physicalValues())
				options.getItems().add(attr);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case SKILL:
			options.getItems().addAll(ShadowrunCore.getSkills());
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Skill)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case SKILLGROUP:
			options.getItems().addAll(ShadowrunCore.getSkillgroups());
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((SkillGroup)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case SENSE:
			for (Sense sense : Sense.values())
				options.getItems().addAll(sense);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Sense)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		}
		
		options.getSelectionModel().select(0);
		VBox content = new VBox(20);
		content.getChildren().addAll(options);
		
		String title = UI.getString("dialog.choicetype.title");
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, title, content);
		
		return options.getSelectionModel().getSelectedItem();
	}

}
