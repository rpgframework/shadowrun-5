/**
 * 
 */
package org.prelle.shadowrun5.jfx;

/**
 * @author Stefan
 *
 */
public enum ViewMode {

	GENERATION,
	MODIFICATION,
	VIEW_ONLY
	
}
