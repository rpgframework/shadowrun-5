/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.spells.SpellSelectionPane;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageAlchemy extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private SpellSelectionPane spellPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageAlchemy(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		/*
		 * Enable or disable page
		 */
		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			if (type.usesSpells()) {
				logger.debug(type+" uses spells - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use spells - disable page");
				activeProperty().set(false);
			}
		} else
			logger.warn("No magic or resonance type selected yet");
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectAlchemy.title"));

		spellPane = new SpellSelectionPane(charGen.getAlchemyController(), this, true);
		spellPane.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		imgRec = new ImageView(new Image(getClass().getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectAlchemy.instruct"));
		instruction.setWrapText(true);

		String fName = "images/shadowrun/img_spells.png";
		InputStream in = getClass().getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(false, false);
		int pointsSK = charGen.getAlchemyController().getSpellsLeft();
		side.setPoints(pointsSK);
		side.setPoints2Name(null);
		side.setPoints2(-1);
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);

		VBox layout = new VBox(5);
		layout.getChildren().addAll(lineInstr, spellPane);
		spellPane.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
//		logger.debug("----------------handle--"+event.getType()+"---------------------");
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_SPELLS_RITUALS:
			logger.debug("RCV "+event);
			side.setPoints((int) event.getValue());
			break;
		case MAGICORRESONANCE_CHANGED:
			logger.debug("RCV "+event);
			MagicOrResonanceOption option = (MagicOrResonanceOption) event.getKey();
			// Enable or disable page
			if (option.getType().usesSpells()) {
				logger.debug(option.getType()+" uses spells - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(option.getType()+" does not use spells - disable page");
				activeProperty().set(false);
			}
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
			} else {
				lineInstr.setVisible(true);
				instruction.setText(String.format(UI.getString("wizard.selectAlchemy.instruct"), charGen.getConcept().getName()));
			}
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			charGen = (CharacterGenerator) event.getKey();
			break;
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			side.setPoints( charGen.getSpellController().getSpellsLeft() );
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}
