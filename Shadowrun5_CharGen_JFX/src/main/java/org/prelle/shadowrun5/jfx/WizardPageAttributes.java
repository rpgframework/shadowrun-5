/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.jfx.attributes.AttributePanePrimary2;
import org.prelle.shadowrun5.jfx.attributes.AttributePaneSecondary;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageAttributes extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private AttributePanePrimary2 primary;
	private AttributePaneSecondary derived;
	private Label description;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageAttributes(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectAttr.title"));

		primary = new AttributePanePrimary2(charGen.getAttributeController());
		primary.setData(charGen.getCharacter());
		derived = new AttributePaneSecondary(charGen.getAttributeController(), CharGenMode.CREATING);
		derived.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		description = new Label();
		description.setWrapText(true);

		String fName = "images/shadowrun/img_attributes.png";
		InputStream in = SR5Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(false, false);
		side.setPoints(charGen.getAttributeController().getPointsLeft());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);

		content.getChildren().addAll(primary, derived, description);

		VBox layout = new VBox();
		layout.getChildren().add(content);
		VBox.setVgrow(layout, Priority.NEVER);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		primary.getStyleClass().add("text-small-secondary");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getAttributeController().getPointsLeft());
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_ATTRIBUTES:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getAttributeController().getPointsLeft());
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		default:
		}
	}

}
