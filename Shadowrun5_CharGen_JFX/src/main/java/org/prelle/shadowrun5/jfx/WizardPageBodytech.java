/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.items.CommonEquipmentSelectionPane;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageBodytech extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private CommonEquipmentSelectionPane selectPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageBodytech(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectBodytech.title"));

		selectPane = new CommonEquipmentSelectionPane(charGen.getEquipmentController(), this, ItemType.bodytechTypes());
		selectPane.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		imgRec = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectBodytech.instruct"));
		instruction.setWrapText(true);

		String fName = "images/shadowrun/img_attributes.png";
		InputStream in = getClass().getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		side.setPoints2(charGen.getCharacter().getNuyen());
		side.setPoints2Name(UI.getString("label.nuyen"));
		side.setPoints(Integer.MAX_VALUE);
		side.setKarma(-1);
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);

		VBox layout = new VBox(5);
		layout.getChildren().addAll(lineInstr, selectPane);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_COMPLEX_FORMS:
			logger.debug("RCV "+event);
			side.setPoints((int) event.getValue());
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
			} else {
				lineInstr.setVisible(true);
				instruction.setText(String.format(UI.getString("wizard.selectBodytech.instruct"), charGen.getConcept().getName()));
			}
		case NUYEN_CHANGED:
			side.setPoints2(charGen.getCharacter().getNuyen());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}
