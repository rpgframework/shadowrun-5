/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageCharacterConcept extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private static PropertyResourceBundle CORE = ShadowrunCore.getI18nResources();

	private CharacterGenerator charGen;

	private static Map<CharacterConcept,Image> imageByRace;

	private ToggleGroup radioGrp;
	private List<RadioButton> radios;
	private List<ImageView> images;
	private Label descHeading;
	private Label description;

	//--------------------------------------------------------------------
	public WizardPageCharacterConcept(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		imageByRace  = new HashMap<CharacterConcept, Image>();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		// Select first data
		radioGrp.selectToggle(radioGrp.getToggles().get(0));

		Image img = new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/shadowrun/img_charconcept.png"));
		super.setImage(img);
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private Image getImage(CharacterConcept concept) {
		Image img = imageByRace.get(concept);
		if (img==null) {
			String fname = "images/concept_"+concept.getId()+".png";
			logger.trace("Load "+fname);
			InputStream in = getClass().getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
				imageByRace.put(concept, img);
			} else
				logger.warn("Missing image at "+fname);
		}
		return img;
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		radios = new ArrayList<RadioButton>();
		images = new ArrayList<ImageView>();

		setTitle(UI.getString("wizard.selectConcept.title"));

		// Buttons
		radioGrp = new ToggleGroup();

		// Fill with data
		for (CharacterConcept cpt : ShadowrunCore.getCharacterConcepts()) {
			RadioButton radio = new RadioButton(cpt.getName());
			radio.setUserData(cpt);
			radios.add(radio);
			radioGrp.getToggles().add(radio);

			ImageView iview = new ImageView();
			iview.setUserData(cpt);
			images.add(iview);
			Image img = getImage(cpt);
			if (img!=null) {
				iview.setImage(img);
			}
		}

		descHeading = new Label();

		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		GridPane grid = new GridPane();
		grid.setGridLinesVisible(false);
		ColumnConstraints col2 = new ColumnConstraints(100);
		ColumnConstraints col3 = new ColumnConstraints(75);
		grid.getColumnConstraints().add(new ColumnConstraints());
		grid.getColumnConstraints().add(new ColumnConstraints());
		grid.getColumnConstraints().add(col2);
		grid.getColumnConstraints().add(col3);
		// Radios
		for (int i=0; i<radios.size(); i++) {
			RadioButton button = radios.get(i);
			grid.add(button, 0,i+1);
			GridPane.setMargin(button, new Insets(-55,0,0,0));
			grid.getRowConstraints().add(new RowConstraints(55));
		}
		grid.getRowConstraints().add(new RowConstraints(55));

		// Images
		for (int i=0; i<images.size(); i++) {
			int x = 2 + (i%2);
			int y = i;
			grid.add(images.get(i), x, y, 3,2);
		}

		ScrollPane scroll = new ScrollPane(description);
		scroll.setFitToWidth(true);
		scroll.setStyle("-fx-min-width: 15em; -fx-max-width: 21em");
		scroll.setMaxHeight(Double.MAX_VALUE);
		VBox descBox = new VBox(20);
		descBox.getChildren().addAll(descHeading, scroll);
		VBox.setVgrow(scroll, Priority.ALWAYS);

		HBox content = new HBox();
		content.setSpacing(60);
		content.getChildren().addAll(grid, descBox);

		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		for (RadioButton radio : radios)
			radio.getStyleClass().add("text-body");
//		spinner.getStyleClass().add("text-small-secondary");
		descHeading.getStyleClass().add("text-small-subheader");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		radioGrp.selectedToggleProperty().addListener( (ov,o,n) -> {
			logger.debug("concept changed to1 "+n);
			CharacterConcept newConcept = (CharacterConcept) ((RadioButton)n).getUserData();
			logger.debug("concept changed to2 "+newConcept);
			update(newConcept);
			if (charGen!=null)
				charGen.setConcept(newConcept);
		}
		);

		for (ImageView iview : images) {
			iview.setOnMouseClicked(event -> {
				CharacterConcept newConcept = (CharacterConcept) ((ImageView)event.getSource()).getUserData();
				for (RadioButton foo : radios) {
					if (foo.getUserData()==newConcept)
						radioGrp.selectToggle(foo);
				}
//				update(newConcept);
			});
		}
	}

	//-------------------------------------------------------------------
	private void update(CharacterConcept concept) {
		descHeading.setText(concept.getName());
		description.setText( CORE.getString("concept."+concept.getId()+".text") );
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		CharacterConcept concept = (CharacterConcept) radioGrp.getSelectedToggle().getUserData();
		logger.warn("TODO: eventually set concept "+concept);
		charGen.getCharacter().setConcept(concept);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			charGen = (CharacterGenerator) event.getKey();
			break;
		default:
		}
	}

}
