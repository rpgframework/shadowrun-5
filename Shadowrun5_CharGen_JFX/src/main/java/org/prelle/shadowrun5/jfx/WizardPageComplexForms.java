/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.jfx.cforms.ComplexFormSelectionPane;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageComplexForms extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private ComplexFormSelectionPane selectPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageComplexForms(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		/*
		 * Enable or disable page
		 */
		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			if (type.usesResonance()) {
				logger.debug(type+" uses resonance - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use resonance - disable page");
				activeProperty().set(false);
			}
		} else
			logger.warn("No magic or resonance type selected yet");
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectComplexForms.title"));

		selectPane = new ComplexFormSelectionPane(charGen.getComplexFormController(), this);
		selectPane.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		imgRec = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectComplexForms.instruct"));
		instruction.setWrapText(true);

		String fName = "images/shadowrun/img_complexforms.png";
		InputStream in = getClass().getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		int pointsSK = charGen.getSpellController().getSpellsLeft();
		side.setPoints(pointsSK);
		side.setPoints2Name(null);
		side.setPoints2(-1);
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);

		VBox layout = new VBox(5);
		layout.getChildren().addAll(lineInstr, selectPane);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getComplexFormController().getComplexFormsLeft());
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_COMPLEX_FORMS:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getComplexFormController().getComplexFormsLeft());
			break;
		case MAGICORRESONANCE_CHANGED:
			logger.debug("RCV "+event);
			MagicOrResonanceOption option = (MagicOrResonanceOption) event.getKey();
			// Enable or disable page
			if (option.getType().usesResonance()) {
				logger.debug(option.getType()+" uses resonance - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(option.getType()+" does not use resonance - disable page");
				activeProperty().set(false);
			}
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
			} else {
				lineInstr.setVisible(true);
				instruction.setText(String.format(UI.getString("wizard.selectComplexForms.instruct"), charGen.getConcept().getName()));
			}
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}
