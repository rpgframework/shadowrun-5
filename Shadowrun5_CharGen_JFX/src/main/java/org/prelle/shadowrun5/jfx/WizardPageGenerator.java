/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.CharacterGeneratorRegistry;

import de.rpgframework.character.HardcopyPluginData;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WizardPageGenerator extends WizardPage {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter model;
	private ListView<CharacterGenerator> options;
	private Label descHeading;
	private Label description;

	//--------------------------------------------------------------------
	public WizardPageGenerator(Wizard wizard, ShadowrunCharacter model) {
		super(wizard);
		this.model = model;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		options= new ListView<CharacterGenerator>();
		options.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		options.setCellFactory(new Callback<ListView<CharacterGenerator>, ListCell<CharacterGenerator>>() {
			public ListCell<CharacterGenerator> call(ListView<CharacterGenerator> arg0) {
				return new CharacterGeneratorListCell();
			}
		});

		setTitle(UI.getString("wizard.selectGenerator.title"));

		// Fill with data
		for (CharacterGenerator cpt : CharacterGeneratorRegistry.getGenerators()) {
			logger.info("Found "+cpt);
//			RadioButton radio = new RadioButton(cpt.getName());
//			radio.setUserData(cpt);
			options.getItems().add(cpt);
		}
		options.getSelectionModel().select(CharacterGeneratorRegistry.getSelected());

		descHeading = new Label();

		description = new Label();
		description.setWrapText(true);


		Image img = new Image(getClass().getResourceAsStream("images/shadowrun/img_generator.png"));
		super.setImage(img);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox descBox = new VBox(20);
		descBox.getChildren().addAll(descHeading, description);
		description.setPrefWidth(400);


		HBox content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(options, descBox);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		descHeading.getStyleClass().add("text-small-subheader");
		description.getStyleClass().add("text-body");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		options.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CharacterGenerator>() {
			public void changed(ObservableValue<? extends CharacterGenerator> observable,
					CharacterGenerator oldValue, CharacterGenerator newValue) {
					update(newValue);
			}
		});
	}

	//-------------------------------------------------------------------
	private void update(CharacterGenerator charGen) {
		if (charGen!=null) {
			descHeading.setText(charGen.getName());
			description.setText( charGen.getHelpText() );
			CharacterGeneratorRegistry.select(charGen, model);
		} else {
			descHeading.setText(null);
			description.setText(null);
		}
	}

	//--------------------------------------------------------------------
	public boolean canBeFinisshed() {
		if (CharacterGeneratorRegistry.getSelected()!=null)
			return CharacterGeneratorRegistry.getSelected().hasEnoughData();
		return false;
	}

}

class CharacterGeneratorListCell extends ListCell<CharacterGenerator> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private VBox box;
	private Label lblHeading;
	private Hyperlink lblHardcopy;

	//--------------------------------------------------------------------
	public CharacterGeneratorListCell() {
		lblHeading = new Label();
		lblHardcopy = new Hyperlink();
		box = new VBox(5);
		box.getChildren().addAll(lblHeading, lblHardcopy);

		lblHardcopy.setOnAction(event -> {
			HardcopyPluginData hardcopy = (HardcopyPluginData) ((Hyperlink)event.getSource()).getUserData();
			logger.warn("TODO: open "+hardcopy.getPlugin().getID()+" on page "+hardcopy.getPage());
		});
	}


	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CharacterGenerator item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(box);
			lblHeading.setText(item.getName());
			lblHardcopy.setText(item.getProductName()+" "+item.getPage());
			lblHardcopy.setUserData(item);
		}
	}
}