/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.util.Collection;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.PriorityMagicOrResonanceGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.attributes.AttributeField;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageMagicOrResonance extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter model;
	private CharacterGenerator charGen;
	private MagicOrResonanceController magicGen;
	private AttributeController attrCtrl;

	private ListView<MagicOrResonanceOption> morOptList;
	private Hyperlink page	;
	private Label description;

	private AttributeField fldEdge;
	private AttributeField fldMagic;
	private AttributeField fldResonance;
	private VBox tileEdge;
	private VBox tileMagic;
	private VBox tileResonance;

	private HBox content;
	private WizardPointsPane side;

	/**
	 * If a choice has been made on this page and it is revisited,
	 * these modifications must be undone
	 */
	private Collection<Modification> undoIfVisited;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageMagicOrResonance(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.magicGen = charGen.getMagicOrResonanceController();
		if (this.magicGen==null)
			throw new NullPointerException("MagicOrResonance controller not set");
		attrCtrl = charGen.getAttributeController();
		model = charGen.getCharacter();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

//		morOptList.getSelectionModel().select(0);
		refreshValues();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectMagicOrResonance.title"));

		morOptList = new ListView<MagicOrResonanceOption>();
		morOptList.setCellFactory(new Callback<ListView<MagicOrResonanceOption>, ListCell<MagicOrResonanceOption>>() {
			@Override
			public ListCell<MagicOrResonanceOption> call(ListView<MagicOrResonanceOption> arg0) {
				return new MagicOrResonanceCell();
			}
		});

		morOptList.getItems().addAll(magicGen.getAvailable());

		/*
		 * Edge
		 */
		fldEdge = new AttributeField();
		fldMagic = new AttributeField();
		fldResonance = new AttributeField();


		content = new HBox();
		content.setSpacing(5);

		page = new Hyperlink();

		description = new Label();
		description.setWrapText(true);

		side = new WizardPointsPane(false, false);
		side.setKarma(model.getKarmaFree());
		setSide(side);

		Image img = new Image(getClass().getResourceAsStream("images/shadowrun/img_magic_reso.png"));
		super.setImage(img);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		description.setStyle("-fx-max-width: 25em;");

		Label heaEdge = new Label(Attribute.EDGE.getName());
		heaEdge.getStyleClass().add("text-subheader");
		tileEdge = new VBox();
//		tileEdge.getStyleClass().add("bordered");
		tileEdge.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileEdge.getChildren().addAll(heaEdge, fldEdge);

		Label heaMagic = new Label(Attribute.MAGIC.getName());
		heaMagic.getStyleClass().add("text-subheader");
		tileMagic = new VBox();
//		tileMagic.getStyleClass().add("bordered");
		tileMagic.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileMagic.getChildren().addAll(heaMagic, fldMagic);

		Label heaResonance = new Label(Attribute.RESONANCE.getName());
		heaResonance.getStyleClass().add("text-subheader");
		tileResonance = new VBox();
//		tileResonance.getStyleClass().add("bordered");
		tileResonance.setStyle("-fx-spacing: 1em; -fx-padding: 0.5em");
		tileResonance.getChildren().addAll(heaResonance, fldResonance);

		VBox valueCol = new VBox();
		valueCol.setStyle("-fx-spacing: 2em;");
		valueCol.setMaxHeight(Double.MAX_VALUE);
		valueCol.getChildren().addAll(tileEdge, tileMagic, tileResonance);



		VBox bxInfo = new VBox(20);
		bxInfo.getChildren().add(page);
		bxInfo.getChildren().add(description);

		content.getChildren().addAll(morOptList, valueCol, bxInfo);
		setImageInsets(new Insets(-40,0,0,0));
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		page.getStyleClass().add("text-body");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		morOptList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));

		fldEdge.setOnDecAction(event -> attrCtrl.decrease(Attribute.EDGE));
		fldEdge.setOnIncAction(event -> attrCtrl.increase(Attribute.EDGE));
		fldMagic.setOnDecAction(event -> attrCtrl.decrease(Attribute.MAGIC));
		fldMagic.setOnIncAction(event -> attrCtrl.increase(Attribute.MAGIC));
		fldResonance.setOnDecAction(event -> attrCtrl.decrease(Attribute.RESONANCE));
		fldResonance.setOnIncAction(event -> attrCtrl.increase(Attribute.RESONANCE));
	}

	//-------------------------------------------------------------------
	private void update(MagicOrResonanceOption option) {
		if (option==null) {
			tileMagic.setDisable(true);
			tileResonance.setDisable(true);
			page.setText(null);
			description.setText(null);
		} else {
			MagicOrResonanceType type = option.getType();

			try {
				page.setText(type.getProductName()+" "+option.getType().getPage());
				description.setText(type.getHelpText());
			} catch (Exception e) {
				logger.error(e.toString());
			}

			tileMagic.setDisable(!type.usesMagic());
			tileResonance.setDisable(!type.usesResonance());
		}

		side.setPoints(attrCtrl.getPointsLeftSpecial());
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MagicOrResonanceOption> property, MagicOrResonanceOption oldRace,
			MagicOrResonanceOption newOpt) {
		logger.debug("Currently display metatype "+newOpt);

		magicGen.select(newOpt);
		update(newOpt);
		refreshValues();
	}

	//--------------------------------------------------------------------
	private void refreshValues() {
		fldEdge.setText(String.valueOf(model.getAttribute(Attribute.EDGE).getModifiedValue()));
		fldMagic.setText(String.valueOf(model.getAttribute(Attribute.MAGIC).getModifiedValue()));
		fldResonance.setText(String.valueOf(model.getAttribute(Attribute.RESONANCE).getModifiedValue()));
		fldEdge.setDisableDec(!attrCtrl.canBeDecreased(Attribute.EDGE));
		fldEdge.setDisableInc(!attrCtrl.canBeIncreased(Attribute.EDGE));
		fldMagic.setDisableDec(!attrCtrl.canBeDecreased(Attribute.MAGIC));
		fldMagic.setDisableInc(!attrCtrl.canBeIncreased(Attribute.MAGIC));
		fldResonance.setDisableDec(!attrCtrl.canBeDecreased(Attribute.RESONANCE));
		fldResonance.setDisableInc(!attrCtrl.canBeIncreased(Attribute.RESONANCE));
		side.setPoints(attrCtrl.getPointsLeftSpecial());
		side.setKarma(model.getKarmaFree());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case MAGICORRESONANCE_AVAILABLE_CHANGED:
			logger.trace("RCV "+event);
			List<MagicOrResonanceOption> avail = (List<MagicOrResonanceOption>) event.getKey();
			morOptList.getItems().clear();
			morOptList.getItems().addAll(avail);
			morOptList.getSelectionModel().select(0);
			break;
		case ATTRIBUTE_CHANGED:
			Attribute attr = (Attribute)event.getKey();
			if (attr!=Attribute.EDGE && attr!=Attribute.MAGIC && attr!=Attribute.RESONANCE)
				return;
			logger.debug("RCV "+event);
			refreshValues();
			break;
		case POINTS_LEFT_ATTRIBUTES:
			logger.trace("RCV "+event);
			refreshValues();
			break;
		case CHARACTER_CHANGED:
			logger.trace("RCV "+event);
			refreshValues();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	@Override
	public void pageVisited() {
		if (undoIfVisited!=null && !undoIfVisited.isEmpty()) {
			logger.debug("Page revisited - undo previous choice: "+undoIfVisited);
			charGen.undo(undoIfVisited);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 * @see org.prelle.javafx.WizardPage#pageLeft(org.prelle.javafx.CloseType)
	 */
	@Override
	public void pageLeft() {
		if (magicGen instanceof PriorityMagicOrResonanceGenerator) {
			undoIfVisited = charGen.apply(model.getMagicOrResonanceType().getName(), magicGen.getToApplyFromSelection());
			logger.debug("Page left - store for undo: "+undoIfVisited);
		}
	}

}

class MagicOrResonanceCell extends ListCell<MagicOrResonanceOption> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private HBox layout;
	private VBox box;
	private Label lblHeading;
	private Hyperlink lblHardcopy;
	private Label lblKarma;

	//--------------------------------------------------------------------
	public MagicOrResonanceCell() {
		lblHeading = new Label();
		lblHardcopy = new Hyperlink();
		lblKarma = new Label();
		lblKarma.getStyleClass().add("text-subheader");
		box = new VBox(5);
		box.getChildren().addAll(lblHeading, lblHardcopy);

		lblHardcopy.setOnAction(event -> {
			MagicOrResonanceOption hardcopy = (MagicOrResonanceOption) ((Hyperlink)event.getSource()).getUserData();
			logger.warn("TODO: open "+hardcopy);
		});
		layout = new HBox(5);
		layout.getChildren().addAll(box, lblKarma);
		HBox.setHgrow(box, Priority.ALWAYS);
		box.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MagicOrResonanceOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			lblHeading.setText(item.getType().getName());
			lblHardcopy.setText(item.getType().getProductName()+" "+item.getType().getPage());
			lblHardcopy.setUserData(item);
			lblKarma.setText(item.getType().getCost()+"");
		}
	}
}