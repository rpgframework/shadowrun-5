/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.MetaTypeOption;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.MetatypeController;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageMetaType extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".metatype");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private MetatypeController charGen;
	private ShadowrunCharacter model;

	private static Map<MetaType,Image> imageByRace;

	private ListView<MetaTypeOption> metaList;
	private Hyperlink page	;
	private Label lbWarning;
	private Label description;
	private Region statsAttributes;
	private Region statsPowers;

	private HBox sideBySide;
	private HBox content;
	private WizardPointsPane side;

	//-------------------------------------------------------------------
	static {
		imageByRace = new HashMap<MetaType, Image>();
	}

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageMetaType(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		logger.info("----------------WizardPageMetaType.init-----------------------\n\n\n");
		this.charGen = charGen.getMetatypeController();
		if (this.charGen==null)
			throw new NullPointerException("Metatype controller not set");
		model = charGen.getCharacter();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		metaList.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectMetatype.title"));

		metaList = new ListView<MetaTypeOption>();
		metaList.setCellFactory(new Callback<ListView<MetaTypeOption>, ListCell<MetaTypeOption>>() {
			@Override
			public ListCell<MetaTypeOption> call(ListView<MetaTypeOption> arg0) {
				return new MetatypeListCell();
			}
		});

		metaList.getItems().addAll(charGen.getAvailable());

		content = new HBox();
		content.setSpacing(5);

		page = new Hyperlink();
		lbWarning = new Label();
		lbWarning.setStyle("-fx-text-fill: -fx-focus-color");

		description = new Label();
		description.setWrapText(true);

		sideBySide = new HBox(20);

		side = new WizardPointsPane(false, false);
		side.setKarma(model.getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		description.setPrefWidth(700);

		VBox bxInfo = new VBox(20);
		bxInfo.getChildren().add(page);
		bxInfo.getChildren().add(lbWarning);
		bxInfo.getChildren().add(description);
		bxInfo.getChildren().add(sideBySide);

		content.getChildren().addAll(metaList, bxInfo);
		setImageInsets(new Insets(-40,0,0,0));
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		page.getStyleClass().add("text-body");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		metaList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));
	}

	//-------------------------------------------------------------------
	private void update(MetaTypeOption option) {
		MetaType newRace = option.getType();
		if (newRace.getVariantOf()!=null)
			newRace = newRace.getVariantOf();
		Image img = imageByRace.get(newRace);
		if (img==null) {
			String fname = "images/shadowrun/race_"+newRace.getKey()+".png";
			logger.trace("Load "+fname);
			InputStream in = SR5Constants.class.getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
				imageByRace.put(newRace, img);
			} else
				logger.warn("Missing image at "+fname);
		}
		setImage(img);
		newRace = option.getType();

		try {
			page.setText(option.getType().getProductName()+" "+option.getType().getPage());
			if (!newRace.getPlugin().getID().equals("CORE")) {
				lbWarning.setText("Merkmale dieses Metatypes werden derzeit i.d.R. noch nicht berücksichtigt");
			} else
				lbWarning.setText(null);
			description.setText(newRace.getHelpText());

			// Remove old entries
			sideBySide.getChildren().clear();
			// Create new panes
			statsAttributes = ShadowrunJFXUtils.getAttributeModPane(newRace);
			statsPowers     = ShadowrunJFXUtils.getOtherModPane(newRace);
			// Style them
			statsAttributes.getStyleClass().addAll("text-body","stats-block");
			statsPowers.getStyleClass().addAll("text-body","stats-block");
			// Layout them
			statsAttributes.setMinWidth(140);
			statsPowers.setMinWidth(240);
			// Add them
			sideBySide.getChildren().add(statsAttributes);
			sideBySide.getChildren().add(statsPowers);
		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MetaTypeOption> property, MetaTypeOption oldRace,
			MetaTypeOption newRace) {
		logger.debug("Currently display metatype "+newRace);

		if (newRace!=null) {
			charGen.select(newRace.getType());
			update(newRace);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case METATYPES_AVAILABLE_CHANGED:
		case CONSTRUCTIONKIT_CHANGED:
			logger.info("*****RCV "+event);
			metaList.getItems().clear();
			metaList.getItems().addAll(charGen.getAvailable());
			metaList.getSelectionModel().select(0);
			break;
		case CHARACTER_CHANGED:
		case EXPERIENCE_CHANGED:
			logger.info("*****RCV "+event);
			side.setKarma(model.getKarmaFree());
			break;
		default:
		}
	}

}

class MetatypeListCell extends ListCell<MetaTypeOption> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".metatype");

	private HBox layout;
	private Label lblKarma;
	private ImageView ivMetatype;
	private Label lblName;

	//--------------------------------------------------------------------
	public MetatypeListCell() {
		lblKarma = new Label();
		lblKarma.getStyleClass().add("text-subheader");
		lblName  = new Label();
		lblName.getStyleClass().add("text-small-subheader");
		ivMetatype = new ImageView();
		ivMetatype.setFitHeight(64);
		ivMetatype.setFitWidth(64);

		layout = new HBox(5);
		layout.getChildren().addAll(ivMetatype, lblName, lblKarma);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetaTypeOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
			ivMetatype.setImage(null);
		} else {
			setGraphic(layout);
			lblName.setText(item.getType().getName());
			lblKarma.setText(""+item.getAdditionalKarmaKost());

			String metaID = (item.getType().getVariantOf()!=null)?item.getType().getVariantOf().getId():item.getType().getId();
			String fName = "images/metaicon_"+metaID+"_rike.png";
			InputStream in = SR5Constants.class.getResourceAsStream(fName);
			if (in==null) {
				logger.warn("Missing "+fName);
			} else {
				Image img = new Image(in);
				ivMetatype.setImage(img);
			}
		}
	}
}