/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.ShadowrunCharacter.Gender;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author Stefan
 *
 */
public class WizardPageName extends WizardPage implements GenerationEventListener, ChangeListener<String>  {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private GridPane content;
	private TextField runnerName;
	private TextField realName;
	private ChoiceBox<Gender> gender;
	private TextField hairColor;
	private TextField eyeColor;
	private TextField weight;
	private TextField size_tf;
	private TextField ethnicity;
	private Slider size;
	private ImageView portrait;
	private byte[] imgData;
	private Label lblToDosHead;
	private Label lblToDos;

//	private Button randomHair;
//	private Button randomEyes;
//	private Button randomSize;
	private Button openFileChooser;

	//--------------------------------------------------------------------
	public WizardPageName(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

//		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectName.title"));

		runnerName= new TextField();
		realName  = new TextField();
		hairColor = new TextField();
		eyeColor  = new TextField();
		weight    = new TextField();
		size_tf   = new TextField();
		ethnicity = new TextField();
		weight.setPromptText(UI.getString("prompt.weight"));
		size_tf.setPromptText(UI.getString("prompt.size"));

		int min = 50;
		int max = 250;
		int avg = (max-min)/2 + min;
		size      = new Slider(min,max,avg);
		size.setShowTickLabels(true);
		size.setShowTickMarks(true);
//		size.setMajorTickUnit(50);
		size.setMinorTickCount(5);
		gender    = new ChoiceBox<Gender>(FXCollections.observableArrayList(Gender.MALE, Gender.FEMALE));
		portrait = new ImageView();
		portrait.setFitHeight(200);
		portrait.setFitWidth(200);
		portrait.setPreserveRatio(true);
		portrait.setImage(new Image(SR5Constants.class.getResourceAsStream("images/guest-256.png")));
		portrait.setStyle("-fx-border-width: 1px; -fx-border-style: solid; -fx-border-color: -fx-box-border;");
		lblToDos = new Label("-");
		lblToDos.setStyle("-fx-text-fill: red;");
		lblToDos.setWrapText(true);

		/*
		 * Buttons
		 */
//		randomHair = new Button(UI.getString("button.roll"));
//		randomEyes = new Button(UI.getString("button.roll"));
//		randomSize = new Button(UI.getString("button.roll"));

		/*
		 * Button portrait selection
		 */
		openFileChooser = new Button(UI.getString("button.select"));


		Image img = new Image(SR5Constants.class.getResourceAsStream("images/shadowrun/img_generator.png"));
		super.setImage(img);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox foo = new HBox(portrait);
		foo.getStyleClass().add("bordered");

		VBox portBox = new VBox(10);
		portBox.setAlignment(Pos.TOP_CENTER);
		portBox.getChildren().addAll(foo, openFileChooser);

		lblToDosHead = new Label(UI.getString("label.todos"));

		content = new GridPane();
		content.setVgap(5);
		content.setHgap(5);
		content.add(new Label(UI.getString("label.streetname")), 0, 0);
		content.add(new Label(UI.getString("label.realname")), 0, 1);
		content.add(new Label(UI.getString("label.gender")), 0, 2);
		content.add(new Label(UI.getString("label.size"  )), 0, 3);
		content.add(new Label(UI.getString("label.weight")), 0, 4);
		content.add(new Label(UI.getString("label.hair"  )), 0, 5);
		content.add(new Label(UI.getString("label.eyes"  )), 0, 6);
		content.add(new Label(UI.getString("label.ethnicity")), 0, 7);
		content.add(lblToDosHead, 0, 9);
		content.add(runnerName, 1, 0);
		content.add(realName  , 1, 1);
		content.add(gender    , 1, 2);
		content.add(size_tf   , 1, 3);
		content.add(weight    , 1, 4);
		content.add(hairColor , 1, 5);
		content.add(eyeColor  , 1, 6);
		content.add(ethnicity , 1, 7);
		content.add(lblToDos  , 1, 9, 3,1);
//		content.add(randomSize, 2, 3);
//		content.add(randomHair, 2, 5);
//		content.add(randomEyes, 2, 6);

		Region padding = new Region();
		content.add(padding, 1, 8);
		GridPane.setVgrow(padding, Priority.ALWAYS);

		content.add(portBox  , 3, 0, 1,7);
		GridPane.setValignment(portBox, VPos.TOP);

		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		runnerName.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setName(n));
		realName.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setRealName(n));
		hairColor.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setHairColor(n));
		eyeColor.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setEyeColor(n));
		ethnicity.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setEthnicity(n));

//		randomHair.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				hairColor.setText(charGen.rollHair());
//			}
//		});
//		randomEyes.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				eyeColor.setText(charGen.rollEyes());
//			}
//		});
//		randomSize.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				int[] sAw = charGen.rollSizeAndWeight();
////				size.setValue(sAw[0]);
//				size_tf.setText(String.valueOf(sAw[0]));
//				weight.setText(sAw[1]+"");
//			}
//		});
		openFileChooser.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FileChooser chooser = new FileChooser();
				chooser.setInitialDirectory(new File(System.getProperty("user.home")));
				chooser.getExtensionFilters().addAll(
		                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
		                new FileChooser.ExtensionFilter("PNG", "*.png")
		            );
				File selection = chooser.showOpenDialog(new Stage());
				if (selection!=null) {
					try {
						byte[] imgBytes = Files.readAllBytes(selection.toPath());
						portrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
						imgData = imgBytes;
					} catch (IOException e) {
						logger.warn("Failed loading image from "+selection+": "+e);
					}
				}
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends String> textfield, String oldVal,	String newVal) {
		if (logger.isTraceEnabled())
			logger.trace("changed "+textfield+" from "+oldVal+" to "+newVal);

		String n = runnerName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); runnerName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); runnerName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); runnerName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); runnerName.setText(n); }

		n = realName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); realName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); realName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); realName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); realName.setText(n); }

		n = ethnicity.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); ethnicity.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); ethnicity.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); ethnicity.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); ethnicity.setText(n); }

		lblToDos.setText(String.join(", ", charGen.getToDos()));
		lblToDosHead.setVisible(!charGen.getToDos().isEmpty());
	}

	//-------------------------------------------------------------------
	private void copyToCharacter() {
		charGen.getCharacter().setName(runnerName.getText());
		charGen.getCharacter().setRealName(realName.getText());
		charGen.getCharacter().setHairColor(hairColor.getText());
		charGen.getCharacter().setEyeColor(eyeColor.getText());
		charGen.getCharacter().setGender(gender.getValue());
		charGen.getCharacter().setEthnicity(ethnicity.getText());
		try {
			if (weight.getText().length()>0)
				charGen.getCharacter().setWeight((int)Math.round(Double.parseDouble(weight.getText())));
			if (size_tf.getText().length()>0)
				charGen.getCharacter().setSize((int)Math.round(Double.parseDouble(size_tf.getText())));
			if (imgData!=null)
				charGen.getCharacter().setImage(imgData);
		} catch (Exception e) {
			wizard.getScreenManager().showAlertAndCall(AlertType.ERROR, "Invalid data", "Error validating character.\n"+e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			charGen = (CharacterGenerator) event.getKey();
			break;
		default:
//			logger.debug("RCV "+event.getType()+": "+charGen.getToDos());
			lblToDos.setText(String.join(", ",charGen.getToDos()));
			lblToDosHead.setVisible(!charGen.getToDos().isEmpty());
		}
	}

}
