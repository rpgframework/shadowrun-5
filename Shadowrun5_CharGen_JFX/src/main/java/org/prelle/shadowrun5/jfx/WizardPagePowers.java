/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.jfx.powers.PowerSelectionPane;
import org.prelle.shadowrun5.MagicOrResonanceOption;
import org.prelle.shadowrun5.MagicOrResonanceType;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPagePowers extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private PowerSelectionPane spellPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	private Button decPP, incPP;
	private Label lblPP;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPagePowers(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			if (type.usesPowers()) {
				logger.debug(type+" uses adept powers - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use adept powers - disable page");
				activeProperty().set(false);
			}
		} else
			logger.warn("No magic or resonance type selected yet");
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectPowers.title"));

		spellPane = new PowerSelectionPane(charGen.getPowerController(), this);
		spellPane.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		imgRec = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectPowers.instruct"));
		instruction.setWrapText(true);

		String fName = "images/shadowrun/img_powers.png";
		InputStream in = ClassLoader.getSystemResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		float pointsSK = charGen.getPowerController().getPowerPointsLeft();
		side.setPoints(pointsSK);
		side.setPoints2Name(UI.getString("label.powerpoints"));
		side.setPoints2(charGen.getCharacter().getPowerPoints());
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);

		decPP = new Button("\uE0C6");
		incPP = new Button("\uE0C5");
		lblPP = new Label("?");
		decPP.setStyle("-fx-font-family: 'Segoe UI Symbol'; -fx-border-width: 0px; -fx-text-fill: textcolor-highlight-primary");
		incPP.setStyle("-fx-font-family: 'Segoe UI Symbol'; -fx-border-width: 0px; -fx-text-fill: textcolor-highlight-primary");
		lblPP.setStyle("-fx-text-fill: textcolor-highlight-primary");
		HBox extra = new HBox(3, decPP, lblPP, incPP);
		side.setExtra(extra);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);

		VBox layout = new VBox(5);
		layout.getChildren().addAll(spellPane);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		decPP.setOnAction(ev -> charGen.getPowerController().decreasePowerPoint());
		incPP.setOnAction(ev -> charGen.getPowerController().increasePowerPoints());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_POWERS:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getPowerController().getPowerPointsLeft());
			lblPP.setText(String.valueOf(charGen.getCharacter().getBoughtPowerPoints()));
			decPP.setDisable(!charGen.getPowerController().canDecreasePowerPoints());
			incPP.setDisable(!charGen.getPowerController().canIncreasePowerPoints());
			side.setPoints2(charGen.getCharacter().getPowerPoints());
			break;
		case MAGICORRESONANCE_CHANGED:
			logger.debug("RCV "+event);
			MagicOrResonanceOption option = (MagicOrResonanceOption) event.getKey();
			// Enable or disable page
			if (option.getType().usesPowers()) {
				logger.debug(option.getType()+" uses adept powers - enable page");
				activeProperty().set(true);
				side.setPoints2(charGen.getCharacter().getPowerPoints());
			} else {
				logger.debug(option.getType()+" does not use adept powers - disable page");
				activeProperty().set(false);
			}
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
			} else {
				lineInstr.setVisible(true);
				instruction.setText(String.format(UI.getString("wizard.selectPowers.instruct"), charGen.getConcept().getName()));
			}
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getPowerController().getPowerPointsLeft());
			lblPP.setText(String.valueOf(charGen.getCharacter().getBoughtPowerPoints()));
			decPP.setDisable(!charGen.getPowerController().canDecreasePowerPoints());
			incPP.setDisable(!charGen.getPowerController().canIncreasePowerPoints());
			side.setPoints2(charGen.getCharacter().getPowerPoints());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}
