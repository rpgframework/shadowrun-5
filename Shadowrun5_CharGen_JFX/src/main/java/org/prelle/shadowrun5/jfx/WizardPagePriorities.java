/**
 * 
 */
package org.prelle.shadowrun5.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.CharacterGeneratorRegistry;

import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPagePriorities extends WizardPage {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName());

	private static PropertyResourceBundle CORE = ShadowrunCore.getI18nResources();

	private CharacterGenerator charGen;

	private PriorityTable table;
	private Label description;
	
	private VBox content;

	//--------------------------------------------------------------------
	public WizardPagePriorities(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectPrio.title"));

		table = new PriorityTable((NewPriorityCharacterGenerator) CharacterGeneratorRegistry.getSelected());
		
		content = new VBox();
		content.setSpacing(5);
		
		description = new Label();
		description.setWrapText(true);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		description.setPrefWidth(500);
		
		content.getChildren().addAll(table, description);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		table.getStyleClass().add("text-small-secondary");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

}
