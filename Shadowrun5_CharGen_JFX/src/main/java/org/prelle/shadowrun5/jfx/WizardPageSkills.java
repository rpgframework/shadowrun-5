/**
 *
 */
package org.prelle.shadowrun5.jfx;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.skills.SkillSelectionPane;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageSkills extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CharacterGenerator charGen;

	private SkillSelectionPane skillPane;
	private ImageView imgRec;
	private Label instruction;
	private HBox lineInstr;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageSkills(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectSkills.title"));

		skillPane = new SkillSelectionPane(charGen.getSkillController(), this);
		skillPane.setData(charGen.getCharacter());

		content = new HBox();
		content.setSpacing(20);

		imgRec = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		instruction = new Label(UI.getString("wizard.selectSkills.instruct"));
		instruction.setWrapText(true);

		String fName = "images/shadowrun/img_skills.png";
		InputStream in = getClass().getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, true);
		int pointsSK = charGen.getSkillController().getPointsLeftSkills();
		int pointsSG = charGen.getSkillController().getPointsLeftSkillGroups();
		int pointsLA = charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage();
		side.setPoints(pointsSK);
		if (pointsSG==-1) {
			side.setPoints2Name(null);
			side.setPoints2(-1);
		} else {
			side.setPoints2Name(UI.getString("label.points.skillgroups"));
			side.setPoints2(pointsSG);
		}
		side.setPoints3(pointsLA);
		side.setPoints3Name(UI.getString("label.points.knowledge"));
		side.setKarma(charGen.getCharacter().getKarmaFree());

		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		lineInstr = new HBox(5);
		lineInstr.getChildren().addAll(imgRec, instruction);

		VBox layout = new VBox(5);
		layout.getChildren().addAll(skillPane);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_SKILLS:
			logger.debug("RCV "+event);
			side.setPoints((int) event.getValue());
			side.setPoints3( (int)charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage());
			break;
		case POINTS_LEFT_SKILLGROUPS:
			logger.debug("RCV "+event);
			side.setPoints2((int) event.getValue());
			break;
		case POINTS_LEFT_KNOWLEDGE_SKILLS:
			logger.debug("RCV "+event);
			side.setPoints3( (int)charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage());
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
			} else {
				lineInstr.setVisible(true);
				instruction.setText(String.format(UI.getString("wizard.selectSkills.instruct"), charGen.getConcept().getName()));
			}
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setPoints( charGen.getSkillController().getPointsLeftSkills());
			side.setPoints2 ( charGen.getSkillController().getPointsLeftSkillGroups());
			side.setPoints3( (int)charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage());
			side.setKarma ( charGen.getCharacter().getKarmaFree());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}
