/**
 * 
 */
package org.prelle.shadowrun5.jfx;

import java.util.PropertyResourceBundle;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class WizardPointsPane extends GridPane {

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private Label lblPoints;
	private Label lblKarma;
	private Label lblPoints2;
	private Label lblPoints3;
	private Label heaKarma;
	private Label heaPoints2;
	private Label heaPoints3;
	private Node  extra;

	private boolean showPoints2;
	private boolean showPoints3;
	
	//-------------------------------------------------------------------
	public WizardPointsPane(boolean showPoints2, boolean showPoints3) {
		this.showPoints2 = showPoints2;
		this.showPoints3 = showPoints3;
		
		initComponents();
		initLayout();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblPoints = new Label("-");
		lblKarma  = new Label();
		lblPoints2  = new Label();
		heaPoints2  = new Label();
		lblPoints3  = new Label();
		heaPoints3  = new Label();
		
		lblPoints.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-size: large");
		lblKarma.setStyle("-fx-text-fill: textcolor-highlight-primary");
		lblPoints2.setStyle("-fx-text-fill: textcolor-highlight-primary");
		lblPoints3.setStyle("-fx-text-fill: textcolor-highlight-primary");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setVgap(5);
		setHgap(5);
		
		Label heaPoints = new Label(UI.getString("label.points"));
		heaKarma  = new Label(UI.getString("label.karma"));
		
		add(heaPoints, 0,0);
		add(lblPoints, 1,0);
		if (showPoints2) {
			add(heaPoints2 , 0,1);
			add(lblPoints2 , 1,1);
		}
		if (showPoints3) {
			add(heaPoints3 , 0,2);
			add(lblPoints3 , 1,2);
		}
		add(heaKarma , 0,3);
		add(lblKarma , 1,3);
	}
	
	//-------------------------------------------------------------------
	public void setKarma(int karma) {
		lblKarma.setText( String.valueOf(karma));
	}
	
	//-------------------------------------------------------------------
	public void setPoints(int value) {
		if (value!=Integer.MAX_VALUE)
			lblPoints.setText( String.valueOf(value));
		else
			lblPoints.setText("");
	}
	
	//-------------------------------------------------------------------
	public void setPoints(float value) {
		lblPoints.setText( String.format("%.2f", value));
	}
	
	//-------------------------------------------------------------------
	public void setPoints2(int value) {
		if (value>=0)
			lblPoints2.setText( String.valueOf(value));
		else
			lblPoints2.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void setPoints2(float value) {
		if (value>=0)
			lblPoints2.setText( String.format("%.2f", value));
		else
			lblPoints2.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void setPoints2Name(String value) {
		heaPoints2.setText(value);
	}
	
	//-------------------------------------------------------------------
	public void setPoints3(int value) {
		if (value>=0)
			lblPoints3.setText( String.valueOf(value));
		else
			lblPoints3.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void setPoints3(float value) {
		if (value>=0)
			lblPoints3.setText( String.format("%.2f", value));
		else
			lblPoints3.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void setPoints3Name(String value) {
		heaPoints3.setText(value);
	}
	
//	//-------------------------------------------------------------------
//	public void setKarmaName(String value) {
//		heaKarma.setText(value);
//	}

	//-------------------------------------------------------------------
	public void setExtra(Node extra) {
		if (this.extra!=null)
			getChildren().remove(this.extra);
		this.extra = extra;
		getChildren().remove(lblPoints2);
		if (extra==null) {
			add(lblPoints2 , 1,1);
		} else
			add(extra, 1,1);
	}

}
