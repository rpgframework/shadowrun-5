package org.prelle.shadowrun5.jfx.attributes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class AttributeField extends HBox {
	public Button dec;
	public Button inc;
	private TextField value;

	public AttributeField() {
		dec  = new Button("<");
		inc  = new Button(">");
		dec.getStyleClass().add("dark-background");
		inc.getStyleClass().add("dark-background");
		value = new TextField();
		value.setPrefColumnCount(1);
		this.getChildren().addAll(dec, value, inc);
	}
	public void setText(String val) {
		this.value.setText(val);
	}

	public void setOnDecAction(EventHandler<ActionEvent> h) {
		dec.setOnAction(h);
	}

	public void setOnIncAction(EventHandler<ActionEvent> h) {
		inc.setOnAction(h);
	}

	public void setDisableDec(boolean disable) {
		dec.setDisable(disable);
	}

	public void setDisableInc(boolean disable) {
		inc.setDisable(disable);
	}

}