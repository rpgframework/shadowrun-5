/**
 * 
 */
package org.prelle.shadowrun5.jfx.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ShadowrunJFXUtils;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributePanePrimary extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);
	
	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private AttributeController control;
	private ShadowrunCharacter    model;
	private CharGenMode           mode;

	private Label headAttr, headPoints, headMod, headValue, headCost;
	private Map<Attribute, Label> modification;
	private Map<Attribute, AttributeField> distributed;
	private Map<Attribute, Label> finalValue;
	private Map<Attribute, Label> costValue;

	private Attribute[] toShow = new Attribute[]{Attribute.BODY,Attribute.AGILITY,Attribute.REACTION,Attribute.STRENGTH, Attribute.WILLPOWER,Attribute.LOGIC,Attribute.INTUITION,Attribute.CHARISMA,Attribute.EDGE};
	
	//--------------------------------------------------------------------
	public AttributePanePrimary(AttributeController cntrl, CharGenMode mode) {
		this.control = cntrl;
		this.mode    = mode;
		if (cntrl==null) throw new NullPointerException("Control not set");
		if (mode ==null) throw new NullPointerException("Mode not set");

		modification = new HashMap<Attribute, Label>();
		distributed  = new HashMap<Attribute, AttributeField>();
		finalValue   = new HashMap<Attribute, Label>();
		costValue    = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
		doInteractivity();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setVgap(2);
		super.setMaxWidth(Double.MAX_VALUE);
		
		headAttr   = new Label(UI.getString("label.attributes"));
		headPoints = new Label(UI.getString("label.points"));
		headMod    = new Label(UI.getString("label.modified.short"));
		headValue  = new Label(UI.getString("label.value"));
		headCost   = new Label(UI.getString("label.cost"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headMod.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headCost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headPoints.getStyleClass().add("table-heading");
		headMod.getStyleClass().add("table-heading");
		headValue.getStyleClass().add("table-heading");
		headCost.getStyleClass().add("table-heading");
		
		headPoints.setAlignment(Pos.CENTER);
		headValue.setAlignment(Pos.CENTER);
		GridPane.setVgrow(headAttr, Priority.NEVER);
		
		for (final Attribute attr : toShow) {
			AttributeField value = new AttributeField();
			Label modVal= new Label();
			Label finVal= new Label();
			finVal.getStyleClass().add("result");
			
			finalValue  .put(attr, finVal);
			distributed .put(attr, value);
			modification.put(attr, modVal);
			costValue   .put(attr, new Label());
		}
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		setVgap(10);
		switch (mode) {
		case CREATING:
			this.add(headAttr  , 0,0);
			this.add(headPoints, 1,0);
			this.add(headMod   , 2,0);
			this.add(headValue , 3,0);
			break;
		case LEVELING:
			this.add(headAttr  , 0,0);
			this.add(headPoints, 1,0);
			this.add(headMod   , 2,0);
			this.add(headValue , 3,0);
			this.add(headCost  , 4,0);
			break;
		}
		
		int y=0;
		for (final Attribute attr : toShow) {
			y++;
			Label longName  = new Label(attr.getName());
			Label modVal    = modification.get(attr);
			AttributeField points = distributed.get(attr);
			Label finVal    = finalValue.get(attr);
			Label cost      = costValue.get(attr);
			
			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName .getStyleClass().add(lineStyle);
			modVal.getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			finVal.getStyleClass().add(lineStyle);
			cost  .getStyleClass().add(lineStyle);
			
			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			modVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			cost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			modVal.setAlignment(Pos.CENTER);
			
			this.add(longName , 0, y);
			switch (mode) {
			case CREATING:
				this.add( distributed.get(attr), 1, y);
				this.add(modification.get(attr), 2, y);
				this.add(  finalValue.get(attr), 3, y);
				break;
			case LEVELING:
				this.add( distributed.get(attr), 1, y);
				this.add(modification.get(attr), 2, y);
				this.add(  finalValue.get(attr), 3, y);
				this.add(   costValue.get(attr), 4, y);
				break;
			}
			
			GridPane.setMargin(points, new Insets(0,10,0,10));
		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
		for (final Attribute attr : toShow) {
			AttributeField field = distributed.get(attr);
			field.inc.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.increase(attr);
				}
			});
			field.dec.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.decrease(attr);
				}
			});
		}
	}

	//-------------------------------------------------------------------
	public void updateContent() {
		for (Attribute attr : toShow) {
			Label modif_l = modification.get(attr);
			AttributeField field = distributed.get(attr);
			Label cost_l  = costValue.get(attr);
			Label final_l = finalValue.get(attr);
			
			AttributeValue data = model.getAttribute(attr);
			
			if (data.getModifier()==0) {
				modif_l.setTooltip(null);
			} else {
				modif_l.setTooltip(new Tooltip(ShadowrunJFXUtils.getModificationTooltip(data)));
			}
			
			String modString = (data.getModifier()==0)?"":String.valueOf(data.getModifier());
					
			switch (mode) {
			case CREATING:
				field.setText(Integer.toString(data.getPoints()));
				modif_l.setText(String.valueOf(data.getModifier()));
				final_l.setText(String.valueOf(data.getModifiedValue()));
				break;
			case LEVELING:
				field.setText(Integer.toString(data.getPoints()));
				modif_l.setText(String.valueOf(data.getModifier()));
				final_l.setText(String.valueOf(data.getModifiedValue()));
				break;
			}
//			start_l.setText(Integer.toString(data.getStart()));
			modif_l.setText(modString);
			cost_l .setText(Integer.toString(control.getIncreaseCost(attr)));
			field.inc.setDisable(!control.canBeIncreased(attr));
			field.dec.setDisable(!control.canBeDecreased(attr));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.ATTRIBUTE_CHANGED) {
			Attribute      attribute = (Attribute) event.getKey();
			if (!attribute.isPrimary())
				return;
			if (logger.isTraceEnabled())
				logger.trace("handle "+event);
			AttributeValue set       = (AttributeValue) event.getValue();
			Label modVal = this.modification.get(attribute);
			Label finV_l = finalValue.get(attribute);
			
			if (attribute.isPrimary()) {
				// Update increase and decrease buttons
				AttributeField field     = distributed.get(attribute);
				field.inc.setDisable(!control.canBeIncreased(attribute));
				field.dec.setDisable(!control.canBeDecreased(attribute));
				if (field !=null) field .setText(Integer.toString(set.getModifiedValue()));
			}
			// Update values
			if (modVal!=null) { 
				modVal.setText(Integer.toString(set.getModifier()));
			} else {
				logger.warn("No label for modifier "+attribute+" in "+modification.keySet());
			}
			if (finV_l!=null) finV_l.setText(Integer.toString(set.getModifiedValue()));
			updateContent();
		} else if (event.getType()==GenerationEventType.POINTS_LEFT_ATTRIBUTES) {
			if (logger.isTraceEnabled())
				logger.trace("handle "+event);
			updateContent();
		} else if (event.getType()==GenerationEventType.EXPERIENCE_CHANGED || event.getType()==GenerationEventType.CHARACTER_CHANGED) {
			if (logger.isTraceEnabled())
				logger.trace("handle "+event);
			updateContent();
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
		doInteractivity();
		
		GenerationEventDispatcher.addListener(this);	
	}

}
