/**
 * 
 */
package org.prelle.shadowrun5.jfx.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.charctrl.AttributeController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * @author prelle
 *
 */
public class AttributePanePrimary2 extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private AttributeController control;
	private ShadowrunCharacter    model;

	private Label headAttr, headPoints, headMax, headCost;
	private Map<Attribute, AttributeField> distributed;
	private Map<Attribute, Label> maxValue;
	private Map<Attribute, Label> costValue;

	private Attribute[] toShow = new Attribute[]{Attribute.BODY,Attribute.AGILITY,Attribute.REACTION,Attribute.STRENGTH, Attribute.WILLPOWER,Attribute.LOGIC,Attribute.INTUITION,Attribute.CHARISMA,Attribute.EDGE};
	
	//--------------------------------------------------------------------
	public AttributePanePrimary2(AttributeController cntrl) {
		this.control = cntrl;
		if (cntrl==null) throw new NullPointerException("Control not set");

		distributed  = new HashMap<Attribute, AttributeField>();
		maxValue   = new HashMap<Attribute, Label>();
		costValue    = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
		doInteractivity();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setVgap(2);
		super.setMaxWidth(Double.MAX_VALUE);
		
		headAttr   = new Label(UI.getString("label.attributes"));
		headPoints = new Label(UI.getString("label.points"));
		headMax    = new Label(UI.getString("label.maximum"));
		headCost   = new Label(UI.getString("label.cost"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headMax.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headCost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-heading");
		headPoints.getStyleClass().add("table-heading");
		headMax.getStyleClass().add("table-heading");
		headCost.getStyleClass().add("table-heading");
		
		headPoints.setAlignment(Pos.CENTER);
		GridPane.setVgrow(headAttr, Priority.NEVER);
		
		for (final Attribute attr : toShow) {
			AttributeField value = new AttributeField();
			Label maxVal= new Label();
			maxVal.getStyleClass().add("result");
			
			maxValue  .put(attr, maxVal);
			distributed .put(attr, value);
			costValue   .put(attr, new Label());
		}
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		setVgap(10);
			this.add(headAttr  , 0,0);
			this.add(headPoints, 1,0);
			this.add(headMax   , 2,0);
			this.add(headCost  , 3,0);
		
		int y=0;
		for (final Attribute attr : toShow) {
			y++;
			Label longName  = new Label(attr.getName());
			AttributeField points = distributed.get(attr);
			Label maxVal    = maxValue.get(attr);
			Label cost      = costValue.get(attr);
			
			String lineStyle = ((y%2)==1)?"even":"odd";
			GridPane.setVgrow(longName, Priority.ALWAYS);
			longName .getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			maxVal.getStyleClass().add(lineStyle);
			cost  .getStyleClass().add(lineStyle);
			
			longName.setUserData(attr);

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			maxVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			cost.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(longName , 0, y);
			this.add(distributed.get(attr), 1, y);
			this.add(   maxValue.get(attr), 2, y);
			this.add(  costValue.get(attr), 3, y);
			
			GridPane.setMargin(points, new Insets(0,10,0,10));
		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
		for (final Attribute attr : toShow) {
			AttributeField field = distributed.get(attr);
			field.inc.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.increase(attr);
				}
			});
			field.dec.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.decrease(attr);
				}
			});
		}
	}

	//-------------------------------------------------------------------
	public void updateContent() {
		for (Attribute attr : toShow) {
			AttributeField field = distributed.get(attr);
			Label cost_l  = costValue.get(attr);
			Label max_l = maxValue.get(attr);
			
			AttributeValue data = model.getAttribute(attr);
			logger.debug("data = "+data);
					
				field.setText(Integer.toString(data.getModifiedValue()));
				max_l.setText(String.valueOf(data.getMaximum()));
//			start_l.setText(Integer.toString(data.getStart()));
			cost_l .setText(Integer.toString(control.getIncreaseCost(attr)));
			field.inc.setDisable(!control.canBeIncreased(attr));
			field.dec.setDisable(!control.canBeDecreased(attr));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.ATTRIBUTE_CHANGED) {
			Attribute      attribute = (Attribute) event.getKey();
			if (!attribute.isPrimary())
				return;
			if (logger.isTraceEnabled())
				logger.trace("handle "+event);
			AttributeValue set       = (AttributeValue) event.getValue();
			Label finV_l = maxValue.get(attribute);
			
			if (attribute.isPrimary()) {
				// Update increase and decrease buttons
				AttributeField field     = distributed.get(attribute);
				field.inc.setDisable(!control.canBeIncreased(attribute));
				field.dec.setDisable(!control.canBeDecreased(attribute));
				if (field !=null) field .setText(Integer.toString(set.getModifiedValue()));
			}
			if (finV_l!=null) finV_l.setText(Integer.toString(set.getModifiedValue()));
			updateContent();
		} else if (event.getType()==GenerationEventType.CHARACTER_CHANGED) {
			updateContent();
		} else if (event.getType()==GenerationEventType.POINTS_LEFT_ATTRIBUTES) {
			logger.trace("RCV "+event);
			updateContent();
		} else if (event.getType()==GenerationEventType.EXPERIENCE_CHANGED) {
			logger.trace("RCV "+event);
			updateContent();
		}
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		updateContent();
		doInteractivity();
		
		GenerationEventDispatcher.addListener(this);	
	}

}
