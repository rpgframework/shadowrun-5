/**
 *
 */
package org.prelle.shadowrun5.jfx.cforms;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.ComplexFormController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class ComplexFormSelectionPane extends ThreeColumnPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ComplexFormController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<ComplexForm> lvAvailable;
	private ListView<ComplexFormValue> lvSelected;
	private Label descHeading;
	private Label descRef;
	private Label descRange, descDur, descDrain;
	private Label description;

	//-------------------------------------------------------------------
	/**
	 */
	public ComplexFormSelectionPane(ComplexFormController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;
		if (ctrl==null)
			throw new NullPointerException();

		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setColumn1Header(UI.getString("label.available"));
		setColumn2Header(UI.getString("label.selected"));
		setColumn3Header(UI.getString("label.description"));

		lvAvailable = new ListView<ComplexForm>();
		lvAvailable.setCellFactory(new Callback<ListView<ComplexForm>, ListCell<ComplexForm>>() {
			public ListCell<ComplexForm> call(ListView<ComplexForm> param) {
				ComplexFormListCell cell =  new ComplexFormListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n));
		lvSelected  = new ListView<ComplexFormValue>();
		lvSelected.setCellFactory(new Callback<ListView<ComplexFormValue>, ListCell<ComplexFormValue>>() {
			public ListCell<ComplexFormValue> call(ListView<ComplexFormValue> param) {
				ComplexFormValueListCell cell =  new ComplexFormValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				describe(n.getModifyable());
		});

		Label phAvailable = new Label(UI.getString("complexformselectpane.placeholder.available"));
		Label phSelected  = new Label(UI.getString("complexformselectpane.placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);

		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descRange   = new Label();
		descDur     = new Label();
		descDrain   = new Label();

		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");

	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblRange = new Label(UI.getString("label.target")+":");
		Label lblDur   = new Label(UI.getString("label.duration")+":");
		Label lblDrain = new Label(UI.getString("label.fading")+":");
		lblRange.setStyle("-fx-font-weight: bold");
		lblDur.setStyle("-fx-font-weight: bold");
		lblDrain.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblRange, 2, 0);
		descGrid.add(descRange, 3, 0);
		descGrid.add( lblDur  , 0, 1);
		descGrid.add(descDur  , 1, 1);
		descGrid.add( lblDrain, 2, 1);
		descGrid.add(descDrain, 3, 1);
		descGrid.setHgap(5);

		VBox column1 = new VBox(10);
		column1.getChildren().addAll(lvAvailable);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);

		VBox column3 = new VBox(10);
		column3.getChildren().addAll(descHeading, descRef, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);

		setColumn1Node(column1);
		setColumn2Node(lvSelected);
		setColumn3Node(column3);
		setHeadersVisible(true);

		HBox.setHgrow(column1.getParent(), Priority.SOMETIMES);
		HBox.setHgrow(lvSelected.getParent(), Priority.SOMETIMES);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvSelected.setOnDragDropped(event -> dragFromCFormDropped(event));
		lvSelected.setOnDragOver(event -> dragFromCFormOver(event));
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableComplexForms());

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getComplexForms());
	}

	//--------------------------------------------------------------------
	private void describe(ComplexForm data) {
		if (data==null)
			return;
		descHeading.setText(data.getName().toUpperCase());
		descRef.setText(data.getProductName()+" "+data.getPage());
		descDur .setText(data.getDuration().getName());
		descRange.setText(data.getTarget().getName());
		if (data.getFading()<0)
			descDrain.setText(UI.getString("label.cflevel")+" "+data.getFading());
		else if (data.getFading()>0)
			descDrain.setText(UI.getString("label.cflevel")+" +"+data.getFading());
		else
			descDrain.setText(UI.getString("label.cflevel"));

		description.setText(data.getHelpText());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case COMPLEX_FORMS_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	private void dragFromCFormDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("complexform")) {
					ComplexForm data = ShadowrunCore.getComplexForm(tail);
					if (data==null) {
						logger.warn("Cannot find complex form for dropped id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						// Special handling for knowledge and language skills
//						if (data.getType()==SkillType.KNOWLEDGE || data.getType()==SkillType.LANGUAGE) {
//							logger.debug(data.getName()+" must be entered by user");
//							String name = askForSkillName();
//							if (name!=null) {
//								ctrl.selectKnowledgeOrLanguage(data, name);
//							}
//						} else {
							ctrl.select(data);
//						}
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromCFormOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

}
