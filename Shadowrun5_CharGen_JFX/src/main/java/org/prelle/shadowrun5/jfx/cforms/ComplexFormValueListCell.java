package org.prelle.shadowrun5.jfx.cforms;

import java.util.ResourceBundle;

import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ComplexFormValue;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class ComplexFormValueListCell extends ListCell<ComplexFormValue> {

	private final static ResourceBundle CORE = ShadowrunCore.getI18nResources();

	private Label lblName;
	private Label lblLine1;
	private StackPane stack;
	private ImageView imgRecommended;

	private ComplexFormValue data;

	//-------------------------------------------------------------------
	public ComplexFormValueListCell() {
		initComponents();
		initLayout();
		initStyle();

		imgRecommended.setVisible(false);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		imgRecommended = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		stack    = new StackPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox layout   = new VBox(3);
		layout.getChildren().addAll(lblName, lblLine1);

		// Recommended icon
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack.getChildren().addAll(layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		lblName.setStyle("-fx-font-weight: bold;");
		setStyle("-fx-pref-width: 16em");
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ComplexFormValue item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(stack);
			ComplexForm spell = item.getModifyable();
			lblName.setText(spell.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(makeFeatureString(spell));
		}
	}

	//-------------------------------------------------------------------
	private static String makeFeatureString(ComplexForm spell) {
		StringBuffer buf = new StringBuffer();
		buf.append(CORE.getString("label.complexform.duration")+": "+spell.getDuration().getShortName());
		buf.append("  \t"+CORE.getString("label.complexform.target")+": "+spell.getTarget().getShortName());
		return buf.toString();
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("complexformval:"+data.getModifyable().getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}
}