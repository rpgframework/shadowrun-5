/**
 *
 */
package org.prelle.shadowrun5.jfx.develop;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.rpgframework.jfx.DevelopmentPage;
import org.prelle.shadowrun5.HistoryElementImpl;
import org.prelle.shadowrun5.RewardImpl;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;
import de.rpgframework.products.ProductServiceLoader;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class DevelopmentScreenSR5 extends DevelopmentPage {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	/**
	 */
	public DevelopmentScreenSR5() {
		super(UI, RoleplayingSystem.SHADOWRUN);
		setConverter(new StringConverter<Modification>() {
			public String toString(Modification value) {
				logger.debug("Convert "+value);
				return ShadowrunTools.getModificationString(value);
			}
			public Modification fromString(String arg0) { return null;}
		});
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
//		getItems().clear();
//		getItems().addAll(ShadowrunTools.convertToHistoryElementList(model, super.shallBeAggregated()));
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
//		setTitle(model.getName()+" / "+UI.getString("label.development"));

//		history.getItems().clear();
//		history.getItems().addAll(SplitterTools.convertToHistoryElementList(model));
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentPage#openAdd()
	 */
	@Override
	public HistoryElement openAdd() {
		RewardBox dialog = new RewardBox();

		ManagedDialog screen = new ManagedDialog(UI.getString("dialog.reward.title"), dialog, CloseType.OK, CloseType.CANCEL);
		screen.setButtonPredicateCheck( closeType -> {
			logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
			if (closeType==CloseType.OK) {
				return dialog.hasEnoughData();
			}
			return true;
		});
		screen.setOnAction(CloseType.OK, event -> getManager().close(screen, CloseType.OK));
		screen.setOnAction(CloseType.CANCEL, event -> getManager().close(screen, CloseType.CANCEL));
//		skin.setDisabled(CloseType.OK, true);
//		dialog.enoughDataProperty().addListener( (ov,o,n) -> {
//			skin.setDisabled(CloseType.OK, !n);
//		});
		CloseType closed = (CloseType)getManager().showAndWait(screen);

		if (closed==CloseType.OK) {
			RewardImpl reward = dialog.getDataAsReward();
			logger.debug("Add reward "+reward);
			ShadowrunTools.reward(model, reward);
			HistoryElementImpl elem = new HistoryElementImpl();
			elem.setName(reward.getTitle());
			elem.addGained(reward);
			if (reward.getId()!=null)
				elem.setAdventure(ProductServiceLoader.getInstance().getAdventure(RoleplayingSystem.SHADOWRUN, reward.getId()));
			logger.info("Fire EXP change");
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, new int[]{model.getKarmaFree(), model.getKarmaInvested()}));
			return elem;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentPage#openEdit(de.rpgframework.genericrpg.HistoryElement)
	 */
	@Override
	public boolean openEdit(HistoryElement elem) {
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getManager().showAlertAndCall(
					AlertType.ERROR,
					UI.getString("error.not-possible"),
					UI.getString("error.only-single-rewards-editable"));
			return false;
		}

		Reward toEdit = elem.getGained().get(0);
		RewardBox dialog = new RewardBox(toEdit);

		ManagedDialog screen = new ManagedDialog(UI.getString("dialog.reward.title"), dialog, CloseType.OK, CloseType.CANCEL);
		screen.setButtonPredicateCheck( closeType -> {
			logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
			if (closeType==CloseType.OK) {
				return dialog.hasEnoughData();
			}
			return true;
		});
		screen.setOnAction(CloseType.OK, event -> getManager().close(screen, CloseType.OK));
		screen.setOnAction(CloseType.CANCEL, event -> getManager().close(screen, CloseType.CANCEL));
		CloseType closed = (CloseType)getManager().showAndWait(screen);

		if (closed==CloseType.OK) {
			RewardImpl reward = dialog.getDataAsReward();
			logger.debug("Copy edited data: ORIG = "+toEdit);
			toEdit.setTitle(reward.getTitle());
			toEdit.setId(reward.getId());
			toEdit.setDate(reward.getDate());
			// Was single reward. Changes in reward title, change element title
			((HistoryElementImpl)elem).setName(reward.getTitle());
			ProductService sessServ = ProductServiceLoader.getInstance();
			if (reward.getId()!=null)  {
				Adventure adv = sessServ.getAdventure(RoleplayingSystem.SHADOWRUN, reward.getId());
				if (adv==null) {
					logger.warn("Reference o an unknown adventure: "+reward.getId());
				} else
					((HistoryElementImpl)elem).setAdventure(adv);

			}
//			((HistoryElementImpl)elem).set(reward.getTitle());
			logger.debug("Copy edited data: NEW  = "+toEdit);
			logger.debug("Element now   = "+elem);
			return true;
		}
		return false;
	}

}