/**
 *
 */
package org.prelle.shadowrun5.jfx.fluent;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.rpgframework.jfx.NumericalValueTableCell;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.jfx.ViewMode;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;

/**
 * @author Stefan
 *
 */
public class AttributeTable extends TableView<AttributeValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AttributeTable.class.getName());

	private CharacterController ctrl;

	private TableColumn<AttributeValue, Boolean> recCol;
	private TableColumn<AttributeValue, String> nameCol;
	private TableColumn<AttributeValue, AttributeValue> valCol;
	private TableColumn<AttributeValue, Number> modCol;
	private TableColumn<AttributeValue, Number> sumCol;

	//--------------------------------------------------------------------
	public AttributeTable(CharacterController ctrl, ViewMode mode) {
		if (ctrl==null)
			throw new NullPointerException();
		this.ctrl = ctrl;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();

		for (Attribute key : Attribute.primaryTableValues()) {
			getItems().add(ctrl.getCharacter().getAttribute(key));
		}

	}

	//--------------------------------------------------------------------
	private void initColumns() {
		recCol = new TableColumn<>();
		nameCol = new TableColumn<>(RES.getString("column.name"));
		valCol  = new TableColumn<>(RES.getString("column.value"));
		modCol  = new TableColumn<>(RES.getString("column.mod"));
		sumCol  = new TableColumn<>(RES.getString("column.sum"));

		recCol .setId("attrtable-rec");
		nameCol.setId("attrtable-name");
		valCol .setId("attrtable-val");
		modCol .setId("attrtable-mod");
		sumCol .setId("attrtable-sum");

		valCol.setStyle("-fx-alignment: center");
		modCol.setStyle("-fx-alignment: center");
		sumCol.setStyle("-fx-alignment: center");

		recCol.setPrefWidth(40);
		nameCol.setMinWidth(120);
		valCol.setPrefWidth(110);
		modCol.setMinWidth(40);
		sumCol.setMinWidth(50);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initLayout() {
		getColumns().addAll(recCol, nameCol, valCol, modCol, sumCol);
	}

	//--------------------------------------------------------------------
	private void initValueFactories() {
//		recCol.setCellValueFactory(cdf -> new SimpleBooleanProperty(ctrl.getSkillController().isConceptSkill(cdf.getValue().getModifyable())));
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		valCol .setCellValueFactory(cdf -> new SimpleObjectProperty<AttributeValue>(cdf.getValue()));
		modCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getVolatileModifier()));
		sumCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getModifiedValue()));
	}

	//--------------------------------------------------------------------
	private void initCellFactories() {
		valCol.setCellFactory( (col) -> new NumericalValueTableCell<Attribute,AttributeValue,AttributeValue>(ctrl.getAttributeController()));
		recCol.setCellFactory(col -> new TableCell<AttributeValue,Boolean>(){
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					if (item) {
						Label lb = new Label("\uE735");
						lb.setStyle("-fx-text-fill: recommendation; -fx-font-family: 'Segoe MDL2 Assets';");
						setGraphic(lb);
					} else
						setGraphic(null);
				}
			}
		});
		modCol.setCellFactory( col -> new TableCell<AttributeValue,Number>() {
			public void updateItem(Number item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || ((Integer)item)==0) {
					setGraphic(null);
				} else {
					Label lb = new Label(String.valueOf(item));
					lb.setStyle("-fx-font-weight: bold;");
					Tooltip tt = new Tooltip(getTableRow().getItem().getVolatileModificationString());
					lb.setTooltip(tt);
					setGraphic(lb);
				}
			}
		});
	}
}
