package org.prelle.shadowrun5.jfx.fluent;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5DevelopmentPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5EquipmentPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5MagicPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5MatrixPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5OverviewPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5ResonancePage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5SINLifestylePage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5VehiclePage;
import org.prelle.shadowrun5.gen.CharacterGenerator;
import org.prelle.shadowrun5.gen.CommonSR5CharacterGenerator;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.CharGenWizardNG;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ViewMode;
import org.prelle.shadowrun5.levelling.CharacterLeveller;

import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterHandle.Format;
import de.rpgframework.character.CharacterHandle.Type;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import javafx.scene.control.MenuItem;

/**
 * @author Stefan Prelle
 *
 */
public class CharacterViewScreenSR5Fluent extends ManagedScreen implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CharacterViewScreenSR5Fluent.class.getName());

	private final static String CSS_COMMON = "css/shadowrun-common.css";
	private final static String CSS = "css/shadowrun-light.css";

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;

	private SR5OverviewPage  pgOverview;
	private SR5MagicPage     pgMagic;
	private SR5MatrixPage    pgMatrix;
	private SR5ResonancePage  pgResonance;
	private SR5EquipmentPage  pgEquipment;
	private SR5VehiclePage   pgVehicles;
	private SR5SINLifestylePage pgLiving;
	private SR5DevelopmentPage  pgDevelop;

	private MenuItem navOverview;
	private MenuItem navMagic;
	private MenuItem navResonance;
	private MenuItem navEquipment;
	private MenuItem navMatrix;
	private MenuItem navVehicles;
	private MenuItem navLiving;
	private MenuItem navExperience;

	//-------------------------------------------------------------------
	public CharacterViewScreenSR5Fluent(CharacterController control, ViewMode mode, CharacterHandle handle) {
		this.setId("genesis/shadowrun");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		model = control.getCharacter();

		initComponents();
		initLayout();
		initNavigation();
		initInteractivity();

		refresh();
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#getStyleSheets()
	 */
	@Override
	public String[] getStyleSheets() {
		return new String[] {
				SR5Constants.class.getResource(CSS_COMMON).toExternalForm(), 
				SR5Constants.class.getResource(CSS).toExternalForm()};
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		pgOverview  = new SR5OverviewPage(control, mode, handle, this);
		pgMagic     = new SR5MagicPage(control, mode, handle, this);
		pgResonance = new SR5ResonancePage(control, mode, handle, this);
		pgMatrix    = new SR5MatrixPage(control, mode, handle, this);
		pgEquipment = new SR5EquipmentPage(control, mode, handle, this);
		pgVehicles  = new SR5VehiclePage(control, mode, handle, this);
		pgLiving    = new SR5SINLifestylePage(control, handle, this);
		pgDevelop   = new SR5DevelopmentPage(control, handle, this);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.setLandingPage(pgOverview);
	}

	//-------------------------------------------------------------------
	private void initNavigation() {
		navEquipment  = new MenuItem(UI.getString("navItem.equipment"), new SymbolIcon("ShoppingCart"));
		navLiving     = new MenuItem(UI.getString("navItem.living"), new SymbolIcon("Home"));
		navMagic      = new MenuItem(UI.getString("navItem.magic"), new SymbolIcon("Mage"));
		navMatrix     = new MenuItem(UI.getString("navItem.matrix"), new SymbolIcon("TiltDown"));
		navOverview   = new MenuItem(UI.getString("navItem.overview"), new SymbolIcon("RedEye"));
		navResonance  = new MenuItem(UI.getString("navItem.resonance"), new SymbolIcon("Robot"));
		navVehicles   = new MenuItem(UI.getString("navItem.vehicles"), new SymbolIcon("Car"));
		navExperience = new MenuItem(UI.getString("navItem.experience"), new SymbolIcon("Footprints"));

		this.getNavigationItems().addAll(navOverview, navMagic, navResonance, navEquipment, navMatrix, navVehicles, navLiving, navExperience);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		setCanBeLeftCallback( screen -> userTriesToLeave());
	}

	//-------------------------------------------------------------------
	private boolean saveCharacter() {
		logger.debug("START: saveCharacter");

		if (mode==ViewMode.MODIFICATION) {
			/*
			 * Write all made modifications to character
			 */
			logger.debug("Add modifications to character log");
			((CharacterLeveller)control).updateHistory();
		}

		try {
			/*
			 * 1. Convert character into Byte Buffer - or fail
			 */
			byte[] encoded = null;
			try { encoded = ShadowrunCore.save(model); } catch (IOException e) {
				logger.error("Cannot save character, since encoding failed: "+e);
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				getManager().showAlertAndCall(
						AlertType.ERROR,
						UI.getString("error.encoding.title"),
						UI.getString("error.encoding.content")+"\n"+out
						);
				return false;
			}

			/*
			 * 2. Use character service to save character
			 */
			try {
				if (handle==null) {
					logger.debug("CharacterHandle does not exist yet - prepare it");
					handle = CharacterProviderLoader.getCharacterProvider().createCharacter(model.getName(), RoleplayingSystem.SHADOWRUN);
				}
				logger.info("Save character "+model.getName());
				CharacterProviderLoader.getCharacterProvider().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, null, encoded);
				logger.info("Saved character "+model.getName()+" successfully");
			} catch (IOException e) {
				logger.error("Failed saving character",e);
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				getManager().showAlertAndCall(
						AlertType.ERROR,
						UI.getString("error.saving_character.title"),
						UI.getString("error.saving_character.message")+"\n"+out
						);
				return false;
			}

			// 3. Eventually rename
			try {
				if (handle!=null && !handle.getName().equals(model.getName())) {
					logger.info("Character has been renamed");
					CharacterProviderLoader.getCharacterProvider().renameCharacter(handle, model.getName());
				}
			} catch (IOException e) {
				logger.error("Renaming failed",e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Renaming failed: "+e);
			}

			/*
			 * 4. Update portrait
			 */
			logger.debug("Update portrait");
			CharacterProvider charServ = CharacterProviderLoader.getCharacterProvider();
			try {
				if (model.getImage()!=null && handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Update character image");
						attach.setData(model.getImage());
						charServ.modifyAttachment(handle, attach);
					} else {
						charServ.addAttachment(handle, Type.CHARACTER, Format.IMAGE, null, model.getImage());
					}
				} else if (handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Delete old character image");
						charServ.removeAttachment(handle, attach);
					}
				}
			} catch (IOException e) {
				logger.error("Failed modifying portrait attachment",e);
			}
		} finally {
			logger.debug("STOP : saveCharacter");
		}
		return true;
	}

	//-------------------------------------------------------------------
	private boolean userTriesToLeave() {
		logger.info("userTriesToLeave");
		
		if (mode==ViewMode.GENERATION) {
			logger.warn("TODO: Check if creation is finished");
			if ( ((CommonSR5CharacterGenerator)control).hasEnoughData() ) {
				logger.info("User wants to leave and generator is finished - try to save character");
				return saveCharacter();
			} else {
				logger.info("User wants to leave the generation early.");
				CloseType result = getManager().showAlertAndCall(
						AlertType.CONFIRMATION,
						UI.getString("alert.cancel_creation.title"),
						UI.getString("alert.cancel_creation.message")
						);
				return result==CloseType.YES;
			}
		} else {
			logger.info("User wants to leave character modifiction");
			CloseType result = getManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					UI.getString("alert.save_character.title"),
					UI.getString("alert.save_character.message")
					);
			if (result==CloseType.YES) {
				logger.debug("User confirmed saving character");
				saveCharacter();
			} else if (result==CloseType.CANCEL) {
				logger.debug("User cancelled leaving");
				return false;
			} else {
				logger.debug("User denied saving character - reload it");
				try {
					if (handle!=null) {
						handle.setCharacter(null);
						handle.getCharacter();
					}
				} catch (IOException e) {
					logger.error("Failed reloading character",e);
					getManager().showAlertAndCall(AlertType.ERROR, "", UI.getString("alert.reloading.char"));
				}
			}
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	public void startGeneration(CharacterGenerator charGen) {
		// TODO Auto-generated method stub
		logger.warn("TODO: startGeneration: "+charGen);

		CharGenWizardNG wizard = new CharGenWizardNG(model);
		wizard.getStylesheets().addAll(
				SR5Constants.class.getResource(CSS_COMMON).toExternalForm(),
				SR5Constants.class.getResource(CSS).toExternalForm()
				);
		logger.warn("CharGenWizardNG.style= "+wizard.getStylesheets());
		CloseType close = (CloseType)getManager().showAndWait(wizard);
		logger.info("Closed with "+close);

		if (close==CloseType.FINISH) {
			logger.info("Wizard finished");
//			CoriolisCharacter model = charGen.getCharacter();
//			try {
//				byte[] data = CoriolisCore.save(model);
//				handle = RPGFrameworkLoader.getInstance().getCharacterService().createCharacter(model.getName(), RoleplayingSystem.CORIOLIS);
//				RPGFrameworkLoader.getInstance().getCharacterService().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, model.getName()+".xml", data);
//				manager.showAlertAndCall(AlertType.NOTIFICATION, UI.getString("alert.generation_finished.title"),
//						String.format(UI.getString("alert.generation_finished.message"), handle.getPath().toString()));
//			} catch (IOException e) {
//				logger.error("Failed writing newly created character to disk",e);
//				manager.showAlertAndCall(AlertType.ERROR, UI.getString("error.saving_character.title"),
//						String.format(UI.getString("error.saving_character.message"), e.toString()));
//			}
//			this.control = new CoriolisCharacterLeveller(model);
			refresh();
			
		} else {
			logger.warn("Wizard "+close);
			getScreenManager().closeScreen();
		}

	}

	//--------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh");

		pgOverview.refresh();
		pgMagic.refresh();
		pgResonance.refresh();
		pgEquipment.refresh();
		pgVehicles.refresh();
		pgLiving.refresh();
		pgDevelop.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#navigationItemChanged(javafx.scene.control.MenuItem, javafx.scene.control.MenuItem)
	 */
	@Override
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
		logger.info("Navigation changed to "+newValue);
		
		if (newValue==navOverview) {
			setContent(pgOverview);
		} else if (newValue==navMagic) {
			setContent(pgMagic);
		} else if (newValue==navResonance) {
			setContent(pgResonance);
		} else if (newValue==navMatrix) {
			setContent(pgMatrix);
		} else if (newValue==navEquipment) {
			setContent(pgEquipment);
		} else if (newValue==navVehicles) {
			setContent(pgVehicles);
		} else if (newValue==navLiving) {
			setContent(pgLiving);
		} else if (newValue==navExperience) {
			setContent(pgDevelop);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.debug("......"+value);
		pgOverview.setResponsiveMode(value);
		pgMagic.setResponsiveMode(value);
		pgResonance.setResponsiveMode(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		logger.debug("RCV "+event.getType());
		switch (event.getType()) {
		case CHARACTER_CHANGED:
		default:
			refresh();
			break;
		}
	}

}
