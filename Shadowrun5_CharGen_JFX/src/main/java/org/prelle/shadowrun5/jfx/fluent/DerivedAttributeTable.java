/**
 *
 */
package org.prelle.shadowrun5.jfx.fluent;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.charctrl.CharacterController;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;

/**
 * @author Stefan
 *
 */
public class DerivedAttributeTable extends TableView<AttributeValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(AttributeTable.class.getName());

	private CharacterController ctrl;

	private TableColumn<AttributeValue, String> nameCol;
	private TableColumn<AttributeValue, Number> sumCol;

	//--------------------------------------------------------------------
	public DerivedAttributeTable(CharacterController ctrl) {
		if (ctrl==null)
			throw new NullPointerException();
		this.ctrl = ctrl;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();

		for (Attribute key : Attribute.derivedTableValues()) {
			getItems().add(ctrl.getCharacter().getAttribute(key));
		}

	}

	//--------------------------------------------------------------------
	private void initColumns() {
		nameCol = new TableColumn<>(RES.getString("column.name"));
		sumCol  = new TableColumn<>(RES.getString("column.value"));

		nameCol.setId("attrtable-name");
		sumCol .setId("attrtable-sum");

		sumCol.setStyle("-fx-alignment: center");

		nameCol.setMinWidth(130);
		nameCol.setPrefWidth(160);
		sumCol.setMinWidth(40);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initLayout() {
		getColumns().addAll(nameCol, sumCol);
	}

	//--------------------------------------------------------------------
	private void initValueFactories() {
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		sumCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(cdf.getValue().getModifiedValue()));
	}

	//--------------------------------------------------------------------
	private void initCellFactories() {
		sumCol.setCellFactory( col -> new TableCell<AttributeValue,Number>() {
			public void updateItem(Number item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || ((Integer)item)==0) {
					setGraphic(null);
				} else {
					AttributeValue aVal = getTableRow().getItem();
					ShadowrunCharacter model = ctrl.getCharacter();
					String modString = "";
					
					Label lb = new Label(String.valueOf(item));
					switch (aVal.getAttribute()) {
					case INITIATIVE_PHYSICAL:
						lb.setText(ShadowrunTools.getInitiativeString(model, aVal.getAttribute()));
						modString = aVal.getVolatileModificationString();
						if (model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifier()>0)
							if (modString.isEmpty())
								modString=model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getVolatileModificationString();
							else
								modString+="\n"+model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getVolatileModificationString();
						break;
					case INITIATIVE_MATRIX:
						lb.setText(ShadowrunTools.getInitiativeString(model, aVal.getAttribute()));
						modString = aVal.getVolatileModificationString();
						if (model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getModifier()>0) {
							if (modString.isEmpty())
								modString=model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getVolatileModificationString();
							else
								modString+="\n"+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getVolatileModificationString();
						}
						break;
					case INITIATIVE_ASTRAL:
						lb.setText(ShadowrunTools.getInitiativeString(model, aVal.getAttribute()));
						modString = aVal.getVolatileModificationString();
						if (model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getModifier()>0) {
							if (modString.isEmpty())
								modString=model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getVolatileModificationString();
							else
								modString+="\n"+model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getVolatileModificationString();
						}
						break;
					default:
						modString = aVal.getVolatileModificationString();
					}

					if (modString!=null && modString.length()>2)
						lb.setStyle("-fx-font-weight: bold;");
					Tooltip tt = new Tooltip(modString);
					lb.setTooltip(tt);
					setGraphic(lb);
				}
			}
		});
	}

}
