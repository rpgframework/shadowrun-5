/**
 * 
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class AvailableSlotListCell extends ListCell<AvailableSlot> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	interface CarriedItemObtainer {
		public CarriedItem getCarriedItem();
	}
	
	interface RefreshableParent {
		public void refreshAsParent();
	}
	
	private EquipmentController control;
	private CarriedItemObtainer itemObtainer;
	private ScreenManagerProvider provider;
	private RefreshableParent parent;
	
	private Label lblSlotName;
	private Label lblContent;
	private Button btnEdit;
	private HBox  layout;
	
	private AvailableSlot data;

	//-------------------------------------------------------------------
	/**
	 */
	public AvailableSlotListCell(EquipmentController ctrl, CarriedItemObtainer item, ScreenManagerProvider provider, RefreshableParent parent) {
		this.control      = ctrl;
		this.itemObtainer = item;
		this.provider     = provider;
		this.parent       = parent;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblSlotName = new Label();
		lblSlotName.getStyleClass().add("text-subheader");
		lblContent  = new Label();
		lblContent.setWrapText(true);
		
		btnEdit = new Button(null, new FontIcon("\uE104"));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox col1 = new VBox(10);
		col1.getChildren().addAll(lblSlotName, lblContent);
//		lblSlotName.setPrefWidth(Double.MAX_VALUE);
//		lblContent.setPrefWidth(Double.MAX_VALUE);
		
		layout = new HBox(10);
		layout.getChildren().addAll(col1, btnEdit);
		HBox.setHgrow(col1, Priority.ALWAYS);
		
		setStyle("-fx-pref-height: 5em");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(event -> editClicked(event));
	}

	//-------------------------------------------------------------------
	public void updateItem(AvailableSlot item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;
		logger.debug("updateItem called for "+item+"  empty="+empty);
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			
			lblSlotName.setText(item.getSlot().getName());
			StringBuffer buf = new StringBuffer();
			Iterator<CarriedItem> it = item.getUserEmbeddedItems().iterator();
			for (CarriedItem tmp : item.getUserEmbeddedItems()) {
				String append = ItemUtilJFX.getShortItemModificationString(tmp.getModifications());
				if (append.isEmpty())
					buf.append(tmp.getName());
				else
					buf.append(tmp.getName()+"  ("+append+")");
				if (it.hasNext())
					buf.append("\n");
			}
			lblContent.setText(buf.toString());
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(ActionEvent event) {
		logger.debug("editClicked for slot "+data);
		
		List<ItemTemplate> items = control.getEmbeddableIn(itemObtainer.getCarriedItem(), data.getSlot());
		logger.debug("available for "+data.getSlot()+" = "+items);
		
		EmbedItemsPane pane = new EmbedItemsPane(control, itemObtainer.getCarriedItem(), data, provider);
		GenerationEventDispatcher.addListener(pane);
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, 
				String.format(UI.getString("embeditemsdialog.title"),data.getSlot().getName()), 
				pane);
		GenerationEventDispatcher.removeListener(pane);
		logger.debug("Embedding done");
		StringBuffer buf = new StringBuffer();
		Iterator<CarriedItem> it = data.getAllEmbeddedItems().iterator();
		for (CarriedItem tmp : data.getAllEmbeddedItems()) {
			String append = ItemUtilJFX.getShortItemModificationString(tmp.getModifications());
			if (append.isEmpty())
				buf.append(tmp.getName());
			else
				buf.append(tmp.getName()+"  ("+append+")");
			if (it.hasNext())
				buf.append("\n");
		}
		lblContent.setText(buf.toString());
		
		// Refresh parent
		if (parent!=null)
			parent.refreshAsParent();
	}

}
