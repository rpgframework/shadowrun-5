/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class BigCarriedItemListCell extends ListCell<CarriedItem> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private EquipmentController charGen;
	private ListView<CarriedItem> parent;
	private ScreenManagerProvider provider;

	private transient CarriedItem data;

	private Label lblName;
	private Label lblPrice2;
	private Label lblAccessories;
	private Label lblWiFi;
	private VBox dataLayout;
	private HBox layout;
	private HBox bxAction;

	//-------------------------------------------------------------------
	public BigCarriedItemListCell(EquipmentController charGen, ListView<CarriedItem> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		getStyleClass().add(NORMAL_STYLE);
		initComponents();
//		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("text-small-subheader");
		lblWiFi = new Label();
		lblAccessories = new Label();


		dataLayout = new VBox();
		dataLayout.getChildren().add(lblName);

		/*
		 * per Item Action Bar
		 */
		FontIcon icoEdit   = new FontIcon("\uE17E\uE104");
		FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
		FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
		Button btnEdit   = new Button(null, icoEdit);
		Button btnPopup  = new Button(null, icoPopup);
//		Button btnDelete = new Button(null, icoDelete);
//		Button btnSell   = new Button(null, icoSell);
//		Button btnUndo   = new Button(null, icoUndo);
		btnEdit.setStyle("-fx-border-width: 0px");
		btnPopup.setStyle("-fx-border-width: 0px");
		icoDelete.setStyle("-fx-padding: 3px");
		icoSell.setStyle("-fx-padding: 3px");
		icoUndo.setStyle("-fx-padding: 3px");

		MenuItem menUndo = new MenuItem(UI.getString("label.removeitem.undo"), icoUndo);
		MenuItem menSell = new MenuItem(UI.getString("label.removeitem.sell"), icoSell);
		MenuItem menDelete = new MenuItem(UI.getString("label.removeitem.trash"), icoDelete);
		ContextMenu menu = new ContextMenu();
		menu.getItems().add(menUndo);
		menu.getItems().add(menSell);
		menu.getItems().add(menDelete);
		btnPopup.setContextMenu(menu);
//		btnPopup.setOnMouseEntered(event -> {
//			logger.debug("onMouseEntered");
//			menu.show(btnPopup, Side.BOTTOM, 0, 0);
//			});

		btnPopup.setOnAction(event -> menu.show(btnPopup, Side.BOTTOM, 0, 0));
		btnEdit.setOnAction(event -> openEditDialog(data));
		menDelete.setOnAction(event -> charGen.sell(data, 0f));
		menSell.setOnAction(event -> charGen.sell(data, 0.5f));
		menUndo.setOnAction(event -> charGen.deselect(data));

		VBox bxButtons = new VBox();
		bxButtons.setStyle("-fx-spacing: 0.5em; -fx-padding-top: 0.1em;");
		bxButtons.getChildren().addAll(btnEdit, btnPopup);

		lblPrice2 = new Label();
		lblPrice2.setRotate(90);
//		lblPrice2.getStyleClass().add("itemprice-label");
		Group grp = new Group();
		grp.getChildren().add(lblPrice2);

		bxAction = new HBox();
		bxAction.getStyleClass().add("action-bar");
		bxAction.getChildren().addAll(bxButtons, grp);
		bxAction.setStyle("-fx-padding: 0.1em;");



		layout = new HBox();
		HBox.setHgrow(dataLayout, Priority.ALWAYS);
		layout.getChildren().addAll(dataLayout, bxAction);

//		layout.setStyle("-fx-min-width: 21em; -fx-max-width: 28em");
		layout.prefWidthProperty().bind(parent.widthProperty().subtract(24));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (item.getCount()==1)
				lblName.setText(item.getNameWithRating());
			else
				lblName.setText(item.getNameWithRating()+"  ("+item.getCount()+"x)");

			lblPrice2.setText(String.valueOf(item.getAsValue(ItemAttribute.PRICE).getModifiedValue())+" \u00A5");
			logger.debug("Item price of '"+item.getNameWithRating()+"' is "+item.getAsValue(ItemAttribute.PRICE));
			dataLayout.getChildren().retainAll(lblName);
			dataLayout.getChildren().add(ItemUtilJFX.getItemInfoNode(data, charGen));
			bxAction.setVisible(!item.isCreatedByModification());

			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void openEditDialog(CarriedItem data) {
		logger.debug("User clicked edit button");
		logger.debug("slots of "+data+" = "+data.getSlots());
		logger.debug("slots of raw "+data.getItem()+" = "+data.getItem().getSlots());

		String title= UI.getString("dialog.edititem.title")+" / "+data.getName();

		EditCarriedItemPane pane = new EditCarriedItemPane(charGen, provider);
		pane.setData(data);


		CloseType result = provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, title, pane);
		if (result==CloseType.OK) {
			int newCount = pane.getNewCount();
			if (newCount!=data.getCount()) {
				logger.info("Update count of "+data+" from "+data.getCount()+" to "+newCount);
				int loop = Math.abs(newCount - data.getCount());
				for (int i=0; i<loop; i++) {
					if (newCount<data.getCount()) {
						if (!charGen.decrease(data))
							break;
					} else {
						if (!charGen.increase(data))
							break;
					}
				}
				logger.info("New counter of "+data+" is "+data.getCount());
			}
			// Refresh cell content
			lblPrice2.setText(String.valueOf(data.getAsValue(ItemAttribute.PRICE).getModifiedValue())+" \u00A5");
			dataLayout.getChildren().retainAll(lblName);
			dataLayout.getChildren().add(ItemUtilJFX.getItemInfoNode(data, charGen));
		}
	}

}
