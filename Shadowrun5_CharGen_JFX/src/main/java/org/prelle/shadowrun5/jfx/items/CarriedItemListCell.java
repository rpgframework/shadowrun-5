package org.prelle.shadowrun5.jfx.items;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.FlipControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

public class CarriedItemListCell extends ListCell<CarriedItem> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".items");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private EquipmentController charGen;
	private ListView<CarriedItem> parent;
	private ScreenManagerProvider provider;

	private transient CarriedItem data;

	private FlipControl flip;
	private VBox frontLayout;
	private Label name;
	private Label accessories;
	private Label price;
	private Label count;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane frontStack;

	private Label builtIn;
	private Map<AvailableSlot,ChoiceBox<ItemTemplate>> cbSlots;
	private VBox back;
	private HBox line1;
	private TextField tfPrice;

	private ImageView imgRecommended;

	//-------------------------------------------------------------------
	public CarriedItemListCell(EquipmentController charGen, ListView<CarriedItem> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		initFrontComponents();
		initBackComponents();

		initStyle();
		initLayoutFront();
		initLayoutBack();
		initInteractivity();
		getStyleClass().add(NORMAL_STYLE);

		flip = new FlipControl();
//		flip.setSkin(new AdaptingFlipControlSkin(flip, new AdaptingFlipControlBehavior(flip)));
		flip.getItems().addAll(frontStack, back);
		flip.setStyle("-fx-pref-height: 5em");
	}

	//-------------------------------------------------------------------
	private void initFrontComponents() {
		price = new Label();
		price.getStyleClass().add("itemprice-label");
		StackPane.setAlignment(price, Pos.BOTTOM_RIGHT);

		// Content
		name    = new Label();
		accessories = new Label("acc");
		accessories.setWrapText(true);
		count   = new Label("anz");
		btnEdit = new Button("\uE1C2");

		// Recommended icon
		imgRecommended = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		frontStack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		frontLayout  = new VBox(5);
		frontStack.getChildren().addAll(price, frontLayout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
	}

	//-------------------------------------------------------------------
	private void initBackComponents() {
		builtIn = new Label();
		builtIn.setWrapText(true);
		
		tfPrice = new TextField();
		tfPrice.setStyle("-fx-pref-width: 4em");

		cbSlots = new HashMap<AvailableSlot, ChoiceBox<ItemTemplate>>();

		btnDec  = new Button("-");
		lblVal  = new Label("?");
		btnInc  = new Button("+");

		back = new VBox();
		back.setStyle("-fx-spacing: 0.2em");

		back.getChildren().add(name);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		price.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent");

		//		setStyle("-fx-pref-width: 24em");
		accessories.setStyle("-fx-pref-width: 15em");
		frontLayout.getStyleClass().add("content");
		back.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	private void initLayoutFront() {

		Region spacing = new Region();
		spacing.setMaxWidth(Double.MAX_VALUE);
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name, spacing, count);
		HBox.setHgrow(spacing, Priority.ALWAYS);

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, accessories);

		frontLayout.getChildren().addAll(line1, line2);

		frontLayout.setMaxWidth(Double.MAX_VALUE);

		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initLayoutBack() {
		// Block count
		Label heaCount = new Label("Count");
		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		HBox blkCount = new HBox(4);
		blkCount.getChildren().addAll(heaCount, tiles);
		blkCount.setAlignment(Pos.CENTER_LEFT);
		
		// Block Price
		Label heaPrice = new Label("\u00A5");
		HBox blkPrice = new HBox(4);
		blkCount.getChildren().addAll(heaPrice, tfPrice);
		
		// Line 1
		line1 = new HBox(8);
		line1.getChildren().addAll(blkCount, blkPrice);
		
		back.getChildren().add(line1);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			charGen.increase(data);
			updateItem(data, false);
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			charGen.decrease(data);
			updateItem(data, false);
			parent.refresh();
		});
		btnEdit.setOnAction(event -> {
			flip.flip();
//			editClicked(data);
		});

		tfPrice.textProperty().addListener( (ov,o,n) -> {
			logger.warn("TODO: price changed to "+n);
		});
		
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		String id = "gear:"+data.getItem().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void fillBackPane(CarriedItem item) {
		cbSlots.clear();
		logger.info("fillBackPane of "+item);
		for (AvailableSlot slot : item.getSlots()) {
			logger.info("Slot "+slot);
			Label heaSlot = new Label(slot.getSlot().getName());
			ChoiceBox<ItemTemplate> cbAccess = new ChoiceBox<>();
			cbAccess.setConverter(new StringConverter<ItemTemplate>() {
				public String toString(ItemTemplate item) {	return item.getName(); }
				public ItemTemplate fromString(String v) {return null;}
			});
			cbAccess.getItems().addAll(charGen.getEmbeddableIn(item, slot.getSlot()));
			
			HBox line = new HBox(5);
			line.getChildren().addAll(heaSlot, cbAccess);
//			back.getChildren().add(line);
			cbSlots.put(slot, cbAccess);
			// Set data
			if (!slot.getAllEmbeddedItems().isEmpty()) {
				cbAccess.getSelectionModel().select(slot.getAllEmbeddedItems().get(0).getItem());
			}
		}
		
		// Interactivity
//		for (AccessorySlot slot : item.getItem().getSlots()) {
//			ChoiceBox<ItemTemplate> cbAccess = cbSlots.get(slot);
//			cbAccess.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//				logger.warn("TODO: select accessory "+n+" for slot "+slot);
////				if (o!=null)
////					charGen.remove(item, null);
//				if (n!=null)
//					charGen.embed(item, n);
//			});
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			data = item;
			name.setText(item.getName());
			price.setText("\u00A5 "+item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
			tfPrice.setPromptText(String.valueOf(item.getAsValue(ItemAttribute.PRICE).getModifiedValue()));
			if (item.getCount()==1)
				count.setText(null);
			else
				count.setText(item.getCount()+"x");
			lblVal.setText(String.valueOf(item.getCount()));
			
			StringBuffer buf = new StringBuffer();
			Iterator<CarriedItem> it = item.getAccessories().iterator();
			while (it.hasNext()) {
				CarriedItem acc = it.next();
				buf.append(acc.getName());
				if (it.hasNext())
					buf.append(", ");
			}
			accessories.setText(buf.toString());
			imgRecommended.setVisible(false);
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());

			// Back side
			back.getChildren().retainAll(builtIn, line1);
			fillBackPane(item);

			setGraphic(flip);
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(CarriedItem data) {
		logger.debug("editClicked");

//		PowerSpecializationPane dia = new PowerSpecializationPane(charGen, data);
//
//		provider.getScreenManager().showAlertAndCall(
//				AlertType.NOTIFICATION,
//				UI.getString("powerselectpane.specialization.dialog.header"),
//				dia);
//		GenerationEventDispatcher.removeListener(dia);
//		updateItem(data, false);
	}

}