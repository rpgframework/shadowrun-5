/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class CommonEquipmentSelectionPane extends OptionalDescriptionPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".items");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	protected EquipmentController ctrl;
	ScreenManagerProvider provider;
	private ShadowrunCharacter model;
	private ItemType[] allowedItemTypes;

	private Label hdAvailable;
	private Label hdSelected;
	private Label hdDescript;
	private ItemTemplateSelector selector;
	protected ListView<CarriedItem> lvSelected;
	private VBox column3;
	private VBox colDescription;
	private ObjectProperty<ItemTemplate> selectedItem;

	//--------------------------------------------------------------------
	public CommonEquipmentSelectionPane(EquipmentController ctrl, ScreenManagerProvider provider, ItemType... allowedItemTypes) {
		this.ctrl = ctrl;
		if (ctrl==null)
			throw new NullPointerException();
		this.provider = provider;
		this.allowedItemTypes = allowedItemTypes;

		initComponents();
		initCellFactories();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
		GenerationEventDispatcher.addListener((ItemTemplateSelectorSkin)selector.getSkin());
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(UI.getString("label.available"));
		hdSelected  = new Label(UI.getString("label.selected"));
		hdDescript  = new Label(UI.getString("label.description"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdDescript.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdSelected.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdDescript.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdSelected.getStyleClass().add("text-subheader");

		selector = new ItemTemplateSelector(allowedItemTypes, ctrl);
		lvSelected  = new ListView<>();


		Label phSelected = new Label(UI.getString("equipmentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		colDescription = new VBox(5);
	}

	//--------------------------------------------------------------------
	protected  void initCellFactories() {
		lvSelected.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {
			public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
				BigCarriedItemListCell cell = new BigCarriedItemListCell(ctrl, lvSelected, provider);
//				cell.setStyle("-fx-max-width: 28em; -fx-pref-width: 26em");
				return cell;
			}
		});
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		selector.setStyle("-fx-min-width: 13em");
		lvSelected.setStyle("-fx-min-width: 22em; -fx-pref-width: 29em");


		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(selector, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, selector);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(hdSelected, lvSelected);
		column3 = new VBox(10);
		VBox.setVgrow(colDescription, Priority.ALWAYS);
		column3.getChildren().addAll(hdDescript, colDescription);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		selector.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);
		
		colDescription.setStyle("-fx-pref-width: 23em");
		column3.setStyle("-fx-max-width: 25em");

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);

		HBox content = new HBox(20, column1, column2);
		setChildren(content, column3);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {

		selector.selectedItemProperty().addListener( (ov,o,n) -> templateSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				templateSelectionChanged( n.getItem());
			}
		});
		lvSelected.setOnDragDropped(event -> dragDropped(event));
		lvSelected.setOnDragOver(event -> dragOver(event));
	}

	//--------------------------------------------------------------------
	private void templateSelectionChanged(ItemTemplate data) {
		if (selectedItem!=null) {
			selectedItem.set(data);
		}
		if (data!=null) {
			logger.debug("Selected "+data);
			
			if (colDescription!=null)
				column3.getChildren().remove(colDescription);
			colDescription = ItemUtilJFX.getItemInfoNode(data);
			column3.getChildren().add(colDescription);
			VBox.setVgrow(colDescription, Priority.ALWAYS);
		}
	}

	//--------------------------------------------------------------------
	private void refresh()  {
//		lvAvailable.getItems().clear();
//		lvAvailable.getItems().addAll(ctrl.getAvailablePowers());

		lvSelected.getItems().clear();
		logger.debug("Model = "+model);
		logger.debug("Items = "+model.getItems(false, allowedItemTypes));
		lvSelected.getItems().addAll(model.getItems(false, allowedItemTypes));
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private void askAndEquip(ItemTemplate data) {
//		List<SelectionOptionType> options = ctrl.getOptions(data);
//		logger.warn("TODO: options for "+data+": "+options);
//		
//		GridPane content = new GridPane();
//		content.setStyle("-fx-vgap: 1em; -fx-hgap: 1em");
//		Label lblPrice = new Label(data.getPrice()+"\u00A5");
//		lblPrice.setMaxHeight(Double.MAX_VALUE);
//		lblPrice.setAlignment(Pos.CENTER_LEFT);
//		lblPrice.setStyle("-fx-font-size: 200%");
//		content.add(lblPrice , 2, 0, 1,2);
//		
//		List<SelectionOption> selectedOptions = new ArrayList<SelectionOption>();
//		
//		if (options.contains(SelectionOptionType.RATING)) {
//			final EquipmentController.SelectionOption opt1 = new SelectionOption(SelectionOptionType.RATING, 1);
//			selectedOptions.add(opt1);
//			ChoiceBox<Integer> cbRate = new ChoiceBox<>();
//			cbRate.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//				logger.debug("Rating changed to "+n);
//				SelectionOption[] foo = new SelectionOption[selectedOptions.size()];
//				foo = selectedOptions.toArray(foo);
//				lblPrice.setText("\u00A5"+ctrl.getCost(data, foo));
//			});
//			// Add components to question dialog
//			Label heaRate = new Label(UI.getString("label.rating"));
//			for (int i=1; i<=data.getMaximumRating(); i++)
//				cbRate.getItems().add(i);
//			content.add(heaRate, 0, 0);
//			content.add(cbRate , 1, 0);
//		}
//		
//		if (options.contains(SelectionOptionType.BODYTECH_QUALITY)) {
//			final EquipmentController.SelectionOption opt2 = new SelectionOption(SelectionOptionType.BODYTECH_QUALITY, BodytechQuality.STANDARD);
//			selectedOptions.add(opt2);
//			ChoiceBox<BodytechQuality> cbQual = new ChoiceBox<>();
//			cbQual.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//				logger.debug("Quality changed to "+n);
//				SelectionOption[] foo = new SelectionOption[selectedOptions.size()];
//				foo = selectedOptions.toArray(foo);
//				lblPrice.setText("\u00A5"+ctrl.getCost(data, foo));
//			});
//			// Add components to question dialog
//			Label heaQual = new Label(UI.getString("label.quality"));
//			cbQual.getItems().addAll(BodytechQuality.values());
//			cbQual.setConverter(new StringConverter<BodytechQuality>() {
//				public String toString(BodytechQuality data) { return data.getName(); }
//				public BodytechQuality fromString(String string) { return null; }
//			});
//			content.add(heaQual, 0, 1);
//			content.add(cbQual , 1, 1);
//		}
//		
//		if (!selectedOptions.isEmpty()) {
//			provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, "Stufe", content);
//		}
//		
		SelectionOption[] options = ItemUtilJFX.askOptionsFor(provider, ctrl, data);
		ctrl.select(data, options);
	}

	//-------------------------------------------------------------------
	private void dragDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("gear")) {
					ItemTemplate data = ShadowrunCore.getItem(tail);
					if (data==null) {
						logger.warn("Cannot find equipment for dropped id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						
						Platform.runLater(new Runnable() {
							public void run() {
								askAndEquip(data);
							}
						});
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EQUIPMENT_ADDED:
		case EQUIPMENT_CHANGED:
		case EQUIPMENT_REMOVED:
		case EQUIPMENT_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}

	}

	//-------------------------------------------------------------------
	public void removeListener() {
		GenerationEventDispatcher.removeListener((ItemTemplateSelectorSkin)selector.getSkin());
	}

}
