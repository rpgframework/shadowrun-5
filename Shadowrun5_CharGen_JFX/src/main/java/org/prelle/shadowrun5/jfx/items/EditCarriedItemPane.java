/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.AvailableSlotListCell.RefreshableParent;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class EditCarriedItemPane extends VBox implements GenerationEventListener, RefreshableParent {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private EquipmentController control;
	private ScreenManagerProvider provider;
	private CarriedItem model;

	private TextField tfCount;
	private Button btnDec, btnInc;
	private Slider slCount;
	private Group  bxData;
	private Label  valTotal, valNewCost, valAvail;
	private ListView<CarriedItem> lvModifications;
	private ListView<AvailableSlot> lvSlots;

	private int newCount;
	private int newPrice;

	//--------------------------------------------------------------------
	public EditCarriedItemPane(EquipmentController ctrl, ScreenManagerProvider provider) {
		this.control = ctrl;
		this.provider = provider;
		initComponents();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		tfCount = new TextField();
		tfCount.setStyle("-fx-pref-width: 4em");
		btnDec = new Button(null, new FontIcon("\uE17E\uE108"));
		btnInc = new Button(null, new FontIcon("\uE17E\uE109"));
		btnDec.setStyle("-fx-border-width: 0px");
		btnInc.setStyle("-fx-border-width: 0px");

		slCount = new Slider();
		slCount.setMin(1.0);
		slCount.setMax(100.0);
		slCount.setMajorTickUnit(10);
		slCount.setMinorTickCount(5);
		slCount.setShowTickLabels(false);
		slCount.setShowTickMarks(true);
		slCount.setBlockIncrement(10);
		slCount.setStyle("-fx-max-width: 13em");

		valTotal   = new Label();
		valNewCost = new Label();
		valAvail   = new Label();
		valTotal.setStyle("-fx-text-fill: textcolor-highlight-primary");
		valNewCost.setStyle("-fx-text-fill: textcolor-highlight-primary");
		valAvail.setStyle("-fx-text-fill: textcolor-highlight-primary");

		bxData = new Group();
		lvModifications = new ListView<>();
		lvModifications.setPlaceholder(new Label("Noch nicht implementiert"));
		lvModifications.setStyle("-fx-max-height: 8em");
		lvSlots = new ListView<>();
		lvSlots.setOrientation(Orientation.HORIZONTAL);
		lvSlots.setCellFactory( param -> new AvailableSlotListCell(control, () -> {return model;}, provider, this));
		lvSlots.setSkin(new NewTiledListViewSkin<AvailableSlot>(lvSlots));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label heaMods  = new Label(UI.getString("label.modifications"));
		Label heaSlots = new Label(UI.getString("label.slots"));
		Label heaCount = new Label(UI.getString("label.count"));
		Label heaData  = new Label(UI.getString("label.data"));
		Label heaCost  = new Label(UI.getString("label.cost"));
		Label lblTotal = new Label(UI.getString("label.total"));
		Label lblNewCost= new Label(UI.getString("label.newcost"));
		Label lblAvail = new Label(UI.getString("label.available"));
		heaMods.getStyleClass().add("text-subheader");
		heaSlots.getStyleClass().add("text-subheader");
		heaCount.getStyleClass().add("text-subheader");
		heaData.getStyleClass().add("text-subheader");
		heaCost.getStyleClass().add("text-subheader");
		heaMods.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaSlots.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaCount.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaData.setStyle("-fx-text-fill: textcolor-highlight-primary");
		heaCost.setStyle("-fx-text-fill: textcolor-highlight-primary");

		TilePane paneCount = new TilePane();
		paneCount.getChildren().addAll(btnDec, tfCount, btnInc);
		paneCount.setStyle("-fx-max-width: 13em");

		GridPane costGrid = new GridPane();
		costGrid.add(lblTotal  , 0, 0);
		costGrid.add(lblNewCost, 0, 1);
		costGrid.add(lblAvail  , 0, 2);
		costGrid.add(valTotal  , 1, 0);
		costGrid.add(valNewCost, 1, 1);
		costGrid.add(valAvail  , 1, 2);

		VBox bxCnt = new VBox();
		bxCnt.setStyle("-fx-spacing: 1em");
		bxCnt.getChildren().addAll(heaCount , paneCount, slCount);
		VBox bxCst = new VBox();
		bxCst.setStyle("-fx-spacing: 1em");
		bxCst.getChildren().addAll(heaCost, costGrid);
		VBox bxDat = new VBox();
		bxDat.setStyle("-fx-spacing: 1em");
		bxDat.getChildren().addAll(heaData, bxData);
		HBox line1 = new HBox();
		line1.setStyle("-fx-spacing: 2em");
		line1.getChildren().addAll(bxCnt, bxCst, bxDat);

		getChildren().addAll(line1, heaMods, lvModifications, heaSlots, lvSlots);
		VBox.setMargin(heaSlots, new Insets(20,0,0,0));
		VBox.setVgrow(lvSlots, Priority.ALWAYS);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		slCount.valueProperty().addListener( (ov,o,n) -> {
			if (control.canChangeCount(model, (int)Math.abs((Double)n))) {
				newCount = (int)Math.abs((Double)n);
				newPrice = control.getCountChangeCost(model, newCount);
				refreshCount();
			} else
				slCount.setValue(newCount);
		});
		tfCount.textProperty().addListener( (ov,o,n) -> {
			if (n==null)
				return;
			try {
				int cnt = Integer.parseInt(n);
				newCount = cnt;
				refreshCount();
			} catch (Exception e) {
				tfCount.setText(String.valueOf(newCount));
			}
		});
		btnDec.setOnAction(event -> {
			if (newCount>1) {
				newCount--;
				newPrice = control.getCountChangeCost(model, newCount);
				refreshCount();
			}
		});
		btnInc.setOnAction(event -> {
			if (newCount<100) {
				newCount++;
				newPrice = control.getCountChangeCost(model, newCount);
				refreshCount();
			}
		});
	}

	//--------------------------------------------------------------------
	private void refreshCount() {
		tfCount.setText(String.valueOf(newCount));
		slCount.setValue(newCount);
		valNewCost.setText( newPrice +" \u00A5");

		btnDec.setDisable(!control.canChangeCount(model, newCount-1));
		btnInc.setDisable(!control.canChangeCount(model, newCount+1));
	}

	//--------------------------------------------------------------------
	private void refresh() {
		refreshCount();
		logger.debug("Call specific node");
		bxData.getChildren().clear();
		bxData.getChildren().add(ItemUtilJFX.getItemInfoNode(model, control));
		lvModifications.getItems().clear();

		lvSlots.getItems().clear();
		lvSlots.getItems().addAll(model.getSlots());
		valTotal.setText(model.getAsValue(ItemAttribute.PRICE).getModifiedValue()+" \u00A5");
		valAvail.setText(control.getModel().getNuyen()+" \u00A5");
	}

	//--------------------------------------------------------------------
	public void setData(CarriedItem item) {
		this.model = item;
		newCount = item.getCount();
		newPrice = 0;

		valTotal.setText(model.getAsValue(ItemAttribute.PRICE).getModifiedValue()+" \u00A5");
		valAvail.setText(control.getModel().getNuyen()+" \u00A5");
		refresh();
	}

	//--------------------------------------------------------------------
	public int getNewCount() {
		return newCount;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		// TODO Auto-generated method stub
		logger.debug("RCV "+event);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.AvailableSlotListCell.RefreshableParent#refreshAsParent()
	 */
	@Override
	public void refreshAsParent() {
		refresh();
	}

}
