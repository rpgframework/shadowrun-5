/**
 * 
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.Arrays;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.Resource;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class EmbedItemsPane extends VBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private EquipmentController control;
	private CarriedItem model;
	private AvailableSlot slot;
	private ScreenManagerProvider provider;

	private ThreeColumnPane columns;
	private ListView<ItemTemplate> lvTemplates;
	private ListView<CarriedItem> lvSelected;
	private VBox colDescription;
	
	private ObjectProperty<ItemTemplate> selectedItem;

	//-------------------------------------------------------------------
	public EmbedItemsPane(EquipmentController ctrl, CarriedItem model, AvailableSlot slot, ScreenManagerProvider provider) {
		this.control = ctrl;
		this.model   = model;
		this.slot    = slot;
		this.provider= provider;
		
		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lvTemplates = new ListView<ItemTemplate>();
		lvTemplates.setCellFactory(new Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>>() {
			public ListCell<ItemTemplate> call(ListView<ItemTemplate> param) {
				ItemTemplateListCell2 cell = new ItemTemplateListCell2(control);
				cell.setStyle("-fx-max-width: 30em; -fx-pref-width: 30em");
				return cell;
			}
		});
		
		lvSelected = new ListView<CarriedItem>();
		lvSelected.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {
			public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
				BigCarriedItemListCell cell = new BigCarriedItemListCell(control, lvSelected, provider);
				cell.setStyle("-fx-max-width: 40em; -fx-pref-width: 30em");
				return cell;
			}
		});

		Label phSelected = new Label(Resource.get(UI, "equipmentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);
		
		colDescription = new VBox(5);
		
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		columns = new ThreeColumnPane();
		
		columns.setColumn1Header(UI.getString("label.available"));
		columns.setColumn2Header(UI.getString("label.selected"));
		columns.setColumn3Header(UI.getString("label.description"));
		columns.setHeadersVisible(true);
		columns.setColumn1Node(lvTemplates);
		columns.setColumn2Node(lvSelected);
		columns.setColumn3Node(colDescription);
		columns.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		lvTemplates.setStyle("-fx-max-width: 30em; -fx-pref-width: 30em");
		lvSelected.setStyle("-fx-max-width: 40em; -fx-pref-width: 40em");
		getChildren().add(columns);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvTemplates.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				templateSelectionChanged( n);
			}
		});
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				templateSelectionChanged( n.getItem());
			}
		});
		lvSelected.setOnDragDropped(event -> dragDropped(event));
		lvSelected.setOnDragOver(event -> dragOver(event));
	}

	//--------------------------------------------------------------------
	private void templateSelectionChanged(ItemTemplate data) {
		logger.info("templateSelectionChanged("+data+")");
		if (selectedItem!=null) {
			selectedItem.set(data);
		}
		if (data!=null) {
			logger.debug("Selected "+data);
			
			colDescription.getChildren().clear();
			colDescription.getChildren().add(ItemUtilJFX.getItemInfoNode(data));
			VBox.setVgrow(colDescription, Priority.ALWAYS);
		}
	}
	
	//-------------------------------------------------------------------
	private void refresh() {
		lvTemplates.getItems().clear();
		lvTemplates.getItems().addAll(control.getEmbeddableIn(model, slot.getSlot()));
		
		logger.info("Embedded items in slot are "+slot.getUserEmbeddedItems());
		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(slot.getUserEmbeddedItems());
	}

	//-------------------------------------------------------------------
	private void dragDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("gear")) {
					ItemTemplate data = ShadowrunCore.getItem(tail);
					if (data==null) {
						logger.warn("Cannot find equipment for dropped id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						
						Platform.runLater(new Runnable() {
							public void run() {
								askAndEquip(data);
								logger.debug("Dropped finish - refresh now");
								refresh();
							}
						});
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private boolean askAndEquip(ItemTemplate data) {
		SelectionOption[] selectedOptions = ItemUtilJFX.askOptionsFor(provider, control, data);
		logger.debug("Selected options = "+Arrays.toString(selectedOptions));
		try {
			CarriedItem embedded = control.embed(model, data, slot.getSlot(), selectedOptions);
			return embedded != null;
		} catch (Exception e) {
			logger.error("Exception while embedding: ",e);
			return false;
		}
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EQUIPMENT_ADDED:
		case EQUIPMENT_REMOVED:
		case EQUIPMENT_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

}
