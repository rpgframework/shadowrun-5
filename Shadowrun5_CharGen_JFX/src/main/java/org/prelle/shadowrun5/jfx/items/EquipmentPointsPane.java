/**
 * 
 */
package org.prelle.shadowrun5.jfx.items;

import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.CharacterController;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.jfx.PointsPane;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author prelle
 *
 */
public class EquipmentPointsPane extends PointsPane {
	
	private EquipmentController equipCtrl;
	private Label lblKarmaConverted;
	private Button btnDec;
	private Button btnInc;

	//-------------------------------------------------------------------
	public EquipmentPointsPane(CharacterController ctrl, Class<?> type) {
		super(ctrl, type);
	
		if (ctrl.getMode()==CharGenMode.CREATING) {
			equipCtrl = ctrl.getEquipmentController();
		}
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		super.initComponents();
		
		lblKarmaConverted = new Label("0");
		btnDec = new Button("-");
		btnDec.setStyle("-fx-font-size: 150%");
		btnInc = new Button("+");
		btnInc.setStyle("-fx-font-size: 150%");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		super.initLayout();
		
		if (ctrl.getMode()==CharGenMode.CREATING) {
			Label heaKarmaConverted = new Label();
			heaKarmaConverted.setWrapText(true);		
			heaKarmaConverted.setText(UI.getString("equipment.karmaconverted"));

			HBox line = new HBox();
			line.getChildren().addAll(btnDec, lblKarmaConverted, btnInc);
			lblKarmaConverted.setStyle("-fx-pref-width: 4em");
			lblKarmaConverted.setAlignment(Pos.CENTER);
			lblKarmaConverted.setTextAlignment(TextAlignment.CENTER);
			btnDec.setStyle("-fx-pref-width: 4em");
			btnInc.setStyle("-fx-pref-width: 4em");
			line.setAlignment(Pos.CENTER);

			getChildren().addAll(heaKarmaConverted, line);
			VBox.setMargin(heaKarmaConverted, new Insets(20,0,10,0));
			this.setAlignment(Pos.TOP_CENTER);
		}
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		if (equipCtrl==null)
			return;
		
		btnDec.setOnAction(event -> equipCtrl.decreaseBoughtNuyen());
		btnInc.setOnAction(event -> equipCtrl.increaseBoughtNuyen());
	}

	//-------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		
		if (equipCtrl!=null) {
			btnDec.setDisable(!equipCtrl.canDecreaseBoughtNuyen());
			lblKarmaConverted.setText(String.valueOf(equipCtrl.getBoughtNuyen()));
			btnInc.setDisable(!equipCtrl.canIncreaseBoughtNuyen());
		}
	}

}
