/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class EquipmentSelector extends HBox {

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private EquipmentController control;
//	private TextField tfName;
//	private ChoiceBox<ItemType> cbTypes;
//	private ChoiceBox<ItemSubType> cbSubTypes;
//	private ListView<ItemTemplate> lvItems;
	private ItemTemplateSelector selector;

	private Node detailNode;
	private ChoiceBox<Integer> cbRating;
	private HBox ratingNode;

	//-------------------------------------------------------------------
	public EquipmentSelector(EquipmentController ctrl, ItemType... allowedTypes) {
		this.control = ctrl;

		initComponents(allowedTypes);
		initLayout();
		initInteractivity();

//		cbTypes.getSelectionModel().select(0);
//		cbSubTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(ItemType... allowedTypes) {
//		tfName = new TextField();
//		tfName.setPromptText(UI.getString("itemselector.name.prompt"));
//
//		cbTypes = new ChoiceBox<ItemType>();
//		cbTypes.setMaxWidth(Double.MAX_VALUE);
//		cbTypes.getItems().addAll(allowedTypes);
//		cbTypes.setConverter(new StringConverter<ItemType>() {
//			public String toString(ItemType data) {return data.getName();}
//			public ItemType fromString(String data) {return null;}
//		});
//
//		cbSubTypes = new ChoiceBox<ItemSubType>();
//		cbSubTypes.setMaxWidth(Double.MAX_VALUE);
//		cbSubTypes.setConverter(new StringConverter<ItemSubType>() {
//			public String toString(ItemSubType data) {return data.getName();}
//			public ItemSubType fromString(String data) {return null;}
//		});
//
//		lvItems = new ListView<ItemTemplate>();
//		lvItems.setCellFactory(new Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>>() {
//
//			@Override
//			public ListCell<ItemTemplate> call(ListView<ItemTemplate> param) {
//				return new ItemTemplateListCell2(control);
//			}
//		});
		selector = new ItemTemplateSelector(allowedTypes, control);

		cbRating = new ChoiceBox<>();
		cbRating.getItems().addAll(1,2,3,4,5,6);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		VBox column1 = new VBox();
//		column1.setStyle("-fx-spacing: 0.5em");
//		column1.getChildren().addAll(tfName, cbTypes, cbSubTypes, lvItems);
//
//		lvItems.setStyle("-fx-pref-width: 30em");

		setSpacing(20);
		getChildren().add(selector);

		// Elements for second column
		Label lblRating = new Label(UI.getString("label.rating"));
		ratingNode = new HBox(10, lblRating, cbRating);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
//		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			if (n!=null) {
//				cbSubTypes.getItems().clear();
//				cbSubTypes.getItems().addAll(n.getSubTypes());
//			}
//		});
//
//		cbSubTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			if (n!=null) {
//				lvItems.getItems().clear();
//				lvItems.getItems().addAll(ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()));
//			}
//		});

		selector.selectedItemProperty().addListener( (ov,o,n) -> {
//		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				if (detailNode!=null)
					getChildren().remove(detailNode);
				VBox column2 = new VBox(20);
				column2.getChildren().add(ItemUtilJFX.getItemInfoNode(n));
				Region spacing = new Region();
				spacing.setMaxHeight(Double.MAX_VALUE);
				column2.getChildren().add(spacing);
				VBox.setVgrow(spacing, Priority.ALWAYS);
				if (n.hasRating()) {
					column2.getChildren().add(ratingNode);
				}

				detailNode = column2;
				getChildren().add(detailNode);
			}
		});

//		tfName.setOnAction(event -> {
//			String filter = tfName.getText();
//			List<ItemTemplate> poss = ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue());
//			List<ItemTemplate> filtered = new ArrayList<>();
//			if (filter.length()==0) {
//				filtered = poss;
//			} else {
//				// Filter those names that match search string
//				for (ItemTemplate tmp : poss) {
//					if (tmp.getName().toLowerCase().contains(filter.toLowerCase())) {
//						filtered.add(tmp);
//					}
//				}
//			}
//			lvItems.getItems().clear();
//			lvItems.getItems().addAll(filtered);
//		});
	}

	//--------------------------------------------------------------------
	public ItemTemplate getTemplate() {
		return selector.getSelectedItem();
//		return lvItems.getSelectionModel().getSelectedItem();
	}

	//--------------------------------------------------------------------
	public int getRating() {
		Integer val = cbRating.getValue();
		if (val==null)
			return 0;
		return val;
	}

}
