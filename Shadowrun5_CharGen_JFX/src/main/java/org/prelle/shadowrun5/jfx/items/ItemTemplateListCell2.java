/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.items.ItemTemplate;

/**
 * @author prelle
 *
 */
public class ItemTemplateListCell2 extends ListCell<ItemTemplate> {

	private final static String NORMAL_STYLE = "equipment-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private EquipmentController control;
	private ItemTemplate data;

	private Label lblName;
	private Label lblAvail;
	private Label lblPrice;
	private HBox  lower;
	private StackPane stack;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemTemplateListCell2(EquipmentController ctrl) {
		this.control = ctrl;
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");
		lblPrice.setStyle("-fx-pref-width: 6em");

		lblAvail = new Label();
		lblAvail.setStyle("-fx-pref-width: 4em");
		lblAvail.getStyleClass().add("itemprice-label");
		lower   = new HBox(10);
		lower.setMaxWidth(Double.MAX_VALUE);
		lower.getChildren().addAll(buffer, lblAvail, lblPrice);
		HBox.setHgrow(buffer, Priority.ALWAYS);

		stack = new StackPane();
		stack.getChildren().addAll(lblName, lower);
		stack.setMaxWidth(Double.MAX_VALUE);
		StackPane.setAlignment(lower, Pos.CENTER_RIGHT);
		StackPane.setAlignment(lblName, Pos.CENTER_LEFT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ItemTemplate item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(((ItemTemplate)item).getName());
			lblPrice.setText("\u00A5 "+item.getPrice());
			lblAvail.setText(item.getAvailability().toString());
			if (control.canBeSelected(item)) {
				lblPrice.getStyleClass().remove(NOT_MET_STYLE);
				lblAvail.getStyleClass().remove(NOT_MET_STYLE);
			} else {
				lblPrice.getStyleClass().add(NOT_MET_STYLE);
				lblAvail.getStyleClass().add(NOT_MET_STYLE);
			}
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("gear:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}


}
