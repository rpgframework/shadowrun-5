/**
 * 
 */
package org.prelle.shadowrun5.jfx.items;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelector extends Control implements IListSelector<ItemTemplate>{
	
	private ItemType[] allowedTypes;
	private ObjectProperty<ItemTemplate> selected;

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemType[] allowed, EquipmentController control) {
		allowedTypes = allowed;
		selected = new SimpleObjectProperty<ItemTemplate>();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemType[] getAllowedTypes() {
		return allowedTypes;
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<ItemTemplate> selectedItemProperty() {
		return selected;
	}

	//-------------------------------------------------------------------
	public ItemTemplate getSelectedItem() {
		return selected.get();
	}

	//-------------------------------------------------------------------
	void setSelectedItem(ItemTemplate data) {
		selected.set(data);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<ItemTemplate> getSelectionModel() {
		return ((ItemTemplateSelectorSkin)getSkin()).getSelectionModel();
	}
	
}
