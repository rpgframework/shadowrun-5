/**
 * 
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelectorSkin extends SkinBase<ItemTemplateSelector> implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private EquipmentController controller;
	private VBox content;
	private TextField tfName;
	private ChoiceBox<ItemType> cbTypes;
	private ChoiceBox<ItemSubType> cbSubTypes;
	private ListView<ItemTemplate> lvItems;
	
	//-------------------------------------------------------------------
	/**
	 * @param control
	 */
	public ItemTemplateSelectorSkin(ItemTemplateSelector control, EquipmentController controller) {
		super(control);
		this.controller = controller;
		
		initComponents(control.getAllowedTypes());
		initLayout();
		initInteractivity();
		cbTypes.getSelectionModel().select(0);
		cbSubTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(ItemType... allowedTypes) {
		tfName = new TextField();
		tfName.setPromptText(UI.getString("itemselector.name.prompt"));

		cbTypes = new ChoiceBox<ItemType>();
		cbTypes.setMaxWidth(Double.MAX_VALUE);
		cbTypes.getItems().addAll(allowedTypes);
		cbTypes.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType data) {return data.getName();}
			public ItemType fromString(String data) {return null;}
		});

		cbSubTypes = new ChoiceBox<ItemSubType>();
		cbSubTypes.setMaxWidth(Double.MAX_VALUE);
		cbSubTypes.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType data) {return data.getName();}
			public ItemSubType fromString(String data) {return null;}
		});

		lvItems = new ListView<ItemTemplate>();
		lvItems.setCellFactory(new Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>>() {
			public ListCell<ItemTemplate> call(ListView<ItemTemplate> param) {
				return new ItemTemplateListCell2(controller);
			}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new VBox();
		content.setStyle("-fx-spacing: 0.5em");
		content.getChildren().addAll(tfName, cbTypes, cbSubTypes, lvItems);
		VBox.setVgrow(lvItems, Priority.ALWAYS);

		lvItems.setStyle("-fx-pref-width: 30em");
		lvItems.setMaxHeight(Double.MAX_VALUE);

		getSkinnable().impl_getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				cbSubTypes.getItems().clear();
				lvItems.getItems().clear();
				cbSubTypes.getItems().addAll(n.getSubTypes());
				cbSubTypes.getSelectionModel().select(0);
			}
		});

		cbSubTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				lvItems.getItems().clear();
				lvItems.getItems().addAll(
						ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()).stream().filter(it -> !it.isSelectableByModificationOnly()).collect(Collectors.toList()));
			}
		});

		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			this.getSkinnable().setSelectedItem(n);
		});

		tfName.setOnAction(event -> {
			String filter = tfName.getText();
			List<ItemTemplate> poss = ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue());
			List<ItemTemplate> filtered = new ArrayList<>();
			if (filter.length()==0) {
				filtered = poss;
			} else {
				// Filter those names that match search string
				for (ItemTemplate tmp : poss) {
					if (tmp.getName().toLowerCase().contains(filter.toLowerCase())) {
						filtered.add(tmp);
					}
				}
			}
			lvItems.getItems().clear();
			lvItems.getItems().addAll(filtered);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EQUIPMENT_AVAILABLE_CHANGED:
		case NUYEN_CHANGED:
			logger.debug("RCV "+event);
			lvItems.getItems().clear();
			lvItems.getItems().addAll(ShadowrunCore.getItems(cbTypes.getValue(), cbSubTypes.getValue()));
			break;
		default:
		}
	}

	SelectionModel<ItemTemplate> getSelectionModel() {
		return lvItems.getSelectionModel();
	}

}
