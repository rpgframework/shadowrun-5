/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.charctrl.EquipmentController;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun5.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun5.items.BodytechQuality;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ItemUtilJFX {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".items");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	//-------------------------------------------------------------------
	public static VBox getItemInfoNode(ItemTemplate item) {
		Label lblName = new Label(item.getName());
		lblName.getStyleClass().add("text-subheader");

		Label lblProd = new Label(item.getProductName()+" "+item.getPage());
		lblProd.getStyleClass().add("itemprice-label");


		VBox box = new VBox(10);
		box.setStyle("-fx-max-width: 40em");
		box.setMaxHeight(Double.MAX_VALUE);
		box.getChildren().addAll(lblName, lblProd);
		if (item.getHelpText()!=null) {
			Label lblDesc = new Label(item.getHelpText());
			lblDesc.setWrapText(true);
			box.getChildren().add(lblDesc);
		}

		switch (item.getType()) {
		case WEAPON:
			box.getChildren().add(getWeaponNode(item));
			break;
		case BIOWARE:
		case CYBERWARE:
			box.getChildren().add(getAugmentationNode(item));
			break;
		case ARMOR:
			box.getChildren().add(getArmorNode(item));
			break;
		case ELECTRONICS:
			switch (item.getSubtype()) {
			case COMMLINK:
				box.getChildren().add(getMatrixDeviceNode(item));
				break;
			case CYBERDECK:
				box.getChildren().add(getCyberdeckNode(item));
				break;
			default:
				logger.warn("No special display for "+item.getType()+"/"+item.getSubtype());
			}
			break;
		default:
			logger.warn("No special display for "+item.getType());
			VBox modBox = new VBox(3);
			for (Modification mod : item.getModifications()) {
				modBox.getChildren().add(new Label(ShadowrunTools.getModificationString(mod)));
			}
			box.getChildren().add(modBox);
		}

		// WiFi
		if (!item.getWiFiAdvantageStrings().isEmpty()) {
			box.getChildren().add(getWiFiAdvantagesNode(item));
		}

		return box;
	}

	//-------------------------------------------------------------------
	public static Node getWiFiAdvantagesNode(ItemTemplate item) {
		Label heaWifi = new Label(UI.getString("label.wifiadvantage")+": ");
		heaWifi.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-weight: bold;");

		Label lblWifi = new Label(String.join(",\n", item.getWiFiAdvantageStrings()));
		lblWifi.setWrapText(true);
		lblWifi.setStyle("-fx-max-width: 26em");

		VBox flow = new VBox();
		flow.getChildren().addAll(heaWifi, lblWifi);
		VBox.setMargin(flow, new Insets(5, 0, 5, 0));
		return flow;
	}

	//-------------------------------------------------------------------
	private static GridPane getWeaponNode(ItemTemplate item) {
		int COL_ACCU = 0;
		int COL_DMG  = 1;
		int COL_AP   = 2;
		int COL_REA  = 3;
		int COL_MODE = 4;
		int COL_RC   = 5;
		int COL_AMMO = 6;

		Label heaAcc  = new Label(ItemAttribute.ACCURACY.getShortName());
		Label heaDmg  = new Label(ItemAttribute.DAMAGE.getShortName());
		Label heaAP   = new Label(ItemAttribute.ARMOR_PENETRATION.getShortName());
		Label heaRea  = new Label(ItemAttribute.REACH.getShortName());
		Label heaMode = new Label(ItemAttribute.MODE.getShortName());
		Label heaRC   = new Label(ItemAttribute.RECOIL_COMPENSATION.getShortName());
		Label heaAmmo = new Label(ItemAttribute.AMMUNITION.getShortName());

		heaAcc .getStyleClass().add("table-head");
		heaDmg .getStyleClass().add("table-head");
		heaAP  .getStyleClass().add("table-head");
		heaRea .getStyleClass().add("table-head");
		heaMode.getStyleClass().add("table-head");
		heaRC  .getStyleClass().add("table-head");
		heaAmmo.getStyleClass().add("table-head");

		heaAcc .setMaxWidth(Double.MAX_VALUE);
		heaDmg .setMaxWidth(Double.MAX_VALUE);
		heaAP  .setMaxWidth(Double.MAX_VALUE);
		heaRea .setMaxWidth(Double.MAX_VALUE);
		heaMode.setMaxWidth(Double.MAX_VALUE);
		heaRC  .setMaxWidth(Double.MAX_VALUE);
		heaAmmo.setMaxWidth(Double.MAX_VALUE);

		heaAcc .setAlignment(Pos.CENTER);
		heaDmg .setAlignment(Pos.CENTER);
		heaAP  .setAlignment(Pos.CENTER);
		heaRea .setAlignment(Pos.CENTER);
		heaMode.setAlignment(Pos.CENTER);
		heaRC  .setAlignment(Pos.CENTER);
		heaAmmo.setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.getColumnConstraints().add(new ColumnConstraints( 70)); // Damage
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Armor Penetration
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Reach
		grid.getColumnConstraints().add(new ColumnConstraints(100)); // Mode
		grid.getColumnConstraints().add(new ColumnConstraints( 30)); // Recoil compensation
		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Ammo
		grid.add(heaAcc , COL_ACCU, 0);
		grid.add(heaDmg , COL_DMG , 0);
		grid.add(heaAP  , COL_AP  , 0);
		grid.add(heaRea , COL_REA , 0);
		grid.add(heaMode, COL_MODE, 0);
		grid.add(heaRC  , COL_RC  , 0);
		grid.add(heaAmmo, COL_AMMO, 0);

		// Data
		Label lblAcc = new Label(String.valueOf(item.getWeaponData().getAccuracy()));
		Label lblDmg = new Label(item.getWeaponData().getDamage().toString());
		Label lblAP  = new Label(String.valueOf(item.getWeaponData().getArmorPenetration()));
		Label lblRea = new Label(String.valueOf(item.getWeaponData().getReach()));
		Label lblMod = new Label(String.join(", ", item.getWeaponData().getFireModeNames()));
		Label lblRC  = new Label(String.valueOf(item.getWeaponData().getRecoilCompensation()));
		Label lblAmm = new Label(String.valueOf(item.getWeaponData().getAmmunition()));
		grid.add(lblAcc, COL_ACCU, 1);
		grid.add(lblDmg, COL_DMG , 1);
		grid.add(lblAP , COL_AP  , 1);
		grid.add(lblRea, COL_REA , 1);
		grid.add(lblMod, COL_MODE, 1);
		grid.add(lblRC , COL_RC  , 1);
		grid.add(lblAmm, COL_AMMO, 1);
		GridPane.setHalignment(lblAcc, HPos.CENTER);
		GridPane.setHalignment(lblDmg, HPos.CENTER);
		GridPane.setHalignment(lblAP , HPos.CENTER);
		GridPane.setHalignment(lblRea, HPos.CENTER);
		GridPane.setHalignment(lblMod, HPos.CENTER);
		GridPane.setHalignment(lblRC , HPos.CENTER);
		GridPane.setHalignment(lblAmm, HPos.CENTER);

		return grid;
	}

	//-------------------------------------------------------------------
	private static GridPane getAugmentationNode(ItemTemplate item) {
		int COL_RATE = 0;
		int COL_ESSE = 1;
		int COL_CAP  = 2;
		int COL_AVAI = 3;
		int COL_PRIC = 4;

		Label heaRat  = new Label(ItemAttribute.MAX_RATING.getName());
		Label heaEss  = new Label(ItemAttribute.ESSENCECOST.getName());
		Label heaCap  = new Label(ItemAttribute.CAPACITY.getShortName());
		Label heaAva  = new Label(ItemAttribute.AVAILABILITY.getShortName());
		Label heaPri  = new Label(ItemAttribute.PRICE.getName());

		heaRat .getStyleClass().add("table-head");
		heaEss .getStyleClass().add("table-head");
		heaCap .getStyleClass().add("table-head");
		heaAva .getStyleClass().add("table-head");
		heaPri .getStyleClass().add("table-head");

		heaRat .setMaxWidth(Double.MAX_VALUE);
		heaEss .setMaxWidth(Double.MAX_VALUE);
		heaCap .setMaxWidth(Double.MAX_VALUE);
		heaAva .setMaxWidth(Double.MAX_VALUE);
		heaPri .setMaxWidth(Double.MAX_VALUE);

		heaRat .setAlignment(Pos.CENTER);
		heaEss .setAlignment(Pos.CENTER);
		heaCap .setAlignment(Pos.CENTER);
		heaAva .setAlignment(Pos.CENTER);
		heaPri .setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		grid.getColumnConstraints().add(new ColumnConstraints( 60)); // Rating
		grid.getColumnConstraints().add(new ColumnConstraints( 60)); // Essence
		grid.getColumnConstraints().add(new ColumnConstraints( 60)); // Capacity
		grid.getColumnConstraints().add(new ColumnConstraints( 60)); // Availability
		grid.getColumnConstraints().add(new ColumnConstraints( 80)); // Price
		grid.add(heaRat , COL_RATE, 0);
		grid.add(heaEss , COL_ESSE, 0);
		grid.add(heaCap , COL_CAP , 0);
		grid.add(heaAva , COL_AVAI, 0);
		grid.add(heaPri , COL_PRIC, 0);

		// Data
		int maxRating = item.hasRating()?item.getMaximumRating():1;
		maxRating = Math.max(1,  maxRating);
		for (int i=1; i<=maxRating; i++) {
			Label lblRat = new Label(item.hasRating()?String.valueOf(i):"-");
			// Essence
			float essence = item.getBodyTech().getEssence();
			if (item.multipliesWithRate(Multiply.ESSENCE))
				essence *= i;
			// Capacity
			//			float cap = item.getBodyTech().get;
			//			if (item.multipliesWithRate(Multiply.ESSENCE))
			//				essence *= i;
			// Availability
			int avail = item.getAvailability().getValue();
			if (item.multipliesWithRate(Multiply.AVAIL))
				avail *= i;
			// Price
			int price = item.getPrice();
			if (item.multipliesWithRate(Multiply.PRICE))
				price *= i;
			// Capacity
			int cap = 0;
			if (!item.getSlots().isEmpty()) {
				cap = item.getSlots().iterator().next().getCapacity();
			}

			Label lblEss = new Label(String.valueOf(essence));
			Label lblCap = new Label( (cap==0)?"-":String.valueOf(cap));
			Label lblAva = new Label(String.valueOf(avail)+item.getAvailability().getLegality().getShortCode());
			Label lblPri = new Label(String.valueOf(price)+"\u00A5");
			grid.add(lblRat, COL_RATE, i);
			grid.add(lblEss, COL_ESSE, i);
			grid.add(lblCap, COL_CAP , i);
			grid.add(lblAva, COL_AVAI, i);
			grid.add(lblPri, COL_PRIC, i);
			GridPane.setHalignment(lblRat, HPos.CENTER);
			GridPane.setHalignment(lblEss, HPos.CENTER);
			GridPane.setHalignment(lblCap, HPos.CENTER);
			GridPane.setHalignment(lblAva, HPos.CENTER);
			GridPane.setHalignment(lblPri, HPos.CENTER);
		}

		return grid;
	}

	//-------------------------------------------------------------------
	private static GridPane getArmorNode(ItemTemplate item) {
		int COL_ARM  = 0;

		Label heaArm  = new Label(ItemAttribute.ARMOR.getName());

		heaArm .getStyleClass().add("table-head");

		heaArm .setMaxWidth(Double.MAX_VALUE);

		heaArm .setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		//		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.add(heaArm , COL_ARM , 0);

		// Data
		if (item.getArmorData()!=null) {
			Label lblAcc = new Label(item.getArmorData().addsToMain()?"+":"" + item.getArmorData().getRating());
			grid.add(lblAcc, COL_ARM , 1);
			GridPane.setHalignment(lblAcc, HPos.CENTER);
		}

		return grid;
	}

	//-------------------------------------------------------------------
	private static GridPane getMatrixDeviceNode(ItemTemplate item) {
		int COL_DEV  = 0;
//		int COL_ATT  = 1;
//		int COL_SLZ  = 2;
		int COL_FIR  = 3;
		int COL_PRO  = 4;
		int COL_PRG  = 5;

		Label heaDev  = new Label(ItemAttribute.DEVICE_RATING.getShortName());
//		Label heaAtt  = new Label(ItemAttribute.ATTACK.getShortName());
//		Label heaSlz  = new Label(ItemAttribute.SLEAZE.getShortName());
		Label heaFir  = new Label(ItemAttribute.FIREWALL.getShortName());
		Label heaPro  = new Label(ItemAttribute.DATA_PROCESSING.getShortName());
		Label heaPrg  = new Label(ItemAttribute.CONCURRENT_PROGRAMS.getShortName());

		heaDev .getStyleClass().add("table-head");
//		heaAtt .getStyleClass().add("table-head");
//		heaSlz .getStyleClass().add("table-head");
		heaFir .getStyleClass().add("table-head");
		heaPro .getStyleClass().add("table-head");
		heaPrg .getStyleClass().add("table-head");

		heaDev .setMaxWidth(Double.MAX_VALUE);
//		heaAtt .setMaxWidth(Double.MAX_VALUE);
//		heaSlz .setMaxWidth(Double.MAX_VALUE);
		heaFir .setMaxWidth(Double.MAX_VALUE);
		heaPro .setMaxWidth(Double.MAX_VALUE);
		heaPrg .setMaxWidth(Double.MAX_VALUE);

		heaDev .setAlignment(Pos.CENTER);
//		heaAtt .setAlignment(Pos.CENTER);
//		heaSlz .setAlignment(Pos.CENTER);
		heaFir .setAlignment(Pos.CENTER);
		heaPro .setAlignment(Pos.CENTER);
		heaPrg .setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		//		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.add(heaDev , COL_DEV , 0);
//		grid.add(heaAtt , COL_ATT , 0);
//		grid.add(heaSlz , COL_SLZ , 0);
		grid.add(heaFir , COL_FIR , 0);
		grid.add(heaPro , COL_PRO , 0);
		grid.add(heaPrg , COL_PRG , 0);

		// Data
		Label lblDev = new Label(item.getDeviceRating()+"");
//		Label lblAtt = new Label();
//		Label lblSlz = new Label();
		Label lblFir = new Label();
		Label lblPro = new Label();
		Label lblPrg = new Label();
		grid.add(lblDev, COL_DEV , 1);
//		grid.add(lblAtt, COL_ATT , 1);
//		grid.add(lblSlz, COL_SLZ , 1);
		grid.add(lblFir, COL_FIR , 1);
		grid.add(lblPro, COL_PRO , 1);
		grid.add(lblPrg, COL_PRG , 1);
		GridPane.setHalignment(heaDev, HPos.CENTER);
		
		if (item.getSubtype()==ItemSubType.COMMLINK) {
			lblFir.setText(""+item.getDeviceRating());
			lblPro.setText(""+item.getDeviceRating());
			lblPrg.setText("1");
		}

		return grid;
	}

	//-------------------------------------------------------------------
	private static GridPane getCyberdeckNode(ItemTemplate item) {
		int COL_DEV  = 0;
		int COL_ATT  = 1;
		int COL_PRO  = 2;

		Label heaDev  = new Label(ItemAttribute.DEVICE_RATING.getShortName());
		Label heaAtt  = new Label(ItemAttribute.CYBER_ATTRIBUTES.getShortName());
		Label heaPro  = new Label(ItemAttribute.CONCURRENT_PROGRAMS.getShortName());

		heaDev .getStyleClass().add("table-head");
		heaAtt .getStyleClass().add("table-head");
		heaPro .getStyleClass().add("table-head");

		heaDev .setMaxWidth(Double.MAX_VALUE);
		heaAtt .setMaxWidth(Double.MAX_VALUE);
		heaPro .setMaxWidth(Double.MAX_VALUE);

		heaDev .setAlignment(Pos.CENTER);
		heaAtt .setAlignment(Pos.CENTER);
		heaPro .setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		//		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.add(heaDev , COL_DEV , 0);
		grid.add(heaAtt , COL_ATT , 0);
		grid.add(heaPro , COL_PRO , 0);

		// Data
		Label lblDev = new Label(item.getDeviceRating()+"");
		Label lblAtt = new Label();
		Label lblPro = new Label();
		lblDev .setMaxWidth(Double.MAX_VALUE);
		lblAtt .setMaxWidth(Double.MAX_VALUE);
		lblPro .setMaxWidth(Double.MAX_VALUE);
		lblDev .setAlignment(Pos.CENTER);
		lblAtt .setAlignment(Pos.CENTER);
		lblPro .setAlignment(Pos.CENTER);
		grid.add(lblDev, COL_DEV , 1);
		grid.add(lblAtt, COL_ATT , 1);
		grid.add(lblPro, COL_PRO , 1);
		GridPane.setHalignment(heaDev, HPos.CENTER);
		
		if (item.getCyberdeckData()!=null) {
			StringBuffer buf = new StringBuffer();
			for (int i=0; i<item.getCyberdeckData().getArray().length; i++)
				buf.append(item.getCyberdeckData().getArray()[i]+" ");
			lblAtt.setText(buf.toString());
			lblPro.setText(""+item.getCyberdeckData().getPrograms());
		}

		return grid;
	}

	//-------------------------------------------------------------------
	public static Node getItemInfoNode(CarriedItem item, EquipmentController ctrl) {
		ShadowrunCharacter model = ctrl.getModel();

		VBox box = new VBox(10);
		box.setStyle("-fx-spacing:0.5em; ");
		box.setMaxWidth(Double.MAX_VALUE);

		//		// Rating
		//		if (item.getItem().hasRating()) {
		//			Label lbRat = new Label(UI.getString("label.rating"));
		//			ChoiceBox<Integer> cbRating = new ChoiceBox<Integer>();
		//			for (int i=1; i<=item.getItem().getMaximumRating(); i++) {
		//				if (ctrl.canBuyLevel(item, i))
		//					cbRating.getItems().add(i);
		//			}
		//			cbRating.getSelectionModel().select( (Integer)item.getRating() );
		//			HBox line = new HBox(5);
		//			line.getChildren().addAll(lbRat, cbRating);
		//			box.getChildren().add(line);
		//			
		//			cbRating.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
		//				logger.debug("Change rating of "+item+" from "+o+" to "+n);
		//				ctrl.changeRating(item, n);
		//			});
		//		}


		try {
			switch (item.getItem().getType()) {
			case ARMOR:
				box.getChildren().add(getArmorNode(item,model));
				break;
			case WEAPON:
				box.getChildren().add(getWeaponNode(item,model));
				break;
			case BIOWARE:
			case CYBERWARE:
				box.getChildren().add(getAugmentationNode(item, ctrl));
				break;
			case VEHICLES:
				box.getChildren().add(getVehicleNode(item,model));
				break;
			default:
				logger.debug("No special handling for "+item.getItem().getType());
			}
		} catch (Exception e) {
			box.getChildren().add(new Label("ERROR: "+e));
			logger.error("Failed getting item data",e);
		}

		/*
		 * Accessories
		 */
		//		logger.warn(item.dump());
		//		logger.debug("Accessories of "+item+" are "+item.getAccessories());
		if (!item.getAccessories().isEmpty()) {
			List<String> accessNames = new ArrayList<String>();
			item.getAccessories().forEach(sub -> accessNames.add(sub.getName()));
			//			lblAccessories.setText(String.join(", ", item.getAccessories()));
			Label heaModif = new Label(UI.getString("label.accessory")+": ");
			heaModif.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-weight: bold;");

			FlowPane flow = new FlowPane();
			flow.getChildren().add(heaModif);
			Iterator<String> it = accessNames.iterator();
			while (it.hasNext()) {
				Label lbl = new Label(it.next());
				if (it.hasNext())
					lbl.setText(lbl.getText()+",  ");
				flow.getChildren().add(lbl);
			}
			box.getChildren().add(flow);
			VBox.setMargin(flow, new Insets(5, 0, 5, 0));
			//			layout.getChildren().add(lblAccessories);
			//			lblAccessories.setText(UI.getString("label.accessory")+": "+String.join(", ", accessNames));
			//			VBox.setMargin(lblAccessories, new Insets(5, 0, 5, 0));
		}

		/*
		 * WiFi Advantages
		 */
		if (!item.getWiFiAdvantageStringRecursivly().isEmpty()) {
			box.getChildren().add(getWiFiAdvantagesNode(item));
		}

		/*
		 * Modifications
		 */
		List<Modification> mods = item.getCharacterModifications();
		if (!mods.isEmpty()) {
			box.getChildren().add(getModificationsNode(item));
		}

		return box;
	}

	//-------------------------------------------------------------------
	private static Node getWiFiAdvantagesNode(CarriedItem item) {
		Label heaWifi = new Label(UI.getString("label.wifiadvantage")+": ");
		heaWifi.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-weight: bold;");

		Label lblWifi = new Label(String.join(",\n", item.getWiFiAdvantageStringRecursivly()));
		lblWifi.setWrapText(true);
//		lblWifi.setStyle("-fx-max-width: 24em");

		VBox flow = new VBox();
		flow.getChildren().addAll(heaWifi, lblWifi);
		VBox.setMargin(flow, new Insets(5, 0, 5, 0));
		return flow;
	}

	//-------------------------------------------------------------------
	private static Node getModificationsNode(CarriedItem item) {
		Label heading = new Label(UI.getString("label.modifications")+": ");
		heading.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-weight: bold;");

		List<String> ret = new ArrayList<>();
		for (Modification mod : item.getCharacterModifications())
			ret.add(ShadowrunTools.getModificationString(mod));
		Label lblWifi = new Label(String.join(",\n", ret));
		lblWifi.setWrapText(true);
		lblWifi.setStyle("-fx-max-width: 24em");

		VBox flow = new VBox();
		flow.getChildren().addAll(heading, lblWifi);
		VBox.setMargin(flow, new Insets(5, 0, 5, 0));
		return flow;
	}


	//-------------------------------------------------------------------
	private static Node getWeaponNode(CarriedItem item, ShadowrunCharacter model) {
		VBox layout = new VBox();
		layout.setStyle("-fx-spacing: 0.5em");
		layout.setMaxWidth(Double.MAX_VALUE);

		int COL_ACCU = 0;
		int COL_DMG  = 1;
		int COL_AP   = 2;
		int COL_REA  = 3;
		int COL_MODE = 4;
		int COL_RC   = 5;
		int COL_AMMO = 6;
		int COL_SKIL = 7;
		int COL_POOL = 8;

		Label heaAcc  = new Label(ItemAttribute.ACCURACY.getShortName());
		Label heaDmg  = new Label(ItemAttribute.DAMAGE.getShortName());
		Label heaAP   = new Label(ItemAttribute.ARMOR_PENETRATION.getShortName());
		Label heaRea  = new Label(ItemAttribute.REACH.getShortName());
		Label heaMode = new Label(ItemAttribute.MODE.getShortName());
		Label heaRC   = new Label(ItemAttribute.RECOIL_COMPENSATION.getShortName());
		Label heaAmmo = new Label(ItemAttribute.AMMUNITION.getShortName());
		Label heaSkil = new Label(UI.getString("label.skill"));
		Label heaPool = new Label(UI.getString("label.pool"));

		heaAcc .getStyleClass().add("table-head");
		heaDmg .getStyleClass().add("table-head");
		heaAP  .getStyleClass().add("table-head");
		heaRea .getStyleClass().add("table-head");
		heaMode.getStyleClass().add("table-head");
		heaRC  .getStyleClass().add("table-head");
		heaAmmo.getStyleClass().add("table-head");
		heaSkil.getStyleClass().add("table-head");
		heaPool.getStyleClass().add("table-head");

		heaAcc .setMaxWidth(Double.MAX_VALUE);
		heaDmg .setMaxWidth(Double.MAX_VALUE);
		heaAP  .setMaxWidth(Double.MAX_VALUE);
		heaRea .setMaxWidth(Double.MAX_VALUE);
		heaMode.setMaxWidth(Double.MAX_VALUE);
		heaRC  .setMaxWidth(Double.MAX_VALUE);
		heaAmmo.setMaxWidth(Double.MAX_VALUE);
		heaSkil.setMaxWidth(Double.MAX_VALUE);
		heaPool.setMaxWidth(Double.MAX_VALUE);

		heaAcc .setAlignment(Pos.CENTER);
		heaDmg .setAlignment(Pos.CENTER);
		heaAP  .setAlignment(Pos.CENTER);
		heaRea .setAlignment(Pos.CENTER);
		heaMode.setAlignment(Pos.CENTER);
		heaRC  .setAlignment(Pos.CENTER);
		heaAmmo.setAlignment(Pos.CENTER);
		heaSkil.setAlignment(Pos.CENTER);
		heaPool.setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.getColumnConstraints().add(new ColumnConstraints( 70)); // Damage
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Armor Penetration
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Reach
		grid.getColumnConstraints().add(new ColumnConstraints(100)); // Mode
		grid.getColumnConstraints().add(new ColumnConstraints( 30)); // Recoil compensation
		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Ammo
		grid.getColumnConstraints().add(new ColumnConstraints(250)); // Skill
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Pool
		grid.add(heaAcc , COL_ACCU, 0);
		grid.add(heaDmg , COL_DMG , 0);
		grid.add(heaAP  , COL_AP  , 0);
		grid.add(heaRea , COL_REA , 0);
		grid.add(heaMode, COL_MODE, 0);
		grid.add(heaRC  , COL_RC  , 0);
		grid.add(heaAmmo, COL_AMMO, 0);
		grid.add(heaSkil , COL_SKIL, 0);
		grid.add(heaPool, COL_POOL, 0);

		Label lblAcc = new Label(item.getAsValue(ItemAttribute.ACCURACY).toString());
		Label lblDmg = new Label(item.getAsValue(ItemAttribute.DAMAGE).toString());
		Label lblAP  = new Label(item.getAsValue(ItemAttribute.ARMOR_PENETRATION).toString());
		Label lblRea = new Label(item.getAsValue(ItemAttribute.REACH).toString());
		Label lblMod = new Label(item.getAsObject(ItemAttribute.MODE).toString());
		Label lblRC  = new Label(item.getAsValue(ItemAttribute.RECOIL_COMPENSATION).toString());
		Label lblAmm = new Label(item.getAsObject(ItemAttribute.AMMUNITION).toString());
		Label lblSkil = new Label(item.getItem().getWeaponData().getSkill().getName());
		if (item.getItem().getWeaponData().getSpecialization()!=null)
			lblSkil.setText(lblSkil.getText()+"/"+item.getItem().getWeaponData().getSpecialization().getName());
		Label lblPool = new Label(String.valueOf(ShadowrunTools.getWeaponPool(model, item)));
		grid.add(lblAcc, COL_ACCU, 1);
		grid.add(lblDmg, COL_DMG , 1);
		grid.add(lblAP , COL_AP  , 1);
		grid.add(lblRea, COL_REA , 1);
		grid.add(lblMod, COL_MODE, 1);
		grid.add(lblRC , COL_RC  , 1);
		grid.add(lblAmm, COL_AMMO, 1);
		grid.add(lblSkil, COL_SKIL, 1);
		grid.add(lblPool, COL_POOL, 1);
		GridPane.setHalignment(lblAcc, HPos.CENTER);
		GridPane.setHalignment(lblDmg, HPos.CENTER);
		GridPane.setHalignment(lblAP , HPos.CENTER);
		GridPane.setHalignment(lblRea, HPos.CENTER);
		GridPane.setHalignment(lblMod, HPos.CENTER);
		GridPane.setHalignment(lblRC , HPos.CENTER);
		GridPane.setHalignment(lblAmm, HPos.CENTER);
		GridPane.setHalignment(lblSkil, HPos.LEFT);
		GridPane.setHalignment(lblPool, HPos.CENTER);

		layout.getChildren().add(grid);

		//		/*
		//		 * Accessories
		//		 */
		//		if (!item.getAccessories().isEmpty()) {
		//			List<String> accessNames = new ArrayList<String>();
		//			item.getAccessories().forEach(sub -> accessNames.add(sub.getName()));
		////			lblAccessories.setText(String.join(", ", item.getAccessories()));
		//			Label heaWifi = new Label(UI.getString("label.accessory")+": ");
		//			heaWifi.setStyle("-fx-text-fill: textcolor-highlight-primary; -fx-font-weight: bold;");
		//			
		//			FlowPane flow = new FlowPane();
		//			flow.getChildren().add(heaWifi);
		//			Iterator<String> it = accessNames.iterator();
		//			while (it.hasNext()) {
		//				Label lbl = new Label(it.next());
		//				if (it.hasNext())
		//					lbl.setText(lbl.getText()+",  ");
		//				flow.getChildren().add(lbl);
		//			}
		//			layout.getChildren().add(flow);
		//			VBox.setMargin(flow, new Insets(5, 0, 5, 0));
		////			layout.getChildren().add(lblAccessories);
		////			lblAccessories.setText(UI.getString("label.accessory")+": "+String.join(", ", accessNames));
		////			VBox.setMargin(lblAccessories, new Insets(5, 0, 5, 0));
		//		}
		return layout;
	}

	//-------------------------------------------------------------------
	private static Node getArmorNode(CarriedItem item, ShadowrunCharacter model) {
		VBox layout = new VBox();
		layout.setStyle("-fx-spacing: 0.5em");
		layout.setMaxWidth(Double.MAX_VALUE);

		int COL_ARM  = 0;

		Label heaArm  = new Label(ItemAttribute.ARMOR.getName());

		heaArm .getStyleClass().add("table-head");

		heaArm .setMaxWidth(Double.MAX_VALUE);

		heaArm .setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		//		grid.getColumnConstraints().add(new ColumnConstraints( 50)); // Accuracy
		grid.add(heaArm , COL_ARM, 0);

		Label lblArm = new Label(item.getItem().getArmorData().addsToMain()?"+":"" + item.getAsValue(ItemAttribute.ARMOR).toString());
		grid.add(lblArm, COL_ARM, 1);
		GridPane.setHalignment(lblArm, HPos.CENTER);

		layout.getChildren().add(grid);

		return layout;
	}

	//-------------------------------------------------------------------
	private static Node getAugmentationNode(CarriedItem item, EquipmentController ctrl) {
		Label lbQuality = new Label(UI.getString("label.quality")+": ");
		ChoiceBox<BodytechQuality> cbQuality = new ChoiceBox<BodytechQuality>();
		cbQuality.getItems().addAll(BodytechQuality.values());
		cbQuality.setConverter(new StringConverter<BodytechQuality>() {
			public String toString(BodytechQuality data) { return data.getName(); }
			public BodytechQuality fromString(String string) { return null; }
		});
		cbQuality.setValue(item.getQuality());
		cbQuality.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.debug("try to change quality to "+n);
			ctrl.changeQuality(item, n);
		});
		//		Label cbQuality = new Label(item.getQuality().getName());
		HBox line = new HBox(5);
		line.getChildren().addAll(lbQuality, cbQuality);
		return line;
	}

	//-------------------------------------------------------------------
	private static Node getVehicleNode(CarriedItem item, ShadowrunCharacter model) {
		if (item==null)
			throw new NullPointerException("Empty item");
		VBox layout = new VBox();
		layout.setStyle("-fx-spacing: 0.5em");
		layout.setMaxWidth(Double.MAX_VALUE);

		int COL_HAND = 0;
		int COL_SPED = 1;
		int COL_ACCL = 2;
		int COL_REA  = 3;
		int COL_MODE = 4;
		int COL_RC   = 5;
		int COL_AMMO = 6;
		int COL_SEAT = 7;
		int COL_SKIL = 8;
		int COL_POOL = 9;

		Label heaAcc  = new Label(ItemAttribute.HANDLING.getShortName());
		Label heaDmg  = new Label(ItemAttribute.SPEED.getShortName());
		Label heaAP   = new Label(ItemAttribute.ACCELERATION.getShortName());
		Label heaRea  = new Label(ItemAttribute.BODY.getShortName());
		Label heaMode = new Label(ItemAttribute.ARMOR.getShortName());
		Label heaRC   = new Label(ItemAttribute.PILOT.getShortName());
		Label heaAmmo = new Label(ItemAttribute.SENSORS.getShortName());
		Label heaSeat = new Label(ItemAttribute.SEATS.getShortName());
		Label heaSkil = new Label(UI.getString("label.skill"));
		Label heaPool = new Label(UI.getString("label.pool"));

		heaAcc .getStyleClass().add("table-head");
		heaDmg .getStyleClass().add("table-head");
		heaAP  .getStyleClass().add("table-head");
		heaRea .getStyleClass().add("table-head");
		heaMode.getStyleClass().add("table-head");
		heaRC  .getStyleClass().add("table-head");
		heaAmmo.getStyleClass().add("table-head");
		heaSeat.getStyleClass().add("table-head");
		heaSkil.getStyleClass().add("table-head");
		heaPool.getStyleClass().add("table-head");

		heaAcc .setMaxWidth(Double.MAX_VALUE);
		heaDmg .setMaxWidth(Double.MAX_VALUE);
		heaAP  .setMaxWidth(Double.MAX_VALUE);
		heaRea .setMaxWidth(Double.MAX_VALUE);
		heaMode.setMaxWidth(Double.MAX_VALUE);
		heaRC  .setMaxWidth(Double.MAX_VALUE);
		heaAmmo.setMaxWidth(Double.MAX_VALUE);
		heaSeat.setMaxWidth(Double.MAX_VALUE);
		heaSkil.setMaxWidth(Double.MAX_VALUE);
		heaPool.setMaxWidth(Double.MAX_VALUE);

		heaAcc .setAlignment(Pos.CENTER);
		heaDmg .setAlignment(Pos.CENTER);
		heaAP  .setAlignment(Pos.CENTER);
		heaRea .setAlignment(Pos.CENTER);
		heaMode.setAlignment(Pos.CENTER);
		heaRC  .setAlignment(Pos.CENTER);
		heaAmmo.setAlignment(Pos.CENTER);
		heaSeat.setAlignment(Pos.CENTER);
		heaSkil.setAlignment(Pos.CENTER);
		heaPool.setAlignment(Pos.CENTER);

		GridPane grid = new GridPane();
		grid.getColumnConstraints().add(new ColumnConstraints( 70)); // Handling
		grid.getColumnConstraints().add(new ColumnConstraints( 70)); // Speed
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Acceleraton
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Body
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Armor
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Pilot
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Sensor
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Seats
		grid.getColumnConstraints().add(new ColumnConstraints(250)); // Skill
		grid.getColumnConstraints().add(new ColumnConstraints( 40)); // Pool
		grid.add(heaAcc , COL_HAND, 0);
		grid.add(heaDmg , COL_SPED , 0);
		grid.add(heaAP  , COL_ACCL  , 0);
		grid.add(heaRea , COL_REA , 0);
		grid.add(heaMode, COL_MODE, 0);
		grid.add(heaRC  , COL_RC  , 0);
		grid.add(heaAmmo, COL_AMMO, 0);
		grid.add(heaSeat, COL_SEAT, 0);
		grid.add(heaSkil, COL_SKIL, 0);
		grid.add(heaPool, COL_POOL, 0);

		Label lblAcc = new Label(item.getAsObject(ItemAttribute.HANDLING).toString());
		Label lblDmg = new Label(item.getAsObject(ItemAttribute.SPEED).toString());
		Label lblAP  = new Label(item.getAsValue(ItemAttribute.ACCELERATION).toString());
		Label lblRea = new Label(item.getAsValue(ItemAttribute.BODY).toString());
		Label lblMod = new Label(item.getAsValue(ItemAttribute.ARMOR).toString());
		Label lblRC  = new Label(item.getAsValue(ItemAttribute.PILOT).toString());
		Label lblAmm = new Label(item.getAsValue(ItemAttribute.SENSORS).toString());
		Label lblSea = new Label(item.getAsValue(ItemAttribute.SEATS).toString());
		Skill skill = ShadowrunTools.getSkillForVehicle(item.getItem());
		Label lblSkil = new Label( (skill!=null)?skill.getName():"Skill not set"); //item.getItem().getVehicleData().getSkill().getName());
		//		if (item.getItem().getVehicleData().getSpecialization()!=null)
		//			lblSkil.setText(lblSkil.getText()+"/"+item.getItem().getWeaponData().getSpecialization().getName());
		Label lblPool = new Label( (skill!=null)?(String.valueOf(model.getSkillPool(skill))):"TODO"); //String.valueOf(ShadowrunTools.getWeaponPool(model, item)));
		logger.warn("Which skill is required for vehicle?");
		grid.add(lblAcc, COL_HAND, 1);
		grid.add(lblDmg, COL_SPED , 1);
		grid.add(lblAP , COL_ACCL  , 1);
		grid.add(lblRea, COL_REA , 1);
		grid.add(lblMod, COL_MODE, 1);
		grid.add(lblRC , COL_RC  , 1);
		grid.add(lblAmm, COL_AMMO, 1);
		grid.add(lblSea, COL_SEAT, 1);
		grid.add(lblSkil, COL_SKIL, 1);
		grid.add(lblPool, COL_POOL, 1);
		GridPane.setHalignment(lblAcc, HPos.CENTER);
		GridPane.setHalignment(lblDmg, HPos.CENTER);
		GridPane.setHalignment(lblAP , HPos.CENTER);
		GridPane.setHalignment(lblRea, HPos.CENTER);
		GridPane.setHalignment(lblMod, HPos.CENTER);
		GridPane.setHalignment(lblRC , HPos.CENTER);
		GridPane.setHalignment(lblAmm, HPos.CENTER);
		GridPane.setHalignment(lblSea, HPos.CENTER);
		GridPane.setHalignment(lblSkil, HPos.LEFT);
		GridPane.setHalignment(lblPool, HPos.CENTER);

		layout.getChildren().add(grid);
		return layout;
	}

	//-------------------------------------------------------------------
	public static String getShortItemModificationString(Collection<Modification> list) {
		List<String> ret = new ArrayList<String>();
		for (Modification mod : list) {
			if (mod instanceof ItemAttributeModification) {
				ItemAttributeModification iaMod = (ItemAttributeModification)mod;
				if (iaMod.getValue()<0)
					ret.add( iaMod.getAttribute().getShortName()+""+iaMod.getValue());
				else
					ret.add( iaMod.getAttribute().getShortName()+"+"+iaMod.getValue());
			} else
				logger.error("No string output for "+mod.getClass());
		}

		return String.join(", ", ret);
	}

	//-------------------------------------------------------------------
	public static SelectionOption[] askOptionsFor(ScreenManagerProvider provider, EquipmentController control, ItemTemplate data) {
		List<SelectionOptionType> options = control.getOptions(data);
		logger.debug("ask options for "+data+": "+options);

		GridPane content = new GridPane();
		content.setStyle("-fx-vgap: 1em; -fx-hgap: 1em");
		Label lblPrice = new Label(data.getPrice()+"\u00A5");
		lblPrice.setMaxHeight(Double.MAX_VALUE);
		lblPrice.setAlignment(Pos.CENTER_LEFT);
		lblPrice.setStyle("-fx-font-size: 200%");
		content.add(lblPrice , 2, 0, 1,2);

		List<SelectionOption> selectedOptions = new ArrayList<SelectionOption>();

		if (options.contains(SelectionOptionType.RATING)) {
			final EquipmentController.SelectionOption opt1 = new SelectionOption(SelectionOptionType.RATING, 1);
			selectedOptions.add(opt1);
			ChoiceBox<Integer> cbRate = new ChoiceBox<>();
			cbRate.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
				logger.debug("Rating changed to "+n);
				opt1.setValue(n);
				SelectionOption[] foo = new SelectionOption[selectedOptions.size()];
				foo = selectedOptions.toArray(foo);
				lblPrice.setText("\u00A5"+control.getCost(data, foo));
			});
			// Add components to question dialog
			Label heaRate = new Label(UI.getString("label.rating"));
			logger.debug("  maximum rating of "+data+" is "+data.getMaximumRating());
			for (int i=1; i<=data.getMaximumRating(); i++)
				cbRate.getItems().add(i);
			content.add(heaRate, 0, 0);
			content.add(cbRate , 1, 0);
		}

		if (options.contains(SelectionOptionType.BODYTECH_QUALITY)) {
			final EquipmentController.SelectionOption opt2 = new SelectionOption(SelectionOptionType.BODYTECH_QUALITY, BodytechQuality.STANDARD);
			selectedOptions.add(opt2);
			ChoiceBox<BodytechQuality> cbQual = new ChoiceBox<>();
			cbQual.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
				logger.debug("Quality changed to "+n);
				opt2.setValue(n);
				SelectionOption[] foo = new SelectionOption[selectedOptions.size()];
				foo = selectedOptions.toArray(foo);
				lblPrice.setText("\u00A5"+control.getCost(data, foo));
			});
			// Add components to question dialog
			Label heaQual = new Label(UI.getString("label.quality"));
			cbQual.getItems().addAll(BodytechQuality.values());
			cbQual.setConverter(new StringConverter<BodytechQuality>() {
				public String toString(BodytechQuality data) { return data.getName(); }
				public BodytechQuality fromString(String string) { return null; }
			});
			content.add(heaQual, 0, 1);
			content.add(cbQual , 1, 1);
		}

		if (!selectedOptions.isEmpty()) {
			provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, "Stufe", content);
			SelectionOption[] foo = new SelectionOption[selectedOptions.size()];
			foo = selectedOptions.toArray(foo);
			return foo;
		} else {
			return new SelectionOption[0];
		}
	}

}

