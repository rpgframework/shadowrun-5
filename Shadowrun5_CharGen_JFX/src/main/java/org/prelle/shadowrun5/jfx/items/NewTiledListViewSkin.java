/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.collections.ListChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.TilePane;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class NewTiledListViewSkin<T> extends SkinBase<ListView<T>> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private TilePane flow;
	private ScrollPane scroll;

	//-------------------------------------------------------------------
	protected NewTiledListViewSkin(ListView<T> control) {
		super(control);
		initComponents();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		flow = new TilePane(getSkinnable().getOrientation());
		if (getSkinnable().getOrientation()==Orientation.HORIZONTAL)
			flow.setPrefRows(3);
		else
			flow.setPrefColumns(3);

		scroll = new ScrollPane(flow);

        getChildren().add(flow);
	}

	//-------------------------------------------------------------------
	private void updateChildren() {
		flow.getChildren().clear();

		int index=0;
		for (T item : getSkinnable().getItems()) {
			Callback<ListView<T>, ListCell<T>> factory = getSkinnable().getCellFactory();
			if (factory!=null) {
				ListCell<T> cell = factory.call(getSkinnable());
				cell.updateListView(getSkinnable());
				logger.warn("JAVA9: did not set ListCellSkin");
//				cell.setSkin(new ListCellSkin<T>(cell));
				cell.updateIndex(index++);
				cell.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
				logger.debug("Add cell "+cell.getGraphic()+" to TilePane");
				flow.getChildren().add(cell); 
			} else {
				Label cell = new Label(String.valueOf(item));
				flow.getChildren().add(cell);
			}
		}
		
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().itemsProperty().get().addListener(new ListChangeListener<T>(){

			@Override
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends T> c) {
				logger.debug("List changed "+c);
				updateChildren();
			}});
	}

}
