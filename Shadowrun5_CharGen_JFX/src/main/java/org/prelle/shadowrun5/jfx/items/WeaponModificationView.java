/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import org.prelle.shadowrun5.items.ItemTemplate;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.ListCell;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WeaponModificationView extends Control {

	private ObservableList<ItemTemplate> modifications;
	private Callback<WeaponModificationView, ListCell<ItemTemplate>> cellFactory;

	//--------------------------------------------------------------------
	public WeaponModificationView() {
		modifications = FXCollections.observableArrayList();

		setSkin(new WeaponModificationViewSkin(this));
	}

	//--------------------------------------------------------------------
	public ObservableList<ItemTemplate> getItems() {
		return modifications;
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

}
