/**
 *
 */
package org.prelle.shadowrun5.jfx.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.collections.ListChangeListener;
import javafx.geometry.Orientation;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.TilePane;

/**
 * @author Stefan
 *
 */
public class WeaponModificationViewSkin extends
		SkinBase<WeaponModificationView> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private TilePane flow;

	//--------------------------------------------------------------------
	/**
	 * @param arg0
	 */
	public WeaponModificationViewSkin(WeaponModificationView ctrl) {
		super(ctrl);
		flow = new TilePane(Orientation.HORIZONTAL);
		flow.setPrefColumns(3);

		getSkinnable().impl_getChildren().add(flow);

		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getSkinnable().getItems().addListener(new ListChangeListener<ItemTemplate>(){
			public void onChanged(
					javafx.collections.ListChangeListener.Change<? extends ItemTemplate> change) {
				logger.debug("Handle "+change);
				if (change.wasAdded()) {

				}

			}});
	}

	//--------------------------------------------------------------------
	private void refresh() {
		flow.getChildren().clear();
		
		
	}


}
