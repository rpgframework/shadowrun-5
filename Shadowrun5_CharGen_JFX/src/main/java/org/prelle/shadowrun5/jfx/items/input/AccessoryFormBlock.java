package org.prelle.shadowrun5.jfx.items.input;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun5.requirements.ItemHookRequirement;
import org.prelle.shadowrun5.requirements.Requirement;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;

import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class AccessoryFormBlock implements FormBlock {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ChoiceBox<ItemHook> cbHook;
	private TextField tfSize;
	private VBox layout;
	
	//-------------------------------------------------------------------
	public AccessoryFormBlock() {
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbHook = new ChoiceBox<ItemHook>();
		cbHook.getItems().addAll(ItemHook.values());
		cbHook.getItems().remove(ItemHook.ARMOR_ADDITION);
		cbHook.setConverter(new StringConverter<ItemHook>() {
			public ItemHook fromString(String value) { return null;	}
			public String toString(ItemHook value) { return value.getName(); }
		});
		tfSize = new TextField();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.accessory.title"));
		Label lblChoice   = new Label(UI.getString("label.hooktype"));
		Label lblSize   = new Label(UI.getString("label.size"));
		lblTitle.getStyleClass().add("text-subheader");
		
		VBox line = new VBox();
		line.getChildren().addAll(lblChoice, cbHook, lblSize, tfSize);
		line.setStyle("-fx-spacing: 0.5em");
		
		layout = new VBox(lblTitle, line);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		ItemHookRequirement req = null;
		for (Requirement tmp : item.getRequirements()) {
			if (tmp instanceof ItemHookRequirement) {
				req = (ItemHookRequirement) tmp;
				break;
			}
		}			

		ItemHook slot = cbHook.getValue();
		int cap = 0;
		if (tfSize.getText()!=null)
			cap = Integer.parseInt(tfSize.getText());
		if (slot==null) {
			// Remove requirement, if exists
			if (req!=null)
				item.getRequirements().remove(req);
		} else {
			if (req==null) {
				req = new ItemHookRequirement(slot, cap);
				item.getRequirements().add(req);
			} else {
				req.setSlot(slot);
				req.setCapacity(cap);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		int      size = 0;
		ItemHook slot = null;
		for (Requirement req : item.getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				slot = ((ItemHookRequirement)req).getSlot();
				size = ((ItemHookRequirement)req).getCapacity();
			}
		}
		if (slot==null) {
			cbHook.getSelectionModel().clearSelection();
			tfSize.setText(null);
		} else {
			cbHook.getSelectionModel().select(slot);
			tfSize.setText(String.valueOf(size));
		}
		
//		cbHasRating.setSelected(item.hasRating());
//		List<Multiply> mult = Arrays.asList(item.getMultiplyWithRate());
//		cbRateAvailability.setSelected(mult.contains(Multiply.AVAIL));
//		cbRateCapacity    .setSelected(mult.contains(Multiply.CAPACITY));
//		cbRateDevice      .setSelected(mult.contains(Multiply.DEVICE));
//		cbRatePrice       .setSelected(mult.contains(Multiply.PRICE));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		return true;
	}

}