package org.prelle.shadowrun5.jfx.items.input;

import java.util.PropertyResourceBundle;

import org.prelle.shadowrun5.items.BodytechData;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;

import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class AugmentationFormBlock implements FormBlock {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private TextField tfEsse;
	private VBox layout;
	
	//-------------------------------------------------------------------
	public AugmentationFormBlock() {
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		tfEsse = new TextField();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.augmentation.title"));
		Label lblEsse  = new Label(ItemAttribute.ESSENCECOST.getName());
		lblTitle.getStyleClass().add("text-subheader");
		
		HBox line = new HBox();
		line.getChildren().addAll(lblEsse, tfEsse);
		line.setStyle("-fx-spacing: 0.5em");
		
		layout = new VBox(lblTitle, line);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		if (tfEsse.getText().isEmpty()) {
			item.setBodyTech(null);
		} else {
			BodytechData toSet = new BodytechData();
			toSet.setEssence(Float.parseFloat(tfEsse.getText()));
			item.setBodyTech(toSet);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		if (item.getBodyTech()==null) {
			tfEsse.setText(null);
		} else {
			tfEsse.setText(String.valueOf(item.getBodyTech().getEssence()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		boolean isOkay = true;

		// Essence
		try {
			Float.parseFloat(tfEsse.getText());
			tfEsse.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfEsse.getStyleClass().contains(ERROR_CLASS))
				tfEsse.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}
		return isOkay;
	}

}