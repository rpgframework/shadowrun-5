/**
 * 
 */
package org.prelle.shadowrun5.jfx.items.input;

import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun5.ShadowrunCustomDataCore;
import org.prelle.shadowrun5.WriteablePropertyResourceBundle;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyIntegerWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ItemInputTab extends Tab {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private ScreenManager manager;
	private ChoiceBox<ItemType> cbType;
	private ChoiceBox<ItemSubType> cbSubType;
	private TableView<ItemTemplate> table;
	private TableColumn<ItemTemplate, String> colName;
	private TableColumn<ItemTemplate, String> colAvail;
	private TableColumn<ItemTemplate, Number> colPrice;
	private TableColumn<ItemTemplate, String> colAction;
	private TableColumn<ItemTemplate, Boolean> colCheck;
	private Button btnAdd;

	//-------------------------------------------------------------------
	public ItemInputTab() {
		super(UI.getString("tab.input.items.title"));
		
		initComponents();
		initColumns();
		initFactories();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(ItemType.values());
		cbType.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType object) { return object.getName(); }
			public ItemType fromString(String string) { return null; }
		});
		cbSubType = new ChoiceBox<>();
		cbSubType.getItems().addAll(ItemSubType.values());
		cbSubType.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType object) { return object.getName(); }
			public ItemSubType fromString(String string) { return null; }
		});
		table = new TableView<>();
		table.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		btnAdd = new Button(null, new FontIcon("\uE109"));
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		VBox bxChoices = new VBox();
		bxChoices.setStyle("-fx-spacing: 0.4em");
		bxChoices.getChildren().addAll(cbType, cbSubType);
		
		HBox bxLine = new HBox();
		bxLine.setStyle("-fx-spacing: 0.7em");
		bxLine.getChildren().addAll(bxChoices, btnAdd);
		bxLine.setAlignment(Pos.TOP_LEFT);
		HBox.setHgrow(bxChoices, Priority.ALWAYS);
		
		VBox layout = new VBox();
		layout.setStyle("-fx-spacing: 0.4em");
		
		layout.getChildren().addAll(bxLine, table);
		setContent(layout);
	}
	
	//-------------------------------------------------------------------
	private void initColumns() {
		colName  = new TableColumn<>(UI.getString("label.name"));
		colAvail = new TableColumn<>(ItemAttribute.AVAILABILITY.getName());
		colPrice = new TableColumn<>(ItemAttribute.PRICE.getName());
		colAction= new TableColumn<>(UI.getString("label.action"));
		colCheck = new TableColumn<>(UI.getString("label.checkResult"));
		
		
		table.getColumns().add(colName);
		table.getColumns().add(colAvail);
		table.getColumns().add(colPrice);
		table.getColumns().add(colAction);
		table.getColumns().add(colCheck);
	}
	
	//-------------------------------------------------------------------
	private void initFactories() {
		colName.setCellValueFactory( param -> { return new ReadOnlyStringWrapper(param.getValue().getName()); });
		colAvail.setCellValueFactory( param -> { return new ReadOnlyStringWrapper(param.getValue().getAvailability()+""); });
		colPrice.setCellValueFactory( param -> { return new ReadOnlyIntegerWrapper(param.getValue().getPrice()); });
		colAction.setCellValueFactory( param -> { return new ReadOnlyStringWrapper(param.getValue().getId()); });
		colCheck.setCellValueFactory( param -> {
			ItemTemplate item = param.getValue();
			boolean okay = item.getPage()!=0 && item.getHelpText()!=null && !item.getHelpText().isEmpty();
			return new ReadOnlyBooleanWrapper(okay); });
		
		colAction.setCellFactory(param -> new TableCell<ItemTemplate, String>() {
			public void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Button btnDel = new Button("\uE107");
					btnDel.setUserData(item);
					btnDel.setOnAction(event -> {
						String id = (String)((Button)event.getSource()).getUserData();
						ItemTemplate data = ShadowrunCustomDataCore.getItem(id);
						table.getItems().remove(data);
						ShadowrunCustomDataCore.removeItem(data);
						ShadowrunCustomDataCore.saveEquipment();
					});
					setGraphic(btnDel);
				}
			}
		});
		colCheck.setCellFactory(param -> new TableCell<ItemTemplate, Boolean>() {
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Label lbl = new Label(item?"Okay":"Problem");
					if (item) {
						lbl.setStyle("-fx-text-fill: green");
					} else {
						lbl.setStyle("-fx-text-fill: red");
					}
					setGraphic(lbl);
				}
			}
		});
		
		table.setRowFactory(tv -> {
			TableRow<ItemTemplate> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (event.getClickCount()==2) {
					editClicked(row.getItem());
				}
			});
			return row;
		});
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				cbSubType.getItems().clear();
				cbSubType.getItems().addAll(n.getSubTypes());
			}
		});
		
		cbSubType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				setData(ShadowrunCustomDataCore.getItems(cbType.getValue()));
			}
		});
		btnAdd.setOnAction(event -> addClicked());
		
		table.setOnMouseClicked(event -> {});
	}
	
	//-------------------------------------------------------------------
	public void setData(List<ItemTemplate> data) {
		table.getItems().clear();
		table.getItems().addAll(data);
	}
	
	//-------------------------------------------------------------------
	private void addClicked() {
		logger.debug("addClicked");
		ItemType type = cbType.getValue();
		ItemSubType subType = cbSubType.getValue();
		
		ItemDataForm form = new ItemDataForm(manager);
		form.preselectTypes(type, subType);
		
		NavigButtonControl buttonCtrl = new NavigButtonControl();
		buttonCtrl.setCallback( new Callback<CloseType, Boolean>() {
			public Boolean call(CloseType param) {
				logger.debug("Checking button "+param);
				if (param==CloseType.OK)
					return form.isInputOkay();
				return true;
			}
		});
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, 
				UI.getString("form.itemdata.title"), 
				form, buttonCtrl);
		logger.debug("Closed with "+result);
		if (result==CloseType.OK) {
			ItemTemplate item = new ItemTemplate();
			WriteablePropertyResourceBundle i18n = ShadowrunCustomDataCore.getI18nResources(ItemTemplate.class);
			WriteablePropertyResourceBundle i18nHelp = ShadowrunCustomDataCore.getI18nHelpResources(ItemTemplate.class);
			item.setResourceBundle(i18n);
			item.setHelpResourceBundle(i18nHelp);
			form.readInto(item);
			logger.debug("Read "+item);
			table.getItems().add(item);
			logger.debug("Bundle = "+i18n.dump());
			
			ShadowrunCustomDataCore.addItem(item);
			ShadowrunCustomDataCore.saveEquipment();
		}
	}
	
	//-------------------------------------------------------------------
	private void editClicked(ItemTemplate toEdit) {
		logger.debug("editClicked");
		ItemDataForm form = new ItemDataForm(manager);
		form.preselectTypes(toEdit.getType(), toEdit.getSubtype());
		form.fillFrom(toEdit);
		
		NavigButtonControl buttonCtrl = new NavigButtonControl();
		buttonCtrl.setCallback( new Callback<CloseType, Boolean>() {
			public Boolean call(CloseType param) {
				logger.debug("Checking button "+param);
				if (param==CloseType.OK)
					return form.isInputOkay();
				return true;
			}
		});
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, 
				UI.getString("form.itemdata.title"), 
				form, buttonCtrl);
		logger.debug("Closed with "+result);
		if (result==CloseType.OK) {
			form.readInto(toEdit);
			logger.debug("Read "+toEdit);
			
			ShadowrunCustomDataCore.saveEquipment();
		}
	}
	
	//-------------------------------------------------------------------
	public void setScreenManager(ScreenManager manager) {
		this.manager = manager;
	}

}
