package org.prelle.shadowrun5.jfx.items.input;

import java.util.Collections;
import java.util.Comparator;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormEventType;
import org.prelle.shadowrun5.modifications.AccessoryModification;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;
import org.prelle.shadowrun5.modifications.ItemHookModification;
import org.prelle.shadowrun5.modifications.SelectSkillModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class ModificationsBlock implements FormBlock {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private ItemDataForm parent;
	private ScreenManager manager;
	private ChoiceBox<Class<? extends Modification>> cbModType;
	private Button btnAdd;
	private ListView<Modification> list;
	private VBox     layout;
	
	//-------------------------------------------------------------------
	public ModificationsBlock(ItemDataForm parent, ScreenManager manager) {
		this.parent  = parent;
		this.manager = manager;
		initComponents();
		initLayout();
		initInteractivity();
		
		cbModType.getSelectionModel().select(0);
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbModType = new ChoiceBox<Class<? extends Modification>>();
		cbModType.getItems().add(ItemHookModification.class);
		cbModType.getItems().add(AccessoryModification.class);
		cbModType.getItems().add(AttributeModification.class);
		cbModType.getItems().add(SkillModification.class);
		cbModType.getItems().add(SelectSkillModification.class);
		cbModType.getItems().add(SkillGroupModification.class);
		cbModType.getItems().add(ItemAttributeModification.class);
		cbModType.setConverter(new StringConverter<Class<? extends Modification>>() {
			public Class<? extends Modification> fromString(String value) {
				return null;
			}
			public String toString(Class<? extends Modification> value) {
				return UI.getString("form.itemdata.modifications."+value.getSimpleName());
			}
		});
		btnAdd = new Button("\uE109");
		list   = new ListView<Modification>();
		list.setCellFactory(param -> new ListCell<Modification>(){
			public void updateItem(Modification item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setText(null);
				} else {
					Button btnDel = new Button("\uE107");
					btnDel.setUserData(item);
					btnDel.setOnAction(event -> deleteModification( (Modification)((Button)event.getSource()).getUserData() ));
					this.setGraphic(btnDel);
					if (item instanceof AttributeModification) {
						AttributeModification mod = (AttributeModification)item;
						if (mod.getValue()<1)
							setText(mod.getAttribute().getName()+" "+mod.getValue());
						else
							setText(mod.getAttribute().getName()+" +"+mod.getValue());
					} else if (item instanceof ItemHookModification) {
						ItemHookModification mod = (ItemHookModification)item;
						String action = mod.isRemove()?"Remove":"Add";
						if (mod.getCapacity()>0)
							setText(action+" slot '"+mod.getHook().getName()+"' with capacity "+mod.getCapacity());
						else
							setText(action+" slot '"+mod.getHook().getName()+"'");

					} else if (item instanceof SkillModification) {
						SkillModification mod = (SkillModification)item;
						if (mod.isConditional()) 
							setText(UI.getString("label.conditional")+" "+mod.getSkill().getName()+" +"+mod.getValue());
						else
							setText(mod.getSkill().getName()+" +"+mod.getValue());
					} else if (item instanceof SkillGroupModification) {
						SkillGroupModification mod = (SkillGroupModification)item;
						if (mod.isConditional()) 
							setText(UI.getString("label.conditional")+" "+mod.getSkillGroup().getName()+" +"+mod.getValue());
						else
							setText(mod.getSkillGroup().getName()+" +"+mod.getValue());
					} else
						setText(item.toString());
				}
			}
		});
	}
	
	
	//-------------------------------------------------------------------
	protected void deleteModification(Modification mod) {
		list.getItems().remove(mod);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.modifications.title"));
		lblTitle.getStyleClass().add("text-subheader");
		
		HBox line = new HBox();
		line.getChildren().addAll(cbModType, btnAdd);
		line.setStyle("-fx-spacing: 0.5em");
		
		list.setStyle("-fx-pref-height: 10em");
		layout = new VBox(lblTitle, line, list);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAdd.setOnAction(event -> {
			Class<? extends Modification> cls = cbModType.getValue();
			if (cls==ItemHookModification.class) {
				addItemHookModification();
			} else if (cls==AccessoryModification.class) {
				addAccessoryModification();
			} else if (cls==AttributeModification.class) {
				addAttributeModification();
			} else if (cls==SelectSkillModification.class) {
				addSelectSkillModification();
			} else if (cls==SkillModification.class) {
				addSkillModification();
			} else if (cls==SkillGroupModification.class) {
				addSkillGroupModification();
			} else {
				logger.warn("Don't know how to add "+cls);
				try {
					Modification mod = cls.newInstance();
					list.getItems().add(mod);
				} catch (Exception e) {
					logger.error("Error instantiating "+cls,e);
				}
			}
		});
	}

	//-------------------------------------------------------------------
	private void addItemHookModification() {
		Label lblChoice   = new Label(UI.getString("label.hooktype"));
		Label lblCapacity = new Label(UI.getString("label.capacity"));
		ChoiceBox<ItemHook> cbHook = new ChoiceBox<ItemHook>();
		cbHook.getItems().addAll(ItemHook.values());
		cbHook.getItems().remove(ItemHook.ARMOR_ADDITION);
		cbHook.setConverter(new StringConverter<ItemHook>() {
			public ItemHook fromString(String value) { return null;	}
			public String toString(ItemHook value) { return value.getName(); }
		});
		TextField tfCapacity = new TextField();
		
		GridPane layout = new GridPane();
		layout.add(  lblChoice, 0, 0);
		layout.add(  cbHook   , 1, 0);
		layout.add(lblCapacity, 0, 1);
		layout.add( tfCapacity, 1, 1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		cbHook.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null || !n.hasCapacity()) {
				tfCapacity.setText(null);
				tfCapacity.setDisable(true);
			} else
				tfCapacity.setDisable(false);
		});
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			ItemHook hook = cbHook.getValue();
			int capacity  = 0;
			if (hook.hasCapacity()) {
				try {
					capacity = Integer.parseInt(tfCapacity.getText());
				} catch (NumberFormatException e) {
					logger.error("Error parsing capacity: "+tfCapacity.getText());
				}
			}
			ItemHookModification toAdd = new ItemHookModification(hook, capacity);
			list.getItems().add(toAdd);
		}
	}

	//-------------------------------------------------------------------
	private void addAccessoryModification() {
		Label lblChoice   = new Label(UI.getString("label.hooktype"));
		Label lblAccess   = new Label(UI.getString("label.accessory"));
		ChoiceBox<ItemHook> cbHook = new ChoiceBox<ItemHook>();
		cbHook.getItems().addAll(ItemHook.values());
		cbHook.getItems().remove(ItemHook.ARMOR_ADDITION);
		cbHook.setConverter(new StringConverter<ItemHook>() {
			public ItemHook fromString(String value) { return null;	}
			public String toString(ItemHook value) { return value.getName(); }
		});
		ChoiceBox<ItemTemplate> cbAccess = new ChoiceBox<ItemTemplate>();
		ShadowrunCore.getItems(ItemType.ACCESSORY);
		
		
		GridPane layout = new GridPane();
		layout.add(lblChoice, 0, 0);
		layout.add(cbHook   , 1, 0);
		layout.add(lblAccess, 0, 1);
		layout.add( cbAccess, 1, 1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		cbHook.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			cbAccess.getItems().clear();
			if (n!=null) {
				cbAccess.getItems().addAll(ShadowrunCore.getAccessoryItems(n, null));
			}
		});
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			ItemHook hook = cbHook.getValue();
			ItemTemplate item = cbAccess.getValue();
			AccessoryModification toAdd = new AccessoryModification(hook, item, 0, null);
			list.getItems().add(toAdd);
			
			logger.debug("Added "+toAdd);
			if (toAdd.getItem()!=null && toAdd.getItem().getModifications().size()>0) {
				for (Modification mod : toAdd.getItem().getModifications()) {
					parent.handleFormBlockEvent(FormEventType.APPLY_MODIFICATION, mod);
				}
			}
		}
	}

	//-------------------------------------------------------------------
	private void addAttributeModification() {
		Label lblAttr   = new Label(UI.getString("label.attribute"));
		Label lblVal    = new Label(UI.getString("label.value"));
		ChoiceBox<Attribute> cbAttr = new ChoiceBox<Attribute>();
		cbAttr.getItems().addAll(Attribute.values());
		cbAttr.setConverter(new StringConverter<Attribute>() {
			public Attribute fromString(String value) { return null;	}
			public String toString(Attribute value) { return value.getName(); }
		});
		TextField cbVal = new TextField();
		
		
		GridPane layout = new GridPane();
		layout.add(lblAttr, 0, 0);
		layout.add(cbAttr , 1, 0);
		layout.add(lblVal , 0, 1);
		layout.add( cbVal , 1, 1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
//		cbHook.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			cbAccess.getItems().clear();
//			if (n!=null) {
//				cbAccess.getItems().addAll(ShadowrunCore.getAccessoryItems(n, null));
//			}
//		});
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			Attribute hook = cbAttr.getValue();
			int item = Integer.parseInt(cbVal.getText());
			AttributeModification toAdd = new AttributeModification(hook, item);
			list.getItems().add(toAdd);
			
			logger.debug("Added "+toAdd);
		}
	}

	//-------------------------------------------------------------------
	private void addSelectSkillModification() {
		Label lblSkil   = new Label(UI.getString("label.skilltype"));
		Label lblVal    = new Label(UI.getString("label.value"));
		ChoiceBox<SkillType> cbType = new ChoiceBox<SkillType>();
		cbType.getItems().addAll(SkillType.values());
		cbType.setValue(SkillType.NOT_SET);
		Collections.sort(cbType.getItems(), new Comparator<SkillType>() {
			public int compare(SkillType o1, SkillType o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbType.setConverter(new StringConverter<SkillType>() {
			public SkillType fromString(String value) { return null;	}
			public String toString(SkillType value) { return value.getName(); }
		});
		TextField cbVal = new TextField();
		CheckBox cbCond = new CheckBox(UI.getString("label.modification.conditional"));
		cbCond.setWrapText(true);
		
		GridPane layout = new GridPane();
		layout.add(lblSkil, 0, 0);
		layout.add(cbType , 1, 0);
		layout.add(lblVal , 0, 1);
		layout.add( cbVal , 1, 1);
		layout.add(cbCond , 0, 2, 2,1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			SkillType type= cbType.getValue();
			int val = Integer.parseInt(cbVal.getText());
			SelectSkillModification toAdd = new SelectSkillModification(type, val);
			toAdd.setConditional(cbCond.isSelected());
			list.getItems().add(toAdd);
			
			logger.debug("Added "+toAdd);
		}
	}

	//-------------------------------------------------------------------
	private void addSkillModification() {
		Label lblSkil   = new Label(UI.getString("label.skill"));
		Label lblVal    = new Label(UI.getString("label.value"));
		ChoiceBox<Skill> cbAttr = new ChoiceBox<Skill>();
		cbAttr.getItems().addAll(ShadowrunCore.getSkills());
		Collections.sort(cbAttr.getItems(), new Comparator<Skill>() {
			public int compare(Skill o1, Skill o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbAttr.setConverter(new StringConverter<Skill>() {
			public Skill fromString(String value) { return null;	}
			public String toString(Skill value) { return value.getName(); }
		});
		TextField cbVal = new TextField();
		CheckBox cbCond = new CheckBox(UI.getString("label.modification.conditional"));
		cbCond.setWrapText(true);
		
		GridPane layout = new GridPane();
		layout.add(lblSkil, 0, 0);
		layout.add(cbAttr , 1, 0);
		layout.add(lblVal , 0, 1);
		layout.add( cbVal , 1, 1);
		layout.add(cbCond , 0, 2, 2,1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			Skill ref = cbAttr.getValue();
			int val = Integer.parseInt(cbVal.getText());
			SkillModification toAdd = new SkillModification(ref, val);
			toAdd.setConditional(cbCond.isSelected());
			list.getItems().add(toAdd);
			
			logger.debug("Added "+toAdd);
		}
	}

	//-------------------------------------------------------------------
	private void addSkillGroupModification() {
		Label lblSkil   = new Label(UI.getString("label.skillgroup"));
		Label lblVal    = new Label(UI.getString("label.value"));
		ChoiceBox<SkillGroup> cbAttr = new ChoiceBox<SkillGroup>();
		cbAttr.getItems().addAll(ShadowrunCore.getSkillgroups());
		Collections.sort(cbAttr.getItems(), new Comparator<SkillGroup>() {
			public int compare(SkillGroup o1, SkillGroup o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		cbAttr.setConverter(new StringConverter<SkillGroup>() {
			public SkillGroup fromString(String value) { return null;	}
			public String toString(SkillGroup value) { return value.getName(); }
		});
		TextField cbVal = new TextField();
		CheckBox cbCond = new CheckBox(UI.getString("label.modification.conditional"));
		cbCond.setWrapText(true);
		
		GridPane layout = new GridPane();
		layout.add(lblSkil, 0, 0);
		layout.add(cbAttr , 1, 0);
		layout.add(lblVal , 0, 1);
		layout.add( cbVal , 1, 1);
		layout.add(cbCond , 0, 2, 2,1);
		layout.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		CloseType result = manager.showAlertAndCall(AlertType.QUESTION, "", layout);
		if (result==CloseType.OK) {
			SkillGroup ref = cbAttr.getValue();
			int val = Integer.parseInt(cbVal.getText());
			SkillGroupModification toAdd = new SkillGroupModification(ref, val);
			toAdd.setConditional(cbCond.isSelected());
			list.getItems().add(toAdd);
			
			logger.debug("Added "+toAdd);
		}
		
	}
	
	//-------------------------------------------------------------------
	public void clear() {
		list.getItems().clear();
	}
	
	//-------------------------------------------------------------------
	public void addHook(ItemHook hook) {
		ItemHookModification toAdd = new ItemHookModification(hook, 0);
		list.getItems().add(toAdd);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		item.getModifications().addAll(list.getItems());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		list.getItems().clear();
		list.getItems().addAll(item.getModifications());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		return true;
	}
	
}