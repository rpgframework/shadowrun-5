package org.prelle.shadowrun5.jfx.items.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;

import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class RatingFormBlock implements FormBlock {
	
	private static PropertyResourceBundle UI = SR5Constants.RES;

	private CheckBox cbHasRating;
	private CheckBox cbRateCapacity;
	private CheckBox cbRateDevice;
	private CheckBox cbRatePrice;
	private CheckBox cbRateAvailability;
	private CheckBox cbRateEssence;
	private CheckBox cbRateModif;
	private TextField tfMaxRating;
	private VBox     layout;
	
	//-------------------------------------------------------------------
	public RatingFormBlock() {
		initComponents();
		initLayout();
		initInteractivity();
		
		cbHasRating.setSelected(true);
		cbHasRating.setSelected(false);
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbHasRating = new CheckBox(UI.getString("form.itemdata.hasRating"));
		cbRateAvailability = new CheckBox(ItemAttribute.AVAILABILITY.getName());
		cbRateCapacity     = new CheckBox(ItemAttribute.CAPACITY.getName());
		cbRateDevice       = new CheckBox(ItemAttribute.DEVICE_RATING.getName());
		cbRatePrice        = new CheckBox(ItemAttribute.PRICE.getName());
		cbRateEssence      = new CheckBox(ItemAttribute.ESSENCECOST.getName());
		cbRateModif        = new CheckBox(UI.getString("form.itemdata.modifications.title"));
		tfMaxRating        = new TextField();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.rating.title"));
		Label lblDesc  = new Label(UI.getString("form.itemdata.rating.desc"));
		Label lblMax   = new Label(UI.getString("form.itemdata.rating.maxRating"));
		lblTitle.getStyleClass().add("text-subheader");
		
		FlowPane line = new FlowPane();
		line.getChildren().addAll(cbRateCapacity, cbRateAvailability, cbRateDevice, cbRatePrice, cbRateEssence, cbRateModif);
		line.setStyle("-fx-spacing: 0.5em; -fx-vgap: 0.5em; -fx-hgap: 0.5em");
		
		
		tfMaxRating.setStyle("-fx-pref-width: 3em");
		Region grow = new Region();
		grow.setMaxWidth(Double.MAX_VALUE);
		HBox line2 = new HBox();
		line2.getChildren().addAll(lblDesc, grow, lblMax, tfMaxRating);
		line2.setStyle("-fx-spacing: 0.5em");
		HBox.setHgrow(grow, Priority.ALWAYS);
		
		layout = new VBox(lblTitle, line2, line);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbHasRating.selectedProperty().addListener( (ov,o,n) -> {
			cbRateAvailability.setDisable(!n);
			cbRateCapacity.setDisable(!n);
			cbRateDevice.setDisable(!n);
			cbRatePrice.setDisable(!n);
			cbRateEssence.setDisable(!n);
			cbRateModif.setDisable(!n);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return cbHasRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		item.setHasRating(cbHasRating.isSelected());
		List<Multiply> multi = new ArrayList<>();
		if (cbHasRating.isSelected()) {
			if (cbRateAvailability.isSelected()) multi.add(Multiply.AVAIL);
			if (cbRateCapacity    .isSelected()) multi.add(Multiply.CAPACITY);
			if (cbRateDevice      .isSelected()) multi.add(Multiply.DEVICE);
			if (cbRatePrice       .isSelected()) multi.add(Multiply.PRICE);
			if (cbRateEssence     .isSelected()) multi.add(Multiply.ESSENCE);
			if (cbRateModif       .isSelected()) multi.add(Multiply.MODIFIER);
			item.setMaximumRating(Integer.parseInt(tfMaxRating.getText()));
		}
		item.setMultiplyWitRate( multi.toArray(new Multiply[multi.size()]) );
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		cbHasRating.setSelected(item.hasRating());
		List<Multiply> mult = Arrays.asList(item.getMultiplyWithRate());
		cbRateAvailability.setSelected(mult.contains(Multiply.AVAIL));
		cbRateCapacity    .setSelected(mult.contains(Multiply.CAPACITY));
		cbRateDevice      .setSelected(mult.contains(Multiply.DEVICE));
		cbRatePrice       .setSelected(mult.contains(Multiply.PRICE));
		cbRateEssence     .setSelected(mult.contains(Multiply.ESSENCE));
		cbRateModif       .setSelected(mult.contains(Multiply.MODIFIER));
		tfMaxRating       .setText(String.valueOf(item.getMaximumRating()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		if (!cbHasRating.isSelected())
			return true;
		try {
			Integer.parseInt(tfMaxRating.getText());
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
}