/**
 *
 */
package org.prelle.shadowrun5.jfx.items.input;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ManagedScreenPage;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun5.ShadowrunCustomDataCore;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


/**
 * @author prelle
 *
 */
public class ShadowrunDataInputScreen extends ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	public final static String KEY_MAIL = "my.mailaddress";
	public final static String KEY_NAME = "my.name";
	public static Preferences CONFIG = Preferences.userRoot().node("/org/rpgframework/"+System.getProperty("application.id")+"/shadowrun");


	private VBox sidebar;

	private TabPane pane;
	private ItemInputTab tabItem;

	private Button btnUpload;

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunDataInputScreen() {
		super(UI.getString("screen.datainput.title"));

		initComponents();
		initLayout();
		initInteractivity();

//		getContextButtons().add(btnUpload);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		pane = new TabPane();
		pane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		pane.sideProperty().set(Side.LEFT);
		pane.rotateGraphicProperty().set(true);
		initItems();
		initEducations();

		btnUpload = new Button(null,new FontIcon("\uE0FA"));
		btnUpload.setTooltip(new Tooltip(UI.getString("button.upload.tooltip")));
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Sidebar
		Label lblInfo = new Label(UI.getString("screen.datainput.info"));
		lblInfo.setWrapText(true);
		sidebar = new VBox();
		sidebar.getChildren().add(lblInfo);
		sidebar.getStyleClass().add("section-bar");
		sidebar.setStyle("-fx-pref-width: 15em");

		HBox layout = new HBox();
		layout.setStyle("-fx-spacing: 1em");
		layout.getChildren().addAll(sidebar, pane);
		HBox.setMargin(sidebar, new Insets(0,0,20,0));
		HBox.setMargin(pane, new Insets(0,0,20,10));

		setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnUpload.setOnAction(event -> {
			Tab selected = pane.getSelectionModel().getSelectedItem();
			if (selected==tabItem)
				uploadItemsClicked();
			else
				logger.warn("Don't know how to upload "+selected);
		});
	}

	//-------------------------------------------------------------------
	private void initItems() {
		tabItem = new ItemInputTab();
		tabItem.setData(ShadowrunCustomDataCore.getItems());
		pane.getTabs().add(tabItem);
	}

	//-------------------------------------------------------------------
	private void initEducations() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#setManager(org.prelle.javafx.ScreenManager)
	 */
	@Override
	public void setManager(ScreenManager manager) {
		super.setManager(manager);
		tabItem.setScreenManager(manager);
	}

	//-------------------------------------------------------------------
	private void uploadItemsClicked() {
		logger.info("upload items");

		List<ItemTemplate> toUpload = new ArrayList<ItemTemplate>();
		for (ItemTemplate item : ShadowrunCustomDataCore.getItems()) {
			if ( item.getPage()!=0 && item.getHelpText()!=null && !item.getHelpText().isEmpty())
				toUpload.add(item);
		}

		NavigButtonControl buttonControl = new NavigButtonControl();
		UploadPane pane = new UploadPane();
		buttonControl.setCallback(pane);
		String title = UI.getString("dialog.upload.items.title");

		CloseType result = getManager().showAlertAndCall(AlertType.QUESTION, title, pane, buttonControl);
		if (result==CloseType.OK) {
			boolean success = ShadowrunCustomDataCore.upload(toUpload, pane.getFromAddress(), pane.getDisplayName(), pane.getDescription());
			if (success) {
				CONFIG.put(KEY_MAIL, pane.getFromAddress());
				CONFIG.put(KEY_NAME, pane.getDisplayName());
				getManager().showAlertAndCall(AlertType.NOTIFICATION, "", UI.getString("dialog.upload.items.success"));
			} else {
				getManager().showAlertAndCall(AlertType.NOTIFICATION, "", UI.getString("dialog.upload.items.error"));
			}
		}
	}

}
