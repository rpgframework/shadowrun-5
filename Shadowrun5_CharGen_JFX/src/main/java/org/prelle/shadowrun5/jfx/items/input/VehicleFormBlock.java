package org.prelle.shadowrun5.jfx.items.input;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.persist.OnRoadOffRoadConverter;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.VehicleData;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class VehicleFormBlock implements FormBlock {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private static final OnRoadOffRoadConverter CONVERT = new OnRoadOffRoadConverter();
	private Map<ItemAttribute, List<ItemAttributeModification>> mods;
	
	private TextField tfHan;
	private TextField tfSpd;
	private TextField tfAcc;
	private TextField tfBod;
	private TextField tfArm;
	private TextField tfPil;
	private TextField tfSen;
	private TextField tfSea;
	private VBox     layout;
	
	//-------------------------------------------------------------------
	public VehicleFormBlock() {
		mods = new HashMap<ItemAttribute, List<ItemAttributeModification>>();
		initComponents();
		initLayout();
		initInteractivity();
		
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfHan = new TextField();
		tfSpd = new TextField();
		tfAcc = new TextField();
		tfBod = new TextField();
		tfArm = new TextField();
		tfPil = new TextField();
		tfSen = new TextField();
		tfSea = new TextField();
		
		tfAcc.setPrefColumnCount(3);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.vehicle.title"));
//		Label lblDesc  = new Label(UI.getString("form.itemdata.vehicle.desc"));
		lblTitle.getStyleClass().add("text-subheader");
		
		Label lblHan = new Label(ItemAttribute.HANDLING.getShortName());
		Label lblSpd = new Label(ItemAttribute.SPEED.getShortName());
		Label lblAcc = new Label(ItemAttribute.ACCELERATION.getShortName());
		Label lblBod = new Label(ItemAttribute.BODY.getShortName());
		Label lblArm = new Label(ItemAttribute.ARMOR.getShortName());
		Label lblPil = new Label(ItemAttribute.PILOT.getShortName());
		Label lblSen = new Label(ItemAttribute.SENSORS.getShortName());
		Label lblSea = new Label(ItemAttribute.SEATS.getShortName());
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.2em");

		grid.add(lblHan, 0, 1);
		grid.add(tfHan , 1, 1);

		grid.add(lblSpd, 2, 1);
		grid.add(tfSpd , 3, 1);

		grid.add(lblAcc, 4, 1);
		grid.add(tfAcc , 5, 1);

		grid.add(lblBod, 6, 1);
		grid.add(tfBod , 7, 1);

		grid.add(lblArm, 8, 1);
		grid.add(tfArm , 9, 1);
		
		grid.add(lblPil, 10, 1);
		grid.add(tfPil , 11, 1);
		
		grid.add(lblSen, 12, 1);
		grid.add(tfSen , 13, 1);
		
		grid.add(lblSea, 14, 1);
		grid.add(tfSea , 15, 1);
		
		
		lblHan.setStyle("-fx-pref-width: "+lblHan.getText().length()+"em");
		lblSpd.setStyle("-fx-pref-width: "+(lblSpd.getText().length()+1)+"em");
		lblAcc.setStyle("-fx-pref-width: "+lblAcc.getText().length()+"em");
		lblBod.setStyle("-fx-pref-width: "+lblBod.getText().length()+"em");
		lblArm.setStyle("-fx-pref-width: "+lblArm.getText().length()+"em");
		lblPil.setStyle("-fx-pref-width: "+lblPil.getText().length()+"em");
		lblSen.setStyle("-fx-pref-width: "+lblSen.getText().length()+"em");
		lblSea.setStyle("-fx-pref-width: "+lblSea.getText().length()+"em");
		tfHan.setStyle("-fx-pref-width: 4em");
		tfSpd.setStyle("-fx-pref-width: 4em");
		tfAcc.setStyle("-fx-pref-width: 2em");
		tfBod.setStyle("-fx-pref-width: 2em");
		tfArm.setStyle("-fx-pref-width: 3em");
		tfPil.setStyle("-fx-pref-width: 2em");
		tfSen.setStyle("-fx-pref-width: 3em");
		tfSea.setStyle("-fx-pref-width: 3em");
		
		for (Node node : grid.getChildren()) {
			if (node instanceof Label)
				node.setStyle("-fx-min-width: 3em");
			if (node instanceof Label) {
				((Label)node).setAlignment(Pos.CENTER_RIGHT);
			}
		}
		
//		tfDmg.setStyle("-fx-pref-width: 8em");
//		lblSkil.setStyle("-fx-pref-width: 25em");
		
		layout = new VBox(lblTitle, grid);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		VehicleData data = new VehicleData();
		data.setHandling(CONVERT.read(tfHan.getText()));
		data.setSpeed   (CONVERT.read(tfSpd.getText()));
		data.setAcceleration(Integer.parseInt(tfAcc.getText()));
		data.setBody    (Integer.parseInt(tfBod.getText()));
		data.setArmor   (Integer.parseInt(tfArm.getText()));
		data.setPilot   (Integer.parseInt(tfPil.getText()));
		data.setSensor  (Integer.parseInt(tfSen.getText()));
		data.setSeats   (Integer.parseInt(tfSea.getText()));

		item.setVehicleData(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		VehicleData data = item.getVehicleData();
		if (data==null) {
			tfHan.setText(null);
			tfSpd.setText(null);
			tfAcc.setText(null);
			tfSpd.setText(null);
			tfBod.setText(null);
			tfPil.setText(null);
			tfSen.setText(null);
			tfSea.setText(null);
		} else {
			tfHan.setText(CONVERT.write(data.getHandling()));
			tfSpd.setText(CONVERT.write(data.getSpeed()));
			tfAcc.setText(String.valueOf(data.getAcceleration()));
			tfBod.setText(String.valueOf(data.getBody()));
			tfArm.setText(String.valueOf(data.getArmor()));
			tfPil.setText(String.valueOf(data.getPilot()));
			tfSea.setText(String.valueOf(data.getSeats()));
			tfSen.setText(String.valueOf(data.getSensor()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		logger.debug("checkInput");
		boolean isOkay = true;

		// Handling
		try {
			CONVERT.read(tfHan.getText());
			tfHan.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfHan.getStyleClass().contains(ERROR_CLASS))
				tfHan.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Speed
		try {
			CONVERT.read(tfSpd.getText());
			tfSpd.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfSpd.getStyleClass().contains(ERROR_CLASS))
				tfSpd.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Acceleration
		try {
			Integer.parseInt(tfAcc.getText());
			tfAcc.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfAcc.getStyleClass().contains(ERROR_CLASS))
				tfAcc.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Body
		try {
			Integer.parseInt(tfBod.getText());
			tfBod.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfBod.getStyleClass().contains(ERROR_CLASS))
				tfBod.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Armor
		try {
			Integer.parseInt(tfArm.getText());
			tfArm.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfArm.getStyleClass().contains(ERROR_CLASS))
				tfArm.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Pilot
		try {
			Integer.parseInt(tfPil.getText());
			tfPil.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfPil.getStyleClass().contains(ERROR_CLASS))
				tfPil.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Sensor
		try {
			Integer.parseInt(tfSen.getText());
			tfSen.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfSen.getStyleClass().contains(ERROR_CLASS))
				tfSen.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Seats
		try {
			Integer.parseInt(tfSea.getText());
			tfSea.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			if (!tfSea.getStyleClass().contains(ERROR_CLASS))
				tfSea.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}
			
		return isOkay;
	}
	
}