package org.prelle.shadowrun5.jfx.items.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.items.AmmunitionSlot;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.WeaponData;
import org.prelle.shadowrun5.items.AmmunitionSlot.AmmoSlotType;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;
import org.prelle.shadowrun5.persist.AmmunitionConverter;
import org.prelle.shadowrun5.persist.FireModesConverter;
import org.prelle.shadowrun5.persist.WeaponDamageConverter;

import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
class WeaponFormBlock implements FormBlock {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private Map<ItemAttribute, List<ItemAttributeModification>> mods;
	
	private CheckBox cbIsWeapon;
	private ChoiceBox<Skill> cbSkill;
	private ChoiceBox<SkillSpecialization> cbSpecial;
	private TextField tfAcc;
	private TextField tfDmg;
	private TextField tfAP;
	private VBox     layout;
	private Label     modAcc;
	private Label     modDmg;
	private Label     modAP;
	// For firearms
	private TextField tfMode;
	private TextField tfRC;
	private TextField tfAmmo;
	// For melee
	private TextField tfReach;
	
	//-------------------------------------------------------------------
	public WeaponFormBlock() {
		mods = new HashMap<ItemAttribute, List<ItemAttributeModification>>();
		initComponents();
		initLayout();
		initInteractivity();
		
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbIsWeapon = new CheckBox(UI.getString("form.itemdata.isWeapon"));
		cbSkill    = new ChoiceBox<Skill>();
		cbSkill.getItems().addAll(ShadowrunCore.getSkills(SkillType.COMBAT));
		cbSkill.setConverter(new StringConverter<Skill>() {
			public Skill fromString(String value) { return null; }
			public String toString(Skill value) { return value.getName(); }
		});
		cbSpecial  = new ChoiceBox<SkillSpecialization>();
		cbSpecial.setConverter(new StringConverter<SkillSpecialization>() {
			public SkillSpecialization fromString(String value) { return null; }
			public String toString(SkillSpecialization value) { return value.getName(); }
		});
		
		tfAcc  = new TextField();
		tfDmg  = new TextField();
		tfAP   = new TextField();
		tfMode = new TextField();
		tfRC   = new TextField();
		tfAmmo = new TextField();
		tfReach= new TextField();
		
		
		modAcc = new Label();
		modDmg = new Label();
		modAP  = new Label();
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblTitle = new Label(UI.getString("form.itemdata.weapon.title"));
		Label lblDesc  = new Label(UI.getString("form.itemdata.weapon.desc"));
		lblTitle.getStyleClass().add("text-subheader");
		
		Label lblSkil  = new Label(UI.getString("label.weaponskill"));
		Label lblSpec  = new Label(UI.getString("label.specialization"));
		Label lblAcc   = new Label(ItemAttribute.ACCURACY.getShortName());
		Label lblDmg   = new Label(ItemAttribute.DAMAGE.getShortName());
		Label lblAP    = new Label(ItemAttribute.ARMOR_PENETRATION.getShortName());
		Label lblMode  = new Label(ItemAttribute.MODE.getShortName());
		Label lblRC    = new Label(ItemAttribute.RECOIL_COMPENSATION.getShortName());
		Label lblAmmo  = new Label(ItemAttribute.AMMUNITION.getShortName());
		Label lblReach = new Label(ItemAttribute.REACH.getShortName());
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 0.5em");
		grid.add(lblSkil  , 0, 0, 2,1);
		grid.add(cbSkill  , 2, 0, 3,1);
		grid.add(lblSpec  , 6, 0, 2,1);
		grid.add(cbSpecial, 8, 0, 3,1);
		grid.add(lblAcc, 0, 1);
		grid.add( tfAcc, 1, 1);
		grid.add(modAcc, 2, 1);
		
		grid.add(lblDmg, 3, 1);
		grid.add( tfDmg, 4, 1);
		grid.add(modDmg, 5, 1);
		
		grid.add(lblAP , 6, 1);
		grid.add( tfAP , 7, 1);
		grid.add(modAP , 8, 1);

		grid.add(lblMode, 0, 2);
		grid.add(tfMode , 1, 2);
		
		grid.add(lblRC  , 3, 2);
		grid.add(tfRC   , 4, 2);
		
		grid.add(lblAmmo, 6, 2);
		grid.add(tfAmmo , 7, 2);
		
		grid.add(lblReach, 9, 2);
		grid.add(tfReach , 10, 2);
		
		lblAcc.setStyle("-fx-min-width: "+lblAcc.getText().length()+"em");
		lblDmg.setStyle("-fx-min-width: "+lblDmg.getText().length()+"em");
		lblAP.setStyle("-fx-min-width: "+lblAP.getText().length()+"em");
		lblMode.setStyle("-fx-min-width: "+lblMode.getText().length()+"em");
		tfMode.setStyle("-fx-min-width: 6em");
		lblRC.setStyle("-fx-min-width: "+lblRC.getText().length()+"em");
		lblAmmo.setStyle("-fx-min-width: "+lblAmmo.getText().length()+"em");
		lblReach.setStyle("-fx-min-width: "+lblReach.getText().length()+"em");
//		tfDmg.setStyle("-fx-pref-width: 8em");
//		lblSkil.setStyle("-fx-pref-width: 25em");
		
		layout = new VBox(lblTitle, lblDesc, grid);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbIsWeapon.selectedProperty().addListener( (ov,o,n) -> {
			tfAcc.setDisable(!n);
			tfDmg.setDisable(!n);
			tfAP.setDisable(!n);
		});
		
		cbSkill.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			cbSpecial.getItems().clear();
			if (n!=null) {
				cbSpecial.getItems().addAll(n.getSpecializations());
			}
		});
		
		cbIsWeapon.selectedProperty().addListener( (ov,o,n) -> {
			layout.setVisible(n);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getContent()
	 */
	@Override
	public Parent getContent() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#getCheckBox()
	 */
	@Override
	public CheckBox getCheckBox() {
		return cbIsWeapon;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#readInto(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void readInto(ItemTemplate item) {
		if (cbIsWeapon.isSelected()) {
			WeaponData data = new WeaponData();
				data.setSkill(cbSkill.getValue());
				if (cbSpecial.getValue()!=null)
					data.setSpecialization(cbSpecial.getValue());
				data.setAccuracy(Integer.parseInt(tfAcc.getText()));
				data.setArmorPenetration(Integer.parseInt(tfAP.getText()));
				try { data.setDamage(  (new WeaponDamageConverter()).read( tfDmg.getText())); } catch (Exception e) {}
				if (!tfMode.getText().isEmpty())
					try { data.setFireModes(  (new FireModesConverter()).read( tfMode.getText())); } catch (Exception e) {}
				if (!tfRC.getText().isEmpty())
					data.setRecoilCompensation(Integer.parseInt(tfRC.getText()));
				if (!tfAmmo.getText().isEmpty())
					data.setAmmunition(new AmmunitionSlot(Integer.parseInt(tfAmmo.getText()), AmmoSlotType.MAGAZINE));
				if (!tfReach.getText().isEmpty())
					data.setReach(Integer.parseInt(tfReach.getText()));
					
				
				item.setWeaponData(data);
		} else {
			item.setWeaponData(null);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#fillFrom(org.prelle.shadowrun5.items.ItemTemplate)
	 */
	@Override
	public void fillFrom(ItemTemplate item) {
		WeaponData data = item.getWeaponData();
		if (data==null) {
			cbSkill.getSelectionModel().clearSelection();
			cbSpecial.getSelectionModel().clearSelection();
			tfAcc.setText(null);
			tfAP.setText(null);
			tfDmg.setText(null);
			tfReach.setText(null);
			tfRC.setText(null);
		} else {
			cbSkill.setValue(data.getSkill());
			if (data.getSpecialization()!=null)
				cbSpecial.setValue(data.getSpecialization());
			tfAcc.setText(String.valueOf(data.getAccuracy()));
			tfAP.setText(String.valueOf(data.getArmorPenetration()));
			try {
				tfDmg.setText(  (new WeaponDamageConverter()).write( data.getDamage()));
				tfMode.setText( (new FireModesConverter()).write(data.getFireModes()));
			} catch (Exception e) {
			}
			tfRC.setText(String.valueOf(data.getRecoilCompensation()));
			tfAmmo.setText(String.valueOf(data.getAmmunition()));
			tfReach.setText(String.valueOf(data.getReach()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.jfx.items.input.ItemDataForm.FormBlock#checkInput()
	 */
	@Override
	public boolean checkInput() {
		if (!cbIsWeapon.isSelected()) {
			cbSkill.getStyleClass().remove(ERROR_CLASS);
			tfAcc.getStyleClass().remove(ERROR_CLASS);
			tfAP.getStyleClass().remove(ERROR_CLASS);
			tfDmg.getStyleClass().remove(ERROR_CLASS);
			return true;
		}
		
		logger.debug("checkInput");
		boolean isOkay = true;
		
		// Skill
		if (cbSkill.getValue()==null) {
			if (!cbSkill.getStyleClass().contains(ERROR_CLASS))
				cbSkill.getStyleClass().add(ERROR_CLASS);
			logger.debug("Skill is not set");
			isOkay=false;
		} else
			cbSkill.getStyleClass().remove(ERROR_CLASS);

		// Accuracy
		try {
			Integer.parseInt(tfAcc.getText());
			tfAcc.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			logger.debug("Accuracy is invalid: "+e);
			if (!tfAcc.getStyleClass().contains(ERROR_CLASS))
				tfAcc.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Armor Penetration
		try {
			Integer.parseInt(tfAP.getText());
			tfAP.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			logger.debug("Armor penetration is invalid: "+e);
			if (!tfAP.getStyleClass().contains(ERROR_CLASS))
				tfAP.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Damage
		try {
			(new WeaponDamageConverter()).read( tfDmg.getText());
			tfDmg.getStyleClass().remove(ERROR_CLASS);
		} catch (Exception e) {
			logger.debug("Weapon damage is invalid: "+e);
			if (!tfDmg.getStyleClass().contains(ERROR_CLASS))
				tfDmg.getStyleClass().add(ERROR_CLASS);
			isOkay=false;
		}

		// Fire mode
		if (!tfMode.getText().isEmpty()) {
			try {
				(new FireModesConverter()).read( tfMode.getText());
				tfMode.getStyleClass().remove(ERROR_CLASS);
			} catch (Exception e) {
				logger.debug("Mode is invalid: "+e);
				if (!tfMode.getStyleClass().contains(ERROR_CLASS))
					tfMode.getStyleClass().add(ERROR_CLASS);
				isOkay=false;
			}
		}

		// Ammunition
		if (!tfAmmo.getText().isEmpty()) {
			try {
				(new AmmunitionConverter()).read( tfAmmo.getText());
				tfMode.getStyleClass().remove(ERROR_CLASS);
			} catch (Exception e) {
				logger.debug("Ammunition is invalid: "+e);
				if (!tfMode.getStyleClass().contains(ERROR_CLASS))
					tfMode.getStyleClass().add(ERROR_CLASS);
				isOkay=false;
			}
		}

		// Recoil compensation
		if (!tfRC.getText().isEmpty()) {
			try {
				Integer.parseInt(tfRC.getText());
				tfRC.getStyleClass().remove(ERROR_CLASS);
			} catch (Exception e) {
				logger.debug("Recoil compensation is invalid: "+e);
				if (!tfRC.getStyleClass().contains(ERROR_CLASS))
					tfRC.getStyleClass().add(ERROR_CLASS);
				isOkay=false;
			}
		}
			
		logger.debug("isOkay = "+isOkay);
		return isOkay;
	}
	
	//-------------------------------------------------------------------
	private int getModificator(ItemAttribute attr) {
		List<ItemAttributeModification> list = mods.get(attr);
		if (list==null) 
			return 0;
		
		int sum = 0;
		for (ItemAttributeModification mod : list) {
			sum += mod.getValue();
		}
		return sum;
	}
	
	//-------------------------------------------------------------------
	private void updateModifier() {
		// Accuracy
		int mod = getModificator(ItemAttribute.ACCURACY);
		if (mod!=0)
			modAcc.setText(String.valueOf(mod));
		else
			modAcc.setText(null);
		
		// Armor Penet
		mod = getModificator(ItemAttribute.ARMOR_PENETRATION);
		if (mod>0)
			modAP.setText("+"+mod);
		else if (mod<0)
			modAP.setText(String.valueOf(mod));
		else
			modAP.setText(null);
	}
	
	//-------------------------------------------------------------------
	public void addModification(ItemAttributeModification aMod) {
		List<ItemAttributeModification> list = mods.get(aMod.getModifiedItem());
		if (list==null) {
			list = new ArrayList<ItemAttributeModification>();
			mods.put(aMod.getModifiedItem(), list);
		}
		
		list.add(aMod);
		updateModifier();
	}
	
	//-------------------------------------------------------------------
	public void removeModification(ItemAttributeModification aMod) {
		List<ItemAttributeModification> list = mods.get(aMod.getModifiedItem());
		if (list==null) {
			return;
		}
		
		list.remove(aMod);
		updateModifier();
	}
	
}