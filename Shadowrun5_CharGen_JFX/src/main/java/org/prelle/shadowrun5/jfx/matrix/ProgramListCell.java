package org.prelle.shadowrun5.jfx.matrix;

import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.charctrl.ProgramController;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class ProgramListCell extends ListCell<Program> {

	private final static String NORMAL_STYLE = "equipment-cell";

	private Label lblName;
	private Label lblAvail;
	private Label lblPrice;
	private HBox  lower;
	private StackPane stack;

	private Program data;
	private ProgramController control;
	//	private MatrixScreen parent;

	//-------------------------------------------------------------------
	public ProgramListCell(ProgramController ctrl) {
		this.control = ctrl;
		initComponents();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");
		lblPrice.setStyle("-fx-pref-width: 6em");

		lblAvail = new Label();
		lblAvail.setStyle("-fx-pref-width: 4em");
		lblAvail.getStyleClass().add("itemprice-label");
		lower   = new HBox(10);
		lower.setMaxWidth(Double.MAX_VALUE);
		lower.getChildren().addAll(buffer, lblAvail, lblPrice);
		HBox.setHgrow(buffer, Priority.ALWAYS);

		stack = new StackPane();
		stack.getChildren().addAll(lblName, lower);
		stack.setMaxWidth(Double.MAX_VALUE);
		StackPane.setAlignment(lower, Pos.CENTER_RIGHT);
		StackPane.setAlignment(lblName, Pos.CENTER_LEFT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Program item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(((Program)item).getName());
			lblPrice.setText("\u00A5 "+control.getPrice(item));
			lblAvail.setText(control.getAvailability(item).toString());
//			if (control.canBeSelected(item)) {
//				lblPrice.getStyleClass().remove(NOT_MET_STYLE);
//				lblAvail.getStyleClass().remove(NOT_MET_STYLE);
//			} else {
//				lblPrice.getStyleClass().add(NOT_MET_STYLE);
//				lblAvail.getStyleClass().add(NOT_MET_STYLE);
//			}
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("program:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}
}