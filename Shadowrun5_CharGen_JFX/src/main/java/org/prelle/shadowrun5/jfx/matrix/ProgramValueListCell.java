package org.prelle.shadowrun5.jfx.matrix;

import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class ProgramValueListCell extends ListCell<ProgramValue> {

	private ImageView iview;
	private Label lblName;
	private Label lblLine1;

	private HBox bxLayout;
	private VBox bxData;

	private transient ProgramValue data;

	//-------------------------------------------------------------------
	public ProgramValueListCell() {
		initComponents();
		initLayout();
		initStyle();
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();

		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1);

		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ProgramValue item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			Program spell = item.getModifyable();
			lblName.setText(spell.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(spell.getProductName()+" "+spell.getPage());
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		if (data==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		String id = "program:"+data.getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

}