/**
 * 
 */
package org.prelle.shadowrun5.jfx.newwizard;

import org.prelle.shadowrun5.BasePluginData;

import javafx.scene.Node;

/**
 * Called from the CharacterDataNodes when an item is selected for description
 * @author prelle
 *
 */
public interface SelectionCallback {

	public void showDescription(Node node, BasePluginData value);
	
}
