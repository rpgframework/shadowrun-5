package org.prelle.shadowrun5.jfx.powers;

import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class PowerListCell extends ListCell<AdeptPower> {

	private final static String NORMAL_STYLE = "power-cell";

	private AdeptPowerController control;
	private AdeptPower data;

	private Label lineName;
	private Label lineDescription;
	private VBox  layout;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	//-------------------------------------------------------------------
	public PowerListCell(AdeptPowerController ctrl) {
		this.control = ctrl;
		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		lineDescription = new Label();
		layout   = new VBox(3);
		layout.getChildren().addAll(lineName, lineDescription);

		// Recommended icon
		imgRecommended = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(AdeptPower item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
			lblType.setText(String.valueOf(item.getCost()));
			lineName.setText(((AdeptPower)item).getName());
			lineDescription.setText(item.getProductName()+" "+item.getPage());
			imgRecommended.setVisible(control.isRecommended((AdeptPower) item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("power:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

}