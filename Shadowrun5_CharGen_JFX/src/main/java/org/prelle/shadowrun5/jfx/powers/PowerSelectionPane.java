/**
 * 
 */
package org.prelle.shadowrun5.jfx.powers;

import java.util.PropertyResourceBundle;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.ShadowrunJFXUtils;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class PowerSelectionPane extends HBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".powers");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private AdeptPowerController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private Label hdAvailable;
	private Label hdSelected;
	private Label hdDescript;
	private ListView<AdeptPower> lvAvailable;
	private ListView<AdeptPowerValue> lvSelected;
	private VBox colDescription;
	private Label lblName;
	private Label lblPage;
	private Label lblDesc;
	private ObjectProperty<AdeptPower> selectedItem;

	//--------------------------------------------------------------------
	public PowerSelectionPane(AdeptPowerController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		hdAvailable = new Label(UI.getString("label.available"));
		hdSelected  = new Label(UI.getString("label.selected"));
		hdDescript  = new Label(UI.getString("label.description"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdDescript.getStyleClass().add("text-subheader");
		//		hdAvailable.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdSelected.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdDescript.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdSelected.getStyleClass().add("text-subheader");
		lvAvailable = new ListView<>();
		lvSelected  = new ListView<>();

		lvAvailable.setCellFactory(new Callback<ListView<AdeptPower>, ListCell<AdeptPower>>() {
			public ListCell<AdeptPower> call(ListView<AdeptPower> param) {
				return new PowerListCell(ctrl);
			}
		});
		lvSelected.setCellFactory(new Callback<ListView<AdeptPowerValue>, ListCell<AdeptPowerValue>>() {
			public ListCell<AdeptPowerValue> call(ListView<AdeptPowerValue> param) {
				return new PowerValueListCell(ctrl, lvSelected, provider);
			}
		});

		Label phSelected = new Label(UI.getString("skillvaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		lblName = new Label();
		lblName.getStyleClass().add("text-small-secondary");
		lblName.setStyle("-fx-font-weight: bold");
		lblPage = new Label();
		lblPage.setStyle("-fx-font-style: italic");
		lblDesc = new Label();
		lblDesc.setWrapText(true);

		colDescription = new VBox(5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-min-width: 13em; -fx-pref-width: 20em");
		lvSelected .setStyle("-fx-min-width: 19em; -fx-pref-width: 23em");
		colDescription.setStyle("-fx-pref-width: 20em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, lvAvailable);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(hdSelected, lvSelected);
		VBox column3 = new VBox(10);
		VBox.setVgrow(colDescription, Priority.ALWAYS);
		column3.getChildren().addAll(hdDescript, colDescription);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);
		colDescription.getChildren().addAll(lblName, lblPage, lblDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2, column3);
		column3.setStyle("-fx-max-width: 20em");

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> powerSelectionChanged(n));
		lvAvailable.setOnDragDropped(event -> dragDroppedAvailable(event));
		lvAvailable.setOnDragOver(event -> dragOver(event));
		
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				powerSelectionChanged( n.getModifyable());
			}
		});
		lvSelected.setOnDragDropped(event -> dragDroppedSelected(event));
		lvSelected.setOnDragOver(event -> dragOver(event));
	}

	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<AdeptPower> selectedItemProperty() {
		return selectedItem;
	}

	//--------------------------------------------------------------------
	private void powerSelectionChanged(AdeptPower data) {
		if(selectedItem!=null) {
			selectedItem.set(data);
		}
		if (data!=null) {
			logger.debug("Selected "+data);
			lblName.setText(data.getName());
			lblPage.setText(data.getProductName()+" "+data.getPage());
			lblDesc.setText(data.getHelpText());
		}
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailablePowers());

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getAdeptPowers());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private String askForSkillName() {
		Label label = new Label(UI.getString("skillselectpane.skillname_dialog.label"));
		label.getStyleClass().add("text-small-subheader");
		TextField tfName = new TextField();
		VBox content = new VBox(5);
		content.getChildren().addAll(label, tfName);

		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, UI.getString("skillselectpane.skillname_dialog.title"), content);
		if (close==CloseType.OK) {
			String name = tfName.getText();
			if (name!=null && name.trim().length()>0)
				return name.trim();
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragDroppedSelected(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("power")) {
					AdeptPower data = ShadowrunCore.getAdeptPower(tail);
					if (data==null) {
						logger.warn("Cannot find adept power for dropped power id '"+tail+"'");						
					} else {
						event.setDropCompleted(success);
						event.consume();
						if (data.needsChoice()) {
							Platform.runLater(new Runnable() {
								public void run() {
									logger.debug("To select "+data+", a further choice is necessary");
									Object selection = ShadowrunJFXUtils.choose(provider, model, data.getSelectFrom());
									ctrl.select(data, selection);
								}});
						} else {
							ctrl.select(data);
						}
						return;
					}
				} 
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragDroppedAvailable(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("power")) {
					StringTokenizer tok = new StringTokenizer(tail,":");
					tail = tok.nextToken();
					String uuid = (tok.hasMoreTokens())?tok.nextToken():null;
					AdeptPowerValue pVal = null;
					for (AdeptPowerValue tmp : model.getAdeptPowers()) {
						if (!tmp.getModifyable().getId().equals(tail))
							continue;
						if (uuid==null || tmp.getUniqueId().toString().equals(uuid)) {
							pVal = tmp;
							break;
						}
					}
					logger.debug("AdeptPower to deselect: "+pVal);
					
					if (pVal==null) {
						logger.warn("Cannot find adept power for dropped power id '"+tail+"'");						
					} else {
						event.setDropCompleted(success);
						event.consume();
						ctrl.deselect(pVal);
						return;
					}
				} 
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POWER_ADDED:
		case POWER_CHANGED:
		case POWER_REMOVED:
		case POWERS_AVAILABLE_CHANGED:
		case POINTS_LEFT_POWERS:
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

}