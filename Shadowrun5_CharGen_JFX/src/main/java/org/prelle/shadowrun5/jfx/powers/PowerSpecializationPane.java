package org.prelle.shadowrun5.jfx.powers;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.AdeptPowerValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.charctrl.AdeptPowerController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.beans.property.BooleanProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class PowerSpecializationPane extends VBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".powers");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private AdeptPowerController control;
	private AdeptPowerValue sVal;
	private AdeptPower data;

	private CheckBox[] boxes;
	private Map<BooleanProperty, SkillSpecialization> specByProp;

	//--------------------------------------------------------------------
	public PowerSpecializationPane(AdeptPowerController ctrl, AdeptPowerValue sVal)  {
		super(5);
		this.control = ctrl;
		this.sVal    = sVal;
		this.data   = sVal.getModifyable();
		specByProp   = new HashMap<>();

		initComponents();
		initLayout();
		initInteractivity();
		refresh();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		//		boxes = new CheckBox[data.getSpecializations().size()];
		//		int i=0;
		//		for (SkillSpecialization spec : data.getSpecializations()) {
		//			boxes[i] = new CheckBox(spec.getName());
		//			boxes[i].setUserData(spec);
		//			specByProp.put(boxes[i].selectedProperty(), spec);
		//			i++;
		//		}
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label label = new Label(String.format(UI.getString("skillselectpane.specialization.dialog.text"), data.getName()));
		label.setWrapText(true);
		this.getChildren().add(label);
		//		for (int i=0; i<boxes.length; i++) {
		//			this.getChildren().add(boxes[i]);
		//		}
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		//		for (int i=0; i<boxes.length; i++) {
		//			boxes[i].selectedProperty().addListener( (ov,o,n) -> {
		//				SkillSpecialization spec = specByProp.get(ov);
		//				if (n)
		//					control.select(sVal, spec);
		//				else
		//					control.deselect(sVal, spec);
		//			});
		//		}		
	}

	//--------------------------------------------------------------------
	public void refresh() {
		//		for (int i=0; i<boxes.length; i++) {
		//			SkillSpecialization spec = (SkillSpecialization)boxes[i].getUserData();
		//			boolean selected = sVal.hasSpecialization(spec);
		//			boxes[i].setSelected(selected);
		//			if (selected)
		//				boxes[i].setDisable(!control.canBeDeselected(sVal, spec));
		//			else
		//				boxes[i].setDisable(!control.canBeSelected(sVal, spec));
		//		}

	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POWER_ADDED:
		case POWER_CHANGED:
		case POWER_REMOVED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}
}