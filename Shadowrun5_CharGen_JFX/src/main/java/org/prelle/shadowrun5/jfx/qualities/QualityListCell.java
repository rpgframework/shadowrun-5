package org.prelle.shadowrun5.jfx.qualities;

import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class QualityListCell extends ListCell<Quality> {

	private final static String NORMAL_STYLE = "skill-cell";
	private final static String GROUP_STYLE = "skillgroup-cell";

	private QualityController control;
	private Quality data;
	private CharGenMode mode;

	private Label lineName;
	private Label lineDescription;
	private VBox  layout;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	//-------------------------------------------------------------------
	public QualityListCell(QualityController ctrl, CharGenMode mode) {
		this.control = ctrl;
		this.mode    = mode;
		lblType = new Label();
		lblType.getStyleClass().add("cost");
		StackPane.setAlignment(lblType, Pos.CENTER_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		lineDescription = new Label();
		layout   = new VBox(3);
		layout.getChildren().addAll(lineName, lineDescription);

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Quality item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		getStyleClass().remove(GROUP_STYLE);
		getStyleClass().remove(NORMAL_STYLE);

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
				getStyleClass().add(NORMAL_STYLE);
				switch (item.getType()) {
				case NEGATIVE:
					if (mode==CharGenMode.LEVELING)
						lblType.setText("-"+String.valueOf(item.getCost()*2));
					else
						lblType.setText("-"+String.valueOf(item.getCost()));
//					lblType.setStyle("-fx-text-fill: red");
					break;
				case POSITIVE:
					if (mode==CharGenMode.LEVELING)
						lblType.setText(String.valueOf(item.getCost()*2));
					else
						lblType.setText(String.valueOf(item.getCost()));
//					lblType.setStyle("-fx-text-fill: green");
					break;
				default:
				}

				lineName.setText(((Quality)item).getName());
				lineDescription.setText(item.getProductName()+" "+item.getPage());
				imgRecommended.setVisible(control.isRecommended((Quality) item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("quality:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

}