/**
 * 
 */
package org.prelle.shadowrun5.jfx.qualities;

import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.jfx.newwizard.SelectionCallback;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ChoiceType;
import org.prelle.shadowrun5.MentorSpirit;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.Quality.QualityType;
import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.actions.ShadowrunAction.Category;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import de.rpgframework.genericrpg.ModifyableValue;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("restriction")
public class QualitySelectionPane extends HBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".qualities");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private QualityController ctrl;
	private SelectionCallback parent;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;
	private CharGenMode mode;

	private ChoiceBox<Quality.QualityType> cbFilter;
	private Label hdAvailable;
	private Label hdSelected;
	private Label hdDescript;
	private ListView<Quality> lvAvailable;
	private ListView<ModifyableValue<?>> lvSelected;
	private VBox colDescription;
	private Label lblName;
	private Label lblPage;
	private Label lblDesc;
	private ObjectProperty<BasePluginData> selectedItem;
	
	private boolean hideThirdColumn;

	//--------------------------------------------------------------------
	public QualitySelectionPane(QualityController ctrl, ScreenManagerProvider provider, CharGenMode mode) {
		logger.warn("------------setMode to "+mode+"----------------------");
		this.ctrl = ctrl;
		this.provider = provider;
		this.mode = mode;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	public QualitySelectionPane(QualityController ctrl, ScreenManagerProvider provider, CharGenMode mode, boolean hide) {
		this.hideThirdColumn = hide;
		this.ctrl = ctrl;
		this.provider = provider;
		this.mode = mode;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		hdAvailable = new Label(UI.getString("label.available"));
		hdSelected  = new Label(UI.getString("label.selected"));
		hdDescript  = new Label(UI.getString("label.description"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdDescript.getStyleClass().add("text-subheader");
		//		hdAvailable.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdSelected.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdDescript.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdSelected.getStyleClass().add("text-subheader");
		cbFilter = new ChoiceBox<>();
		cbFilter.getItems().addAll(Quality.QualityType.values());
		cbFilter.setConverter(new StringConverter<Quality.QualityType>() {
			public String toString(QualityType object) { return object.getName(); }
			public QualityType fromString(String string) { return null; }
		});
		lvAvailable = new ListView<>();
		lvSelected  = new ListView<>();

		lvAvailable.setCellFactory(new Callback<ListView<Quality>, ListCell<Quality>>() {
			public ListCell<Quality> call(ListView<Quality> param) {
				return new QualityListCell(ctrl, mode);
			}
		});
//		lvSelected.setCellFactory(new Callback<ListView<QualityValue>, ListCell<QualityValue>>() {
//			public ListCell<QualityValue> call(ListView<QualityValue> param) {
//				return new QualityValueListCell(ctrl, lvSelected, provider);
//			}
//		});

		Label phSelected = new Label(UI.getString("skillvaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		lblName = new Label();
		lblName.getStyleClass().add("text-small-secondary");
		lblName.setStyle("-fx-font-weight: bold");
		lblPage = new Label();
		lblPage.setStyle("-fx-font-style: italic");
		lblDesc = new Label();
		lblDesc.setWrapText(true);

		colDescription = new VBox(5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-min-width: 20em; -fx-pref-width: 24em");
		lvSelected.setStyle("-fx-min-width: 15em; -fx-pref-width: 24em");
		colDescription.setStyle("-fx-pref-width: 20em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbFilter, lvAvailable);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(hdSelected, lvSelected);
		VBox column3 = new VBox(10);
		VBox.setVgrow(colDescription, Priority.ALWAYS);
		column3.getChildren().addAll(hdDescript, colDescription);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);
		colDescription.getChildren().addAll(lblName, lblPage, lblDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);
		if (!hideThirdColumn)
			getChildren().addAll(column3);
		column3.setStyle("-fx-max-width: 20em");

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		column1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column1, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> skillSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				skillSelectionChanged( ((QualityValue)n).getModifyable());
			}
		});
		lvSelected.setOnDragDropped(event -> dragFromQualityDropped(event));
		lvSelected.setOnDragOver(event -> dragFromQualityOver(event));
		lvAvailable.setOnDragDropped(event -> dragFromQualityValueDropped(event));
		lvAvailable.setOnDragOver(event -> dragFromQualityValueOver(event));
		cbFilter.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
	}

	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> selectedItemProperty() {
		return selectedItem;
	}

	//--------------------------------------------------------------------
	private void skillSelectionChanged(BasePluginData data) {
		if(selectedItem!=null) {
			selectedItem.set(data);
		}
		if (data!=null) {
			logger.debug("Selected "+data);
			lblName.setText(data.getName());
			lblPage.setText(data.getProductName()+" "+data.getPage());
			lblDesc.setText(data.getHelpText());
		}
		if (parent!=null)
			parent.showDescription(this, data);
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbFilter.getValue()!=null) {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities().stream().filter(qual -> qual.getType()==cbFilter.getValue()).collect(Collectors.toList()));
		} else {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities());
		}

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getQualities());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private <T> T letUserSelect(ChoiceBox<T> choices) {
		Label label = new Label(UI.getString("qualityselectpane.qualityoption_dialog.label"));
		label.getStyleClass().add("text-small-subheader");
		VBox content = new VBox(5);
		content.getChildren().addAll(label, choices);
		
		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, UI.getString("qualityselectpane.qualityoption_dialog.title"), content);
		if (close==CloseType.OK) {
			return choices.getValue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromQualityDropped(DragEvent event) {
		Runnable runner = null;
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("quality")) {
					Quality data = ShadowrunCore.getQuality(tail);
					if (data==null) {
						logger.warn("Cannot find quality for dropped quality id '"+tail+"'");						
					} else {
						// Some qualities need to be provide options first
						if (data.needsChoice()) {
							ChoiceType type = data.getSelect();
							logger.debug("select option "+type+" later");
							
							/*
							 * The following code needs to be in an extra platform thread
							 * to prevent the DragView image to stay visible when selecting
							 * the option - and prevent an exception later
							 */
							runner = new Runnable() {
								public void run() {
									logger.debug("select option "+type+" now");
									switch (type) {
									case ATTRIBUTE:
										ChoiceBox<Attribute> choicesA = new ChoiceBox<Attribute>();
										choicesA.setConverter(new StringConverter<Attribute>() {
											public String toString(Attribute object) {return object.getName();}
											public Attribute fromString(String string) {return null;}
										});
										choicesA.getItems().addAll(Attribute.primaryValues());
										choicesA.getItems().addAll(Attribute.RESONANCE, Attribute.MAGIC);
										Attribute selectA = letUserSelect(choicesA);
										ctrl.select(data, selectA);
										break;
									case SKILLGROUP:
										ChoiceBox<SkillGroup> choicesSG = new ChoiceBox<SkillGroup>();
										choicesSG.setConverter(new StringConverter<SkillGroup>() {
											public String toString(SkillGroup object) {return object.getName();}
											public SkillGroup fromString(String string) {return null;}
										});
										choicesSG.getItems().addAll(ctrl.getChoicesFor(data));
										SkillGroup selectSG = letUserSelect(choicesSG);
										ctrl.select(data, selectSG);
										break;
									case SKILL:
										ChoiceBox<Skill> choicesS = new ChoiceBox<Skill>();
										choicesS.setConverter(new StringConverter<Skill>() {
											public String toString(Skill object) {return object.getName();}
											public Skill fromString(String string) {return null;}
										});
										choicesS.getItems().addAll(ShadowrunCore.getSkills());
										Skill selectS = letUserSelect(choicesS);
										ctrl.select(data, selectS);
										break;
									case MATRIX_ACTION:
										ChoiceBox<ShadowrunAction> choicesAct = new ChoiceBox<ShadowrunAction>();
										choicesAct.setConverter(new StringConverter<ShadowrunAction>() {
											public String toString(ShadowrunAction object) {return object.getName();}
											public ShadowrunAction fromString(String string) {return null;}
										});
										choicesAct.getItems().addAll(ShadowrunCore.getActions(Category.MATRIX));
										ShadowrunAction selectAct = letUserSelect(choicesAct);
										ctrl.select(data, selectAct);
										break;
									case MENTOR_SPIRIT:
										ChoiceBox<MentorSpirit> choicesSpirit = new ChoiceBox<MentorSpirit>();
										choicesSpirit.setConverter(new StringConverter<MentorSpirit>() {
											public String toString(MentorSpirit object) {return object.getName();}
											public MentorSpirit fromString(String string) {return null;}
										});
										choicesSpirit.getItems().addAll(ShadowrunCore.getMentorSpirits());
										MentorSpirit selectSpirit = letUserSelect(choicesSpirit);
										ctrl.select(data, selectSpirit);
										break;
									default:
										logger.error("Don't know how to select "+type);
									}
								}
							};
						} else {
							ctrl.select(data);
						}
					} // if data==null else
				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
		
		if (runner!=null)
			Platform.runLater(runner);
	}

	//-------------------------------------------------------------------
	private Quality getQualityByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("quality")) {
				return ShadowrunCore.getQuality(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromQualityOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Quality qual = getQualityByEvent(event);
			if (qual!=null && qual.isFreeSelectable()) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragFromQualityValueDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			Quality data = getQualityByEvent(event);
			if (data==null) {
				logger.warn("Cannot find quality for dropped quality");						
			} else if (!data.isFreeSelectable()) {
				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
				if (model.hasQuality(data.getId())) {
					QualityValue ref = model.getQuality(data.getId());
					ctrl.deselect(ref);
				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromQualityValueOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Quality qual = getQualityByEvent(event);
			if (qual!=null && qual.isFreeSelectable()) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case QUALITY_AVAILABLE_CHANGED:
			refresh();
			break;
		case QUALITY_ADDED:
		case QUALITY_CHANGED:
		case QUALITY_REMOVED:
		case CHARACTER_CHANGED:
			logger.info("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	public void setSelectionCallback(SelectionCallback parent) {
		this.parent = parent;
	}

}
