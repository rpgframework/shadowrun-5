package org.prelle.shadowrun5.jfx.qualities;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.charctrl.QualityController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import de.rpgframework.genericrpg.ModifyableValue;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class QualityValueListCell extends ListCell<QualityValue> {

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private final static String NORMAL_STYLE = "quality-cell";

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".qualities");

	private QualityController charGen;
	private ListView<QualityValue> parent;
	private ScreenManagerProvider provider;

	private transient QualityValue data;

	private HBox layout;
	private Label name;
	private Label attrib;
	private Label total;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	private TilePane tiles;

	private Label  tfDescr;

	//-------------------------------------------------------------------
	public QualityValueListCell(QualityController charGen, ListView<QualityValue> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content		
		layout  = new HBox(5);
		name    = new Label();
		attrib  = new Label();
		total   = new Label();
		tfDescr = new Label();
		btnEdit = new Button("\uE1C2");
		btnDec  = new Button("-");
		lblVal  = new Label("?");
		btnInc  = new Button("+");

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent; ");
		btnInc.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");		
		
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name, attrib);		

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, tfDescr);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
//		VBox foo = new VBox(2);
//		foo.getChildren().addAll(total, tiles);
		layout.getChildren().addAll(bxCenter, tiles);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			charGen.increase((QualityValue)data);
			updateItem(data, false); 
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			charGen.decrease((QualityValue)data);
			updateItem(data, false); 
			parent.refresh();
		});
		btnEdit.setOnAction(event -> {
			editClicked((QualityValue)data);
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+((QualityValue)data).getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(QualityValue itemX, boolean empty) {
		super.updateItem(itemX, empty);

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
			data = itemX;

			QualityValue item = (QualityValue)itemX;
			Quality data = item.getModifyable();
			name.setText(item.getName());
//			attrib.setText(attr.getShortName()+" "+charGen.getModel().getAttribute(attr).getModifiedValue());
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			tfDescr.setText(item.getDescription());
			imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			lblType.setText(data.isFreeSelectable()?null:UI.getString("label.not_removable"));
			if (data.isFreeSelectable()) {
				btnEdit.setText("\uE1C2");
//				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
			}
//			logger.debug("Item = "+item);
//			logger.debug("Data = "+data);
			tiles.setVisible(data.getMax()>1 && data.isFreeSelectable());
			lblVal.setText(" "+String.valueOf(item.getPoints())+" ");
			int sum = item.getModifiedValue();
//			total.setText(String.format(UI.getString("skillvaluelistview.skillvaluelistcell.total"), sum));
			btnDec.setDisable(!charGen.canBeDecreased(item));
			btnInc.setDisable(!charGen.canBeIncreased(item));
			setGraphic(stack);
			
//			logger.debug("btnEdit = "+btnEdit.getStyleClass());
//			if (btnEdit.getScene()!=null)
//				logger.debug("CSS1 = "+btnEdit.getScene().getStylesheets());
//			if (getScene()!=null)
//				logger.debug("CSS2 = "+getScene().getStylesheets());
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(QualityValue ref) {
		logger.debug("editClicked");

		TextField tf = new TextField(ref.getDescription());
		// Prevent entering XML relevant chars
		tf.textProperty().addListener( (ov,o,n) -> {
			if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); tf.setText(n); }
			if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); tf.setText(n); }
			if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); tf.setText(n); }
			if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); tf.setText(n); }
		});
		tf.setOnAction(event -> {
			ManagedScreen screen = (ManagedScreen) tf.getParent().getParent().getParent().getParent();
			logger.debug("Action on "+screen);
//			screen.impl_navigClicked(CloseType.OK, event);
		});
		CloseType close = provider.getScreenManager().showAlertAndCall(
				AlertType.QUESTION, 
				UI.getString("qualitylistview.namedialog.title"), 
				tf);
		if (close==CloseType.OK) {
			ref.setDescription(tf.getText());
			tfDescr.setText(tf.getText());
		}

		
//		QualitySpecializationPane dia = new QualitySpecializationPane(charGen, data);
//
//		provider.getScreenManager().showAlertAndCall(
//				AlertType.NOTIFICATION, 
//				UI.getString("skillselectpane.specialization.dialog.header"), 
//				dia);
//		GenerationEventDispatcher.removeListener(dia);
//		updateItem(data, false);
	}

}