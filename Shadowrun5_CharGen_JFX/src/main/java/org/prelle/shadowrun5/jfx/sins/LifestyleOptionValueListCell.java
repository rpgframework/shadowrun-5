/**
 * 
 */
package org.prelle.shadowrun5.jfx.sins;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.shadowrun5.LifestyleOptionValue;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.modifications.LifestyleModification;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class LifestyleOptionValueListCell extends ListCell<LifestyleOptionValue> {

	private Label lblName;
	private Label lblAddition;
	private Label lblCost;
	private StackPane stack;
	
	private LifestyleOptionValue data;

	//-------------------------------------------------------------------
	public LifestyleOptionValueListCell() {
		initComponents();
		initLayout();
		this.setOnDragDetected(event -> dragStarted(event));
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName    = new Label();
		lblAddition = new Label();
		lblCost   = new Label();
		
		lblName.getStyleClass().add("text-small-subheader");
		lblCost.getStyleClass().add("text-subheader");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		VBox box = new VBox();
		box.getChildren().addAll(lblName, lblAddition);
		
		stack = new StackPane();
		stack.getChildren().addAll(lblCost, box);
		StackPane.setAlignment(box, Pos.TOP_LEFT);
		StackPane.setAlignment(lblCost, Pos.TOP_RIGHT);
//		stack.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	public static String getLifestyleModificationString(Collection<Modification> mods) {
		List<String> ret = new ArrayList<>();
		for (Modification mod : mods) {
			if (mod instanceof LifestyleCostModification) {
				LifestyleCostModification lMod = (LifestyleCostModification)mod;
				if (lMod.getPercent()!=0) {
					if (lMod.getPercent()>0)
						ret.add("+"+lMod.getPercent()+"%");
					else
						ret.add(lMod.getPercent()+"%");
				} else {
					if (lMod.getFixed()>0)
						ret.add("+"+lMod.getFixed()+"\u00A5");
					else
						ret.add(lMod.getFixed()+"\u00A5");
				}
			} else if (mod instanceof LifestyleModification) {
//				LifestyleModification lMod = (LifestyleModification)mod;
//				if (lMod.getValue()>0)
//					ret.add(lMod.getAttribute().getName()+" +"+lMod.getValue());
//				else
//					ret.add(lMod.getAttribute().getName()+" "+lMod.getValue());
			} else
				ret.add(mod.getClass().getSimpleName());
		}
		return String.join(", ", ret);
	}

	//-------------------------------------------------------------------
	public void updateItem(LifestyleOptionValue item, boolean empty) {
		super.updateItem(item, empty);
		data = item;
		
		if (empty) {
			setGraphic(null);
			setText(null);
		} else {
			lblName.setText(item.getOption().getName());
			lblAddition.setText(item.getOption().getProductNameShort()+" "+item.getOption().getPage());
			lblCost.setText(getLifestyleModificationString(item.getOption().getModifications()));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);
        
        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        if (data==null)
        	return;
        content.putString("lifestyleoption:"+data.getOption().getId());        	
        db.setContent(content);
       
        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);
        
        event.consume();	
    }

}
