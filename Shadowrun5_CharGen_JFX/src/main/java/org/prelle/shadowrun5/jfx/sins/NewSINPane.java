package org.prelle.shadowrun5.jfx.sins;

import java.util.PropertyResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.charctrl.SINController;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class NewSINPane extends HBox {

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SINController control;
	private NavigButtonControl btnControl;

	private Label    lblNuyenFree;
	private TextField tfName;
	private ChoiceBox<SIN.Quality> cbQuality;
	private Label    lblCost;
	private TextArea taDesc;

	//-------------------------------------------------------------------
	public NewSINPane(SINController control, NavigButtonControl ctrl, ShadowrunCharacter model) {
		this.control = control;
		btnControl   = ctrl;

		initComponents();
		initLayout();
		initInteractivity();

		lblNuyenFree.setText(model.getNuyen()+" ");
		cbQuality.getSelectionModel().select(SIN.Quality.ANYONE);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();
		cbQuality = new ChoiceBox<>();
		cbQuality.getItems().addAll(Quality.getSelectableValues());
		taDesc = new TextArea();
		lblCost= new Label();

		lblNuyenFree = new Label();
		lblNuyenFree.getStyleClass().add("text-header");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblName = new Label(UI.getString("label.name"));
		Label lblQual = new Label(UI.getString("label.quality"));
		Label lblDesc = new Label(UI.getString("label.description"));

		HBox qualLine = new HBox(cbQuality, lblCost);
		qualLine.setStyle("-fx-spacing: 1em");

		VBox layout = new VBox();
		layout.getChildren().addAll(lblName, tfName, lblQual, qualLine, lblDesc, taDesc);
		VBox.setMargin(lblQual, new Insets(20,0,0,0));
		VBox.setMargin(lblDesc, new Insets(20,0,0,0));

		Label heaNuyen      = new Label(UI.getString("label.nuyen"));
		VBox points = new VBox();
		points.setAlignment(Pos.TOP_CENTER);
		points.getStyleClass().add("section-bar");
		points.setStyle("-fx-pref-width: 16em");
		points.getChildren().addAll(heaNuyen, lblNuyenFree);

		getChildren().addAll(points, layout);
		setStyle("-fx-spacing: 2em");
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		boolean enoughText = tfName.getText()!=null && tfName.getText().length()>0;
		boolean canSelect  = control.canCreateNewSIN(cbQuality.getValue());
		btnControl.setDisabled(CloseType.OK, !(enoughText && canSelect));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbQuality.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)-> {
			if (n!=null) {
				lblCost.setText("\u00A5 "+(2500*n.getValue()));
				updateOKButton();
			}
		});
		tfName.textProperty().addListener( (ov,o,n) -> updateOKButton());

	}

	//-------------------------------------------------------------------
	public Object[] getData() {
		return new Object[]{tfName.getText(), cbQuality.getValue(), taDesc.getText()};
	}

}