/**
 *
 */
package org.prelle.shadowrun5.jfx.skills;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.newwizard.SelectionCallback;

import de.rpgframework.genericrpg.ModifyableValue;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class SkillSelectionPane extends HBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SkillController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ChoiceBox<SkillType> cbType;
	private Label hdAvailable;
	private Label hdSelected;
	private Label hdDescript;
	private ListView<BasePluginData> lvAvailable;
	private ListView<ModifyableValue<?>> lvSelected;
	private VBox colDescription;
	private Label lblName;
	private Label lblPage;
	private Label lblDesc;
	private ObjectProperty<BasePluginData> selectedItem;
	private boolean hideThird;
	private SelectionCallback callback;

	//--------------------------------------------------------------------
	public SkillSelectionPane(SkillController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	public SkillSelectionPane(SkillController ctrl, ScreenManagerProvider provider, SelectionCallback callback) {
		this.ctrl = ctrl;
		this.provider = provider;
		this.hideThird = true;
		this.callback = callback;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		hdAvailable = new Label(UI.getString("label.available"));
		hdSelected  = new Label(UI.getString("label.selected"));
		hdDescript  = new Label(UI.getString("label.description"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdDescript.getStyleClass().add("text-subheader");
		//		hdAvailable.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdSelected.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdDescript.setStyle("-fx-text-fill: textcolor-highlight-primary");

		cbType = new ChoiceBox<>();
		cbType.setConverter(new StringConverter<Skill.SkillType>() {
			public String toString(SkillType type) { return type.getName();	}
			public SkillType fromString(String arg0) {return null;}
		});
		cbType.getItems().addAll(SkillType.values());
		cbType.getItems().remove(SkillType.NOT_SET);
		cbType.getItems().remove(SkillType.ACTION);

		lvAvailable = new ListView<>();
		lvSelected  = new ListView<>();

		lvAvailable.setCellFactory(new Callback<ListView<BasePluginData>, ListCell<BasePluginData>>() {
			public ListCell<BasePluginData> call(ListView<BasePluginData> param) {
				return new SkillListCell(ctrl);
			}
		});
		lvSelected.setCellFactory(new Callback<ListView<ModifyableValue<?>>, ListCell<ModifyableValue<?>>>() {
			public ListCell<ModifyableValue<?>> call(ListView<ModifyableValue<?>> param) {
				return new SkillValueListCell(ctrl, lvSelected, provider);
			}
		});

		Label phSelected = new Label(UI.getString("skillvaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		lblName = new Label();
		lblName.getStyleClass().add("text-small-secondary");
		lblName.setStyle("-fx-font-weight: bold");
		lblPage = new Label();
		lblPage.setStyle("-fx-font-style: italic");
		lblDesc = new Label();
		lblDesc.setWrapText(true);

		colDescription = new VBox(5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-min-width: 13em");
		lvSelected.setStyle("-fx-min-width: 21em; -fx-pref-width: 32em");
		colDescription.setStyle("-fx-pref-width: 20em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbType, lvAvailable);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(hdSelected, lvSelected);
		VBox column3 = new VBox(10);
		VBox.setVgrow(colDescription, Priority.ALWAYS);
		column3.getChildren().addAll(hdDescript, colDescription);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);
		colDescription.getChildren().addAll(lblName, lblPage, lblDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);
		if (!hideThird)
			getChildren().add(column3);
		column3.setStyle("-fx-max-width: 20em");

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			refresh();
		});
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> skillSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				if (n instanceof SkillValue) {
					skillSelectionChanged( ((SkillValue)n).getModifyable());
				} else {
					skillSelectionChanged( ((SkillGroupValue)n).getModifyable());
				}
			}
		});
		lvSelected.setOnDragDropped(event -> dragFromSkillDropped(event));
		lvSelected.setOnDragOver(event -> dragFromSkillOver(event));
	}

	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> selectedItemProperty() {
		return selectedItem;
	}

	//--------------------------------------------------------------------
	private void skillSelectionChanged(BasePluginData data) {
		if(selectedItem!=null) {
			selectedItem.set(data);
		}
		if (data!=null) {
			logger.debug("Selected "+data);
			lblName.setText(data.getName());
			lblPage.setText(data.getProductName()+" "+data.getPage());
			lblDesc.setText(data.getHelpText());
			if (data instanceof SkillGroup) {
				StringBuffer buf = new StringBuffer();
				for (Skill skill : ShadowrunCore.getSkills((SkillGroup) data)) {
					buf.append(skill.getName()+"\n");
				}
				lblDesc.setText(buf.toString());
			}
			if (callback!=null)
				callback.showDescription(this, data);
		}
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbType.getValue()==null) {
			lvAvailable.getItems().addAll(ctrl.getAvailableSkillGroups());
			lvAvailable.getItems().addAll(ctrl.getAvailableSkills());
		} else {
			lvAvailable.getItems().addAll(ctrl.getAvailableSkillGroups(cbType.getValue()));
			lvAvailable.getItems().addAll(ctrl.getAvailableSkills(cbType.getValue()));
		}

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getSkillGroupValues());
		lvSelected.getItems().addAll(model.getSkillValues(false));
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private String askForSkillName() {
		Label label = new Label(UI.getString("skillselectpane.skillname_dialog.label"));
		label.getStyleClass().add("text-small-subheader");
		TextField tfName = new TextField();
		VBox content = new VBox(5);
		content.getChildren().addAll(label, tfName);

		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, UI.getString("skillselectpane.skillname_dialog.title"), content);
		if (close==CloseType.OK) {
			String name = tfName.getText();
			logger.debug("Name to return = "+name);
			if (name!=null && name.trim().length()>0) {
				logger.debug("1");
				return name.trim();
			}
			logger.debug("2");
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromSkillDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("skill")) {
					Skill data = ShadowrunCore.getSkill(tail);
					if (data==null) {
						logger.warn("Cannot find skill for dropped skill id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						// Special handling for knowledge and language skills
						if (data.getType()==SkillType.KNOWLEDGE || data.getType()==SkillType.LANGUAGE) {
							logger.debug(data.getName()+" must be entered by user");
							Platform.runLater(new Runnable(){
								public void run() {
									String name = askForSkillName();
									logger.debug("returned "+name);
									if (name!=null) {
										ctrl.selectKnowledgeOrLanguage(data, name);
									} else
										logger.debug("Ignore knowledge or language skill because user did not enter a name");
								}});
						} else {
							ctrl.select(data);
						}
						return;
					}
				} else
					if (head.equals("skillgrp")) {
						SkillGroup data = ShadowrunCore.getSkillGroup(tail);
						if (data!=null) // && control.canBeAdded(res))
							ctrl.select(data);
					}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromSkillOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SKILL_ADDED:
		case SKILL_CHANGED:
		case SKILL_REMOVED:
		case SKILLS_AVAILABLE_CHANGED:
		case SKILLGROUP_ADDED:
		case SKILLGROUP_CHANGED:
		case SKILLGROUP_REMOVED:
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

}

class SkillListCell extends ListCell<BasePluginData> {

	private final static String NORMAL_STYLE = "skill-cell";
	private final static String GROUP_STYLE = "skillgroup-cell";

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SkillController control;
	private BasePluginData data;

	private Label lineName;
	private Label lineDescription;
	private VBox  layout;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	//-------------------------------------------------------------------
	public SkillListCell(SkillController ctrl) {
		this.control = ctrl;
		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		lineDescription = new Label();
		layout   = new VBox(3);
		layout.getChildren().addAll(lineName, lineDescription);

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(BasePluginData item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		getStyleClass().remove(GROUP_STYLE);
		getStyleClass().remove(NORMAL_STYLE);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (item instanceof Skill) {
				getStyleClass().add(NORMAL_STYLE);
				lblType.setText(null);
				lineName.setText(((Skill)item).getName());
				lineDescription.setText(item.getProductName()+" "+item.getPage());
				imgRecommended.setVisible(control.isRecommended((Skill) item));
			} else {
				getStyleClass().add(GROUP_STYLE);
				lblType.setText(UI.getString("skillselectpane.celltype.group"));
				lineName.setText(((SkillGroup)item).getName());
				lineDescription.setText(item.getProductName()+" "+item.getPage());
				imgRecommended.setVisible(control.isRecommended((SkillGroup) item));
			}
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		if (data instanceof Skill)
			content.putString("skill:"+data.getId());
		else if (data instanceof SkillGroup)
			content.putString("skillgrp:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

}

class SkillValueListCell extends ListCell<ModifyableValue<?>> {

	private final static String NORMAL_STYLE = "skill-cell";
	private final static String GROUP_STYLE = "skillgroup-cell";

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SkillController charGen;
	private ListView<ModifyableValue<?>> parent;
	private ScreenManagerProvider provider;

	private transient ModifyableValue<?> data;

	private HBox layout;
	private Label name;
	private Label attrib;
	private Label total;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	private VBox  bxSpecial;

	//-------------------------------------------------------------------
	public SkillValueListCell(SkillController charGen, ListView<ModifyableValue<?>> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content
		layout  = new HBox(5);
		name    = new Label();
		attrib  = new Label();
		total   = new Label();
		bxSpecial = new VBox(1);
		btnEdit = new Button("\uE1C2");
		btnDec  = new Button("-");
		lblVal  = new Label("?");
		btnInc  = new Button("+");

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR5Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent");

		//		setStyle("-fx-pref-width: 24em");
		layout.getStyleClass().add("content");
		layout.setStyle("-fx-min-width: 21em; -fx-pref-width: 28em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name, attrib);

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, bxSpecial);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		VBox foo = new VBox(2);
		foo.getChildren().addAll(total, tiles);
		layout.getChildren().addAll(bxCenter, foo);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			if (data instanceof SkillValue)
				charGen.increase((SkillValue)data);
			else
				charGen.increase((SkillGroupValue)data);
			updateItem(data, false);
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			if (data instanceof SkillValue)
				charGen.decrease((SkillValue)data);
			else
				charGen.decrease((SkillGroupValue)data);
			updateItem(data, false);
			parent.refresh();
		});
		btnEdit.setOnAction(event -> {
			if (data instanceof SkillValue)
				editClicked((SkillValue)data);
			else
				editClicked((SkillGroupValue)data);
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = null;
		if (data instanceof SkillValue)
			id = "skill:"+((SkillValue)data).getModifyable().getId();
		else
			id = "skillgroup:"+((SkillGroupValue)data).getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ModifyableValue<?> itemX, boolean empty) {
		super.updateItem(itemX, empty);

		getStyleClass().remove(GROUP_STYLE);
		getStyleClass().remove(NORMAL_STYLE);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			data = itemX;

			if (itemX instanceof SkillValue) {
				getStyleClass().add(NORMAL_STYLE);
				SkillValue item = (SkillValue)itemX;
				Attribute attr = item.getModifyable().getAttribute1();
				name.setText(item.getName());
				attrib.setText(attr.getShortName()+" "+charGen.getModel().getAttribute(attr).getModifiedValue());
				//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
				bxSpecial.getChildren().clear();
				if (item.getSkillSpecializations().isEmpty()) {
					Label phSpecial = new Label(UI.getString("skillvaluelistview.skillvaluelistcell.placeholder"));
					phSpecial.setStyle("-fx-font-style: italic");
					bxSpecial.getChildren().add(phSpecial);
				} else {
					for (SkillSpecializationValue spec : item.getSkillSpecializations()) {
						Label phSpecial = new Label(spec.getName());
						//					phSpecial.setStyle("-fx-font-style: italic");
						bxSpecial.getChildren().add(phSpecial);
					}
				}
				imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
				lblType.setText(null);
				lblVal.setText(" "+String.valueOf(item.getModifiedValue())+" ");
				int sum = charGen.getModel().getAttribute(attr).getModifiedValue() + item.getModifiedValue();
				total.setText(String.format(UI.getString("skillvaluelistview.skillvaluelistcell.total"), sum));
				btnEdit.setText("\uE1C2");
				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
				btnEdit.setVisible(item.getModifyable().getType()!=SkillType.KNOWLEDGE && item.getModifyable().getType()!=SkillType.LANGUAGE);
				btnEdit.setDisable(!charGen.canSpecializeIn(item));
				btnDec.setDisable(!charGen.canBeDecreased(item));
				btnInc.setDisable(!charGen.canBeIncreased(item));
			} else if (itemX instanceof SkillGroupValue) {
				getStyleClass().add(GROUP_STYLE);
				SkillGroupValue item = (SkillGroupValue)itemX;
				name.setText(item.getModifyable().getName());
				attrib.setText(null);
				bxSpecial.getChildren().clear();
				lblVal.setText(" "+String.valueOf(item.getModifiedValue())+" ");
				total.setText(null);
				btnEdit.setText("\uE16B");
				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.breakgroup")));
				btnEdit.setDisable(!charGen.canBreakSkillGroup(item));
				btnDec.setDisable(!charGen.canBeDecreased(item));
				btnInc.setDisable(!charGen.canBeIncreased(item));
				imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
				lblType.setText(UI.getString("skillselectpane.celltype.group"));
			}
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(SkillValue data) {
		logger.debug("editClicked");

		SkillSpecializationPane dia = new SkillSpecializationPane(charGen, data);

		provider.getScreenManager().showAlertAndCall(
				AlertType.NOTIFICATION,
				UI.getString("skillselectpane.specialization.dialog.header"),
				dia);
		GenerationEventDispatcher.removeListener(dia);
		updateItem(data, false);
	}

	//-------------------------------------------------------------------
	private void editClicked(SkillGroupValue data) {
		logger.debug("editClicked - break skill group");

		CloseType result = provider.getScreenManager().showAlertAndCall(AlertType.CONFIRMATION,
				UI.getString("skillvaluelistview.breakskill.title"),
				UI.getString("skillvaluelistview.breakskill.content"));
		if (result==CloseType.OK || result==CloseType.YES)
			charGen.breakSkillGroup(data);
	}

}

class SkillSpecializationPane extends VBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SkillController control;
	private SkillValue sVal;
	private Skill skill;

	private CheckBox[] boxes;
	private Map<BooleanProperty, SkillSpecialization> specByProp;

	//--------------------------------------------------------------------
	public SkillSpecializationPane(SkillController ctrl, SkillValue sVal)  {
		super(5);
		this.control = ctrl;
		this.sVal    = sVal;
		this.skill   = sVal.getModifyable();
		specByProp   = new HashMap<>();

		initComponents();
		initLayout();
		initInteractivity();
		refresh();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		boxes = new CheckBox[skill.getSpecializations().size()];
		int i=0;
		for (SkillSpecialization spec : skill.getSpecializations()) {
			boxes[i] = new CheckBox(spec.getName());
			boxes[i].setUserData(spec);
			specByProp.put(boxes[i].selectedProperty(), spec);
			i++;
		}
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label label = new Label(String.format(UI.getString("skillselectpane.specialization.dialog.text"), skill.getName()));
		label.setWrapText(true);
		this.getChildren().add(label);
		for (int i=0; i<boxes.length; i++) {
			this.getChildren().add(boxes[i]);
		}
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		for (int i=0; i<boxes.length; i++) {
			boxes[i].selectedProperty().addListener( (ov,o,n) -> {
				SkillSpecialization spec = specByProp.get(ov);
				if (n)
					control.select(sVal, spec);
				else
					control.deselect(sVal, spec);
			});
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		for (int i=0; i<boxes.length; i++) {
			SkillSpecialization spec = (SkillSpecialization)boxes[i].getUserData();
			boolean selected = sVal.hasSpecialization(spec);
			boxes[i].setSelected(selected);
			if (selected)
				boxes[i].setDisable(!control.canBeDeselected(sVal, spec));
			else
				boxes[i].setDisable(!control.canBeSelected(sVal, spec));
		}

	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SKILL_ADDED:
		case SKILL_CHANGED:
		case SKILL_REMOVED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}
}