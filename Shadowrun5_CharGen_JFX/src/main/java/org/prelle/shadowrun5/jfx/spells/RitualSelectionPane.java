/**
 * 
 */
package org.prelle.shadowrun5.jfx.spells;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.Ritual;
import org.prelle.shadowrun5.RitualFeature;
import org.prelle.shadowrun5.RitualFeatureReference;
import org.prelle.shadowrun5.RitualValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.charctrl.RitualController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class RitualSelectionPane extends ThreeColumnPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private RitualController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<Ritual> lvAvailable;
	private ListView<RitualValue> lvSelected;
	private Label descHeading;
	private Label descRef;
	private Label descFeat;
	private Label description;

	//-------------------------------------------------------------------
	/**
	 */
	public RitualSelectionPane(RitualController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;

		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setColumn1Header(UI.getString("label.available"));
		setColumn2Header(UI.getString("label.selected"));
		setColumn3Header(UI.getString("label.description"));

		lvAvailable = new ListView<Ritual>();
		lvAvailable.setCellFactory(new Callback<ListView<Ritual>, ListCell<Ritual>>() {
			public ListCell<Ritual> call(ListView<Ritual> param) {
				RitualListCell cell =  new RitualListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem());
				});
				return cell;
			}
		});
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n));
		lvSelected  = new ListView<RitualValue>();
		lvSelected.setCellFactory(new Callback<ListView<RitualValue>, ListCell<RitualValue>>() {
			public ListCell<RitualValue> call(ListView<RitualValue> param) {
				RitualValueListCell cell =  new RitualValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) 
				describe(n.getModifyable());
		});
		
		Label phAvailable = new Label(UI.getString("ritualselectpane.placeholder.available"));
		Label phSelected  = new Label(UI.getString("ritualselectpane.placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);

		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descFeat    = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox column1 = new VBox(10);
		column1.getChildren().addAll(lvAvailable);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		
		VBox column3 = new VBox(10);
		column3.getChildren().addAll(descHeading, descRef, descFeat, description);
		VBox.setVgrow(description, Priority.ALWAYS);

		setColumn1Node(column1);
		setColumn2Node(lvSelected);
		setColumn3Node(column3);
		setHeadersVisible(true);
		
		HBox.setHgrow(column1.getParent(), Priority.SOMETIMES);
		HBox.setHgrow(lvSelected.getParent(), Priority.SOMETIMES);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailableRituals());

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getRituals());
	}

	//--------------------------------------------------------------------
	private void describe(Ritual data) {
		if (data==null)
			return;
		descHeading.setText(data.getName().toUpperCase());
		descRef.setText(data.getProductName()+" "+data.getPage());
		descFeat.setText(RitualListCell.makeFeatureString(data));
		description.setText(data.getHelpText());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case RITUALS_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

}

class RitualListCell extends ListCell<Ritual> {
	
	private final static Map<RitualFeature, Image> IMAGES = new HashMap<>();
	
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	
	private HBox bxLayout;
	private VBox bxData;
	
	//-------------------------------------------------------------------
	static {
//		IMAGES.put(Category.COMBAT   , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.combat.png")));
//		IMAGES.put(Category.HEALTH   , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.health.png")));
//		IMAGES.put(Category.DETECTION, new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.detection.png")));
//		IMAGES.put(Category.ILLUSION , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.illusion.png")));
//		IMAGES.put(Category.MANIPULATION, new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.manipulation.png")));
	}
	
	//-------------------------------------------------------------------
	public RitualListCell() {
		initComponents();
		initLayout();
		initStyle();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Ritual item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			lblName.setText(item.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(makeFeatureString(item));
//			iview.setImage(IMAGES.get(item.getCategory()));
		}
	}
	
	static String makeFeatureString(Ritual ritual) {
		StringBuffer buf = new StringBuffer();
		Iterator<RitualFeatureReference> it = ritual.getFeatures().iterator();
		while (it.hasNext()) {
			RitualFeatureReference ref = it.next();
			buf.append(ref.getFeature().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		return buf.toString();
	}
}
