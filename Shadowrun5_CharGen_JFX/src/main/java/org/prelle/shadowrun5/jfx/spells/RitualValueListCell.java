package org.prelle.shadowrun5.jfx.spells;

import java.util.Iterator;

import org.prelle.shadowrun5.Ritual;
import org.prelle.shadowrun5.RitualFeature;
import org.prelle.shadowrun5.RitualFeatureReference;
import org.prelle.shadowrun5.RitualValue;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class RitualValueListCell extends ListCell<RitualValue> {
	
//	private final static Map<RitualFeature, Image> IMAGES = new HashMap<>();
	
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	private Label lblLine2;
	
	private HBox bxLayout;
	private VBox bxData;
	
	//-------------------------------------------------------------------
	static {
//		IMAGES.put(Category.COMBAT   , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.combat.png")));
//		IMAGES.put(Category.HEALTH   , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.health.png")));
//		IMAGES.put(Category.DETECTION, new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.detection.png")));
//		IMAGES.put(Category.ILLUSION , new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.illusion.png")));
//		IMAGES.put(Category.MANIPULATION, new Image(ClassLoader.getSystemResourceAsStream("images/shadowrun/ritualtype.manipulation.png")));
	}
	
	//-------------------------------------------------------------------
	public RitualValueListCell() {
		initComponents();
		initLayout();
		initStyle();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		lblLine2 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1, lblLine2);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(RitualValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			Ritual ritual = item.getModifyable();
			lblName.setText(ritual.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(makeFeatureString(ritual));
//			iview.setImage(IMAGES.get(ritual.getCategory()));
		}
	}
	
	//-------------------------------------------------------------------
	private static String makeFeatureString(Ritual ritual) {
		StringBuffer buf = new StringBuffer();
		Iterator<RitualFeatureReference> it = ritual.getFeatures().iterator();
		while (it.hasNext()) {
			RitualFeatureReference ref = it.next();
			buf.append(ref.getFeature().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		return buf.toString();
	}
}