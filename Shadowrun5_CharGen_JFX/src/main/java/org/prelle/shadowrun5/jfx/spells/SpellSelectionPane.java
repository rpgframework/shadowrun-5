/**
 * 
 */
package org.prelle.shadowrun5.jfx.spells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellValue;
import org.prelle.shadowrun5.Spell.Category;
import org.prelle.shadowrun5.charctrl.SpellController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SpellSelectionPane extends ThreeColumnPane implements GenerationEventListener {

	private static PropertyResourceBundle CORE = ShadowrunCore.getI18nResources();

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private SpellController ctrl;
	private boolean alchemistic;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ChoiceBox<Spell.Category> cbCategory;
	private ListView<Spell> lvAvailable;
	private ListView<SpellValue> lvSelected;
	private Label descHeading;
	private Label descRef;
	private Label descFeat;
	private Label descType, descRange, descDur, descDrain;
	private Label description;
	
	private Spell selectedAvailable;

	//-------------------------------------------------------------------
	/**
	 */
	public SpellSelectionPane(SpellController ctrl, ScreenManagerProvider provider, boolean alchemistic) {
		this.ctrl = ctrl;
		this.provider = provider;
		this.alchemistic = alchemistic;

		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
		cbCategory.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbCategory = new ChoiceBox<>();
		for (Spell.Category cat : Spell.Category.values())
			cbCategory.getItems().add(cat);
		cbCategory.setConverter(new StringConverter<Spell.Category>() {
			public String toString(Category object) { return object.getName();}
			public Category fromString(String string) { return null; }
		});
		cbCategory.setMaxWidth(Double.MAX_VALUE);
		
		setColumn1Header(UI.getString("spellselectpane.available"));
		setColumn2Header(UI.getString("spellselectpane.selected"));
		setColumn3Header(UI.getString("spellselectpane.description"));

		lvAvailable = new ListView<Spell>();
		lvAvailable.setCellFactory(new Callback<ListView<Spell>, ListCell<Spell>>() {
			public ListCell<Spell> call(ListView<Spell> param) {
				SpellListCell cell =  new SpellListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem(), alchemistic);
				});
				return cell;
			}
		});
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n));
		lvSelected  = new ListView<SpellValue>();
		lvSelected.setCellFactory(new Callback<ListView<SpellValue>, ListCell<SpellValue>>() {
			public ListCell<SpellValue> call(ListView<SpellValue> param) {
				SpellValueListCell cell =  new SpellValueListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				describe(n.getModifyable());
			});
		
		Label phAvailable = new Label(UI.getString("spellselectpane.placeholder.available"));
		Label phSelected  = new Label(UI.getString("spellselectpane.placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);

		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descFeat    = new Label();
		descType    = new Label();
		descRange   = new Label();
		descDur     = new Label();
		descDrain   = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblType  = new Label(UI.getString("label.type")+":");
		Label lblRange = new Label(UI.getString("label.range")+":");
		Label lblDur   = new Label(UI.getString("label.duration")+":");
		Label lblDrain = new Label(UI.getString("label.drain")+":");
		lblType.setStyle("-fx-font-weight: bold");
		lblRange.setStyle("-fx-font-weight: bold");
		lblDur.setStyle("-fx-font-weight: bold");
		lblDrain.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblType , 0, 0);
		descGrid.add(descType , 1, 0);
		descGrid.add( lblRange, 2, 0);
		descGrid.add(descRange, 3, 0);
		descGrid.add( lblDur  , 0, 1);
		descGrid.add(descDur  , 1, 1);
		descGrid.add( lblDrain, 2, 1);
		descGrid.add(descDrain, 3, 1);
		descGrid.setHgap(5);
		
		VBox column1 = new VBox(10);
		column1.getChildren().addAll(cbCategory, lvAvailable);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		
		VBox column3 = new VBox(10);
		column3.getChildren().addAll(descHeading, descRef, descFeat, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);

		setColumn1Node(column1);
		setColumn2Node(lvSelected);
		setColumn3Node(column3);
		setHeadersVisible(true);
		
		HBox.setHgrow(column1.getParent(), Priority.SOMETIMES);
		HBox.setHgrow(lvSelected.getParent(), Priority.SOMETIMES);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbCategory.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> selectedAvailable=n);
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		lvAvailable.getItems().clear();
		if (cbCategory.getSelectionModel().getSelectedItem()==null)
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(alchemistic));
		else
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(cbCategory.getSelectionModel().getSelectedItem(), alchemistic));

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getSpells(alchemistic));

		lvSelected.setOnDragDropped(event -> dragFromSpellDropped(event));
		lvSelected.setOnDragOver(event -> dragFromSpellOver(event));
		lvAvailable.setOnDragDropped(event -> dragFromSpellValueDropped(event));
		lvAvailable.setOnDragOver(event -> dragFromSpellValueOver(event));
	}

	//--------------------------------------------------------------------
	private void describe(Spell data) {
		if (data==null)
			return;
		descHeading.setText(data.getName().toUpperCase());
		descRef.setText(data.getProductName()+" "+data.getPage());
		descFeat.setText(SpellListCell.makeFeatureString(data));
		descType.setText(data.getType().getName());
		descDur .setText(data.getDuration().getName());
		if (data.getRange()==null) {
			System.err.println("No range for "+data);
		} else {
		descRange.setText(data.getRange().getName());
		}
		if (data.getDrain()<0)
			descDrain.setText(CORE.getString("label.force")+" "+data.getDrain());
		else if (data.getDrain()>0)
			descDrain.setText(CORE.getString("label.force")+" +"+data.getDrain());
		else 
			descDrain.setText(CORE.getString("label.force"));
		
		description.setText(data.getHelpText());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SPELLS_AVAILABLE_CHANGED:
		case SPELL_ADDED:
		case SPELL_REMOVED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	private void dragFromSpellDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("spell")) {
					Spell data = ShadowrunCore.getSpell(tail);
					if (data==null) {
						logger.warn("Cannot find quality for dropped quality id '"+tail+"'");						
					} else {
						ctrl.select(data, alchemistic);
					} // if data==null else
				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private Spell getSpellByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("spell")) {
				return ShadowrunCore.getSpell(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromSpellOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Spell qual = getSpellByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragFromSpellValueDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			Spell data = getSpellByEvent(event);
			if (data==null) {
				logger.warn("Cannot find quality for dropped quality");						
//			} else if (!data.isFreeSelectable()) {
//				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
				if (model.hasSpell(data.getId())) {
					SpellValue ref = model.getSpell(data.getId());
					ctrl.deselect(ref);
				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromSpellValueOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Spell qual = getSpellByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Spell getSelectedAvailable() {
		return selectedAvailable;
	}

}
