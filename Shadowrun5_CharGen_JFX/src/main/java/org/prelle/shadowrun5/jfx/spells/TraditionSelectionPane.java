/**
 * 
 */
package org.prelle.shadowrun5.jfx.spells;

import java.util.PropertyResourceBundle;

import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Tradition;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;
import org.prelle.shadowrun5.jfx.newwizard.SelectionCallback;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class TraditionSelectionPane extends ThreeColumnPane implements GenerationEventListener {

	private static PropertyResourceBundle UI = SR5Constants.RES;
	
	private ShadowrunCharacter model;

	private ListView<Tradition> lvAvailable;
	private Label descHeading;
	private Label descRef;
	private Label descAttr1, descAttr2;
	private Label description;
	private boolean hideThird;
	private SelectionCallback callback;

	//-------------------------------------------------------------------
	/**
	 */
	public TraditionSelectionPane() {
		setSkin(new ColumnOneAndThreePaneSkin(this));
		
		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
//		lvAvailable.getSelectionModel().select(ShadowrunCore.getTradition("hermetic"));
	}

	//-------------------------------------------------------------------
	/**
	 */
	public TraditionSelectionPane(SelectionCallback callback) {
		this.hideThird = true;
		this.callback = callback;
		setSkin(new ColumnOneAndThreePaneSkin(this));
		
		initComponents();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
//		lvAvailable.getSelectionModel().select(ShadowrunCore.getTradition("hermetic"));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setColumn1Header(UI.getString("label.available"));
		if (!hideThird)
		setColumn3Header(UI.getString("label.description"));

		lvAvailable = new ListView<Tradition>();
		lvAvailable.getItems().addAll(ShadowrunCore.getTraditions());
		lvAvailable.setCellFactory(new Callback<ListView<Tradition>, ListCell<Tradition>>() {
			public ListCell<Tradition> call(ListView<Tradition> param) {
				return  new TraditionListCell();
			}
		});
		
		Label phAvailable = new Label(UI.getString("traditionselectpane.placeholder.available"));
		phAvailable.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);

		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		
		descAttr1   = new Label();
		descAttr2   = new Label();
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 30em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblDrain = new Label(UI.getString("label.drain"));
		Label lblAttr1 = new Label(UI.getString("label.attribute1")+":");
		Label lblAttr2 = new Label(UI.getString("label.attribute2")+":");
		lblAttr1.setStyle("-fx-font-weight: bold");
		lblAttr2.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblAttr1 , 0, 0);
		descGrid.add(descAttr1, 1, 0);
		descGrid.add( lblAttr2, 2, 0);
		descGrid.add(descAttr2, 3, 0);
		descGrid.setHgap(5);
		
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		
		VBox column3 = new VBox(10);
		column3.getChildren().addAll(descHeading, descRef, lblDrain, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);

		setColumn1Node(lvAvailable);
		if (!hideThird)
			setColumn3Node(column3);
		setHeadersVisible(true);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> describe(n));
	}

	//--------------------------------------------------------------------
	private void describe(Tradition data) {
		if (data==null)
			return;
		if (model!=null)
			model.setTradition(data);
		descHeading.setText(data.getName().toUpperCase());
		descRef.setText(data.getProductName()+" "+data.getPage());
		descAttr1.setText(data.getDrainAttribute1().getName());
		descAttr2.setText(data.getDrainAttribute2().getName());
		
		description.setText(data.getHelpText());
		
		if (callback!=null)
			callback.showDescription(this, data);
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		if (model.getTradition()==null)
			lvAvailable.getSelectionModel().clearSelection();
		else
			lvAvailable.getSelectionModel().select(model.getTradition());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
//		switch (event.getType()) {
//		case ATTRIBUTE_CHANGED:
//			logger.debug("RCV "+event);
////			refresh();
//			break;
//		default:
//		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		if (lvAvailable.getSelectionModel().getSelectedItem()!=model.getTradition())
			lvAvailable.getSelectionModel().select(model.getTradition());
	}

}

class TraditionListCell extends ListCell<Tradition> {
	
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	private Label lblLine2;
	
	private HBox bxLayout;
	private VBox bxData;
	
	//-------------------------------------------------------------------
	public TraditionListCell() {
		initComponents();
		initLayout();
		initStyle();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		lblLine2 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1, lblLine2);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Tradition item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			lblName.setText(item.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(makeShortInfoString(item));
//			iview.setImage(IMAGES.get(item.getCategory()));
		}
	}
	
	static String makeShortInfoString(Tradition item) {
		StringBuffer buf = new StringBuffer();
		buf.append(item.getDrainAttribute1().getName());
		buf.append(", ");
		buf.append(item.getDrainAttribute2().getName());
		return buf.toString();
	}
}
