/**
 *
 */
package org.prelle.shadowrun5.jfx.summonables;

import java.util.Arrays;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.ThreeColumnPane;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.SummonableValue;
import org.prelle.shadowrun5.charctrl.SummonableController;
import org.prelle.shadowrun5.gen.event.GenerationEvent;
import org.prelle.shadowrun5.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun5.gen.event.GenerationEventListener;
import org.prelle.shadowrun5.jfx.SR5Constants;

import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class SummonableSelectionPane extends ThreeColumnPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".summonables");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private SummonableController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ListView<Summonable> lvAvailable;
	private ListView<SummonableValue> lvSelected;
	private Label lblName;
	private Label lblPage;
	private Label lblDesc;

	//--------------------------------------------------------------------
	public SummonableSelectionPane(SummonableController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
		this.provider = provider;
		initCompoments();
		initLayout();
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		lvAvailable = new ListView<>();
		lvSelected  = new ListView<>();

		lvAvailable.setCellFactory(new Callback<ListView<Summonable>, ListCell<Summonable>>() {
			public ListCell<Summonable> call(ListView<Summonable> param) {
				return new SummonableListCell(ctrl);
			}
		});
		Label phAvailable = new Label(UI.getString("summonable.placeholder.available"));
		phAvailable.setStyle("-fx-text-fill: -fx-text-base-color");
		phAvailable.setWrapText(true);
		lvSelected.setPlaceholder(phAvailable);

		lvSelected.setCellFactory(new Callback<ListView<SummonableValue>, ListCell<SummonableValue>>() {
			public ListCell<SummonableValue> call(ListView<SummonableValue> param) {
				return new SummonableValueListCell(ctrl, lvSelected, provider);
			}
		});

		Label phSelected = new Label(UI.getString("summonable.placeholder.selected"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		lblName = new Label();
		lblName.getStyleClass().add("text-small-secondary");
		lblName.setStyle("-fx-font-weight: bold");
		lblPage = new Label();
		lblPage.setStyle("-fx-font-style: italic");
		lblDesc = new Label();
		lblDesc.setWrapText(true);

	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setColumn1Header(UI.getString("label.available"));
		setColumn2Header(UI.getString("label.selected"));
		setColumn3Header(UI.getString("label.description"));


		lvAvailable.setStyle("-fx-min-width: 13em");
		lvSelected.setStyle("-fx-min-width: 21em; -fx-pref-width: 32em");
		VBox colDescription = new VBox(5);
		colDescription.setStyle("-fx-pref-width: 20em");
		colDescription.getChildren().addAll(lblName, lblPage, lblDesc);

		setColumn1Node(lvAvailable);
		setColumn2Node(lvSelected);
		setColumn3Node(colDescription);
		setHeadersVisible(true);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> summonableSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				summonableSelectionChanged( ((SummonableValue)n).getSummonable());
			}
		});
		lvSelected.setOnDragDropped(event -> dragFromSummonableDropped(event));
		lvSelected.setOnDragOver(event -> dragFromSummonableOver(event));
	}

	//--------------------------------------------------------------------
	private void summonableSelectionChanged(Summonable data) {
		if (data!=null) {
			logger.debug("Selected "+data);
			lblName.setText(data.getName());
			lblPage.setText(data.getProductName()+" "+data.getPage());
			lblDesc.setText(data.getHelpText());
		}
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(ctrl.getAvailable());

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getSummonables());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private int[] askForRatingAndServices(Summonable data) {
		Label lblRati = new Label(UI.getString("label.rating"));
		Label lblServ = new Label(UI.getString("label.services"));
		lblRati.getStyleClass().add("text-small-subheader");
		lblServ.getStyleClass().add("text-small-subheader");
		ChoiceBox<Integer> cbRati = new ChoiceBox<>();
		TextField tfServ = new TextField();
		for (int i=1; i<=6; i++) {
			if (ctrl.canBeSelected(data))
				cbRati.getItems().add(i);
		}
		cbRati.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> tfServ.setText(String.valueOf(n)));
//		tfServ.textProperty().addListener( (ov,o,n) -> {
//			try {
//				Integer.parseInt(tfServ.getText().trim());
//			} catch (NumberFormatException e) {
//				tfServ.setText("1");
//			}
//		});
		if (cbRati.getItems().size()>0)
			cbRati.getSelectionModel().select(0);
		
		GridPane content = new GridPane();
		content.add(lblRati, 0, 0);
		content.add( cbRati, 1, 0);
		content.add(lblServ, 0, 1);
		content.add( tfServ, 1, 1);
		content.setVgap(10);
		content.setHgap(10);

		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, UI.getString("dialog.summonable.title"), content);
		if (close==CloseType.OK) {
			int serv = 1;
			try {
				serv = Integer.parseInt(tfServ.getText().trim());
			} catch (NumberFormatException e) {
				logger.warn("User entered invalid input for number of services: "+tfServ.getText());
			}
			return new int[]{cbRati.getValue(), serv};
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromSummonableDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("summonable")) {
					Summonable data = ShadowrunCore.getSpirit(tail);
					if (data==null)
						data = ShadowrunCore.getSprite(tail);
					if (data==null) {
						logger.warn("Cannot find summonable for dropped summonable id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						// Request rating
						final Summonable foo = data;
						logger.debug("rating must be entered by user");
						Platform.runLater(new Runnable(){
							public void run() {
								int[] ratServ = askForRatingAndServices(foo);
								if (ratServ!=null) {
									logger.debug("User choose "+Arrays.toString(ratServ));
									ctrl.select(foo, ratServ[1]);
									logger.warn("TODO: adhere rating");
								}
							}});
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromSummonableOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun5.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case SUMMONABLE_ADDED:
		case SUMMONABLE_CHANGED:
		case SUMMONABLE_REMOVED:
		case SUMMONABLES_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

}

class SummonableListCell extends ListCell<Summonable> {

	private final static String NORMAL_STYLE = "summonable-cell";

	private SummonableController control;
	private BasePluginData data;

	private Label lineName;
	private Label lineDescription;
	private VBox  layout;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	//-------------------------------------------------------------------
	public SummonableListCell(SummonableController ctrl) {
		this.control = ctrl;
		getStyleClass().add(NORMAL_STYLE);
		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_RIGHT);

		// Content
		lineName = new Label();
		lineName.setStyle("-fx-font-weight: bold;");
		lineDescription = new Label();
		layout   = new VBox(3);
		layout.getChildren().addAll(lineName, lineDescription);

		// Recommended icon
		imgRecommended = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);
		imgRecommended.setVisible(false);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblType, layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Summonable item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblType.setText(null);
			lineName.setText(((Summonable)item).getName());
			lineDescription.setText(item.getProductName()+" "+item.getPage());
			//				imgRecommended.setVisible(control.isRecommended((Summonable) item));
			setGraphic(stack);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("summonable:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

}

class SummonableValueListCell extends ListCell<SummonableValue> {

	private final static Logger logger = LogManager.getLogger(SR5Constants.BASE_LOGGER_NAME+".summonables");

	private static PropertyResourceBundle UI = SR5Constants.RES;

	private final static String NORMAL_STYLE = "summonable-cell";

	private SummonableController charGen;
	private ListView<SummonableValue> parent;
	private ScreenManagerProvider provider;

	private transient SummonableValue data;

	private HBox layout;
	private Label name;
	private Label print;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private StackPane stack;
	private ImageView imgRecommended;

	//-------------------------------------------------------------------
	public SummonableValueListCell(SummonableController charGen, ListView<SummonableValue> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		getStyleClass().add(NORMAL_STYLE);
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		// Content
		layout  = new HBox(5);
		name    = new Label();
		print   = new Label();
		btnDec  = new Button("-");
		lblVal  = new Label("?");
		btnInc  = new Button("+");

		// Recommended icon
		imgRecommended = new ImageView(new Image(ClassLoader.getSystemResourceAsStream(SR5Constants.PREFIX+"/images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		print.getStyleClass().add("text-secondary-info");
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");

		//		setStyle("-fx-pref-width: 24em");
		layout.getStyleClass().add("content");
		layout.setStyle("-fx-min-width: 21em; -fx-pref-width: 28em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label total = new Label(UI.getString("label.number_of_services"));
		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(name, print);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		VBox foo = new VBox(2);
		foo.getChildren().addAll(total, tiles);
		layout.getChildren().addAll(bxCenter, foo);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			data.setServices(data.getServices()+1);
			updateItem(data, false);
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			data.setServices(data.getServices()-1);
			updateItem(data, false);
			if (data.getServices()==0)
				charGen.deselect(data);
			parent.refresh();
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "summonable:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = null;
		id = "summonable:"+data.getSummonable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SummonableValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			data = item;

			name.setText(item.getName());
			print.setText(item.getSummonable().getProductName()+" "+item.getSummonable().getPage());
			imgRecommended.setVisible(false);
			lblVal.setText(" "+String.valueOf(item.getServices())+" ");
			
			setGraphic(layout);
		}
	}
}