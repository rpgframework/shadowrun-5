package org.prelle.shadowrun5.jfx;

import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ModernUI;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.RPGFrameworkJFXConstants;
import org.prelle.rpgframework.shadowrun5.data.Shadowrun5DataPlugin;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.LicenseValue;
import org.prelle.shadowrun5.LifestyleOptionValue;
import org.prelle.shadowrun5.LifestyleValue;
import org.prelle.shadowrun5.Priority;
import org.prelle.shadowrun5.PriorityType;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillSpecializationValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.charctrl.CharGenMode;
import org.prelle.shadowrun5.charctrl.SkillController;
import org.prelle.shadowrun5.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5MagicPage;
import org.prelle.shadowrun5.chargen.jfx.pages.SR5OverviewPage;
import org.prelle.shadowrun5.gen.PriorityCharacterGenerator;
import org.prelle.shadowrun5.gen.PrioritySkillGenerator;
import org.prelle.shadowrun5.items.AvailableSlot;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.jfx.attributes.AttributeCheckboxTable;
import org.prelle.shadowrun5.jfx.attributes.AttributePanePrimary;
import org.prelle.shadowrun5.jfx.attributes.AttributePaneSecondary;
import org.prelle.shadowrun5.jfx.connections.ConnectionSelectionPane;
import org.prelle.shadowrun5.jfx.fluent.CharacterViewScreenSR5Fluent;
import org.prelle.shadowrun5.jfx.items.BigCarriedItemListCell;
import org.prelle.shadowrun5.jfx.items.EditCarriedItemPane;
import org.prelle.shadowrun5.jfx.skills.SkillSelectionPane;
import org.prelle.shadowrun5.jfx.spells.SpellSelectionPane;

import de.rpgframework.core.RoleplayingSystem;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.util.Callback;

public class MyShadowrunStarter extends Application implements ScreenManagerProvider {

	private ScreenManager manager;

	public MyShadowrunStarter() {
		manager = new ScreenManager();
	}

	@Override
	public void start(Stage stage) throws Exception {
		ShadowrunCore.initialize(new DummyRulePlugin<>(RoleplayingSystem.SHADOWRUN, "CORE"));
		Shadowrun5DataPlugin plugin = new Shadowrun5DataPlugin();
		plugin.init(null);

//		ShadowrunCharacter model = ShadowrunCore.load(MyShadowrunStarter.class.getResourceAsStream("adept.xml"));
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setRealName("Rob Roy");
		model.getAttribute(Attribute.AGILITY).setPoints(4);
		SIN sin1 = new SIN(SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
		sin1.setName("Manfred Muster");
		model.addSIN(sin1);
		SIN sin2 = new SIN(SIN.Quality.GOOD_MATCH);
		sin2.setName("Bertha Beispiel");
		model.addSIN(sin2);
		LifestyleValue life1 = new LifestyleValue(ShadowrunCore.getLifestyle("middle"));
		life1.setName("Bude in Bonn");
		life1.setPaidMonths(5);
		life1.setSIN(sin2.getUniqueId());
		life1.addOption(new LifestyleOptionValue(ShadowrunCore.getLifestyleOption("dangerous_area")));
		life1.addOption(new LifestyleOptionValue(ShadowrunCore.getLifestyleOption("cramped")));
		model.addLifestyle(life1);
		LicenseValue lic1 = new LicenseValue();
		lic1.setRating(SIN.Quality.SUPERFICIALLY_PLAUSIBLE);
		lic1.setName("Waffenschein");
		lic1.setSIN(sin2.getUniqueId());
		model.addLicense(lic1);

		Skill unarmed = ShadowrunCore.getSkill("unarmed");
		SkillValue sVal = new SkillValue(unarmed, 3);
		sVal.addSpecialization(new SkillSpecializationValue(unarmed.getSpecializations().get(1)));
		sVal.addSpecialization(new SkillSpecializationValue(unarmed.getSpecializations().get(0)));
		model.addSkill(sVal);

		PriorityCharacterGenerator prioGen = new PriorityCharacterGenerator();
		prioGen.start(model);
		prioGen.setPriority(PriorityType.RESOURCES, Priority.C);
		prioGen.getQualityController().select(ShadowrunCore.getQuality("sinner_criminal"));
		ScreenManager manager = new ScreenManager();
		Region dia = null;
		int variant = 14;
		switch (variant) {
		case 0:
			dia = new SelectPrioritiesPage(new NewPriorityCharacterGenerator());
			break;
		case 1:
			dia = new SelectCharacterConceptPage();
			break;
		case 2:
			prioGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
			prioGen.getMetatypeController().select(ShadowrunCore.getMetaType("troll"));
			dia = new AttributeCheckboxTable(prioGen.getAttributeController());
			((AttributeCheckboxTable)dia).setData(model);
			break;
		case 3:
			prioGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
			prioGen.getMetatypeController().select(ShadowrunCore.getMetaType("troll"));
			AttributePanePrimary prim = new AttributePanePrimary(prioGen.getAttributeController(), CharGenMode.CREATING);
			AttributePaneSecondary sec= new AttributePaneSecondary(prioGen.getAttributeController(), CharGenMode.CREATING);
			prim.setData(model);
			sec.setData(model);
			dia = new HBox(20);
			((HBox)dia).getChildren().addAll(prim, sec);
			break;
		case 4:
			prioGen.setPriority(PriorityType.SKILLS, Priority.B);
			SkillController skillCtrl = prioGen.getSkillController();
			((PrioritySkillGenerator)skillCtrl).setData(4, 10);
			dia = new SkillSelectionPane(skillCtrl, this);
			((SkillSelectionPane)dia).setData(model);
			prioGen.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
			break;
		case 5:
			prioGen.setPriority(PriorityType.MAGIC, Priority.B);
			dia = new SpellSelectionPane(prioGen.getSpellController() , this, false);
			((SpellSelectionPane)dia).setData(model);
//			prioGen.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
			break;
		case 6:
			prioGen.setPriority(PriorityType.MAGIC, Priority.B);
			dia = new ConnectionSelectionPane(prioGen.getConnectionController() , this);
			((ConnectionSelectionPane)dia).setData(model);
//			prioGen.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
			break;
//		case 7:
//			SINScreen sinScreen = new SINScreen(prioGen);
//			sinScreen.setData(model);
//			dia = manager;
////			manager.show(sinScreen);
//			break;
		case 8:
			CarriedItem rifle =new CarriedItem(ShadowrunCore.getItem("enfield_as7"));
			rifle.addSlot(new AvailableSlot(ItemHook.TOP));
			rifle.addSlot(new AvailableSlot(ItemHook.UNDER));
			rifle.addAccessory(ItemHook.TOP, new CarriedItem(ShadowrunCore.getItem("laser_sight")));
			rifle.addAccessory(ItemHook.UNDER, new CarriedItem(ShadowrunCore.getItem("bipod")));
			rifle.addAccessory(ItemHook.INTERNAL, new CarriedItem(ShadowrunCore.getItem("short_barrel")));
			ListView<CarriedItem> items = new ListView<CarriedItem>();
			items.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {

				@Override
				public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
					// TODO Auto-generated method stub
					return new BigCarriedItemListCell(prioGen.getEquipmentController(), items, null);
				}
			});
			items.getItems().add(rifle);

			dia = items;
			break;
		case 9:
			rifle =new CarriedItem(ShadowrunCore.getItem("enfield_as7"));
//			rifle.addSlot(new AvailableSlot(ItemHook.TOP));
//			rifle.addSlot(new AvailableSlot(ItemHook.UNDER));
//			rifle.addSlot(new AvailableSlot(ItemHook.BARREL));
			rifle.addAccessory(ItemHook.TOP, new CarriedItem(ShadowrunCore.getItem("laser_sight")));
			rifle.addAccessory(ItemHook.UNDER, new CarriedItem(ShadowrunCore.getItem("bipod")));

			EditCarriedItemPane editItem = new EditCarriedItemPane(prioGen.getEquipmentController(), this);
			editItem.setData(rifle);

			dia = editItem;
			break;
//		case 10:
//			LifestyleScreen lifeScreen = new LifestyleScreen(prioGen);
//			lifeScreen.setData(model);
//			dia = manager;
////			manager.navigateTo(lifeScreen);
//			break;
//		case 11:
//			model = new ShadowrunCharacter();
//			NewPriorityCharacterGenerator prioGen2 = new NewPriorityCharacterGenerator();
//			prioGen2.start(model);
//			ScrollableCharacterWizard scrollScreen = new ScrollableCharacterWizard(prioGen2);
//			dia = manager;
//			manager.navigateTo(scrollScreen);
//			break;
		case 12:
			model = ShadowrunCore.load(MyShadowrunStarter.class.getResourceAsStream("adept.xml"));
			NewPriorityCharacterGenerator prioGen2 = new NewPriorityCharacterGenerator();
			prioGen2.start(model);


			SR5OverviewPage page = new SR5OverviewPage(prioGen2, ViewMode.MODIFICATION, null, this);

			dia = page;
			break;
		case 13:
			model = ShadowrunCore.load(MyShadowrunStarter.class.getResourceAsStream("mysticadept.xml"));
			prioGen2 = new NewPriorityCharacterGenerator();
			prioGen2.start(model);

			SR5MagicPage page2 = new SR5MagicPage(prioGen2, ViewMode.MODIFICATION, null, this);

			dia = page2;
			break;
		case 14:
			model = ShadowrunCore.load(MyShadowrunStarter.class.getResourceAsStream("mysticadept.xml"));
			prioGen2 = new NewPriorityCharacterGenerator();
			prioGen2.start(model);

			ManagedScreen managed = new CharacterViewScreenSR5Fluent(prioGen2, ViewMode.GENERATION, null);
			manager.navigateTo(managed);

			dia = manager;
			break;
		}

		Scene scene = new Scene(dia, 1400, 900);
		ModernUI.initialize(scene);
		scene.getStylesheets().add(RPGFrameworkJFXConstants.class.getResource("css/rpgframework.css").toExternalForm());
		scene.getStylesheets().add("file:/home/prelle/workspaces/RPGFramework/Shadowrun5/Shadowrun5/Shadowrun_CharGen_JFX/target/classes/org/prelle/shadowrun/jfx/css/shadowrun-common.css");
		scene.getStylesheets().add("file:/home/prelle/workspaces/RPGFramework/Shadowrun5/Shadowrun5/Shadowrun_CharGen_JFX/target/classes/org/prelle/shadowrun/jfx/css/shadowrun-dark.css");
		stage.setScene(scene);
		stage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public ScreenManager getScreenManager() {
		// TODO Auto-generated method stub
		return manager;
	}

}
