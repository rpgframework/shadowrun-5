/**
 * @author Stefan Prelle
 *
 */
module shadowrun.core {
	exports org.prelle.rpgframework.shadowrun5;
	exports org.prelle.shadowrun5;
	exports org.prelle.shadowrun5.persist;
	exports org.prelle.shadowrun5.requirements;
	exports org.prelle.shadowrun5.modifications;
	exports org.prelle.shadowrun5.proc;
	exports org.prelle.shadowrun5.actions;
	exports org.prelle.shadowrun5.items;

	opens org.prelle.shadowrun5 to simple.persist;
	opens org.prelle.shadowrun5.actions to simple.persist;
	opens org.prelle.shadowrun5.items to simple.persist;
	opens org.prelle.shadowrun5.modifications to simple.persist;
	opens org.prelle.shadowrun5.requirements to simple.persist;

	provides de.rpgframework.character.RulePlugin with org.prelle.rpgframework.shadowrun5.Shadowrun5Rules;

	requires activation;
	requires java.datatransfer;
	requires java.mail;
	requires java.naming;
	requires java.xml;
	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires simple.persist;
	requires de.rpgframework.products;
}