package org.prelle.rpgframework.shadowrun5;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.character.DecodeEncodeException;
import de.rpgframework.core.CommandBus;
import de.rpgframework.core.CommandBusListener;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun5Rules implements RulePlugin<ShadowrunCharacter>, CommandBusListener {

	private final static Logger logger = LogManager.getLogger("shadowrun");

	public final static String PROP_DEVELOPER_MODE = "developer_mode";

	private static List<RulePluginFeatures> FEATURES = new ArrayList<RulePluginFeatures>();

	private ShadowrunCharacterPlugin charac;

	//-------------------------------------------------------------------
	static {
		FEATURES.add(RulePluginFeatures.PERSISTENCE);
	}

	//-------------------------------------------------------------------
	/**
	 */
	public Shadowrun5Rules() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "CORE";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 5 Core Rules";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return new ArrayList<String>();
	}

	//-------------------------------------------------------------------
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return FEATURES;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
		switch (type) {
		case ENCODE:
			if (values[0]!=RoleplayingSystem.SHADOWRUN) return false;
			if (values.length<2) return false;
			return (values[1] instanceof ShadowrunCharacter);
		case DECODE:
			if (values[0]!=RoleplayingSystem.SHADOWRUN) return false;
			if (values.length<2) return false;
			return (values[1] instanceof byte[]);
		default:
			return false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		logger.debug("handleCommand("+type+", "+Arrays.toString(values)+")");
		switch (type) {
		case ENCODE:
			ShadowrunCharacter model = (ShadowrunCharacter)values[1];
			byte[] raw;
			try {
				raw = charac.marshal(model);
				return new CommandResult(type, raw);
			} catch (DecodeEncodeException e) {
				return new CommandResult(type, false, e.toString());
			}
		case DECODE:
			raw = (byte[])values[1];
			try {
				model = charac.unmarshal(raw);
				logger.debug("Unmarshal done");
				return new CommandResult(type, model);
			} catch (DecodeEncodeException e) {
				return new CommandResult(type, false, e.toString());
			}
		default:
			return new CommandResult(type, false, "Not supported");
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		logger.debug("Add configuration to "+addBelow);
		ConfigContainer configRoot = addBelow.createContainer("shadowrun");
		configRoot.setResourceBundle(ShadowrunCore.getI18nResources());
		configRoot.createOption(PROP_DEVELOPER_MODE, ConfigOption.Type.BOOLEAN, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		logger.debug("START: -----Init "+getID()+"---------------------------");
		charac = new ShadowrunCharacterPlugin();
		ShadowrunCore.initialize(this);
//		logger.fatal("Stop here");
//		System.exit(0);

		CommandBus.registerBusCommandListener(this);
		logger.debug("STOP : -----Init "+getID()+"---------------------------");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return ClassLoader.getSystemResourceAsStream("org/prelle/rpgframework/shadowrun5/data/core/i18n/shadowrun/core.html");
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
