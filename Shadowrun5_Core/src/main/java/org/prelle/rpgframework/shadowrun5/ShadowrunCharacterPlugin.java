/**
 * 
 */
package org.prelle.rpgframework.shadowrun5;

import java.io.IOException;

import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;

import de.rpgframework.character.DecodeEncodeException;

/**
 * @author prelle
 *
 */
public class ShadowrunCharacterPlugin {

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunCharacterPlugin() {
	}

//	//-------------------------------------------------------------------
//	public byte[] getImageBytes(ShadowrunCharacter charac) {
//		return charac.getImage();
//	}

	//-------------------------------------------------------------------
	public String getName(ShadowrunCharacter data) {
		return data.getName();
	}

	//-------------------------------------------------------------------
	public byte[] marshal(ShadowrunCharacter charac) throws DecodeEncodeException {
		try {
			return ShadowrunCore.save(charac);
		} catch (IOException e) {
			if (e.getCause()!=null)
				throw new DecodeEncodeException("IO-Error encoding", e.getCause());
			throw new DecodeEncodeException("IO-Error encoding", e);
		}
	}

	//-------------------------------------------------------------------
	public ShadowrunCharacter unmarshal(byte[] data) throws DecodeEncodeException {
		try {
			return ShadowrunCore.load(data);
		} catch (IOException e) {
			if (e.getCause()!=null)
				throw new DecodeEncodeException("IO-Error decoding", e.getCause());
			throw new DecodeEncodeException("IO-Error decoding", e);
		}
	}

}
