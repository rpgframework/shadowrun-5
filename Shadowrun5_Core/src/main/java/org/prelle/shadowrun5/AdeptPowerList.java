/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="powers")
@ElementList(entry="power",type=AdeptPower.class,inline=true)
public class AdeptPowerList extends ArrayList<AdeptPower> {

	private static final long serialVersionUID = 6013236759027604205L;

	//-------------------------------------------------------------------
	/**
	 */
	public AdeptPowerList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public AdeptPowerList(Collection<? extends AdeptPower> c) {
		super(c);
	}

}
