/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.UUID;

import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.persist.AdeptPowerConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class AdeptPowerValue extends ModifyableImpl implements Modifyable, Comparable<AdeptPowerValue> {

	@Attribute(name="power")
	@AttribConvert(AdeptPowerConverter.class)
	private AdeptPower power;
	@Attribute
	private int level;
//	@Attribute(name="selSkill")
//	@AttribConvert(SkillConverter.class)
//	private Skill selectedSkill;
//	@Attribute(name="selAttr")
//	private org.prelle.shadowrun.Attribute selectedAttribute;
	@Attribute(name="choice")
	private String choiceReference;
	@Attribute(name="uniqueid")
	private UUID uniqueId;
	private transient Object choice;

	//-------------------------------------------------------------------
	public AdeptPowerValue() {
		uniqueId = UUID.randomUUID();
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue(AdeptPower power) {
		uniqueId = UUID.randomUUID();
		this.power = power;
		if (power.hasLevels())
			level=1;
	}

	//-------------------------------------------------------------------
	public String getName() {
		StringBuffer ret = new StringBuffer(power.getName());
//		if (selectedSkill!=null) {
//			ret.append(" ("+selectedSkill.getName()+")");
//		} else 
			if (choice!=null) {
				if (choice instanceof org.prelle.shadowrun5.Attribute)
					ret.append(" ("+((org.prelle.shadowrun5.Attribute)choice).getName()+")");
				else if (choice instanceof Skill)
					ret.append(" ("+((Skill)choice).getName()+")");
				else if (choice instanceof SkillGroup)
					ret.append(" ("+((SkillGroup)choice).getName()+")");
				else if (choice instanceof ShadowrunAction)
					ret.append(" ("+((ShadowrunAction)choice).getName()+")");
				else if (choice instanceof Sense)
					ret.append(" ("+((Sense)choice).getName()+")");
				else 
					ret.append(" ("+choice+")");
		}
		if (power.hasLevels())
			ret.append(" "+level);
		return ret.toString();
	}

	//-------------------------------------------------------------------
	public String getId() {
		return power.getId();
	}

	//-------------------------------------------------------------------
	public String getTypeId() {
		return power.getTypeId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AdeptPowerValue other) {
		return power.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public AdeptPower getModifyable() {
		return power;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

//	//--------------------------------------------------------------------
//	/**
//	 * @return the selectedSkill
//	 */
//	public Skill getSelectedSkill() {
//		return selectedSkill;
//	}
//
//	//--------------------------------------------------------------------
//	/**
//	 * @param selectedSkill the selectedSkill to set
//	 */
//	public void setSelectedSkill(Skill selectedSkill) {
//		this.selectedSkill = selectedSkill;
//	}
//
//	//--------------------------------------------------------------------
//	/**
//	 * @return the selectedAttribute
//	 */
//	public org.prelle.shadowrun.Attribute getSelectedAttribute() {
//		return selectedAttribute;
//	}
//
//	//--------------------------------------------------------------------
//	/**
//	 * @param selectedAttribute the selectedAttribute to set
//	 */
//	public void setSelectedAttribute(
//			org.prelle.shadowrun.Attribute selectedAttribute) {
//		this.selectedAttribute = selectedAttribute;
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

}
