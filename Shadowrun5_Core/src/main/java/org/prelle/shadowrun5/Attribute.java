/**
 *
 */
package org.prelle.shadowrun5;

/**
 * @author prelle
 *
 */
public enum Attribute {

	BODY,
	AGILITY,
	REACTION,
	STRENGTH,
	WILLPOWER,
	LOGIC,
	INTUITION,
	CHARISMA,
	EDGE,
	MAGIC,
	RESONANCE,
	ESSENCE,

	INITIATIVE_PHYSICAL,
	INITIATIVE_MATRIX,
	INITIATIVE_ASTRAL,
	INITIATIVE_DICE_PHYSICAL,
	INITIATIVE_DICE_MATRIX,
	INITIATIVE_DICE_ASTRAL,
	LIMIT_SOCIAL,
	LIMIT_PHYSICAL,
	LIMIT_MENTAL,
	LIMIT_ASTRAL,

	COMPOSURE,
	JUDGE_INTENTIONS,
	MEMORY,
	LIFT_CARRY,
	MOVEMENT,

	STREET_CRED,
	NOTORIETY,
	PUBLIC_AWARENESS,

	ATTACK,
	SLEAZE,
	DATA_PROCESSING,
	FIREWALL,
	
	ARMOR,
	MELEE_DAMAGE,
	REACH,
	PHYSICAL_MONITOR,
	STUN_MONITOR,
	;

	//-------------------------------------------------------------------
	public String getShortName() {
		return ShadowrunCore.getI18nResources().getString("attribute."+this.name().toLowerCase()+".short");
	}

    //-------------------------------------------------------------------
    public String getName() {
        return ShadowrunCore.getI18nResources().getString("attribute."+this.name().toLowerCase());
    }

	//-------------------------------------------------------------------
	public static Attribute[] primaryValues() {
		return new Attribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA};
	}

	//-------------------------------------------------------------------
	public static Attribute[] derivedValues() {
		return new Attribute[]{INITIATIVE_PHYSICAL, INITIATIVE_MATRIX, INITIATIVE_ASTRAL, LIMIT_PHYSICAL, LIMIT_SOCIAL, LIMIT_MENTAL, LIMIT_ASTRAL, COMPOSURE, JUDGE_INTENTIONS, MEMORY, LIFT_CARRY, MOVEMENT, STREET_CRED, NOTORIETY, PUBLIC_AWARENESS,
				};
	}

	//-------------------------------------------------------------------
	public static Attribute[] secondaryValues() {
		return new Attribute[]{INITIATIVE_PHYSICAL, INITIATIVE_MATRIX, INITIATIVE_ASTRAL, LIMIT_PHYSICAL, LIMIT_SOCIAL, LIMIT_MENTAL, LIMIT_ASTRAL, COMPOSURE, JUDGE_INTENTIONS, MEMORY, LIFT_CARRY, MOVEMENT, STREET_CRED, NOTORIETY, PUBLIC_AWARENESS,
				ATTACK, SLEAZE, DATA_PROCESSING, FIREWALL, ARMOR, MELEE_DAMAGE, REACH, PHYSICAL_MONITOR, STUN_MONITOR};
	}

	//-------------------------------------------------------------------
	public static Attribute[] specialAttributes() {
		return new Attribute[]{EDGE,MAGIC,RESONANCE,ESSENCE};
	}

	//-------------------------------------------------------------------
	public static Attribute[] primaryAndSpecialValues() {
		return new Attribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA,EDGE,MAGIC,RESONANCE,ESSENCE};
	}

	//-------------------------------------------------------------------
	public static Attribute[] physicalValues() {
		return new Attribute[]{BODY,AGILITY,REACTION,STRENGTH};
	}

	//-------------------------------------------------------------------
	public static Attribute[] primaryTableValues() {
		return new Attribute[]{BODY,AGILITY,REACTION,STRENGTH, WILLPOWER,LOGIC,INTUITION,CHARISMA,EDGE,MAGIC,RESONANCE};
	}

	//-------------------------------------------------------------------
	public static Attribute[] derivedTableValues() {
		return new Attribute[]{INITIATIVE_PHYSICAL, INITIATIVE_MATRIX, INITIATIVE_ASTRAL, LIMIT_PHYSICAL, LIMIT_SOCIAL, LIMIT_MENTAL, LIMIT_ASTRAL, COMPOSURE, JUDGE_INTENTIONS, MEMORY, LIFT_CARRY, MOVEMENT};
	}

	//-------------------------------------------------------------------
	public boolean isPrimary() {
		for (Attribute key : primaryValues())
			if (this==key) return true;
		return false;
	}

	//-------------------------------------------------------------------
	public boolean isSpecial() {
		for (Attribute key : specialAttributes())
			if (this==key) return true;
		return false;
	}

	//-------------------------------------------------------------------
	public boolean isPhysical() {
		for (Attribute key : physicalValues())
			if (this==key) return true;
		return false;
	}

}
