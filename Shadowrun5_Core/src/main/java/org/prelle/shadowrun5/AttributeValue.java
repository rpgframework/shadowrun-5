/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationValueType;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name="attr")
public class AttributeValue extends ModifyableImpl implements Comparable<AttributeValue>, ModifyableValue<Attribute>, NumericalValue<Attribute> , SelectedValue<Attribute>{

	@org.prelle.simplepersist.Attribute(name="id",required=true)
	private Attribute id;

	/**
	 * The recent value of the attribute, including modifications by
	 * race on generation, the distributed points on generation
	 * and the points bought.
	 * Bought = unmodifiedValue - start;
	 */
	@org.prelle.simplepersist.Attribute(name="value",required=true)
	private int distributed;

	/**
	 * The final value of the attribute after generation, before
	 * exp have been spent.
	 * During priority generation this contains the value until
	 * which the attribute is paid by attribute points
	 */
	@org.prelle.simplepersist.Attribute(name="start",required=false)
	private int start;


	//-------------------------------------------------------------------
	public AttributeValue() {
	}

	//-------------------------------------------------------------------
	public AttributeValue(Attribute attr, int val) {
		this.id = attr;
		this.distributed = val;
		this.start = val;
	}

	//-------------------------------------------------------------------
	public AttributeValue(AttributeValue copy) {
		this.id = copy.getModifyable();
		this.start = copy.getStart();
		this.distributed = copy.getPoints();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s  \tSTART=%d  NORM=%d MOD=%d  VAL=%d", id, start, distributed, getModifier(), getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Attribute getAttribute() {
		return id;
	}

	//-------------------------------------------------------------------
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.CURRENT)
					count += aMod.getValue();
			}
		}
		return count;
	}

	//-------------------------------------------------------------------
	public int getFixedModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.CURRENT && 
						((aMod.getSource() instanceof MetaType) || (aMod.getSource() instanceof MagicOrResonanceType)))
					count += aMod.getValue();
			}
		}
		return count;
	}

	//-------------------------------------------------------------------
	public int getVolatileModifier() {
		return getModifier() - getFixedModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return distributed + getModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Attribute getModifyable() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AttributeValue other) {
		return id.compareTo(other.getModifyable());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int distributed) {
		this.distributed = distributed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	//-------------------------------------------------------------------
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	//-------------------------------------------------------------------
	/**
	 * Points gained by investing EXP
	 * @return the v1Value
	 */
	public int getBought() {
		return distributed - start;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return distributed + getFixedModifier();
	}

	//--------------------------------------------------------------------
	public int getFixedPoints() {
		return distributed + getFixedModifier();
	}

	//--------------------------------------------------------------------
	public int getMaximum() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.MAX)
					count += aMod.getValue();
			}
		}
		return Math.max(count, getModifier());
	}

	//--------------------------------------------------------------------
	public int getEnhancedMaximum() {
		int count = Math.round(getMaximum()*1.5f);
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.ENHANCED_MAX)
					count += aMod.getValue();
			}
		}
		return count;
	}

	//--------------------------------------------------------------------
	public String getVolatileModificationString() {
		List<String> data = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.CURRENT && !aMod.isConditional()) {
					if (aMod.getSource() instanceof MetaType)
						continue;
					if (aMod.getSource() instanceof MagicOrResonanceType)
						continue;
					if (aMod.getSource() instanceof CarriedItem) {
						data.add("+"+aMod.getValue()+" "+((CarriedItem)mod.getSource()).getName());
					} else {
						if (aMod.getSource()!=null)
							data.add("+"+aMod.getValue()+" "+aMod.getSource().getClass());
						else
							data.add("+"+aMod.getValue());
					}
				}
			}
		}
		return String.join("\n", data);
	}

}
