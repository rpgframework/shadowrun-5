/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="attributes")
@ElementList(entry="attr",type=AttributeValue.class)
public class Attributes extends ArrayList<AttributeValue> {

	private static final long serialVersionUID = 1L;

	private final static Logger logger = LogManager.getLogger("shadowrun");

	private transient Map<Attribute, AttributeValue> secondary;

	//-------------------------------------------------------------------
	/**
	 */
	public Attributes() {
		secondary = new HashMap<Attribute, AttributeValue>();
		for (Attribute attr : Attribute.values()) {
			AttributeValue toAdd = new AttributeValue(attr, 0);
			add(toAdd);
		}
	}

	//-------------------------------------------------------------------
	@Override
	public boolean add(AttributeValue val) {
		if (val.getAttribute().isPrimary() || val.getAttribute().isSpecial()) {
		for (AttributeValue tmp : new ArrayList<AttributeValue>(this)) {
			if (tmp.getAttribute()==val.getAttribute()) {
				super.remove(tmp);
			}
		}
		return super.add(val);
		} else {
			if (secondary.containsKey(val.getAttribute()))
				throw new IllegalStateException("Already exists");
			secondary.put(val.getAttribute(), val);
			return true;
		}
		
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		for (AttributeValue tmp : this)
			buf.append("\n "+tmp.getAttribute()+" \t "+tmp);
		for (Attribute tmp : Attribute.secondaryValues())
			buf.append("\n "+tmp+" \t "+get(tmp));

		return buf.toString();
	}

	//-------------------------------------------------------------------
	public int getValue(Attribute key) {
		return get(key).getModifiedValue();
	}

	//-------------------------------------------------------------------
	public AttributeValue get(Attribute key) {
		if (key==null)
			throw new NullPointerException("Attribute may not be null");
		for (AttributeValue pair : this) {
			if (pair.getAttribute()==key) {
				return pair;
			}
		}
		AttributeValue pair = secondary.get(key);
		if (pair!=null) {
			return pair;
		}

		logger.error("Something accessed an unset attribute: "+key+"\nsecondary="+secondary);
		throw new NoSuchElementException(String.valueOf(key));
	}

	//--------------------------------------------------------------------
	public void addModification(AttributeModification mod) {
		get(mod.getAttribute()).addModification(mod);
	}

	//--------------------------------------------------------------------
	public void removeModification(AttributeModification mod) {
		get(mod.getAttribute()).removeModification(mod);
	}

}
