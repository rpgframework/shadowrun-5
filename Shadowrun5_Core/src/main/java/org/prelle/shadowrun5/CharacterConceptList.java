/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="concepts")
@ElementList(entry="concept",type=CharacterConcept.class)
public class CharacterConceptList extends ArrayList<CharacterConcept> {

}
