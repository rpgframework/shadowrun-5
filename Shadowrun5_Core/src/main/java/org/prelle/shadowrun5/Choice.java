/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public class Choice {

	@Attribute
	private ChoiceType type;
	@ElementList(entry="option",type=String.class)
	private List<String> optionIDs;
	
}
