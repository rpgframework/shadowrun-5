/**
 * 
 */
package org.prelle.shadowrun5;

/**
 * @author prelle
 *
 */
public enum ChoiceType {
	
	ATTRIBUTE,
	MATRIX_ACTION,
	SKILL,
	SKILLGROUP,
	COMBAT_SKILL,
	LIMIT,
	PHYSICAL_ATTRIBUTE,
	SENSE,
	MENTOR_SPIRIT,
	PROGRAM

}
