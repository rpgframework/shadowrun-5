/**
 *
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.List;

import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ComplexForm extends BasePluginData implements Comparable<ComplexForm> {

	public enum Duration {
		IMMEDIATE,
		SUSTAINED,
		PERMANENT,
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("cplxform.duration."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("cplxform.duration."+name().toLowerCase()+".short"); }
	}

	public enum Target {
		DEVICE,
		FILE,
		PERSONA,
		SELF,
		SPRITE,
		;
		public String getName() { return ShadowrunCore.getI18nResources().getString("cplxform.target."+name().toLowerCase()); }
		public String getShortName() { return ShadowrunCore.getI18nResources().getString("cplxform.target."+name().toLowerCase()+".short"); }
	}

	@Attribute(required=true)
	private String id;
	@Attribute(name="tar",required=true)
	private Target target;
	@Attribute(name="dur",required=true)
	private Duration duration;
	@Attribute(name="fad",required=true)
	private int fading;
	@Element
	private ModificationList modifications;

	//--------------------------------------------------------------------
	/**
	 */
	public ComplexForm() {
		modifications = new ModificationList();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "ComplexForm(id=null)";
    	if (i18n==null)
    		return "complexform."+id.toLowerCase()+".title";
    	String key = "complexform."+id.toLowerCase()+".title";
    	if (i18n.containsKey(key))
        return i18n.getString(key);
    		logger.error("Missing key '"+key+"' in "+i18n.getBaseBundleName());
    	return key;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "complexform."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "complexform."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ComplexForm o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the target
	 */
	public Target getTarget() {
		return target;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the fading
	 */
	public int getFading() {
		return fading;
	}
}
