/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="complexforms")
@ElementList(entry="complexform",type=ComplexForm.class)
public class ComplexFormList extends ArrayList<ComplexForm> {

}
