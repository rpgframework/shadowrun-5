/**
 *
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.ComplexFormConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class ComplexFormValue extends ModifyableImpl implements Modifyable, Comparable<ComplexFormValue> {

	@Attribute(name="ref")
	@AttribConvert(ComplexFormConverter.class)
	private ComplexForm complexForm;

	//-------------------------------------------------------------------
	public ComplexFormValue() {
	}

	//-------------------------------------------------------------------
	public ComplexFormValue(ComplexForm data) {
		this.complexForm = data;
	}

	//-------------------------------------------------------------------
	public String getName() {
		return complexForm.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ComplexFormValue other) {
		return complexForm.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public ComplexForm getModifyable() {
		return complexForm;
	}

}
