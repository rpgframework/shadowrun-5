/**
 *
 */
package org.prelle.shadowrun5;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author Stefan
 *
 */
public class Connection {

	@Attribute(name="id")
	private UUID id;
	@Attribute(name="infl")
	private int influence;
	@Attribute(name="loy")
	private int loyalty;
	@Attribute
	private String name;
	@Attribute
	private String type;
	@Element
	private String description;

	//--------------------------------------------------------------------
	public Connection() {
		id = UUID.randomUUID();
	}

	//--------------------------------------------------------------------
	public Connection(String name, String type, int infl, int loy) {
		this.name = name;
		this.type = type;
		influence = infl;
		loyalty   = loy;
		id = UUID.randomUUID();
	}

	//--------------------------------------------------------------------
	public String toString() {
		return name+" (type="+type+", infl="+influence+", loy="+loyalty+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the loyalty
	 */
	public int getLoyalty() {
		return loyalty;
	}

	//--------------------------------------------------------------------
	/**
	 * @param loyalty the loyalty to set
	 */
	public void setLoyalty(int loyalty) {
		this.loyalty = loyalty;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//--------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the influence
	 */
	public int getInfluence() {
		return influence;
	}

	//--------------------------------------------------------------------
	/**
	 * @param influence the influence to set
	 */
	public void setInfluence(int influence) {
		this.influence = influence;
	}

	//-------------------------------------------------------------------
	public String getType(){
		return type;
	}

	//--------------------------------------------------------------------
	public void setType(String type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public UUID getId() {
		return id;
	}
}
