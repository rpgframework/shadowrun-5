/**
 * 
 */
package org.prelle.shadowrun5;

/**
 * @author prelle
 *
 */
public enum DamageType {
	PHYSICAL,
	MENTAL,
	SPECIAL,
	;
	public String getName() { return ShadowrunCore.getI18nResources().getString("damagetype."+name().toLowerCase()); }
	public String getShortName() { return ShadowrunCore.getI18nResources().getString("damagetype."+name().toLowerCase()+".short"); }
}
