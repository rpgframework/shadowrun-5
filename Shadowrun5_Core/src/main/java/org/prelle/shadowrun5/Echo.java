/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class Echo extends BasePluginData implements Comparable<Echo> {
	
	private static Logger logger = LogManager.getLogger("shadowrun");
	
	@Attribute
	private String id;
	@Attribute(name="max")
	private int max;
	@Attribute
	private ChoiceType select;
	
	@Element
	private ModificationList modifications;
	
	//-------------------------------------------------------------------
	public Echo() {
		modifications = new ModificationList();
	}
	
	//-------------------------------------------------------------------
	public Echo(String id) {
		this();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append(id+"\n");
		buf.append(modifications+"\n");
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("echo."+id);
		} catch (MissingResourceException e) {
			logger.error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "echo."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "echo."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Echo o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//-------------------------------------------------------------------
	/**
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the select
	 */
	public ChoiceType getSelect() {
		return select;
	}

	//-------------------------------------------------------------------
	/**
	 * @param select the select to set
	 */
	public void setSelect(ChoiceType select) {
		this.select = select;
	}

}
