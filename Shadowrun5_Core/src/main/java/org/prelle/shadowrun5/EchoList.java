/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="echoes")
@ElementList(entry="echo",type=Echo.class,inline=true)
public class EchoList extends ArrayList<Echo> {

	private static final long serialVersionUID = -5648925612149808658L;

	//-------------------------------------------------------------------
	/**
	 */
	public EchoList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public EchoList(Collection<? extends Echo> c) {
		super(c);
	}

}
