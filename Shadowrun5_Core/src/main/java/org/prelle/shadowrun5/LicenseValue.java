/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class LicenseValue extends UniqueObject {
	
	@Attribute(name="sin")
	private UUID sin;
	@Attribute
	private String name;
	@Attribute(required=true)
	private SIN.Quality rating;

	//-------------------------------------------------------------------
	public LicenseValue() {
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "LicenseValue("+name+", "+rating+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sin
	 */
	public UUID getSIN() {
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sin the sin to set
	 */
	public void setSIN(UUID sin) {
		this.sin = sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public SIN.Quality getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(SIN.Quality rating) {
		this.rating = rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
