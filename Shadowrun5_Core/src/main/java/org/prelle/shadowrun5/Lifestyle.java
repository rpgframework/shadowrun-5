/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class Lifestyle extends BasePluginData implements Comparable<Lifestyle> {
	
	@Attribute
	private String id;
	@Attribute(required=true)
	private int cost;
	@Attribute(required=false)
	private int dice;
	@Attribute(required=false)
	private int factor;
	
	@Attribute(name="comfMin")
	private int comfortMin;
	@Attribute(name="comfMax")
	private int comfortMax;
	
	@Attribute(name="secMin")
	private int securityMin;
	@Attribute(name="secMax")
	private int securityMax;
	
	@Attribute(name="neighMin")
	private int neighborhoodMin;
	@Attribute(name="neighMax")
	private int neighborhoodMax;

	@Attribute
	private int points;
	
	//-------------------------------------------------------------------
	public Lifestyle() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Lifestyle o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "Lifestyle(id=null)";
    	if (i18n==null)
    		return "lifestyle."+id.toLowerCase()+".title";
        return i18n.getString("lifestyle."+id.toLowerCase()+".title");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "lifestyle."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "lifestyle."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dice
	 */
	public int getDice() {
		return dice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the factor
	 */
	public int getFactor() {
		return factor;
	}
	
}
