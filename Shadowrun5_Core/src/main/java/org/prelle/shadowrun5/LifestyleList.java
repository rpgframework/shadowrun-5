/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="lifestyles")
@ElementList(entry="lifestyle",type=Lifestyle.class)
public class LifestyleList extends ArrayList<Lifestyle> {

}
