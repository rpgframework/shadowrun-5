/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author prelle
 *
 */
public class LifestyleOption extends BasePluginData implements Comparable<LifestyleOption>, Modifyable {
	
	@Attribute
	private String id;
	@Element
	private ModificationList modifications;
	@Attribute(name="neg")
	private boolean negative;

	//-------------------------------------------------------------------
	public LifestyleOption() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(LifestyleOption o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "LifestyleOption(id=null)";
    	if (i18n==null)
    		return "lifestyleoption."+id.toLowerCase()+".title";
        return i18n.getString("lifestyleoption."+id.toLowerCase()+".title");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "lifestyleoption."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "lifestyleoption."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		return new ArrayList<Modification>(modifications);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#setModifications(java.util.List)
	 */
	@Override
	public void setModifications(List<Modification> mods) {
		modifications = new ModificationList();
		modifications.addAll(mods);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#addModification(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void addModification(Modification mod) {
		modifications.add(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modifyable#removeModification(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void removeModification(Modification mod) {
		modifications.remove(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the negative
	 */
	public boolean isNegative() {
		return negative;
	}
	
}
