/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.LifestyleOptionConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class LifestyleOptionValue implements Comparable<LifestyleOptionValue> {
	
	@Attribute(name="ref")
	@AttribConvert(value=LifestyleOptionConverter.class)
	private LifestyleOption option;

	//-------------------------------------------------------------------
	public LifestyleOptionValue() {
	}

	//-------------------------------------------------------------------
	public LifestyleOptionValue(LifestyleOption data) {
		this.option = data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(LifestyleOptionValue other) {
		return option.compareTo(other.getOption());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the option
	 */
	public LifestyleOption getOption() {
		return option;
	}

}
