/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.persist.LifestyleConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class LifestyleValue extends UniqueObject {

	private static Logger logger = LogManager.getLogger("shadowrun");
	
	@Attribute(name="name")
	private String name;
	@Attribute(name="ref")
	@AttribConvert(value=LifestyleConverter.class)
	private Lifestyle lifestyle;
	@ElementList(type=LifestyleOptionValue.class, entry="lifeopt")
	private List<LifestyleOptionValue> options;
	@Attribute(name="sin")
	private UUID sin;
	@Attribute(name="months")
	private int paidMonths;
	@Element
	private String description;
	@Attribute(name="prim")
	private boolean primary;

	//-------------------------------------------------------------------
	public LifestyleValue() {
		options = new ArrayList<LifestyleOptionValue>();
	}

	//-------------------------------------------------------------------
	public LifestyleValue(Lifestyle val) {
		this();
		lifestyle = val;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (name==null) {
			if (lifestyle==null)
				return "ERROR: Lifestyle not set";
			return "Unnamed "+lifestyle.getName();
		}
		return name;
	}

	//-------------------------------------------------------------------
	public List<LifestyleOptionValue> getOptions() {
		return options;
	}

	//-------------------------------------------------------------------
	public void addOption(LifestyleOptionValue option) {
		options.add(option);
	}

	//-------------------------------------------------------------------
	public void removeOption(LifestyleOptionValue option) {
		options.remove(option);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sin
	 */
	public UUID getSIN() {
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sin the sin to set
	 */
	public void setSIN(UUID sin) {
		this.sin = sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the paidMonths
	 */
	public int getPaidMonths() {
		return paidMonths;
	}

	//-------------------------------------------------------------------
	/**
	 * @param paidMonths the paidMonths to set
	 */
	public void setPaidMonths(int paidMonths) {
		this.paidMonths = paidMonths;
	}

	//-------------------------------------------------------------------
	public int getCostPerMonth() {
		int ret = lifestyle.getCost();
		double toAdd = 0;
		for (LifestyleOptionValue opt : options) {
			for (Modification mod : opt.getOption().getModifications()) {
				if (mod instanceof LifestyleCostModification) {
					LifestyleCostModification cMod = (LifestyleCostModification)mod;
					if (cMod.getFixed()>0)
						toAdd += cMod.getFixed();
					else
						toAdd += ret * cMod.getPercent() / 100.0;
				} else
					logger.error("Unsupported Lifestylemodification when calculating cost: "+mod);
			}
		}
		// Static mods
		for (Modification mod : modifications) {
			if (mod instanceof LifestyleCostModification) {
				LifestyleCostModification cMod = (LifestyleCostModification)mod;
				if (cMod.getFixed()>0)
					toAdd += cMod.getFixed();
				else
					toAdd += ret * cMod.getPercent() / 100.0;
			} else
				logger.error("Unsupported Lifestylemodification when calculating cost: "+mod);
		}

		
		return (ret+(int)toAdd);
	}

	//-------------------------------------------------------------------
	public int getCost() {
		return getCostPerMonth() * paidMonths;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the lifestyle
	 */
	public Lifestyle getLifestyle() {
		return lifestyle;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	public void setLifestyle(Lifestyle data) {
		this.lifestyle = data;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the primary
	 */
	public boolean isPrimary() {
		return primary;
	}

	//-------------------------------------------------------------------
	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

}
