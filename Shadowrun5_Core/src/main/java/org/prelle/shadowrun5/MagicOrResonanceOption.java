/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.shadowrun5.persist.MagicOrResonanceTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="magicopt")
public class MagicOrResonanceOption extends PriorityOption {

	@Attribute(required=true)
	@AttribConvert(MagicOrResonanceTypeConverter.class)
	private MagicOrResonanceType type;
	@Attribute
	private int value;
	@Element
	private ModificationList modifications;

	//-------------------------------------------------------------------
	public MagicOrResonanceOption() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceOption(MagicOrResonanceType type, Modification...mods) {
		this.type = type;
		modifications = new ModificationList();
		for (Modification mod : mods)
			modifications.add(mod);
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (type!=null)
			return type.getId();
		return super.toString();
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(PriorityOption o) {
    	if (o instanceof MagicOrResonanceOption) {
    		MagicOrResonanceOption other = (MagicOrResonanceOption)o;
     		return type.compareTo(other.getType());
    	}
    	return 0;
	}

	//-------------------------------------------------------------------
	public Collection<Modification> getModifications() {
		List<Modification> ret = new ArrayList<>(modifications);
//		ret.addAll(type.getModifications());
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

}
