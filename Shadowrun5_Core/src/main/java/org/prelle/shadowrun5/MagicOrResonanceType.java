/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;

import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MagicOrResonanceType extends BasePluginData implements Comparable<MagicOrResonanceType> {

	@Attribute(required=true)
	private String id;
	@Attribute
	private boolean magic;
	@Attribute
	private boolean resonance;
	@Attribute
	private boolean spells;
	@Attribute
	private boolean powers;
	@Attribute
	private boolean paysPowers;
	@Attribute
	private int cost;
	@Element
	private ModificationList modifications;

	//-------------------------------------------------------------------
	public MagicOrResonanceType() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public MagicOrResonanceType(String id) {
		this.id = id;
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public String getKey() {
		return id;
	}

    //-------------------------------------------------------------------
    public String getName() {
    	if (id==null)
    		return "MagicOrResonanceOption(id=null)";
    	if (i18n==null)
    		return "magicOrResonance."+id.toLowerCase()+".title";
        return i18n.getString("magicOrResonance."+id.toLowerCase()+".title");
    }

    //-------------------------------------------------------------------
    public String toString() {
    	return getName();
    }

    //-------------------------------------------------------------------
	public String getId() {
		return id;
	}

    //-------------------------------------------------------------------
	public void setId(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MagicOrResonanceType o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "magicOrResonance."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "magicOrResonance."+id+".desc";
	}

	//-------------------------------------------------------------------
	public boolean usesMagic() {
		return magic;
	}

	//-------------------------------------------------------------------
	public boolean usesResonance() {
		return resonance;
	}

	//-------------------------------------------------------------------
	public boolean usesSpells() {
		return spells;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the powers
	 */
	public boolean usesPowers() {
		return powers;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the paysPowers
	 */
	public boolean paysPowers() {
		return paysPowers;
	}

	//-------------------------------------------------------------------
	public Collection<Modification> getModifications() {
		// Fix source
		for (Modification mod : modifications) {
			if (mod.getSource()==null)
				mod.setSource(this);
		}
		return modifications;
	}

	public int getCost() {
		return cost;
	}

}
