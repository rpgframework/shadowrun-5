/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class MentorSpirit extends BasePluginData implements Comparable<MentorSpirit>{

	@Attribute
	private String id;

	//-------------------------------------------------------------------
	/**
	 */
	public MentorSpirit() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MentorSpirit o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null) 
			return id;
		try {
			return i18n.getString("mentorspirit."+id);
		} catch (MissingResourceException e) {
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
			logger.error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "mentorspirit."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "mentorspirit."+id+".desc";
	}

}
