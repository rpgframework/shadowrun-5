/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="mentorspirits")
@ElementList(entry="mentorspirit",type=MentorSpirit.class,inline=true)
public class MentorSpiritList extends ArrayList<MentorSpirit> {

	private static final long serialVersionUID = 9171098678035688150L;

	//-------------------------------------------------------------------
	/**
	 */
	public MentorSpiritList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MentorSpiritList(Collection<? extends MentorSpirit> c) {
		super(c);
	}

}
