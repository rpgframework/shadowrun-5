/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.Collection;

import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.shadowrun5.persist.MetaTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="metatype")
public class MetaType extends BasePluginData implements Comparable<MetaType> {

	@Attribute(required=true)
	private String id;
	@Element
	private ModificationList modifications;
	@Attribute(name="variantof")
	@AttribConvert(MetaTypeConverter.class)
	private MetaType variantOf;

	//-------------------------------------------------------------------
	public MetaType() {
	}

	//-------------------------------------------------------------------
	public MetaType(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	public String getKey() {
		return id;
	}

    //-------------------------------------------------------------------
    public String getName() {
    	if (id==null)
    		return "MetaType(id=null)";
    	if (i18n==null)
    		return "metatype."+id.toLowerCase()+".title";
        return i18n.getString("metatype."+id.toLowerCase()+".title");
    }

    //-------------------------------------------------------------------
    public String toString() {
    	return getName();
    }

    //-------------------------------------------------------------------
	public String getId() {
		return id;
	}

    //-------------------------------------------------------------------
	public void setId(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(MetaType o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	public void add(ModificationList data2) {
		// TODO Auto-generated method stub
		modifications = data2;
	}

	//-------------------------------------------------------------------
	public Collection<Modification> getModifications() {
		return modifications;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "metatype."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "metatype."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the variantOf
	 */
	public MetaType getVariantOf() {
		return variantOf;
	}

	//--------------------------------------------------------------------
	/**
	 * @param variantOf the variantOf to set
	 */
	public void setVariantOf(MetaType variantOf) {
		this.variantOf = variantOf;
	}
	
}
