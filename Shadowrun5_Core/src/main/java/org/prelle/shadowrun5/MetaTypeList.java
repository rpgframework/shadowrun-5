/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="metatypes")
@ElementList(entry="metatype",type=MetaType.class)
public class MetaTypeList extends ArrayList<MetaType> {

}
