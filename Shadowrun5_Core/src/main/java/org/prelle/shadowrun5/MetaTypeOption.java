/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.MetaTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@Root(name="metaopt")
public class MetaTypeOption extends PriorityOption {
	
	@Attribute(required=true)
	@AttribConvert(MetaTypeConverter.class)
	private MetaType type;
	@Attribute(name="spec")
	private int specialAttributePoints;
	@Attribute(name="karma")
	private int additionalKarmaKost;

	//--------------------------------------------------------------------
	public MetaTypeOption() {
		
	}

	//--------------------------------------------------------------------
	public MetaTypeOption(MetaType type) {
		this.type = type;
	}

	//--------------------------------------------------------------------
	public MetaTypeOption(MetaType type, int karma) {
		this.type = type;
		this.additionalKarmaKost = karma;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (type==null)
			return "Empty-MetaTypeOption";
		if (additionalKarmaKost==0)
			return type.getName()+" ("+specialAttributePoints+")";
		return type.getName()+" ("+specialAttributePoints+")  "+additionalKarmaKost+" "+ShadowrunCore.getI18nResources().getString("label.karma");		
	}

	//--------------------------------------------------------------------
    public int compareTo(PriorityOption o) {
    	if (o instanceof MetaTypeOption) {
    		MetaTypeOption other = (MetaTypeOption)o;
    		int cmp = ((Integer)specialAttributePoints).compareTo(other.getSpecialAttributePoints());
    		if (cmp!=0) return -cmp;
    		return type.compareTo(other.getType());
    	}
    	return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public MetaType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the specialAttributePoints
	 */
	public int getSpecialAttributePoints() {
		return specialAttributePoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the additionalKarmaKost
	 */
	public int getAdditionalKarmaKost() {
		return additionalKarmaKost;
	}

}
