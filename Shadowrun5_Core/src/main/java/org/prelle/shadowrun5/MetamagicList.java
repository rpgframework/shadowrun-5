/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="metamagics")
@ElementList(entry="metamagic",type=Metamagic.class,inline=true)
public class MetamagicList extends ArrayList<Metamagic> {

	private static final long serialVersionUID = -5648925612149808658L;

	//-------------------------------------------------------------------
	/**
	 */
	public MetamagicList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public MetamagicList(Collection<? extends Metamagic> c) {
		super(c);
	}

}
