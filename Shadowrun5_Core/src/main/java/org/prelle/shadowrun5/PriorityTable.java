/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("serial")
public class PriorityTable extends HashMap<PriorityType, Map<Priority, PriorityTableEntry>> {

	//--------------------------------------------------------------------
	public PriorityTable() {
		for (PriorityType type : PriorityType.values()) {
			Map<Priority, PriorityTableEntry> col = new HashMap<>();
			put(type, col);
			
			for (Priority prio : Priority.values()) {
				col.put(prio, new PriorityTableEntry());
			}
		}
	}
}
