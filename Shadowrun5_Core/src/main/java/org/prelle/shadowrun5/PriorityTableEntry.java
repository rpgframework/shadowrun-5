/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collections;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("serial")
@Root(name="priotableentry")
@ElementListUnion({
		@ElementList(entry="metaopt", type=MetaTypeOption.class),
		@ElementList(entry="magicopt", type=MagicOrResonanceOption.class)
})
public class PriorityTableEntry extends ArrayList<PriorityOption> {

	@Attribute(required=true)
	protected PriorityType type;
	@Attribute(name="prio",required=true)
	protected Priority priority;

	//--------------------------------------------------------------------
	public PriorityTableEntry() {
	}

	//--------------------------------------------------------------------
	public PriorityTableEntry(PriorityType type, Priority prio) {
		this.type = type;
		this.priority = prio;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public PriorityType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the priority
	 */
	public Priority getPriority() {
		return priority;
	}

	//-------------------------------------------------------------------
	public void mergeFrom(PriorityTableEntry tmp) {
		addAll(tmp);
//		Collections.sort(this);
	}

}
