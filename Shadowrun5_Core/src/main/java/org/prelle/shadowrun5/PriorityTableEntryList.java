/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author Stefan
 *
 */
@SuppressWarnings("serial")
@Root(name="priotable")
@ElementList(entry="priotableentry",type=PriorityTableEntry.class)
public class PriorityTableEntryList extends ArrayList<PriorityTableEntry> {

	
	
	//--------------------------------------------------------------------
	/**
	 */
	public PriorityTableEntryList() {
		// TODO Auto-generated constructor stub
	}

}
