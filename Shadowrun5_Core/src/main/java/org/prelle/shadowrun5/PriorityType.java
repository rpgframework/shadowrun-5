package org.prelle.shadowrun5;

public enum PriorityType {
	METATYPE,
	ATTRIBUTE,
	MAGIC,
	SKILLS,
	RESOURCES
}