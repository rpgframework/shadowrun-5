/**
 *
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class Program extends BasePluginData implements Comparable<Program> {

	private static Logger logger = LogManager.getLogger("shadowrun");

	public enum ProgramType {
		STANDARD,
		HACKING,
		AGENT
		;

	    //-------------------------------------------------------------------
	    public String getName() {
	        return ShadowrunCore.getI18nResources().getString("programtype."+this.name().toLowerCase());
	    }
	}

	@Attribute(required=true)
	private String id;
	@Attribute(required=true)
	private ProgramType type;
	@Attribute(name="rate")
	private boolean rating;
	@Element
	private ModificationList modifications;

	//--------------------------------------------------------------------
	public Program() {
		modifications = new ModificationList();
		type = ProgramType.STANDARD;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("program."+id);
		} catch (MissingResourceException e) {
			logger.error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return id;
		}
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "program."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "program."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Program o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ProgramType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ProgramType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	public boolean hasRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	public void setRating(boolean rating) {
		this.rating = rating;
	}
}
