/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="programs")
@ElementList(entry="program",type=Program.class,inline=true)
public class ProgramList extends ArrayList<Program> {

	private static final long serialVersionUID = -7162251904580435269L;

	//-------------------------------------------------------------------
	/**
	 */
	public ProgramList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ProgramList(Collection<? extends Program> c) {
		super(c);
	}

}
