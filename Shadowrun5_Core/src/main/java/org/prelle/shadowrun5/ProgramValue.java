/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.ProgramConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class ProgramValue extends ModifyableImpl implements Modifyable, Comparable<ProgramValue> {

	@Attribute(name="ref")
	@AttribConvert(ProgramConverter.class)
	private Program program;
	@Attribute(name="rating")
	private int rating;

	//-------------------------------------------------------------------
	public ProgramValue() {
	}

	//-------------------------------------------------------------------
	public ProgramValue(Program power) {
		this.program = power;
	}

	//-------------------------------------------------------------------
	public String getName() {
		StringBuffer ret = new StringBuffer(program.getName());
		return ret.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ProgramValue other) {
		return program.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public Program getModifyable() {
		return program;
	}

	//-------------------------------------------------------------------
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	public void setRating(int rating) {
		this.rating = rating;
	}

}
