/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="qualities")
@ElementList(entry="quality",type=Quality.class,inline=true)
public class QualityList extends ArrayList<Quality> {

	private static final long serialVersionUID = -9164631466079745472L;

	//-------------------------------------------------------------------
	/**
	 */
	public QualityList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public QualityList(Collection<? extends Quality> c) {
		super(c);
	}

}
