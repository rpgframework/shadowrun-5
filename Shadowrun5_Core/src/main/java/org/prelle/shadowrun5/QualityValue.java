/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.actions.ShadowrunAction;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.shadowrun5.persist.QualityConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "qualityval")
public class QualityValue extends ModifyableImpl implements ModifyableValue<Quality>, Comparable<QualityValue> {


	@Attribute(name="quality")
	@AttribConvert(QualityConverter.class)
	private Quality quality;
	/**
	 * The current value on the skill without the attributes
	 */
	@Attribute(name="val")
	private int value;
	@Element
	private String description;
	@Attribute(name="choice")
	private String choiceReference;
	private transient Object choice;

	//-------------------------------------------------------------------
	public QualityValue() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public QualityValue(Quality data, int val) {
		this();
		this.quality = data;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (modifications.isEmpty())
			return String.format("%s = %d",
					getName(),
					value
					);
		return String.format("%s = %d (mod=%s)",
				getName(),
				value,
				String.valueOf(modifications)
				);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Quality getModifyable() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QualityValue other) {
		return quality.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
//		for (Modification mod : modifications) {
//			if (mod instanceof SkillModification) {
//				SkillModification sMod = (SkillModification)mod;
//				if (sMod.getSkill()==skill)
//					count += sMod.getValue();
//			}
//		}
		return count;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (quality==null) 
			return "null";
		
		if (description!=null)
			return quality.getName()+" ("+description+")";
		if (choice!=null) {
			switch (quality.getSelect()) {
			case ATTRIBUTE:
			case LIMIT:
			case PHYSICAL_ATTRIBUTE:
				return quality.getName()+" ("+((org.prelle.shadowrun5.Attribute)choice).getName()+")";
			case COMBAT_SKILL:
			case SKILL:
				return quality.getName()+" ("+((Skill)choice).getName()+")";
			case MATRIX_ACTION: return quality.getName()+" ("+((ShadowrunAction)choice).getName()+")";
			case SKILLGROUP: return quality.getName()+" ("+((SkillGroup)choice).getName()+")";
			case MENTOR_SPIRIT: return quality.getName()+" ("+((MentorSpirit)choice).getName()+")";
//			case SENSE:
			}
			return quality.getName()+" ("+choice+")";
		}
		return quality.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		List<Modification> ret = new ArrayList<>();
		if (!quality.needsChoice())
			ret.addAll(quality.getModifications());
		ret.addAll(modifications);
		return ret;
	}

//	//-------------------------------------------------------------------
//	public void addModification(Modification mod) {
//		modifications.add(mod);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
		
		if (choice==null)
			return;
		
//		if (choiceReference==null)
//			System.err.println("Set ref by "+choice+"    // "+choice.getClass());
		if (choice instanceof BasePluginData) {
			choiceReference = ((BasePluginData)choice).getId();
		} else
			choiceReference = String.valueOf(choice);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}

}
