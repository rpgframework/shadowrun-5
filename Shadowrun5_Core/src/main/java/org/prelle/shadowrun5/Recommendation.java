/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.QualityConverter;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.shadowrun5.persist.SkillGroupConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class Recommendation {

	@org.prelle.simplepersist.Attribute
	private Attribute attr;
	@org.prelle.simplepersist.Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@org.prelle.simplepersist.Attribute
	@AttribConvert(SkillGroupConverter.class)
	private SkillGroup skillgrp;
	@AttribConvert(QualityConverter.class)
	private Quality quality;

	//--------------------------------------------------------------------
	public Recommendation() {
	}

	//--------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer("Recommend");
		if (attr!=null)
			buf.append(" attr="+attr);
		if (skill!=null)
			buf.append(" skill="+skill);
		if (skillgrp!=null)
			buf.append(" skillgrp="+skillgrp);
		if (quality!=null)
			buf.append(" quality="+quality);
		return buf.toString();
	}

	//--------------------------------------------------------------------
	public Attribute getAttribute() {
		return attr;
	}

	//--------------------------------------------------------------------
	public Skill getSkill() {
		return skill;
	}

	//--------------------------------------------------------------------
	public SkillGroup getSkillgroup() {
		return skillgrp;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		return quality;
	}

}
