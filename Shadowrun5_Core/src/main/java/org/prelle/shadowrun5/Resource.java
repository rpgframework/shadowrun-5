package org.prelle.shadowrun5;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;

/**
 * @author Stefan Prelle
 *
 */
public class Resource {
	
	//-------------------------------------------------------------------
	public static String get(ResourceBundle res, String key) {
		if (res==null) 
			return key;
		try {
			return res.getString(key);
		} catch (MissingResourceException e) {
			LogManager.getLogger("shadowrun5").error("Missing key '"+key+"' in "+res.getBaseBundleName());
			LogManager.getLogger("shadowrun5").error(" => "+e.getStackTrace()[3]);
		}
		return key;
	}
	
	//-------------------------------------------------------------------
	public static String format(ResourceBundle res, String key, Object...objects) {
		try {
			return String.format(res.getString(key), objects);
		} catch (MissingResourceException e) {
			LogManager.getLogger("shadowrun5").error("Missing key '"+key+"' in "+res.getBaseBundleName());
			LogManager.getLogger("shadowrun5").error(" => "+e.getStackTrace()[3]);
		}
		return key;
	}

}
