/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public class Ritual extends BasePluginData implements Comparable<Ritual> {
	
	private static Logger logger = LogManager.getLogger("shadowrun");
	
	@Attribute
	private String id;
	
	@ElementList(entry="ritualfeature",type=RitualFeatureReference.class, inline=true)
	private List<RitualFeatureReference> features;
	
	//-------------------------------------------------------------------
	public Ritual() {
		features = new ArrayList<RitualFeatureReference>();
	}
	
	//-------------------------------------------------------------------
	public Ritual(String id) {
		this();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append(id+"\n");
		buf.append(features+"\n");
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("ritual."+id);
		} catch (MissingResourceException e) {
			logger.error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "ritual."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "ritual."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Ritual o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the features
	 */
	public List<RitualFeatureReference> getFeatures() {
		return features;
	}

	//-------------------------------------------------------------------
	/**
	 * @param features the features to set
	 */
	public void setFeatures(List<RitualFeatureReference> features) {
		this.features = features;
	}

}
