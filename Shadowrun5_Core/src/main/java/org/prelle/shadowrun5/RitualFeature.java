/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class RitualFeature extends BasePluginData implements Comparable<RitualFeature> {
	
	@Attribute(required=true)
	private String id;

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("ritualfeature."+id.toLowerCase());
		} catch (MissingResourceException e) {
			LogManager.getLogger("shadowrun").error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return "ritualfeature."+id.toLowerCase();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "ritualfeature."+id.toLowerCase()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "ritualfeature."+id.toLowerCase()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RitualFeature o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

}
