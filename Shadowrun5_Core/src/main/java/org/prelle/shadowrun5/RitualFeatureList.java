/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="features")
@ElementList(entry="feature",type=RitualFeature.class)
public class RitualFeatureList extends ArrayList<RitualFeature> {

}
