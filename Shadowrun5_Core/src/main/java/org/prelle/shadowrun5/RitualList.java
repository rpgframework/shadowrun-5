/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="rituals")
@ElementList(entry="ritual",type=Ritual.class,inline=true)
public class RitualList extends ArrayList<Ritual> {

	private static final long serialVersionUID = 7483417073470617654L;

	//-------------------------------------------------------------------
	/**
	 */
	public RitualList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public RitualList(Collection<? extends Ritual> c) {
		super(c);
	}

}
