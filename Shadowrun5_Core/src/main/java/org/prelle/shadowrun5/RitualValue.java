/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.RitualConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class RitualValue extends ModifyableImpl implements Modifyable, Comparable<RitualValue> {

	@Attribute(name="ritual")
	@AttribConvert(RitualConverter.class)
	private Ritual ritual;

	//-------------------------------------------------------------------
	public RitualValue() {
	}

	//-------------------------------------------------------------------
	public RitualValue(Ritual ritual) {
		this.ritual = ritual;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(RitualValue other) {
		return ritual.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public Ritual getModifyable() {
		return ritual;
	}

}
