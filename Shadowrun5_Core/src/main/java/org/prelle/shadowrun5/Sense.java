/**
 * 
 */
package org.prelle.shadowrun5;

/**
 * @author prelle
 *
 */
public enum Sense {

	LOW_LIGHT_VISION,
	THERMOGRAPHIC_VISION,
	VISION_MAGNIFICATION,
	HIGH_FREQUENCY_HEARING,
	LOW_FREQUENCY_HEARING,
	DIRECTION_SENSE,
	IMPROVED_TACTILE,
	PERFECT_PITCH,
	HUMAN_SCALE
	;
	
	public String getName() {
        return ShadowrunCore.getI18nResources().getString("sense."+this.name().toLowerCase());
	}
}
