/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class SensorFunction extends BasePluginData implements Comparable<SensorFunction> {

	@Attribute(required=true)
	private String id;

	//-------------------------------------------------------------------
	public SensorFunction() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return null;
		try {
			return i18n.getString("sensorfunction."+id);
		} catch (MissingResourceException e) {
			logger.warn("Missing "+e.getKey()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "sensorfunction."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "sensorfunction."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SensorFunction o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

}
