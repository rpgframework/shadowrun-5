/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "sensorfunctions")
@ElementList(entry="sensorfunction",type=SensorFunction.class)
public class SensorFunctionList extends ArrayList<SensorFunction> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	public SensorFunctionList() {
	}

	//-------------------------------------------------------------------
	public SensorFunctionList(Collection<? extends SensorFunction> c) {
		super(c);
	}

}
