/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.function.Consumer;

import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.CarriedItemList;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.shadowrun5.persist.CharacterConceptConverter;
import org.prelle.shadowrun5.persist.MagicOrResonanceTypeConverter;
import org.prelle.shadowrun5.persist.MetaTypeConverter;
import org.prelle.shadowrun5.persist.TraditionConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="sr5char")
public class ShadowrunCharacter implements RuleSpecificCharacterObject {

	public enum Gender {
		MALE,
		FEMALE
		;
		public String toString() {
			return ShadowrunCore.getI18nResources().getString("gender."+name().toLowerCase());
		}
	}

	@Attribute(name="meta",required=true)
	@AttribConvert(MetaTypeConverter.class)
	private MetaType metatype;
	@Attribute(name="magic")
	@AttribConvert(MagicOrResonanceTypeConverter.class)
	private MagicOrResonanceType magicOrResonanceType;
	@Attribute(name="concept")
	@AttribConvert(CharacterConceptConverter.class)
	private CharacterConcept concept;
	@Attribute(name="karmaI")
	private int karmaInvested;
	@Attribute(name="karmaF")
	private int karmaFree;
	@Attribute(name="nuyen")
	private int nuyen;
	@Attribute(name="power")
	private int boughtPowerPoints;
	@Element
	private String name;
	@Element(name="realname")
	private String realname;
	@ElementList(entry="attribute",type=AttributeValue.class)
	protected Attributes attributes;
	@ElementListUnion({
	    @ElementList(entry="skillgrpval", type=SkillGroupValue.class),
	    @ElementList(entry="skillval", type=SkillValue.class),
	 })
	protected List<ModifyableValue<?>> skills;
	@Attribute(name="trad")
	@AttribConvert(TraditionConverter.class)
	protected Tradition tradition;
	@ElementList(entry="spellval", type=SpellValue.class)
	protected List<SpellValue> spells;
	@ElementList(entry="ritualval", type=RitualValue.class)
	protected List<RitualValue> rituals;
	@ElementList(entry="qualityval", type=QualityValue.class)
	protected List<QualityValue> qualities;
	protected transient List<QualityValue> racialQualities;
	@ElementList(entry="powerval", type=AdeptPowerValue.class)
	protected List<AdeptPowerValue> powers;
	@ElementList(entry="complexval", type=ComplexFormValue.class)
	protected List<ComplexFormValue> complexforms;
	@ElementList(entry="programs", type=Program.class)
	protected List<ProgramValue> programs;
	@ElementList(entry="summonables", type=SummonableValue.class)
	protected List<SummonableValue> summonables;
	@Element
	private byte[] image;
	@Element
	private String hairColor, eyeColor, ethnicity, age;
	@Element
	private int size;
	@Element
	private int weight;
	@Attribute(name="ess")
	private float unusedEssence;
	@Element
	private Gender gender;
	@ElementList(entry="connection", type=Connection.class)
	protected List<Connection> connections;
	@Element
	protected CarriedItemList gear;
	protected transient CarriedItemList autoGear;
	@ElementList(entry="sin", type=SIN.class)
	protected List<SIN> sins;
	@ElementList(entry="lifestyle", type=LifestyleValue.class)
	protected List<LifestyleValue> lifestyles;
	@ElementList(entry="licenses", type=LicenseValue.class)
	protected List<LicenseValue> licenses;

	@Element
	protected ModificationList history;
	@Element
	protected RewardList rewards;
	protected transient List<Sense> specialSenses;
	@Element
	protected String notes;

	//-------------------------------------------------------------------
	/**
	 */
	public ShadowrunCharacter() {
		name = "Unnamed chummer";
		gender    = Gender.MALE;
		attributes = new Attributes();
		skills  = new ArrayList<>();
		spells  = new ArrayList<>();
		rituals = new ArrayList<>();
		qualities = new ArrayList<>();
		racialQualities = new ArrayList<>();
		powers    = new ArrayList<>();
		complexforms = new ArrayList<>();
		programs     = new ArrayList<>();
		connections  = new ArrayList<>();

		gear       = new CarriedItemList();
		autoGear   = new CarriedItemList();
		sins       = new ArrayList<>();
		lifestyles = new ArrayList<>();
		licenses   = new ArrayList<>();
		summonables = new ArrayList<>();

		history = new ModificationList();
		rewards = new RewardList();
		specialSenses = new ArrayList<>();
		magicOrResonanceType = ShadowrunCore.getMagicOrResonanceType("mundane");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void setName(String newName) {
		this.name = newName;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.character.RuleSpecificCharacterObject#getImage()
	 */
	@Override
	public byte[] getImage() {
		return image;
	}

	//-------------------------------------------------------------------
	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the metatype
	 */
	public MetaType getMetatype() {
		return metatype;
	}

	//--------------------------------------------------------------------
	/**
	 * @param metatype the metatype to set
	 */
	public void setMetatype(MetaType metatype) {
		this.metatype = metatype;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaInvested
	 */
	public int getKarmaInvested() {
		return karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaInvested the karmaInvested to set
	 */
	public void setKarmaInvested(int karmaInvested) {
		this.karmaInvested = karmaInvested;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the karmaFree
	 */
	public int getKarmaFree() {
		return karmaFree;
	}

	//--------------------------------------------------------------------
	/**
	 * @param karmaFree the karmaFree to set
	 */
	public void setKarmaFree(int karmaFree) {
		this.karmaFree = karmaFree;
	}

	//-------------------------------------------------------------------
	public AttributeValue getAttribute(org.prelle.shadowrun5.Attribute key) {
		return attributes.get(key);
	}

	//-------------------------------------------------------------------
	public String dumpAttributes() {
		StringBuffer buf = new StringBuffer();
		for (org.prelle.shadowrun5.Attribute key : org.prelle.shadowrun5.Attribute.values()) {
			if (key==org.prelle.shadowrun5.Attribute.ATTACK)
				break;
		  buf.append(key+" = "+attributes.get(key)+"\n");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public Collection<AttributeValue> getAttributes(org.prelle.shadowrun5.Attribute[] select) {
		List<AttributeValue> ret = new ArrayList<>();
		for (org.prelle.shadowrun5.Attribute attr : select)
			ret.add(attributes.get(attr));
		return ret;
	}

	//-------------------------------------------------------------------
	public void addSkill(SkillValue sVal) {
		if (sVal==null)
			throw new NullPointerException();
		if (!skills.contains(sVal))
			skills.add(sVal);
	}

	//-------------------------------------------------------------------
	public void removeSkill(SkillValue sVal) {
		skills.remove(sVal);
	}

	//-------------------------------------------------------------------
	public void addSkillGroup(SkillGroupValue sVal) {
		skills.add(sVal);
	}

	//-------------------------------------------------------------------
	public void removeSkillGroup(SkillGroupValue sVal) {
		if (!skills.remove(sVal))
			System.err.println("No group "+sVal+" in "+skills+" to remove");
	}

	//-------------------------------------------------------------------
	/**
	 * Return a list of all skills with a value of at least 0.
	 * @param withGroups Unfold all selected skillgroups into their corresponding skills
	 * @return
	 */
	public List<SkillValue> getSkillValues(boolean withGroups) {
		ArrayList<SkillValue> ret = new ArrayList<>();
		for (ModifyableValue<?> tmp : skills) {
			if (tmp instanceof SkillValue)
				ret.add((SkillValue) tmp);
			else if (tmp instanceof SkillGroupValue) {
				if (withGroups) {
					SkillGroupValue val = (SkillGroupValue)tmp;
					for (Skill skill : ShadowrunCore.getSkills(val.getModifyable())) {
						SkillValue sVal = new SkillValue(skill, val.getModifiedValue());
						ret.add(sVal);
					}
				}
			}
		}
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Return a list of all skills with a value of at least 0.
	 * @param withGroups Unfold all selected skillgroups into their corresponding skills
	 * @return
	 */
	public List<Skill> getSkills(boolean withGroups) {
		ArrayList<Skill> ret = new ArrayList<>();
		for (ModifyableValue<?> tmp : skills) {
			if (tmp instanceof SkillValue)
				ret.add( ((SkillValue) tmp).getModifyable());
			else if (tmp instanceof SkillGroupValue) {
				if (withGroups) {
					SkillGroupValue val = (SkillGroupValue)tmp;
					for (Skill skill : ShadowrunCore.getSkills(val.getModifyable())) {
						SkillValue sVal = new SkillValue(skill, val.getModifiedValue());
						ret.add(sVal.getModifyable());
					}
				}
			}
		}
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get a combined list of skills and skillgroups for a given skilltype.
	 */
	public List<ModifyableValue<?>> getSkillsAndGroups(SkillType type) {
		List<ModifyableValue<?>> ret = new ArrayList<>();
		// Groups first
		for (SkillGroupValue grp : getSkillGroupValues()) {
			if (grp.getModifyable().getType()==type)
				ret.add(grp);
		}
		// Than skills
		for (SkillValue grp : getSkillValues(false)) {
			if (grp.getModifyable().getType()==type)
				ret.add(grp);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Get a combined list of skills and skillgroups, sorted by skilltype
	 * and alphabetically.
	 */
	public List<ModifyableValue<?>> getSkillsAndGroups() {
		List<ModifyableValue<?>> ret = new ArrayList<>();
		for (SkillType type : SkillType.values()) {
			ret.addAll(getSkillsAndGroups(type));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public SkillGroupValue getSkillGroupValue(SkillGroup sgroup) {
		for (SkillGroupValue tmp : getSkillGroupValues()) {
			if (tmp.getModifyable()==sgroup)
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public List<SkillGroupValue> getSkillGroupValues() {
		ArrayList<SkillGroupValue> ret = new ArrayList<>();
		for (ModifyableValue<?> tmp : skills) {
			if (tmp instanceof SkillGroupValue) {
				ret.add( (SkillGroupValue)tmp );
			}
		}
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<SkillGroup> getSkillGroups() {
		ArrayList<SkillGroup> ret = new ArrayList<>();
		for (ModifyableValue<?> tmp : skills) {
			if (tmp instanceof SkillGroupValue) {
				ret.add( ((SkillGroupValue)tmp).getModifyable() );
			}
		}
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public SkillValue getSkillValue(Skill skill) {
		for (SkillValue tmp : getSkillValues(false)) {
			if (tmp.getModifyable()==skill)
				return tmp;
		}

		// Check groups
		if (skill.getGroup()!=null) {
			SkillGroupValue gVal = getSkillGroupValue(skill.getGroup());
			if (gVal!=null) {
				return new SkillValue(skill, gVal.getModifiedValue());
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	public SkillValue getSkillValueExisting(Skill skill) {
		for (SkillValue tmp : getSkillValues(false)) {
			if (tmp.getModifyable()==skill)
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public int getSkillPool(Skill skill) {
		AttributeValue aVal = getAttribute(skill.getAttribute1());
		SkillValue     sVal = getSkillValue(skill);
		int skillValue = -2;
		if (sVal != null) {
			skillValue = sVal.getModifiedValue();
		}
		if ((aVal.getModifiedValue()+skillValue)<0)
			System.err.println("ShadowrunCharacter.getSkillPool: Skill "+skill+" = Attribute "+aVal+" plus "+skillValue);
		return aVal.getModifiedValue() + skillValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the magicOrResonanceType
	 */
	public MagicOrResonanceType getMagicOrResonanceType() {
		return magicOrResonanceType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param magicOrResonanceType the magicOrResonanceType to set
	 */
	public void setMagicOrResonanceType(MagicOrResonanceType magicOrResonanceType) {
		this.magicOrResonanceType = magicOrResonanceType;
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells() {
		ArrayList<SpellValue> ret = new ArrayList<>(spells);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<SpellValue> getSpells(boolean alchemistic) {
		ArrayList<SpellValue> ret = new ArrayList<>();
		for (SpellValue tmp : spells) {
			if (alchemistic == tmp.isAlchemistic())
				ret.add(tmp);
		}
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addSpell(SpellValue ref) {
		if (!spells.contains(ref))
			spells.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeSpell(SpellValue ref) {
		spells.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasSpell(String id) {
		for (SpellValue ref : spells) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public SpellValue getSpell(String id) {
		for (SpellValue ref : spells) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Spell "+id);
	}

	//-------------------------------------------------------------------
	public List<RitualValue> getRituals() {
		ArrayList<RitualValue> ret = new ArrayList<>(rituals);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addRitual(RitualValue ref) {
		if (!rituals.contains(ref))
			rituals.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeRitual(RitualValue ref) {
		rituals.remove(ref);
	}

	//-------------------------------------------------------------------
	public boolean hasRitual(String id) {
		for (RitualValue ref : rituals) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(qualities);
		ret.addAll(racialQualities);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getUserSelectedQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(qualities);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<QualityValue> getSystemSelectedQualities() {
		ArrayList<QualityValue> ret = new ArrayList<>(racialQualities);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasQuality(String id) {
		for (QualityValue ref : qualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		for (QualityValue ref : racialQualities) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addQuality(QualityValue ref) {
		if (!qualities.contains(ref))
			qualities.add(ref);
	}

	//-------------------------------------------------------------------
	public void addRacialQuality(QualityValue ref) {
		if (!racialQualities.contains(ref))
			racialQualities.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeQuality(QualityValue ref) {
		qualities.remove(ref);
	}

	//-------------------------------------------------------------------
	public void removeRacialQuality(QualityValue ref) {
		racialQualities.remove(ref);
	}

	//-------------------------------------------------------------------
	public QualityValue getQuality(String id) {
		for (QualityValue ref : qualities) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		for (QualityValue ref : racialQualities) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Quality "+id);
	}

	//-------------------------------------------------------------------
	public List<AdeptPowerValue> getAdeptPowers() {
		ArrayList<AdeptPowerValue> ret = new ArrayList<>(powers);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasAdeptPower(String id) {
		for (AdeptPowerValue ref : powers) {
			if (ref.getModifyable().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addAdeptPower(AdeptPowerValue ref) {
		if (!powers.contains(ref))
			powers.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeAdeptPower(AdeptPowerValue ref) {
		powers.remove(ref);
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue getAdeptPower(String id) {
		for (AdeptPowerValue ref : powers) {
			if (ref.getModifyable().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("AdeptPower "+id);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the hairColor
	 */
	public String getHairColor() {
		return hairColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param hairColor the hairColor to set
	 */
	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the eyeColor
	 */
	public String getEyeColor() {
		return eyeColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param eyeColor the eyeColor to set
	 */
	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	//-------------------------------------------------------------------
	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	//-------------------------------------------------------------------
	public int getWeight() {
		return weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	//-------------------------------------------------------------------
	/**
	 * @param gender the gender to set
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the realName
	 */
	public String getRealName() {
		return realname;
	}

	//-------------------------------------------------------------------
	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realname = realName;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the concept
	 */
	public CharacterConcept getConcept() {
		return concept;
	}

	//--------------------------------------------------------------------
	/**
	 * @param concept the concept to set
	 */
	public void setConcept(CharacterConcept concept) {
		this.concept = concept;
	}

	//--------------------------------------------------------------------
	public int getPowerPoints() {
		if (magicOrResonanceType!=null && magicOrResonanceType.usesMagic()) {
			if (magicOrResonanceType.paysPowers())
				return getBoughtPowerPoints();
			else
				return getAttribute(org.prelle.shadowrun5.Attribute.MAGIC).getModifiedValue();
		}
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the boughtPowerPoints
	 */
	public int getBoughtPowerPoints() {
		return boughtPowerPoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @param boughtPowerPoints the boughtPowerPoints to set
	 */
	public void setBoughtPowerPoints(int boughtPowerPoints) {
		this.boughtPowerPoints = boughtPowerPoints;
	}

	//-------------------------------------------------------------------
	/**
	 * @param includeAutoGear Include gear given automatically - e.h. natural armor from metatype
	 */
	public List<CarriedItem> getItems(boolean includeAutoGear) {
		ArrayList<CarriedItem> ret = new ArrayList<>(gear);
		if (includeAutoGear)
			ret.addAll(autoGear);
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItemsRecursive(boolean includeAutoGear) {
		ArrayList<CarriedItem> ret = new ArrayList<>(gear);
		if (includeAutoGear)
			ret.addAll(autoGear);
		for (CarriedItem item : gear) {
			ret.addAll(item.getSubItems());
		}
		// Add auto gear
		ret.addAll(autoGear);
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItems(boolean includeAutoGear, ItemType... types) {
		final List<CarriedItem> ret = new ArrayList<>();
		final List<ItemType> valid = Arrays.asList(types);
		getItems(includeAutoGear).forEach(new Consumer<CarriedItem>() {
			public void accept(CarriedItem item) {
				if (valid.contains(item.getItem().getType()))
					ret.add(item);
			}
		});
//		// Special for WEAPON - add cyber weapons
//		if (valid.contains(ItemType.WEAPON)) {
//			gear.forEach(new Consumer<CarriedItem>() {
//				public void accept(CarriedItem item) {
//					if (item.getWeaponData()!=null && !item.getWeaponData().isEmpty() && !ret.contains(item)) {
//						System.err.println("Also add "+item);
//						ret.add(item);
//					}
//				}
//			});
//		}
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getItemsRecursive(boolean includeAutoGear, ItemType... types) {
		final List<CarriedItem> ret = new ArrayList<>();
		final List<ItemType> valid = Arrays.asList(types);
		getItems(includeAutoGear).forEach(new Consumer<CarriedItem>() {
			public void accept(CarriedItem item) {
				if (valid.contains(item.getItem().getType())) {
					ret.add(item);
				} else if (valid.contains(ItemType.ARMOR) && item.getItem().getArmorData()!=null) {
					ret.add(item);
				}
				for (CarriedItem item2 : item.getSubItems()) {
					if (valid.contains(item2.getItem().getType()))
						ret.add(item2);
				}
			}
		});
		
		// Sort them
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasItem(String id) {
		for (CarriedItem ref : getItems(true)) {
			if (ref.getItem().getId().equals(id))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public void addItem(CarriedItem ref) {
		if (!gear.contains(ref))
			gear.add(ref);
	}

	//-------------------------------------------------------------------
	public boolean removeItem(CarriedItem ref) {
		return gear.remove(ref);
	}

	//-------------------------------------------------------------------
	public CarriedItem getItem(String id) {
		for (CarriedItem ref : getItems(true)) {
			if (ref.getItem().getId().equals(id))
				return ref;
		}
		throw new NoSuchElementException("Item "+id);
	}

	//-------------------------------------------------------------------
	public void clearAutoGear() {
		autoGear.clear();
	}

	//-------------------------------------------------------------------
	public void addAutoItem(CarriedItem ref) {
		if (!autoGear.contains(ref))
			autoGear.add(ref);
	}

	//-------------------------------------------------------------------
	public void removeAutoItem(CarriedItem toRemove) {
		autoGear.remove(toRemove);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the nuyen
	 */
	public int getNuyen() {
		return nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @param nuyen the nuyen to set
	 */
	public void setNuyen(int nuyen) {
		this.nuyen = nuyen;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<ComplexFormValue> getComplexForms() {
		return complexforms;
	}

	//--------------------------------------------------------------------
	public void addComplexForm(ComplexFormValue val) {
		complexforms.add(val);
	}

	//--------------------------------------------------------------------
	public void removeComplexForm(ComplexFormValue val) {
		complexforms.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<Connection> getConnections() {
		return connections;
	}

	//--------------------------------------------------------------------
	public void addConnection(Connection val) {
		connections.add(val);
	}

	//--------------------------------------------------------------------
	public void removeConnection(Connection val) {
		connections.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<SIN> getSINs() {
		return sins;
	}

	//--------------------------------------------------------------------
	public void addSIN(SIN val) {
		if (!sins.contains(val))
			sins.add(val);
	}

	//--------------------------------------------------------------------
	public void removeSIN(SIN val) {
		sins.remove(val);
	}

	//--------------------------------------------------------------------
	public SIN getSIN(UUID uuid) {
		for (SIN sin : sins) {
			if (sin.getUniqueId().equals(uuid))
				return sin;
		}
		throw new NoSuchElementException("No SIN with UUID "+uuid);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LifestyleValue> getLifestyle() {
		return new ArrayList<>(lifestyles);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LifestyleValue> getLifestyles(SIN sin) {
		List<LifestyleValue> ret = new ArrayList<>();
		for (LifestyleValue tmp : lifestyles) {
			if (tmp.getSIN()!=null && tmp.getSIN().equals(sin.getUniqueId()))
				ret.add(tmp);
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public void addLifestyle(LifestyleValue val) {
		if (!lifestyles.contains(val))
			lifestyles.add(val);
	}

	//--------------------------------------------------------------------
	public void removeLifestyle(LifestyleValue val) {
		lifestyles.remove(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses() {
		return new ArrayList<>(licenses);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the complexForms
	 */
	public List<LicenseValue> getLicenses(SIN sin) {
		List<LicenseValue> ret = new ArrayList<>();
		for (LicenseValue tmp : licenses) {
			if (tmp.getSIN().equals(sin.getUniqueId()))
				ret.add(tmp);
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public void addLicense(LicenseValue val) {
		if (!licenses.contains(val))
			licenses.add(val);
	}

	//--------------------------------------------------------------------
	public void removeLicense(LicenseValue val) {
		licenses.remove(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the ethnicity
	 */
	public String getEthnicity() {
		return ethnicity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ethnicity the ethnicity to set
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity;
	}

	//--------------------------------------------------------------------
	public void addToHistory(Modification mod) {
		history.add(mod);
	}

	//--------------------------------------------------------------------
	public void removeFromHistory(Modification mod) {
		history.remove(mod);
	}

	//--------------------------------------------------------------------
	public List<Modification> getHistory() {
		return new ArrayList<Modification>(history);
	}

	//--------------------------------------------------------------------
	public void addReward(Reward rew) {
		if (!rewards.contains(rew))
			rewards.add(rew);
	}

	//--------------------------------------------------------------------
	public List<Reward> getRewards() {
		return new ArrayList<Reward>(rewards);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the tradition
	 */
	public Tradition getTradition() {
		return tradition;
	}

	//-------------------------------------------------------------------
	/**
	 * @param tradition the tradition to set
	 */
	public void setTradition(Tradition tradition) {
		this.tradition = tradition;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the age
	 */
	public String getAge() {
		return age;
	}

	//-------------------------------------------------------------------
	/**
	 * @param age the age to set
	 */
	public void setAge(String age) {
		this.age = age;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the unusedEssence
	 */
	public float getUnusedEssence() {
		return unusedEssence;
	}

	//-------------------------------------------------------------------
	/**
	 * @param unusedEssence the unusedEssence to set
	 */
	public void setUnusedEssence(float unusedEssence) {
		this.unusedEssence = unusedEssence;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the summonables
	 */
	public List<SummonableValue> getSummonables() {
		return summonables;
	}

	//--------------------------------------------------------------------
	public void addSummonable(SummonableValue toAdd) {
		summonables.add(toAdd);
	}

	//--------------------------------------------------------------------
	public void removeSummonable(SummonableValue toRem) {
		summonables.remove(toRem);
	}

	//--------------------------------------------------------------------
	public List<Sense> getSpecialSenses() {
		return specialSenses;
	}

	//--------------------------------------------------------------------
	public void setSpecialSenses(List<Sense> specialSenses) {
		this.specialSenses = specialSenses;
	}

	//-------------------------------------------------------------------
	public String getNotes() {
		return notes;
	}

	//-------------------------------------------------------------------
	public void setNotes(String txt) {
		this.notes = txt;
	}

	//-------------------------------------------------------------------
	public float getEssenceInvested() {
		float sum = 0.0f;
		for (CarriedItem item : getItems(false)) { 
			if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getItem().getType())) {
				float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
				sum += essence;
			}
		}
			
		float normalLow = 6.0f - sum;
		if (getUnusedEssence()==0 || normalLow<getUnusedEssence()) {
			setUnusedEssence(normalLow);
		}
		
		return sum;
	}

}
