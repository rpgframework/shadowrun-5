/**
 *
 */
package org.prelle.shadowrun5;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.items.ItemList;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.items.WeaponData;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class ShadowrunCustomDataCore {

	private static Logger logger = LogManager.getLogger("shadowrun");

	static class PerTypeData {
		Path datafile;
		Path i18nPath;
		Path i18nHelpPath;
		WriteablePropertyResourceBundle i18NResources;
		WriteablePropertyResourceBundle i18NHelpResources;

		public PerTypeData(Path srDataDir, String name) {
			datafile  = srDataDir.resolve(name+".xml");
			i18nPath  = srDataDir.resolve(name+".properties");
			i18nHelpPath = srDataDir.resolve(name+"-help.properties");
			try {
				Files.createDirectories(srDataDir);
				if (!Files.exists(i18nPath))
					Files.createFile(i18nPath);
				if (!Files.exists(i18nHelpPath))
					Files.createFile(i18nHelpPath);
				i18NResources = new WriteablePropertyResourceBundle(new FileReader(i18nPath.toFile()));
				i18NHelpResources = new WriteablePropertyResourceBundle(new FileReader(i18nHelpPath.toFile()));
			} catch (IOException e) {
				logger.fatal("Failed creating custom directory or file: "+i18nPath+": "+e);
			}
		}
	}

	private static Path srDataDir;
	private static Map<Class<? extends BasePluginData>, PerTypeData> definitions;
	private static Serializer serializer;

	private static ItemList items;

	//-------------------------------------------------------------------
	static {
		String dataDir = RPGFrameworkLoader.getInstance().getConfiguration().getOption(RPGFramework.PROP_DATADIR).getStringValue();
		srDataDir = FileSystems.getDefault().getPath(dataDir, "custom", RoleplayingSystem.SHADOWRUN.name().toLowerCase());
		logger.info("Search for custom items in "+srDataDir);

		definitions = new HashMap<Class<? extends BasePluginData>, ShadowrunCustomDataCore.PerTypeData>();
		definitions.put(ItemTemplate.class, new PerTypeData(srDataDir,"items"));


		serializer = new Persister();


		loadEquipment();
	}

	//-------------------------------------------------------------------
	public static WriteablePropertyResourceBundle getI18nResources(Class<? extends BasePluginData> cls) { return definitions.get(cls).i18NResources; }
	public static WriteablePropertyResourceBundle getI18nHelpResources(Class<? extends BasePluginData> cls) { return definitions.get(cls).i18NHelpResources; }

	//-------------------------------------------------------------------
	private static void loadEquipment() {
		PerTypeData typeData = definitions.get(ItemTemplate.class);

		Path readPath = typeData.datafile;
		if (!Files.exists(readPath)) {
			items = new ItemList();
			return;
		}

		logger.debug("Load equipment ("+readPath+")");

		try {
			InputStream in = new FileInputStream(readPath.toFile());
			items = serializer.read(ItemList.class, in);
			logger.info("Successfully loaded "+items.size()+" equipments");

			// Set translation
			for (ItemTemplate tmp : new ArrayList<>(items)) {
				tmp.setResourceBundle(typeData.i18NResources);
				tmp.setHelpResourceBundle(typeData.i18NHelpResources);
				if (logger.isDebugEnabled()) {
					logger.debug("* "+tmp.getName());
					tmp.getPage();
				}
			}

			/*
			 * Sanity checks
			 */
			for (ItemTemplate item : items) {
				if (item.getWeaponData()!=null) {
					WeaponData weapon = item.getWeaponData();
					if (weapon.getDamage()==null) {
						logger.warn("Missing damage in "+item.getId());
					} else if (weapon.getDamage().toString()==null) {
						logger.warn("Null damage string in "+item.getId());
					}
				}
//				if (item instanceof Accessory) {
//					Accessory accessory = (Accessory)item;
//					logger.debug("  Accessory "+item.getName()+" for slots "+accessory.getSlots());
//				}
			}

			Collections.sort(items);

		} catch (Exception e) {
			logger.fatal("Failed loading equipment: "+e,e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void saveEquipment() {
		PerTypeData typeData = definitions.get(ItemTemplate.class);

		// Write properties
		try {
			logger.debug("Write "+typeData.i18nPath);
			typeData.i18NResources.write(new FileWriter(typeData.i18nPath.toFile()), "Custom items");
			logger.debug("Write "+typeData.i18nHelpPath);
			typeData.i18NHelpResources.write(new FileWriter(typeData.i18nHelpPath.toFile()), "Custom items");

			logger.debug("Write "+typeData.datafile);
			serializer.write(items, new FileOutputStream(typeData.datafile.toFile()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getItems() {
		List<ItemTemplate> list = new ArrayList<>(items);
		Collections.sort(items);
		return list;
	}

	//-------------------------------------------------------------------
	public static List<ItemTemplate> getItems(ItemType type) {
		List<ItemTemplate> list = new ArrayList<>();
		for (ItemTemplate item : items) {
			if (item.getType()==type)
				list.add(item);
		}

		return list;
	}

	//-------------------------------------------------------------------
	public static ItemTemplate getItem(String key) {
		for (ItemTemplate tmp : items) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static void addItem(ItemTemplate data) {
		if (getItem(data.getId())!=null)
			throw new IllegalStateException("Item "+data.getId()+" already exists");
		PerTypeData typeData = definitions.get(ItemTemplate.class);
		data.setResourceBundle(typeData.i18NResources);
		data.setHelpResourceBundle(typeData.i18NHelpResources);
		items.add(data);

	}

	//-------------------------------------------------------------------
	public static void removeItem(ItemTemplate data) {
		items.remove(data);
	}

	//--------------------------------------------------------------------
	private static boolean sendEmail(Session session, String fromEmail, String displayName, String subject, BodyPart...parts){
		try {
			Multipart multipart = new MimeMultipart();
			for (BodyPart part : parts)
				multipart.addBodyPart(part);

			MimeMessage msg = new MimeMessage(session);
			msg.addHeader("Content-type", "text/plain; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress(fromEmail, displayName));
			msg.setSubject(subject, "UTF-8");
			msg.setSentDate(new Date());

			msg.setContent(multipart);
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("shadowrun@rpgframework.de", false));
			Transport.send(msg);

			logger.debug("EMail Sent Successfully!!");
			return true;
		} catch (Exception e) {
			logger.error("Failed sending mail",e);
		}
		return false;
	}

	//-------------------------------------------------------------------
	public static boolean upload(List<ItemTemplate> list, String mailAddress, String displayName, String textBody) {
		logger.info("upload "+list.size()+" items");
		ItemList data = new ItemList(list);

		// Build specific property files
		Properties normPro = new Properties();
		Properties helpPro = new Properties();
		for (ItemTemplate item : list) {
			normPro.put("item."+item.getId(), item.getName());
			normPro.put(item.getPageI18NKey(), String.valueOf(item.getPage()));
			if (item.getHelpText()!=null)
				helpPro.put(item.getHelpI18NKey(), item.getHelpText());
		}

		// Write data
		try {
			StringWriter outNorm = new StringWriter();
			normPro.store(outNorm, "Generated by "+displayName+" ("+mailAddress+")");
			StringWriter outHelp = new StringWriter();
			helpPro.store(outHelp, "Generated by "+displayName+" ("+mailAddress+")");
			StringWriter outXML  = new StringWriter();
			serializer.write(data, outXML);

			// Text
			BodyPart partBody = new MimeBodyPart();
			partBody.setText(textBody);
			// XML
			MimeBodyPart partXML = new MimeBodyPart();
			partXML.setDataHandler(new DataHandler(new ByteArrayDataSource(outXML.toString().getBytes("UTF-8"), "text/plain; charset=UTF-8")));
			partXML.setFileName("items.xml");
			// i18n
			MimeBodyPart partNorm = new MimeBodyPart();
			partNorm.setDataHandler(new DataHandler(new ByteArrayDataSource(outNorm.toString().getBytes("UTF-8"), "text/plain; charset=iso-8859-1")));
			partNorm.setFileName("items.properties");
			// i18n-help
			MimeBodyPart partHelp = new MimeBodyPart();
			partHelp.setDataHandler(new DataHandler(new ByteArrayDataSource(outHelp.toString().getBytes("UTF-8"), "text/plain; charset=iso-8859-1")));
			partHelp.setFileName("items-help.properties");

			String smtpHostServer = "smtp.rpgframework.de";

		    Properties props = System.getProperties();

		    props.put("mail.smtp.host", smtpHostServer);

		    Session session = Session.getInstance(props, null);


		    return sendEmail(session, mailAddress, displayName,"Genesis Item Upload", partBody, partXML, partNorm, partHelp);
		} catch (Exception e) {
			logger.error("Failed sending item mail",e);
		}
		return false;
	}

}
