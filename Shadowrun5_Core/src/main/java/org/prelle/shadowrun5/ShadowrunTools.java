/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.Damage;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemAttributeValue;
import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemType;
import org.prelle.shadowrun5.items.LivingPersona;
import org.prelle.shadowrun5.modifications.AddNuyenModification;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.CarriedItemModification;
import org.prelle.shadowrun5.modifications.IncompetentSkillGroupModification;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;
import org.prelle.shadowrun5.modifications.ItemHookModification;
import org.prelle.shadowrun5.modifications.LifestyleCostModification;
import org.prelle.shadowrun5.modifications.ModificationBase;
import org.prelle.shadowrun5.modifications.QualityModification;
import org.prelle.shadowrun5.modifications.RitualModification;
import org.prelle.shadowrun5.modifications.SINModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.modifications.SkillSpecializationModification;
import org.prelle.shadowrun5.modifications.SpellModification;
import org.prelle.shadowrun5.proc.ApplyAttributeModifications;
import org.prelle.shadowrun5.proc.ApplyCarriedItemModifications;
import org.prelle.shadowrun5.proc.ApplySINModifications;
import org.prelle.shadowrun5.proc.ApplySkillModifications;
import org.prelle.shadowrun5.proc.CalculateDerivedAttributes;
import org.prelle.shadowrun5.proc.CharacterProcessor;
import org.prelle.shadowrun5.proc.GetModificationsFromEquipment;
import org.prelle.shadowrun5.proc.GetModificationsFromMagicOrResonance;
import org.prelle.shadowrun5.proc.GetModificationsFromMetaType;
import org.prelle.shadowrun5.proc.GetModificationsFromQualities;
import org.prelle.shadowrun5.proc.ResetModifications;
import org.prelle.shadowrun5.requirements.ItemHookRequirement;
import org.prelle.shadowrun5.requirements.Requirement;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.Datable;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;
import de.rpgframework.products.ProductServiceLoader;

/**
 * @author prelle
 *
 */
public class ShadowrunTools {

	private final static ResourceBundle CORE = ShadowrunCore.getI18nResources();

	private static Logger logger = LogManager.getLogger("shadowrun");

	private final static List<CharacterProcessor> RECALCULATE_STEPS = Arrays.asList(new CharacterProcessor[]{
		new ResetModifications(),
		new GetModificationsFromMetaType(),
		new GetModificationsFromMagicOrResonance(),
		new GetModificationsFromQualities(),
		new GetModificationsFromEquipment(),
		new ApplyAttributeModifications(),
		new ApplySkillModifications(),
		new ApplyCarriedItemModifications(),
		new ApplySINModifications(),
		new CalculateDerivedAttributes(),
	});


	//--------------------------------------------------------------------
	public static void calculateDerived(ShadowrunCharacter model) {
		if (model==null)
			return;
		logger.trace("calculateDerived()");
		AttributeValue val = null;

		/*
		 * Physical initiative
		 */
		int ini =
				model.getAttribute(Attribute.REACTION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_PHYSICAL);
		val.setPoints(ini);

		/*
		 * astral initiative
		 */
		ini =
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_ASTRAL);
		val.setPoints(ini);

		/*
		 * matrix initiative
		 */
		ini =
				model.getAttribute(Attribute.REACTION).getModifiedValue()+
				model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.INITIATIVE_MATRIX);
		val.setPoints(ini);

		/*
		 * physical limit
		 */
		int sum = (
				model.getAttribute(Attribute.STRENGTH).getModifiedValue()*2+
				model.getAttribute(Attribute.BODY).getModifiedValue()+
				model.getAttribute(Attribute.REACTION).getModifiedValue()
				);
		int limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_PHYSICAL);
		val.setPoints(limit);

		/*
		 * mental limit
		 */
		sum = (
				model.getAttribute(Attribute.LOGIC).getModifiedValue()*2+
				model.getAttribute(Attribute.INTUITION).getModifiedValue()+
				model.getAttribute(Attribute.WILLPOWER).getModifiedValue()
				);
		limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_MENTAL);
		val.setPoints(limit);

		/*
		 * social limit
		 */
		sum = (
				model.getAttribute(Attribute.CHARISMA).getModifiedValue()*2+
				model.getAttribute(Attribute.WILLPOWER).getModifiedValue()+
				model.getAttribute(Attribute.ESSENCE).getModifiedValue()
				);
		limit = sum / 3;
		if ( (sum%3)>0)
			limit++;
		val = model.getAttribute(Attribute.LIMIT_SOCIAL);
		val.setPoints(limit);

		/*
		 * astral limit
		 */
		sum = (
		limit = Math.max(model.getAttribute(Attribute.LIMIT_MENTAL).getModifiedValue(), model.getAttribute(Attribute.LIMIT_SOCIAL).getModifiedValue()));
		val = model.getAttribute(Attribute.LIMIT_ASTRAL);
		val.setPoints(limit);

		/*
		 * Composure
		 */
		sum = model.getAttribute(Attribute.CHARISMA).getModifiedValue() + model.getAttribute(Attribute.WILLPOWER).getModifiedValue();
		val = model.getAttribute(Attribute.COMPOSURE);
		val.setPoints(sum);

		/*
		 * Judge intentions
		 */
		sum = model.getAttribute(Attribute.CHARISMA).getModifiedValue() + model.getAttribute(Attribute.INTUITION).getModifiedValue();
		val = model.getAttribute(Attribute.JUDGE_INTENTIONS);
		val.setPoints(sum);

		/*
		 * lifting/carrying
		 */
		sum = model.getAttribute(Attribute.BODY).getModifiedValue() + model.getAttribute(Attribute.STRENGTH).getModifiedValue();
		val = model.getAttribute(Attribute.LIFT_CARRY);
		val.setPoints(sum);

		/*
		 * memory
		 */
		sum = model.getAttribute(Attribute.LOGIC).getModifiedValue() + model.getAttribute(Attribute.WILLPOWER).getModifiedValue();
		val = model.getAttribute(Attribute.MEMORY);
		val.setPoints(sum);

		/*
		 * movement
		 */
		sum = model.getAttribute(Attribute.AGILITY).getModifiedValue() *4;
		val = model.getAttribute(Attribute.MOVEMENT);
		val.setPoints(sum);

		/*
		 * Street credit
		 */
		sum = model.getKarmaInvested() + model.getKarmaFree();
		val = model.getAttribute(Attribute.STREET_CRED);
		val.setPoints( (int)(sum/10) );

		/*
		 * Verify essence
		 */
		calculateEssenceCost(model);
	}

	//--------------------------------------------------------------------
	private static void calculateEssenceCost(ShadowrunCharacter model) {
		logger.trace("calculateEssenceCost()");
		float sum = 0.0f;
		for (CarriedItem item : model.getItems(false)) {
			if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getItem().getType())) {
				float essence = getItemAttribute(model, item, ItemAttribute.ESSENCECOST).getModifiedValue()/1000.0f;
//				float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
				logger.debug("* "+item.getName()+" = "+essence);
				sum += essence;
			}
		}

		float normalLow = 6.0f - sum;
		if (model.getUnusedEssence()==0 || normalLow<model.getUnusedEssence()) {
			logger.info("Unused essence decreased to "+normalLow);
			model.setUnusedEssence(normalLow);
		}
		logger.trace("sum="+sum+"  normalLow="+normalLow+"  unused="+model.getUnusedEssence());

		float min = Math.min(model.getUnusedEssence(), 6.0f-sum);
		if (min!=model.getUnusedEssence()) {
			logger.warn("Fix essence to "+min);
			model.setUnusedEssence(min);
		}
	}

//	//-------------------------------------------------------------------
//	private static void applyStandardItemMdodifications(ShadowrunCharacter model, CarriedItem ref) {
//		logger.info("  apply modifications from item templates (add slots, accessories ...) for "+ref);
//		ref.updateModifications();
//
//		ItemTemplate item = ref.getItem();
//
//		for (Modification mod : item.getModifications()) {
//			if (mod instanceof ItemHookModification) {
//				ItemHookModification aMod = (ItemHookModification)mod;
//				if (ref.getSlot(aMod.getHook())!=null)
//					continue;
//				AvailableSlot hook = (aMod.getCapacity()>1)?(new AvailableSlot(aMod.getHook(), aMod.getCapacity())):(new AvailableSlot(aMod.getHook()));
//				ref.addSlot(hook);
//				logger.debug("    add slot "+aMod.getHook()+" to "+ref);
//			} else if (mod instanceof AccessoryModification) {
//				AccessoryModification aMod = (AccessoryModification)mod;
//				AvailableSlot hook = ref.getSlot(aMod.getHook());
//				if (hook==null) {
//					hook = new AvailableSlot(aMod.getHook());
//					ref.addSlot(hook);
//				}
//				logger.warn("TODO: instantiate accessory in load "+aMod.getItem());
////				CarriedItem accessory = instantiate(aMod.getItem());
////				accessory.setPrice(0);
////				hook.addEmbeddedItem(accessory);
//			} else if (mod instanceof AttributeModification) {
//				mod.setSource(ref);
////				AttributeModification aMod = (AttributeModification)mod;
////				model.getAttribute(aMod.getAttribute()).addModification(aMod);
//			} else {
//				logger.warn("TODO: "+mod);
//			}
//		}
//	}

	//-------------------------------------------------------------------
	public static void applyModification(ShadowrunCharacter model, Modification mod) {
		if (mod.getSource()==null)
			throw new NullPointerException("No source in modification "+mod);

		if (mod instanceof AttributeModification)  {
			AttributeModification newMod = (AttributeModification) mod;
			model.getAttribute(newMod.getAttribute()).addModification(newMod);
		} else if (mod instanceof IncompetentSkillGroupModification) {
			IncompetentSkillGroupModification iMod = (IncompetentSkillGroupModification)mod;
			SkillGroupValue sVal1 = model.getSkillGroupValue(iMod.getSkillGroup());
			if (sVal1!=null)
				sVal1.addModification(iMod);
			else
				logger.warn("Cannot add IncompetentSkillGroupModification for a non-existing skillgroup");
			for (Skill skill : ShadowrunCore.getSkills(iMod.getSkillGroup())) {
				SkillValue sVal = model.getSkillValue(skill);
				if (sVal!=null) {
					sVal.addModification(iMod);
				} else {
					logger.warn("Cannot add IncompetentSkillGroupModification for a non-existing skill of that group");
				}
			}
		} else if (mod instanceof LifestyleCostModification) {
			// Ignore
		} else if (mod instanceof QualityModification) {
			QualityModification qMod = (QualityModification)mod;
			QualityValue toApply = new QualityValue(qMod.getModifiedItem(), qMod.getValue());
			model.addRacialQuality(toApply);
		} else if (mod instanceof CarriedItemModification) {
			CarriedItemModification itemMod = (CarriedItemModification)mod;
			CarriedItem item = new CarriedItem(itemMod.getItem());
			if (itemMod.getItem().hasRating())
				item.setRating(itemMod.getRating());
			item.setCreatedByModification(true);
			model.addItem(item);
		} else if (mod instanceof SkillModification) {
			SkillModification sMod = (SkillModification)mod;
			SkillValue sVal = model.getSkillValue(sMod.getSkill());
			if (sVal!=null)
				sVal.addModification(mod);
			else
				logger.warn("Have modification "+sMod+" from "+sMod.getSource()+" , but character does not have that skill");
		} else {
			logger.info("Don't know how to apply "+mod.getClass());
			System.exit(0);
		}

	}

	//-------------------------------------------------------------------
	public static Object resolveChoiceType(ChoiceType type, String reference) {
		if (reference==null)
			return null;
		switch (type) {
		case ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
		case LIMIT:
			return Attribute.valueOf(reference);
		case SKILL:
		case COMBAT_SKILL:
			return ShadowrunCore.getSkill(reference);
		case SKILLGROUP:
			return ShadowrunCore.getSkillGroup(reference);
		case MATRIX_ACTION:
			return ShadowrunCore.getAction(reference);
		case MENTOR_SPIRIT:
			return ShadowrunCore.getMentorSpirit(reference);
//		case SENSE:
		default:
			throw new IllegalArgumentException("ChoiceType "+type+" not supported yet ("+reference+")");
		}

	}

	//-------------------------------------------------------------------
	public static List<SkillValue> getAllSkillValues(ShadowrunCharacter model, SkillType... types) {
		List<SkillType> filter = Arrays.asList(types);
		if (filter.isEmpty())
			filter = Arrays.asList(SkillType.regularValues());

		List<SkillValue> ret = new ArrayList<>();
		for (Skill skill : ShadowrunCore.getSkills()) {
			if (!filter.contains(skill.getType()))
				continue;
			switch (skill.getType()) {
			case COMBAT:
			case MAGIC:
			case PHYSICAL:
			case RESONANCE:
			case SOCIAL:
			case TECHNICAL:
			case VEHICLE:
				SkillValue val = model.getSkillValue(skill);
				if (val==null) {
					val = new SkillValue(skill, -2);
				}
				ret.add(val);
				break;
			case LANGUAGE:
			case KNOWLEDGE:
				break;
			case ACTION:
			case NOT_SET:
				break;
			}
		}

		for (SkillValue val : model.getSkillValues(true)) {
			SkillType tmpType = val.getModifyable().getType();
			if (!filter.contains(tmpType))
				continue;
			if (Arrays.asList(SkillType.individualValues()).contains(tmpType)) {
				ret.add(val);
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Convert modifications to those that can be applied to a character.
	 */
	public static Modification instantiateModification(Modification mod, Object choice) {
		if (mod instanceof IncompetentSkillGroupModification) {
			IncompetentSkillGroupModification newMod = (IncompetentSkillGroupModification) ((IncompetentSkillGroupModification)mod).clone();
			if (newMod.getSkillGroup()==null) {
				if (choice==null)
					throw new NullPointerException("Choice must be an SkillGroup");
				if (choice instanceof SkillGroup)
					newMod.setSkillGroup((SkillGroup) choice);
				else
					throw new IllegalArgumentException();
			}
			return newMod;
		} else if (mod instanceof AttributeModification)  {
			AttributeModification newMod = (AttributeModification)  ((AttributeModification)mod).clone();
			if (newMod.getAttribute()==null) {
				if (choice==null)
					throw new NullPointerException("Choice must be an Attribute");
				if (choice instanceof Attribute)
					newMod.setAttribute((Attribute)choice);
				else
					throw new IllegalArgumentException();
			}
			return newMod;
		} else if (mod instanceof LifestyleCostModification)  {
			LifestyleCostModification newMod = (LifestyleCostModification)  ((LifestyleCostModification)mod).clone();
			return newMod;
		} else if (mod instanceof SINModification)  {
			SINModification newMod = (SINModification)  ((SINModification)mod).clone();
			return newMod;
		} else if (mod instanceof QualityModification)  {
			QualityModification newMod = (QualityModification)  ((QualityModification)mod).clone();
			return newMod;
		} else if (mod instanceof CarriedItemModification)  {
			CarriedItemModification newMod = (CarriedItemModification)  ((CarriedItemModification)mod).clone();
			return newMod;
		} else {
			logger.warn("No special handling for "+mod.getClass());
			throw new IllegalArgumentException("Don't support "+mod.getClass());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Determine all modifications that must be applied to a character or
	 * a character leveller
	 */
	public static List<Modification> getCharacterModifications(ShadowrunCharacter data) {
		logger.debug(" START: getCharacterModifications");
		List<Modification> ret = new ArrayList<>();
		/*
		 * Modifications depending on metatype
		 */
		logger.debug("  1. Check metatype");
		for (Modification mod : data.getMetatype().getModifications()) {
			Modification newMod = instantiateModification(mod, null);
			newMod.setSource(data.getMetatype());
			ret.add(newMod);
		}

		/*
		 * Modifications depending on adept powers
		 */
		logger.debug("  2. Check adept powers");
		for (AdeptPowerValue power : data.getAdeptPowers()) {
			Object choice = (power.getModifyable().needsChoice()) ? resolveChoiceType(power.getModifyable().getSelectFrom(), power.getChoiceReference()): null;
			for (Modification mod : power.getModifyable().getModifications()) {
				Modification newMod = instantiateModification(mod, choice);
				newMod.setSource(power);
				ret.add(newMod);
			}
		}

		/*
		 * Modifications depending on qualities
		 */
		logger.debug("  3. Check qualities");
		for (QualityValue qual : data.getQualities()) {
			logger.info("Check "+qual);
			Object choice = (qual.getModifyable().needsChoice()) ? resolveChoiceType(qual.getModifyable().getSelect(), qual.getChoiceReference()): null;
			qual.setChoice(choice);
			for (Modification mod : resolveModifications(qual.getModifyable(), choice)) {
				Modification newMod = instantiateModification(mod, choice);
				newMod.setSource(qual);
				ret.add(newMod);
			}
		}

		logger.debug(" STOP : getCharacterModifications");
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Convert modifications to those that can be applied to a character.
	 */
	public static List<Modification> resolveModifications(Quality quality, Object choice) {
		List<Modification> ret = new ArrayList<>();
		if (!quality.needsChoice())
			return ret;

		for (Modification mod : quality.getModifications()) {
			try {
				Modification newMod = instantiateModification(mod, choice);
				ret.add(newMod);
			} catch (Exception e) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Error in character: "+e.toString());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Apply those modifications not saved in the character - e.g. the
	 * resistance boni depending on level
	 */
	public static void recalculateCharacter(ShadowrunCharacter data) {
		logger.info("----------------recalculateCharacter");

//		/*
//		 * Get all modifications that need to be applied to a character
//		 * except those coming from equipment
//		 */
//		logger.debug("1. Apply modifications from character itself");
//		List<Modification> modifications = getCharacterModifications(data);
//		for (Modification mod : modifications) {
//			logger.info("  Apply "+mod+" from "+mod.getSource());
//			applyModification(data, mod);
//		}
//		logger.debug("   Done applying modifications from character itself");
//
//		/*
//		 *
//		 */
//		logger.debug("2. Apply modifications from equipment");
//		for (CarriedItem item : data.getItems()) {
//			item.updateModifications();
//			/*
//			 * Apply those modifications coming from the item
//			 * that need to be applied to the character
//			 */
//			modifications = item.getCharacterModifications();
//			for (Modification mod : modifications) {
//				logger.info("  Apply "+mod+" from "+mod.getSource());
//				applyModification(data, mod);
//			}
//		}
//		logger.debug("   Done applying modifications from equipment");
//
//		logger.debug("3. Calculate derived values");
//		calculateDerived(data);


		List<Modification> unprocessed = new ArrayList<>();
		for (CharacterProcessor step : RECALCULATE_STEPS) {
			logger.info("  run "+step.getClass().getSimpleName());
			unprocessed = step.process(data, unprocessed);
			logger.info("  after "+step.getClass().getSimpleName()+" = "+unprocessed);
		}
		logger.debug("  unprocessed = "+unprocessed);
	}

	//-------------------------------------------------------------------
	public static int getWeaponPool(ShadowrunCharacter model, CarriedItem item) {
		if (item.getItem().getWeaponData()==null) {
			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getType()+" and of type "+item.getItem().getClass());
		}

		Skill skill = item.getItem().getWeaponData().getSkill();
		int pool = model.getSkillPool(skill);
		return pool;
	}

	//-------------------------------------------------------------------
	public static Damage getWeaponDamage(ShadowrunCharacter model, CarriedItem item) {
		if (item.getItem().getWeaponData()==null) {
			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getType()+" and of type "+item.getItem().getClass());
		}

		Damage damage = (Damage)item.getAsValue(ItemAttribute.DAMAGE);
		if (damage.isAddStrength()) {
			AttributeValue val = model.getAttribute(Attribute.STRENGTH);
			Damage damage2 = new Damage();
			damage2.setValue(damage.getValue() + val.getModifiedValue());
			damage2.setType(damage.getType());
			return damage2;
		}
		return damage;
	}

	//-------------------------------------------------------------------
	public static String getModificationString(Modification mod) {
		if (mod instanceof AddNuyenModification) {
			AddNuyenModification aMod = (AddNuyenModification)mod;
			if (aMod.getValue()>0)
				return "+ \u00A5"+aMod.getValue();
			else
				return "- \u00A5"+Math.abs(aMod.getValue());
		} else if (mod instanceof AttributeModification) {
			AttributeModification aMod = (AttributeModification)mod;
			if (aMod.getValue()>0)
				return aMod.getAttribute().getName()+" +"+aMod.getValue();
			else
				return aMod.getAttribute().getName()+" -"+Math.abs(aMod.getValue());

		} else if (mod instanceof SkillModification) {
			SkillModification sMod = (SkillModification)mod;
			if (sMod.getValue()>0)
				return sMod.getSkill().getName()+" +"+sMod.getValue();
			else
				return sMod.getSkill().getName()+" -"+Math.abs(sMod.getValue());

		} else if (mod instanceof SkillGroupModification) {
			SkillGroupModification sMod = (SkillGroupModification)mod;
			if (sMod.getValue()>0)
				return sMod.getSkillGroup().getName()+" +"+sMod.getValue();
			else
				return sMod.getSkillGroup().getName()+" -"+Math.abs(sMod.getValue());

		} else if (mod instanceof QualityModification) {
			QualityModification qMod = (QualityModification)mod;
			if (qMod.isRemove()) {
				return CORE.getString("label.remove")+" "+qMod.getModifiedItem().getName();
			} else
				return qMod.getModifiedItem().getName();
		} else if (mod instanceof SpellModification) {
			SpellModification qMod = (SpellModification)mod;
			return CORE.getString("label.spell")+" "+qMod.getSpell().getName();
		} else if (mod instanceof RitualModification) {
			RitualModification qMod = (RitualModification)mod;
			return CORE.getString("label.ritual")+" "+qMod.getRitual().getName();
		} else if (mod instanceof ItemHookModification) {
			ItemHookModification qMod = (ItemHookModification)mod;
			return String.format(CORE.getString("label.addhook"), qMod.getHook().getName(), qMod.getCapacity());
		} else if (mod instanceof ItemAttributeModification) {
			ItemAttributeModification aMod = (ItemAttributeModification)mod;
			if (aMod.getValue()>0)
				return aMod.getAttribute().getName()+" +"+aMod.getValue();
			else
				return aMod.getAttribute().getName()+" -"+Math.abs(aMod.getValue());

		} else if (mod instanceof SkillSpecializationModification) {
			SkillSpecializationModification sMod = (SkillSpecializationModification)mod;
			return String.format(CORE.getString("mod.skillspecialization"), sMod.getSkill().getName(), sMod.getSkillSpecialization().getName());
		} else if (mod instanceof LifestyleCostModification) {
			LifestyleCostModification sMod = (LifestyleCostModification)mod;
			try {
				return String.format(String.format(CORE.getString("mod.lifestylecost"), sMod.getPercent()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return sMod.toString();
			}
		} else if (mod instanceof CarriedItemModification) {
			CarriedItemModification cMod = (CarriedItemModification)mod;
			return Resource.format(CORE,"mod.carrieditem", cMod.getItem().getName());
		} else {
			logger.error("No getModificationString for "+mod.getClass());
			return "ShadowrunTools.getModificationString";
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @param aggregate Aggregate history elements with same adventure
	 */
	public static List<HistoryElement> convertToHistoryElementList(ShadowrunCharacter charac, boolean aggregate) {
		// Initial reward
		logger.debug("Sort "+charac.getRewards().size()+" rewards  and "+charac.getHistory().size()+" mods");

		/*
		 * Build a merged list of rewards and modifications and sort it by time
		 */
		List<Datable> rewardsAndMods = new ArrayList<Datable>();
		rewardsAndMods.addAll(charac.getRewards());
		for (Modification mod : charac.getHistory()) {
			rewardsAndMods.add(mod.clone());
		}
//		rewardsAndMods.addAll(charac.getHistory());
		sort(rewardsAndMods);

		/*
		 * Does the creation reward need to be added?
		 */
		if (rewardsAndMods.isEmpty() || !(rewardsAndMods.get(0) instanceof Reward)) {
			RewardImpl creation = new RewardImpl(15, CORE.getString("label.reward.creation"));
			rewardsAndMods.add(creation);
			sort(rewardsAndMods);
		}

		/*
		 * Now build a list of HistoryElements. Start a new H
		 */
		List<HistoryElement> ret = new ArrayList<HistoryElement>();
		HistoryElementImpl current = null;
		ProductService sessServ = ProductServiceLoader.getInstance();

		for (Datable item : rewardsAndMods) {
			if (item instanceof RewardImpl) {
				RewardImpl reward = (RewardImpl)item;
				Adventure adv = null;
				if (reward.getId()!=null) {
					adv = sessServ.getAdventure(RoleplayingSystem.SHADOWRUN, reward.getId());
					if (adv==null) {
						logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
					}
				}
				// If is same adventure as current, keep same history element
				if (!aggregate || !(adv!=null && current!=null && adv.getId().equals(current.getAdventureID())) ) {
					current = new HistoryElementImpl();
					current.setName(reward.getTitle());
					if (adv!=null) {
						current.setName(adv.getTitle());
						current.setAdventure(adv);
					}
					ret.add(current);
				}
				current.addGained(reward);
			} else if (item instanceof ModificationBase) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
				} else {
					Modification lastMod = (current.getSpent().isEmpty())?null:current.getSpent().get(current.getSpent().size()-1);
					if (lastMod!=null && lastMod.getClass()==item.getClass()) {
						if (item instanceof SkillModification) {
//							logger.debug("Combine "+lastMod+" with "+item);
							// Aggregate same skill
							SkillModification lastSMod = (SkillModification)lastMod;
							SkillModification newtMod = (SkillModification)item;
							if (lastSMod.getSkill()==newtMod.getSkill()) {
								// Same skill
								lastSMod.setValue(newtMod.getValue());
								lastSMod.setExpCost(lastSMod.getExpCost() + newtMod.getExpCost());
							} else {
								// Different skill
								current.addSpent((ModificationBase<?>) item);
							}
						} else {
							current.addSpent((ModificationBase<?>) item);
						}
					} else {
						current.addSpent((ModificationBase<?>) item);
					}
				}
			} else if (item instanceof AddNuyenModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					current.addSpent((Modification) item);
					ret.add(current);
				} else {
				}
			} else if (item instanceof SpellModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					current.addSpent((Modification) item);
					ret.add(current);
				} else {
					current.addSpent((Modification) item);
				}
			} else {
				logger.error("Don't know how to "+item);
			}
		}


		logger.debug("  return "+ret.size()+" elements");
		return ret;
	}

	//-------------------------------------------------------------------
	private static void sort(List<Datable> rewardsAndMods) {
		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
			public int compare(Datable o1, Datable o2) {
				Long time1 = 0L;
				Long time2 = 0L;
				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();

				int cmp = time1.compareTo(time2);
				if (cmp==0) {
					if (o1 instanceof RewardImpl && o2 instanceof ModificationBase) return -1;
					if (o1 instanceof ModificationBase && o2 instanceof RewardImpl) return  1;
				}
				return cmp;
			}
		});
	}

	//--------------------------------------------------------------------
	public static int calculateInvestedNuyen(ShadowrunCharacter model) {
		if (model==null)
			return -1;

		int invest = 0;

		/*
		 * Gear
		 */
		for (CarriedItem item : model.getItems(false)) {
			invest += (Integer)item.getAsObject(ItemAttribute.PRICE);
		}

		/*
		 * Lifestyle
		 */
		for (LifestyleValue life : model.getLifestyle()) {
			invest += life.getCost();
		}

		/*
		 * SINs
		 */
		for (SIN sin : model.getSINs()) {
			if (sin.getQuality()==SIN.Quality.REAL_SIN)
				continue;
			invest += sin.getQualityValue() * 2500;
		}

		/*
		 * Licences
		 */
		for (LicenseValue sin : model.getLicenses()) {
			invest += sin.getRating().getValue() * 200;
		}

		return invest;
	}

	//-------------------------------------------------------------------
	public static boolean isRequirementMet(Requirement req, ItemTemplate item) {

		if (req instanceof ItemHookRequirement) {
			ItemHookRequirement hookReq = (ItemHookRequirement)req;
			if (hookReq.getSlot()!=null) {
				for (ItemHookModification hookMod : item.getSlots()) {
					if (hookMod.getHook()==hookReq.getSlot())
						return true;
				}
				return false;
			}
		}
		logger.warn("TODO: check requirement "+req+" on item "+item);

		return true;
	}

	//-------------------------------------------------------------------
	public static ItemAttributeValue getItemAttribute(ShadowrunCharacter model, CarriedItem item, ItemAttribute attr) {
		ItemAttributeValue val = item.getAsValue(attr);

		// Apply modifications from accessory slots
		for (CarriedItem accessory : item.getAccessories()) {
			for (Modification mod : accessory.getModifications()) {
				if ((mod instanceof ItemAttributeModification) && ((ItemAttributeModification)mod).getAttribute()==attr) {
					val.addModification(mod);
				}
			}
		}

		switch (attr) {
		case ESSENCECOST:
			if (model.hasQuality("sensitive_system")) {
				val.setPoints(val.getPoints()*2);
			}
			break;
		default:
		}

		// TODO: check requirements
		return val;
	}

	//-------------------------------------------------------------------
	public static String getItemAttributeString(ShadowrunCharacter model, CarriedItem item, ItemAttribute attr) {
		switch (attr) {
		case MODE:
			return String.valueOf(item.getAsObject(attr));
		case SKILL:
			return ((Skill)item.getAsObject(attr)).getName();
		case PRICE:
			return String.valueOf(item.getAsObject(attr));
		case AMMUNITION:
			return String.valueOf(item.getAsObject(attr));
		default:
			ItemAttributeValue val = getItemAttribute(model, item, attr);
			if (val.getModifier()==0)
				return String.valueOf(val.getPoints());
			else
				return val.getPoints()+" ("+val.getModifiedValue()+")";
		}
	}

	//--------------------------------------------------------------------
	public static Map<ItemAttribute, String> getItemAttributeStrings(CarriedItem item) {
		Map<ItemAttribute, String> ret = new HashMap<ItemAttribute, String>();

		ret.put(ItemAttribute.AVAILABILITY  , String.valueOf(item.getAvailability()));
		if (item.isType(ItemType.WEAPON)) {
			ret.put(ItemAttribute.ACCURACY  , item.getAsValue(ItemAttribute.ACCURACY).toString());
			ret.put(ItemAttribute.AMMUNITION, item.getAsValue(ItemAttribute.AMMUNITION).toString());
			ret.put(ItemAttribute.DAMAGE    , item.getAsValue(ItemAttribute.DAMAGE).toString());
			ret.put(ItemAttribute.RECOIL_COMPENSATION, item.getAsValue(ItemAttribute.RECOIL_COMPENSATION).toString());
			ret.put(ItemAttribute.ARMOR_PENETRATION, item.getAsValue(ItemAttribute.ARMOR_PENETRATION).toString());
			ret.put(ItemAttribute.REACH     , item.getAsValue(ItemAttribute.REACH).toString());
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public static String getDrainString(Spell spell) {
		if (spell.getDrain()<0)
			return CORE.getString("label.drain.short")+" "+spell.getDrain();
		if (spell.getDrain()>0)
			return CORE.getString("label.drain.short")+" +"+spell.getDrain();
		return CORE.getString("label.drain.short");
	}

	//--------------------------------------------------------------------
	public static int getLifestyleCost(ShadowrunCharacter model, LifestyleValue val) {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : val.getOptions())
			options.add(opt.getOption());

		int costPerMonth =  getLifestyleCost(model, val.getLifestyle(), options, val.getModifications());
		return costPerMonth * val.getPaidMonths();
	}

	//--------------------------------------------------------------------
	public static int getLifestyleCost(ShadowrunCharacter model, Lifestyle base, List<LifestyleOption> options, List<Modification> staticMods) {
		if (model==null)
			throw new NullPointerException("model is null");
		if (base==null)
			throw new NullPointerException("Lifestyle is null");
		if (options==null)
			throw new NullPointerException("options is null");
		float sum = base.getCost();
		for (LifestyleOption opt : options) {
			if (opt==null)
				continue;
			for (Modification mod : opt.getModifications()) {
				if (mod instanceof LifestyleCostModification) {
					LifestyleCostModification lcMod = (LifestyleCostModification)mod;
					if (lcMod.getPercent()!=0) {
						sum += base.getCost() * lcMod.getPercent() / 100.0;
					} else {
						sum += lcMod.getFixed();
					}
				}
			}
		}

		for (Modification mod : staticMods) {
			if (mod instanceof LifestyleCostModification) {
				LifestyleCostModification lcMod = (LifestyleCostModification)mod;
				if (lcMod.getPercent()!=0) {
					sum += base.getCost() * lcMod.getPercent() / 100.0;
				} else {
					sum += lcMod.getFixed();
				}
			}
		}
//		// Metatype
//		if (model.getMetatype()!=null) {
//			if (model.getMetatype().getId().equals("dwarf") || (model.getMetatype().getVariantOf()!=null && model.getMetatype().getVariantOf().getId().equals("dwarf"))) {
//				sum *= 1.2;
//			} else if (model.getMetatype().getId().equals("troll") || (model.getMetatype().getVariantOf()!=null && model.getMetatype().getVariantOf().getId().equals("troll"))) {
//				sum *= 2;
//			}
//		}
		return Math.round(sum);
	}


	//-------------------------------------------------------------------
	public static void reward(ShadowrunCharacter charac, RewardImpl reward) {
		logger.info("Add reward "+reward+" to "+charac);
		charac.addReward(reward);

		charac.setKarmaFree(charac.getKarmaFree() + reward.getExperiencePoints());
		for (Modification mod : reward.getModifications()) {
			if (mod instanceof AddNuyenModification) {
				AddNuyenModification nMod = (AddNuyenModification)mod;
				charac.setNuyen(charac.getNuyen() + nMod.getValue());
				// Add to history
				charac.addToHistory(mod);

//			if (mod instanceof ResourceModification) {
//				Resource res = ((ResourceModification)mod).getResource();
//				int      val = ((ResourceModification)mod).getValue();
//				String title = ((ResourceModification)mod).getResourceName();
//
//				if (res.isBaseResource()) {
//					for (ResourceReference ref : charac.getResources()) {
//						if (ref.getResource()==res) {
//							ref.setValue(ref.getValue() + val);
//							logger.info("Reward existing resource to "+ref);
//							ref.setDescription(title);
//							break;
//						}
//					}
//				} else {
//					ResourceReference ref = new ResourceReference(res, val);
//					ref.setDescription(title);
//					logger.info("Reward new resource "+ref);
//					charac.addResource(ref);
//				}
//				// Add to history
//				charac.addToHistory(mod);
			} else {
				logger.error("Unsupported modification: "+mod.getClass());
			}
		}
	}

	//-------------------------------------------------------------------
	public static void equip(ShadowrunCharacter model, CarriedItem item) {
		logger.info("Equip "+item.getName());

		List<Modification> mods = item.getCharacterModifications();
		for (Modification tmp : mods) {
			tmp.setSource(item);
			if (tmp instanceof AttributeModification) {
				AttributeModification mod = (AttributeModification)tmp;
				model.getAttribute(mod.getAttribute()).addModification(mod);
				logger.debug(" add mod "+mod);
			} else if (tmp instanceof CarriedItemModification) {
				CarriedItemModification mod = (CarriedItemModification)tmp;
				if (mod.isRemove()) {
					try {
						CarriedItem toRemove = model.getItem(mod.getItem().getId());
						if (toRemove!=null) {
							logger.info("  remove "+toRemove);
							model.removeAutoItem(toRemove);
						}
					} catch (NoSuchElementException e) {
						// Character does not have this item
					}
				} else {
					logger.info("  add "+mod.getItem());
				}
			} else if (tmp instanceof SkillGroupModification) {
				SkillGroupModification mod = (SkillGroupModification)tmp;
				SkillGroupValue sgVal = model.getSkillGroupValue(mod.getSkillGroup());
				if (sgVal!=null) {
					sgVal.addModification(mod);
					logger.debug(" add mod "+mod+" to skill group");
				} else {
					for (Skill skill : ShadowrunCore.getSkills(mod.getSkillGroup())) {
						SkillValue sVal = model.getSkillValue(skill);
						if (sVal!=null) {
							logger.debug(" add mod "+mod+" to skill");
							sVal.addModification(mod);
						}
					}
				}
			} else
				logger.warn("Don't know how to deal with "+tmp.getClass());
		}
	}

	//-------------------------------------------------------------------
	public static void unequip(ShadowrunCharacter model, CarriedItem item) {
		logger.info("Unequip "+item.getName());

		List<Modification> mods = item.getCharacterModifications();
		for (Modification tmp : mods) {
			if (tmp instanceof AttributeModification) {
				AttributeModification mod = (AttributeModification)tmp;
				model.getAttribute(mod.getAttribute()).removeModification(mod);
			} else
				logger.warn("Don't know how to deal with "+tmp.getClass());
		}
	}

	//-------------------------------------------------------------------
	public static void loadEquipmentModificatdions(ShadowrunCharacter model) {
		logger.info("----------------loadEquipmentModifications");
		logger.debug("Update modifications from equipment");
		for (CarriedItem item : model.getItems(true))
			equip(model, item);
	}

	//-------------------------------------------------------------------
	public static Skill getSkillForVehicle(ItemTemplate item) {
		switch (item.getType()) {
		case VEHICLES:
			switch (item.getSubtype()) {
			case BIKES:
			case CARS:
			case TRUCKS:
				return ShadowrunCore.getSkill("pilot_ground_craft");
			case BOATS:
			case SUBMARINES:
				return ShadowrunCore.getSkill("pilot_watercraft");
			case FIXED_WING:
			case ROTORCRAFT:
			case VTOL:
				return ShadowrunCore.getSkill("pilot_aircraft");
			default:
			}
			break;
		case DRONES:
			logger.warn("TODO: detect skill for drones");
//			switch (item.getSubtype()) {
//			case BIKES:
//			case CARS:
//			case TRUCKS:
//				return ShadowrunCore.getSkill("pilot_ground_craft");
//			case BOATS:
//			case SUBMARINES:
//				return ShadowrunCore.getSkill("pilot_watercraft");
//			case FIXED_WING:
//			case ROTORCRAFT:
//			case VTOL:
//				return ShadowrunCore.getSkill("pilot_aircraft");
//			default:
//			}
			break;
		default:
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<CarriedItem> getMatrixItems(ShadowrunCharacter model) {
		List<CarriedItem> ret = model.getItems(true, ItemType.ELECTRONICS).stream()
				.filter(item -> item.isSubType(ItemSubType.COMMLINK) || item.isSubType(ItemSubType.CYBERDECK) || item.isSubType(ItemSubType.RIGGER_CONSOLE))
				.collect(Collectors.toList());

		if (model!=null && model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesResonance()) {
				LivingPersona persona = new LivingPersona(model);
				ret.add(0, persona);
			}
		}

		// Cyberdecks in cyberware
		for (CarriedItem item3 : model.getItems(true, ItemType.CYBERWARE)) {
			List<CarriedItem> accs = item3.getAccessories().stream()
					.filter(item -> item.isSubType(ItemSubType.COMMLINK) || item.isSubType(ItemSubType.CYBERDECK) || item.isSubType(ItemSubType.RIGGER_CONSOLE))
					.collect(Collectors.toList());
			logger.debug("Embedded matrix items in "+item3.getItem().getId()+" = "+accs);
			ret.addAll(accs);
		}

		Collections.sort(ret, new Comparator<CarriedItem>() {
			public int compare(CarriedItem o1, CarriedItem o2) {
				return o1.getSubType().compareTo(o2.getSubType());
			}
		});
		return ret;
	}

	//-------------------------------------------------------------------
	public static int[] getMonitorArray(ShadowrunCharacter model, Attribute attr) {
		int add = model.getAttribute(attr).getModifiedValue();
		add = Math.round( (float)add / 2.0f);
		if (attr==Attribute.BODY && model.getAttribute(Attribute.PHYSICAL_MONITOR)!=null)
			add+=model.getAttribute(Attribute.PHYSICAL_MONITOR).getModifiedValue();
		if (attr==Attribute.WILLPOWER && model.getAttribute(Attribute.STUN_MONITOR)!=null)
			add+=model.getAttribute(Attribute.STUN_MONITOR).getModifiedValue();
		int[] ret = new int[8 + add];

		int start = 0;
		int every = 3;
		if (model.hasQuality("high_pain_tolerance")) {
			start = model.getQuality("high_pain_tolerance").getModifiedValue();
		}
		if (model.hasAdeptPower("pain_resistance")) {
			start = model.getAdeptPower("pain_resistance").getLevel();
		}
		if (model.hasQuality("low_pain_tolerance")) {
			every = 2;
		}

		for (int i=start; i<ret.length; i++) {
			ret[i] = - ((i+1-start)/every);
		}
		logger.warn("TODO: array for "+attr+": "+Arrays.toString(ret));

		return ret;
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("incomplete-switch")
	public static String getInitiativeString(ShadowrunCharacter model, Attribute iniAttribute) {
		int base = model.getAttribute(iniAttribute).getModifiedValue();
		switch (iniAttribute) {
		case INITIATIVE_PHYSICAL:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifiedValue());
		case INITIATIVE_MATRIX:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getModifiedValue());
		case INITIATIVE_ASTRAL:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getModifiedValue());
		}
		return ""+base;
	}

}
