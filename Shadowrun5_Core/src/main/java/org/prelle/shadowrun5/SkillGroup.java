/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.shadowrun5.Skill.SkillType;

/**
 * @author prelle
 *
 */
public class SkillGroup extends BasePluginData implements Comparable<SkillGroup> {
	
	@org.prelle.simplepersist.Attribute(required=true)
	private String    id;
	@org.prelle.simplepersist.Attribute(required=true)
	private SkillType type;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillGroup() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillGroup o) {
		return Collator.getInstance().compare(getName(), o.getName());

	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "skillgroup."+id.toLowerCase()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "skillgroup."+id.toLowerCase()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		return i18n.getString("skillgroup."+id.toLowerCase());
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (i18n!=null)
			return getName();
		return super.toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillType getType() {
		return type;
	}
	
}
