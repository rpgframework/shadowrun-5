/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="skillgroups")
@ElementList(entry="skillgroup",type=SkillGroup.class,inline=true)
public class SkillGroupList extends ArrayList<SkillGroup> {

	private static final long serialVersionUID = 550291427112058863L;

	//-------------------------------------------------------------------
	/**
	 */
	public SkillGroupList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public SkillGroupList(Collection<? extends SkillGroup> c) {
		super(c);
	}

}
