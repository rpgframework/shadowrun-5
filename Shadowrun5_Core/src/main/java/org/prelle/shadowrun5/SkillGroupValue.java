/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.persist.SkillGroupConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "skillgrpval")
public class SkillGroupValue extends ModifyableImpl implements ModifyableValue<SkillGroup>, Comparable<SkillGroupValue> {


	@Attribute(name="id")
	@AttribConvert(SkillGroupConverter.class)
	private SkillGroup grp;
	@Attribute(name="val")
	private int value;
	@Attribute
	private int start;

	//-------------------------------------------------------------------
	public SkillGroupValue() {
	}

	//-------------------------------------------------------------------
	public SkillGroupValue(SkillGroup skill, int val) {
		this.grp = skill;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s = %d",
				String.valueOf(grp),
				value
				);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public SkillGroup getModifyable() {
		return grp;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * Returns the distributed points plus an equipment modifier
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillGroupValue other) {
		return grp.compareTo(other.getModifyable());
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @return the masterships
//	 */
//	public List<MastershipReference> getMasterships() {
//		return masterships;
//	}
//
//	//-------------------------------------------------------------------
//	public boolean hasMastership(Mastership master) {
//		for (MastershipReference ref : masterships)
//			if (ref.getMastership()==master)
//				return true;
//		return false;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param masterships the masterships to set
//	 */
//	public void addMastership(MastershipReference mastership) {
//		if (!masterships.contains(mastership))
//			masterships.add(mastership);
//	}
//
//	//-------------------------------------------------------------------
//	public void removeMastership(Mastership mastership) {
//		for (MastershipReference ref : masterships)
//			if (ref.getMastership()==mastership) {
//				masterships.remove(ref);
//				return;
//			}
//	}
//
//	//-------------------------------------------------------------------
//	public void removeSpecialization(SkillSpecialization special) {
//		for (MastershipReference ref : masterships)
//			if (ref.getSpecialization()!=null && ref.getSpecialization().getSpecial()==special) {
//				masterships.remove(ref);
//				return;
//			}
//	}
//
//	//-------------------------------------------------------------------
//	public int getSpecializationLevel(SkillSpecialization special) {
//		if (special==null)
//			throw new NullPointerException("SkillSpecialization is null");
//		for (MastershipReference tmp : masterships) {
//			if (tmp.getSpecialization()==null)
//				continue;
//			SkillSpecializationValue tmpSpec = tmp.getSpecialization();
//			if (tmpSpec.getSpecial()==null)
//				throw new NullPointerException("Missing specialization in "+tmpSpec);
////			switch (special.getType()) {
////			case NORMAL:
////			}
//			if (tmpSpec.getSpecial().getId().equals(special.getId()))
//				return tmpSpec.getLevel();
//		}
//		
//		return 0;
//	}
//
//	//-------------------------------------------------------------------
//	public MastershipReference setSpecializationLevel(SkillSpecialization special, int level) {
//		// Search for old data
//		for (MastershipReference tmp : masterships) {
//			if (tmp.getSpecialization()==null)
//				continue;
//			SkillSpecializationValue tmpSpec = tmp.getSpecialization();
//			if (tmpSpec.getSpecial().getId().equals(special.getId())) {
//				tmpSpec.setLevel(level);
//				return tmp;
//			}
//				
//		}
//
//		MastershipReference ref = new MastershipReference(special, level); 
//		masterships.add(ref);
//		return ref;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof SkillGroupModification) {
				SkillGroupModification sMod = (SkillGroupModification)mod;
				if (sMod.getSkillGroup()==grp)
					count += sMod.getValue();
			}
		}
		return count;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

}
