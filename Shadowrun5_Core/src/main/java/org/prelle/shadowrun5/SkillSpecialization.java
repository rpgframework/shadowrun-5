/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class SkillSpecialization extends BasePluginData implements Comparable<SkillSpecialization> {

	private static Logger logger = LogManager.getLogger("shadowrun");
	
	public enum SkillSpecializationType {
		NORMAL,
		WEAPON,
		;
	}

	@Attribute
	private String id;
	@Attribute(required=false)
	private SkillSpecializationType type = SkillSpecializationType.NORMAL;
	private transient Skill skill;

	//-------------------------------------------------------------------
	public SkillSpecialization() {
		
	}

	//-------------------------------------------------------------------
	public SkillSpecialization(Skill skill, SkillSpecializationType type, String id) {
		this.skill = skill;
		this.type  = type;
		this.id    = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		if (skill!=null)
			return "skill."+skill.getId()+"."+id+".page";
		return "missing.skill.for."+id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		if (skill!=null)
			return "skill."+skill.getId()+"."+id+".desc";
		return "missing.skill.for."+id;
	}

	//-------------------------------------------------------------------
	public String getName() {
		String prefix = "missing.skill.for."+id.toLowerCase();
		if (skill!=null)
			prefix = "skill."+skill.getId()+"."+id.toLowerCase(); 
		
		if (type==SkillSpecializationType.WEAPON) {
			if (i18n.containsKey(prefix))
				return i18n.getString(prefix);
			return id; // ShadowrunCore.getItem(id).getName();
		}
		if (id!=null) {
			if (i18n.containsKey(prefix))
				return i18n.getString(prefix);
			logger.trace("Missing translation "+prefix+" in "+i18n.getBaseBundleName());
			System.out.println(prefix+"=");
			return id;
//			return ShadowrunCore.getI18nResources().getString("skillspecial."+id);
		}
		return "any "+type;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof SkillSpecialization) {
			SkillSpecialization other = (SkillSpecialization)o;
			if (type!=other.getType()) return false;
			return id.equals(other.getId());
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return type+": "+skill+"("+id+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillSpecializationType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(SkillSpecializationType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillSpecialization other) {
		return Collator.getInstance().compare(this.getName(), other.getName());
	}

}
