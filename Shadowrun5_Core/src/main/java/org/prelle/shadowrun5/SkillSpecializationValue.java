/**
 *
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.SkillSpecializationConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;


/**
 * @author prelle
 *
 */
public class SkillSpecializationValue {

	@Attribute
	@AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization special;

	//-------------------------------------------------------------------
	/*
	 * Constructor for serialization
	 */
	public SkillSpecializationValue() {
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue(SkillSpecialization special) {
		this.special = special;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof SkillSpecializationValue) {
			SkillSpecializationValue other = (SkillSpecializationValue)o;
			return special.equals(other.getSpecial());
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (special==null)
			return "<no_specialization>";
		return special.getName();
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (special==null)
			return "Unknown";
		return special.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the special
	 */
	public SkillSpecialization getSpecial() {
		return special;
	}

	//-------------------------------------------------------------------
	/**
	 * @param special the special to set
	 */
	public void setSpecial(SkillSpecialization special) {
		this.special = special;
	}

}
