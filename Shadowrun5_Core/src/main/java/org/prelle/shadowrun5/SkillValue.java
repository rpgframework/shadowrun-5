/**
 *
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "skillval")
public class SkillValue extends ModifyableImpl implements ModifyableValue<Skill>, Comparable<SkillValue>, NumericalValue<Skill>, SelectedValue<Skill> {


	@Attribute(name="skill")
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute(name="custname")
	private String customName;
	/**
	 * The current value on the skill without the attributes
	 */
	@Attribute(name="val")
	private int value;
	@Attribute
	private int start;
	@ElementList(entry="skillspec",type=SkillSpecializationValue.class,inline=true)
	private List<SkillSpecializationValue> specializations;

	//-------------------------------------------------------------------
	public SkillValue() {
		specializations = new ArrayList<SkillSpecializationValue>();
	}

	//-------------------------------------------------------------------
	public SkillValue(Skill skill, int val) {
		this();
		this.skill = skill;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s = %d (%d+%d)",
				getName(),
				getModifiedValue(),
				value,
				getModifier()
				);
		//		return skill+"="+value+" ("+masterships+")";
	}

//	//-------------------------------------------------------------------
//	public boolean equals(Object o) {
//		if (o instanceof SkillValue) {
//			SkillValue other = (SkillValue)o;
//			if (skill!=other.getSkill()) return false;
//			if (value!=other.getValue()) return false;
//			return masterships.equals(other.getMasterships());
//		}
//		return false;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Skill getModifyable() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillValue other) {
		return skill.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public List<SkillSpecializationValue> getSkillSpecializations() {
		return specializations;
	}

	//-------------------------------------------------------------------
	public boolean hasSpecialization(SkillSpecialization master) {
		for (SkillSpecializationValue ref : specializations)
			if (ref.getSpecial()==master)
				return true;
		return false;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue getSpecialization(SkillSpecialization master) {
		for (SkillSpecializationValue ref : specializations)
			if (ref.getSpecial()==master)
				return ref;
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param specializations the masterships to set
	 */
	public void addSpecialization(SkillSpecializationValue mastership) {
		if (!specializations.contains(mastership))
			specializations.add(mastership);
	}

	//-------------------------------------------------------------------
	public void removeSpecialization(SkillSpecializationValue mastership) {
		for (SkillSpecializationValue ref : specializations)
			if (ref==mastership) {
				specializations.remove(ref);
				return;
			}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==skill && !sMod.isConditional())
					count += sMod.getValue();
			} else if (mod instanceof SkillGroupModification) {
				SkillGroupModification sMod = (SkillGroupModification)mod;
				if (ShadowrunCore.getSkills(sMod.getSkillGroup()).contains(skill) && !sMod.isConditional())
					count += sMod.getValue();
			}
		}
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @param customName the customName to set
	 */
	public void setName(String customName) {
		this.customName = customName;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (customName!=null)
			return customName;
		if (skill==null)
			return "Unknown";
		return skill.getName();
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

}
