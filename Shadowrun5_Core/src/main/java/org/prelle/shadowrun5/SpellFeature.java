/**
 * 
 */
package org.prelle.shadowrun5;

import java.text.Collator;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class SpellFeature extends BasePluginData implements Comparable<SpellFeature> {
	
	@Attribute(required=true)
	private String id;

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("spellfeature."+id.toLowerCase());
		} catch (MissingResourceException e) {
			LogManager.getLogger("shadowrun").error("Missing key "+e.getKey()+" in "+i18n.getBaseBundleName());
			return "spellfeature."+id.toLowerCase();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "spellfeature."+id.toLowerCase()+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "spellfeature."+id.toLowerCase()+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SpellFeature o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

}
