/**
 * 
 */
package org.prelle.shadowrun5;

import org.prelle.shadowrun5.persist.SpellConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class SpellValue extends ModifyableImpl implements Modifyable, Comparable<SpellValue> {

	@Attribute(name="spell")
	@AttribConvert(SpellConverter.class)
	private Spell spell;
	@Attribute
	private boolean alchemistic;

	//-------------------------------------------------------------------
	public SpellValue() {
	}

	//-------------------------------------------------------------------
	public SpellValue(Spell spell) {
		this.spell = spell;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o!=null && o instanceof SpellValue) {
			SpellValue other = (SpellValue)o;
			if (spell!=other.getModifyable()) return false;
			return alchemistic==other.isAlchemistic();
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SpellValue other) {
		return spell.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public Spell getModifyable() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the alchemistic
	 */
	public boolean isAlchemistic() {
		return alchemistic;
	}

	//-------------------------------------------------------------------
	/**
	 * @param alchemistic the alchemistic to set
	 */
	public void setAlchemistic(boolean alchemistic) {
		this.alchemistic = alchemistic;
	}

}
