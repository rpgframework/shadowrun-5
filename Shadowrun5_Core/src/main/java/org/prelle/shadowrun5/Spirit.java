/**
 * 
 */
package org.prelle.shadowrun5;

/**
 * @author prelle
 *
 */
public class Spirit extends Summonable {
	
	@org.prelle.simplepersist.Attribute
	private String id;

	//-------------------------------------------------------------------
	public Spirit() {
		super(Type.SPIRIT);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "Spirit(id=null)";
    	if (i18n==null)
    		return "spirit."+id.toLowerCase()+".title";
    	String key = "spirit."+id.toLowerCase()+".title";
    	if (i18n.containsKey(key))
        return i18n.getString(key);
    		logger.error("Missing key '"+key+"' in "+i18n.getBaseBundleName());
    	return key;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "spirit."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "spirit."+id+".desc";
	}

}
