/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="spirits")
@ElementList(entry="spirit",type=Spirit.class)
public class SpiritList extends ArrayList<Spirit> {

}
