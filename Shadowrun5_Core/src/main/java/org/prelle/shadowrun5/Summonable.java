/**
 *
 */
package org.prelle.shadowrun5;

import java.text.Collator;


/**
 * @author prelle
 *
 */
public abstract class Summonable extends BasePluginData implements Comparable<Summonable> {

	public enum Type {
		SPIRIT,
		SPRITE,
		;
		public String getName() {return ShadowrunCore.getI18nResources().getString("serviceable.type."+this.name().toLowerCase());}
	}


	@org.prelle.simplepersist.Attribute
	private Type type;

	//-------------------------------------------------------------------
	protected Summonable(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Summonable other) {
		int cmp = type.compareTo(other.getType());
		if (cmp!=0)
			return cmp;
		return Collator.getInstance().compare(getName(), other.getName());
	}

}
