/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="traditions")
@ElementList(entry="tradition",type=Tradition.class)
public class TraditionList extends ArrayList<Tradition> {

}
