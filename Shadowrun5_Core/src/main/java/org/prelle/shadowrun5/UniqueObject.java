/**
 * 
 */
package org.prelle.shadowrun5;

import java.util.UUID;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public abstract class UniqueObject extends ModifyableImpl {

	@Attribute(name="uniqueid")
	private UUID uniqueId;
	
	//-------------------------------------------------------------------
	public UniqueObject() {
		uniqueId = UUID.randomUUID();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}
	
}
