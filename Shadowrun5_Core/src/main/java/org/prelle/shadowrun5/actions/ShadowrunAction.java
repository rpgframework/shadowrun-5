/**
 * 
 */
package org.prelle.shadowrun5.actions;

import java.util.MissingResourceException;

import org.prelle.shadowrun5.BasePluginData;
import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class ShadowrunAction extends BasePluginData implements Comparable<ShadowrunAction> {

	public enum Category {
		MATRIX,
	}
	
	public enum Type {
		FREE,
		SIMPLE,
		COMPLEX,
		VARIABLE,
		INTERRUPT
	}
	
	@Attribute(name="id", required=true)
	private String id;
	
	@Attribute(name="cat",required=true)
	private Category category;
	
	@Attribute(name="type",required=true)
	private Type type;

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		try {
			return i18n.getString("action."+id);
		} catch (MissingResourceException e) {
			logger.error("Missing '"+e.getKey()+"' in "+i18n.getBaseBundleName());
		}
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "action."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "action."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ShadowrunAction other) {
		return getName().compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

}
