/**
 *
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.persist.WeaponDamageConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class AmmunitionData {

	@Attribute(name="dmg")
	@AttribConvert(WeaponDamageConverter.class)
	private Damage damageModifier;
	@Attribute(name="ap")
	private int armorPenetrationModifier;
	@Attribute(name="blast")
	private int blast;

	//-------------------------------------------------------------------
	/**
	 */
	public AmmunitionData() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damageModifier
	 */
	public Damage getDamageModifier() {
		return damageModifier;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damageModifier the damageModifier to set
	 */
	public void setDamageModifier(Damage damageModifier) {
		this.damageModifier = damageModifier;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the armorPenetrationModifier
	 */
	public int getArmorPenetrationModifier() {
		return armorPenetrationModifier;
	}

	//-------------------------------------------------------------------
	/**
	 * @param armorPenetrationModifier the armorPenetrationModifier to set
	 */
	public void setArmorPenetrationModifier(int armorPenetrationModifier) {
		this.armorPenetrationModifier = armorPenetrationModifier;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the blast
	 */
	public int getBlast() {
		return blast;
	}

	//-------------------------------------------------------------------
	/**
	 * @param blast the blast to set
	 */
	public void setBlast(int blast) {
		this.blast = blast;
	}

}
