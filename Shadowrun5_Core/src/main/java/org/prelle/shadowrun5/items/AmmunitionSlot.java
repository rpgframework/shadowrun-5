/**
 * 
 */
package org.prelle.shadowrun5.items;

import java.util.NoSuchElementException;

import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author prelle
 *
 */
public class AmmunitionSlot {
	
	public enum AmmoSlotType {
		BREAK_ACTION("b"),
		CLIP("c"),
		DRUM("d"),
		MUZZLE_LOADER("ml"),
		MAGAZINE("m"),
		CYLINDER("cy"),
		BELT("belt"),
		SPECIAL("special")
		
		;
		String val;
		private AmmoSlotType(String val) {
			this.val = val;
		}
		public String getValue() {return val;}
		public static AmmoSlotType getByValue(String val) {
			for (AmmoSlotType mode : AmmoSlotType.values()) {
				if (mode.getValue().equalsIgnoreCase(val))
					return mode;
			}
			throw new NoSuchElementException(val);
		}
		public String toString() {
			return ShadowrunCore.getI18nResources().getString("ammoslottype."+name().toLowerCase());
		}
	}

	private int amount;
	private AmmoSlotType type;
	
	//-------------------------------------------------------------------
	public AmmunitionSlot() {
	}
	
	//-------------------------------------------------------------------
	public AmmunitionSlot(int amount, AmmoSlotType type) {
		this.amount = amount;
		this.type   = type;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		if (type==null)
			return String.valueOf(amount);
		else
			return String.valueOf(amount)+"("+type.toString()+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	//-------------------------------------------------------------------
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public AmmoSlotType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(AmmoSlotType type) {
		this.type = type;
	}

}
