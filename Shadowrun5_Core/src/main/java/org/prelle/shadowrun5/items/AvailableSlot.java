/**
 * 
 */
package org.prelle.shadowrun5.items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
public class AvailableSlot {

	@Attribute
	private ItemHook ref;
	@Attribute
	private int capacity;
	
	@ElementList(entry="carrieditem",type=CarriedItem.class,inline=true)
	private List<CarriedItem> userAddedItems;
	private transient List<CarriedItem> predefinedItems;
	
	//-------------------------------------------------------------------
	public AvailableSlot() {
		userAddedItems = new ArrayList<CarriedItem>();
		predefinedItems = new ArrayList<CarriedItem>();
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook) {
		this();
		this.ref = hook;
		if (hook.hasCapacity)
			throw new IllegalArgumentException("Hook has capacity - use other constructor");
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook, int capacity) {
		this();
		this.ref = hook;
		this.capacity = capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the accessory
	 */
	public List<CarriedItem> getUserEmbeddedItems() {
		return new ArrayList<>(userAddedItems);
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getAllEmbeddedItems() {
		List<CarriedItem> ret = new ArrayList<>(predefinedItems);
		ret.addAll(userAddedItems);
		return ret;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return ref+"(cap="+capacity+", items="+userAddedItems+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessory the accessory to set
	 */
	public void addEmbeddedItem(CarriedItem accessory) {
//		if (accessory.getItem().getId().equals("silencer")) {
//			try {
//				throw new RuntimeException("Trace");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		int cap = 1;
		if (ref.hasCapacity)
			cap = capacity;
		if (userAddedItems.size()>cap)
			throw new IllegalStateException("Cannot add any more items. Already have "+userAddedItems.size());
		
		userAddedItems.add(accessory);
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessory the accessory to set
	 */
	public void addPredefinedEmbeddedItem(CarriedItem accessory) {
//		if (accessory.getItem().getId().equals("silencer")) {
//			try {
//				throw new RuntimeException("Trace");
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		predefinedItems.add(accessory);
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessory the accessory to set
	 */
	public boolean removeEmbeddedItem(CarriedItem accessory) {
		return userAddedItems.remove(accessory);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	//-------------------------------------------------------------------
	public int getUsedCapacity() {
		int sum = 0;
		for (CarriedItem item : userAddedItems) {
			if (item.getItem().getAccessoryData()!=null) {
				int val = item.getItem().getAccessoryData().getCapacitySize();
				try {
					if (Arrays.asList(item.getItem().getMultiplyWithRate()).contains(Multiply.CAPACITY))
						val *= item.getRating();
				} catch (Exception e) {
					System.err.println(e.toString()+" for "+item);
				}
				sum+= val;
			} else {
				sum += item.getCapacity(ref);
			}
		}
		return sum;
	}

	//-------------------------------------------------------------------
	public int getFreeCapacity() {
		return capacity - getUsedCapacity();
	}

}
