/**
 * 
 */
package org.prelle.shadowrun5.items;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class BodytechData {

	@Attribute
	private BodytechQuality quality;
	@Attribute(name="ess")
	private float essence;

	//--------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public BodytechQuality getQuality() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the essence
	 */
	public float getEssence() {
		return essence;
	}

	//-------------------------------------------------------------------
	/**
	 * @param essence the essence to set
	 */
	public void setEssence(float essence) {
		this.essence = essence;
	}
	
}
