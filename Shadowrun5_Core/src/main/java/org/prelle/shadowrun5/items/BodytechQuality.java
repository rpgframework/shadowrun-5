/**
 * 
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author prelle
 *
 */
public enum BodytechQuality {
	
	STANDARD,
	ALPHA,
	BETA,
	DELTA,
	USED
    ;

    public String getName() {
        return ShadowrunCore.getI18nResources().getString("bodytechquality."+name().toLowerCase());
    }

}
