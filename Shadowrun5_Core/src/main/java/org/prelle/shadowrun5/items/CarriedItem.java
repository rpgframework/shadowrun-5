package org.prelle.shadowrun5.items;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ProgramValue;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.UniqueObject;
import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.shadowrun5.modifications.AccessoryModification;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.CarriedItemModification;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;
import org.prelle.shadowrun5.modifications.ItemHookModification;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;
import org.prelle.shadowrun5.persist.ItemConverter;
import org.prelle.shadowrun5.requirements.ItemHookRequirement;
import org.prelle.shadowrun5.requirements.Requirement;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

@Root(name = "itemref")
public class CarriedItem extends UniqueObject implements Comparable<CarriedItem> {

	private final static Logger logger = LogManager.getLogger("shadowrun.items");

	private static PropertyResourceBundle UI = ShadowrunCore.getI18nResources();

	@Attribute(name="ref",required=true)
	@AttribConvert(ItemConverter.class)
	private ItemTemplate ref;
	@Attribute
	private int rating;
	@Attribute
	private String customName;
	@Attribute(name="count",required=false)
	private int count = 1;
	@Attribute
	private ItemLocationType location;
	/**
	 * Nuyen the character invested here. May be 0 for loot
	 */
	@org.prelle.simplepersist.Attribute(name="price",required=false)
	private Integer price = null;
	@Attribute
	private boolean bound;
	@Attribute(name="qual")
	private BodytechQuality quality;

	@ElementList(entry="programs",type=ProgramValue.class)
	private List<ProgramValue> programs;

	@ElementList(entry="accessory",type=AvailableSlot.class)
	private List<AvailableSlot> accessories;
	private transient Map<ItemHook,AvailableSlot> defaultAccessories;


	/**
	 * Modifications made to the character when equipped. Calculated from
	 * enhancements
	 */
	private transient List<Modification> characterModifications;
	/**
	 * Modifications made to the character when equipped, resulting from not
	 * fulfilled requirements.
	 */
	private transient List<Modification> requirementModifications;

	private transient boolean createdByModification;

	//--------------------------------------------------------------------
	public CarriedItem() {
		modifications = new ArrayList<>();
		characterModifications = new ArrayList<>();
		requirementModifications = new ArrayList<>();
		count = 1;
		accessories = new ArrayList<>();
		defaultAccessories = new HashMap<ItemHook, AvailableSlot>();
		programs = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template) {
		this();
		ref = template;

		updateModifications();
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate template, int count) {
		this();
		ref = template;
		this.count = count;
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof CarriedItem) {
			CarriedItem other = (CarriedItem)o;
			if (ref!=other.getItem()) return false;
			//			if (location!=other.getLocation()) return false;
			if (customName!=null && !customName.equals(other.getName())) return false;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	public void updateModifications() {
		modifications.clear();
		characterModifications.clear();
		requirementModifications.clear();
		defaultAccessories.clear();

		List<Multiply> multi = new ArrayList<ItemTemplate.Multiply>();
		if (ref.hasRating() && ref.getMultiplyWithRate()!=null)
			multi = Arrays.asList(ref.getMultiplyWithRate());

		/*
		 * Instantiate all templates and slots
		 */
		for (Modification mod : ref.getModifications()) {
			if (mod instanceof ItemHookModification) {
				// Add or remove hooks
				ItemHookModification hookMod = (ItemHookModification)mod;
				if (hookMod.isRemove()) {
					logger.debug("Remove hook "+hookMod.getHook()+" to "+getName());
					defaultAccessories.remove(hookMod.getHook());
				} else {
					int newCap = hookMod.getCapacity();
					if (multi.contains(Multiply.CAPACITY))
						newCap*= rating;
					logger.debug("Add hook "+hookMod.getHook()+" with capacity "+newCap+" to "+getItem().getId());
					defaultAccessories.put(hookMod.getHook(), new AvailableSlot(hookMod.getHook(), newCap));
				}
				//				logger.debug("Default slots of "+getName()+" now "+defaultAccessories);
			} else if (mod instanceof AccessoryModification) {
				// Apply default modifications
				AccessoryModification accMod = (AccessoryModification)mod.clone();
				CarriedItem item = new CarriedItem(accMod.getItem());
				item.setPrice(0);
				item.setCreatedByModification(true);
				if (accMod.getRating()>0)
					item.setRating(accMod.getRating());
				// Create slot if necessary
				AvailableSlot slot = defaultAccessories.get(accMod.getHook());
				if (slot==null) {
					slot = new AvailableSlot(accMod.getHook());
					defaultAccessories.put(accMod.getHook(), slot);
				}
				logger.debug("Add accessory '"+item.getItem().getId()+"' into slot "+slot+" of "+getItem().getId());
				slot.addPredefinedEmbeddedItem(item);
			} else if (mod instanceof ItemAttributeModification) {
				ItemAttributeModification attrMod = (ItemAttributeModification)mod.clone();
				ItemAttributeModification newMod = new ItemAttributeModification(attrMod.getAttribute(), attrMod.getValue());
				if (ref.hasRating()) {
					logger.debug("   check "+attrMod+" with "+multi);
					if (attrMod.getAttribute()==ItemAttribute.RECOIL_COMPENSATION && multi.contains(Multiply.RECOIL))
						newMod.setValue(newMod.getValue()*rating);
				}
				logger.debug("Modify item attribute '"+attrMod.getAttribute()+" "+newMod.getValue()+"' of "+getItem().getId());
				modifications.add(newMod);
			} else if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod.clone();
				aMod.setSource(this);
				logger.debug("Store character modification '"+aMod+"'  of "+getItem().getId());
				characterModifications.add(aMod);
			} else if (mod instanceof CarriedItemModification) {
				CarriedItemModification cMod = (CarriedItemModification)mod.clone();
				cMod.setSource(this);
				if (cMod.getItem()==null) {
					logger.error("CarriedItemModification without valid item - from "+mod.getSource());
					throw new IllegalArgumentException("CarriedItemModification without valid item - from "+mod.getSource());
				}
				AvailableSlot slot = defaultAccessories.get(ItemHook.INTERNAL);
				if (slot==null) {
					slot = new AvailableSlot(ItemHook.INTERNAL);
					defaultAccessories.put(ItemHook.INTERNAL, slot);
				}
				CarriedItem foo = new CarriedItem(cMod.getItem());
				foo.setCreatedByModification(true);
				slot.addEmbeddedItem(foo);
				logger.info("Created embedded item "+cMod.getItem()+" in INTERNAL SLOT of "+this);
			} else if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod.clone();
				sMod.setSource(this);
				characterModifications.add(sMod);
			} else {
				logger.error("Don't know how to handle modification "+mod.getClass()+" = "+mod);
			}
		}

		/*
		 * Walk through all slots and get modifications
		 */
		for (AvailableSlot slot : defaultAccessories.values()) {
			// Walk through all items in this slot
			for (CarriedItem embedded : slot.getAllEmbeddedItems()) {
				modifications.addAll(embedded.getModifications());
			}
		}
		for (AvailableSlot slot : accessories) {
			// Walk through all items in this slot
			for (CarriedItem embedded : slot.getAllEmbeddedItems()) {
				modifications.addAll(embedded.getModifications());
			}
		}

		//		logger.info("Modifications for "+getName()+" now "+modifications);
	}

	//--------------------------------------------------------------------
	public String toString() {
		return "CarriedItem "+ref+" (Mods: "+modifications+")"; //+" (Depracated: "+ignoreModifications+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemTemplate getItem() {
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @param item the item to set
	 */
	public void setItem(ItemTemplate item) {
		this.ref = item;
	}

	//	//--------------------------------------------------------------------
	//	public ItemLocationType getLocation() {
	//		return location;
	//	}
	//
	//	//--------------------------------------------------------------------
	//	public void setItemLocation(ItemLocationType location) {
	//		this.location = location;
	//	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CarriedItem other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//--------------------------------------------------------------------
	public boolean isType(ItemType type) {
		return ref.getType()==type;
	}

	//--------------------------------------------------------------------
	public boolean isSubType(ItemSubType type) {
		return ref.getSubtype()==type;
	}

	//--------------------------------------------------------------------
	public ItemSubType getSubType() {
		return ref.getSubtype();
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (customName!=null)
			return customName;
		return ref.getName();
	}

	//-------------------------------------------------------------------
	public String getNameWithRating() {
		if (ref.hasRating())
			return getName()+", "+UI.getString("label.rating")+" "+rating;
		return getName();
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		customName = name;
	}

	//--------------------------------------------------------------------
	public Availability getAvailability() {
		Availability ret = new Availability(ref.getAvailability().getValue(), ref.getAvailability().getLegality(), ref.getAvailability().isAddToAvailability());

		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.AVAIL))
			ret.setValue( ref.getAvailability().getValue() * rating);

		/*
		 * If any embedded item has higher values, use them
		 */
		for (CarriedItem access : getAccessories()) {
			Availability foo = access.getAvailability();
			if (foo.isAddToAvailability()) {
				ret.setValue(ret.getValue() + foo.getValue());
			} else
				ret.setValue( Math.max(ret.getValue(), foo.getValue()));
			if (foo.getLegality().ordinal()>ret.getLegality().ordinal())
				ret.setLegality(foo.getLegality());
		}


		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the characterModifications
	 */
	public List<Modification> getCharacterModifications() {
		boolean multiply = false;
		if (ref.getMultiplyWithRate()!=null)
			multiply = Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.MODIFIER);
		logger.debug("  getCharMods of "+ref+" / "+multiply);
		List<Modification> ret = new ArrayList<Modification>();
		for (Modification tmp : ref.getModifications()) {
//			logger.info("* mod "+tmp);
			if (tmp instanceof AttributeModification) {
				AttributeModification newMod = (AttributeModification)((AttributeModification)tmp).clone();
				if (multiply) 
					newMod.setValue(((AttributeModification)tmp).getValue()*getRating());
				newMod.setSource(this);
				ret.add(newMod);
			} else if (tmp instanceof ItemHookModification) {
			} else if (tmp instanceof AccessoryModification) {
			} else if (tmp instanceof CarriedItemModification) {
				CarriedItemModification newMod = (CarriedItemModification) ((CarriedItemModification)tmp).clone();
				newMod.setSource(this);
				ret.add(newMod);
			} else if (tmp instanceof SkillModification) {
				SkillModification newMod = (SkillModification)((SkillModification)tmp).clone();
				if (multiply) 
					newMod.setValue(((SkillModification)tmp).getValue()*getRating());
				newMod.setSource(this);
				ret.add(newMod);
			} else if (tmp instanceof SkillGroupModification) {
				SkillGroupModification newMod = (SkillGroupModification)((SkillGroupModification)tmp).clone();
				if (multiply) 
					newMod.setValue(((SkillGroupModification)tmp).getValue()*getRating());
				newMod.setSource(this);
				ret.add(newMod);
			} else
				logger.warn("TODO: process "+tmp.getClass());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public void addCharacterModification(Modification mod) {
		//		if (!characterModifications.contains(mod))
		characterModifications.add(mod);
	}

	//-------------------------------------------------------------------
	public void removeCharacterModification(Modification mod) {
		characterModifications.remove(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * Get the modifications calculated based upon requirements not met by
	 * the character
	 */
	public List<Modification> getRequirementModifications() {
		return requirementModifications;
	}

	//-------------------------------------------------------------------
	public void addRequirementModification(Modification mod) {
		if (!requirementModifications.contains(mod))
			requirementModifications.add(mod);
	}

	//-------------------------------------------------------------------
	public void clearRequirementModification() {
		requirementModifications.clear();
	}

	//-------------------------------------------------------------------
	public int getCapacity(ItemHook hook) {
		for (Requirement req : ref.getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement hookReq = (ItemHookRequirement)req;
				if (hookReq.getSlot()==hook)
					return hookReq.getCapacity();
			}
		}
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifyableImpl#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		//		List<Modification> ret = new ArrayList<Modification>(ref.getModifications());
		//		if (ref.getWeaponData()!=null)
		//			ret.addAll(ref.getWeaponData().getModifications());
		//
		//		for (AvailableSlot slot : addedAccessories) {
		//			for (CarriedItem item : slot.getEmbeddedItems()) {
		//				ret.addAll(item.getModifications());
		//			}
		//		}
		//
		//		return ret;
		return new ArrayList<Modification>(modifications);
	}

	//-------------------------------------------------------------------
	public Object getAsObject(ItemAttribute attr) {
		switch (attr) {
		case AVAILABILITY:
			return getAvailability();
		case ESSENCECOST:
			return getEssence();
		case AMMUNITION:
			if (ref.getWeaponData()!=null) {
				StringBuffer buf = new StringBuffer();
				if (ref.getWeaponData().getAmmunition()!=null) {
					Iterator<AmmunitionSlot> it = ref.getWeaponData().getAmmunition().iterator();
					while (it.hasNext()) {
						buf.append(it.next().toString());
						if (it.hasNext())
							buf.append("/");
					}
				} else {
					buf.append("-");
				}
				return buf.toString();
			}
			logger.warn("Requesting AMMUNITION for non-Weapon");
			return null;
		case MODE:
			if (ref.getWeaponData()!=null) {
				StringBuffer buf = new StringBuffer();
				if (ref.getWeaponData().getFireModes()!=null) {
					Iterator<FireMode> it = ref.getWeaponData().getFireModes().iterator();
					while (it.hasNext()) {
						buf.append(it.next().getName());
						if (it.hasNext())
							buf.append("/");
					}
				} else {
					buf.append("-");
				}
				return buf.toString();
			}
			logger.warn("Requesting MODE for non-Weapon");
			return null;
		case SKILL:
			if (ref.getWeaponData()!=null)
				return ref.getWeaponData().getSkill();
			logger.warn("Requesting "+attr+" for non-Weapon");
			return null;
		case PRICE:
			return getPrice();
		case HANDLING:
			if (ref.getVehicleData()!=null) {
				return ref.getVehicleData().getHandling();
			}
			logger.warn("Requesting HANDLING for non-vehicle "+ref);
			return null;
		case SPEED:
			if (ref.getVehicleData()!=null) {
				return ref.getVehicleData().getSpeed();
			}
			logger.warn("Requesting SPEED for non-vehicle");
			return null;
		case CYBER_ATTRIBUTES:
			if (ref.getSubtype()==ItemSubType.CYBERDECK)
				return ref.getCyberdeckData().getArray();
			return new int[]{ref.getDeviceRating(),ref.getDeviceRating(),ref.getDeviceRating(),ref.getDeviceRating()};
		default:
			throw new IllegalArgumentException("Use getAsValue for "+attr);
		}

	}

	//-------------------------------------------------------------------
	public ItemAttributeValue getAsValue(ItemAttribute attr) {
		switch (attr) {
		case ACCELERATION:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.ACCELERATION, ref.getVehicleData().getAcceleration(), getModifications());
			logger.warn("Requesting ACCELERATION for non-vehicle");
			return null;
		case ACCURACY:
			if (ref.getWeaponData()!=null)
				return new ItemAttributeValue(ItemAttribute.ACCURACY, ref.getWeaponData().getAccuracy(), getModifications());
			logger.warn("Requesting ACCURACY for non-Weapon");
			return null;
		case ARMOR:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.ARMOR, ref.getVehicleData().getArmor(), getModifications());
			if (ref.getArmorData()!=null) {
				if (ref.getMultiplyWithRate()!=null && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.ARMOR))
					return new ItemAttributeValue(ItemAttribute.ARMOR, rating*ref.getArmorData().getRating(), getModifications());
				else
					return new ItemAttributeValue(ItemAttribute.ARMOR, ref.getArmorData().getRating(), getModifications());
			}
			logger.warn("Requesting ARMOR for non-vehicle and non-armor "+ref);
			return null;
		case ARMOR_PENETRATION:
			if (ref.getWeaponData()!=null)
				return new ItemAttributeValue(ItemAttribute.ARMOR_PENETRATION, ref.getWeaponData().getArmorPenetration(), getModifications());
			logger.warn("Requesting "+attr+" for non-Weapon");
			return null;
		case BODY:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.BODY, ref.getVehicleData().getBody(), getModifications());
			logger.warn("Requesting BODY for non-vehicle");
			return null;
		case DAMAGE:
			if (ref.getWeaponData()!=null)
				return new Damage(ref.getWeaponData().getDamage(), getModifications());
			logger.warn("Requesting "+attr+" for non-Weapon");
			return null;
		case DEVICE_RATING:
			return new ItemAttributeValue(ItemAttribute.DEVICE_RATING, ref.getDeviceRating(), getModifications());
		case PILOT:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.PILOT, ref.getVehicleData().getPilot(), getModifications());
			logger.warn("Requesting PILOT for non-vehicle");
			return null;
		case REACH:
			if (ref.getWeaponData()!=null)
				return new ItemAttributeValue(ItemAttribute.REACH, ref.getWeaponData().getReach(), getModifications());
			logger.warn("Requesting "+attr+" for non-MeleeWeapon");
			return null;
		case RECOIL_COMPENSATION:
			if (ref.getWeaponData()!=null)
				return new ItemAttributeValue(ItemAttribute.RECOIL_COMPENSATION, ref.getWeaponData().getRecoilCompensation(), getModifications());
			logger.warn("Requesting "+attr+" for non-FireWeapon");
			return null;
		case SEATS:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.SEATS, ref.getVehicleData().getSeats(), getModifications());
			logger.warn("Requesting SEATS for non-vehicle");
			return null;
		case SENSORS:
			if (ref.getVehicleData()!=null)
				return new ItemAttributeValue(ItemAttribute.SENSORS, ref.getVehicleData().getSensor(), getModifications());
			logger.warn("Requesting SENSORS for non-vehicle");
			return null;
		case PRICE:
			return new ItemAttributeValue(ItemAttribute.PRICE, getPrice(), new ArrayList<Modification>());
		case ESSENCECOST:
			float essence = ref.getBodyTech().getEssence()*1000;
			if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.ESSENCE))
				essence *= getRating();
			if (ref.getBodyTech().getQuality()!=null) {
				switch (ref.getBodyTech().getQuality()) {
				case ALPHA: essence *= 0.8; break;
				case BETA : essence *= 0.7; break;
				case DELTA: essence *= 0.5; break;
				case USED : essence *= 1.25; break;
				case STANDARD:
				}
			}
			return new ItemAttributeValue(ItemAttribute.ESSENCECOST, Math.round(essence), new ArrayList<Modification>());
		case ATTACK:
		case DATA_PROCESSING:
		case FIREWALL:
		case SLEAZE:
			if (ref.getSubtype()==ItemSubType.CYBERDECK)
				return new ItemAttributeValue(attr, 0, getModifications());
			return new ItemAttributeValue(attr, ref.getDeviceRating(), getModifications());
		case CONCURRENT_PROGRAMS:
			if (ref.getSubtype()==ItemSubType.CYBERDECK)
				return new ItemAttributeValue(attr, ref.getCyberdeckData().getPrograms(), getModifications());
			return new ItemAttributeValue(attr, 1, getModifications());

		default:
			throw new IllegalArgumentException("Use getAsObject for "+attr);
		}

	}

	//-------------------------------------------------------------------
	/**
	 * Give the advantages of this item and recursively walk through all
	 * embedded items and return their advantages as well
	 */
	public Collection<String> getWiFiAdvantageStringRecursivly() {
		List<String> ret = new ArrayList<>();
		ret.addAll(ref.getWiFiAdvantageStrings());
		// Add eventually existing advantages from accessories
		for (AvailableSlot aVal : accessories) {
			for (CarriedItem accessory : aVal.getAllEmbeddedItems())
				ret.addAll(accessory.getWiFiAdvantageStringRecursivly());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Give the advantages of this item and non-recursively walk through all
	 * embedded items and return their advantages as well
	 */
	public Collection<String> getWiFiAdvantageStrings() {
		List<String> ret = new ArrayList<>();
		ret.addAll(ref.getWiFiAdvantageStrings());
		// Add eventually existing advantages from accessories
		//		for (AvailableSlot aVal : accessories) {
		//			for (CarriedItem accessory : aVal.getEmbeddedItems())
		//				ret.addAll(accessory.getItem().getWiFiAdvantageStrings());
		//		}
		return ret;
	}

	//-------------------------------------------------------------------
	public void addSlot(AvailableSlot slot) {
		if (getSlot(slot.getSlot())!=null)
			throw new IllegalArgumentException("Already have a slot "+slot.getSlot());

		accessories.add(slot);
	}

	//-------------------------------------------------------------------
	public Collection<AvailableSlot> getSlots() {
		Map<ItemHook, AvailableSlot> ret = new HashMap<ItemHook, AvailableSlot>();
		for (AvailableSlot slot : defaultAccessories.values()) {
			ret.put(slot.getSlot(), slot);
			logger.debug("getSlots(): Add default: "+slot);
		}
		for (AvailableSlot slot : accessories) {
			ret.put(slot.getSlot(), slot);
			logger.debug("getSlots(): Add empty: "+slot);
		}

		return new ArrayList<>(ret.values());
	}

	//-------------------------------------------------------------------
	public AvailableSlot getSlot(ItemHook hook) {
		for (AvailableSlot slot : defaultAccessories.values()) {
			if (slot.getSlot()==hook)
				return slot;
		}
		for (AvailableSlot slot : accessories) {
			if (slot.getSlot()==hook)
				return slot;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public Collection<CarriedItem> getAccessories() {
		List<CarriedItem> ret = new ArrayList<CarriedItem>();
		for (AvailableSlot slot : defaultAccessories.values()) {
			ret.addAll(slot.getAllEmbeddedItems());
		}
		for (AvailableSlot slot : accessories) {
			ret.addAll(slot.getUserEmbeddedItems());
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public boolean hasHook(ItemHook slot) {
		for (AvailableSlot avail : accessories) {
			if (avail.getSlot()==slot)
				return true;
		}
		return defaultAccessories.containsKey(slot);
	}

	//-------------------------------------------------------------------
	public void addAccessory(ItemHook slot, CarriedItem val) {
		if (!hasHook(slot))
			throw new IllegalArgumentException("No hook "+slot+" in instance of "+ref.getId());

		for (AvailableSlot avail : accessories) {
			if (avail.getSlot()==slot) {
				avail.addEmbeddedItem(val);
				updateModifications();
				return;
			}
		}
		// Add new
		AvailableSlot realSlot = getSlot(slot);
		realSlot.addEmbeddedItem(val);
		accessories.add(realSlot);

		updateModifications();
	}

	//-------------------------------------------------------------------
	public boolean removeAccessory(CarriedItem val) {
		boolean removed = false;
		//		logger.debug("removeAccessory on "+getName()+" checks slots: "+accessories);
		for (AvailableSlot avail : getSlots()) {
			if (avail.removeEmbeddedItem(val)) {
				removed = true;
				logger.debug("Removed "+val+" from slot "+avail);
			} 
		}
		if (!removed)
			logger.warn("Did not find any slot that contains "+val+" in "+dump());
		updateModifications();
		return removed;
	}

	//-------------------------------------------------------------------
	public void setPrice(int price) {
		this.price = price;
	}

	//-------------------------------------------------------------------
	private int getPrice() {
		if (createdByModification)
			return 0;
		if (price!=null)
			return price;

		int ret = ref.getPrice();

		//		logger.info("......"+ret+"...."+ref.hasRating()+"/"+rating+"/"+Arrays.toString(ref.getMultiplyWithRate()));

		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.PRICE))
			ret *= rating;

		// Calculate extra multiplier modifications
		for (CarriedItem access : getAccessories()) {
			//			logger.info("........access "+access.getItem().getId()+" has Price "+access.getPrice());
			if (access.getPrice()<0)
				ret *= (access.getPrice()*-1);
		}
		// Calculate extra modifications
		for (CarriedItem access : getAccessories()) {
			if (access.getPrice()>0)
				ret += access.getPrice();
		}
		// Calculate embedded programs
		for (ProgramValue tmp : getPrograms()) {
			Program access = tmp.getModifyable();
			switch (access.getType()) {
			case STANDARD: ret += 80; break;
			case HACKING : ret += 250; break;
			case AGENT:
				if (tmp.getRating()<4)
					ret += (tmp.getRating()*1000);
				else
					ret += (tmp.getRating()*2000);
			}
		}

		ret*=count;

		return ret;
	}

	//-------------------------------------------------------------------
	private float getEssence() {
		float ret = ref.getBodyTech().getEssence();

		//		logger.info("......"+ret+"...."+ref.hasRating()+"/"+rating+"/"+Arrays.toString(ref.getMultiplyWithRate()));

		if (ref.hasRating() && Arrays.asList(ref.getMultiplyWithRate()).contains(Multiply.ESSENCE))
			ret *= rating;

		// Modify essence by quality
		if (ref.getBodyTech().getQuality()!=null) {
			switch (ref.getBodyTech().getQuality()) {
			case STANDARD: break;
			case ALPHA: ret *= 0.8f; break;
			case BETA : ret *= 0.7f; break;
			case DELTA: ret *= 0.5f; break;
			case USED : ret *= 1.25f; break;
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Check if the item has a specific accessory embedded
	 * @param item
	 * @return
	 */
	public boolean hasEmbedded(ItemTemplate item) {
		// Built-in accessories
		//		if (ref instanceof FirearmWeapon) {
		//			for (AccessoryValue tmp : ((FirearmWeapon)ref).getAccessories()) {
		//				if (tmp.getAccessory().getId().equals(item.getId()))
		//					return true;
		//			}
		//		}

		for (CarriedItem tmp : getAccessories()) {
			if (tmp.getItem().getId().equals(item.getId()))
				return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String dump() {
		return dump(0);
	}

	//-------------------------------------------------------------------
	public String dump(int depth) {
		String indent = new String(new char[depth]).replace('\0', ' ');
		StringBuffer buf = new StringBuffer(indent+getName()+"   (Cost "+getPrice()+")");
		for (String wifi : getWiFiAdvantageStrings()) {
			buf.append("\n"+indent+"  WIFI: "+wifi);
		}

		for (AvailableSlot slot : getSlots()) {
			if (slot.getCapacity()==0)
				buf.append("\n"+indent+"- "+slot.getSlot().getName());
			else
				buf.append("\n"+indent+"- "+slot.getSlot().getName()+" (Capacity "+slot.getUsedCapacity()+"/"+slot.getCapacity()+")");

			for (CarriedItem accessory : slot.getAllEmbeddedItems()) {
				// Embedded items
				int size = accessory.getCapacity(slot.getSlot());
				buf.append("\n"+indent+" +(Capacity "+size+") "+accessory.dump(depth+2));
			}

		}
		return buf.toString();
	}


	//-------------------------------------------------------------------
	public List<WeaponData> getWeaponData() {
		List<WeaponData> ret = new ArrayList<WeaponData>();
		if (ref.getWeaponData()!=null)
			ret.add(ref.getWeaponData());

		// Add weapons from accessories
		for (CarriedItem embedded : getSubItems()) {
			ret.addAll(embedded.getWeaponData());
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getSubItems() {
		List<CarriedItem> ret = new ArrayList<CarriedItem>();
		// Add weapons from accessories
		for (AvailableSlot slot : accessories) {
			for (CarriedItem embedded : slot.getAllEmbeddedItems()) {
				ret.add(embedded);
			}
		}
		for (AvailableSlot slot : defaultAccessories.values()) {
			for (CarriedItem embedded : slot.getAllEmbeddedItems()) {
				ret.add(embedded);
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		if (ref!=null && !ref.hasRating())
			throw new IllegalArgumentException("Item "+ref.getId()+" has no rating");
		this.rating = rating;

		updateModifications();
	}

	//-------------------------------------------------------------------
	/**
	 * For foci: is it already bound
	 */
	public boolean isBound() {
		return bound;
	}

	//-------------------------------------------------------------------
	/**
	 * @param bound the bound to set
	 */
	public void setBound(boolean bound) {
		this.bound = bound;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public BodytechQuality getQuality() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(BodytechQuality quality) {
		this.quality = quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the characterModifications
	 */
	public List<ProgramValue> getPrograms() {
		List<ProgramValue> ret = new ArrayList<ProgramValue>();
		ret.addAll(programs);
		Collections.sort(ret);
		return ret;
	}

	//-------------------------------------------------------------------
	public void addProgram(ProgramValue mod) {
		if (!programs.contains(mod))
			programs.add(mod);
	}

	//-------------------------------------------------------------------
	public void removeProgram(ProgramValue mod) {
		programs.remove(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the createdByModification
	 */
	public boolean isCreatedByModification() {
		return createdByModification;
	}

	//-------------------------------------------------------------------
	/**
	 * @param createdByModification the createdByModification to set
	 */
	public void setCreatedByModification(boolean createdByModification) {
		this.createdByModification = createdByModification;
	}


}
