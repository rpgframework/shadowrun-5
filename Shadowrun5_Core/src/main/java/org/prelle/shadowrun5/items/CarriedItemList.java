/**
 * 
 */
package org.prelle.shadowrun5.items;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;

/**
 * @author prelle
 *
 */
@ElementList(entry="item",type=CarriedItem.class)
public class CarriedItemList extends ArrayList<CarriedItem> {

	private static final long serialVersionUID = 1L;


}
