/**
 * 
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.persist.IntegerArrayConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class CyberdeckData {

	@Attribute(name="array")
	@AttribConvert(IntegerArrayConverter.class)
	private int[] array;
	@Attribute
	private int programs;

	//-------------------------------------------------------------------
	/**
	 * @return the array
	 */
	public int[] getArray() {
		return array;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the programs
	 */
	public int getPrograms() {
		return programs;
	}

	//-------------------------------------------------------------------
	/**
	 * @param array the array to set
	 */
	public void setArray(int[] array) {
		this.array = array;
	}

	//-------------------------------------------------------------------
	/**
	 * @param programs the programs to set
	 */
	public void setPrograms(int programs) {
		this.programs = programs;
	}

}
