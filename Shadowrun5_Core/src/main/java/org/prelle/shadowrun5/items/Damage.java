/**
 *
 */
package org.prelle.shadowrun5.items;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.prelle.shadowrun5.ShadowrunCore;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class Damage extends ItemAttributeValue {

	public enum Type {
		PHYSICAL,
		STUN
	}

	public enum WeaponDamageType {
		NORMAL,
		ELECTRICAL,
		FLECHETTE,
	}

	private boolean addStrength;
	private Type type;
	private WeaponDamageType weaponDamageType = WeaponDamageType.NORMAL;

	//--------------------------------------------------------------------
	public Damage() {
		super(ItemAttribute.DAMAGE,0, new ArrayList<Modification>());
	}

	//--------------------------------------------------------------------
	public Damage(int val, boolean addStrength, Type type, WeaponDamageType dType) {
		super(ItemAttribute.DAMAGE,val, new ArrayList<Modification>());
		this.addStrength = addStrength;
		this.type = type;
		this.weaponDamageType = dType;
	}

	//--------------------------------------------------------------------
	public Damage(Damage copy, List<Modification> mods) {
		super(ItemAttribute.DAMAGE, copy.getValue(), mods);
		addStrength = copy.addStrength();
		type        = copy.getType();
		weaponDamageType = copy.getWeaponDamageType();
		modifications.addAll(mods);
	}

	//--------------------------------------------------------------------
	public boolean addStrength() {
		return addStrength;
	}

	//--------------------------------------------------------------------
	public int getValue() {
		return value;
	}

	//--------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//--------------------------------------------------------------------
	public String toString() {
		StringBuffer buf = new StringBuffer();
		PropertyResourceBundle res = ShadowrunCore.getI18nResources();
		if (addStrength) {
			buf.append("("+org.prelle.shadowrun5.Attribute.STRENGTH.getShortName());
			buf.append("+"+value);
			buf.append(")");
		} else
			buf.append(String.valueOf(value));

		if (type==Type.PHYSICAL)
			buf.append(res.getString("damage.physical.short"));
		else
			buf.append(res.getString("damage.stun.short"));

		if (weaponDamageType!=null && weaponDamageType!=WeaponDamageType.NORMAL) {
			buf.append("("+res.getString("damage.damagetype."+weaponDamageType.name().toLowerCase())+")");
		}

		return buf.toString();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the addStrength
	 */
	public boolean isAddStrength() {
		return addStrength;
	}

	//--------------------------------------------------------------------
	/**
	 * @param addStrength the addStrength to set
	 */
	public void setAddStrength(boolean addStrength) {
		this.addStrength = addStrength;
	}

	//--------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the weaponDamageType
	 */
	public WeaponDamageType getWeaponDamageType() {
		return weaponDamageType;
	}

	//--------------------------------------------------------------------
	/**
	 * @param weaponDamageType the weaponDamageType to set
	 */
	public void setWeaponDamageType(WeaponDamageType weaponDamageType) {
		this.weaponDamageType = weaponDamageType;
	}


}
