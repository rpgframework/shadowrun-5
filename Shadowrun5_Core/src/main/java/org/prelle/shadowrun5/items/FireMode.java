package org.prelle.shadowrun5.items;

import java.util.NoSuchElementException;

import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.simplepersist.EnumValue;

public enum FireMode {
	@EnumValue("SA")
	SEMI_AUTOMATIC("SA"),
	@EnumValue("SS")
	SINGLE_SHOT("SS"),
	@EnumValue("BF")
	BURST_FIRE("BF"),
	@EnumValue("FA")
	FULL_AUTO("FA")
	;
	String val;
	private FireMode(String val) {
		this.val = val;
	}
	public String getValue() {return val;}
	public static FireMode getByValue(String val) {
		for (FireMode mode : FireMode.values()) {
			if (mode.getValue().equalsIgnoreCase(val))
				return mode;
			if (mode.getName().equalsIgnoreCase(val))
				return mode;
		}
		throw new NoSuchElementException(val);
	}
	public String getName() {
        return ShadowrunCore.getI18nResources().getString("firemode."+name().toLowerCase());
	}
}