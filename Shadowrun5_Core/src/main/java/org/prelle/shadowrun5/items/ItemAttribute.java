/**
 *
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public enum ItemAttribute {

	ACCELERATION,
	ACCURACY,
	AMMUNITION,
	ARMOR,
	ARMOR_PENETRATION,
	AVAILABILITY,
	BODY,
	CAPACITY,
	CONCEALABILITY,
	DAMAGE,
	DEVICE_RATING,
	ESSENCECOST,
	HANDLING,
	HAS_RATING,
	MODE,
	MAX_RATING,
	PILOT,
	PRICE,
	REACH,
	RECOIL_COMPENSATION,
	QUALITY,
	SEATS,
	SENSORS,
	SKILL,
	SPEED,

	ATTACK,
	SLEAZE,
	DATA_PROCESSING,
	FIREWALL,
	CONCURRENT_PROGRAMS,
	CYBER_ATTRIBUTES,
	;

	//--------------------------------------------------------------------
	public String getName() {
		return ShadowrunCore.getI18nResources().getString("itemattribute."+ItemAttribute.this.name().toLowerCase());
	}

	//--------------------------------------------------------------------
	public String getShortName() {
		return ShadowrunCore.getI18nResources().getString("itemattribute."+ItemAttribute.this.name().toLowerCase()+".short");
	}

}
