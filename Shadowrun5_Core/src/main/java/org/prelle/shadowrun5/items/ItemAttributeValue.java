package org.prelle.shadowrun5.items;

import java.util.List;

import org.prelle.shadowrun5.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

public class ItemAttributeValue extends ModifyableImpl implements
		ModifyableValue<ItemAttribute> {

	private ItemAttribute attribute;
	protected int value;

	//--------------------------------------------------------------------
	public ItemAttributeValue(ItemAttribute attr, int val, List<Modification> mods) {
		this.attribute = attr;
		value = val;
		super.modifications = mods;
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (getModifier()==0)
			return String.valueOf(value);
		return value+" ("+getModifiedValue()+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public ItemAttribute getModifyable() {
		return attribute;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int points) {
		value = points;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof ItemAttributeModification) {
				ItemAttributeModification sMod = (ItemAttributeModification)mod;
				if (sMod.getModifiedItem()==attribute)
					count += sMod.getValue();
			}
		}
		return count;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

}
