/**
 *
 */
package org.prelle.shadowrun5.items;

import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun5.BasePluginData;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;

/**
 * @author Stefan
 *
 */
public class ItemEnhancement extends BasePluginData  {

	@Attribute(required=true)
	private String id;

	//--------------------------------------------------------------------
	/**
	 */
	public ItemEnhancement() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		try {
			return i18n.getString("itemmod."+id);
		} catch (MissingResourceException e) {
			logger.error(e.getMessage()+" in "+i18n.getBaseBundleName());
			return "itemmod."+id;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "itemmod."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	public List<Modification> getModifications() {
		return null;
	}

}
