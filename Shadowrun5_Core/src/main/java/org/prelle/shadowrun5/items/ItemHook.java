/**
 *
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public enum ItemHook {

	BARREL,
	INTERNAL,
	SMARTGUN,
	TOP,
	UNDER,
	STOCK,
	COMMLINK,
	OPTICAL(true),
	AUDIO(true),
	SENSOR_HOUSING(true),
	ARMOR(true),
	ARMOR_ADDITION(true),
//	HEADWARE_IMPLANT(true),
	CYBERHEAD_DECK,
	CYBERHEAD_COMM,
	CYBEREYE_IMPLANT(true),
	CYBEREAR_IMPLANT(true),
	CYBERLIMB_IMPLANT(true),
	VEHICLE_DRIVE(true),
	VEHICLE_PROTECTION(true),
	VEHICLE_WEAPON(true),
	VEHICLE_BODY(true),
	VEHICLE_ELECTRONICS(true),
	VEHICLE_COSMETICS(true),
	
	;

	boolean hasCapacity;
	
	//-------------------------------------------------------------------
	private ItemHook() {
		hasCapacity = false;
	}
	
	//-------------------------------------------------------------------
	private ItemHook(boolean cap) {
		hasCapacity = cap;
	}
	
	//-------------------------------------------------------------------
	public String getName() {
		return ShadowrunCore.getI18nResources().getString("itemhook."+this.name().toLowerCase());
	}
	
	//-------------------------------------------------------------------
	public boolean hasCapacity() {
		return hasCapacity;
	}
}
