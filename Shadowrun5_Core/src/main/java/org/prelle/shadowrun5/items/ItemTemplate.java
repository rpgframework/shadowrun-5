package org.prelle.shadowrun5.items;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.MissingResourceException;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.modifications.ItemHookModification;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.shadowrun5.persist.AvailabilityConverter;
import org.prelle.shadowrun5.persist.MultiplyByRatingConverter;
import org.prelle.shadowrun5.requirements.RequirementList;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name = "item")
public class ItemTemplate extends BasePluginData implements Comparable<ItemTemplate> {

	private final static Logger logger = LogManager.getLogger("shadowrun.items");

	public enum Legality {
		LEGAL,
		RESTRICTED,
		FORBIDDEN
		;
		public String getShortCode() {
			if (this==LEGAL)
				return "";
			return ShadowrunCore.getI18nResources().getString("availability."+this.name().toLowerCase());
		}
		public static Legality valueOfShortCode(String val) {
			if (val.equalsIgnoreCase("f") || val.equalsIgnoreCase("v"))
				return Legality.FORBIDDEN;
			if (val.equalsIgnoreCase("r") || val.equalsIgnoreCase("e"))
				return Legality.RESTRICTED;
			for (Legality tmp : Legality.values())
				if (tmp.getShortCode().equalsIgnoreCase(val))
					return tmp;
			throw new IllegalArgumentException(val);
		}
	}

	public enum Multiply {
		CAPACITY,
		DEVICE,
		PRICE,
		AVAIL,
		ESSENCE,
		SIZE,
		RECOIL,
		MODIFIER,
		ARMOR,
	}

	@Attribute(required=true)
	private String id;
	@Attribute(required=false)
	private String customName;
	@Attribute
	protected ItemType type;
	@Attribute
	protected ItemSubType subtype;
	@Attribute(name="avail",required=false)
	@AttribConvert(AvailabilityConverter.class)
	private Availability availability;
	/* Price in Nuyen */
	@Attribute(required=true)
	private int cost;
	@Attribute(name="conceal")
	private int concealability;
	@Element(name="wifimods")
	private ModificationList wifiModifications;
	@Attribute(name="devrat")
	private int deviceRating;
	@Attribute(name="rate")
	private boolean rating;
	@Attribute(name="ratemul")
	@AttribConvert(MultiplyByRatingConverter.class)
	private Multiply[] multiplyWithRate;
	@Attribute(name="maxrat")
	private int maximumRating;
	@Attribute(name="minrat")
	private int minimumRating;
	@Attribute(name="modonly")
	private boolean selectableByModificationOnly;

	/*
	 * For items that have accessory slots
	 */

	@Element
	protected ModificationList modifications;
//	@Element(name="accessories")
//	private ItemList builtInAccessories;


	@Element(name="requires")
	private RequirementList requirements;
	@Element(name="accessory")
	private AccessoryData accessoryData;
	@Element(name="ammo")
	private AmmunitionData ammunitionData;
	@Element(name="armor")
	private ArmorData armorData;
	@Element(name="weapon")
	private WeaponData weaponData;
	@Element(name="cyberdeck")
	private CyberdeckData cyberdeckData;
	@Element(name="riggerconsole")
	private RiggerConsoleData riggerConsoleData;
	@Element(name="bodytech")
	private BodytechData bodyTech;
	@Element(name="vehicle")
	private VehicleData vehicle;

	//--------------------------------------------------------------------
	public ItemTemplate() {
//		typeData = new ArrayList<ItemTypeData>();
//		types    = new ArrayList<>();
//		materialType = MaterialType.OTHER;
		availability = new Availability(0, false);
		modifications= new ModificationList();
		requirements = new RequirementList();
//		builtInAccessories = new ItemList();
	}

	//--------------------------------------------------------------------
	public ItemTemplate(String id) {
		this();
		this.id = id;
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer(id+"(");
//		buf.append("ld="+load);
//		buf.append(",rob="+rigidity);
//		buf.append(",cplx="+complexity);
//		buf.append(",types="+typeData);
		buf.append(")");
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "item."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "item."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ItemTemplate) {
			ItemTemplate other = (ItemTemplate)o;
			if (!id.equals(other.getId())) return false;
			if (availability!=other.getAvailability()) return false;
			if (cost!=other.getPrice()) return false;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ItemTemplate other) {
//		int cmp = type.compareTo(other.getType());
//		if (cmp!=0) return cmp;

		return Collator.getInstance().compare(getName(), other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (i18n==null || customName!=null)
			return customName;
		try {
			return i18n.getString("item."+id);
		} catch (MissingResourceException e) {
			logger.warn("Missing "+e.getKey()+" in "+i18n.getBaseBundleName());
			if (MISSING!=null)
				MISSING.println(e.getKey()+"=");
			return "item."+id;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the availability
	 */
	public Availability getAvailability() {
		return availability;
	}

	//--------------------------------------------------------------------
	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Availability availability) {
		this.availability = availability;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public int getPrice() {
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.cost = price;
	}

	//--------------------------------------------------------------------
//	public void overwriteItemTypeData(ItemTypeData data) {
//		typeData.clear();
//		typeData.add(data);
//	}
//
//	//--------------------------------------------------------------------
//	public Collection<ItemTypeData> getTypeData() {
//		return new ArrayList<ItemTypeData>(typeData);
//	}
//
//	//--------------------------------------------------------------------
//	public boolean isType(ItemType type) {
//		if ( (typeData.isEmpty() && types.isEmpty()) && type == ItemType.OTHER) {
//			return true;
//		}
//		for (ItemTypeData data : typeData) {
//			if (data.getType() == type) {
//				return true;
//			}
//		}
//
//		return types.contains(type);
//	}
//
//	//--------------------------------------------------------------------
//	public ItemTypeData getType(ItemType type) {
//		for (ItemTypeData data : typeData)
//			if (data.getType()==type)
//				return data;
//		return null;
//	}
//
//	//--------------------------------------------------------------------
//	@SuppressWarnings("unchecked")
//	public <T extends ItemTypeData> T getType(Class<T> type) {
//		for (ItemTypeData data : typeData)
//			if (data.getClass()==type)
//				return (T)data;
//		return null;
//	}
//
//	//--------------------------------------------------------------------
//	public ItemType getFirstItemType() {
//		if (types.isEmpty())
//			return ItemType.OTHER;
//		return types.iterator().next();
//	}
//
//	//--------------------------------------------------------------------
//	public void addItemType(ItemType type) {
//		types.add(type);
//	}
//
//	//--------------------------------------------------------------------
//	public void replaceItemType(ItemType type) {
//		types.clear();
//		typeData.clear();
//		types.add(type);
//	}
//
//	//--------------------------------------------------------------------
//	public void addItemTypeData(ItemTypeData attributes) {
//		typeData.add(attributes);
//	}
//
//	//--------------------------------------------------------------------
//	public void add(Object value) {
//		if (value instanceof ItemType)
//			types.add((ItemType) value);
//		else if (value instanceof ItemTypeData)
//			typeData.add((ItemTypeData) value);
//		else
//			logger.error("Cannot add "+value.getClass());
//	}

	//-------------------------------------------------------------------
	public void setCustomName(String name) {
		this.customName = name;
		// Calculcate ID from name
		StringBuffer buf = new StringBuffer("custom");
		for (int i=0; i<name.length(); i++) {
			if (Character.isWhitespace(name.charAt(i)))
				continue;
			buf.append(name.charAt(i));
		}
		id = buf.toString();
	}

	//-------------------------------------------------------------------
	public boolean isCustom() {
		return id.startsWith("custom");
	}

	//-------------------------------------------------------------------
	public ItemType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

	//--------------------------------------------------------------------
	public void addSlot(ItemHookModification hook) {
		modifications.add(hook);
	}

	//--------------------------------------------------------------------
	public boolean hasSlot(ItemHook slot) {
		for (Modification mod : modifications) {
			if (mod instanceof ItemHookModification)
				if (((ItemHookModification) mod).getHook()==slot);
					return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public Collection<ItemHookModification> getSlots() {
		List<ItemHookModification> ret = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof ItemHookModification)
				ret.add((ItemHookModification) mod);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public Collection<String> getWiFiAdvantageStrings() {
		List<String> ret = new ArrayList<>();
		if (i18n.containsKey("item."+id+".wifi")) {
			StringTokenizer tok = new StringTokenizer(i18n.getString("item."+id+".wifi"),"\n");
			while (tok.hasMoreTokens())
				ret.add(tok.nextToken());
		}
//		for (ItemTemplate tmp : builtInAccessories) {
//			ret.addAll(tmp.getWiFiAdvantageStrings());
//		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the wifiModifications
	 */
	public ModificationList getWifiModifications() {
		return wifiModifications;
	}

	//-------------------------------------------------------------------
//	public Collection<ItemTemplate> getAccessories() {
//		return new ArrayList<ItemTemplate>(builtInAccessories);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the requirements
	 */
	public RequirementList getRequirements() {
		return requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @param requirements the requirements to set
	 */
	public void setRequirements(RequirementList requirements) {
		this.requirements = requirements;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public ModificationList getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the deviceRating
	 */
	public int getDeviceRating() {
		return deviceRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public boolean hasRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	public void setHasRating(boolean value) {
		rating = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multiplyWithRate
	 */
	public Multiply[] getMultiplyWithRate() {
		return multiplyWithRate;
	}

	//-------------------------------------------------------------------
	public boolean multipliesWithRate(Multiply val) {
		return Arrays.asList(multiplyWithRate).contains(val);
	}

	//-------------------------------------------------------------------
	public void setMultiplyWitRate(Multiply[] value) {
		multiplyWithRate = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the concealability
	 */
	public int getConcealability() {
		return concealability;
	}

	//-------------------------------------------------------------------
	/**
	 * @param concealability the concealability to set
	 */
	public void setConcealability(int concealability) {
		this.concealability = concealability;
	}

	//-------------------------------------------------------------------
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param subtype the subtype to set
	 */
	public void setSubtype(ItemSubType subtype) {
		this.subtype = subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @param deviceRating the deviceRating to set
	 */
	public void setDeviceRating(int deviceRating) {
		this.deviceRating = deviceRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the weaponData
	 */
	public WeaponData getWeaponData() {
		return weaponData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param weaponData the weaponData to set
	 */
	public void setWeaponData(WeaponData weaponData) {
		this.weaponData = weaponData;
	}

//	//-------------------------------------------------------------------
//	public ItemAttributeValue getAsValue(ItemAttribute attr) {
//		switch (attr) {
//		case ACCURACY:
//			if (getWeaponData()!=null)
//				return new ItemAttributeValue(ItemAttribute.ACCURACY, getWeaponData().getAccuracy(), getModifications());
//			logger.warn("Requesting ACCURACY for non-Weapon");
//			return null;
//		case AMMUNITION:
//			if (getWeaponData()!=null)
//				return new ItemAttributeValue(ItemAttribute.AMMUNITION, getWeaponData().getAmmunition(), getModifications());
//			logger.warn("Requesting AMMUNITION for non-Weapon");
//			return null;
//		case ARMOR_PENETRATION:
//			if (getWeaponData()!=null)
//				return new ItemAttributeValue(ItemAttribute.ARMOR_PENETRATION, getWeaponData().getArmorPenetration(), getModifications());
//			logger.warn("Requesting "+attr+" for non-Weapon");
//			return null;
//		case DAMAGE:
//			if (getWeaponData()!=null)
//				return new Damage(getWeaponData().getDamage(), getModifications());
//			logger.warn("Requesting "+attr+" for non-Weapon");
//			return null;
//		case REACH:
//			if (getWeaponData()!=null)
//				return new ItemAttributeValue(ItemAttribute.REACH, getWeaponData().getReach(), getModifications());
//			logger.warn("Requesting "+attr+" for non-MeleeWeapon");
//			return null;
//		case RECOIL_COMPENSATION:
//			if (getWeaponData()!=null)
//				return new ItemAttributeValue(ItemAttribute.RECOIL_COMPENSATION, getWeaponData().getRecoilCompensation(), getModifications());
//			logger.warn("Requesting "+attr+" for non-FireWeapon");
//			return null;
//		default:
//			throw new IllegalArgumentException("Use getAsObject for "+attr);
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public Object getAsObject(ItemAttribute attr) {
//		switch (attr) {
//		case MODE:
//			if (getWeaponData()!=null)
//				return getWeaponData().getFireModes();
//			logger.warn("Requesting "+attr+" for non-weapon");
//			return null;
//		default:
//			throw new IllegalArgumentException("Use getAsValue for "+attr);
//		}
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the accessoryData
	 */
	public AccessoryData getAccessoryData() {
		return accessoryData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessoryData the accessoryData to set
	 */
	public void setAccessoryData(AccessoryData accessoryData) {
		this.accessoryData = accessoryData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param modifications the modifications to set
	 */
	public void setModifications(ModificationList modifications) {
		this.modifications = modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the maximumRating
	 */
	public int getMaximumRating() {
		if (rating && maximumRating<2)
			return 2;
		return maximumRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param maximumRating the maximumRating to set
	 */
	public void setMaximumRating(int maximumRating) {
		this.maximumRating = maximumRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cyberdeckData
	 */
	public CyberdeckData getCyberdeckData() {
		return cyberdeckData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cyberdeckData the cyberdeckData to set
	 */
	public void setCyberdeckData(CyberdeckData cyberdeckData) {
		this.cyberdeckData = cyberdeckData;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the riggerConsoleData
	 */
	public RiggerConsoleData getRiggerConsoleData() {
		return riggerConsoleData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param riggerConsoleData the riggerConsoleData to set
	 */
	public void setRiggerConsoleData(RiggerConsoleData riggerConsoleData) {
		this.riggerConsoleData = riggerConsoleData;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the bodyTech
	 */
	public BodytechData getBodyTech() {
		return bodyTech;
	}

	//-------------------------------------------------------------------
	/**
	 * @param bodyTech the bodyTech to set
	 */
	public void setBodyTech(BodytechData bodyTech) {
		this.bodyTech = bodyTech;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the vehicle
	 */
	public VehicleData getVehicleData() {
		return vehicle;
	}

	//-------------------------------------------------------------------
	/**
	 * @param vehicle the vehicle to set
	 */
	public void setVehicleData(VehicleData vehicle) {
		this.vehicle = vehicle;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the ammunitionData
	 */
	public AmmunitionData getAmmunitionData() {
		return ammunitionData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammunitionData the ammunitionData to set
	 */
	public void setAmmunitionData(AmmunitionData ammunitionData) {
		this.ammunitionData = ammunitionData;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the armorData
	 */
	public ArmorData getArmorData() {
		return armorData;
	}

	//--------------------------------------------------------------------
	/**
	 * @param armorData the armorData to set
	 */
	public void setArmorData(ArmorData armorData) {
		this.armorData = armorData;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the minimumRating
	 */
	public int getMinimumRating() {
		return minimumRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param minimumRating the minimumRating to set
	 */
	public void setMinimumRating(int minimumRating) {
		this.minimumRating = minimumRating;
	}

	public boolean isSelectableByModificationOnly() {
		return selectableByModificationOnly;
	}

	public void setSelectableByModificationOnly(boolean selectableByModificationOnly) {
		this.selectableByModificationOnly = selectableByModificationOnly;
	}

}
