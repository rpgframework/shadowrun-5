/**
 *
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author prelle
 *
 */
public enum ItemType {

	ACCESSORY(
			ItemSubType.ACCESSORY, 
			ItemSubType.MODIFICATION,
			ItemSubType.VISION_ENHANCEMENT,
			ItemSubType.AUDIO_ENHANCEMENT,
			ItemSubType.SENSOR,
			ItemSubType.COMMLINK,
			ItemSubType.CYBERDECK,
			ItemSubType.CYBER_LIMB_ENHANCEMENT,
			ItemSubType.CYBER_LIMB_ACCESSORY
			),
	ARMOR(ItemSubType.ARMOR_BODY, ItemSubType.ARMOR_HELMET, ItemSubType.ARMOR_SHIELD),
	ARMOR_ADDITION,
	BIOWARE(
			ItemSubType.BIOWARE_COSMETICS, 
			ItemSubType.BIOWARE_STANDARD, 
			ItemSubType.BIOWARE_CULTURED,
			ItemSubType.BIOWARE_IMPLANT_WEAPON,
			ItemSubType.SYMBIONTS
			),
	CYBERWARE(
			ItemSubType.CYBER_COSMETICS,
			ItemSubType.CYBER_HEADWARE,
			ItemSubType.CYBER_EYEWARE,
			ItemSubType.CYBER_BODYWARE,
			ItemSubType.CYBER_EARWARE,
			ItemSubType.CYBER_IMPLANT_WEAPON,
			ItemSubType.CYBER_LIMBS
			),
	ELECTRONICS(
			ItemSubType.COMMLINK,
			ItemSubType.CYBERDECK,
			ItemSubType.ELECTRONIC_ACCESSORIES,
			ItemSubType.RIGGER_CONSOLE,
			ItemSubType.RFID,
			ItemSubType.COMMUNICATION,
			ItemSubType.SOFTWARE,
			ItemSubType.ID_CREDIT,
			ItemSubType.IMAGING,
			ItemSubType.OPTICAL,
			ItemSubType.AUDIO,
			ItemSubType.SENSOR_HOUSING,
			ItemSubType.SECURITY,
			ItemSubType.BREAKING
			),
	NANOWARE(
			ItemSubType.NANOWARE_HARD,
			ItemSubType.NANOWARE_SOFT,
			ItemSubType.NANO_CYBERWARE,
			ItemSubType.NANOTECH_EQUIPMENT
			),
	GENETICS(
			ItemSubType.GENOM_CHANGES,
			ItemSubType.EXOTIC_METAGENETICS,
			ItemSubType.TRANSGENETICS,
			ItemSubType.MICRO_ENVIRONADAPT,
			ItemSubType.COMPLEMENTAL_GENETICS
			),
	WEAPON(
			ItemSubType.CLOSE_COMBAT,
			ItemSubType.PROJECTILES,
			ItemSubType.TASERS_HOLDOUTS,
			ItemSubType.PISTOLS_LIGHT,
			ItemSubType.PISTOLS_HEAVY,
			ItemSubType.MACHINE_PISTOLS,
			ItemSubType.SUBMACHINE_GUNS,
			ItemSubType.ASSAULT_RIFLES,
			ItemSubType.SNIPER_RIFLES,
			ItemSubType.SHOTGUNS,
			ItemSubType.SPECIAL_WEAPONS,
			ItemSubType.MACHINE_GUNS,
			ItemSubType.CANNONS_LAUNCHERS
			),
	AMMUNITION(
			ItemSubType.FIREARMS,
			ItemSubType.GRENADES,
			ItemSubType.ROCKETS,
			ItemSubType.MISSILES
			),
	CHEMICALS(
			ItemSubType.INDUSTRIAL_CHEMICALS,
			ItemSubType.TOXINS,
			ItemSubType.DRUGS,
			ItemSubType.BTL
			),
	SURVIVAL(
			ItemSubType.SURVIVAL_GEAR,
			ItemSubType.GRAPPLE_GUN
			),
	BIOLOGY(
			ItemSubType.BIOTECH,
			ItemSubType.SLAP_PATCHES
			),
	VEHICLES(
			ItemSubType.BIKES,
			ItemSubType.CARS,
			ItemSubType.TRUCKS,
			ItemSubType.BOATS,
			ItemSubType.SUBMARINES,
			ItemSubType.FIXED_WING,
			ItemSubType.ROTORCRAFT,
			ItemSubType.VTOL
			),
	DRONES(
			ItemSubType.MICRODRONES,
			ItemSubType.MINIDRONES,
			ItemSubType.SMALL_DRONES,
			ItemSubType.MEDIUM_DRONES,
			ItemSubType.LARGE_DRONES
			),
	MAGICAL(
			ItemSubType.FOCI_ENCHANTING,
			ItemSubType.FOCI_METAMAGIC,
			ItemSubType.FOCI_POWER,
			ItemSubType.FOCI_QI,
			ItemSubType.FOCI_SPELL,
			ItemSubType.FOCI_SPIRIT,
			ItemSubType.FOCI_WEAPON,
			ItemSubType.MAGICAL_FORMULA,
			ItemSubType.MAGICAL_SUPPLIES
			),
    ;

	private ItemSubType[] subTypes;

	private ItemType(ItemSubType...data) {
		this.subTypes = data;
	}

    public String getName() {
        return ShadowrunCore.getI18nResources().getString("itemtype."+name().toLowerCase());
    }

    public ItemSubType[] getSubTypes() {
    	return subTypes;
    }

    public static ItemType[] bodytechTypes() {
    	return new ItemType[]{CYBERWARE,BIOWARE, NANOWARE, GENETICS};
    }

    public static ItemType[] gearTypes() {
    	return new ItemType[]{ARMOR,ELECTRONICS,BIOLOGY,CHEMICALS,SURVIVAL,AMMUNITION};
    }

    public static ItemType[] vehicleTypes() {
    	return new ItemType[]{VEHICLES,DRONES};
    }
}
