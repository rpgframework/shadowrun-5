/**
 *
 */
package org.prelle.shadowrun5.items;

import java.util.ArrayList;

import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public class LivingPersona extends CarriedItem {

	private ShadowrunCharacter model;

	//--------------------------------------------------------------------
	public LivingPersona(ShadowrunCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	@Override
	public String getName() {
		return ShadowrunCore.getI18nResources().getString("label.living_persona");
	}

	//--------------------------------------------------------------------
	@Override
	public boolean isType(ItemType type) {
		return type==ItemType.ELECTRONICS;
	}

	//--------------------------------------------------------------------
	public boolean isSubType(ItemSubType type) {
		return type==ItemSubType.LIVING_PERSONA;
	}

	//-------------------------------------------------------------------
	@Override
	public Object getAsObject(ItemAttribute attr) {
		switch (attr) {
		case CYBER_ATTRIBUTES:
			return null;
		default:
			return super.getAsValue(attr);
		}
	}

	//-------------------------------------------------------------------
	@Override
	public ItemAttributeValue getAsValue(ItemAttribute attr) {
		switch (attr) {
		case ATTACK:
			return new ItemAttributeValue(attr, model.getAttribute(Attribute.CHARISMA).getModifiedValue(), new ArrayList<>());
		case DATA_PROCESSING:
			return new ItemAttributeValue(attr, model.getAttribute(Attribute.LOGIC).getModifiedValue(), new ArrayList<>());
		case FIREWALL:
			return new ItemAttributeValue(attr, model.getAttribute(Attribute.WILLPOWER).getModifiedValue(), new ArrayList<>());
		case SLEAZE:
			return new ItemAttributeValue(attr, model.getAttribute(Attribute.INTUITION).getModifiedValue(), new ArrayList<>());
		case DEVICE_RATING:
			return new ItemAttributeValue(attr, model.getAttribute(Attribute.RESONANCE).getModifiedValue(), new ArrayList<>());
		case CONCURRENT_PROGRAMS:
			return new ItemAttributeValue(attr, 0, new ArrayList<>());
		default:
			return super.getAsValue(attr);
		}
	}

	@Override
	public ItemSubType getSubType() {
		return ItemSubType.LIVING_PERSONA;
	}
}
