/**
 * 
 */
package org.prelle.shadowrun5.items;

/**
 * @author prelle
 *
 */
public class OnRoadOffRoadValue {
	
	private int onRoad;
	private int offRoad;

	//-------------------------------------------------------------------
	public OnRoadOffRoadValue() {
	}

	//-------------------------------------------------------------------
	public void set(int val) {
		onRoad = val;
		offRoad= val;
	}

	//-------------------------------------------------------------------
	public void set(int on, int off) {
		onRoad = on;
		offRoad= off;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the onRoad
	 */
	public int getOnRoad() {
		return onRoad;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the offRoad
	 */
	public int getOffRoad() {
		return offRoad;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (onRoad==offRoad)
			return String.valueOf(onRoad);
		else
			return onRoad+"/"+offRoad;
	}

}
