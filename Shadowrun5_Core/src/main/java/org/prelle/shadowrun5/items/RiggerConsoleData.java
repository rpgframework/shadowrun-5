/**
 * 
 */
package org.prelle.shadowrun5.items;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class RiggerConsoleData {

	@Attribute(name="proc")
	private int dataProcessing;
	@Attribute(name="fire")
	private int firewall;

	//-------------------------------------------------------------------
	/**
	 * @return the dataProcessing
	 */
	public int getDataProcessing() {
		return dataProcessing;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the firewall
	 */
	public int getFirewall() {
		return firewall;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dataProcessing the dataProcessing to set
	 */
	public void setDataProcessing(int dataProcessing) {
		this.dataProcessing = dataProcessing;
	}

	//-------------------------------------------------------------------
	/**
	 * @param firewall the firewall to set
	 */
	public void setFirewall(int firewall) {
		this.firewall = firewall;
	}

}
