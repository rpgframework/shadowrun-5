/**
 * 
 */
package org.prelle.shadowrun5.items;

import org.prelle.shadowrun5.persist.OnRoadOffRoadConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="vehicle")
public class VehicleData {

	@Attribute(name="han")
	@AttribConvert(OnRoadOffRoadConverter.class)
	private OnRoadOffRoadValue handling;
	@Attribute(name="spd")
	@AttribConvert(OnRoadOffRoadConverter.class)
	private OnRoadOffRoadValue speed;
	@Attribute(name="acc")
	private int acceleration;
	@Attribute(name="bod")
	private int body;
	@Attribute(name="arm")
	private int armor;
	@Attribute(name="pil")
	private int pilot;
	@Attribute(name="sen")
	private int sensor;
	@Attribute(name="sea")
	private int seats;
	
	//-------------------------------------------------------------------
	public VehicleData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the handling
	 */
	public OnRoadOffRoadValue getHandling() {
		return handling;
	}

	//-------------------------------------------------------------------
	/**
	 * @param handling the handling to set
	 */
	public void setHandling(OnRoadOffRoadValue handling) {
		this.handling = handling;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the speed
	 */
	public OnRoadOffRoadValue getSpeed() {
		return speed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(OnRoadOffRoadValue speed) {
		this.speed = speed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the acceleration
	 */
	public int getAcceleration() {
		return acceleration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param acceleration the acceleration to set
	 */
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the body
	 */
	public int getBody() {
		return body;
	}

	//-------------------------------------------------------------------
	/**
	 * @param body the body to set
	 */
	public void setBody(int body) {
		this.body = body;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the armor
	 */
	public int getArmor() {
		return armor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param armor the armor to set
	 */
	public void setArmor(int armor) {
		this.armor = armor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the pilot
	 */
	public int getPilot() {
		return pilot;
	}

	//-------------------------------------------------------------------
	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(int pilot) {
		this.pilot = pilot;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sensor
	 */
	public int getSensor() {
		return sensor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sensor the sensor to set
	 */
	public void setSensor(int sensor) {
		this.sensor = sensor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the seats
	 */
	public int getSeats() {
		return seats;
	}

	//-------------------------------------------------------------------
	/**
	 * @param seats the seats to set
	 */
	public void setSeats(int seats) {
		this.seats = seats;
	}

}
