/**
 *
 */
package org.prelle.shadowrun5.items;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.persist.AmmunitionConverter;
import org.prelle.shadowrun5.persist.FireModesConverter;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.shadowrun5.persist.SkillSpecializationConverter;
import org.prelle.shadowrun5.persist.WeaponDamageConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class WeaponData extends ModifyableImpl {

	private transient ItemTemplate parent;
	@Attribute
	@AttribConvert(value=SkillConverter.class)
	private Skill skill;
	@Attribute
	@AttribConvert(value=SkillSpecializationConverter.class)
	private SkillSpecialization spec;
	@Attribute(name="acc")
	private int accuracy;

	@Attribute(name="dmg")
	@AttribConvert(WeaponDamageConverter.class)
	protected Damage damage;
	@Attribute(name="ap")
	private int armorPenetration;
	// Firearms
	@Attribute(name="rc")
	private int recoilCompensation;
	@Attribute(name="mode")
	@AttribConvert(FireModesConverter.class)
	private List<FireMode> mode;
	@Attribute(name="ammo")
	@AttribConvert(AmmunitionConverter.class)
	private List<AmmunitionSlot> ammo;
	// Melee
	@Attribute
	private int reach;


	//-------------------------------------------------------------------
	/**
	 */
	public WeaponData() {
		ammo = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (parent!=null)
			return parent.getName();
		return "?";
	}

	//--------------------------------------------------------------------
	/**
	 * Convenience for getSkillSpecialization().getSkill()
	 * @return the skill
	 */
	public Skill getSkill() {
		if (skill!=null)
			return skill;
		return spec.getSkill();
	}

	//--------------------------------------------------------------------
	public int getAccuracy() {
		return accuracy;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public Damage getDamage() {
		return damage;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the armorPenetration
	 */
	public int getArmorPenetration() {
		return armorPenetration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the spec
	 */
	public SkillSpecialization getSpecialization() {
		return spec;
	}

	//-------------------------------------------------------------------
	/**
	 * @param spec the spec to set
	 */
	public void setSpecialization(SkillSpecialization spec) {
		if (spec!=null)
			this.skill = spec.getSkill();
		this.spec = spec;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param accuracy the accuracy to set
	 */
	public void setAccuracy(int accuracy) {
		this.accuracy = accuracy;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(Damage damage) {
		this.damage = damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @param armorPenetration the armorPenetration to set
	 */
	public void setArmorPenetration(int armorPenetration) {
		this.armorPenetration = armorPenetration;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the recoilCompensation
	 */
	public int getRecoilCompensation() {
		return recoilCompensation;
	}

//	//--------------------------------------------------------------------
//	public ItemAttributeValue getRecoilCompensation(List<Modification> mods) {
//		return new ItemAttributeValue(ItemAttribute.RECOIL_COMPENSATION, recoilCompensation, mods);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the mode
	 */
	public List<FireMode> getFireModes() {
		return mode;
	}

	//-------------------------------------------------------------------
	public List<String> getFireModeNames() {
		List<String> ret = new ArrayList<String>();
		if (mode!=null) {
		for (FireMode tmp : mode)
			ret.add(tmp.getName());
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public List<AmmunitionSlot> getAmmunition() {
		return ammo;
	}

//	//--------------------------------------------------------------------
//	public ItemAttributeValue getAmmunition(List<Modification> mods) {
//		return new ItemAttributeValue(ItemAttribute.AMMUNITION, ammo, mods);
//	}

	//--------------------------------------------------------------------
	public int getReach() {
		return reach;
	}

//	//--------------------------------------------------------------------
//	public ItemAttributeValue getReach(List<Modification> mods) {
//		return new ItemAttributeValue(ItemAttribute.REACH, reach, mods);
//	}

	//--------------------------------------------------------------------
	public boolean isMeleeWeapon() {
		return skill!=null && (
				skill.getId().equals("unarmed")
				|| skill.getId().equals("blades")
				|| skill.getId().equals("clubs")
				|| skill.getId().equals("exoticClose")
				);
	}

	//--------------------------------------------------------------------
	public boolean isRangedWeapon() {
		return skill!=null && !isMeleeWeapon();
	}

	//-------------------------------------------------------------------
	/**
	 * @param recoilCompensation the recoilCompensation to set
	 */
	public void setRecoilCompensation(int recoilCompensation) {
		this.recoilCompensation = recoilCompensation;
	}

	//-------------------------------------------------------------------
	/**
	 * @param mode the mode to set
	 */
	public void setFireModes(List<FireMode> mode) {
		this.mode = mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(AmmunitionSlot ammo) {
		this.ammo = Collections.singletonList(ammo);
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(List<AmmunitionSlot> ammo) {
		this.ammo = ammo;
	}

	//-------------------------------------------------------------------
	/**
	 * @param reach the reach to set
	 */
	public void setReach(int reach) {
		this.reach = reach;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the parent
	 */
	public ItemTemplate getParent() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @param parent the parent to set
	 */
	public void setParent(ItemTemplate parent) {
		this.parent = parent;
	}

}
