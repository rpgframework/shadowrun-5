package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class AttributeModification extends ModificationBase<Attribute> {

	public enum ModificationType {
		RELATIVE,
		ABSOLUTE,
	}

	@org.prelle.simplepersist.Attribute
	private ModificationValueType type;
	@org.prelle.simplepersist.Attribute
	private ModificationType modType;
	@org.prelle.simplepersist.Attribute
    private Attribute attr;
	@org.prelle.simplepersist.Attribute
    private int val;
	@org.prelle.simplepersist.Attribute(name="cond",required=false)
    private Boolean conditional = null;

    //-----------------------------------------------------------------------
    public AttributeModification() {
        type = ModificationValueType.CURRENT;
        modType = ModificationType.RELATIVE;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(Attribute attr, int val) {
        type = ModificationValueType.CURRENT;
        modType = ModificationType.RELATIVE;
        this.attr = attr;
        this.val  = val;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(ModificationValueType type, Attribute attr, int val) {
        this.type = type;
        modType = ModificationType.RELATIVE;
        this.attr = attr;
        this.val  = val;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(ModificationValueType type, Attribute attr, int val, ModificationType mType) {
        this.type = type;
        modType   = mType;
        this.attr = attr;
        this.val  = val;
    }

    //-------------------------------------------------------------------
    /**
     * @see org.prelle.shadowrun5.modifications.ModificationBase#clone()
     */
    @Override
	public ValueModification<Attribute> clone() {
   		return new AttributeModification(type, attr, val, modType);
     }

    //-----------------------------------------------------------------------
    public String toString() {
        if (modType==ModificationType.RELATIVE)
            return attr+"/"+type+((val<0)?(" "+val):(" +"+val))+" for "+getExpCost()+" karma";
        if (attr==null)
        	return "ATTR_NOT_SET";
        return attr.getName()+" = "+val;
    }

    //-----------------------------------------------------------------------
    public ModificationValueType getType() {
        return type;
    }

    //-----------------------------------------------------------------------
    public void setType(ModificationValueType type) {
        this.type = type;
    }

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public Attribute getModifiedItem() {
		return attr;
	}

    //-----------------------------------------------------------------------
    public Attribute getAttribute() {
        return attr;
    }

    //-----------------------------------------------------------------------
    public void setAttribute(Attribute attr) {
        this.attr = attr;
    }

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getAttribute()!=attr) return false;
            if (amod.getExpCost()  !=expCost) return false;
            if (amod.getSource()   !=source) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof AttributeModification))
            return toString().compareTo(obj.toString());
        AttributeModification other = (AttributeModification)obj;
        if (attr!=other.getAttribute())
            return (Integer.valueOf(attr.ordinal())).compareTo(Integer.valueOf(other.getAttribute().ordinal()));
        return (Integer.valueOf(type.ordinal()).compareTo(Integer.valueOf(other.getType().ordinal())));
    }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		if (conditional==null) return false;
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modType
	 */
	public ModificationType getModificationType() {
		return modType;
	}

	//-------------------------------------------------------------------
	/**
	 * @param modType the modType to set
	 */
	public void setModificationType(ModificationType modType) {
		this.modType = modType;
	}

}// AttributeModification
