package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.persist.ItemConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="itemmod")
public class CarriedItemModification implements Modification {

	@Attribute
	@AttribConvert(ItemConverter.class)
    private ItemTemplate item;
	private transient Object source;
	@Attribute
    private int rating;
	@Attribute(name="remove")
    private boolean remove;

    //-----------------------------------------------------------------------
    public CarriedItemModification() {
    }

    //-----------------------------------------------------------------------
    public CarriedItemModification(ItemTemplate item, int rating, Object source, boolean remove) {
        this.item = item;
        this.rating = rating;
        this.source = source;
        this.remove = remove;
    }

    //-----------------------------------------------------------------------
    public String toString() {
           return "Add item "+item;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof CarriedItemModification) {
            CarriedItemModification amod = (CarriedItemModification)o;
            return (amod.getItem()==item);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof CarriedItemModification) {
            CarriedItemModification amod = (CarriedItemModification)o;
            if (amod.getItem()!=item) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof CarriedItemModification))
            return toString().compareTo(obj.toString());
        CarriedItemModification other = (CarriedItemModification)obj;
         return item.compareTo(other.getItem());
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#clone()
	 */
	@Override
	public Modification clone() {
		return new CarriedItemModification(item, rating, source, remove);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.modifications.ModificationBase#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemTemplate getItem() {
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the remove
	 */
	public boolean isRemove() {
		return remove;
	}

	//-------------------------------------------------------------------
	/**
	 * @param remove the remove to set
	 */
	public void setRemove(boolean remove) {
		this.remove = remove;
	}


}//
