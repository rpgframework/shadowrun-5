/**
 *
 */
package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.persist.ComplexFormConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ComplexFormModification implements Modification {

	@Attribute
	@AttribConvert(ComplexFormConverter.class)
	private ComplexForm spell;

	@org.prelle.simplepersist.Attribute
	protected int expCost;
	@org.prelle.simplepersist.Attribute
	protected Date date;

	private transient Object source;

	//-------------------------------------------------------------------
	public ComplexFormModification() {
	}

	//-------------------------------------------------------------------
	public ComplexFormModification(ComplexForm type, Object src, Date date) {
		this.spell = type;
		this.source= src;
		this.date  = date;
	}

	//-------------------------------------------------------------------
	public String toString() {
			return "ComplexForm "+spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new ComplexFormModification(spell, source, date);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
		this.expCost = expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	public ComplexForm getComplexForm() {
		return spell;
	}

}
