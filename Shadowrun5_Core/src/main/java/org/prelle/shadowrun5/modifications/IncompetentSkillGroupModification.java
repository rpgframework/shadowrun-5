package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.persist.SkillGroupConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class IncompetentSkillGroupModification implements Modification {

	@Attribute(required=false)
	@AttribConvert(value=SkillGroupConverter.class)
	private SkillGroup ref;
	private transient Object source;

    //-----------------------------------------------------------------------
    public IncompetentSkillGroupModification() {
    }

    //-----------------------------------------------------------------------
    public IncompetentSkillGroupModification(SkillGroup ref, Object source) {
        this.ref = ref;
        this.source = source;
    }

    //-----------------------------------------------------------------------
    public String toString() {
         if (ref==null)
        	return "SKILLGRP_NOT_SET";
       	 return "Incompetence in "+ref;
    }

    //-----------------------------------------------------------------------
    public SkillGroup getSkillGroup() {
        return ref;
    }

    //-----------------------------------------------------------------------
    public void setSkillGroup(SkillGroup grp) {
        this.ref = grp;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof IncompetentSkillGroupModification) {
            IncompetentSkillGroupModification amod = (IncompetentSkillGroupModification)o;
            return(amod.getSkillGroup()    !=ref) ;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new IncompetentSkillGroupModification(ref, source);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

}
