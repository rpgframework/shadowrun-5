package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author prelle
 *
 */
public class ItemAttributeModification extends ModificationBase<ItemAttribute> {

	@Attribute
    private ItemAttribute attr;
	@Attribute
    private int val;
	@Attribute(name="cond",required=false)
    private Boolean conditional = null;

    //-----------------------------------------------------------------------
    public ItemAttributeModification() {
    }

    //-----------------------------------------------------------------------
    public ItemAttributeModification(ItemAttribute attr, int val) {
        this.attr = attr;
        this.val  = val;
    }

    //-------------------------------------------------------------------
    /**
     * @see org.prelle.shadowrun5.modifications.ModificationBase#clone()
     */
    @Override
	public ValueModification<ItemAttribute> clone() {
   		return new ItemAttributeModification(attr, val);
     }

    //-----------------------------------------------------------------------
    public String toString() {
           return attr+"/"+((val<0)?(" "+val):(" +"+val));
    }

    //-----------------------------------------------------------------------
    public ItemAttribute getAttribute() {
        return attr;
    }

    //-----------------------------------------------------------------------
    public void setAttribute(ItemAttribute attr) {
        this.attr = attr;
    }

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public ItemAttribute getModifiedItem() {
		return attr;
	}

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemAttributeModification))
            return toString().compareTo(obj.toString());
        ItemAttributeModification other = (ItemAttributeModification)obj;
        if (attr!=other.getAttribute())
            return (Integer.valueOf(attr.ordinal())).compareTo(Integer.valueOf(other.getAttribute().ordinal()));
        return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

}// ItemAttributeModification
