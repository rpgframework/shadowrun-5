/**
 * 
 */
package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class LifestyleCostModification extends ModifyableImpl implements Modification {
	
	@Attribute
	private int percent;
	@Attribute
	private int fixed;
	protected transient Object source;

	//-------------------------------------------------------------------
	public LifestyleCostModification() {
	}

	//-------------------------------------------------------------------
	public LifestyleCostModification(int percent, int fixed, Object source) {
		this.percent = percent;
		this.fixed   = fixed;
		this.source  = source;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "LifeStyleMod(+fixed="+fixed+"  percent="+percent+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the percent
	 */
	public int getPercent() {
		return percent;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the fixed
	 */
	public int getFixed() {
		return fixed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Modification clone() {
		return new LifestyleCostModification(percent, fixed, source);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

}
