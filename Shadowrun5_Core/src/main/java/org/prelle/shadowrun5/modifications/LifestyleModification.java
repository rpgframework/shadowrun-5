/**
 * 
 */
package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class LifestyleModification extends ModifyableImpl implements Modification {
	
	public enum LifestyleAttribute {
		POINTS,
		COMFORT,
		SECURITY,
		NEIGHBORHOOD
		;

	    //-------------------------------------------------------------------
	    public String getName() {
	        return ShadowrunCore.getI18nResources().getString("lifestyleattribute."+this.name().toLowerCase());
	    }
		
	}
	
	@Attribute(name="attr",required=true)
	private LifestyleAttribute attribute;
	@Attribute(required=true)
	private int value;

	//-------------------------------------------------------------------
	public LifestyleModification(LifestyleAttribute attribute, int val) {
		this.attribute = attribute;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public LifestyleModification() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the points
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the attribute
	 */
	public LifestyleAttribute getAttribute() {
		return attribute;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Modification clone() {
		return new LifestyleModification(attribute, value);
	}

	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSource(Object src) {
		// TODO Auto-generated method stub
		
	}

}
