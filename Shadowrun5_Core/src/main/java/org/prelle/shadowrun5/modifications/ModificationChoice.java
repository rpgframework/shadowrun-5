package org.prelle.shadowrun5.modifications;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;


@SuppressWarnings("serial")
@Root(name = "selmod")
@ElementListUnion({
    @ElementList(entry="attrmod", type=AttributeModification.class),
    @ElementList(entry="itemattrmod", type=ItemAttributeModification.class),
    @ElementList(entry="optionmod", type=OptionModification.class),
    @ElementList(entry="skillgrpmod", type=SkillGroupModification.class),
    @ElementList(entry="skillmod", type=SkillModification.class),
 })
public class ModificationChoice extends ArrayList<Modification> implements Modification {

	@Attribute(name="num",required=false)
    protected int numberOfChoices;
	private transient Object source;

    //-----------------------------------------------------------------------
    public ModificationChoice() {
        numberOfChoices = 1;
    }

    //-----------------------------------------------------------------------
    public ModificationChoice(int noOfChoices) {
        this.numberOfChoices = noOfChoices;
    }

    //-----------------------------------------------------------------------
    public ModificationChoice(List<Modification> mods, int val) {
    	addAll(mods);
        this.numberOfChoices = val;
   }

    //-----------------------------------------------------------------------
    public ModificationChoice(Modification... mods) {
        for (Modification tmp : mods)
        	add(tmp);
        this.numberOfChoices = 1;
    }

    //-----------------------------------------------------------------------
    public boolean equals(Object o) {
        if (o instanceof ModificationChoice) {
            ModificationChoice mc = (ModificationChoice)o;
            if (numberOfChoices!=mc.getNumberOfChoices()) return false;
//            Modification[] otherOpt = mc.getOptions();
//            if (otherOpt.length!=mods.size())
//                return false;
//            for (int i=0; i<mods.size(); i++)
//                if (!mods.get(i).equals(otherOpt[i]))
//                    return false;
//            return true;
            return super.equals(mc.getOptionList());
        }
        return false;
    }

    //-----------------------------------------------------------------------
    public Modification[] getOptions() {
        Modification[] modArray = new Modification[size()];
        modArray = (Modification[])toArray(modArray);
        return modArray;
    }

    //-----------------------------------------------------------------------
    public List<Modification> getOptionList() {
    	return new ArrayList<Modification>(this);
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	StringBuffer buf = null;
    		buf = new StringBuffer("Select "+numberOfChoices+" of (");

    		Iterator<Modification> it = iterator();
    		while (it.hasNext()) {
    			buf.append(it.next().toString());
    			if (it.hasNext())
    				buf.append("|");
    		}

        buf.append(")");
        return buf.toString();
    }

    //------------------------------------------------
    /*
     * @see org.prelle.dsatool.modifications.Modification#clone()
     */
    public ModificationChoice clone() {
    	ModificationChoice ret = new ModificationChoice(numberOfChoices);
    	ret.setSource(getSource());
    	ret.addAll(this);
    	return ret;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification arg0) {
        return 0;
    }

    //-------------------------------------------------------
    /**
     * @return Returns the numberOfChoices.
     */
   public int getNumberOfChoices() {
        return numberOfChoices;
    }

    //--------------------------------------------------------
    /**
     * @param numberOfChoices The numberOfChoices to set.
     */
    public void setNumberOfChoices(int numberOfChoices) {
        this.numberOfChoices = numberOfChoices;
    }

	@Override
	public Date getDate() {
		return null;
	}

	@Override
	public void setDate(Date date) {
	}

	@Override
	public int getExpCost() {
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public void setSource(Object src) {
		this.source  = src;
	}

}// ModificationChoice
