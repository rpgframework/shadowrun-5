/**
 *
 */
package org.prelle.shadowrun5.modifications;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="modifications")
@ElementListUnion({
    @ElementList(entry="accessorymod", type=AccessoryModification.class),
    @ElementList(entry="addlstylemod", type=AddLifestyleModification.class),
    @ElementList(entry="attrmod", type=AttributeModification.class),
    @ElementList(entry="cformmod", type=ComplexFormModification.class),
    @ElementList(entry="incompmod", type=IncompetentSkillGroupModification.class),
    @ElementList(entry="itemmod", type=CarriedItemModification.class),
    @ElementList(entry="itemattrmod", type=ItemAttributeModification.class),
    @ElementList(entry="itemhookmod", type=ItemHookModification.class),
    @ElementList(entry="lifecostmod", type=LifestyleCostModification.class),
    @ElementList(entry="lifestylemod", type=LifestyleModification.class),
    @ElementList(entry="nuyenmod", type=AddNuyenModification.class),
    @ElementList(entry="optionmod", type=OptionModification.class),
    @ElementList(entry="qualitymod", type=QualityModification.class),
    @ElementList(entry="ritualmod", type=RitualModification.class),
    @ElementList(entry="selmod", type=ModificationChoice.class),
    @ElementList(entry="selskillmod", type=SelectSkillModification.class),
    @ElementList(entry="sinmod", type=SINModification.class),
    @ElementList(entry="skillgrpmod", type=SkillGroupModification.class),
    @ElementList(entry="skillmod", type=SkillModification.class),
    @ElementList(entry="skillspecmod", type=SkillSpecializationModification.class),
    @ElementList(entry="specrulemod", type=SpecialRuleModification.class),
    @ElementList(entry="spellmod", type=SpellModification.class),
 })
public class ModificationList extends ArrayList<Modification> {

	//-------------------------------------------------------------------
	public ModificationList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ModificationList(Collection<? extends Modification> c) {
		super(c);
		}


}
