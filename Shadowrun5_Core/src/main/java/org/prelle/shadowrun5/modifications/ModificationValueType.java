/**
 * 
 */
package org.prelle.shadowrun5.modifications;

/**
 * @author prelle
 *
 */
public enum ModificationValueType {

	CURRENT,
	MAX,
	ENHANCED_MAX
	
}
