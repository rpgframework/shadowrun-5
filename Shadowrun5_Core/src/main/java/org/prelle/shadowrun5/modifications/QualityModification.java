/**
 * 
 */
package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.persist.QualityConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class QualityModification extends ModificationBase<Quality> {

	@Attribute
	@AttribConvert(QualityConverter.class)
	private Quality ref;
	@Attribute
	private int val;
	@Attribute
	private boolean remove;

	//-------------------------------------------------------------------
	public QualityModification() {
	}

	//-------------------------------------------------------------------
	public QualityModification(Quality data) {
		this.ref = data;
	}

	//-------------------------------------------------------------------
	public QualityModification(Quality data, int val) {
		this.ref = data;
		this.val = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (remove)
			return "Remove "+ref+" for "+super.getExpCost()+" karma";
		return "Add "+ref+" for "+super.getExpCost()+" karma";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.val = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the remove
	 */
	public boolean isRemove() {
		return remove;
	}

	//-------------------------------------------------------------------
	/**
	 * @param remove the remove to set
	 */
	public void setRemove(boolean remove) {
		this.remove = remove;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public Quality getModifiedItem() {
		return ref;
	}

}
