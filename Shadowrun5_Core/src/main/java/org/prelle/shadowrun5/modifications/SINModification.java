/**
 * 
 */
package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.SIN;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SINModification implements Modification {
	
	@Attribute
	private SIN.Quality quality;
	@Attribute
	private boolean criminal;
	protected Date date;
	protected transient Object source;

	//-------------------------------------------------------------------
	public SINModification() {
	}

	//-------------------------------------------------------------------
	public SINModification(SIN.Quality quality, boolean criminal, Object source) {
		this.quality = quality;
		this.criminal = criminal;
		this.source  = source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new SINModification(quality, criminal, source);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public SIN.Quality getQuality() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the criminal
	 */
	public boolean isCriminal() {
		return criminal;
	}

}
