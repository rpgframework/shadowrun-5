package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SelectSkillModification extends ModificationBase<Skill> {

	@Attribute
	private SkillType type;
	@Attribute(required=false)
	private int val;
	@Attribute(name="cond")
	private boolean conditional;
	@org.prelle.simplepersist.Attribute
	private ModificationValueType modType;

    
    //-----------------------------------------------------------------------
    public SelectSkillModification() {
        type = null;
        modType = ModificationValueType.CURRENT;
    }
    
    //-----------------------------------------------------------------------
    public SelectSkillModification(SkillType type, int val) {
        this();
        this.type = type;
        this.val  = val;
        modType = ModificationValueType.CURRENT;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
         if (type==null)
             return "Selected skill +"+val;
          return "Selected skill of type "+type+" +"+val;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SelectSkillModification) {
            SelectSkillModification amod = (SelectSkillModification)o;
            if (amod.getType()     !=type) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof SelectSkillModification) {
            SelectSkillModification amod = (SelectSkillModification)o;
            if (amod.getType()     !=type) return false;
             return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modType
	 */
	public ModificationValueType getModificationType() {
		return modType;
	}
    
}// AttributeModification
