package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.persist.SkillGroupConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillGroupModification extends ModificationBase<SkillGroup> {

	@Attribute(required=true)
	@AttribConvert(value=SkillGroupConverter.class)
	private SkillGroup ref;
	@Attribute(required=false)
	private int val;
	@Attribute(name="cond")
	private boolean conditional;

    //-----------------------------------------------------------------------
    public SkillGroupModification() {
    }

    //-----------------------------------------------------------------------
    public SkillGroupModification(SkillGroup ref, int val) {
        this();
        this.ref = ref;
        this.val  = val;
        super.modifiedItem = ref;
    }

    //-----------------------------------------------------------------------
    public String toString() {
         if (ref==null)
        	return "SKILLGRP_NOT_SET";
       	 return ref+" = "+val;
    }

    //-----------------------------------------------------------------------
    public SkillGroup getSkillGroup() {
        return ref;
    }

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillGroupModification) {
            SkillGroupModification amod = (SkillGroupModification)o;
            if (amod.getSkillGroup()    !=ref) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

}
