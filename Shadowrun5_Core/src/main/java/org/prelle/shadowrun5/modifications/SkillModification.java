package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.Skill.SkillType;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillModification extends ModificationBase<Skill> {

	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill ref;
	@Attribute
	private SkillType type;
	@Attribute(required=false)
	private int val;
	@Attribute(name="cond")
	private boolean conditional;
	@org.prelle.simplepersist.Attribute
	private ModificationValueType modType;

    
    //-----------------------------------------------------------------------
    public SkillModification() {
        type = null;
        modType = ModificationValueType.CURRENT;
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(Skill ref, int val) {
        this();
        this.ref = ref;
        this.val  = val;
        modType = ModificationValueType.CURRENT;
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(Skill ref, int val, Date date, int expCost) {
        this();
        this.ref = ref;
        this.val  = val;
        this.date = date;
        this.expCost = expCost;
        modType = ModificationValueType.CURRENT;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
         if (ref==null && type==null)
        	return "SKILL_NOT_SET";
         if (ref!=null)
        	 return ref.getName()+" = "+val+" ("+modType+")";
         return "Any "+type+" skill +"+val;
    }
    
    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return ref;
    }
    
    //-----------------------------------------------------------------------
    public void setSkill(Skill val) {
        this.ref = val;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillModification) {
            SkillModification amod = (SkillModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getSkill()    !=ref) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof SkillModification) {
            SkillModification amod = (SkillModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getSkill()    !=ref) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public SkillType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modType
	 */
	public ModificationValueType getModificationType() {
		return modType;
	}
    
}// AttributeModification
