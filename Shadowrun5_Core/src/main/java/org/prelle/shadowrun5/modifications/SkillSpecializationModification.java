package org.prelle.shadowrun5.modifications;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillSpecialization;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.shadowrun5.persist.SkillSpecializationConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="skillspecmod")
public class SkillSpecializationModification extends ModificationBase<SkillSpecialization> {

	@Attribute
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute
	@AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization ref;

    //-----------------------------------------------------------------------
    public SkillSpecializationModification() {
    }

    //-----------------------------------------------------------------------
    public SkillSpecializationModification(Skill skill, SkillSpecialization ref) {
    	this.skill = skill;
        this.ref = ref;
    }

    //-----------------------------------------------------------------------
    public String toString() {
         return "spec="+ref;
    }

    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return skill;
    }

    //-----------------------------------------------------------------------
    public void setSkill(Skill val) {
        this.skill = val;
    }

    //-----------------------------------------------------------------------
    public SkillSpecialization getSkillSpecialization() {
        return ref;
    }

    //-----------------------------------------------------------------------
    public void setSkillSpecialization(SkillSpecialization val) {
        this.ref = val;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillSpecializationModification) {
            SkillSpecializationModification amod = (SkillSpecializationModification)o;
            if (amod.getSkillSpecialization()     !=ref) return false;
            if (amod.getSkill()    !=skill) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());

}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int arg0) {
	}

}