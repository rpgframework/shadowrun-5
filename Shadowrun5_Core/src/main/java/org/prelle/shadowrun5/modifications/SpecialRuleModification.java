/**
 * 
 */
package org.prelle.shadowrun5.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="specrulemod")
public class SpecialRuleModification implements Modification {
	
	public enum Rule {
		/**
		 * high_pain_tolerance / Hohe Schmerztoleranz
		 * Pool reduction by damage is 1 point later
		 */
		PAIN_TOLERANCE_HIGH,
		/**
		 * low_pain_tolerance / Niedrige Schmerztoleranz
		 * Pool reduction for every 2 points of damage
		 */
		PAIN_TOLERANCE_LOW,
		/**
		 * will_to_live / Überlebenswille
		 * Fields for Additional damage below 0
		 */
		DAMAGE_OVERFLOW,
		/** 
		 * uncouth / Ungehobelt
		 * Social skills cost twice as much
		 * */
		SOCIAL_COST_TWICE,
		/** 
		 * technical_school_education / Abschluss Beruffachschule (Run faster)
		 * For every spent point in knowledge skill, get 2 points
		 * */
		KNOWLEDGE_2_FOR_1_GEN,
		/** How many karma points can be converted to nuyen on generation */
		KARMA_TO_NUYEN,
		/** On generation academic knowledge at half price */
		SKILL_ACADEMIC_COST_REDUCED,
		/** On generation: Additional CH*4 points for connections, Rating at least 8 */
		FRIENDS_IN_HIGH_PLACES,
		/** (Levelling only) Reduce karma costs for skills up to 5 by 1, increase by 2 above 5 */
		JACK_OF_ALL_TRADES,
		/** On generation languages at half price. On levelling save 1 karma, if above 2 */
		SKILL_LANGUAGE_COST_REDUCED,
		/** On generation languages at half price. On levelling save 1 karma, if above 2 */
		SKILL_STREETLORE_COST_REDUCED,
	}
	
	@Attribute
	private Rule ref;
	@Attribute(name="lvl")
	private int level;
	
	private transient Object source;

	//-------------------------------------------------------------------
	public SpecialRuleModification() {
	}

	//-------------------------------------------------------------------
	public SpecialRuleModification(Rule type) {
		this.ref = type;
	}

	//-------------------------------------------------------------------
	public SpecialRuleModification(Rule type, int val) {
		this.ref = type;
		this.level = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
			return "Rule "+ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new SpecialRuleModification(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	public Rule getRule() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}
