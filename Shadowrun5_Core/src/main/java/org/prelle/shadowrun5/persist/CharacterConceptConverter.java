package org.prelle.shadowrun5.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.simplepersist.StringValueConverter;

public class CharacterConceptConverter implements StringValueConverter<CharacterConcept> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CharacterConcept read(String v) throws Exception {
		CharacterConcept data = ShadowrunCore.getCharacterConcept(v);
		if (data==null) {
			logger.error("No such CharacterConcept: "+v);
			throw new IllegalArgumentException("No such CharacterConcept: "+v);
		}
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(CharacterConcept v) throws Exception {
		return v.getId();
	}
	
}