/**
 *
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.ComplexForm;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class ComplexFormConverter implements StringValueConverter<ComplexForm> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ComplexForm value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ComplexForm read(String idref) throws Exception {
		ComplexForm data = ShadowrunCore.getComplexForm(idref);
		if (data==null)
			throw new ReferenceException(ReferenceType.COMPLEXFORM, idref);

		return data;
	}

}
