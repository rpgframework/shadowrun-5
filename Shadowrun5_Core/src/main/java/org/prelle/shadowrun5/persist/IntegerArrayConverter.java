/**
 * 
 */
package org.prelle.shadowrun5.persist;

import java.util.StringTokenizer;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class IntegerArrayConverter implements StringValueConverter<int[]> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(int[] value) throws Exception {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<value.length; i++)
			buf.append(value[i]+" ");
		return buf.toString().trim();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public int[] read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v);
		int[] ret = new int[tok.countTokens()];
		for (int i=0; i<ret.length; i++) {
			ret[i] = Integer.parseInt(tok.nextToken());
		}
		return ret;
	}

}
