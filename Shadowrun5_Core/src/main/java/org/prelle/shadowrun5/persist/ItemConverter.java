package org.prelle.shadowrun5.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class ItemConverter implements StringValueConverter<ItemTemplate> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ItemTemplate read(String v) throws Exception {
		ItemTemplate item = ShadowrunCore.getItem(v);
		if (item==null) {
			logger.error("Unknown item reference: '"+v+"'");
			throw new ReferenceException(ReferenceType.ITEM, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ItemTemplate v) throws Exception {
		return v.getId();
	}
	
}