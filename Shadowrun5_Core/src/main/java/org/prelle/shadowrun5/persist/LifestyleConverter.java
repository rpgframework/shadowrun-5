package org.prelle.shadowrun5.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Lifestyle;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class LifestyleConverter implements StringValueConverter<Lifestyle> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Lifestyle read(String v) throws Exception {
		Lifestyle item = ShadowrunCore.getLifestyle(v);
		if (item==null) {
			logger.error("Unknown item reference: '"+v+"'");
			throw new ReferenceException(ReferenceType.LIFESTYLE_OPTION, v);
		}
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Lifestyle v) throws Exception {
		return v.getId();
	}
	
}