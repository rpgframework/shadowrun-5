/**
 * 
 */
package org.prelle.shadowrun5.persist;

import java.util.StringTokenizer;

import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.ItemTemplate.Multiply;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class MultiplyByRatingConverter implements StringValueConverter<ItemTemplate.Multiply[]> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Multiply[] value) throws Exception {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<value.length; i++)
			buf.append(value[i].name()+" ");
		return buf.toString().trim();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Multiply[] read(String v) throws Exception {
		StringTokenizer tok = new StringTokenizer(v);
		Multiply[] ret = new Multiply[tok.countTokens()];
		for (int i=0; i<ret.length; i++) {
			ret[i] = Multiply.valueOf(tok.nextToken());
		}
		return ret;
	}

}
