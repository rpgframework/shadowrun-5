/**
 * 
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.Program;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class ProgramConverter implements StringValueConverter<Program> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Program value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Program read(String idref) throws Exception {
		Program data = ShadowrunCore.getProgram(idref);
		if (data==null)
			throw new ReferenceException(ReferenceType.PROGRAM, idref);

		return data;
	}

}
