/**
 * 
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class QualityConverter implements StringValueConverter<Quality> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Quality value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Quality read(String idref) throws Exception {
		Quality skill = ShadowrunCore.getQuality(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.QUALITY, idref);

		return skill;
	}

}
