/**
 *
 */
package org.prelle.shadowrun5.persist;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class ReferenceException extends RuntimeException {

	public enum ReferenceType {
		ADEPTPOWER,
		COMPLEXFORM,
		ITEM,
		MAGIC, // Magic or resonance
		METATYPE,
		QUALITY,
		LIFESTYLE,
		LIFESTYLE_OPTION,
		PROGRAM,
		RITUAL,
		RITUALFEATURE,
		SKILL,
		SKILLGROUP,
		SKILLSPECIALIZATION,
		SPELL,
		SPELLFEATURE,
		SUMMONABLE,
		TRADITION,
	}

	private ReferenceType type;
	private String reference;

	//--------------------------------------------------------------------
	public ReferenceException(ReferenceType type, String ref) {
		super("Invalid reference to "+type+" '"+ref+"'");
		this.type = type;
		this.reference = ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ReferenceType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

}
