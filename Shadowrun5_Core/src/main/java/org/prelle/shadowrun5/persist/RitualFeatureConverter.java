/**
 * 
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.RitualFeature;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class RitualFeatureConverter implements StringValueConverter<RitualFeature> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(RitualFeature value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public RitualFeature read(String idref) throws Exception {
		RitualFeature skill = ShadowrunCore.getRitualFeature(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.RITUALFEATURE, idref);

		return skill;
	}

}
