/**
 * 
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class SkillConverter implements StringValueConverter<Skill> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Skill value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Skill read(String idref) throws Exception {
		Skill skill = ShadowrunCore.getSkill(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.SKILL, idref);

		return skill;
	}

}
