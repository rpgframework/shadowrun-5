/**
 * 
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.SkillGroup;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class SkillGroupConverter implements StringValueConverter<SkillGroup> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(SkillGroup value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public SkillGroup read(String idref) throws Exception {
		SkillGroup skill = ShadowrunCore.getSkillGroup(idref);
		if (skill==null)
			throw new ReferenceException(ReferenceType.SKILLGROUP, idref);

		return skill;
	}

}
