/**
 *
 */
package org.prelle.shadowrun5.persist;

import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Summonable;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class SummonableConverter implements StringValueConverter<Summonable> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Summonable value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Summonable read(String idref) throws Exception {
		Summonable  ret = ShadowrunCore.getSpirit(idref);
		if (ret==null)
			ret = ShadowrunCore.getSprite(idref);
		if (ret==null)
			throw new ReferenceException(ReferenceType.SUMMONABLE, idref);

		return ret;
	}

}
