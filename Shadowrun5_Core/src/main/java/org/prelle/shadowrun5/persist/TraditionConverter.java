package org.prelle.shadowrun5.persist;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Tradition;
import org.prelle.shadowrun5.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

public class TraditionConverter implements StringValueConverter<Tradition> {
	
	private final static Logger logger = LogManager.getLogger("shadowrun.persist");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Tradition read(String idref) throws Exception {
		Tradition data = ShadowrunCore.getTradition(idref);
		if (data==null) {
			logger.error("No such Tradition: "+idref);
			throw new ReferenceException(ReferenceType.TRADITION, idref);
		}
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Tradition v) throws Exception {
		return v.getId();
	}
	
}