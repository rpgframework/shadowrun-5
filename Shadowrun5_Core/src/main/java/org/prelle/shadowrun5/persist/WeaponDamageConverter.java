package org.prelle.shadowrun5.persist;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.prelle.shadowrun5.items.Damage;
import org.prelle.shadowrun5.items.Damage.Type;
import org.prelle.shadowrun5.items.Damage.WeaponDamageType;
import org.prelle.simplepersist.StringValueConverter;

public class WeaponDamageConverter implements StringValueConverter<Damage> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Damage read(String v) throws Exception {
		v = v.trim();

		Damage ret = new Damage();
		if (v.endsWith("(e)")) {
			ret.setWeaponDamageType(WeaponDamageType.ELECTRICAL);
			v = v.substring(0, v.length()-3).trim();
		}
		if (v.endsWith("(f)")) {
			ret.setWeaponDamageType(WeaponDamageType.FLECHETTE);
			v = v.substring(0, v.length()-3).trim();
		}

		if (v.endsWith("P"))
			ret.setType(Type.PHYSICAL);
		else if (v.endsWith("S"))
			ret.setType(Type.STUN);
		v = v.substring(0, v.length()-1);

		if (v.startsWith("(")) {
			StringTokenizer tok = new StringTokenizer(v, "()+ ");
			tok.nextToken(); // STR
			ret.setAddStrength(true);
			if (!tok.hasMoreTokens())
				throw new NoSuchElementException("Missing second token in '"+v+"'");
			ret.setValue(Integer.parseInt(tok.nextToken()));
		} else {
			ret.setValue(Integer.parseInt(v));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Damage v) throws Exception {
		if (v==null)
			return null;
		StringBuffer buf = new StringBuffer();
		if (v.addStrength()) {
			buf.append("(STR+"+v.getValue()+")");
		} else {
			buf.append(String.valueOf(v.getValue()));
		}
		if (v.getType()==Type.PHYSICAL)
			buf.append("P");
		else
			buf.append("S");

		if (v.getWeaponDamageType()!=null && v.getWeaponDamageType()==WeaponDamageType.ELECTRICAL) {
			buf.append("(e)");
		}
		return buf.toString();
	}

}