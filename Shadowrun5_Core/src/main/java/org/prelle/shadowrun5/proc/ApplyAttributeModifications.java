/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.modifications.AttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyAttributeModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public ApplyAttributeModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process AttributeModifications
			for (Modification tmp : previous) {
				if (tmp instanceof AttributeModification) {
					logger.debug("Apply "+tmp);
					model.getAttribute( ((AttributeModification)tmp).getAttribute() ).addModification(tmp);
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
