/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.modifications.CarriedItemModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyCarriedItemModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public ApplyCarriedItemModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process CarriedItemModification
			for (Modification tmp : previous) {
				if (tmp instanceof CarriedItemModification) {
					logger.info("Apply "+tmp);
					CarriedItemModification mod = (CarriedItemModification)tmp;
					if (mod.isRemove()) {
						for (CarriedItem item : model.getItems(false)) {
							if (item.getItem()==mod.getItem()) {
								logger.info("  Remove "+item);
							}
						}
					} else {
						// Add
						CarriedItem toAdd = new CarriedItem(mod.getItem());
						if (mod.getItem().hasRating())
							toAdd.setRating(mod.getRating());
						model.addAutoItem(toAdd);
//						ShadowrunTools.equip(model, toAdd);
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
