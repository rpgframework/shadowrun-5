/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SIN.Quality;
import org.prelle.shadowrun5.modifications.SINModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplySINModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public ApplySINModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Sort SINMods before MemorizeUUIDMods
			Collections.sort(previous, new Comparator<Modification>() {
				public int compare(Modification o1, Modification o2) {
					if (o1 instanceof MemorizeUUIDModification)
						return +1;
					if (o1 instanceof SINModification)
						return -1;
					return 0;
				}
			});
			
			for (Modification tmp : previous) {
				if (tmp instanceof SINModification) {
					SINModification mod = (SINModification)tmp;
					SIN sin = new SIN(mod.getQuality());
					if (sin.getQuality()==Quality.REAL_SIN)
						sin.setName(model.getRealName());
					model.addSIN(sin);
					logger.info("Created "+sin.getQuality().name()+" SIN "+sin);
				} else if (tmp instanceof MemorizeUUIDModification) {
					MemorizeUUIDModification mod = (MemorizeUUIDModification)tmp;
					if (mod.getType()==MemorizeUUIDModification.Type.SIN) {
						boolean found = false;
						for (SIN sin : model.getSINs()) {
							if (sin.getQuality()==Quality.REAL_SIN) {
								sin.setUniqueId(mod.getUUID());
								logger.info("  reapply old UUID to REAL_SIN: "+mod.getUUID());
								found = true;
							}
						}
						if (!found) {
							logger.warn("Did not find REAL_SIN to apply old UUID to");
							unprocessed.add(mod);
						}
					} else {
						unprocessed.add(tmp);						
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
