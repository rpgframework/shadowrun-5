/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;
import org.prelle.shadowrun5.modifications.SkillGroupModification;
import org.prelle.shadowrun5.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplySkillModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public ApplySkillModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process AttributeModifications
			for (Modification tmp : previous) {
				if (tmp instanceof SkillModification) {
					SkillValue sVal = model.getSkillValue( ((SkillModification)tmp).getSkill() );
					if (sVal!=null) {
						logger.info("Apply "+tmp+" to skill "+sVal);
						sVal.addModification(tmp);
					} else {
						logger.warn("Cannot apply "+tmp+", since skill does not exist in character");						
					}
				} else if (tmp instanceof SkillGroupModification) {
					SkillGroupValue sgVal = model.getSkillGroupValue( ((SkillGroupModification)tmp).getSkillGroup() );
					if (sgVal!=null) {
						logger.info("  Apply "+tmp+" to skill group "+sgVal);
						sgVal.addModification(tmp);
					} else {
						logger.warn("Have SkillGroupModification, but char does not have skill group");
						for (Skill skill : ShadowrunCore.getSkills(((SkillGroupModification)tmp).getSkillGroup())) {
							SkillValue sVal = model.getSkillValue( skill );
							if (sVal!=null) {
								logger.info("  Apply group modification "+tmp+" to skill "+sVal);
								sVal.addModification(tmp);
							} else {
								logger.warn("  character does not have skill "+skill+" to modify with "+tmp);
							}
						}
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
