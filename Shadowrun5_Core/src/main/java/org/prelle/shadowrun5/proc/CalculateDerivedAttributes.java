/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunTools;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateDerivedAttributes implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.gen.proc");

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
//		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Set character back to zero
//			logger.debug("1. Calculate derived attributes");
			ShadowrunTools.calculateDerived(model);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
