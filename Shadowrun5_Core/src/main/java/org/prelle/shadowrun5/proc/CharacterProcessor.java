/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.List;

import org.prelle.shadowrun5.ShadowrunCharacter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public interface CharacterProcessor {

	/**
	 * @param unprocessed Unprocessed modifications from previous steps
	 * @return Unprocessed modification
	 */
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed);
	
}
