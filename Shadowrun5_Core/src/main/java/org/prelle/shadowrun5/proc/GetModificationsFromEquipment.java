/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromEquipment implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public GetModificationsFromEquipment() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Apply modifications by qualities
//			logger.debug("2. Apply modifications from qualities");
			for (CarriedItem ref :model.getItems(false)) {
				logger.info("ITEM "+ref+" / "+ref.getCharacterModifications());
				if (ref.getModifications()!=null && !ref.getCharacterModifications().isEmpty()) {
					logger.info(" - "+ref.getItem().getId()+" has modifications: "+ref.getCharacterModifications());
					unprocessed.addAll(ref.getCharacterModifications());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
