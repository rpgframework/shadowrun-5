/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.ShadowrunCharacter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMetaType implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.proc");
	
	//-------------------------------------------------------------------
	public GetModificationsFromMetaType() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Apply modifications by metatype
			if (model.getMetatype()!=null) {
				logger.debug("1. Apply modifications from metatype "+model.getMetatype().getId()+" = "+model.getMetatype().getModifications());
				unprocessed.addAll(model.getMetatype().getModifications());
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
