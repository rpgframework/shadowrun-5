/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.Quality;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.modifications.QualityModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromQualities implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun.gen");
	
	//-------------------------------------------------------------------
	public GetModificationsFromQualities() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			for (Modification tmp : previous) {
				if (tmp instanceof QualityModification) {
					Quality data = ((QualityModification)tmp).getModifiedItem();
					QualityValue ref = new QualityValue(data, 1);
					if (data.needsChoice()) {
						logger.warn("   TODO: implement choices");
					}
					logger.info("Add quality "+data.getId()+" from "+tmp.getSource());
					model.addRacialQuality(ref);
				} else
					unprocessed.add(tmp);
			}
			
			
			// Apply modifications by qualities
//			logger.debug("2. Apply modifications from qualities");
			for (QualityValue ref :model.getQualities()) {
//				logger.info("QUAL "+ref);
				if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
					logger.debug(" - "+ref.getModifyable().getId()+" has modifications");
					unprocessed.addAll(ref.getModifications());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}
