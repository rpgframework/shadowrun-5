/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.Date;
import java.util.UUID;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class MemorizeUUIDModification implements Modification {
	
	public enum Type {
		SIN
	}
	
	private Type type;
	private UUID uuid;

	//-------------------------------------------------------------------
	/**
	 */
	public MemorizeUUIDModification(Type type, UUID uuid) {
		this.type = type;
		this.uuid = uuid;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new MemorizeUUIDModification(type, uuid);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
	}

	//-------------------------------------------------------------------
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public UUID getUUID() {
		return uuid;
	}

}
