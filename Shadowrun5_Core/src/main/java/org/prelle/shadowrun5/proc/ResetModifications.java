/**
 * 
 */
package org.prelle.shadowrun5.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.proc.MemorizeUUIDModification.Type;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.QualityValue;
import org.prelle.shadowrun5.SIN;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.SkillGroupValue;
import org.prelle.shadowrun5.SkillValue;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ResetModifications implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun.proc");
	
	private UUID realSINUUID;

	//-------------------------------------------------------------------
	private void clearCharacter(ShadowrunCharacter model) {
		// Reset attributes
		for (Attribute key : Attribute.values()) {
			AttributeValue val = model.getAttribute(key);
			if (val!=null) {
				val.clearModifications();
			}
		}
		for (Attribute key : new Attribute[] {Attribute.INITIATIVE_DICE_PHYSICAL, Attribute.INITIATIVE_DICE_ASTRAL, Attribute.INITIATIVE_DICE_MATRIX})
			model.getAttribute(key).setPoints(1);
		
		// Reset skills
		for (SkillValue val : model.getSkillValues(false)) {
			if (val!=null) {
				val.clearModifications();
			}
		}
		
		// Reset skillgroup values
		for (SkillGroupValue val : model.getSkillGroupValues()) {
			if (val!=null) {
				val.clearModifications();
			}
		}
		
		// Reset qualities
		for (QualityValue ref : model.getSystemSelectedQualities()) {
			model.removeRacialQuality(ref);
		}
		
		// Remove SINs given by modifications
		for (SIN ref : new ArrayList<SIN>(model.getSINs())) {
			if (ref.isCriminal() || ref.getQuality()==SIN.Quality.REAL_SIN) {
				if (ref.getUniqueId()!=null)
					realSINUUID = ref.getUniqueId();
				model.removeSIN(ref);
			}
		}
	}

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Set character back to zero
			clearCharacter(model);
			if (realSINUUID!=null)
				unprocessed.add(new MemorizeUUIDModification(Type.SIN, realSINUUID));
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}
	
}
