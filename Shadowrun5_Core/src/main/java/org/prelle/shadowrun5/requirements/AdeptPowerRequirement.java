package org.prelle.shadowrun5.requirements;

import org.prelle.shadowrun5.AdeptPower;
import org.prelle.shadowrun5.persist.AdeptPowerConverter;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "powerreq")
public class AdeptPowerRequirement extends Requirement {

	@Attribute(name="ref")
	private String id;
    private transient AdeptPower resolved;
   
    //-----------------------------------------------------------------------
    public AdeptPowerRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public AdeptPowerRequirement(AdeptPower attr) {
        this.resolved = attr;
        this.id  = attr.getId();
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return id;
    }
 
    //-----------------------------------------------------------------------
    public AdeptPower getAdeptPower() {
    	if (resolved!=null)
    		return resolved;

    	if (resolve())
    		return resolved;
    	return null;
    }
    
    //-----------------------------------------------------------------------
    public void setAdeptPower(AdeptPower attr) {
        this.resolved = attr;
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new AdeptPowerRequirement(resolved);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AdeptPowerRequirement) {
            AdeptPowerRequirement amod = (AdeptPowerRequirement)o;
            return (amod.getAdeptPower()==resolved);
        } else
            return false;
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.requirements.Requirement#resolve()
	 */
	@Override
	public boolean resolve() {
		if (resolved!=null)
			return true;
		if (id==null)
			return false;

		try {
			resolved = (new AdeptPowerConverter()).read(id);
			return true;
		} catch (Exception e) {
			logger.error(e);
		}

		return false;
	}
 
}
