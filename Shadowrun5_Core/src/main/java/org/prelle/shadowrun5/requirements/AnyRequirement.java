package org.prelle.shadowrun5.requirements;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;


public class AnyRequirement extends Requirement {
    
	@ElementListUnion({
	    @ElementList(entry="attrreq"   , type=AttributeRequirement.class),
	    @ElementList(entry="itemreq"   , type=ItemRequirement.class),
	    @ElementList(entry="itemtypereq"   , type=ItemTypeRequirement.class),
	    @ElementList(entry="itemsubtypereq"   , type=ItemSubTypeRequirement.class),
	    @ElementList(entry="metareq"   , type=MetatypeRequirement.class),
	    @ElementList(entry="qualreq"   , type=QualityRequirement.class),
	    @ElementList(entry="skillreq"  , type=SkillRequirement.class),
	    @ElementList(entry="slotreq"   , type=ItemHookRequirement.class),
	 })
    protected List<Requirement> optionList;
    
    //-----------------------------------------------------------------------
    public AnyRequirement() {
        optionList   = new ArrayList<Requirement>();
    }
    
    //-----------------------------------------------------------------------
    public AnyRequirement(List<Requirement> mods) {
        this.optionList   = mods;
   }
    
    //-----------------------------------------------------------------------
    public AnyRequirement(Requirement... mods) {
        this.optionList   = new ArrayList<Requirement>();
        for (Requirement tmp : mods)
        	optionList.add(tmp);
    }
    
    //-----------------------------------------------------------------------
    public void add(Requirement mod) {
        if (!optionList.contains(mod)) {
            optionList.add(mod);
        }
    }
    
    //-----------------------------------------------------------------------
    public void add(Object mod) {
        if (!optionList.contains(mod) && mod instanceof Requirement) {
            optionList.add((Requirement)mod);
        }
    }
   
    //-----------------------------------------------------------------------
    public void remove(Requirement mod) {
        optionList.remove(mod);
    }
    
    //-----------------------------------------------------------------------
    public boolean equals(Object o) {
        if (o instanceof AnyRequirement) {
            AnyRequirement mc = (AnyRequirement)o;
            return optionList.equals(mc.getOptionList());
        }
        return false;
    }
    
    //-----------------------------------------------------------------------
    public Requirement[] getOptions() {
        Requirement[] modArray = new Requirement[optionList.size()];
        modArray = (Requirement[]) optionList.toArray(modArray);
        return modArray;
    }
    
    //-----------------------------------------------------------------------
    public List<Requirement> getOptionList() {
    	return new ArrayList<Requirement>(optionList);
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        StringBuffer buf = new StringBuffer("Require any of (");
        
        Iterator<Requirement> it = optionList.iterator();
        while (it.hasNext()) {
            buf.append(it.next().toString());
            if (it.hasNext())
                buf.append("|");
        }
        
        buf.append(")");
        return buf.toString();
    }
    
}
