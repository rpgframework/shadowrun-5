/**
 * 
 */
package org.prelle.shadowrun5.requirements;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun5.items.ItemSubType;
import org.prelle.shadowrun5.persist.ItemSubTypesConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="itemsubtypereq")
public class ItemSubTypeRequirement extends Requirement {
	
	/**
	 * One of these types must be met
	 */
	@Attribute
	@AttribConvert(ItemSubTypesConverter.class)
	private List<ItemSubType> type;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemSubTypeRequirement() {
		type = new ArrayList<ItemSubType>();
	}

	//-------------------------------------------------------------------
	/**
	 * One of these types must be met
	 * @return the slot
	 */
	public List<ItemSubType> getType() {
		return type;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Needs subtype of "+type;
	}

}
