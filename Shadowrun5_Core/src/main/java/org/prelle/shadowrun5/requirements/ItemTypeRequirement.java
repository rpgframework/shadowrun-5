/**
 * 
 */
package org.prelle.shadowrun5.requirements;

import org.prelle.shadowrun5.items.ItemType;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class ItemTypeRequirement extends Requirement {
	
	@Attribute
	private ItemType type;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemTypeRequirement() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemType getType() {
		return type;
	}

}
