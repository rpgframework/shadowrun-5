package org.prelle.shadowrun5.requirements;

import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.persist.MetaTypeConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "metareq")
public class MetatypeRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
	@AttribConvert(MetaTypeConverter.class)
    private MetaType ref;
   
    //-----------------------------------------------------------------------
    public MetatypeRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public MetatypeRequirement(MetaType attr) {
        this.ref = attr;
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return ref+"";
    }
 
    //-----------------------------------------------------------------------
    public MetaType getMetaType() {
        return ref;
    }
    
    //-----------------------------------------------------------------------
    public void setMetaType(MetaType attr) {
        this.ref = attr;
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new MetatypeRequirement(ref);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof MetatypeRequirement) {
            MetatypeRequirement amod = (MetatypeRequirement)o;
            return (amod.getMetaType()==ref);
        } else
            return false;
    }
   
}
