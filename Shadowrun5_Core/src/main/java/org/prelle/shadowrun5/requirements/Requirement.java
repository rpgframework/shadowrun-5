package org.prelle.shadowrun5.requirements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public abstract class Requirement {

	protected final static Logger logger = LogManager.getLogger("shadowrun.req");

	@Attribute
    private boolean negated;

	//--------------------------------------------------------------------
	public Requirement() {
	}

	public boolean isNegated() {
		return negated;
	}

	public void setNegated(boolean negated) {
		this.negated = negated;
	}

	//--------------------------------------------------------------------
	/**
	 * Called when loading data in a second pass to resolve references
	 * to other data just loaded
	 * @return TRUE, when resolution was successfull
	 */
	public boolean resolve() {
		return true;
	}

}
