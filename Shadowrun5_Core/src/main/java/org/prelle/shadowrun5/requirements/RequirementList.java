/**
 * 
 */
package org.prelle.shadowrun5.requirements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="requires")
@ElementListUnion({
    @ElementList(entry="anyreq"    , type=AnyRequirement.class),
    @ElementList(entry="attrreq"   , type=AttributeRequirement.class),
    @ElementList(entry="itemreq"   , type=ItemRequirement.class),
    @ElementList(entry="itemtypereq"   , type=ItemTypeRequirement.class),
    @ElementList(entry="itemsubtypereq"   , type=ItemSubTypeRequirement.class),
    @ElementList(entry="metareq"   , type=MetatypeRequirement.class),
    @ElementList(entry="powerreq"  , type=AdeptPowerRequirement.class),
    @ElementList(entry="qualreq"   , type=QualityRequirement.class),
    @ElementList(entry="skillreq"  , type=SkillRequirement.class),
    @ElementList(entry="slotreq"   , type=ItemHookRequirement.class),
 })
public class RequirementList extends ArrayList<Requirement> {

	private static final long serialVersionUID = 1L;

	//-------------------------------------------------------------------
	/**
	 */
	public RequirementList() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public RequirementList(Collection<? extends Requirement> c) {
		super(c);
	}

	//-------------------------------------------------------------------
	public List<Requirement> getRequirements() {
		return this;
	}
}
