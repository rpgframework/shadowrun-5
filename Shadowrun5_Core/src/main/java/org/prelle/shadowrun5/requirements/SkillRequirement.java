package org.prelle.shadowrun5.requirements;

import org.prelle.shadowrun5.Skill;
import org.prelle.shadowrun5.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "skillreq")
public class SkillRequirement extends Requirement {

	@org.prelle.simplepersist.Attribute
	@AttribConvert(SkillConverter.class)
    private Skill skill;
	@org.prelle.simplepersist.Attribute
    private int val;
    
    //-----------------------------------------------------------------------
    public SkillRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public SkillRequirement(Skill attr, int val) {
        this.skill = attr;
        this.val  = val;
    }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return skill+" "+val;
    }
 
    //-----------------------------------------------------------------------
    public Skill getSkill() {
        return skill;
    }
    
    //-----------------------------------------------------------------------
    public void setSkill(Skill attr) {
        this.skill = attr;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
    //-----------------------------------------------------------------------
    public Object clone() {
        return new SkillRequirement(skill, val);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillRequirement) {
            SkillRequirement amod = (SkillRequirement)o;
            if (amod.getSkill()!=skill) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }
   
}
