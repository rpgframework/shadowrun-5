/**
 * 
 */
package foo;

import org.prelle.shadowrun5.CharacterConcept;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class SRCoreLoader {

	//-------------------------------------------------------------------
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>(RoleplayingSystem.SHADOWRUN, "CORE"));
		
		for (CharacterConcept c : ShadowrunCore.getCharacterConcepts())
			System.out.println("+ "+c);
	}

}
