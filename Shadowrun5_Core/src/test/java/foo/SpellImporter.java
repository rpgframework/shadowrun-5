/**
 * 
 */
package foo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.DamageType;
import org.prelle.shadowrun5.DummyRulePlugin;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;
import org.prelle.shadowrun5.Spell;
import org.prelle.shadowrun5.SpellFeature;
import org.prelle.shadowrun5.SpellFeatureReference;
import org.prelle.shadowrun5.SpellList;
import org.prelle.shadowrun5.Spell.Category;
import org.prelle.shadowrun5.Spell.Duration;
import org.prelle.shadowrun5.Spell.Range;
import org.prelle.shadowrun5.Spell.Type;
import org.prelle.simplepersist.Persister;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class SpellImporter {
	
	private final static Logger logger = LogManager.getLogger("import");

	//-------------------------------------------------------------------
	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws IOException {
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>(RoleplayingSystem.SHADOWRUN, "CORE"));
		
		new SpellImporter();
	}

	Map<String,List<String>> pros;
	Map<String,SpellList> list;
	Persister serializer;

	//-------------------------------------------------------------------
	public SpellImporter() throws IOException {
		pros = new HashMap<String, List<String>>();
		list = new HashMap<>();
		serializer = new Persister();
		
		process("rawdata/Heilzauber.csv", Spell.Category.HEALTH);
		process("rawdata/Illusionszauber.csv", Spell.Category.ILLUSION);
		process("rawdata/Kampfzauber.csv", Spell.Category.COMBAT);
		process("rawdata/Manipulationszauber.csv", Spell.Category.MANIPULATION);
		process("rawdata/Wahrnehmungszauber.csv", Spell.Category.DETECTION);
		
		for (String key : pros.keySet()) {
			PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(key+".properties", false), "ISO-8859-1"));
			for (String line : pros.get(key))
				out.println(line);
			out.close();
			
			serializer.write(list.get(key), new FileWriter(key+".xml"));
		}
	}

	//-------------------------------------------------------------------
	public void process(String file, Category category) throws IOException {
		System.out.println(file);
		
		BufferedReader in = new BufferedReader(new FileReader(file));
		// Skip first line
		in.readLine();
		do {
			String line = in.readLine();
			if (line==null || line.length()==0) 
				break;
			StringTokenizer tok = new StringTokenizer(line, ";");
			String id = tok.nextToken().trim();
			String name = tok.nextToken().trim();

			String val  = tok.nextToken().trim();
			Spell.Type type = null;
			if (val.equalsIgnoreCase("M")) type = Type.MANA;
			if (val.equalsIgnoreCase("P")) type = Type.PHYSICAL;
			if (type==null)
				logger.warn("Type missing in "+name);

			val  = tok.nextToken().trim();
			Spell.Range range = null;
			if (val.equalsIgnoreCase("B")) range = Range.TOUCH;
			if (val.equalsIgnoreCase("BF")) range = Range.LINE_OF_SIGHT;
			if (val.equalsIgnoreCase("BF (F)")) range = Range.LINE_OF_SIGHT_AREA;
			if (val.equalsIgnoreCase("S")) range = Range.SELF;
			if (val.equalsIgnoreCase("S (F)")) range = Range.SELF_AREA;
			if (range==null)
				logger.warn("Range missing in "+name+"  ("+val+")");

			val  = tok.nextToken().trim();
			Spell.Duration duration = null;
			if (val.equalsIgnoreCase("A")) duration = Duration.SUSTAINED;
			if (val.equalsIgnoreCase("S")) duration = Duration.INSTANTANEOUS;
			if (val.equalsIgnoreCase("P")) duration = Duration.PERMANENT;
			if (val.equalsIgnoreCase("sp.")) duration = Duration.SPECIAL;
			if (duration==null)
				logger.warn("Duration missing in "+name);

			val  = tok.nextToken().trim();
			int cost = Integer.MAX_VALUE;
			if (val.startsWith("KS - 6")) cost = -6;
			if (val.startsWith("KS - 5")) cost = -5;
			if (val.startsWith("KS - 4")) cost = -4;
			if (val.startsWith("KS - 3")) cost = -3;
			if (val.startsWith("KS - 2")) cost = -2;
			if (val.startsWith("KS - 1")) cost = -1;
			if (val.equals("KS")) cost = 0;
			if (val.startsWith("KS + 1")) cost = +1;
			if (val.startsWith("KS + 2")) cost = +2;
			if (val.startsWith("KS + 3")) cost = +3;
			if (val.startsWith("KS + 4")) cost = +4;
			if (val.startsWith("KS + 5")) cost = +5;
			if (cost==Integer.MAX_VALUE)
				logger.warn("Cost missing in "+name);
			
			List<SpellFeatureReference> features = new ArrayList<>();
			DamageType dmgType = null;
			String featureString = tok.nextToken().trim();
			StringTokenizer tok2 = new StringTokenizer(featureString,",-");
			outer:
			while (tok2.hasMoreTokens()) {
				String featName = tok2.nextToken().trim();
				for (SpellFeature feat : ShadowrunCore.getSpellFeatures()) {
					if (feat.getName().equalsIgnoreCase(featName)) {
						features.add(new SpellFeatureReference(feat));
						continue outer;
					}
				}
				if (featName.equals("Schaden K")) { dmgType=DamageType.PHYSICAL; continue outer; }
				if (featName.equals("Schaden G")) { dmgType=DamageType.MENTAL; continue outer; }
				if (featName.equals("Schaden speziell")) { dmgType=DamageType.SPECIAL; continue outer; }
				if (featName.equals("Ungerichtet")) { features.add(new SpellFeatureReference(ShadowrunCore.getSpellFeature("area"))); continue outer; }
				logger.warn("Unknown feature '"+featName+"'");
			}
			
			val = tok.nextToken().trim();
			tok2 = new StringTokenizer(val," ");
			String key = tok2.nextToken();
			int page = Integer.parseInt(tok2.nextToken());
			
			SpellList spellList = list.get(key);
			List<String> pro = pros.get(key);
			if (spellList==null) {
				spellList = new SpellList();
				pro = new ArrayList<String>();
				list.put(key, spellList);
				pros.put(key, pro);
			}
			
			pro.add("spell."+id+"="+name);
			pro.add("spell."+id+".page="+page);
			
			Spell spell = new Spell(id);
			spell.setCategory(category);
			spell.setDrain(cost);
			spell.setDuration(duration);
			spell.setType(type);
			spell.setRange(range);
			spell.setDamage(dmgType);
			if (!features.isEmpty())
				spell.setFeatures(features);
			spellList.add(spell);
			
			logger.debug(name+" \t"+range);
		} while (true);
		in.close();
	}

}
