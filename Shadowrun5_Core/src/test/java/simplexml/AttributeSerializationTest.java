/**
 * 
 */
package simplexml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.AttributeValue;
import org.prelle.shadowrun5.Attributes;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class AttributeSerializationTest {

	final static String DATA = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<attr id=\"AGILITY\" start=\"3\" value=\"3\"/>\n";
	final static String DATA2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+"<attributes>\n"
			+"   <attr id=\"BODY\" value=\"0\"/>\n"
			+"   <attr id=\"AGILITY\" start=\"3\" value=\"3\"/>\n"
			+"   <attr id=\"REACTION\" value=\"0\"/>\n"
			+"   <attr id=\"STRENGTH\" value=\"0\"/>\n"
			+"   <attr id=\"WILLPOWER\" value=\"0\"/>\n"
			+"   <attr id=\"LOGIC\" value=\"0\"/>\n"
			+"   <attr id=\"INTUITION\" value=\"0\"/>\n"
			+"   <attr id=\"CHARISMA\" value=\"0\"/>\n"
			+"</attributes>\n";
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	public AttributeSerializationTest() throws Exception {
//		Format format = new Format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		AttributeValue data = new AttributeValue(Attribute.AGILITY, 3);
		
		try {
			StringWriter out = new StringWriter();
			serializer.write(data, out);
			System.out.println(out.toString());
			
			assertEquals(DATA, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			AttributeValue result = (AttributeValue) serializer.read(AttributeValue.class, new StringReader(DATA));
			
			assertEquals(Attribute.AGILITY, result.getModifyable());
			assertEquals(3, result.getModifiedValue());
		} catch (Exception e) {
			fail(e.toString());
		}
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void serializeList() {
////		AttributeValue data2 = new AttributeValue(Attribute.AGILITY, 3);
//		Attributes data = new Attributes();
////		data.add(data2);
//		for (Attribute attr : Attribute.primaryValues()) {
//			AttributeValue toAdd = new AttributeValue(attr, 0);
//			data.add(toAdd);
//		}
////		data.set(Attribute.AGILITY, 3);
//		
//		try {
//			StringWriter out = new StringWriter();
//			serializer.write(data, out);
//			
//			assertEquals(DATA2, out.toString());
//		} catch (Exception e) {
//			fail(e.toString());
//		}
//	}

	//-------------------------------------------------------------------
	@Test
	public void deserializeList() {
		try {
			System.out.println(DATA2);
			Attributes result = serializer.read(Attributes.class, new StringReader(DATA2));
			System.out.println(result.toString());
			assertEquals(0, result.get(Attribute.CHARISMA).getModifiedValue());
			assertEquals(3, result.get(Attribute.AGILITY ).getModifiedValue());
		} catch (Exception e) {
			fail(e.toString());
		}
	}

}
