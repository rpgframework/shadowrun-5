/**
 * 
 */
package simplexml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class MetaTypeSerializationTest {

	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String DATA = HEAD
			+"\n<metatype id=\"human\">"
			+"\n   <modifications>"
			+"\n      <attrmod attr=\"AGILITY\" modType=\"RELATIVE\" type=\"CURRENT\" val=\"3\"/>"
			+"\n   </modifications>"
			+"\n</metatype>\n";
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
	}

	//-------------------------------------------------------------------
	public MetaTypeSerializationTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		MetaType data = new MetaType("human");
		ModificationList data2 = new ModificationList();
		data2.add(new AttributeModification(Attribute.AGILITY, 3));
		data.add(data2);
		
		try {
			StringWriter out = new StringWriter();
			serializer.write(data, out);
			System.out.println(out.toString());
			
			assertEquals(DATA, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() throws Exception {
		MetaType result = (MetaType) serializer.read(MetaType.class, new StringReader(DATA));

		assertFalse(result.getModifications().isEmpty());
		assertEquals(Attribute.AGILITY, ((AttributeModification)result.getModifications().iterator().next()).getAttribute());
		assertEquals(3, ((AttributeModification)result.getModifications().iterator().next()).getValue());
	}

}
