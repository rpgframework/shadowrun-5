/**
 * 
 */
package simplexml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.junit.Test;
import org.prelle.shadowrun5.Attribute;
import org.prelle.shadowrun5.MetaType;
import org.prelle.shadowrun5.modifications.AttributeModification;
import org.prelle.shadowrun5.modifications.ModificationList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class ModificationSerializationTest {

	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String DATA = HEAD
			+"\n<modifications>"
			+"\n   <attrmod attr=\"AGILITY\" modType=\"RELATIVE\" type=\"CURRENT\" val=\"3\"/>"
			+"\n</modifications>\n";
	final static String DATA2 = HEAD
			+"\n<metatype id=\"human\">"
			+"\n   <modifications>"
			+"\n      <attrmod attr=\"AGILITY\" modType=\"RELATIVE\" type=\"CURRENT\" val=\"3\"/>"
			+"\n   </modifications>"
			+"\n</metatype>\n";
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
	}

	//-------------------------------------------------------------------
	public ModificationSerializationTest() throws Exception {
//		Format format = new Format("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() throws SerializationException, IOException {
		ModificationList data = new ModificationList();
		data.add(new AttributeModification(Attribute.AGILITY, 3));
		
			StringWriter out = new StringWriter();
			serializer.write(data, out);
			System.out.println(out.toString());
			
			assertEquals(DATA, out.toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() throws SerializationException, IOException {
		System.out.println("------------deserialize-----------------------------");
		ModificationList data = new ModificationList();
		data.add(new AttributeModification(Attribute.AGILITY, 3));

		ModificationList result = serializer.read(ModificationList.class, new StringReader(DATA));
			
			assertEquals(data, result);
	}

	//-------------------------------------------------------------------
	@Test
	public void serializeMeta() {
		System.out.println("------------serializeMeta-----------------------------");
		MetaType data = new MetaType("human");
		ModificationList data2 = new ModificationList();
		data2.add(new AttributeModification(Attribute.AGILITY, 3));
		data.add(data2);
		
		try {
			StringWriter out = new StringWriter();
			serializer.write(data, out);
			System.out.println(out.toString());
			
			assertEquals(DATA2, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
