/**
 * 
 */
package simplexml.items;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.prelle.shadowrun5.items.AmmunitionSlot;
import org.prelle.shadowrun5.items.Availability;
import org.prelle.shadowrun5.items.CarriedItem;
import org.prelle.shadowrun5.items.Damage;
import org.prelle.shadowrun5.items.FireMode;
import org.prelle.shadowrun5.items.ItemAttribute;
import org.prelle.shadowrun5.items.ItemHook;
import org.prelle.shadowrun5.items.ItemTemplate;
import org.prelle.shadowrun5.items.WeaponData;
import org.prelle.shadowrun5.items.AmmunitionSlot.AmmoSlotType;
import org.prelle.shadowrun5.items.Damage.Type;
import org.prelle.shadowrun5.items.Damage.WeaponDamageType;
import org.prelle.shadowrun5.items.ItemTemplate.Legality;
import org.prelle.shadowrun5.modifications.AccessoryModification;
import org.prelle.shadowrun5.modifications.ItemAttributeModification;
import org.prelle.shadowrun5.modifications.ItemHookModification;

/**
 * @author prelle
 *
 */
public class WeaponTest {
	
	private static ItemTemplate laser;
	private static ItemTemplate shotgun;
	private static ItemTemplate chainsaw;
	private static ItemTemplate smart;
	private static ItemTemplate ares;


	//-------------------------------------------------------------------
	static {
		laser = new ItemTemplate("lasermarker");
		laser.getModifications().add(new ItemAttributeModification(ItemAttribute.ACCURACY, 1));
		smart = new ItemTemplate("smartlink");
		smart.getModifications().add(new ItemAttributeModification(ItemAttribute.ACCURACY, 2));
		
		WeaponData shotgunData = new WeaponData();
		shotgunData.setAccuracy(4);
		shotgunData.setDamage(new Damage(13, false, Type.PHYSICAL, WeaponDamageType.NORMAL));
		shotgunData.setArmorPenetration(-1);
		shotgunData.setFireModes(Arrays.asList(FireMode.SEMI_AUTOMATIC, FireMode.BURST_FIRE));
		shotgunData.setRecoilCompensation(0);
		shotgunData.setAmmunition(new AmmunitionSlot(10, AmmoSlotType.MAGAZINE));
		shotgun = new ItemTemplate("enfield");
		shotgun.setAvailability(new Availability(12, Legality.FORBIDDEN, false));
		shotgun.setPrice(1100);
		shotgun.setWeaponData(shotgunData);
		shotgun.getModifications().add(new ItemHookModification(ItemHook.BARREL, 0));
		shotgun.getModifications().add(new ItemHookModification(ItemHook.UNDER, 0));
		shotgun.getModifications().add(new ItemHookModification(ItemHook.TOP, 0));
//		shotgun.getModifications().add(new AccessoryModification(ItemHook.BARREL, laser, 0, null));
		
		WeaponData chainsawData = new WeaponData();
		chainsawData.setAccuracy(5);
		chainsawData.setReach(1);
		chainsawData.setDamage(new Damage(8, false, Type.PHYSICAL, WeaponDamageType.NORMAL));
		chainsawData.setArmorPenetration(4);
		chainsaw = new ItemTemplate("Unterlauf Kettensäge");
		chainsaw.setWeaponData(chainsawData);

		ares = new ItemTemplate("ares_predator_v");
		WeaponData aresData = new WeaponData();
		aresData.setAccuracy(5);
		ares.setWeaponData(aresData);
		ares.getModifications().add(new ItemHookModification(ItemHook.BARREL, 0));
		ares.getModifications().add(new ItemHookModification(ItemHook.UNDER, 0));
		ares.getModifications().add(new ItemHookModification(ItemHook.TOP, 0));
		ares.getModifications().add(new AccessoryModification(ItemHook.INTERNAL, smart, 0, null));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	//-------------------------------------------------------------------
	/**
	 * Test method for {@link org.prelle.shadowrun5.items.CarriedItem#getAsValue(org.prelle.shadowrun5.items.ItemAttribute)}.
	 */
	@Test
	public void testGetAsValue() {
		CarriedItem laserItem = new CarriedItem(laser);
		CarriedItem shotgunItem = new CarriedItem(shotgun);
		shotgunItem.addAccessory(ItemHook.TOP, laserItem);
		
		System.out.println("Accuracy = "+shotgunItem.getAsValue(ItemAttribute.ACCURACY));
		assertEquals(4, shotgunItem.getAsValue(ItemAttribute.ACCURACY).getPoints());
		assertEquals(5, shotgunItem.getAsValue(ItemAttribute.ACCURACY).getModifiedValue());
		assertEquals(13, shotgunItem.getAsValue(ItemAttribute.DAMAGE).getPoints());
		assertEquals(13, shotgunItem.getAsValue(ItemAttribute.DAMAGE).getModifiedValue());
		assertEquals(-1, shotgunItem.getAsValue(ItemAttribute.ARMOR_PENETRATION).getPoints());
		assertEquals(-1, shotgunItem.getAsValue(ItemAttribute.ARMOR_PENETRATION).getModifiedValue());
		
		assertEquals(1, shotgunItem.getSubItems().size());
		
		assertEquals(1100, shotgunItem.getAsObject(ItemAttribute.PRICE));
	}

	//-------------------------------------------------------------------
	@Test
	public void testWithChainsaw() {
		CarriedItem laserItem = new CarriedItem(laser);
		CarriedItem shotgunItem = new CarriedItem(shotgun);
		CarriedItem chainsawItem = new CarriedItem(chainsaw);
		shotgunItem.addAccessory(ItemHook.TOP, laserItem);
		shotgunItem.addAccessory(ItemHook.UNDER, chainsawItem);
		
		System.out.println("Accuracy = "+shotgunItem.getAsValue(ItemAttribute.ACCURACY));
		assertEquals(4, shotgunItem.getAsValue(ItemAttribute.ACCURACY).getPoints());
		assertEquals(5, shotgunItem.getAsValue(ItemAttribute.ACCURACY).getModifiedValue());
		assertEquals(13, shotgunItem.getAsValue(ItemAttribute.DAMAGE).getPoints());
		assertEquals(13, shotgunItem.getAsValue(ItemAttribute.DAMAGE).getModifiedValue());
		assertEquals(-1, shotgunItem.getAsValue(ItemAttribute.ARMOR_PENETRATION).getPoints());
		assertEquals(-1, shotgunItem.getAsValue(ItemAttribute.ARMOR_PENETRATION).getModifiedValue());
		
		assertEquals(2, shotgunItem.getSubItems().size());
//		for (WeaponData wdata : shotgunItem.getWeaponData()) {
//			System.out.println("wdata = "+wdata.getAccuracy());
//		}
		
		assertEquals(1100, shotgunItem.getAsObject(ItemAttribute.PRICE));
	}

	//-------------------------------------------------------------------
	@Test
	public void testWithAres() {
		CarriedItem aresItem = new CarriedItem(ares);
		
		System.out.println("Accuracy = "+aresItem.getAsValue(ItemAttribute.ACCURACY));
		assertEquals(5, aresItem.getAsValue(ItemAttribute.ACCURACY).getPoints());
		assertEquals(7, aresItem.getAsValue(ItemAttribute.ACCURACY).getModifiedValue());
		
		aresItem.updateModifications();
		assertEquals(5, aresItem.getAsValue(ItemAttribute.ACCURACY).getPoints());
		assertEquals(7, aresItem.getAsValue(ItemAttribute.ACCURACY).getModifiedValue());
	}

}
