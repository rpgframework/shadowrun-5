/**
 * @author Stefan Prelle
 *
 */
module shadowrun.data {
	exports org.prelle.rpgframework.shadowrun5.data;

	provides de.rpgframework.character.RulePlugin with org.prelle.rpgframework.shadowrun5.data.Shadowrun5DataPlugin;

	requires org.apache.logging.log4j;
	requires transitive de.rpgframework.core;
	requires transitive de.rpgframework.chars;
	requires transitive shadowrun.core;
}