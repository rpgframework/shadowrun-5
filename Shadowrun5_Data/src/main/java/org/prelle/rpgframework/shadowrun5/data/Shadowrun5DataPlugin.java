/**
 *
 */
package org.prelle.rpgframework.shadowrun5.data;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun5.BasePluginData;
import org.prelle.shadowrun5.ShadowrunCharacter;
import org.prelle.shadowrun5.ShadowrunCore;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun5DataPlugin implements RulePlugin<ShadowrunCharacter> {

	private final static Logger logger = LogManager.getLogger("shadowrun5.data");

	//-------------------------------------------------------------------
	/**
	 */
	public Shadowrun5DataPlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return Arrays.asList(new RulePluginFeatures[]{RulePluginFeatures.DATA});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
			return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		return new CommandResult(type, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		String pack = getClass().getPackage().getName();
		pack = "org/prelle/rpgframework/shadowrun5/data";
		Class<Shadowrun5DataPlugin> clazz = Shadowrun5DataPlugin.class;

		logger.info("START -------------------------------Core-----------------------------------------------");
		PluginSkeleton CORE = new PluginSkeleton("CORE", "Shadowrun 5 Core Rules");
		ShadowrunCore.loadSkillgroups(CORE, clazz.getResourceAsStream("core/data/skillgroups.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSkills(CORE, clazz.getResourceAsStream("core/data/skills.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpellFeatures(CORE, clazz.getResourceAsStream("core/data/spellfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpells(CORE, clazz.getResourceAsStream("core/data/spells.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRitualFeatures(CORE, clazz.getResourceAsStream("core/data/ritualfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRituals(CORE, clazz.getResourceAsStream("core/data/rituals.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadQualities(CORE, clazz.getResourceAsStream("core/data/qualities.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMagicOrResonanceTypes(CORE, clazz.getResourceAsStream("core/data/magicOrResonance.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment(CORE, clazz.getResourceAsStream("core/data/items_accessories2.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment(CORE, clazz.getResourceAsStream("core/data/equipment2.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment(CORE, clazz.getResourceAsStream("core/data/equipment_eyeware.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment(CORE, clazz.getResourceAsStream("core/data/equipment_2ndPass.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetaTypes(CORE, clazz.getResourceAsStream("core/data/metatypes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadPriorityTableEntries(CORE, clazz.getResourceAsStream("core/data/priorities.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadAdeptPowers(CORE, clazz.getResourceAsStream("core/data/adeptpowers.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadComplexForms(CORE, clazz.getResourceAsStream("core/data/complexforms.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLifestyles(CORE, clazz.getResourceAsStream("core/data/lifestyles.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLifestyleOptions(CORE, clazz.getResourceAsStream("core/data/lifestyleoptions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.initCharacterConcepts(clazz.getResourceAsStream("core/data/concepts.xml"));
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpirits(CORE, clazz.getResourceAsStream("core/data/spirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSprites(CORE, clazz.getResourceAsStream("core/data/sprites.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadTraditions(CORE, clazz.getResourceAsStream("core/data/traditions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMentorSpirits(CORE, clazz.getResourceAsStream("core/data/mentorspirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSensorFunctions(CORE, clazz.getResourceAsStream("core/data/sensorfunctions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadPrograms(CORE, clazz.getResourceAsStream("core/data/programs.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetamagics(CORE, clazz.getResourceAsStream("core/data/metamagics.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEchoes(CORE, clazz.getResourceAsStream("core/data/echoes.xml"), CORE.getResources(), CORE.getHelpResources());
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Run Faster-----------------------------------------");
		PluginSkeleton RUNF = new PluginSkeleton("RUNFASTER", "Run Faster");
		ShadowrunCore.loadQualities(RUNF, clazz.getResourceAsStream("runfaster/data/qualities.xml"), RUNF.getResources(), RUNF.getHelpResources());
		ShadowrunCore.loadQualities(RUNF, clazz.getResourceAsStream("runfaster/data/qualities_metagenic.xml"), RUNF.getResources(), RUNF.getHelpResources());
		ShadowrunCore.loadMetaTypes(RUNF, clazz.getResourceAsStream("runfaster/data/metatypes.xml"), RUNF.getResources(), RUNF.getHelpResources());
		ShadowrunCore.loadPriorityTableEntries(RUNF, clazz.getResourceAsStream("runfaster/data/priorities.xml"), RUNF.getResources(), RUNF.getHelpResources());
		ShadowrunCore.loadLifestyles(RUNF, clazz.getResourceAsStream("runfaster/data/lifestyles.xml"), RUNF.getResources(), RUNF.getHelpResources());
		ShadowrunCore.loadLifestyleOptions(RUNF, clazz.getResourceAsStream("runfaster/data/lifestyleoptions.xml"), RUNF.getResources(), RUNF.getHelpResources());
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Street Grimoire------------------------------------");
		PluginSkeleton STREET = new PluginSkeleton("STREETGRIMOIRE", "Street Grimoire");
		ShadowrunCore.loadSpells(STREET, clazz.getResourceAsStream("streetgrimoire/data/spells.xml"), STREET.getResources(), STREET.getHelpResources());
		ShadowrunCore.loadRitualFeatures(STREET, clazz.getResourceAsStream("streetgrimoire/data/ritualfeatures.xml"), STREET.getResources(), STREET.getHelpResources());
		ShadowrunCore.loadQualities(STREET, clazz.getResourceAsStream("streetgrimoire/data/qualities.xml"), STREET.getResources(), STREET.getHelpResources());
		ShadowrunCore.loadTraditions(STREET, clazz.getResourceAsStream("streetgrimoire/data/traditions.xml"), STREET.getResources(), STREET.getHelpResources());
		ShadowrunCore.loadAdeptPowers(STREET, clazz.getResourceAsStream("streetgrimoire/data/adeptpowers.xml"), STREET.getResources(), STREET.getHelpResources());
		ShadowrunCore.loadMentorSpirits(STREET, clazz.getResourceAsStream("streetgrimoire/data/mentorspirits.xml"), STREET.getResources(), STREET.getHelpResources());
		BasePluginData.flushMissingKeys();

//		logger.info("START -------------------------------Shadow Spells--------------------------------------");
//		PluginSkeleton SHSPELL = new PluginSkeleton("STREETGRIMOIRE", "Street Grimoire");
//		ShadowrunCore.loadSpellFeatures(SHSPELL, clazz.getResourceAsStream("shadowspells/data/spellfeatures.xml"), SHSPELL.getResources(), SHSPELL.getHelpResources());
//		ShadowrunCore.loadSpells(SHSPELL, clazz.getResourceAsStream("shadowspells/data/spells.xml"), SHSPELL.getResources(), SHSPELL.getHelpResources());
//		BasePluginData.flushMissingKeys();


		logger.info("START -------------------------------Bullets and Bandages-------------------------------");
		PluginSkeleton BULLETS = new PluginSkeleton("RUNFASTER", "Run Faster");
//		ShadowrunCore.loadQualities(RUNF, clazz.getResourceAsStream("bullets/data/qualities.xml"), RUNF.getResources(), BULLETS.getHelpResources());
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Run and Gun----------------------------------------");
		PluginSkeleton RUNGUN = new PluginSkeleton("RUNANDGUN", "Run and gun");
		ShadowrunCore.loadQualities(RUNGUN, clazz.getResourceAsStream("runandgun/data/qualities.xml"), RUNGUN.getResources(), RUNGUN.getHelpResources());
		ShadowrunCore.loadEquipment(RUNGUN, clazz.getResourceAsStream("runandgun/data/items.xml"), RUNGUN.getResources(), RUNGUN.getHelpResources());
		BasePluginData.flushMissingKeys();

//		logger.info("START -------------------------------Hard Targets---------------------------------------");
//		PluginSkeleton HARD = new PluginSkeleton("HARD_TARGETS", "Run Faster");
//		ShadowrunCore.loadEquipment(HARD, clazz.getResourceAsStream("hard_targets/data/accessories.xml"), HARD.getResources(), HARD.getHelpResources());
//		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Kill Code------------------------------------------");
		PluginSkeleton KILL = new PluginSkeleton("KILLCODE", "Kill Code");
		ShadowrunCore.loadQualities(KILL, clazz.getResourceAsStream("killcode/data/qualities.xml"), KILL.getResources(), KILL.getHelpResources());
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Grimmes Erwachen-----------------------------------");
		PluginSkeleton GRIMM = new PluginSkeleton("grimm", "Grimmes Erwachen");
		ShadowrunCore.loadTraditions(GRIMM, clazz.getResourceAsStream("grimm/data/traditions.xml"), GRIMM.getResources(), GRIMM.getHelpResources());
		BasePluginData.flushMissingKeys();


		logger.debug("STOP : -----Init "+getID()+"---------------------------");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getLanguages()
	 */
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}
