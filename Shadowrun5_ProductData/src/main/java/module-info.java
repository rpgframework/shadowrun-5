/**
 * @author Stefan Prelle
 *
 */
module rpgframework.products.shadowrun5 {
	exports de.rpgframework.products.shadowrun5;
	opens de.rpgframework.products.shadowrun5;

	provides de.rpgframework.products.ProductDataPlugin with de.rpgframework.products.shadowrun5.ProductDataShadowrun5;

	requires de.rpgframework.core;
	requires de.rpgframework.products;
}